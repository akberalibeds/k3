-- MySQL dump 10.13  Distrib 5.7.21, for Linux (x86_64)
--
-- Host: localhost    Database: giomani_laravel
-- ------------------------------------------------------
-- Server version	5.7.21-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `Time`
--

DROP TABLE IF EXISTS `Time`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Time` (
  `getOrderTime` varchar(100) DEFAULT NULL,
  `backupTime` varchar(100) DEFAULT NULL,
  `getActiveOrderTime` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Time`
--

LOCK TABLES `Time` WRITE;
/*!40000 ALTER TABLE `Time` DISABLE KEYS */;
/*!40000 ALTER TABLE `Time` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `accounts`
--

DROP TABLE IF EXISTS `accounts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `accounts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uname` varchar(255) DEFAULT NULL,
  `pwd` varchar(255) DEFAULT NULL,
  `cid` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `accounts`
--

LOCK TABLES `accounts` WRITE;
/*!40000 ALTER TABLE `accounts` DISABLE KEYS */;
/*!40000 ALTER TABLE `accounts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `action_log`
--

DROP TABLE IF EXISTS `action_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `action_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `action` varchar(7) DEFAULT NULL,
  `hash` varchar(64) DEFAULT NULL,
  `more` varchar(48) DEFAULT NULL,
  `term` varchar(24) DEFAULT NULL,
  `u` varchar(32) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `action_log`
--

LOCK TABLES `action_log` WRITE;
/*!40000 ALTER TABLE `action_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `action_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `address`
--

DROP TABLE IF EXISTS `address`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `address` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_customer` int(11) DEFAULT NULL,
  `id_address_type` int(11) DEFAULT NULL,
  `id_address_status` int(11) DEFAULT NULL,
  `name` varchar(128) DEFAULT NULL,
  `address_line_1` varchar(128) DEFAULT NULL,
  `address_line_2` varchar(128) DEFAULT NULL,
  `address_line_3` varchar(128) DEFAULT NULL,
  `address_line_4` varchar(128) DEFAULT NULL,
  `address_line_5` varchar(128) DEFAULT NULL,
  `postal_reference` varchar(20) DEFAULT NULL,
  `country_code` varchar(4) DEFAULT NULL,
  `longitude` varchar(32) DEFAULT NULL,
  `latitude` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `address`
--

LOCK TABLES `address` WRITE;
/*!40000 ALTER TABLE `address` DISABLE KEYS */;
/*!40000 ALTER TABLE `address` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `address_status`
--

DROP TABLE IF EXISTS `address_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `address_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `address_status` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `address_status`
--

LOCK TABLES `address_status` WRITE;
/*!40000 ALTER TABLE `address_status` DISABLE KEYS */;
/*!40000 ALTER TABLE `address_status` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `address_type`
--

DROP TABLE IF EXISTS `address_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `address_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `address_type` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `address_type`
--

LOCK TABLES `address_type` WRITE;
/*!40000 ALTER TABLE `address_type` DISABLE KEYS */;
/*!40000 ALTER TABLE `address_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `agents`
--

DROP TABLE IF EXISTS `agents`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `agents` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `name` varchar(400) DEFAULT NULL,
  `paid` double DEFAULT NULL,
  `owed` double DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `agents`
--

LOCK TABLES `agents` WRITE;
/*!40000 ALTER TABLE `agents` DISABLE KEYS */;
/*!40000 ALTER TABLE `agents` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `allowed_ips`
--

DROP TABLE IF EXISTS `allowed_ips`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `allowed_ips` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `address` varchar(48) DEFAULT NULL,
  `description` text,
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `allowed_ips`
--

LOCK TABLES `allowed_ips` WRITE;
/*!40000 ALTER TABLE `allowed_ips` DISABLE KEYS */;
/*!40000 ALTER TABLE `allowed_ips` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `amazonStock`
--

DROP TABLE IF EXISTS `amazonStock`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `amazonStock` (
  `itemCode` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `amazonStock`
--

LOCK TABLES `amazonStock` WRITE;
/*!40000 ALTER TABLE `amazonStock` DISABLE KEYS */;
/*!40000 ALTER TABLE `amazonStock` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `archiveOrders`
--

DROP TABLE IF EXISTS `archiveOrders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `archiveOrders` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `cid` int(6) DEFAULT NULL,
  `orderStatus` varchar(100) DEFAULT NULL,
  `startDate` varchar(50) DEFAULT NULL,
  `startTime` varchar(50) DEFAULT NULL,
  `endDate` varchar(50) DEFAULT NULL,
  `endTime` varchar(50) DEFAULT NULL,
  `staffName` varchar(50) DEFAULT NULL,
  `staffid` int(6) DEFAULT NULL,
  `paid` double DEFAULT '0',
  `total` double DEFAULT '0',
  `lastMaintainer` varchar(100) DEFAULT '',
  `lastMaintainID` int(3) DEFAULT NULL,
  `delRoute` varchar(200) DEFAULT NULL,
  `dispatchType` varchar(100) DEFAULT 'Delivery',
  `collector` varchar(200) DEFAULT NULL,
  `colDelDate` varchar(100) DEFAULT NULL,
  `carrier` varchar(200) DEFAULT NULL,
  `orderType` varchar(100) DEFAULT NULL,
  `direct` int(1) DEFAULT NULL,
  `paymentType` varchar(200) DEFAULT NULL,
  `delOrder` int(5) DEFAULT NULL,
  `authNo` varchar(100) DEFAULT NULL,
  `cardNo` varchar(4) DEFAULT NULL,
  `exp` varchar(20) DEFAULT NULL,
  `companyName` varchar(200) DEFAULT NULL,
  `ouref` varchar(400) DEFAULT NULL,
  `time_stamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `proforma` int(1) NOT NULL DEFAULT '0',
  `showVat` int(1) DEFAULT '0',
  `showNotes` int(1) DEFAULT '1',
  `serType` varchar(100) DEFAULT 'sale',
  `icType` varchar(100) DEFAULT 'invoice',
  `iNo` varchar(100) DEFAULT NULL,
  `ebayID` varchar(400) DEFAULT NULL,
  `vatAmount` double DEFAULT NULL,
  `zeroVat` int(1) DEFAULT '0',
  `ebayStatus` varchar(100) DEFAULT NULL,
  `deliverBy` varchar(100) NOT NULL DEFAULT '0',
  `tx` int(255) NOT NULL DEFAULT '0',
  `delTime` varchar(20) DEFAULT NULL,
  `delStat` varchar(20) DEFAULT NULL,
  `agent` varchar(400) DEFAULT NULL,
  `agentPaid` int(1) DEFAULT '0',
  `agentOwed` double DEFAULT '0',
  `rate` double DEFAULT '0',
  `txt` int(1) DEFAULT '0',
  `code` varchar(200) DEFAULT NULL,
  `parcelid` varchar(20) DEFAULT NULL,
  `neighbour` varchar(400) DEFAULT '0',
  `aftersales` int(1) DEFAULT '0',
  `safePlace` varchar(200) DEFAULT '0',
  `done` int(1) DEFAULT '0',
  `ebay` varchar(100) DEFAULT '0',
  `notLoaded` int(1) DEFAULT '0',
  `p` int(1) DEFAULT '0',
  `wes` int(1) DEFAULT '0',
  `cStatus` varchar(100) DEFAULT 'none',
  `colOrder` int(10) DEFAULT '0',
  `colTime` varchar(10) DEFAULT NULL,
  `colRoute` varchar(100) DEFAULT NULL,
  `extra` varchar(10) DEFAULT NULL,
  `beds` varchar(255) DEFAULT '0',
  `delDate` varchar(20) DEFAULT NULL,
  `colID` int(255) DEFAULT '0',
  `delID` int(255) DEFAULT '0',
  `lat` varchar(100) DEFAULT NULL,
  `lng` varchar(100) DEFAULT NULL,
  `amazonID` varchar(100) DEFAULT NULL,
  `amzShipped` int(1) DEFAULT '0',
  `refund` int(1) DEFAULT '0',
  `postLoc` varchar(50) DEFAULT '0',
  `post` int(1) DEFAULT '0',
  `sender` varchar(200) DEFAULT '0',
  `stamp` varchar(100) DEFAULT NULL,
  `startStamp` varchar(100) DEFAULT '0',
  `wholeError` int(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `cid` (`cid`)
) ENGINE=MyISAM AUTO_INCREMENT=137898 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `archiveOrders`
--

LOCK TABLES `archiveOrders` WRITE;
/*!40000 ALTER TABLE `archiveOrders` DISABLE KEYS */;
/*!40000 ALTER TABLE `archiveOrders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `barcodes`
--

DROP TABLE IF EXISTS `barcodes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `barcodes` (
  `oid` int(255) DEFAULT NULL,
  `code` varchar(400) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `barcodes`
--

LOCK TABLES `barcodes` WRITE;
/*!40000 ALTER TABLE `barcodes` DISABLE KEYS */;
/*!40000 ALTER TABLE `barcodes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `blockDays`
--

DROP TABLE IF EXISTS `blockDays`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `blockDays` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` varchar(100) DEFAULT NULL,
  `saturday` date DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `blockDays`
--

LOCK TABLES `blockDays` WRITE;
/*!40000 ALTER TABLE `blockDays` DISABLE KEYS */;
/*!40000 ALTER TABLE `blockDays` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bulletin`
--

DROP TABLE IF EXISTS `bulletin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bulletin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `expires` timestamp NULL DEFAULT NULL,
  `message` text,
  `staff_id` int(11) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bulletin`
--

LOCK TABLES `bulletin` WRITE;
/*!40000 ALTER TABLE `bulletin` DISABLE KEYS */;
/*!40000 ALTER TABLE `bulletin` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bulletins`
--

DROP TABLE IF EXISTS `bulletins`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bulletins` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `expires` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `message` text COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bulletins`
--

LOCK TABLES `bulletins` WRITE;
/*!40000 ALTER TABLE `bulletins` DISABLE KEYS */;
/*!40000 ALTER TABLE `bulletins` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `camp`
--

DROP TABLE IF EXISTS `camp`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `camp` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(400) DEFAULT NULL,
  `sent` int(11) DEFAULT NULL,
  `not_sent` int(11) DEFAULT NULL,
  `date` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `camp`
--

LOCK TABLES `camp` WRITE;
/*!40000 ALTER TABLE `camp` DISABLE KEYS */;
/*!40000 ALTER TABLE `camp` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cancelled_orders`
--

DROP TABLE IF EXISTS `cancelled_orders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cancelled_orders` (
  `id` int(11) NOT NULL DEFAULT '0',
  `id_company` int(11) DEFAULT '0',
  `id_status` int(11) DEFAULT '1' COMMENT 'This should be ‘id_order_status’; the values are form the ‘order_status’ table',
  `cid` int(11) DEFAULT NULL,
  `delRoute` varchar(32) CHARACTER SET utf8 DEFAULT NULL,
  `endDate` varchar(32) CHARACTER SET utf8 DEFAULT NULL,
  `endTime` varchar(32) CHARACTER SET utf8 DEFAULT NULL,
  `lastMaintainer` varchar(48) CHARACTER SET utf8 DEFAULT '',
  `lastMaintainID` int(3) DEFAULT NULL,
  `orderStatus` varchar(24) CHARACTER SET utf8 DEFAULT NULL,
  `paid` double DEFAULT '0',
  `startDate` varchar(24) CHARACTER SET utf8 DEFAULT NULL,
  `startTime` varchar(24) CHARACTER SET utf8 DEFAULT NULL,
  `staffName` varchar(48) CHARACTER SET utf8 DEFAULT NULL,
  `staffid` int(6) DEFAULT NULL,
  `total` double DEFAULT '0',
  `dispatchType` varchar(32) CHARACTER SET utf8 DEFAULT 'Delivery',
  `collector` varchar(32) CHARACTER SET utf8 DEFAULT NULL,
  `colDelDate` varchar(24) CHARACTER SET utf8 DEFAULT NULL,
  `carrier` varchar(32) CHARACTER SET utf8 DEFAULT NULL,
  `orderType` varchar(32) CHARACTER SET utf8 DEFAULT NULL,
  `direct` int(1) DEFAULT NULL,
  `paymentType` varchar(32) CHARACTER SET utf8 DEFAULT NULL,
  `delOrder` int(5) DEFAULT NULL,
  `authNo` varchar(32) CHARACTER SET utf8 DEFAULT NULL,
  `cardNo` varchar(4) CHARACTER SET utf8 DEFAULT NULL,
  `exp` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `companyName` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `ouref` varchar(400) CHARACTER SET utf8 DEFAULT NULL,
  `time_stamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `proforma` int(1) NOT NULL DEFAULT '0',
  `showVat` int(1) DEFAULT '0',
  `showNotes` int(1) DEFAULT '1',
  `serType` varchar(100) CHARACTER SET utf8 DEFAULT 'sale',
  `icType` varchar(100) CHARACTER SET utf8 DEFAULT 'invoice',
  `iNo` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `ebayID` varchar(64) CHARACTER SET utf8 DEFAULT NULL,
  `vatAmount` double DEFAULT NULL,
  `zeroVat` int(1) DEFAULT '0',
  `ebayStatus` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `deliverBy` varchar(100) CHARACTER SET utf8 NOT NULL DEFAULT '0',
  `tx` int(255) NOT NULL DEFAULT '0',
  `delTime` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `delStat` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `agent` text CHARACTER SET utf8,
  `agentPaid` int(1) DEFAULT '0',
  `agentOwed` double DEFAULT '0',
  `rate` double DEFAULT '0',
  `txt` int(1) DEFAULT '0',
  `code` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `parcelid` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `neighbour` varchar(400) CHARACTER SET utf8 DEFAULT '0',
  `aftersales` int(1) DEFAULT '0',
  `safePlace` varchar(200) CHARACTER SET utf8 DEFAULT '0',
  `done` int(1) DEFAULT '0',
  `ebay` varchar(100) CHARACTER SET utf8 DEFAULT '0',
  `notLoaded` int(1) DEFAULT '0',
  `p` int(1) DEFAULT '0',
  `wes` int(1) DEFAULT '0',
  `cStatus` varchar(100) CHARACTER SET utf8 DEFAULT 'none',
  `colOrder` int(10) DEFAULT '0',
  `colTime` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `colRoute` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `extra` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `beds` varchar(255) CHARACTER SET utf8 DEFAULT '0',
  `delDate` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `colID` int(255) DEFAULT '0',
  `delID` int(255) DEFAULT '0',
  `lat` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `lng` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `amazonID` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `amzShipped` int(1) DEFAULT '0',
  `refund` int(1) DEFAULT '0',
  `postLoc` varchar(50) CHARACTER SET utf8 DEFAULT '0',
  `post` int(1) DEFAULT '0',
  `sender` varchar(200) CHARACTER SET utf8 DEFAULT '0',
  `stamp` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `startStamp` varchar(100) CHARACTER SET utf8 DEFAULT '0',
  `wholeError` int(1) DEFAULT '0',
  `mfError` int(1) DEFAULT '0',
  `feedbackScore` int(1) DEFAULT '0',
  `delissue` int(1) DEFAULT '0',
  `defects` int(1) DEFAULT '0',
  `g_id` varchar(100) CHARACTER SET utf8 DEFAULT '0',
  `g_num` varchar(100) CHARACTER SET utf8 DEFAULT '0',
  `g_desc` varchar(128) CHARACTER SET utf8 DEFAULT '0',
  `g_ship` varchar(24) CHARACTER SET utf8 DEFAULT '0',
  `oos` int(1) DEFAULT '0',
  `mgmt` int(1) DEFAULT '0',
  `processed` int(1) DEFAULT '0',
  `paidBeds` int(1) DEFAULT '0',
  `pTime` varchar(16) CHARACTER SET utf8 DEFAULT '0',
  `refundBox` int(1) DEFAULT '0',
  `id_delivery_route` int(11) DEFAULT '0',
  `id_order_type` int(11) DEFAULT '0',
  `id_payment_type` int(11) DEFAULT '0',
  `hold` int(1) DEFAULT '0',
  `missed` int(11) DEFAULT '0',
  `urgent` int(11) DEFAULT '0',
  `retention` int(11) DEFAULT '0',
  `paypal` int(11) DEFAULT '0',
  `reopen` int(11) DEFAULT '0',
  `id_supplier` int(11) DEFAULT '0',
  `paid_supplier` int(11) DEFAULT '0',
  `invoice_made` int(11) DEFAULT '0',
  `id_seller` int(11) DEFAULT '0',
  `seller_paid` int(11) DEFAULT '0',
  `discount_amount` decimal(9,2) DEFAULT '0.00',
  `discount_rate` decimal(6,2) DEFAULT '0.00' COMMENT 'expressed as a percentage',
  `vat_paid` decimal(6,2) DEFAULT '0.00' COMMENT 'expressed as a percentage',
  `paypal_payment` text CHARACTER SET utf8,
  `last_trans_id` varchar(50) CHARACTER SET utf8 DEFAULT '0',
  `to_collect` int(1) DEFAULT '0',
  `deleted_by` varchar(50) DEFAULT NULL,
  `delete_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `is_split` int(1) DEFAULT '0',
  `paid_courier` int(1) DEFAULT '0',
  `is_collected` int(1) DEFAULT '0',
  `escalate` int(1) DEFAULT '0',
  `ordered` int(1) DEFAULT '0',
  `merch_ref` varchar(50) DEFAULT NULL,
  `trust_pilot` int(1) DEFAULT '0',
  `stolen` int(11) DEFAULT '0',
  `sales_record` varchar(10) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cancelled_orders`
--

LOCK TABLES `cancelled_orders` WRITE;
/*!40000 ALTER TABLE `cancelled_orders` DISABLE KEYS */;
/*!40000 ALTER TABLE `cancelled_orders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categories` (
  `id` int(1) NOT NULL AUTO_INCREMENT,
  `always_in_stock` tinyint(1) DEFAULT '0',
  `id_staff` int(11) DEFAULT NULL,
  `id_supplier` int(11) DEFAULT '0',
  `description` text,
  `name` varchar(200) DEFAULT NULL,
  `staff` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categories`
--

LOCK TABLES `categories` WRITE;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `chats`
--

DROP TABLE IF EXISTS `chats`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `chats` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `sender` varchar(100) DEFAULT NULL,
  `rcvr` varchar(100) DEFAULT NULL,
  `date` varchar(100) DEFAULT NULL,
  `msg` varchar(1000) DEFAULT NULL,
  `seen` int(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `chats`
--

LOCK TABLES `chats` WRITE;
/*!40000 ALTER TABLE `chats` DISABLE KEYS */;
/*!40000 ALTER TABLE `chats` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `clicks`
--

DROP TABLE IF EXISTS `clicks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `clicks` (
  `camp` int(11) DEFAULT NULL,
  `link` varchar(400) DEFAULT NULL,
  `time` varchar(100) DEFAULT NULL,
  `id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `clicks`
--

LOCK TABLES `clicks` WRITE;
/*!40000 ALTER TABLE `clicks` DISABLE KEYS */;
/*!40000 ALTER TABLE `clicks` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `codes`
--

DROP TABLE IF EXISTS `codes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `codes` (
  `lineid` int(11) DEFAULT NULL,
  `bar` varchar(48) DEFAULT NULL,
  `oid` int(11) DEFAULT NULL,
  `parcelid` int(11) DEFAULT NULL,
  `itemCode` varchar(128) DEFAULT NULL,
  `loaded` int(1) DEFAULT '0',
  `warehouse` int(1) DEFAULT '0',
  `complete` int(1) DEFAULT '0',
  `collected` int(1) DEFAULT '0',
  `pk` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`pk`),
  KEY `index_bar` (`bar`),
  KEY `idx_codes_oid` (`oid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `codes`
--

LOCK TABLES `codes` WRITE;
/*!40000 ALTER TABLE `codes` DISABLE KEYS */;
/*!40000 ALTER TABLE `codes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `col`
--

DROP TABLE IF EXISTS `col`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `col` (
  `oid` int(255) DEFAULT NULL,
  `date` varchar(20) DEFAULT NULL,
  `route` varchar(100) DEFAULT NULL,
  `num` int(10) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `col`
--

LOCK TABLES `col` WRITE;
/*!40000 ALTER TABLE `col` DISABLE KEYS */;
/*!40000 ALTER TABLE `col` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `colRoute`
--

DROP TABLE IF EXISTS `colRoute`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `colRoute` (
  `route` varchar(100) DEFAULT NULL,
  `date` varchar(20) DEFAULT NULL,
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `driver` varchar(100) DEFAULT NULL,
  `staff` varchar(100) DEFAULT NULL,
  `status` varchar(100) DEFAULT 'Routed',
  `mate` varchar(100) DEFAULT NULL,
  `vehicle` varchar(100) DEFAULT NULL,
  `payments` double DEFAULT '0',
  `toCollect` double DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `colRoute`
--

LOCK TABLES `colRoute` WRITE;
/*!40000 ALTER TABLE `colRoute` DISABLE KEYS */;
/*!40000 ALTER TABLE `colRoute` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `company`
--

DROP TABLE IF EXISTS `company`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `company` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company_name` varchar(128) DEFAULT NULL,
  `id_email` int(11) DEFAULT '0',
  `l_id_email` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `company`
--

LOCK TABLES `company` WRITE;
/*!40000 ALTER TABLE `company` DISABLE KEYS */;
/*!40000 ALTER TABLE `company` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cons`
--

DROP TABLE IF EXISTS `cons`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cons` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `consid` varchar(20) DEFAULT NULL,
  `deliverTo` varchar(200) DEFAULT NULL,
  `town` varchar(200) DEFAULT NULL,
  `postcode` varchar(20) DEFAULT NULL,
  `pieces` int(10) DEFAULT NULL,
  `weight` double DEFAULT NULL,
  `type` varchar(200) DEFAULT 'Delivery',
  `date` varchar(40) DEFAULT NULL,
  `status` varchar(100) DEFAULT NULL,
  `stamp` varchar(40) DEFAULT NULL,
  `to` int(255) DEFAULT NULL,
  `sender` int(255) DEFAULT NULL,
  `parcelid` int(255) DEFAULT NULL,
  `price` double DEFAULT '0',
  `paid` double DEFAULT '0',
  `oid` int(255) DEFAULT NULL,
  `delDate` varchar(20) DEFAULT NULL,
  `colID` int(255) DEFAULT NULL,
  `hide` int(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cons`
--

LOCK TABLES `cons` WRITE;
/*!40000 ALTER TABLE `cons` DISABLE KEYS */;
/*!40000 ALTER TABLE `cons` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `containerFiles`
--

DROP TABLE IF EXISTS `containerFiles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `containerFiles` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) DEFAULT NULL,
  `containerID` varchar(400) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `containerFiles`
--

LOCK TABLES `containerFiles` WRITE;
/*!40000 ALTER TABLE `containerFiles` DISABLE KEYS */;
/*!40000 ALTER TABLE `containerFiles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `containers`
--

DROP TABLE IF EXISTS `containers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `containers` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `loaded` varchar(20) DEFAULT NULL,
  `eta` varchar(20) DEFAULT NULL,
  `containerID` varchar(400) DEFAULT NULL,
  `rxd` int(1) DEFAULT '0',
  `items` text,
  `staff_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `containers`
--

LOCK TABLES `containers` WRITE;
/*!40000 ALTER TABLE `containers` DISABLE KEYS */;
/*!40000 ALTER TABLE `containers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `coordinates`
--

DROP TABLE IF EXISTS `coordinates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `coordinates` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pc` varchar(10) DEFAULT NULL,
  `lat` varchar(30) DEFAULT NULL,
  `lng` varchar(30) DEFAULT NULL,
  `street` varchar(100) DEFAULT NULL,
  `town` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_postcode` (`pc`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `coordinates`
--

LOCK TABLES `coordinates` WRITE;
/*!40000 ALTER TABLE `coordinates` DISABLE KEYS */;
/*!40000 ALTER TABLE `coordinates` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `count`
--

DROP TABLE IF EXISTS `count`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `count` (
  `num` int(255) DEFAULT NULL,
  `parcel_id_seed` int(11) DEFAULT NULL,
  `orders_count` int(11) DEFAULT NULL,
  `refund` int(11) DEFAULT NULL,
  `post` int(11) DEFAULT NULL,
  `exchange` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `count`
--

LOCK TABLES `count` WRITE;
/*!40000 ALTER TABLE `count` DISABLE KEYS */;
/*!40000 ALTER TABLE `count` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `counter`
--

DROP TABLE IF EXISTS `counter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `counter` (
  `num` int(255) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `counter`
--

LOCK TABLES `counter` WRITE;
/*!40000 ALTER TABLE `counter` DISABLE KEYS */;
/*!40000 ALTER TABLE `counter` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `credit_notes`
--

DROP TABLE IF EXISTS `credit_notes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `credit_notes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `amount` decimal(11,2) NOT NULL,
  `order_id` int(11) NOT NULL,
  `seller_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `used` decimal(11,2) DEFAULT '0.00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `credit_notes`
--

LOCK TABLES `credit_notes` WRITE;
/*!40000 ALTER TABLE `credit_notes` DISABLE KEYS */;
/*!40000 ALTER TABLE `credit_notes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cron_tasks`
--

DROP TABLE IF EXISTS `cron_tasks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cron_tasks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `description` varchar(150) DEFAULT NULL,
  `value` int(11) DEFAULT '0',
  `log` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cron_tasks`
--

LOCK TABLES `cron_tasks` WRITE;
/*!40000 ALTER TABLE `cron_tasks` DISABLE KEYS */;
/*!40000 ALTER TABLE `cron_tasks` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customer_type`
--

DROP TABLE IF EXISTS `customer_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customer_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_type` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customer_type`
--

LOCK TABLES `customer_type` WRITE;
/*!40000 ALTER TABLE `customer_type` DISABLE KEYS */;
/*!40000 ALTER TABLE `customer_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customers`
--

DROP TABLE IF EXISTS `customers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `accountNo` varchar(100) DEFAULT NULL,
  `agent` int(6) DEFAULT '0',
  `businessName` varchar(255) DEFAULT NULL,
  `email1` varchar(255) DEFAULT NULL,
  `email2` varchar(255) DEFAULT NULL,
  `email3` varchar(255) DEFAULT NULL,
  `fax` varchar(100) DEFAULT NULL,
  `mob` varchar(100) DEFAULT NULL,
  `number` varchar(255) DEFAULT NULL,
  `web` varchar(200) DEFAULT NULL,
  `street` varchar(255) DEFAULT NULL,
  `tel` varchar(100) DEFAULT NULL,
  `town` varchar(255) DEFAULT NULL,
  `postcode` varchar(20) DEFAULT NULL,
  `dBusinessName` varchar(255) DEFAULT NULL,
  `dNumber` varchar(255) DEFAULT NULL,
  `dStreet` varchar(255) DEFAULT NULL,
  `dTown` varchar(255) DEFAULT NULL,
  `dPostcode` varchar(20) DEFAULT NULL,
  `discount` double DEFAULT '0',
  `id_customer_type` int(11) DEFAULT NULL,
  `id_seller` int(11) DEFAULT NULL,
  `isAccount` int(1) DEFAULT '0',
  `paymentType` varchar(200) DEFAULT NULL,
  `type` varchar(20) DEFAULT NULL,
  `owes` double DEFAULT '0',
  `userID` varchar(200) DEFAULT NULL,
  `company` varchar(255) DEFAULT '0',
  `mail` int(11) DEFAULT '1',
  `timestamp` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `numerical_pc` bigint(20) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `index_postcode` (`postcode`),
  KEY `index_email1` (`email1`),
  KEY `index_email2` (`email2`),
  KEY `index_email3` (`email3`),
  KEY `index_dPostcode` (`dPostcode`),
  KEY `index_dBusinessName` (`dBusinessName`),
  KEY `index_businessName` (`businessName`),
  KEY `index_seller` (`id_seller`),
  KEY `num_pc` (`numerical_pc`),
  FULLTEXT KEY `Index_Text` (`businessName`),
  FULLTEXT KEY `TEXT_postcode` (`postcode`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customers`
--

LOCK TABLES `customers` WRITE;
/*!40000 ALTER TABLE `customers` DISABLE KEYS */;
/*!40000 ALTER TABLE `customers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customers_b`
--

DROP TABLE IF EXISTS `customers_b`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customers_b` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `accountNo` varchar(100) DEFAULT NULL,
  `agent` int(6) DEFAULT '0',
  `businessName` varchar(255) DEFAULT NULL,
  `email1` varchar(255) DEFAULT NULL,
  `email2` varchar(255) DEFAULT NULL,
  `email3` varchar(255) DEFAULT NULL,
  `fax` varchar(100) DEFAULT NULL,
  `mob` varchar(100) DEFAULT NULL,
  `number` varchar(255) DEFAULT NULL,
  `web` varchar(200) DEFAULT NULL,
  `street` varchar(255) DEFAULT NULL,
  `tel` varchar(100) DEFAULT NULL,
  `town` varchar(255) DEFAULT NULL,
  `postcode` varchar(20) DEFAULT NULL,
  `dBusinessName` varchar(255) DEFAULT NULL,
  `dNumber` varchar(255) DEFAULT NULL,
  `dStreet` varchar(255) DEFAULT NULL,
  `dTown` varchar(255) DEFAULT NULL,
  `dPostcode` varchar(20) DEFAULT NULL,
  `discount` double DEFAULT '0',
  `id_customer_type` int(11) DEFAULT NULL,
  `id_seller` int(11) DEFAULT NULL,
  `isAccount` int(1) DEFAULT '0',
  `paymentType` varchar(200) DEFAULT NULL,
  `type` varchar(20) DEFAULT NULL,
  `owes` double DEFAULT '0',
  `userID` varchar(200) DEFAULT NULL,
  `company` varchar(255) DEFAULT '0',
  `mail` int(11) DEFAULT '1',
  `timestamp` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customers_b`
--

LOCK TABLES `customers_b` WRITE;
/*!40000 ALTER TABLE `customers_b` DISABLE KEYS */;
/*!40000 ALTER TABLE `customers_b` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `delCustomers`
--

DROP TABLE IF EXISTS `delCustomers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `delCustomers` (
  `id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `delCustomers`
--

LOCK TABLES `delCustomers` WRITE;
/*!40000 ALTER TABLE `delCustomers` DISABLE KEYS */;
/*!40000 ALTER TABLE `delCustomers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `deliveries`
--

DROP TABLE IF EXISTS `deliveries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `deliveries` (
  `oid` int(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `deliveries`
--

LOCK TABLES `deliveries` WRITE;
/*!40000 ALTER TABLE `deliveries` DISABLE KEYS */;
/*!40000 ALTER TABLE `deliveries` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `delivery`
--

DROP TABLE IF EXISTS `delivery`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `delivery` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `I` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `delivery`
--

LOCK TABLES `delivery` WRITE;
/*!40000 ALTER TABLE `delivery` DISABLE KEYS */;
/*!40000 ALTER TABLE `delivery` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `delivery_status`
--

DROP TABLE IF EXISTS `delivery_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `delivery_status` (
  `id` int(11) NOT NULL,
  `delivery_status` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `delivery_status`
--

LOCK TABLES `delivery_status` WRITE;
/*!40000 ALTER TABLE `delivery_status` DISABLE KEYS */;
/*!40000 ALTER TABLE `delivery_status` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `devices`
--

DROP TABLE IF EXISTS `devices`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `devices` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `imei` varchar(400) DEFAULT NULL,
  `tel` varchar(20) DEFAULT NULL,
  `driver` int(11) DEFAULT NULL,
  `token` text,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `devices`
--

LOCK TABLES `devices` WRITE;
/*!40000 ALTER TABLE `devices` DISABLE KEYS */;
/*!40000 ALTER TABLE `devices` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dictionary`
--

DROP TABLE IF EXISTS `dictionary`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dictionary` (
  `name` varchar(64) NOT NULL DEFAULT '',
  `value` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`name`),
  UNIQUE KEY `name_UNIQUE` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dictionary`
--

LOCK TABLES `dictionary` WRITE;
/*!40000 ALTER TABLE `dictionary` DISABLE KEYS */;
/*!40000 ALTER TABLE `dictionary` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `direct`
--

DROP TABLE IF EXISTS `direct`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `direct` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `oid` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `number` varchar(100) DEFAULT NULL,
  `street` varchar(100) DEFAULT NULL,
  `town` varchar(100) DEFAULT NULL,
  `postcode` varchar(20) DEFAULT NULL,
  `tel` varchar(20) DEFAULT NULL,
  `mob` varchar(20) DEFAULT NULL,
  `email` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index-order_id` (`oid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `direct`
--

LOCK TABLES `direct` WRITE;
/*!40000 ALTER TABLE `direct` DISABLE KEYS */;
/*!40000 ALTER TABLE `direct` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `disp`
--

DROP TABLE IF EXISTS `disp`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `disp` (
  `oid` int(11) DEFAULT NULL,
  `date` varchar(20) DEFAULT NULL,
  `route` varchar(48) DEFAULT NULL,
  `num` int(10) DEFAULT '0',
  `van` int(1) DEFAULT '1',
  `id_route` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `id_disp` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id_disp`),
  KEY `index_oid` (`oid`),
  KEY `index_id_route` (`id_route`),
  KEY `index_date` (`date`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `disp`
--

LOCK TABLES `disp` WRITE;
/*!40000 ALTER TABLE `disp` DISABLE KEYS */;
/*!40000 ALTER TABLE `disp` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dispRoute`
--

DROP TABLE IF EXISTS `dispRoute`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dispRoute` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` varchar(20) DEFAULT NULL,
  `driver` varchar(48) DEFAULT NULL,
  `driverPaid` int(1) DEFAULT '0',
  `mate` varchar(48) DEFAULT NULL,
  `matePaid` int(1) DEFAULT '0',
  `picker` varchar(100) DEFAULT NULL,
  `payments` double DEFAULT '0',
  `route` varchar(24) DEFAULT NULL,
  `staff` varchar(24) DEFAULT NULL,
  `startTime` varchar(20) DEFAULT NULL,
  `status` varchar(24) DEFAULT 'Routed',
  `toCollect` double DEFAULT '0',
  `vehicle` varchar(24) DEFAULT NULL,
  `id_route` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `loader` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_id_route` (`id_route`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dispRoute`
--

LOCK TABLES `dispRoute` WRITE;
/*!40000 ALTER TABLE `dispRoute` DISABLE KEYS */;
/*!40000 ALTER TABLE `dispRoute` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dispatch_type`
--

DROP TABLE IF EXISTS `dispatch_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dispatch_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `dispatch_type` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dispatch_type`
--

LOCK TABLES `dispatch_type` WRITE;
/*!40000 ALTER TABLE `dispatch_type` DISABLE KEYS */;
/*!40000 ALTER TABLE `dispatch_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `driverDays`
--

DROP TABLE IF EXISTS `driverDays`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `driverDays` (
  `name` varchar(200) DEFAULT NULL,
  `day` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `driverDays`
--

LOCK TABLES `driverDays` WRITE;
/*!40000 ALTER TABLE `driverDays` DISABLE KEYS */;
/*!40000 ALTER TABLE `driverDays` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `driverFines`
--

DROP TABLE IF EXISTS `driverFines`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `driverFines` (
  `driver` varchar(100) DEFAULT NULL,
  `amount` double DEFAULT NULL,
  `paid` int(1) DEFAULT '0',
  `date` varchar(20) DEFAULT NULL,
  `staff` varchar(100) DEFAULT NULL,
  `notes` varchar(1000) DEFAULT NULL,
  `id` int(255) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `driverFines`
--

LOCK TABLES `driverFines` WRITE;
/*!40000 ALTER TABLE `driverFines` DISABLE KEYS */;
/*!40000 ALTER TABLE `driverFines` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `driverLocs`
--

DROP TABLE IF EXISTS `driverLocs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `driverLocs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `driver_id` int(11) DEFAULT NULL,
  `lat` double DEFAULT '0',
  `lng` double DEFAULT '0',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `device_INDEX` (`driver_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `driverLocs`
--

LOCK TABLES `driverLocs` WRITE;
/*!40000 ALTER TABLE `driverLocs` DISABLE KEYS */;
/*!40000 ALTER TABLE `driverLocs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `driverPayments`
--

DROP TABLE IF EXISTS `driverPayments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `driverPayments` (
  `dates` varchar(600) DEFAULT NULL,
  `amount` double DEFAULT NULL,
  `date` varchar(20) DEFAULT NULL,
  `driver` varchar(200) DEFAULT NULL,
  `staff` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `driverPayments`
--

LOCK TABLES `driverPayments` WRITE;
/*!40000 ALTER TABLE `driverPayments` DISABLE KEYS */;
/*!40000 ALTER TABLE `driverPayments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `drivers`
--

DROP TABLE IF EXISTS `drivers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `drivers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `score` int(5) DEFAULT NULL,
  `uName` varchar(64) DEFAULT NULL,
  `pwd` varchar(255) DEFAULT NULL,
  `num` varchar(24) DEFAULT NULL,
  `rate` decimal(6,2) DEFAULT '65.00',
  `active` char(1) DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `drivers`
--

LOCK TABLES `drivers` WRITE;
/*!40000 ALTER TABLE `drivers` DISABLE KEYS */;
/*!40000 ALTER TABLE `drivers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `eInvoice`
--

DROP TABLE IF EXISTS `eInvoice`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `eInvoice` (
  `date` varchar(100) DEFAULT NULL,
  `paid` int(1) DEFAULT '0',
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `time` varchar(100) DEFAULT '0',
  `company` varchar(50) DEFAULT NULL,
  `id_supplier` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `eInvoice`
--

LOCK TABLES `eInvoice` WRITE;
/*!40000 ALTER TABLE `eInvoice` DISABLE KEYS */;
/*!40000 ALTER TABLE `eInvoice` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `eInvoiced`
--

DROP TABLE IF EXISTS `eInvoiced`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `eInvoiced` (
  `date` varchar(100) DEFAULT NULL,
  `paid` int(1) DEFAULT '0',
  `id` int(11) NOT NULL,
  `time` varchar(100) DEFAULT '0',
  `company` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `eInvoiced`
--

LOCK TABLES `eInvoiced` WRITE;
/*!40000 ALTER TABLE `eInvoiced` DISABLE KEYS */;
/*!40000 ALTER TABLE `eInvoiced` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ebayAccounts`
--

DROP TABLE IF EXISTS `ebayAccounts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ebayAccounts` (
  `name` varchar(400) DEFAULT NULL,
  `appID` varchar(400) DEFAULT NULL,
  `certID` varchar(400) DEFAULT NULL,
  `devID` varchar(400) DEFAULT NULL,
  `token` varchar(10000) DEFAULT NULL,
  `folder` varchar(100) DEFAULT NULL,
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `compat_level` int(11) DEFAULT NULL,
  `last_edit` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ebayAccounts`
--

LOCK TABLES `ebayAccounts` WRITE;
/*!40000 ALTER TABLE `ebayAccounts` DISABLE KEYS */;
/*!40000 ALTER TABLE `ebayAccounts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ebayStock`
--

DROP TABLE IF EXISTS `ebayStock`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ebayStock` (
  `itemCode` varchar(200) DEFAULT NULL,
  `itemQty` int(100) DEFAULT NULL,
  `itemID` varchar(200) DEFAULT NULL,
  `itemSKU` varchar(200) DEFAULT NULL,
  `updateStock` int(1) DEFAULT '0',
  `ebay` varchar(100) DEFAULT '0',
  `id` int(255) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ebayStock`
--

LOCK TABLES `ebayStock` WRITE;
/*!40000 ALTER TABLE `ebayStock` DISABLE KEYS */;
/*!40000 ALTER TABLE `ebayStock` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `emailSettings`
--

DROP TABLE IF EXISTS `emailSettings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `emailSettings` (
  `email` varchar(500) DEFAULT NULL,
  `pwd` varchar(500) DEFAULT NULL,
  `smtp` varchar(400) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_company` int(11) DEFAULT NULL,
  `tls` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `emailSettings`
--

LOCK TABLES `emailSettings` WRITE;
/*!40000 ALTER TABLE `emailSettings` DISABLE KEYS */;
/*!40000 ALTER TABLE `emailSettings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `epdq_orders`
--

DROP TABLE IF EXISTS `epdq_orders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `epdq_orders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `oid` int(11) DEFAULT NULL,
  `result` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `epdq_orders`
--

LOCK TABLES `epdq_orders` WRITE;
/*!40000 ALTER TABLE `epdq_orders` DISABLE KEYS */;
/*!40000 ALTER TABLE `epdq_orders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `feedback`
--

DROP TABLE IF EXISTS `feedback`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `feedback` (
  `id` int(6) NOT NULL AUTO_INCREMENT,
  `comment` varchar(800) DEFAULT NULL,
  `oid` int(11) DEFAULT NULL,
  `type` int(1) DEFAULT '0',
  `used` int(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `oid_UNIQUE` (`oid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `feedback`
--

LOCK TABLES `feedback` WRITE;
/*!40000 ALTER TABLE `feedback` DISABLE KEYS */;
/*!40000 ALTER TABLE `feedback` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `flagged`
--

DROP TABLE IF EXISTS `flagged`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `flagged` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `oid` int(11) DEFAULT NULL,
  `reason` varchar(80) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `flagged`
--

LOCK TABLES `flagged` WRITE;
/*!40000 ALTER TABLE `flagged` DISABLE KEYS */;
/*!40000 ALTER TABLE `flagged` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `freeSat`
--

DROP TABLE IF EXISTS `freeSat`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `freeSat` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` varchar(100) DEFAULT NULL,
  `saturday` date DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `freeSat`
--

LOCK TABLES `freeSat` WRITE;
/*!40000 ALTER TABLE `freeSat` DISABLE KEYS */;
/*!40000 ALTER TABLE `freeSat` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `groupon_sku`
--

DROP TABLE IF EXISTS `groupon_sku`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `groupon_sku` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `g_sku` varchar(50) DEFAULT NULL,
  `sku` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `groupon_sku`
--

LOCK TABLES `groupon_sku` WRITE;
/*!40000 ALTER TABLE `groupon_sku` DISABLE KEYS */;
/*!40000 ALTER TABLE `groupon_sku` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `history`
--

DROP TABLE IF EXISTS `history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `history` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `status` varchar(64) DEFAULT NULL,
  `oid` int(11) DEFAULT NULL,
  `timestamp` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `history`
--

LOCK TABLES `history` WRITE;
/*!40000 ALTER TABLE `history` DISABLE KEYS */;
/*!40000 ALTER TABLE `history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `info`
--

DROP TABLE IF EXISTS `info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` text,
  `datum` varchar(255) DEFAULT NULL,
  `group` varchar(48) DEFAULT NULL,
  `type` int(1) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `info`
--

LOCK TABLES `info` WRITE;
/*!40000 ALTER TABLE `info` DISABLE KEYS */;
/*!40000 ALTER TABLE `info` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `invoiced`
--

DROP TABLE IF EXISTS `invoiced`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `invoiced` (
  `oid` int(255) DEFAULT NULL,
  `date` varchar(20) DEFAULT NULL,
  `paid` int(1) DEFAULT '0',
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tot` double DEFAULT '0',
  `paid_stamp` varchar(50) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `invoiced`
--

LOCK TABLES `invoiced` WRITE;
/*!40000 ALTER TABLE `invoiced` DISABLE KEYS */;
/*!40000 ALTER TABLE `invoiced` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `links`
--

DROP TABLE IF EXISTS `links`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `links` (
  `oid` int(255) DEFAULT NULL,
  `code` varchar(400) DEFAULT NULL,
  `sent` int(1) DEFAULT '0',
  `done` int(1) DEFAULT '0',
  `status` varchar(100) DEFAULT 'Pending delivery date',
  `time` varchar(40) DEFAULT NULL,
  `timestamp` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `id_col` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id_col`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `links`
--

LOCK TABLES `links` WRITE;
/*!40000 ALTER TABLE `links` DISABLE KEYS */;
/*!40000 ALTER TABLE `links` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `log`
--

DROP TABLE IF EXISTS `log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `log` (
  `ip` varchar(100) DEFAULT NULL,
  `time` varchar(20) DEFAULT NULL,
  `forward` varchar(200) DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `log`
--

LOCK TABLES `log` WRITE;
/*!40000 ALTER TABLE `log` DISABLE KEYS */;
/*!40000 ALTER TABLE `log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `logins`
--

DROP TABLE IF EXISTS `logins`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `logins` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ip` varchar(45) DEFAULT NULL,
  `proxy` varchar(45) DEFAULT NULL,
  `user` int(11) DEFAULT NULL,
  `local` varchar(45) DEFAULT NULL,
  `time` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `logins`
--

LOCK TABLES `logins` WRITE;
/*!40000 ALTER TABLE `logins` DISABLE KEYS */;
/*!40000 ALTER TABLE `logins` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `logins_w`
--

DROP TABLE IF EXISTS `logins_w`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `logins_w` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ip` varchar(45) DEFAULT NULL,
  `proxy` varchar(45) DEFAULT NULL,
  `user` int(11) DEFAULT NULL,
  `local` varchar(45) DEFAULT NULL,
  `time` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `logins_w`
--

LOCK TABLES `logins_w` WRITE;
/*!40000 ALTER TABLE `logins_w` DISABLE KEYS */;
/*!40000 ALTER TABLE `logins_w` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `m_error_notes`
--

DROP TABLE IF EXISTS `m_error_notes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `m_error_notes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `note` varchar(255) DEFAULT NULL,
  `cost` float(8,2) DEFAULT '0.00',
  `staff_id` int(11) DEFAULT '0',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `oid` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ORDER_INDEX` (`oid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `m_error_notes`
--

LOCK TABLES `m_error_notes` WRITE;
/*!40000 ALTER TABLE `m_error_notes` DISABLE KEYS */;
/*!40000 ALTER TABLE `m_error_notes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `magento`
--

DROP TABLE IF EXISTS `magento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `magento` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `time` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `magento`
--

LOCK TABLES `magento` WRITE;
/*!40000 ALTER TABLE `magento` DISABLE KEYS */;
/*!40000 ALTER TABLE `magento` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `magento_accounts`
--

DROP TABLE IF EXISTS `magento_accounts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `magento_accounts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user` varchar(150) DEFAULT NULL,
  `password` varchar(200) DEFAULT NULL,
  `active` varchar(45) DEFAULT '0',
  `url` varchar(255) DEFAULT NULL,
  `api_version` double DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `magento_accounts`
--

LOCK TABLES `magento_accounts` WRITE;
/*!40000 ALTER TABLE `magento_accounts` DISABLE KEYS */;
/*!40000 ALTER TABLE `magento_accounts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `magento_links`
--

DROP TABLE IF EXISTS `magento_links`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `magento_links` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `code` (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `magento_links`
--

LOCK TABLES `magento_links` WRITE;
/*!40000 ALTER TABLE `magento_links` DISABLE KEYS */;
/*!40000 ALTER TABLE `magento_links` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mail_lists`
--

DROP TABLE IF EXISTS `mail_lists`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mail_lists` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` text,
  `emails` text,
  `sent` timestamp NULL DEFAULT NULL,
  `title` varchar(32) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `email_temaplte` varchar(24) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mail_lists`
--

LOCK TABLES `mail_lists` WRITE;
/*!40000 ALTER TABLE `mail_lists` DISABLE KEYS */;
/*!40000 ALTER TABLE `mail_lists` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mailer`
--

DROP TABLE IF EXISTS `mailer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mailer` (
  `id` int(11) DEFAULT NULL,
  `time` varchar(100) DEFAULT NULL,
  `camp` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mailer`
--

LOCK TABLES `mailer` WRITE;
/*!40000 ALTER TABLE `mailer` DISABLE KEYS */;
/*!40000 ALTER TABLE `mailer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `manufacturing_error`
--

DROP TABLE IF EXISTS `manufacturing_error`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `manufacturing_error` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) DEFAULT NULL,
  `problem` varchar(45) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `item_id` int(11) DEFAULT NULL,
  `cost` float(8,2) DEFAULT '0.00',
  PRIMARY KEY (`id`),
  KEY `Order_Index` (`order_id`),
  KEY `Item_Index` (`item_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `manufacturing_error`
--

LOCK TABLES `manufacturing_error` WRITE;
/*!40000 ALTER TABLE `manufacturing_error` DISABLE KEYS */;
/*!40000 ALTER TABLE `manufacturing_error` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mateDays`
--

DROP TABLE IF EXISTS `mateDays`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mateDays` (
  `name` varchar(200) DEFAULT NULL,
  `day` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mateDays`
--

LOCK TABLES `mateDays` WRITE;
/*!40000 ALTER TABLE `mateDays` DISABLE KEYS */;
/*!40000 ALTER TABLE `mateDays` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `matePayments`
--

DROP TABLE IF EXISTS `matePayments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `matePayments` (
  `dates` varchar(600) DEFAULT NULL,
  `amount` double DEFAULT NULL,
  `mate` varchar(200) DEFAULT NULL,
  `date` varchar(200) DEFAULT NULL,
  `staff` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `matePayments`
--

LOCK TABLES `matePayments` WRITE;
/*!40000 ALTER TABLE `matePayments` DISABLE KEYS */;
/*!40000 ALTER TABLE `matePayments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mates`
--

DROP TABLE IF EXISTS `mates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mates` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `num` varchar(24) DEFAULT NULL,
  `rate` decimal(8,2) DEFAULT '0.00',
  `matePaid` int(1) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `active` int(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mates`
--

LOCK TABLES `mates` WRITE;
/*!40000 ALTER TABLE `mates` DISABLE KEYS */;
/*!40000 ALTER TABLE `mates` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `maxDays`
--

DROP TABLE IF EXISTS `maxDays`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `maxDays` (
  `date` varchar(100) DEFAULT NULL,
  `route` varchar(200) DEFAULT NULL,
  `num` int(10) DEFAULT NULL,
  `max` int(10) DEFAULT '30',
  `weight` double DEFAULT NULL,
  `cbm` double DEFAULT NULL,
  `van` int(1) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `maxDays`
--

LOCK TABLES `maxDays` WRITE;
/*!40000 ALTER TABLE `maxDays` DISABLE KEYS */;
/*!40000 ALTER TABLE `maxDays` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `messages`
--

DROP TABLE IF EXISTS `messages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `messages` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `conversation_id` int(11) DEFAULT NULL,
  `folder` varchar(48) CHARACTER SET utf8 DEFAULT NULL,
  `from_id` int(11) DEFAULT NULL,
  `is_read` tinyint(1) DEFAULT '0',
  `message` text CHARACTER SET utf8,
  `subject` varchar(128) CHARACTER SET utf8 DEFAULT NULL,
  `to_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `new` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `convo_index` (`conversation_id`),
  KEY `sender_index` (`from_id`),
  KEY `to_index` (`to_id`),
  KEY `new_index` (`new`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `messages`
--

LOCK TABLES `messages` WRITE;
/*!40000 ALTER TABLE `messages` DISABLE KEYS */;
/*!40000 ALTER TABLE `messages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_access_tokens`
--

DROP TABLE IF EXISTS `oauth_access_tokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `client_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_access_tokens_user_id_index` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_access_tokens`
--

LOCK TABLES `oauth_access_tokens` WRITE;
/*!40000 ALTER TABLE `oauth_access_tokens` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_access_tokens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_auth_codes`
--

DROP TABLE IF EXISTS `oauth_auth_codes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `client_id` int(11) NOT NULL,
  `scopes` text COLLATE utf8_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_auth_codes`
--

LOCK TABLES `oauth_auth_codes` WRITE;
/*!40000 ALTER TABLE `oauth_auth_codes` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_auth_codes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_clients`
--

DROP TABLE IF EXISTS `oauth_clients`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_clients` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `redirect` text COLLATE utf8_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_clients_user_id_index` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_clients`
--

LOCK TABLES `oauth_clients` WRITE;
/*!40000 ALTER TABLE `oauth_clients` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_clients` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_personal_access_clients`
--

DROP TABLE IF EXISTS `oauth_personal_access_clients`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_personal_access_clients` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `client_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_personal_access_clients_client_id_index` (`client_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_personal_access_clients`
--

LOCK TABLES `oauth_personal_access_clients` WRITE;
/*!40000 ALTER TABLE `oauth_personal_access_clients` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_personal_access_clients` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_refresh_tokens`
--

DROP TABLE IF EXISTS `oauth_refresh_tokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_refresh_tokens`
--

LOCK TABLES `oauth_refresh_tokens` WRITE;
/*!40000 ALTER TABLE `oauth_refresh_tokens` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_refresh_tokens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oos_search`
--

DROP TABLE IF EXISTS `oos_search`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oos_search` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `items` text,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oos_search`
--

LOCK TABLES `oos_search` WRITE;
/*!40000 ALTER TABLE `oos_search` DISABLE KEYS */;
/*!40000 ALTER TABLE `oos_search` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `orderItems`
--

DROP TABLE IF EXISTS `orderItems`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `orderItems` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '20.01 changed to int(11)',
  `id_staff` int(11) DEFAULT NULL,
  `id_item_status` int(11) DEFAULT NULL,
  `itemid` int(11) DEFAULT NULL,
  `oid` int(11) DEFAULT NULL,
  `bedsName` text,
  `blocked` int(1) DEFAULT '0',
  `currStock` int(6) DEFAULT NULL COMMENT 'changed to int(6) ',
  `discount` double DEFAULT '0',
  `itemStatus` varchar(100) DEFAULT NULL,
  `lineTotal` double DEFAULT NULL,
  `notes` text,
  `price` decimal(8,2) DEFAULT NULL,
  `Qty` int(6) DEFAULT NULL,
  `staffName` varchar(100) DEFAULT NULL,
  `costs` double DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `itemid` (`itemid`),
  KEY `index_order_id` (`oid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `orderItems`
--

LOCK TABLES `orderItems` WRITE;
/*!40000 ALTER TABLE `orderItems` DISABLE KEYS */;
/*!40000 ALTER TABLE `orderItems` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `orderItems_b`
--

DROP TABLE IF EXISTS `orderItems_b`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `orderItems_b` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '20.01 changed to int(11)',
  `id_staff` int(11) DEFAULT NULL,
  `id_item_status` int(11) DEFAULT NULL,
  `itemid` int(11) DEFAULT NULL,
  `oid` int(11) DEFAULT NULL,
  `bedsName` text,
  `blocked` int(1) DEFAULT '0',
  `currStock` int(6) DEFAULT NULL COMMENT 'changed to int(6) ',
  `discount` double DEFAULT '0',
  `itemStatus` varchar(100) DEFAULT NULL,
  `lineTotal` double DEFAULT NULL,
  `notes` text,
  `price` decimal(8,2) DEFAULT NULL,
  `Qty` int(6) DEFAULT NULL,
  `staffName` varchar(100) DEFAULT NULL,
  `costs` double DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `orderItems_b`
--

LOCK TABLES `orderItems_b` WRITE;
/*!40000 ALTER TABLE `orderItems_b` DISABLE KEYS */;
/*!40000 ALTER TABLE `orderItems_b` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `orderNotes`
--

DROP TABLE IF EXISTS `orderNotes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `orderNotes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `oid` int(11) DEFAULT NULL,
  `date` varchar(40) DEFAULT NULL,
  `time` varchar(20) DEFAULT NULL,
  `notes` text,
  `staff` varchar(100) DEFAULT NULL,
  `driver` int(1) DEFAULT '0',
  `pic` tinyint(1) DEFAULT '0',
  `id_staff` int(11) DEFAULT NULL,
  `id_driver` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oid` (`oid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `orderNotes`
--

LOCK TABLES `orderNotes` WRITE;
/*!40000 ALTER TABLE `orderNotes` DISABLE KEYS */;
/*!40000 ALTER TABLE `orderNotes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `orderPayments`
--

DROP TABLE IF EXISTS `orderPayments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `orderPayments` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `stamp` varchar(200) DEFAULT NULL,
  `paid` double DEFAULT NULL,
  `cardNo` varchar(50) DEFAULT NULL,
  `exp` varchar(20) DEFAULT NULL,
  `auth` varchar(100) DEFAULT NULL,
  `oid` int(255) DEFAULT NULL,
  `type` varchar(100) DEFAULT NULL,
  `staff` varchar(200) DEFAULT NULL,
  `id_staff` int(11) DEFAULT NULL,
  `id_payment_type` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `orderPayments`
--

LOCK TABLES `orderPayments` WRITE;
/*!40000 ALTER TABLE `orderPayments` DISABLE KEYS */;
/*!40000 ALTER TABLE `orderPayments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order_status`
--

DROP TABLE IF EXISTS `order_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `order_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_status` varchar(32) DEFAULT NULL,
  `link_status` varchar(128) DEFAULT NULL,
  `show_status` int(1) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `idx_order_status_order_status` (`order_status`),
  KEY `idx_order_status_link_status` (`link_status`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order_status`
--

LOCK TABLES `order_status` WRITE;
/*!40000 ALTER TABLE `order_status` DISABLE KEYS */;
/*!40000 ALTER TABLE `order_status` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order_status_history`
--

DROP TABLE IF EXISTS `order_status_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `order_status_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `changed_datetime` datetime DEFAULT CURRENT_TIMESTAMP,
  `from_status` varchar(24) DEFAULT NULL,
  `order_id` int(11) DEFAULT NULL,
  `text` text,
  `to_status` varchar(24) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `order_id_index` (`order_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order_status_history`
--

LOCK TABLES `order_status_history` WRITE;
/*!40000 ALTER TABLE `order_status_history` DISABLE KEYS */;
/*!40000 ALTER TABLE `order_status_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order_type`
--

DROP TABLE IF EXISTS `order_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `order_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_type` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order_type`
--

LOCK TABLES `order_type` WRITE;
/*!40000 ALTER TABLE `order_type` DISABLE KEYS */;
/*!40000 ALTER TABLE `order_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `orders`
--

DROP TABLE IF EXISTS `orders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `orders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_company` int(11) DEFAULT '0',
  `id_status` int(11) DEFAULT '1' COMMENT 'This should be ‘id_order_status’; the values are form the ‘order_status’ table',
  `cid` int(11) DEFAULT NULL,
  `delRoute` varchar(32) DEFAULT NULL,
  `endDate` varchar(32) DEFAULT NULL,
  `endTime` varchar(32) DEFAULT NULL,
  `lastMaintainer` varchar(48) DEFAULT '',
  `lastMaintainID` int(3) DEFAULT NULL,
  `orderStatus` varchar(24) DEFAULT NULL,
  `paid` double DEFAULT '0',
  `startDate` varchar(24) DEFAULT NULL,
  `startTime` varchar(24) DEFAULT NULL,
  `staffName` varchar(48) DEFAULT NULL,
  `staffid` int(6) DEFAULT NULL,
  `total` double DEFAULT '0',
  `dispatchType` varchar(32) DEFAULT 'Delivery',
  `collector` varchar(32) DEFAULT NULL,
  `colDelDate` varchar(24) DEFAULT NULL,
  `carrier` varchar(32) DEFAULT NULL,
  `orderType` varchar(32) DEFAULT NULL,
  `direct` int(1) DEFAULT NULL,
  `paymentType` varchar(32) DEFAULT NULL,
  `delOrder` int(5) DEFAULT NULL,
  `authNo` varchar(32) DEFAULT NULL,
  `cardNo` varchar(4) DEFAULT NULL,
  `exp` varchar(20) DEFAULT NULL,
  `companyName` varchar(200) DEFAULT NULL,
  `ouref` varchar(400) DEFAULT NULL,
  `time_stamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `proforma` int(1) NOT NULL DEFAULT '0',
  `showVat` int(1) DEFAULT '0',
  `showNotes` int(1) DEFAULT '1',
  `serType` varchar(100) DEFAULT 'sale',
  `icType` varchar(100) DEFAULT 'invoice',
  `iNo` varchar(100) DEFAULT NULL,
  `ebayID` varchar(64) DEFAULT NULL,
  `vatAmount` double DEFAULT NULL,
  `zeroVat` int(1) DEFAULT '0',
  `ebayStatus` varchar(100) DEFAULT NULL,
  `deliverBy` varchar(100) NOT NULL DEFAULT '0',
  `tx` int(255) NOT NULL DEFAULT '0',
  `delTime` varchar(20) DEFAULT NULL,
  `delStat` varchar(20) DEFAULT NULL,
  `agent` text,
  `agentPaid` int(1) DEFAULT '0',
  `agentOwed` double DEFAULT '0',
  `rate` double DEFAULT '0',
  `txt` int(1) DEFAULT '0',
  `code` varchar(64) DEFAULT NULL,
  `parcelid` varchar(20) DEFAULT NULL,
  `neighbour` varchar(400) DEFAULT '0',
  `aftersales` int(1) DEFAULT '0',
  `safePlace` varchar(200) DEFAULT '0',
  `done` int(1) DEFAULT '0',
  `ebay` varchar(100) DEFAULT '0',
  `notLoaded` int(1) DEFAULT '0',
  `p` int(1) DEFAULT '0',
  `wes` int(1) DEFAULT '0',
  `cStatus` varchar(100) DEFAULT 'none',
  `colOrder` int(10) DEFAULT '0',
  `colTime` varchar(10) DEFAULT NULL,
  `colRoute` varchar(100) DEFAULT NULL,
  `extra` varchar(10) DEFAULT NULL,
  `beds` varchar(255) DEFAULT '0',
  `delDate` varchar(20) DEFAULT NULL,
  `colID` int(255) DEFAULT '0',
  `delID` int(255) DEFAULT '0',
  `lat` varchar(100) DEFAULT NULL,
  `lng` varchar(100) DEFAULT NULL,
  `amazonID` varchar(100) DEFAULT NULL,
  `amzShipped` int(1) DEFAULT '0',
  `refund` int(1) DEFAULT '0',
  `postLoc` varchar(50) DEFAULT '0',
  `post` int(1) DEFAULT '0',
  `sender` varchar(200) DEFAULT '0',
  `stamp` varchar(100) DEFAULT NULL,
  `startStamp` varchar(100) DEFAULT '0',
  `wholeError` int(1) DEFAULT '0',
  `mfError` int(1) DEFAULT '0',
  `feedbackScore` int(1) DEFAULT '0',
  `delissue` int(1) DEFAULT '0',
  `defects` int(1) DEFAULT '0',
  `g_id` varchar(100) DEFAULT '0',
  `g_num` varchar(100) DEFAULT '0',
  `g_desc` varchar(128) DEFAULT '0',
  `g_ship` varchar(24) DEFAULT '0',
  `oos` int(1) DEFAULT '0',
  `mgmt` int(1) DEFAULT '0',
  `processed` int(1) DEFAULT '0',
  `paidBeds` int(1) DEFAULT '0',
  `pTime` varchar(16) DEFAULT '0',
  `refundBox` int(1) DEFAULT '0',
  `id_delivery_route` int(11) DEFAULT '0',
  `id_order_type` int(11) DEFAULT '0',
  `id_payment_type` int(11) DEFAULT '0',
  `hold` int(1) DEFAULT '0',
  `missed` int(1) DEFAULT '0',
  `urgent` int(1) DEFAULT '0',
  `retention` int(1) DEFAULT '0',
  `paypal` int(1) DEFAULT '0',
  `reopen` int(1) DEFAULT '0',
  `id_supplier` int(11) DEFAULT '0',
  `paid_supplier` int(1) DEFAULT '0',
  `invoice_made` int(1) DEFAULT '0',
  `id_seller` int(11) DEFAULT '0',
  `seller_paid` int(1) DEFAULT '0',
  `discount_amount` decimal(9,2) DEFAULT '0.00',
  `discount_rate` decimal(6,2) DEFAULT '0.00' COMMENT 'expressed as a percentage',
  `vat_paid` decimal(6,2) DEFAULT '0.00' COMMENT 'expressed as a percentage',
  `paypal_payment` text,
  `last_trans_id` varchar(50) DEFAULT '0',
  `to_collect` int(1) DEFAULT '0',
  `is_split` int(1) DEFAULT '0',
  `voucher_code` varchar(64) DEFAULT NULL,
  `is_shipped` int(1) DEFAULT '0',
  `paid_courier` int(1) DEFAULT '0',
  `tnt_consignment` varchar(48) DEFAULT NULL,
  `ajfoams_tracking_number` varchar(48) DEFAULT NULL,
  `bedtatstic_tracking_number` varchar(48) DEFAULT NULL,
  `is_collected` int(1) DEFAULT '0',
  `escalate` int(1) DEFAULT '0',
  `ordered` int(1) DEFAULT '0',
  `merch_ref` varchar(50) DEFAULT NULL,
  `trust_pilot` int(1) DEFAULT '0',
  `stolen` int(11) DEFAULT '0',
  `tuffnells` tinyint(1) DEFAULT '0',
  `order_number_part_0` char(1) DEFAULT NULL COMMENT 'R',
  `order_number_part_1` char(4) DEFAULT NULL COMMENT 'REQUEST_SOURCE_BEDS = ''B'';\nREQUEST_SOURCE_EBAY = ''E'';\nREQUEST_SOURCE_OTHER = ''T'';\nREQUEST_SOURCE_STAFF = ''S'';',
  `order_number_part_2` char(13) DEFAULT NULL COMMENT 'uniqid()',
  `order_number_part_3` char(3) DEFAULT NULL COMMENT 'mt_rand(1, 999)',
  `order_number_part_4` char(4) DEFAULT NULL COMMENT 'EXTENSION_BASE = ''P'';\nEXTENSION_EXCHANGE = ''E'';\nEXTENSION_SPLIT_ORDER = ''P'';',
  `sales_record` varchar(10) DEFAULT '0',
  `priority` tinyint(1) DEFAULT '0',
  `manual_order` tinyint(1) DEFAULT '0',
  `delivery_issue` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `cid` (`cid`),
  KEY `index_delRoute` (`delRoute`),
  KEY `index_parcelid` (`parcelid`),
  KEY `index_startStamp` (`startStamp`),
  KEY `index_delivery_route` (`id_delivery_route`),
  KEY `inex_code` (`code`),
  KEY `index_ebay_id` (`ebayID`),
  KEY `index_supplier` (`id_supplier`),
  KEY `index_seller` (`seller_paid`),
  KEY `index_id_status` (`id_status`),
  KEY `index_id_route` (`id_delivery_route`),
  KEY `index_order_number` (`order_number_part_0`,`order_number_part_1`,`order_number_part_2`,`order_number_part_3`,`order_number_part_4`),
  KEY `index_beds` (`beds`),
  KEY `index_manual` (`manual_order`),
  KEY `index_id_seller` (`id_seller`),
  KEY `index_invoice_made` (`invoice_made`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `orders`
--

LOCK TABLES `orders` WRITE;
/*!40000 ALTER TABLE `orders` DISABLE KEYS */;
/*!40000 ALTER TABLE `orders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `orders_b`
--

DROP TABLE IF EXISTS `orders_b`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `orders_b` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_company` int(11) DEFAULT '0',
  `id_status` int(11) DEFAULT '1' COMMENT 'This should be ‘id_order_status’; the values are form the ‘order_status’ table',
  `cid` int(11) DEFAULT NULL,
  `delRoute` varchar(32) DEFAULT NULL,
  `endDate` varchar(32) DEFAULT NULL,
  `endTime` varchar(32) DEFAULT NULL,
  `lastMaintainer` varchar(48) DEFAULT '',
  `lastMaintainID` int(3) DEFAULT NULL,
  `orderStatus` varchar(24) DEFAULT NULL,
  `paid` double DEFAULT '0',
  `startDate` varchar(24) DEFAULT NULL,
  `startTime` varchar(24) DEFAULT NULL,
  `staffName` varchar(48) DEFAULT NULL,
  `staffid` int(6) DEFAULT NULL,
  `total` double DEFAULT '0',
  `dispatchType` varchar(32) DEFAULT 'Delivery',
  `collector` varchar(32) DEFAULT NULL,
  `colDelDate` varchar(24) DEFAULT NULL,
  `carrier` varchar(32) DEFAULT NULL,
  `orderType` varchar(32) DEFAULT NULL,
  `direct` int(1) DEFAULT NULL,
  `paymentType` varchar(32) DEFAULT NULL,
  `delOrder` int(5) DEFAULT NULL,
  `authNo` varchar(32) DEFAULT NULL,
  `cardNo` varchar(4) DEFAULT NULL,
  `exp` varchar(20) DEFAULT NULL,
  `companyName` varchar(200) DEFAULT NULL,
  `ouref` varchar(400) DEFAULT NULL,
  `time_stamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `proforma` int(1) NOT NULL DEFAULT '0',
  `showVat` int(1) DEFAULT '0',
  `showNotes` int(1) DEFAULT '1',
  `serType` varchar(100) DEFAULT 'sale',
  `icType` varchar(100) DEFAULT 'invoice',
  `iNo` varchar(100) DEFAULT NULL,
  `ebayID` varchar(64) DEFAULT NULL,
  `vatAmount` double DEFAULT NULL,
  `zeroVat` int(1) DEFAULT '0',
  `ebayStatus` varchar(100) DEFAULT NULL,
  `deliverBy` varchar(100) NOT NULL DEFAULT '0',
  `tx` int(255) NOT NULL DEFAULT '0',
  `delTime` varchar(20) DEFAULT NULL,
  `delStat` varchar(20) DEFAULT NULL,
  `agent` text,
  `agentPaid` int(1) DEFAULT '0',
  `agentOwed` double DEFAULT '0',
  `rate` double DEFAULT '0',
  `txt` int(1) DEFAULT '0',
  `code` varchar(64) DEFAULT NULL,
  `parcelid` varchar(20) DEFAULT NULL,
  `neighbour` varchar(400) DEFAULT '0',
  `aftersales` int(1) DEFAULT '0',
  `safePlace` varchar(200) DEFAULT '0',
  `done` int(1) DEFAULT '0',
  `ebay` varchar(100) DEFAULT '0',
  `notLoaded` int(1) DEFAULT '0',
  `p` int(1) DEFAULT '0',
  `wes` int(1) DEFAULT '0',
  `cStatus` varchar(100) DEFAULT 'none',
  `colOrder` int(10) DEFAULT '0',
  `colTime` varchar(10) DEFAULT NULL,
  `colRoute` varchar(100) DEFAULT NULL,
  `extra` varchar(10) DEFAULT NULL,
  `beds` varchar(255) DEFAULT '0',
  `delDate` varchar(20) DEFAULT NULL,
  `colID` int(255) DEFAULT '0',
  `delID` int(255) DEFAULT '0',
  `lat` varchar(100) DEFAULT NULL,
  `lng` varchar(100) DEFAULT NULL,
  `amazonID` varchar(100) DEFAULT NULL,
  `amzShipped` int(1) DEFAULT '0',
  `refund` int(1) DEFAULT '0',
  `postLoc` varchar(50) DEFAULT '0',
  `post` int(1) DEFAULT '0',
  `sender` varchar(200) DEFAULT '0',
  `stamp` varchar(100) DEFAULT NULL,
  `startStamp` varchar(100) DEFAULT '0',
  `wholeError` int(1) DEFAULT '0',
  `mfError` int(1) DEFAULT '0',
  `feedbackScore` int(1) DEFAULT '0',
  `delissue` int(1) DEFAULT '0',
  `defects` int(1) DEFAULT '0',
  `g_id` varchar(100) DEFAULT '0',
  `g_num` varchar(100) DEFAULT '0',
  `g_desc` varchar(128) DEFAULT '0',
  `g_ship` varchar(24) DEFAULT '0',
  `oos` int(1) DEFAULT '0',
  `mgmt` int(1) DEFAULT '0',
  `processed` int(1) DEFAULT '0',
  `paidBeds` int(1) DEFAULT '0',
  `pTime` varchar(16) DEFAULT '0',
  `refundBox` int(1) DEFAULT '0',
  `id_delivery_route` int(11) DEFAULT '0',
  `id_order_type` int(11) DEFAULT '0',
  `id_payment_type` int(11) DEFAULT '0',
  `hold` int(1) DEFAULT '0',
  `missed` int(1) DEFAULT '0',
  `urgent` int(1) DEFAULT '0',
  `retention` int(1) DEFAULT '0',
  `paypal` int(1) DEFAULT '0',
  `reopen` int(1) DEFAULT '0',
  `id_supplier` int(11) DEFAULT '0',
  `paid_supplier` int(1) DEFAULT '0',
  `invoice_made` int(1) DEFAULT '0',
  `id_seller` int(11) DEFAULT '0',
  `seller_paid` int(1) DEFAULT '0',
  `discount_amount` decimal(9,2) DEFAULT '0.00',
  `discount_rate` decimal(6,2) DEFAULT '0.00' COMMENT 'expressed as a percentage',
  `vat_paid` decimal(6,2) DEFAULT '0.00' COMMENT 'expressed as a percentage',
  `paypal_payment` text,
  `last_trans_id` varchar(50) DEFAULT '0',
  `to_collect` int(1) DEFAULT '0',
  `is_split` int(1) DEFAULT '0',
  `voucher_code` varchar(64) DEFAULT NULL,
  `is_shipped` int(1) DEFAULT '0',
  `paid_courier` int(1) DEFAULT '0',
  `tnt_consignment` varchar(48) DEFAULT NULL,
  `ajfoams_tracking_number` varchar(48) DEFAULT NULL,
  `bedtatstic_tracking_number` varchar(48) DEFAULT NULL,
  `is_collected` int(1) DEFAULT '0',
  `escalate` int(1) DEFAULT '0',
  `ordered` int(1) DEFAULT '0',
  `merch_ref` varchar(50) DEFAULT NULL,
  `trust_pilot` int(1) DEFAULT '0',
  `stolen` int(11) DEFAULT '0',
  `tuffnells` tinyint(1) DEFAULT '0',
  `order_number_part_0` char(1) DEFAULT NULL COMMENT 'R',
  `order_number_part_1` char(4) DEFAULT NULL COMMENT 'REQUEST_SOURCE_BEDS = ''B'';\nREQUEST_SOURCE_EBAY = ''E'';\nREQUEST_SOURCE_OTHER = ''T'';\nREQUEST_SOURCE_STAFF = ''S'';',
  `order_number_part_2` char(13) DEFAULT NULL COMMENT 'uniqid()',
  `order_number_part_3` char(3) DEFAULT NULL COMMENT 'mt_rand(1, 999)',
  `order_number_part_4` char(4) DEFAULT NULL COMMENT 'EXTENSION_BASE = ''P'';\nEXTENSION_EXCHANGE = ''E'';\nEXTENSION_SPLIT_ORDER = ''P'';',
  `sales_record` varchar(10) DEFAULT '0',
  `priority` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `orders_b`
--

LOCK TABLES `orders_b` WRITE;
/*!40000 ALTER TABLE `orders_b` DISABLE KEYS */;
/*!40000 ALTER TABLE `orders_b` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `orders_log`
--

DROP TABLE IF EXISTS `orders_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `orders_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ip` varchar(45) DEFAULT NULL,
  `proxy` varchar(45) DEFAULT NULL,
  `oid` int(11) DEFAULT NULL,
  `local` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `orders_log`
--

LOCK TABLES `orders_log` WRITE;
/*!40000 ALTER TABLE `orders_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `orders_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `out_of_stock`
--

DROP TABLE IF EXISTS `out_of_stock`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `out_of_stock` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `item_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `INDEX_ITEM` (`item_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `out_of_stock`
--

LOCK TABLES `out_of_stock` WRITE;
/*!40000 ALTER TABLE `out_of_stock` DISABLE KEYS */;
/*!40000 ALTER TABLE `out_of_stock` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `packs`
--

DROP TABLE IF EXISTS `packs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `packs` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `l` varchar(50) DEFAULT NULL,
  `h` varchar(50) DEFAULT NULL,
  `w` varchar(50) DEFAULT NULL,
  `we` varchar(50) DEFAULT NULL,
  `oid` int(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `packs`
--

LOCK TABLES `packs` WRITE;
/*!40000 ALTER TABLE `packs` DISABLE KEYS */;
/*!40000 ALTER TABLE `packs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `page_visits`
--

DROP TABLE IF EXISTS `page_visits`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `page_visits` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `oid` int(11) DEFAULT NULL,
  `ip` varchar(45) DEFAULT NULL,
  `time` datetime DEFAULT CURRENT_TIMESTAMP,
  `user` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `OID_KEY` (`oid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `page_visits`
--

LOCK TABLES `page_visits` WRITE;
/*!40000 ALTER TABLE `page_visits` DISABLE KEYS */;
/*!40000 ALTER TABLE `page_visits` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  KEY `password_resets_email_index` (`email`),
  KEY `password_resets_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `passwords`
--

DROP TABLE IF EXISTS `passwords`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `passwords` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(150) DEFAULT NULL,
  `pwd` varchar(150) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `passwords`
--

LOCK TABLES `passwords` WRITE;
/*!40000 ALTER TABLE `passwords` DISABLE KEYS */;
/*!40000 ALTER TABLE `passwords` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `payment_type`
--

DROP TABLE IF EXISTS `payment_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `payment_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `admin` int(1) DEFAULT '0',
  `description` text,
  `payment_type` varchar(32) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `payment_type`
--

LOCK TABLES `payment_type` WRITE;
/*!40000 ALTER TABLE `payment_type` DISABLE KEYS */;
/*!40000 ALTER TABLE `payment_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pda_data`
--

DROP TABLE IF EXISTS `pda_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pda_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `driver_pda_version` float(8,2) DEFAULT NULL,
  `warehouse_pda_version` float(8,2) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pda_data`
--

LOCK TABLES `pda_data` WRITE;
/*!40000 ALTER TABLE `pda_data` DISABLE KEYS */;
/*!40000 ALTER TABLE `pda_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permission_role`
--

DROP TABLE IF EXISTS `permission_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permission_role` (
  `permission_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`permission_id`,`role_id`),
  KEY `permission_role_role_id_foreign` (`role_id`),
  CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permission_role`
--

LOCK TABLES `permission_role` WRITE;
/*!40000 ALTER TABLE `permission_role` DISABLE KEYS */;
/*!40000 ALTER TABLE `permission_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permissions`
--

DROP TABLE IF EXISTS `permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permissions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `permissions_name_unique` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permissions`
--

LOCK TABLES `permissions` WRITE;
/*!40000 ALTER TABLE `permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pickers`
--

DROP TABLE IF EXISTS `pickers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pickers` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `pwd` varchar(200) DEFAULT NULL,
  `uName` varchar(200) DEFAULT NULL,
  `num` varchar(45) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `active` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uName` (`uName`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pickers`
--

LOCK TABLES `pickers` WRITE;
/*!40000 ALTER TABLE `pickers` DISABLE KEYS */;
/*!40000 ALTER TABLE `pickers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `postcode_areas`
--

DROP TABLE IF EXISTS `postcode_areas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `postcode_areas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(12) DEFAULT NULL,
  `area` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `postcode_areas`
--

LOCK TABLES `postcode_areas` WRITE;
/*!40000 ALTER TABLE `postcode_areas` DISABLE KEYS */;
/*!40000 ALTER TABLE `postcode_areas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `postcode_delivery_exceptions`
--

DROP TABLE IF EXISTS `postcode_delivery_exceptions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `postcode_delivery_exceptions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `postcode` varchar(12) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `postcode_delivery_exceptions`
--

LOCK TABLES `postcode_delivery_exceptions` WRITE;
/*!40000 ALTER TABLE `postcode_delivery_exceptions` DISABLE KEYS */;
/*!40000 ALTER TABLE `postcode_delivery_exceptions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `processes`
--

DROP TABLE IF EXISTS `processes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `processes` (
  `pid` int(255) DEFAULT NULL,
  `name` varchar(400) DEFAULT NULL,
  `link` varchar(400) DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `processes`
--

LOCK TABLES `processes` WRITE;
/*!40000 ALTER TABLE `processes` DISABLE KEYS */;
/*!40000 ALTER TABLE `processes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `proof`
--

DROP TABLE IF EXISTS `proof`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `proof` (
  `oid` int(255) DEFAULT NULL,
  `date` varchar(20) DEFAULT NULL,
  `time` varchar(20) DEFAULT NULL,
  `link` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `proof`
--

LOCK TABLES `proof` WRITE;
/*!40000 ALTER TABLE `proof` DISABLE KEYS */;
/*!40000 ALTER TABLE `proof` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `queries`
--

DROP TABLE IF EXISTS `queries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `queries` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `query` text,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `queries`
--

LOCK TABLES `queries` WRITE;
/*!40000 ALTER TABLE `queries` DISABLE KEYS */;
/*!40000 ALTER TABLE `queries` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `refunds`
--

DROP TABLE IF EXISTS `refunds`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `refunds` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` varchar(100) DEFAULT NULL,
  `total` double DEFAULT '0',
  `staff_id` varchar(45) DEFAULT NULL,
  `paymentType` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `refunds`
--

LOCK TABLES `refunds` WRITE;
/*!40000 ALTER TABLE `refunds` DISABLE KEYS */;
/*!40000 ALTER TABLE `refunds` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reminders`
--

DROP TABLE IF EXISTS `reminders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reminders` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `note` varchar(1000) DEFAULT NULL,
  `date` varchar(20) DEFAULT NULL,
  `time` varchar(20) DEFAULT NULL,
  `done` int(1) DEFAULT '0',
  `setBy` varchar(100) DEFAULT NULL,
  `setFor` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reminders`
--

LOCK TABLES `reminders` WRITE;
/*!40000 ALTER TABLE `reminders` DISABLE KEYS */;
/*!40000 ALTER TABLE `reminders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `report_data_items`
--

DROP TABLE IF EXISTS `report_data_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `report_data_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company` varchar(48) DEFAULT NULL,
  `count` int(4) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `item_code` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `report_data_items`
--

LOCK TABLES `report_data_items` WRITE;
/*!40000 ALTER TABLE `report_data_items` DISABLE KEYS */;
/*!40000 ALTER TABLE `report_data_items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `report_data_orders`
--

DROP TABLE IF EXISTS `report_data_orders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `report_data_orders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company` varchar(48) DEFAULT NULL,
  `count` int(6) DEFAULT NULL,
  `orders_date` date DEFAULT NULL,
  `orders_type` varchar(48) DEFAULT NULL,
  `paid` decimal(14,2) DEFAULT NULL,
  `vat` decimal(14,2) DEFAULT NULL,
  `total` decimal(14,2) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_company` (`company`),
  KEY `index_date` (`orders_date`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `report_data_orders`
--

LOCK TABLES `report_data_orders` WRITE;
/*!40000 ALTER TABLE `report_data_orders` DISABLE KEYS */;
/*!40000 ALTER TABLE `report_data_orders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role_user`
--

DROP TABLE IF EXISTS `role_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role_user` (
  `user_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`user_id`,`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role_user`
--

LOCK TABLES `role_user` WRITE;
/*!40000 ALTER TABLE `role_user` DISABLE KEYS */;
/*!40000 ALTER TABLE `role_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `description` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `display_name` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `level` int(3) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `roles_name_unique` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `routeDays`
--

DROP TABLE IF EXISTS `routeDays`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `routeDays` (
  `route` varchar(48) DEFAULT NULL,
  `day` varchar(24) DEFAULT NULL,
  `max` int(5) DEFAULT '30',
  `route_id` int(11) DEFAULT NULL,
  `routeDaysID` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`routeDaysID`),
  KEY `index_route_id` (`route_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `routeDays`
--

LOCK TABLES `routeDays` WRITE;
/*!40000 ALTER TABLE `routeDays` DISABLE KEYS */;
/*!40000 ALTER TABLE `routeDays` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `routeName`
--

DROP TABLE IF EXISTS `routeName`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `routeName` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(48) DEFAULT NULL,
  `max` int(5) DEFAULT '30',
  `van` int(4) DEFAULT '1',
  `msg` int(1) DEFAULT '0',
  `is_wholesale` tinyint(1) DEFAULT '0',
  `in_load_count` tinyint(1) DEFAULT '1',
  `is_route_route` tinyint(1) DEFAULT '0',
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `routeName`
--

LOCK TABLES `routeName` WRITE;
/*!40000 ALTER TABLE `routeName` DISABLE KEYS */;
/*!40000 ALTER TABLE `routeName` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `routeStops`
--

DROP TABLE IF EXISTS `routeStops`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `routeStops` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `routeID` int(6) DEFAULT NULL,
  `postCode` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `routeID` (`routeID`)
) ENGINE=MyISAM AUTO_INCREMENT=328 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `routeStops`
--

LOCK TABLES `routeStops` WRITE;
/*!40000 ALTER TABLE `routeStops` DISABLE KEYS */;
/*!40000 ALTER TABLE `routeStops` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sellers`
--

DROP TABLE IF EXISTS `sellers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sellers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account_limit` int(11) DEFAULT '0',
  `discount` double DEFAULT '0',
  `email` varchar(255) DEFAULT NULL,
  `iName` varchar(64) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `num` varchar(100) DEFAULT NULL,
  `pc` varchar(32) DEFAULT NULL,
  `pwd` varchar(128) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `salt` varchar(128) DEFAULT NULL,
  `street` varchar(128) DEFAULT NULL,
  `tel` varchar(40) DEFAULT NULL,
  `town` varchar(64) DEFAULT NULL,
  `custom_price` int(11) DEFAULT '0',
  `force_pay` int(1) DEFAULT '1',
  `remember_token` varchar(100) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `active_seller` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sellers`
--

LOCK TABLES `sellers` WRITE;
/*!40000 ALTER TABLE `sellers` DISABLE KEYS */;
/*!40000 ALTER TABLE `sellers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sender`
--

DROP TABLE IF EXISTS `sender`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sender` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sender`
--

LOCK TABLES `sender` WRITE;
/*!40000 ALTER TABLE `sender` DISABLE KEYS */;
/*!40000 ALTER TABLE `sender` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sentmail`
--

DROP TABLE IF EXISTS `sentmail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sentmail` (
  `id` int(11) DEFAULT NULL,
  `camp` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sentmail`
--

LOCK TABLES `sentmail` WRITE;
/*!40000 ALTER TABLE `sentmail` DISABLE KEYS */;
/*!40000 ALTER TABLE `sentmail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `settings`
--

DROP TABLE IF EXISTS `settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vat` double DEFAULT NULL,
  `showNotes` int(1) DEFAULT NULL,
  `cod` int(1) DEFAULT '1',
  `wholesale` int(1) DEFAULT '1',
  `companies` varchar(10000) DEFAULT NULL,
  `payments` varchar(10000) DEFAULT NULL,
  `bedsEmail` varchar(10000) DEFAULT NULL,
  `bedsEmailSend` int(1) DEFAULT '0',
  `maps_key` varchar(300) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `settings`
--

LOCK TABLES `settings` WRITE;
/*!40000 ALTER TABLE `settings` DISABLE KEYS */;
/*!40000 ALTER TABLE `settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `skus`
--

DROP TABLE IF EXISTS `skus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `skus` (
  `sku` varchar(200) DEFAULT NULL,
  `original` varchar(200) DEFAULT NULL,
  KEY `index_oringinal_sku` (`sku`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `skus`
--

LOCK TABLES `skus` WRITE;
/*!40000 ALTER TABLE `skus` DISABLE KEYS */;
/*!40000 ALTER TABLE `skus` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `split_table_categories`
--

DROP TABLE IF EXISTS `split_table_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `split_table_categories` (
  `id` int(11) NOT NULL,
  `categorty_id` int(11) DEFAULT NULL,
  `category` varchar(48) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `split_table_categories`
--

LOCK TABLES `split_table_categories` WRITE;
/*!40000 ALTER TABLE `split_table_categories` DISABLE KEYS */;
/*!40000 ALTER TABLE `split_table_categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `staff`
--

DROP TABLE IF EXISTS `staff`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `staff` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fName` varchar(100) DEFAULT NULL,
  `sName` varchar(100) DEFAULT NULL,
  `uName` varchar(255) DEFAULT NULL,
  `pwd` varchar(255) DEFAULT NULL,
  `level` tinyint(1) DEFAULT '0',
  `id_staff_status` int(11) DEFAULT NULL,
  `id_staff_type` int(11) DEFAULT NULL,
  `avatar` varchar(48) DEFAULT NULL,
  `salt` varchar(128) DEFAULT NULL,
  `last_change` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `staff`
--

LOCK TABLES `staff` WRITE;
/*!40000 ALTER TABLE `staff` DISABLE KEYS */;
/*!40000 ALTER TABLE `staff` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `staff_read_bulletin`
--

DROP TABLE IF EXISTS `staff_read_bulletin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `staff_read_bulletin` (
  `staff_id` int(11) DEFAULT NULL,
  `bulletin_id` int(11) DEFAULT NULL COMMENT 'The bulletin records read by a member of staff.'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `staff_read_bulletin`
--

LOCK TABLES `staff_read_bulletin` WRITE;
/*!40000 ALTER TABLE `staff_read_bulletin` DISABLE KEYS */;
/*!40000 ALTER TABLE `staff_read_bulletin` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stf_group`
--

DROP TABLE IF EXISTS `stf_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stf_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_name` varchar(32) DEFAULT NULL,
  `description` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stf_group`
--

LOCK TABLES `stf_group` WRITE;
/*!40000 ALTER TABLE `stf_group` DISABLE KEYS */;
/*!40000 ALTER TABLE `stf_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stf_group_resource_perms`
--

DROP TABLE IF EXISTS `stf_group_resource_perms`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stf_group_resource_perms` (
  `id_group` int(11) DEFAULT NULL,
  `permissions` varchar(4) DEFAULT NULL,
  `resource` varchar(48) DEFAULT NULL,
  `id_resource` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stf_group_resource_perms`
--

LOCK TABLES `stf_group_resource_perms` WRITE;
/*!40000 ALTER TABLE `stf_group_resource_perms` DISABLE KEYS */;
/*!40000 ALTER TABLE `stf_group_resource_perms` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stf_staff_group`
--

DROP TABLE IF EXISTS `stf_staff_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stf_staff_group` (
  `id_staff` int(11) DEFAULT NULL,
  `id_group` int(11) NOT NULL,
  KEY `index1` (`id_staff`,`id_group`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stf_staff_group`
--

LOCK TABLES `stf_staff_group` WRITE;
/*!40000 ALTER TABLE `stf_staff_group` DISABLE KEYS */;
/*!40000 ALTER TABLE `stf_staff_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stf_staff_status`
--

DROP TABLE IF EXISTS `stf_staff_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stf_staff_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `staff_status` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stf_staff_status`
--

LOCK TABLES `stf_staff_status` WRITE;
/*!40000 ALTER TABLE `stf_staff_status` DISABLE KEYS */;
/*!40000 ALTER TABLE `stf_staff_status` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stf_staff_type`
--

DROP TABLE IF EXISTS `stf_staff_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stf_staff_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `staff_type` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stf_staff_type`
--

LOCK TABLES `stf_staff_type` WRITE;
/*!40000 ALTER TABLE `stf_staff_type` DISABLE KEYS */;
/*!40000 ALTER TABLE `stf_staff_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stockDetail`
--

DROP TABLE IF EXISTS `stockDetail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stockDetail` (
  `sid` int(255) DEFAULT NULL,
  `weight` double DEFAULT NULL,
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `cbm` double DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stockDetail`
--

LOCK TABLES `stockDetail` WRITE;
/*!40000 ALTER TABLE `stockDetail` DISABLE KEYS */;
/*!40000 ALTER TABLE `stockDetail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stock_categories`
--

DROP TABLE IF EXISTS `stock_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stock_categories` (
  `id` int(1) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) DEFAULT NULL,
  `description` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stock_categories`
--

LOCK TABLES `stock_categories` WRITE;
/*!40000 ALTER TABLE `stock_categories` DISABLE KEYS */;
/*!40000 ALTER TABLE `stock_categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stock_items`
--

DROP TABLE IF EXISTS `stock_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stock_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '20.01 changed to int(11)',
  `cost` double DEFAULT '0',
  `itemCode` varchar(255) DEFAULT NULL,
  `itemName` varchar(100) DEFAULT NULL,
  `itemDescription` text COMMENT '20.01 changed to text',
  `itemQty` int(5) DEFAULT NULL,
  `itemAlloc` int(5) DEFAULT NULL,
  `itemOnOrder` int(5) DEFAULT NULL,
  `needs_attention` tinyint(1) DEFAULT '1',
  `retail` double DEFAULT '0',
  `wholesale` double DEFAULT '0',
  `weight` decimal(11,3) DEFAULT '0.000',
  `dimensions` varchar(100) DEFAULT '0',
  `bin` varchar(100) DEFAULT NULL,
  `actual` int(11) DEFAULT '0' COMMENT '20.01 changed to int(11)',
  `pieces` int(4) DEFAULT '1',
  `isMulti` varchar(24) DEFAULT '1' COMMENT 'the number of boxes for the item',
  `warehouse` int(1) DEFAULT NULL,
  `label` int(1) DEFAULT '1',
  `blocked` int(1) DEFAULT '1',
  `seller` int(1) DEFAULT '0',
  `cat` varchar(100) DEFAULT '',
  `qty_in_stock` int(11) DEFAULT '0',
  `qty_on_so` int(11) DEFAULT '0',
  `qty_ordered` int(11) DEFAULT '0',
  `category_id` int(11) DEFAULT '0',
  `beds_price` double DEFAULT '0',
  `ebay_price` double DEFAULT '0',
  `route_id` int(11) DEFAULT '0',
  `colour` varchar(24) DEFAULT NULL,
  `gross_weight` decimal(6,2) DEFAULT NULL,
  `manifest_description` text,
  `model_no` varchar(24) DEFAULT NULL,
  `net_weight` decimal(6,2) DEFAULT NULL,
  `product_size` varchar(196) DEFAULT NULL,
  `stock_category_id` int(11) DEFAULT NULL,
  `withdrawn` tinyint(1) DEFAULT '0',
  `always_in_stock` tinyint(1) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_itemCode` (`itemCode`),
  KEY `index_model_no` (`model_no`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stock_items`
--

LOCK TABLES `stock_items` WRITE;
/*!40000 ALTER TABLE `stock_items` DISABLE KEYS */;
/*!40000 ALTER TABLE `stock_items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stock_parts`
--

DROP TABLE IF EXISTS `stock_parts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stock_parts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) DEFAULT NULL,
  `part` varchar(255) DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `PARENT` (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stock_parts`
--

LOCK TABLES `stock_parts` WRITE;
/*!40000 ALTER TABLE `stock_parts` DISABLE KEYS */;
/*!40000 ALTER TABLE `stock_parts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `supplier`
--

DROP TABLE IF EXISTS `supplier`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `supplier` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `supplier_name` varchar(255) DEFAULT NULL,
  `days` int(11) DEFAULT '0',
  `print_orders_on_dispatch` tinyint(1) DEFAULT '0',
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `del_route_id` int(11) DEFAULT '0',
  `route_max` int(11) DEFAULT '0',
  `name` varchar(100) DEFAULT NULL,
  `email` varchar(200) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `remember_token` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `supplier`
--

LOCK TABLES `supplier` WRITE;
/*!40000 ALTER TABLE `supplier` DISABLE KEYS */;
/*!40000 ALTER TABLE `supplier` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `supplier_days`
--

DROP TABLE IF EXISTS `supplier_days`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `supplier_days` (
  `route` varchar(200) DEFAULT NULL,
  `day` varchar(100) DEFAULT NULL,
  `max` int(10) DEFAULT '30',
  `supplier_id` int(11) DEFAULT NULL,
  `route_id` int(11) DEFAULT NULL,
  KEY `index_supplier` (`supplier_id`),
  KEY `route_index` (`route_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `supplier_days`
--

LOCK TABLES `supplier_days` WRITE;
/*!40000 ALTER TABLE `supplier_days` DISABLE KEYS */;
/*!40000 ALTER TABLE `supplier_days` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `test`
--

DROP TABLE IF EXISTS `test`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `test` (
  `id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `test`
--

LOCK TABLES `test` WRITE;
/*!40000 ALTER TABLE `test` DISABLE KEYS */;
/*!40000 ALTER TABLE `test` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `toCol`
--

DROP TABLE IF EXISTS `toCol`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `toCol` (
  `oid` int(255) DEFAULT NULL,
  `tel` int(1) DEFAULT '1',
  `mail` int(1) DEFAULT '1',
  `date` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `toCol`
--

LOCK TABLES `toCol` WRITE;
/*!40000 ALTER TABLE `toCol` DISABLE KEYS */;
/*!40000 ALTER TABLE `toCol` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `toShip`
--

DROP TABLE IF EXISTS `toShip`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `toShip` (
  `oid` int(255) DEFAULT NULL,
  `tel` int(1) DEFAULT '1',
  `mail` int(1) DEFAULT '1',
  `date` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `toShip`
--

LOCK TABLES `toShip` WRITE;
/*!40000 ALTER TABLE `toShip` DISABLE KEYS */;
/*!40000 ALTER TABLE `toShip` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tolls`
--

DROP TABLE IF EXISTS `tolls`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tolls` (
  `date` varchar(20) DEFAULT NULL,
  `time` varchar(20) DEFAULT NULL,
  `driver` varchar(100) DEFAULT NULL,
  `reg` varchar(30) DEFAULT NULL,
  `type` varchar(100) DEFAULT NULL,
  `paid` int(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tolls`
--

LOCK TABLES `tolls` WRITE;
/*!40000 ALTER TABLE `tolls` DISABLE KEYS */;
/*!40000 ALTER TABLE `tolls` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tried`
--

DROP TABLE IF EXISTS `tried`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tried` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `oid` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tried`
--

LOCK TABLES `tried` WRITE;
/*!40000 ALTER TABLE `tried` DISABLE KEYS */;
/*!40000 ALTER TABLE `tried` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `txt_inbox`
--

DROP TABLE IF EXISTS `txt_inbox`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `txt_inbox` (
  `num` varchar(100) DEFAULT NULL,
  `date` varchar(20) DEFAULT NULL,
  `msg` varchar(400) DEFAULT NULL,
  `oid` int(255) DEFAULT NULL,
  `action` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `txt_inbox`
--

LOCK TABLES `txt_inbox` WRITE;
/*!40000 ALTER TABLE `txt_inbox` DISABLE KEYS */;
/*!40000 ALTER TABLE `txt_inbox` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `txts`
--

DROP TABLE IF EXISTS `txts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `txts` (
  `num` varchar(50) DEFAULT NULL,
  `oid` int(255) DEFAULT NULL,
  `date` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `txts`
--

LOCK TABLES `txts` WRITE;
/*!40000 ALTER TABLE `txts` DISABLE KEYS */;
/*!40000 ALTER TABLE `txts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `updates`
--

DROP TABLE IF EXISTS `updates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `updates` (
  `itemCode` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `updates`
--

LOCK TABLES `updates` WRITE;
/*!40000 ALTER TABLE `updates` DISABLE KEYS */;
/*!40000 ALTER TABLE `updates` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_bookmarks`
--

DROP TABLE IF EXISTS `user_bookmarks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_bookmarks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `description` varchar(128) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_bookmarks`
--

LOCK TABLES `user_bookmarks` WRITE;
/*!40000 ALTER TABLE `user_bookmarks` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_bookmarks` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_read_bulletins`
--

DROP TABLE IF EXISTS `user_read_bulletins`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_read_bulletins` (
  `user_id` int(11) NOT NULL,
  `bulletin_id` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_read_bulletins`
--

LOCK TABLES `user_read_bulletins` WRITE;
/*!40000 ALTER TABLE `user_read_bulletins` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_read_bulletins` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `status` tinyint(2) DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vehicleDays`
--

DROP TABLE IF EXISTS `vehicleDays`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vehicleDays` (
  `reg` varchar(200) DEFAULT NULL,
  `day` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vehicleDays`
--

LOCK TABLES `vehicleDays` WRITE;
/*!40000 ALTER TABLE `vehicleDays` DISABLE KEYS */;
/*!40000 ALTER TABLE `vehicleDays` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vehicleNotes`
--

DROP TABLE IF EXISTS `vehicleNotes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vehicleNotes` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `note` varchar(1000) DEFAULT NULL,
  `date` varchar(20) DEFAULT NULL,
  `time` varchar(20) DEFAULT NULL,
  `vid` int(255) DEFAULT NULL,
  `miles` double DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vehicleNotes`
--

LOCK TABLES `vehicleNotes` WRITE;
/*!40000 ALTER TABLE `vehicleNotes` DISABLE KEYS */;
/*!40000 ALTER TABLE `vehicleNotes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vehicle_cost_types`
--

DROP TABLE IF EXISTS `vehicle_cost_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vehicle_cost_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` text,
  `type` varchar(48) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vehicle_cost_types`
--

LOCK TABLES `vehicle_cost_types` WRITE;
/*!40000 ALTER TABLE `vehicle_cost_types` DISABLE KEYS */;
/*!40000 ALTER TABLE `vehicle_cost_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vehicle_costs`
--

DROP TABLE IF EXISTS `vehicle_costs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vehicle_costs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cost` decimal(11,2) DEFAULT NULL,
  `date_incurred` date DEFAULT NULL,
  `mileage` int(6) DEFAULT NULL,
  `notes` text,
  `user_id` int(11) DEFAULT NULL,
  `vehicle_cost_type_id` int(11) DEFAULT NULL,
  `vehicle_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vehicle_costs`
--

LOCK TABLES `vehicle_costs` WRITE;
/*!40000 ALTER TABLE `vehicle_costs` DISABLE KEYS */;
/*!40000 ALTER TABLE `vehicle_costs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vehicles`
--

DROP TABLE IF EXISTS `vehicles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vehicles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `reg` varchar(30) DEFAULT NULL,
  `height` decimal(10,2) DEFAULT '0.00',
  `width` decimal(10,2) DEFAULT '0.00',
  `depth` decimal(10,2) DEFAULT '0.00',
  `weight` decimal(10,2) DEFAULT '0.00',
  `description` text,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vehicles`
--

LOCK TABLES `vehicles` WRITE;
/*!40000 ALTER TABLE `vehicles` DISABLE KEYS */;
/*!40000 ALTER TABLE `vehicles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `warehouse_errors`
--

DROP TABLE IF EXISTS `warehouse_errors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `warehouse_errors` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `note` varchar(200) DEFAULT NULL,
  `item` varchar(45) DEFAULT NULL,
  `oid` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `OID_INDEX` (`oid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `warehouse_errors`
--

LOCK TABLES `warehouse_errors` WRITE;
/*!40000 ALTER TABLE `warehouse_errors` DISABLE KEYS */;
/*!40000 ALTER TABLE `warehouse_errors` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `warehouse_log`
--

DROP TABLE IF EXISTS `warehouse_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `warehouse_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `message` varchar(255) DEFAULT NULL,
  `staff_id` int(11) DEFAULT NULL,
  `warehouse_id` int(11) DEFAULT NULL,
  `warehouse_stock_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `staff` (`staff_id`),
  KEY `warehouse` (`warehouse_id`),
  KEY `warehouse_stck` (`warehouse_stock_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `warehouse_log`
--

LOCK TABLES `warehouse_log` WRITE;
/*!40000 ALTER TABLE `warehouse_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `warehouse_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `warehouse_stock`
--

DROP TABLE IF EXISTS `warehouse_stock`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `warehouse_stock` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `warehouse_id` int(11) DEFAULT NULL,
  `stock_parts_id` int(11) DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `warehouse` (`warehouse_id`),
  KEY `stock` (`stock_parts_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `warehouse_stock`
--

LOCK TABLES `warehouse_stock` WRITE;
/*!40000 ALTER TABLE `warehouse_stock` DISABLE KEYS */;
/*!40000 ALTER TABLE `warehouse_stock` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `warehouses`
--

DROP TABLE IF EXISTS `warehouses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `warehouses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` int(11) DEFAULT NULL,
  `location` varchar(48) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `warehouses`
--

LOCK TABLES `warehouses` WRITE;
/*!40000 ALTER TABLE `warehouses` DISABLE KEYS */;
/*!40000 ALTER TABLE `warehouses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wholesale`
--

DROP TABLE IF EXISTS `wholesale`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wholesale` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_seller` int(11) DEFAULT NULL,
  `id_stock` int(11) DEFAULT NULL,
  `cost` double DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wholesale`
--

LOCK TABLES `wholesale` WRITE;
/*!40000 ALTER TABLE `wholesale` DISABLE KEYS */;
/*!40000 ALTER TABLE `wholesale` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `whstaff`
--

DROP TABLE IF EXISTS `whstaff`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `whstaff` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `name` varchar(400) DEFAULT NULL,
  `uName` varchar(100) DEFAULT NULL,
  `pwd` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `whstaff`
--

LOCK TABLES `whstaff` WRITE;
/*!40000 ALTER TABLE `whstaff` DISABLE KEYS */;
/*!40000 ALTER TABLE `whstaff` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-01-30 10:26:24
