<?php

use App\Order;
use App\Jobs\UpdateOrderStatus;
use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class WowcherOrderDispatchedTest extends TestCase
{

    use DatabaseTransactions;

    public function test_Wowcher_Order_Dispatched_event_name_returns_the_proper_message()
    {
        $orderDispatched = new App\Filters\Orders\WowcherOrderDispatched();

        $this->assertEquals('dispatched', $orderDispatched->eventName());
    }

    public function test_Wowcher_Order_Dispatched_get_event_object_returns_the_proper_object()
    {
        $orderDispatched = new App\Filters\Orders\WowcherOrderDispatched();
        
        $order = Order::create();

        $event = $orderDispatched->get_event_object($order);
        $this->assertInstanceOf('App\Events\Wowcher\OrderDispatched', $event);
    }

    public function test_Wowcher_Order_Dispatched_filters_orders_properly()
    {
        $orderDispatched = new App\Filters\Orders\WowcherOrderDispatched();
        
        $order = Order::create();
        $WowcherOrder = Order::create([
            'companyName' => 'wowcher',
            'orderStatus' => 'routed'
        ]);
        

        $foundOrders = $orderDispatched->filter()->get();

        $this->assertTrue($foundOrders->contains($WowcherOrder));
        $this->assertFalse($foundOrders->contains($order));
    }
    
}
