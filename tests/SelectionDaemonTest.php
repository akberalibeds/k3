<?php

use App\Order;
use App\Customer;
use Carbon\Carbon;
use App\Dispatched;
use App\Notification;
use App\Jobs\SelectionDaemon;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class SelectionDaemonTest extends TestCase
{
    use DatabaseTransactions;

    public $customer;

    public function setUp()
    {
        parent::setUp();

        $this->customer = Customer::create([]);

    }

    public function testChecksForDeliveryDateRequestNotifications()
    {
        $order = Order::create([
            'cid' => $this->customer->id, 
            'done' => '0',
            'orderStatus' => 'Allocated'
        ]);

        (new SelectionDaemon('App\Notifications\Types\DeliveryDateRequest'))->handle();
        (new SelectionDaemon('App\Notifications\Types\DeliveryDateRequest'))->handle();  
        
        $this->hasCountInDatabase('notifications',1, [
            'customerID' => $this->customer->id,
            'orderID' => $order->id,
            'type' => 'App\Notifications\Types\DeliveryDateRequest'
        ]);
    }

    public function testChecksForDeliveryDateRequestReminderNotifications()
    {

        $order = Order::create([
            'cid' => $this->customer->id, 
            'done' => '0',
            'orderStatus' => 'Allocated',
            'time_stamp' => Carbon::now()->subDay(),
        ]);

        $notification = new Notification;
        $notification ->orderID = $order->id;
        $notification->customerID =  $this->customer->id;
        $notification->type = 'App\Notifications\Types\DeliveryDateRequest';
        $notification->hasSent = 1;
        $notification->updated_at = Carbon::now()->subDays(1);
        $notification->save(['timestamps' => false]);

        (new SelectionDaemon('App\Notifications\Types\DeliveryDateRequest'))->handle();
        (new SelectionDaemon('App\Notifications\Types\DeliveryDateRequest'))->handle();
        
        $this->hasCountInDatabase('notifications', 1, [
            'customerID' => $this->customer->id,
            'orderID' => $order->id,
            'type' => 'App\Notifications\Types\DeliveryDateRequest (repeat)'
        ]);

    }

    public function testChecksForDispatchReadyNotifications()
    {
        $order = Order::create([
            'cid' => $this->customer->id,
            'done' => 0,
            'deltime' => '12:00',
        ]);

        $disp = Dispatched::forceCreate([
            'oid' => $order->id,
            'date' => Carbon::now()->addDays(1)->format('d-M-Y'),
        ]);

        (new SelectionDaemon('App\Notifications\Types\DispatchReady'))->handle();
        
        $this->seeInDatabase('notifications', [
            'customerID' => $this->customer->id,
            'orderID' => $order->id,
            'dispID' => $disp->id,
            'type' => 'App\Notifications\Types\DispatchReady'
        ]);
    }

    public function testChecksForDeliveryImminent()
    {
        $order = Order::create([
            'cid' => $this->customer->id,
            'done' => '0',
            'orderStatus' => 'Routed',
            'delTime' =>  Carbon::now()->addHours(1)->format('H:m'),
        ]);
        
        $disp = Dispatched::forceCreate([
            'oid' => $order->id,
            'date' => Carbon::now()->format('d-M-Y'),
        ]);

        (new SelectionDaemon('App\Notifications\Types\DeliveryImminent'))->handle();

        $this->seeInDatabase('notifications', [
            'customerID' => $this->customer->id,
            'orderID' => $order->id,
            'dispID' => $disp->id,
            'type' => 'App\Notifications\Types\DeliveryImminent'
        ]);
    }
}
