<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Helper\Magento\Magento;

class MagentoTest //extends TestCase
{
    // use DatabaseTransactions;

    // public $fakeOrders;

    // public function setUp()
    // {
    //     parent::setUp();

    //     Auth::login(factory('App\User')->create());
        
    //     $orders = file_get_contents(storage_path('magentoOrders.json'));

    //     $this->fakeOrders = json_decode($orders, true);
    // }

    // public function test_it_filters_results_from_magento()
    // {
    //     $magento = new Magento();

    //     $magento->getOrders();

    //     $this->assertCount(1, $magento->orders);
    // }

    // public function test_fresh_order_should_not_be_excluded_from_the_list()
    // {
    //     $magento = new Magento();

    //     $freshOrder = $this->fakeOrders[0];

    //     $magento->getOrders()
    //             ->removeExisting();
        
    //     $this->assertTrue(in_array($freshOrder, $magento->orders));
    // }

    // public function test_it_parses_the_order_from_magento()
    // {
    //     $magento = new Magento();

    //     $freshOrder = $this->fakeOrders[0];

    //     $magento->getOrders()
    //             ->removeExisting()
    //             ->getAllInfo()
    //             ->formatOrders();
        
    //     $this->assertEquals($freshOrder['increment_id'], $magento->orders[0]['order']['beds']);
        
    //     $this->assertEquals('Beds.co.uk', $magento->orders[0]['order']['companyName']);
    // }

    // public function test_it_gets_the_order_info_from_magento_and_saves_the_order()
    // {
    //     $magento = new Magento();

    //     $freshOrder = $this->fakeOrders[0];

    //     $magento->getOrders()
    //             ->removeExisting()
    //             ->getAllInfo()
    //             ->formatOrders()
	// 			->insertOrders();	
        
    //     $this->seeInDatabase('orders', [ 'beds' => $freshOrder['increment_id'] ]);
    // }

    // public function test_it_detects_duplicates()
    // {
    //     $magento = new Magento();

    //     $freshOrder = $this->fakeOrders[0];

    //     $magento->getOrders()
    //             ->removeExisting()
    //             ->getAllInfo()
    //             ->formatOrders()
    //             ->insertOrders();	

    //     $this->seeInDatabase('orders', [ 'beds' => $freshOrder['increment_id'] ]);
        
    //     $magento->getOrders()
    //             ->removeExisting();	
        
    //     $this->assertFalse(in_array($freshOrder, $magento->orders));
    // }
}