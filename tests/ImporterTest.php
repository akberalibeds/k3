<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ImporterTest extends TestCase
{
    use DatabaseTransactions;

    public function test_a_company_belongs_to_one_importer()
    {
 
        $company = App\Company::create();

        $importer = App\Importer::create([
            'name' => 'Woocommerce'
        ]);
        
        $importer->companies()->save($company);

        $this->assertTrue($importer->companies->contains($company));
        $this->assertEquals($company->importer->name, 'Woocommerce');
    }
}