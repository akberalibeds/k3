<?php

use App\Order;
use App\Customer;
use App\Dispatched;
use Carbon\Carbon;
use App\Notification as N;
use App\NotificationMessage as NM;
use App\Jobs\SelectionDaemon;
use App\Jobs\MessageSenderDaemon;
use App\Http\Controllers\Cache;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class MessageSenderDaemonTest extends TestCase
{
    use DatabaseTransactions;
    
    public $customer, $order, $disp, $notification;

    public function setUp()
    {
        parent::setUp();

        $this->customer = Customer::create([
            'dBusinessName' => 'John Doe',
            'tel' => '01234567890',
            'userID' => 'USERID',
            'email1' => 'john.doe@email.com',
        ]);

        $this->order = Order::create([
            'cid' => $this->customer->id,
            'done' => '0',
            'code' => 'TEST-CODE',
            'ebayID' => 'ebayJohn1234',
            'orderStatus' => 'Allocated',
            'sender' => '1',
            'delTime' => Carbon::now()->addHours(1)->format('H:m'),
        ]);

        $this->disp = Dispatched::forceCreate(['oid' => $this->order->id]);

        $this->notification = N::create([
            'customerID' => $this->customer->id,
            'orderID' => $this->order->id,
            'dispID' => $this->disp->id_disp,
            'type' => 'App\Notifications\Types\DispatchReady', 
            'hasSent' => 0,
        ]);
    }

    public function testAbleToSendSMS()
    {
        $message = NM::create([
            'notificationID' => $this->notification->id,
            'recipient' => $this->customer->tel,
            'recipient_name' => $this->customer->dBusinessName,
            'subject' => 'NULL',
            'body' => "Please Choose A Delivery Date for your furniture order \n parcel id: {$this->notification->order->code}\nPlease go to:\n http://premierdeliveries.co.uk?id={$this->notification->order->code}",
            'msgMethod' => "App\Notifications\Methods\SMS",
        ]);

        (new MessageSenderDaemon)->handle();

        $this->assertTrue((boolean) $message->notification->hasSent);
    }

    public function testAbleToSendEmail()
    {
        $type = new $this->notification->type;

        $message = NM::create([
            'notificationID' => $this->notification->id,
            'recipient' => $this->customer->email1,
            'recipient_name' => $this->customer->dBusinessName,
            'subject' => $type->messageSubject($this->notification),
            'body' => $type->messageText((object) ['length' => 'long', 'representation' => 'html'], $this->notification),
            'msgMethod' => "App\Notifications\Methods\Email",
        ]);

        (new MessageSenderDaemon)->handle();

        $this->assertTrue((boolean) $message->notification->hasSent);

    }

    public function testAbleToSendEbay()
    {
        $type = new $this->notification->type;

        $message = NM::create([
            'notificationID' => $this->notification->id,
            'recipient' => $this->customer->userID,
            'recipient_name' => $this->customer->dBusinessName,
            'subject' => $type->messageSubject($this->notification),
            'body' => $type->messageText((object) ['length' => 'long', 'representation' => 'plain_text'], $this->notification),
            'msgMethod' => "App\Notifications\Methods\Ebay",
        ]);

        (new MessageSenderDaemon)->handle();

        $this->assertTrue((boolean) $message->notification->hasSent);

    }

}
