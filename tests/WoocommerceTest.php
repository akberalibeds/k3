<?php

use App\Order;
use Carbon\Carbon;
use App\Imports\SalesImporter\SalesImporter;
use App\Imports\Woocommerce\Woocommerce;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class WoocommerceTest extends TestCase
{

  use DatabaseTransactions;

    protected $woocommerce;
    protected $orders;

    public function setUp()
    {
        parent::setUp();

        $this->actingAs(factory('App\User')->create());

        $this->woocommerce = new Woocommerce('bedland.com',  Carbon::now()->subHours(1), Carbon::now());

        $this->orders = $this->getFakeOrders();
    }

   public function test_it_pulls_only_orders_where_status_is_completed()
   {

        $this->assertTrue(array_key_exists('status', $this->orders[0]));
        
        $this->assertEquals('completed', $this->orders[0]->status);
   }

   public function test_it_pulls_only_orders_in_specified_time()
   {

        $this->assertTrue(array_key_exists('date_created', $this->orders[0]));
        
        $this->assertTrue(new DateTime($this->orders[0]->date_created) < new DateTime("now") );
   }

   public function test_it_parses_the_response_and_it_makes_a_customer()
   {
        $customers = [];
        
        foreach($this->orders as $order) {
            $customers[] = $this->woocommerce->makeCustomer($order);
        }

        $this->assertEquals(count($this->orders), count($customers));
   }

   public function test_it_parses_the_response_and_it_makes_items()
   {
       $hasEmptyItems = false;
        
        foreach($this->orders as $order) {
            $item = $this->woocommerce->makeItems($order);

            $hasEmptyItems = $hasEmptyItems || count($item)==0;
        }
        
        $this->assertFalse($hasEmptyItems);
   }

   public function test_it_parses_the_response_and_it_makes_an_order()
   {
        $orders = [];
        
        foreach($this->orders as $order) {
            $orders[] = $this->woocommerce->makeOrder($order);
        }

        $this->assertEquals(count($this->orders), count($orders));
   }

   public function test_it_converts_the_response()
   {
        $salesImporter = new SalesImporter($this->woocommerce);
        
        $salesImporter->import();

        $ordersWithoutACustomer = \DB::table('orders')
        ->leftJoin('customers', 'orders.cid', '=', 'customers.id')
        ->where('customers.id', "=" ,'')
        ->get();

        $this->assertCount(0, $ordersWithoutACustomer);
        
   }

   public function test_it_does_not_store_duplicate_orders()
   {

        $salesImporter = new SalesImporter($this->woocommerce);
        
        $salesImporter->import();

        $num_of_orders_in_db  = Order::count();

        $salesImporter->import();
        
        $this->assertEquals($num_of_orders_in_db, Order::count());
        
   }

    
}