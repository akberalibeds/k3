<?php

class TestCase extends Illuminate\Foundation\Testing\TestCase
{
    /**
     * The base URL to use while testing the application.
     *
     * @var string
     */
    protected $baseUrl = 'http://localhost';

    /**
     * Creates the application.
     *
     * @return \Illuminate\Foundation\Application
     */
    public function createApplication()
    {
        $app = require __DIR__.'/../bootstrap/app.php';

        $app->make(Illuminate\Contracts\Console\Kernel::class)->bootstrap();

        return $app;
    }

    public function signIn($user = null)
    {
        $user = $user ?: factory('App\User')->make();
        $this->actingAs($user);
        return $this;
    }

    public function getFakeOrders()
    {
        $json_orders = file_get_contents(storage_path('orders.json'));

        return  json_decode($json_orders);
    }
    
    
    protected function hasCountInDatabase($table, $expectedCount, array $data, $connection = null)
    {
        $database = $this->app->make('db');

        $connection = $connection ?: $database->getDefaultConnection();

        $count = $database->connection($connection)->table($table)->where($data)->count();

        $this->assertEquals($expectedCount, $count, sprintf(
            'Expected to find exactly [%d] row(s) but instead found [%d] in database table [%s] that matched attributes [%s].', $expectedCount, $count, $table, json_encode($data)
        ));

        return $this;
    }
}
