<?php

use App\Order;
use App\Imports\SalesImporter\SalesImporter;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class SalesImporterTest extends TestCase
{
    use DatabaseTransactions;

    public function test_it_imports_all()
    {
        $this->actingAs(factory('App\User')->create());
        
        $company = App\Company::create([
        'company_name' => 'bedland.com'
        ]);

        $importer = App\Importer::create([
            'name' => 'Woocommerce',
            'class_name' => 'App\Imports\Woocommerce\Woocommerce'
        ]);
        
        $importer->companies()->save($company);
        
        $num_of_orders_in_db  = Order::count();

        SalesImporter::importAll();

        $this->assertTrue(Order::count() >= $num_of_orders_in_db);

    }
}