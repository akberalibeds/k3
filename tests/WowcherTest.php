<?php

use App\Order;
use App\Jobs\UpdateOrderStatus;
use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class WowcherTest extends TestCase
{

    use DatabaseTransactions;

    public function test_it_does_not_update_the_status_of_the_order_once_created_if_it_is_not_by_wowcher()
    {
        $this->withoutEvents();
        
        $order = Order::create();
        $job = new UpdateOrderStatus();
        
        $job->handle();

        $this->notSeeInDatabase('order_events', ['order_id' => $order->id ]);
    }
   
    public function test_it_updates_the_status_of_the_order_once_created()
    {
        $this->withoutEvents();

        $order = Order::create([
            'companyName' => 'wowcher',
        ]);

        $job = new UpdateOrderStatus();
        
        $job->handle();

        $this->seeInDatabase('order_events', ['order_id' => $order->id ]);
    }

}