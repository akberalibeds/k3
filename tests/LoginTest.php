<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class LoginTest extends TestCase
{

    
    public function test_an_unauthorised_user_is_redirected_to_the_proper_location()
    {

        $this->get('/');
        
        $this->assertRedirectedTo('/not-authorised');

    }

    public function test_a_user_can_visit_the_login_page()
    {
        $this->get('/login')
             ->see('Login');
    }
}
