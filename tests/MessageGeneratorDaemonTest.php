<?php

use App\Order;
use App\Customer;
use App\Dispatched;
use App\Notification;
use App\Jobs\MessageGeneratorDaemon;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class MessageGeneratorDaemonTest extends TestCase
{
    use DatabaseTransactions;

    public function test_it_creates_messages()
    {
        $customer = Customer::create([
            'email1' => 'johnDoe@example.com',
            'dBusinessName' => 'John Doe',
            'userID' => 'USERID',
            'tel' => '12345678910'
        ]);

        $order = Order::create([
            'cid' => $customer->id, 
            'done' => '0',
            'code' => 'TEST-CODE',
            'orderStatus' => 'Allocated',
            'ebayID' => 'ebay123-id123',
            'sender' => '1'
        ]);

        $disp = Dispatched::forceCreate(['oid' => $order->id]);

        $notification = Notification::create([
            'customerID' => $customer->id,
            'orderID' => $order->id,
            'dispID' => $disp->id,
            'type' => 'App\Notifications\Types\DeliveryDateRequest',
            'hasSent' => false
        ]);

        (new MessageGeneratorDaemon)->handle();
        
        $this->seeInDatabase('notification_messages', [
                'msgMethod' => 'App\Notifications\Methods\SMS',
                'notificationID' => $notification->id
            ]);
        
        $this->seeInDatabase('notification_messages', [
            'msgMethod' => 'App\Notifications\Methods\Email',
            'notificationID' => $notification->id
        ]);

        $this->seeInDatabase('notification_messages', [
            'msgMethod' => 'App\Notifications\Methods\Ebay',
            'notificationID' => $notification->id
        ]);
    }
}
