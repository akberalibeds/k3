<?php

use App\StockItem;
use App\StockParts;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class StockPartsTest extends TestCase
{
    use DatabaseTransactions;

    public $stockItem;

    public function setUp()
    {
        parent::setUp();

        $this->stockItem = StockItem::create([
            'pieces' => 0,
        ]);

    }

    public function test_parts_under_amount()
    {
        $stockPart1 = new StockParts([
            'parent_id' => 
            $this->stockItem->id,
            'part' => 'Box 1',
            'quantity' => 0
        ]);
        $stockPart2 =  new StockParts([
            'parent_id' => 
            $this->stockItem->id,
            'part' => 'Box 2',
            'quantity' => 0
        ]);

        $this->stockItem->stockParts->push($stockPart1);
        $this->stockItem->stockParts->push($stockPart2);
        $this->stockItem->push();

        $this->stockItem->pieces = 4;
        StockParts::updateStockParts($this->stockItem);

        $this->assertEquals(
            $this->stockItem->pieces,
            $this->stockItem->stockParts->count()
        );
        $this->stockItem->save();
        $this->assertEquals(
            $this->stockItem->pieces,
            StockParts::where('parent_id', $this->stockItem->id)->count()
        );
    }

    public function test_parts_over_amount(){
        $stockPart1 = new StockParts([
            'parent_id' => 
            $this->stockItem->id,
            'part' => 'Box 1',
            'quantity' => 0
        ]);
        $stockPart2 = new StockParts([
            'parent_id' => 
            $this->stockItem->id,
            'part' => 'Box 2',
            'quantity' => 0
        ]);
        $stockPart3 = new StockParts([
            'parent_id' => 
            $this->stockItem->id,
            'part' => 'Box 3',
            'quantity' => 0
        ]);
        $stockPart4 = new StockParts([
            'parent_id' => 
            $this->stockItem->id,
            'part' => 'Box 4',
            'quantity' => 0
        ]);
        $stockPart5 = new StockParts([
            'parent_id' => 
            $this->stockItem->id,
            'part' => 'Box 5',
            'quantity' => 0
        ]);

        $this->stockItem->stockParts->push($stockPart1);
        $this->stockItem->stockParts->push($stockPart2);
        $this->stockItem->stockParts->push($stockPart3);
        $this->stockItem->stockParts->push($stockPart4);
        $this->stockItem->stockParts->push($stockPart5);
        $this->stockItem->push();

        $this->stockItem->pieces = 3;
        StockParts::updateStockParts($this->stockItem);
        $this->assertGreaterThanOrEqual(
            $this->stockItem->pieces,
            $this->stockItem->stockParts->count()
        );
        $this->stockItem->save();
        $this->assertGreaterThanOrEqual(
            $this->stockItem->pieces , StockParts::where('parent_id', $this->stockItem->id)->count()
        );
    }
}
