<?php

use App\Links;
use App\Jobs\NotifyCustomers;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class NotifyCustomersTest extends TestCase
{

  use DatabaseTransactions;

  protected $job;

  public function setUp()
  {
      parent::setUp();

      $this->job = new NotifyCustomers();
  }

  public function xtest_it_formats_the_tel_number()
  {
      $badFormattedNumbers = [
          '0758 7735380',
          '023 80774967 / 07427 603 021'
      ];

      $this->assertEquals('447587735380', $this->job->getNum($badFormattedNumbers[0]) );

      $this->assertEquals('447427603021',$this->job->getNum($badFormattedNumbers[1]) );
    
  }

  public function xtest_it_evaluates_the_availability_of_a_particular_item()
  {
      $stock = App\StockItem::create(['actual' => '100' ]);

      $inStockItem = $stock->items()->create(['currStock' => 100,]);

      $this->assertEquals('yes', $this->job->inStock($inStockItem) );
    
  }

  public function test_it_updates_the_link_table_if_order_status_criteria_is_met()
  {
     $customer = App\Customer::create(['tel' => '07587735380' ,'mob' => '07000000000', 'email1' => 'john@example.com', 'dBusinessName' => 'John Doe', 'userID' => 'test123' ]);
     $order = App\Order::create(['orderStatus' => 'Allocated', 'delRoute' => 'MIDLANDS', 'cid' => $customer->id, 'ebayID' => 'TEST-EBAY-ID', 'sender' => '0', 'code' => 'TEST-CODE']);
     $link = $order->links()->create(['sent' => 0,'done' => 0, 'code' => 'TEST']);
     $stock = App\StockItem::create(['actual' => 500, 'blocked' => 0]);
     $item = $order->items()->create(['currStock' => 500, 'blocked' => 0, 'itemid' => $stock->id]);

     $this->job->handle();

     $this->assertEquals(1, $link->fresh()->done);
     $this->assertEquals(1, $link->fresh()->sent);

  }

}
