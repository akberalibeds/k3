<?php

use GuzzleHttp\Client;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Middleware;
use GuzzleHttp\Handler\MockHandler;
use App\Helper\Wowcher\Wowcher;
use GuzzleHttp\Exception\RequestException;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class WowcherRequestTest extends TestCase
{

    public function test_it_sets_the_headers_and_body_of_the_request()
    {
        // Create a mock and queue a response.
        $mock = new MockHandler([
                new Response(200, [
                        "Content-Type"=>"application/json",
                        "Accept"=>"application/json",
                ], json_encode([
                    'response' => [
                        'data' => [
                            'token' => 'test-token'
                        ]
                    ]
                ])
           ),
           
           new Response(200, [
                    "Content-Type"=>"application/json",
                    "Accept"=>"application/json",
                ],json_encode([
                    'response' => [
                        'data' => [
                            'message' => 'success'
                        ]
                    ]
                ])
            ),
           
        ]);

        $container = [];

        $history = Middleware::history($container);

        $handler = HandlerStack::create($mock);

        $handler->push($history);
        
        //Each response at the top is asscociated with one request
        $client = new Wowcher(['handler' => $handler]);

        $client->updateWowcherStatus('URPUWF-WMT4V3', 'dispatched');
   

        // Iterate over the requests and responses
        foreach ($container as $index => $transaction) {

            if($index == 1) {
                //test request method
                 $this->assertEquals('PATCH', $transaction['request']->getMethod());

                //store body of request
                $data = json_decode($transaction['request']->getBody(), true)['deliveryStatusUpdate'];

                //test response body
                $this->assertEquals('success', json_decode($transaction['response']->getBody(), true)['response']['data']['message'] );
                
                //test request body
                $this->assertEquals('dispatched', $data['deliveryStatus']);

                //test request headers
                $this->assertEquals('wowcher', $transaction['request']->getHeader('brand')[0]);
                $this->assertEquals('test-token', $transaction['request']->getHeader('wowcher_user')[0]);
            }
          

            if ($transaction['response']) {
                
                //test response status code
                $this->assertEquals(200, $transaction['response']->getStatusCode());
                
            }
        }
    }

}