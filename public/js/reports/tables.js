// @author Giomani Designs (Development Team)
//
google.charts.load('current', { 'packages': ['table', 'controls'] })

google.charts.setOnLoadCallback(chartsIsReady)

function drawTable (tableData, containerId) {
  var column
  var data = new google.visualization.DataTable()
  var formatter

  for (var i = 0, j = tableData.columns.length; i < j; i++) {
    column = tableData.columns[i]
    data.addColumn(column)
  }

  data.addRows(tableData.rows)

  var dashboard = new google.visualization.Dashboard(document.getElementById('dashboard_div'))

  var options = {
    backgroundColor: 'transparent',
    width: 'auto'
  }

  var table = new google.visualization.ChartWrapper({
    chartType: 'Table',
    containerId: 'table_div',
    options: options
  })

  if (tableData.numberColumns !== undefined) {
    formatter = new google.visualization.NumberFormat({
      fractionDigits: 2,
      groupingSymbol: ',',
      negativeColor: '#ff0000' // doesn't work, might one day @see https://developers.google.com/chart/interactive/docs/reference#numberformat
    })
    for (var numberColumn in tableData.numberColumns) {
      formatter.format(data, numberColumn)
    }
  }

  var control = new google.visualization.ControlWrapper({
    controlType: 'StringFilter',
    containerId: 'control_div',
    options: {
      filterColumnIndex: 1
    }
  })
  // var table = new google.visualization.Table(document.getElementById('chart_div'))

  dashboard.bind([control], [table])
  dashboard.draw(data, { allowHtml: true, alternatingRowStyle: true })
}

function chartsIsReady () {
  console.log('Charts is ready')
}
