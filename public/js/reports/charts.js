// // @author Giomani Designs (Development Team)
// //
// var $ = $ || {}
// //
// var tableDataG = {}
// //
// //
// function assembleTable (data)
// {
//   var $itemsDataTable
//   var columnCount
//   var count
//   var dates
//   var datum
//   var html
//   var i, j, m
//   var item
//   var row
//   var rowTotal
//   var stockItemCode
//   var tableRows
//   var title
//
//   setStatus('Processing data <span class="glyphicon glyphicon-refresh glyphicon-spin" aria-hidden="true">')
//
//   switch (tableDataG.period) {
//     case 'days' :
//       title = 'Daily'
//       break
//     case 'months' :
//       title = 'Monthly'
//       break
//     case 'quarters' :
//       title = 'Quarterly'
//       break
//     case 'weeks' :
//       title = 'Weekly'
//       break
//   }
//
//   title += ' report (' + tableDataG.dateFrom.format('YYYY-MMM-DD') + ' - ' + tableDataG.dateTo.format('YYYY-MMM-DD') + ') - ' + tableDataG.series
//
//   $itemsDataTable = $('#items-data-table')
//   $itemsDataTable.html('')
//
//   tableRows = []
//   tableRows.length = 0
//
//   dates = getReportDates(tableDataG.dateAddition, tableDataG.dateFrom, tableDataG.dateTo)
//
//   for (i = 0, j = data.length; i < j; i++) {
//     datum = data[i]
//     stockItemCode = encodeURIComponent(datum.itemCode)
//     if (tableRows[stockItemCode] === undefined) {
//       tableRows[stockItemCode] = {}
//       tableRows.length++
//     }
//     tableRows[stockItemCode][datum.orderDate] = datum.itemCount
//   }
//
//   columnCount = dates.length + 2
//
//   html = ''
//   html += '<div class="table-resposive">'
//   html += '<table class="table-v-striped table-condensed table-report">'
//   html += '<thead>'
//   html += '<tr><th colspan="' + columnCount + '">' + title + '</th></tr>'
//
//   html += '<tr><th></th>'
//
//   for (i = 0, j = dates.length; i < j; i++) {
//     html += '<th><span class="text-vertical">' + dates[i].format('YYYY-MMM-DD') + '</span></th>'
//   }
//   html += '<th>Total</th>'
//
//   html += '</tr>'
//
//   html += '</thead>'
//   html += '<tbody>'
//
//   for (row in tableRows) {
//     rowTotal = 0
//     html += '<tr>'
//     html += '<td>' + decodeURIComponent(row) + '</td>'
//
//     m = 0
//     for (item in tableRows[row]) {
//       while (item !== dates[m].format('YYYY-MM-DD')) {
//         html += '<td>-</td>'
//         m++
//       }
//       m++
//       count = tableRows[row][item]
//       html += '<td>' + count + '</td>'
//       rowTotal += count
//     }
//     for (j = dates.length; m < j; m++) {
//       html += '<td>-</td>'
//     }
//     html += '<td>' + rowTotal + '</td>'
//
//     html += '</tr>'
//   }
//
//   html += '</tbody>'
//   html += '</table>'
//
//   $itemsDataTable.html(html)
// }
// //
// function downloadReport ()
// {
//   var uri = ''
//
//   uri += 'reports-items-download.php?'
//   uri += 'from=' + tableDataG.dateFrom.format('YYYY-MMM-DD')
//   uri += '&period=' + tableDataG.period
//   uri += '&series=' + tableDataG.series
//   uri += '&to=' + tableDataG.dateTo.format('YYYY-MMM-DD')
//
//   document.location.href = uri
// }
// //
// function drawTable ()
// {
//   var dateAddition
//   var dateFrom
//   var dateTo
//   var period
//   var series
//
//   dateFrom = $('#input-from-date').data('DateTimePicker').date()
//   dateTo = $('#input-to-date').data('DateTimePicker').date()
//   period = $('#select-period').val()
//   series = $('#select-data-series').val()
//
//   if (dateFrom && dateTo && dateFrom.isBefore(dateTo)) {
//     if ($('#select-period').val() === '') {
//       showModalPopup('Dates Error', 'Choose a period.')
//     } else {
//       switch (period) {
//         case 'days' :
//           dateAddition = moment.duration({'days': 1})
//           break
//         case 'months' :
//           dateAddition = moment.duration({'months': 1})
//           dateFrom = dateFrom.startOf('month')
//           dateTo = dateTo.endOf('month')
//           break
//         case 'weeks' :
//           dateAddition = moment.duration({'days': 7})
//           dateFrom = dateFrom.startOf('week').add({days: 1})
//           dateTo = dateTo.endOf('week').add({days: 1})
//           break
//       }
//
//       tableDataG.dateAddition = dateAddition
//       tableDataG.dateFrom = dateFrom
//       tableDataG.dateTo = dateTo
//       tableDataG.period = period
//       tableDataG.series = series
//
//       loadGraphData(dateFrom, dateTo, period, series, function (data) {
//         assembleTable(data)
//       })
//     }
//   } else {
//     showModalPopup({
//       body: 'Enter a valid <em>from</em> and <em>to</em> date.',
//       message: '',
//       dismissButtonText: 'Close',
//       title: 'Dates Error!'
//     })
//   }
// }
// //
// function getReportDates (addition, dateFrom, dateTo)
// {
//   var allowedDates
//   var date
//
//   allowedDates = []
//   date = dateFrom
//
//   while (date.isSameOrBefore(dateTo))
//   {
//     allowedDates.push(date.clone())
//     date.add(addition)
//   }
//   return allowedDates
// }
// //
// function loadGraphData (dateFrom, dateTo, period, series, fnDataLoaded)
// {
//   setStatus('Fetching data <span class="glyphicon glyphicon-refresh glyphicon-spin" aria-hidden="true">')
//
//   $.ajax({
//     'method': 'GET',
//     'url': '/reports/itemsData/' + dateFrom.format('YYYY-MM-DD') + '/' + dateTo.format('YYYY-MM-DD') + '/' + period + '/' + series
//   })
//   .done(function (msg)
//   {
//     // setStatus('')
//     msg = JSON.parse(msg)
//     fnDataLoaded(msg.data)
//   })
// }
// //
// function setStatus (message)
// {
//   $('#status-message').html(message)
// }
//
// /* ///////////////////////////////////// */
//
