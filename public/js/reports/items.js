// @author Giomani Designs (Development Team)
//
//
var columns = [
  { heading: 'Date', type: 'string' },
  { heading: 'Item Code', type: 'string' },
  { heading: 'Company', type: 'string' },
  { heading: 'Count', type: 'number' },
  { heading: 'Sum', type: 'string' }
]

function getItemsData () {
  var companies = ''
  var seperator
  var url

  seperator = ''
  for (var company in params.companies) {
    if (params.companies[company]) {
      companies += seperator + (company)
      seperator = '|'
    }
  }

  url = '/reports/data/items/' + params.dateFrom + '/' + params.dateTo + '/' + companies

  appIsBusy(true, 'Loading data...')

  $.form({
    method: 'GET',
    contentType: 'application/json',
    data: null,
    url: url,
    success: function (message) {
      addMessages('loaded ' + message.count + ' rows.')
      addMessages('assembling tabel.')

      switch (params.period) {
        case 'daily' :
          makeDailyTable(message.data, message.count)
          break
        case 'weekly' :
          makePeriodicTable(message.data, message.count, 'w')
          break
        case 'monthly' :
          makePeriodicTable(message.data, message.count, 'm')
          break
        case 'quarterly' :
          makePeriodicTable(message.data, message.count, 'q')
          break
      }
    },
    error: function (message) {
      appIsBusy(false)
    }
  })
}

function addPeriodicRows (periodRows, strDate) {
  var count
  var pItemCode = ''
  var periodItemCount = 0
  var rows = []

  for (var itemCode in periodRows) {
    for (var company in periodRows[itemCode]) {
      count = periodRows[itemCode][company]
      rows.push([strDate, itemCode, company, count])
      strDate = ''
      periodItemCount += count
    }
    rows.push(['******', itemCode + 'Total = ', '', periodItemCount])
    periodItemCount = 0
  }

  return rows
}

function makeDailyTable (data, count) {
  var date
  var itemCode
  var itemCount
  var nextItemCode
  var pDate = ''
  var pItemCode = ''
  var rows = []
  var tSum
  var sum

  tSum = null
  sum = 0
  for (var i = 0; i < count; i++) {
    date = data[i].date

    if (date === pDate) {
      date = ''
    } else {
      pDate = date
    }

    itemCode = data[i].item_code
    if (itemCode === null || itemCode === '') {
      itemCode = '[blank]'
    }

    if (i + 1 === count) {
      nextItemCode = '?'
    } else {
      nextItemCode = data[i + 1].item_code
      if (nextItemCode === null || nextItemCode === '') {
        nextItemCode = '[blank]'
      }
    }

    itemCount = data[i].count
    sum += itemCount

    if (itemCode !== nextItemCode) {
      tSum = '' + sum
      sum = 0
    } else {
      tSum = null
    }

    rows.push([date, itemCode, data[i].company, itemCount, tSum])
  }
  drawTable({
    columns: columns,
    rows: rows
  })
}

function makePeriodicTable (data, count, period) {
  var company
  var date
  var datum
  var periodEnd = moment(params.dateFrom)
  var itemCode
  var periodRows = {}
  var rows = []
  var strdate

  switch (period) {
    case 'w' :
      periodEnd.day('sunday')
      if (periodEnd.isBefore(moment(params.dateFrom))) {
        periodEnd.day(7)
      }
      break
    case 'm' :
      periodEnd.date(1).month(periodEnd.month() + 1).subtract(1, 'd')
      break
    case 'q' :
      var m = (periodEnd.quarter() * 3) + 1
      periodEnd.date(1).month(m).subtract(1, 'd')
      break
  }

  for (var i = 0; i < count; i++) {
    datum = data[i]
    date = moment(datum.date)

    if (date.isAfter(periodEnd)) {
      rows = rows.concat(addPeriodicRows(periodRows, periodEnd.format('YYYY-MM-DD')))
      periodRows = []
      switch (period) {
        case 'w' :
          periodEnd.add(7, 'd')
          break
        case 'm' :
          periodEnd.add(1, 'd').add(1, 'M').subtract(1, 'd')
          break
        case 'q' :
          periodEnd.add(1, 'd').add(3, 'M').subtract(1, 'd')
          break
      }
    }

    if (datum.itemCode == null || datum.itemCode === '') {
      datum.itemCode = '[blank]'
    }
    if (periodRows[datum.item_code] === undefined) {
      periodRows[datum.item_code] = {}
    }

    if (datum.company === undefined || datum.company === '') {
      datum.company = '[blank]'
    }
    if (periodRows[datum.item_code][datum.company] === undefined) {
      periodRows[datum.item_code][datum.company] = 0
    }

    periodRows[datum.item_code][datum.company] = Number(periodRows[datum.item_code][datum.company]) + parseInt(datum.count, 10)
  }

  rows = rows.concat(addPeriodicRows(periodRows, periodEnd.format('YYYY-MM-DD')))

  drawTable({
    columns: columns,
    rows: rows
  })

  appIsBusy(false)
}
