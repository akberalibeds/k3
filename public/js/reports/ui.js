// @author Giomani Designs (Development Team)
//

var dateFromPicker
var dateToPicker

var params = {
  companies: {},
  dateFrom: '',
  dateTo: '',
  period: '',
  chartType: ''
}

function addMessages (messages, forever) {
  messages = Array.isArray(messages) ? messages : [messages]
  forever = forever === true ? 0 : 5000

  for (var i = 0, j = messages.length; i < j; i++) {
    $.notify({
      message: '<span>' + messages[i] + '</span><br>'
    }, {
      delay: forever
    })
  }
}

function allCompanies (self) {
  if ($(self).attr('data-state') === 'all') {
    $('#companies input').prop('checked', 'checked')
    $(self).attr('data-state', 'none')
    $(self).html('none')
  } else {
    $('#companies input').prop('checked', null)
    $(self).attr('data-state', 'all')
    $(self).html('all')
  }
}

function clearAll () {
  clearGraphs()
}

function clearGraphs () {
  $('#charts-container').empty()
}


function draw () {
  var warnings = getParams()

  if (warnings.length !== 0) {
    addMessages(warnings)
  } else {
    getItemsData()
  }
}

function getParams () {
  var checked
  var warnings = []

  checked = false
  $('#companies input').each(function () {
    params.companies[$(this).val()] = $(this).is(':checked')
    checked = ($(this).is(':checked')) || checked
  })
  if (!checked) {
    warnings.push('You need to choose a Company.')
  }

  params.chartType = $('#display input[name="chart-type"]:checked').val()

  params.dateFrom = dateFromPicker.get('select', 'yyyy-mm-dd')
  if (params.dateFrom === '')
  {
    warnings.push('You need to choose From date.')
  }

  params.dateTo = dateToPicker.get('select', 'yyyy-mm-dd')
  if (params.dateTo === '')
  {
    warnings.push('You need to choose To date.')
  }

  params.period = $('#period input[name="period"]:checked').val()

  return warnings
}


function progressBarReset(max) {
  $('#progress-bar progress').prop('value', 0).prop('max', max)
}

function progressProgressBar(p) {
  $('#progress-bar progress').prop('value', p)
}
//
var $picker
var yesterday = new Date()
yesterday.setDate(yesterday.getDate() - 1)

console.log(yesterday)

$picker = $('#date-from').pickadate({
    editable: false,
    max: yesterday,
    min: [2013, 12, 2]
  }
)
dateFromPicker = $picker.pickadate('picker')

$picker = $('#date-to').pickadate({
    editable: false,
    max: yesterday,
    min: [2013, 12, 2]
  }
)

dateToPicker = $picker.pickadate('picker')

$('[data-toggle="tooltip"]').tooltip()
