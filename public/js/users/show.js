// @author Giomani Designs (Development Team)
//
function changeName () {
  appIsBusy(true)
  $.form({
    method: 'PUT',
    data: { 'name': document.getElementById('name-change').value },
    url: '/profile/update-name',
    success: function (data) {
      document.getElementById('user-display-name').innerHTML = data.reply
      appIsBusy(false)
      $('#change-name-collapse').collapse('hide')
    },
    error: function (data) {
      bootbox.alert({
        message: 'There was a problem with the name change.  Inform an administrator.'
      })
      appIsBusy(false)
    }
  })
}
function changePassword () {
  appIsBusy(true)
  $('[id^="error"]').html()
  $.form({
    method: 'PUT',
    data: {
      'old_password': document.getElementById('old_password').value,
      'new_password': document.getElementById('new_password').value,
      'new_password_confirmation': document.getElementById('new_password_confirmation').value
    },
    url: '/profile/update-password',
    success: function (data) {
      appIsBusy(false)
      bootbox.alert('Your password has been changed.', function () {
        console.log('This was logged in the callback!')
      })
    },
    error: function (data) {
      var errors
      var response
      appIsBusy(false)
      response = data.responseJSON
      errors = Object.keys(response)
      for (var error in errors) {
        $('#error-' + errors[error]).html(response[errors[error]].pop())
      }
    }
  })
}
