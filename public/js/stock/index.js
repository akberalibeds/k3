// @author Giomani Designs (Development Team)
//
tableResults.setC({
  objectIdProperty: 'id',
  rowButtonClickFn: function (row) {
    clickOnSO(row.id)
  },
  rowClickFn: function (row) {
    clickOnRow(row.id)
  },
  rowRendererFn: function (row) {
    return renderRow(row)
  },
  selectable: true,
  url: '/stock'
})
tableResults.init()
function clickOnRow (stockId) {
  location.pathname = 'profile'
}
function clickOnSO (stockId) {
  window.open('/stock-orders/' + stockId, '_blank')
}
function renderRow (object) {
  var html
  html = '<td>' + object.itemCode + '</td>' +
    '<td>' + nullIsADash(object.model_no) + '</td>' +
    '<td>' + nullIsADash(object.itemDescription) + '</td>' +
    '<td>' + nullIsADash(object.sales_category) + '</td>' +
    '<td class="text-right">' + nullIsADash(object.actual) + '</td>' +
    '<td class="text-right">'
  if (object.qty_on_so !== 0) {
    html += '<button class="btn btn-default btn-mini" data-ref="">' + object.qty_on_so + '</button>'
  } else {
    html += object.qty_on_so
  }
  html += '</td>' +
    '<td class="text-right">' + (object.actual - object.qty_on_so) + '</td>' +
    '<td class="text-right">' + money(object.cost) + '</td>' +
    '<td class="text-right">' + money(object.beds_price) + '</td>' +
    '<td class="text-right">' + money(object.ebay_price) + '</td>' +
    '<td class="text-right">' + money(object.retail) + '</td>' +
    '<td class="text-right">' + money(object.wholesale) + '</td>' +
    '<td class="text-right">' + nullIsADash(object.pieces) + '</td>' +
    '<td class="text-right">' + object.isMulti + '</td>' +
    '<td class="text-center">' + tickOrCross(object.blocked) + '</td>' +
    '<td class="text-center">' + tickOrCross(object.withdrawn) + '</td>' +
    '<td class="text-center">' + tickOrCross(object.always_in_stock) + '</td>'
  return html
}
