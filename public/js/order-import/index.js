// @author Giomani Designs (Development Team)
//
var uploader = $('#upload-drop').uploadDrop({
  fileIsReadyFn: function () {
    readyToUpload()
  },
  fileTypes: [
    'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
    'application/vnd.ms-excel'
  ],
  finishedFn: function (response) {
    complete(response)
  },
  icons: [
    '<i class="fa fa-file-excel-o fa-5x" aria-hidden="true"></i>',
    '<i class="fa fa-file-excel-o fa-5x" aria-hidden="true"></i>'
  ],
  messageElement: 'upload-messages',
  placeholder: 'Drop data file here.',
  progressElement: '#progress-bar',
  url: '/order-import/'
})
function readyToUpload () {
}
function sheetIsReady () {
  csvViewer.renderTable()
}
function uploadFile () {
  uploader.transmitChunks()
}
function complete (response) {
  var i, j, m, n
  var data = response.sheet.shift()
  var keys
  var row
  var sheet = []
  for (i = 0, j = data.length; i < j; i++) {
    keys = Object.keys(data[i])
    row = []
    for (m = 0, n = keys.length; m < n; m++) {
      row.push(data[i][keys[m]])
    }
    sheet.push(row)
  }
  csvViewer.setC({
    profile: profile,
    readyFn: function () {
      sheetIsReady()
    },
    selectHide: true,
    selectIgnore: false,
    url: '/order-import'
  })
  csvViewer.init(sheet)
}
