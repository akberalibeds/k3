//
//
$('#import-container').importCsv({
  afterTablefn: null,
  beforeUploadFn: function (data) {
    return {
      'send_email': ($('#send_email').is(':checked') ? 'yes' : 'no')
    }
  },
  headingsUrl: '/dispatch/tracking/tnt-tracking/headings',
  uiControlsFn: function () {
    return '<div class="checkbox">' +
        '<label>' +
          '<input id="send_email" name="send_email" type="checkbox"> send email' +
        '</label>' +
      '</div>'
  },
  uploadUrl: '/dispatch/tracking/tnt-tracking/store'
})
