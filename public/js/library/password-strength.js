// @see http://jquerycards.com/forms/inputs/strength-js/
//
$(document).ready(function ($) {
  $('#new-password-container input').strength({
    strengthClass: 'form-control strength',
    strengthMeterClass: 'strength_meter',
    strengthButtonClass: 'button_strength',
    strengthButtonText: 'Show password',
    strengthButtonTextToggle: 'Hide Password'
  })
})


function suggestPassword () {
  var length = 8
  var charset = '0123456789abcdefghijklmnopqrstuvwxyz!£$%^&*_+=-ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789'
  var retVal = ''

  for (var i = 0, n = charset.length; i < length; ++i) {
    retVal += charset.charAt(Math.floor(Math.random() * n))
  }

  $('#new-password-container input').val(retVal)

  $('#new-password-container input').trigger('keyup')
}
