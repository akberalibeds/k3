// @author Giomani Designs (Development Team)
//
(function ($) {
  $.fn.importCsv = function (options) {
    var MODE_DISABLED = 0
    var MODE_UPLOAD = 'upload'
    var MODE_VERIFY = 'verify'

    var $e
    var self = this

    self.getCsvColumns = function () {
      var csvColumns = []

      csvColumns.push('')

      self.$csvTable.find('thead tr th').each(function (i, element) {
        if (i > 0) {
          element = $(element).children()[0]
          if (element !== undefined) {
            csvColumns.push($(element).attr('data-column-name'))
          } else {
            csvColumns.push('')
          }
        }
      })
      return csvColumns
    }
    self.csvData = []

    self.settings = $.extend({
      afterColumnHeadingDropFn: null,
      afterTableFn: null,
      beforeUploadFn: null,
      headings: null,
      headingsUrl: null,
      onSetIgnoreFn: null,
      uiControlsFn: null,
      uploadUrl: null
    }, options)

    self.$busy = $('<div>', {'class': 'busy-loading'}).html('<i class="fa fa-spinner busy-spinner" aria-hidden="true"></i>')
    $('body').prepend(self.$busy)
    self.$busy.hide()

    if (self.settings.headings !== null) {
      setHeadings(self.settings.headings)
    } else if (self.settings.headingsUrl !== null) {
      appIsBusy(true)
      $.form({
        method: 'GET',
        url: options.headingsUrl,
        success: function (data) {
          setHeadings(data)
          appIsBusy(false)
        }
      })
    }

    self.mode = MODE_VERIFY
    self.$filedrop = $('<div>', {'class': 'file-drop-area'})

    self.$filedrop.html('Drop csv file here')

    self.$filedrop
      .on('dragenter', function (event) {
        event.originalEvent.preventDefault()
        event.originalEvent.stopPropagation()
      })
      .on('dragover', function (event) {
        event.originalEvent.preventDefault()
        event.originalEvent.stopPropagation()
      })
      .on('drop', function (event) {
        var file
        var ev = event.originalEvent
        var reader

        ev.preventDefault()
        ev.stopPropagation()

        if (!ev.dataTransfer || !ev.dataTransfer.files.length) {
          return
        }

        file = ev.dataTransfer.files[0]
        reader = new FileReader()

        self.filename = file.name

        setMode(MODE_VERIFY)

        reader.onload = function (event) {
          var $e, $f, $g
          var i, n
          var columns
          var count
          var importColumnCount = 0
          var rowCount

          self.csvData = csvToArray(event.target.result)
          rowCount = self.csvData.length

          for (i = 0; i < rowCount; i++) {
            count = self.csvData[i].length
            if (count > importColumnCount) {
              importColumnCount = count
            }
          }

          self.$csvTable = $('<table>', {'class': 'table table-bordered small table-csv'})

          $e = $('<tr>').append($('<th>').html('ignore'))

          for (i = 0; i < importColumnCount; i++) {
            $f = $('<th>', {'data-column-index': i})
            $f
              .on('dragover', function (event) {
                event.originalEvent.preventDefault()
              })
              .on('drop', function (event) {
                var ev = event.originalEvent
                ev.preventDefault()
                ev.target.appendChild(document.getElementById(ev.dataTransfer.getData('text')))

                if (self.settings.afterColumnHeadingDropFn !== null) {
                  self.settings.afterColumnHeadingDropFn(self.getCsvColumns(), self.csvData)
                }
                setMode(MODE_VERIFY)
              })
            $e.append($f)
          }

          self.$csvTable.append($('<thead>').append($e))

          $g = $('<tbody>')

          for (i = 0; i < rowCount; i++) {
            if (self.csvData[i] !== undefined) {
              columns = self.csvData[i]

              $e = $('<tr>', {'data-ignore': '0', 'data-row-index': i, 'id': 'csvrow-' + i})
              $f = $('<input>', {'data-row-index': i, 'type': 'checkbox'})
              $f
                .on('change', function (ev) {
                  ev.originalEvent.preventDefault()
                  setMode(MODE_VERIFY)

                  var $e = self.$csvTable.find('tr[data-row-index="' + $(this).attr('data-row-index') + '"]')
                  var ignore

                  if ($(this).is(':checked')) {
                    $e.addClass('row-ignore')
                    $e.attr('data-ignore', '1')
                    ignore = true
                  } else {
                    $e.removeClass('row-ignore')
                    $e.attr('data-ignore', '0')
                    ignore = false
                  }

                  if (self.settings.onSetIgnoreFn) {
                    self.settings.onSetIgnoreFn($(this).attr('data-row-index'), ignore)
                  }
                })

              $e.append($('<td>').append($f))

              for (n = 0; n < importColumnCount; n++) {
                $f = $('<td>', {'data-ignore': '0'})
                if (undefined === columns[n]) {
                  $f.html('-')
                } else {
                  $f.html(columns[n])
                }
                $e.append($f)
              }
              $g.append($e)
            }
          }

          self.$csvTable.append($g)

          self.$filedrop.hide()

          $(self).append(self.$headingsContainer)
          $e = $('<div>', {'class': 'table-responsive'})
          $e.append(self.$csvTable)
          $(self).append($e)

          if (self.settings.afterTableFn !== null) {
            self.settings.afterTableFn(self.csvData)
          }
          $(self).append(self.$footer)
        }
        reader.readAsText(file)
      }
    )

    self.$buttonUpload = $('<button>', {'class': 'btn btn-default', 'data-toggle': 'tooltip', 'title': 'Varifies the format of the CSV Data.'}).html('verify')
    self.$buttonUpload.on('click', function (event) {
      var colSpec
      var cont
      var csvColumns = []
      var csvRow
      var csvRows = []
      var data
      var datum
      var extra = null

      self.$csvTable.find('tr').removeClass('row-accepted row-rejected')
      self.$csvTable.find('tr').popover('destroy')

      csvColumns = self.getCsvColumns()

      self.$csvTable.find('tbody tr[data-ignore="0"]').each(function (i, element) {
        csvRow = []
        csvRow.push($(element).attr('data-row-index'))
        $(element).children().each(function (j, element) {
          if (csvColumns[j] !== '') {
            datum = $(element).html().trim()
            csvRow.push(datum)
          }
        })
        csvRows.push(csvRow)
      })

      colSpec = csvColumns.filter(function (current) {
        return current !== ''
      })

      extra = {}
      cont = true

      if (self.settings.beforeUploadFn !== null) {
        cont = self.settings.beforeUploadFn(extra)
      }

      if (cont) {
        data = {
          'data': {
            'columns': colSpec,
            'rows': csvRows
          },
          'extra': extra,
          'mode': self.mode
        }

        appIsBusy(true)

        $.form({
          contentType: 'application/json',
          data: JSON.stringify(data),
          method: 'POST',
          url: self.settings.uploadUrl,
          success: function (result) {
            var i, j, m, n
            var cssClass
            var errors
            var row

            appIsBusy(false)

            for (i = 0, j = result.rejects.length; i < j; i++) {
              row = result.rejects[i]
              $('#csvrow-' + row.index).addClass('row-rejected')
              errors = ''
              for (m = 0, n = row.error_messages.length; m < n; m++) {
                errors += '<p>' + row.error_messages[m] + '</p>'
              }
              $('#csvrow-' + row.index).popover({
                content: errors,
                html: true,
                placement: 'auto',
                trigger: 'hover'
              })
            }

            cssClass = 'row-accepted'

            if (result.rejects.length === 0) {
              if (self.mode === MODE_UPLOAD) {
                cssClass = 'row-uploaded'
                setMode(MODE_DISABLED)
              } else {
                cssClass = 'row-accepted'
                setMode(MODE_UPLOAD)
              }
            }

            for (i = 0; i < result.acceptedCount; i++) {
              row = result.accepts[i]
              $('#csvrow-' + row.index).addClass(cssClass)
            }
          }
        })
      }
    })

    self.$footer = $('<div>', {'class': 'form-inline text-center'})

    if (self.settings.uiControlsFn !== null) {
      self.$footer.append(self.settings.uiControlsFn())
    }

    self.$footer.append(self.$buttonUpload)
    self.$footer.append($e)

    function setHeadings (headings) {
      var $e
      var i, j

      self.headings = headings

      self.$headingsContainer = $('<div>', {'class': 'draggable-storage'})
      self.$headingsContainer
        .on('dragover', function (event) {
          event.originalEvent.preventDefault()
        })
        .on('drop', function (event) {
          var ev = event.originalEvent
          ev.preventDefault()
          self.$headingsContainer.append(document.getElementById(ev.dataTransfer.getData('text')))
          setMode(MODE_VERIFY)
        }
      )

      for (i = 0, j = headings.length; i < j; i++) {
        $e = $('<div>', {'class': 'draggable', 'data-column-index': i, 'data-column-name': headings[i].column, 'draggable': 'true', 'id': 'csvh_' + i})
        $e.on('dragstart', function (event) {
          event.originalEvent.dataTransfer.setData('text', event.target.id)
        })
        $e.html(headings[i].heading)
        self.$headingsContainer.append($e)
      }
    }

    function setMode (mode) {
      self.mode = mode

      switch (mode) {
        case MODE_DISABLED:
          self.$buttonUpload.html('Done')
          self.$buttonUpload.attr('disabled', 'disabled')
          self.find('input').attr('disabled', 'disabled')
          break
        case MODE_UPLOAD:
          self.$buttonUpload.html('Upload')
          break
        case MODE_VERIFY:
          self.$buttonUpload.html('Verify')
          break
      }
    }

    $(self).append(self.$filedrop)
  }
})(jQuery)

// http://www.bennadel.com/blog/1504-ask-ben-parsing-csv-strings-with-javascript-exec-regular-expression-command.htm
function csvToArray (strData, strDelimiter) {
  strDelimiter = (strDelimiter || ',')

  var objPattern = new RegExp(
    (
      // Delimiters.
      '(\\' + strDelimiter + '|\\r?\\n|\\r|^)' +
      // Quoted fields.
      '(?:\"([^\"]*(?:\"\"[^\"]*)*)\"|' +
      // Standard fields.
      '([^\"\\' + strDelimiter + '\\r\\n]*))'
    ),
    'gi'
  )
  var arrData = [[]]
  var arrMatches = null
  var strMatchedDelimiter
  var strMatchedValue

  while ((arrMatches = objPattern.exec(strData))) {
    strMatchedDelimiter = arrMatches[1]

    if (strMatchedDelimiter.length && (strMatchedDelimiter !== strDelimiter)) {
      arrData.push([])
    }
    // Now that we have our delimiter out of the way,
    // let's check to see which kind of value we
    // captured (quoted or unquoted).
    if (arrMatches[2]) {
      // We found a quoted value. When we capture
      // this value, unescape any double quotes.
      strMatchedValue = arrMatches[2].replace(new RegExp('\"\"', 'g'), '\"')
    } else {
      // We found a non-quoted value.
      strMatchedValue = arrMatches[3]
    }
    // Now that we have our value string, let's add
    // it to the data array.
    arrData[ arrData.length - 1 ].push(strMatchedValue)
  }
  // Return the parsed data.
  return (arrData)
}
