// @author Giomani Designs (Development Team)
//
var RowRenderer = {
  renderers: {
    dataFn: null,
    tableRowFns: []
  },
  render: function (rowsData, tableId) {
    var cssClasses = []
    var data = null
    var html = ''
    var fn
    var i, j
    var parts = []
    var row
    var rowElements

    for (var l = 0, m = rowsData.length; l < m; l++) {
      row = rowsData[i]

      for (i = 0, j = this.renderers.tableRowFns.length; i < j; i++) {
        fn = this.renderers.tableRowFns[i]
        rowElements = fn(row)
        cssClasses = cssClasses.concat(rowElements.classes)
        data = data.concat(rowElements.data)
      }

      j = cssClasses.length
      if (j !== 0) {
        parts.push('class="' + (cssClasses.join(' ')) + '"')
      }

      for (i = 0, j = data.length; i < j; i++) {
        parts.push = 'data="' + (data.join(' ')) + '"'
      }

      j = parts.length
      if (j !== 0) {
        html = '<tr ' + (parts.join(' ')) + '>'
      } else {
        html = '<tr>'
      }

      html += this.renderers.dataFn(row, i)

      html += '</tr>'
    }
    document.getElementById(tableId).innerHTML = html
  },
  tableElementId: '',
  addRowRenderer: function (r) {
    this.renderers.tableRowFns.push(r)
  },
  setDataRenderer: function (r) {
    this.renderers.dataFn = r
  },
  setTableId: function (r) {
    this.tableElementId = r
  }
}
