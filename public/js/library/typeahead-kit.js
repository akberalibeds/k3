// @author Giomani Designs
(function ($) {
  $.fn.typeaheadKit = function (options) {
    var self = this

    this.settings = $.extend({
      display: null,
      minLength: 3,
      name: '',
      objectIdProperty: 'id',
      pagingControlsId: 'paging-controls',
      pagingControlsContainerId: 'paging-controls-container',
      querySubjectFn: null,
      queryUrl: null,
      resultsLimit: 5,
      resultsElement: null,
      rowButtonClickFn: null,
      rowClickFn: null,
      rowElementFn: null,
      spinner: '#typeahead-spinner'
    }, options)

    this.bloodhound = new Bloodhound({
      datumTokenizer: Bloodhound.tokenizers.obj.whitespace('value'),
      queryTokenizer: Bloodhound.tokenizers.whitespace,
      remote: {
        url: self.settings.queryUrl,
        prepare: function (query, settings) {
          settings.url = settings.url.replace('%QUERY', encodeURI(query))
          if (self.settings.querySubjectFn !== null) {
            settings.url = settings.url.replace('%SUBJECT', encodeURI(self.settings.querySubjectFn()))
          }
          return settings
        }
      }
    })

    $(this).typeahead(
      {
        classNames: {
          hint: 'form-control typeahead',
          input: 'form-control typeahead'
        },
        hint: true,
        highlight: true,
        minLength: self.settings.minLength,
        rateLimitBy: 'throttle',
        rateLimitWait: 700,
        ttl: 3600
      },
      {
        display: self.settings.display,
        limit: self.settings.resultsLimit,
        name: self.settings.name,
        source: self.bloodhound
      }
    )

    $(self.settings.resultsElement).children('tr').on('click', function (e) {
      e.stopImmediatePropagation()
      self.settings.rowClickFn($(e.delegateTarget).attr('data-object-id'))
    })

    this.tableCache = null

    this.resetTable = function () {
      $(self.settings.resultsElement).html(self.tableCache.table)
      // $(self.settings.resultsElement).children('tr').on('click', function (e) {
      //   self.settings.rowClickFn($(e.delegateTarget).attr('data-object-id'))
      // })
      $('#' + self.settings.pagingControlsContainerId).html(self.tableCache.paging)
      self.val('')
    }

    $(this).bind('typeahead:asyncreceive', function (e) {
      // $(self.settings.spinner).html('')
      spinnerIsOn(false)
    })

    $(this).bind('typeahead:asyncrequest', function (e) {
      // $(self.settings.spinner).html('<i class="busy-spinner fa fa-cog fa-2x" aria-hidden="true"></i>')
      spinnerIsOn(true)
    })

    if (self.settings.resultsElement !== null) {
      $(this).bind('typeahead:render', function (e, suggestions) {
        if (arguments.length > 1) {
          fillTable(arguments)
        }
      })
    }

    if (self.settings.rowClickFn !== null) {
      $(this).bind('typeahead:select', function (e, suggestion) {
        e.stopImmediatePropagation()
        self.settings.rowClickFn(suggestion, e)
      })
    }

    function fillTable (suggestions) {
      var $button
      var $row

      if (self.tableCache === null) {
        self.tableCache = {}
        self.tableCache.table = $(self.settings.resultsElement).children('tr').detach()
        self.tableCache.paging = $('#' + self.settings.pagingControlsId).detach()
      }

      $(self.settings.resultsElement).empty()

      $button = $('<button>', {class: 'btn btn-default', id: 'typeahead-back-button'})
      $button.append('Back to list')
      $button.on('click', function (e) {
        self.resetTable(e)
      })

      $('#' + self.settings.pagingControlsContainerId).html($button)

      for (var i = 1, j = suggestions.length; i < j; i++) {
        var object = suggestions[i]
        $row = $('<tr>', {class: 'row-clickable', 'data-object-id': object[self.settings.objectIdProperty]})
        $row.on('click', function (e) {
          if (self.settings.rowButtonClickFn !== null && e.originalEvent.target.localName === 'button') {
            self.settings.rowButtonClickFn(object)
          } else {
            self.settings.rowClickFn(object)
          }
        })
        if (self.settings.rowElementFn) {
          $row.html(self.settings.rowElementFn(object))
        }
        $(self.settings.resultsElement).append($row)
      }
    }
    function spinnerIsOn (p) {
      if (p) {
        $(self.settings.spinner).addClass('search-spinner')
      } else {
        $(self.settings.spinner).removeClass('search-spinner')
      }
    }
    return this
  }
})(jQuery)
