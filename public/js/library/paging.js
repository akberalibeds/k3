// @author Giomani Designs (Development Team)
//
//
$.paging = function (options) {
  var pageCache = []
  var self = this
  this.settings = $.extend({
    firstPage: 1,
    objectIdProperty: 'id',
    pagingControlsId: 'paging-controls',
    pagingControlsContainerId: 'paging-controls-container',
    resultCounts: null,
    resultsElement: null,
    rowButtonClickFn: null,
    rowClickFn: null,
    rowElementFn: null,
    rowSelectedClass: null,
    selectedCount: null,
    url: null
  }, options)
  this.selections = []
  this.fetchPage = function (pageNumber) {
    $('#' + self.settings.pagingControlsId).empty()
    $(self.settings.resultCounts).empty()
    $(self.settings.resultsElement).empty()
    appIsBusy(true)
    $.form({
      'contentType': 'application/json',
      'method': 'GET',
      'url': self.settings.url + '?page=' + pageNumber,
      'success': function (pagingData) {
        drawData(pagingData)
        appIsBusy(false)
      },
      'error': function () {
        appIsBusy(false)
        console.log('there was an error')
      }
    })
  }
  this.getSelected = function () {
    return self.selections
  }
  this.showSelectedCounts = function () {
    $(self.settings.selectedCount).html(self.selections.length)
  }
  this.fetchPage(self.settings.firstPage)
  this.showSelectedCounts(self.settings.firstPage)
  function drawData (pagingData) {
    var i, j
    var checked
    var className
    var html
    var page0
    var pageN
    var row
    var rowId
    var rows
    pageCache = pagingData
    $(self.settings.resultCounts).html('showing ' + (pagingData.from ? pagingData.from : 0) + ' to ' + (pagingData.to ? pagingData.to : 0) + ' of ' + (pagingData.total ? pagingData.total : 0))
    // TODO: needs making better
    page0 = pagingData.current_page > 4 ? pagingData.current_page - 4 : 1
    pageN = pagingData.last_page - pagingData.current_page > 4 ? page0 + 8 : pagingData.last_page
    html = ''
    html += '<ul class="pagination" id="' + self.settings.pagingControlsId + '">'
    if (pagingData.current_page === 1) {
      html += '<li class="disabled">'
      html += '<span>'
      html += '&laquo;'
      html += '</span>'
    } else {
      html += '<li>'
      html += '<a data-page-number="' + (page0 - 1) + '" href="#" onclick="">'
      html += '&laquo;'
      html += '</a>'
    }
    html += '</li>'
    for (i = page0, j = pageN + 1; i < j; i++) {
      if (pagingData.current_page === i) {
        html += '<li class="active">'
        html += '<span>'
        html += i
        html += '</span>'
      } else {
        html += '<li>'
        html += '<a data-page-number="' + i + '" href="#">'
        html += i
        html += '</a>'
      }
      html += '</li>'
    }
    if (pagingData.current_page === pagingData.last_page) {
      html += '<li class="disabled">'
      html += '<span>'
      html += '&raquo;'
      html += '</span>'
    } else {
      html += '<li>'
      html += '<a data-page-number="' + (pageN + 1) + '" href="#">'
      html += '&raquo;'
      html += '</a>'
    }
    html += '</li>'
    html += '</ul>'
    $('#' + self.settings.pagingControlsContainerId).html(html)
    $('#' + self.settings.pagingControlsId + ' a').on('click', function (e) {
      self.fetchPage($(this).attr('data-page-number'))
    })
    html = ''
    rows = pagingData.data
    for (i = 0, j = rows.length; i < j; i++) {
      row = rows[i]
      if (self.selections.indexOf('' + getRowId(row)) !== -1) {
        checked = ' checked'
        className = ' ' + self.settings.rowSelectedClass
      } else {
        checked = ''
        className = ''
      }
      html += '<tr class="row-clickable' + className + '" data-page-cache-row="' + i + '" data-row-id="' + getRowId(row) + '">'
      if (self.settings.rowSelectedClass !== null) {
        html += '<td data-evignore><input data-row-id="' + getRowId(row) + '" type="checkbox"' + checked + '></td>'
      }
      html += self.settings.rowElementFn(row)
      html += '</tr>'
    }
    $(self.settings.resultsElement).html(html)
    $(self.settings.resultsElement).find('input[type="checkbox"]').on('change', function (e) {
      e.stopPropagation()
      rowId = $(this).attr('data-row-id')
      row = $(self.settings.resultsElement).children('tr[data-row-id="' + rowId + '"]')
      i = self.selections.indexOf('' + rowId)
      if (i !== -1) {
        $(row).removeClass(self.settings.rowSelectedClass)
        self.selections.splice(i, 1)
      } else {
        $(row).addClass(self.settings.rowSelectedClass)
        self.selections.push(rowId)
      }
      self.showSelectedCounts()
    })
    if (self.settings.rowClickFn !== null) {
      $(self.settings.resultsElement).find('tr').each(function (i, obj) {
        $(obj).find('td:not([data-evignore])').on('click', function (e) {
          e.preventDefault()
          rowId = $(this).parent().attr('data-row-id')
          row = pageCache.data[$(this).parent().attr('data-page-cache-row')]
          if (self.settings.rowButtonClickFn !== null && e.originalEvent.target.localName === 'button') {
            self.settings.rowButtonClickFn(row)
          } else {
            self.settings.rowClickFn(rowId, row)
          }
        })
      })
    }
  }
  function getRowId (r) {
    return r[self.settings.objectIdProperty]
  }
  return this
}
