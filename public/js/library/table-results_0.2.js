// Giomani Designs (Development Team)
//
var tableResults = {
  attributes: {
    delay: 750,
    filterDefault: [],
    objectIdProperty: 'id',
    queryable: true,
    rowButtonClickFn: null,
    rowClickFn: null,
    rowRendererFn: null,
    selectable: false,
    showCounts: true,
    showPaging: true,
    selectControls: true,
    url: ''
  },
  containers: {
    anyBegin: 'any-begin',
    counts: 'result-counts',
    filters: 'filters',
    paging: 'paging-controls-container',
    perPage: 'per-page-count',
    results: 'result-container',
    resultsTable: 'table-results-container',
    selectedCount: 'selected-count',
    spinner: 'search-spinner'
  },
  controls: {
    queryInput: 'search-input',
    subjectInput: 'subject-select'
  },
  keyDelayTimeout: null,
  properties: {
    columnCount: 0,
    columns: [],
    currentUrl: null,
    filters: new Map(),
    nextPrevPageUrl: '',
    pagingCache: '',
    parameters: null,
    resultsCache: new Map(),
    selectAllContainer: 'select-all-container',
    selectAllButton: 'select-all-button',
    selectedCache: new Map(),
    showingSelected: false,
    sortables: new Map(),
    limit: 10
  },
  //
  addSelectAllButton: function () {
    var container = document.getElementById(this.properties.selectAllContainer)
    if (container !== null) {
      container.innerHTML = '<button class="btn btn-mini" id="' + this.properties.selectAllButton + '" data-state="all" onclick="tableResults.selectAll(event)" style="width:22px">all</button>'
    }
  },
  // Clear The Cache
  clearCache: function () {
    this.properties.resultsCache = new Map()
    this.clearSelection
  },
  //Clear The filters
  clearFilters: function (e) {
    this.properties.filters.forEach(function (value, key) {
      document.querySelectorAll('[name="' + key + '"]').forEach(function (f) {
        f.checked = false
      })
    })
    this.properties.filters.clear()
    this.reset()
  },
  // Clear any Selections
  clearSelection: function (e) {
    document.querySelectorAll('#' + this.containers.results + ' :checked').forEach(function (obj) {
      obj.checked = false
    })
    this.properties.selectedCache.clear()
    this.updateSelectedCount()
  },
  // Hide any columns
  columnHiding: function () {
    var column
    var columns = document.getElementById(this.containers.resultsTable).querySelectorAll('tr th')
    for (var i = 0, j = columns.length; i < j; i++) {
      column = columns[i]
      column.setAttribute('data-column-index', i + 1)
      column.setAttribute('data-column-hidden', 'false')
    }
  },
  columnVisibility: function () {
    var a = 0
    var checked
    var column
    var columns = document.getElementById(this.containers.resultsTable).querySelectorAll('tr th')
    var html
    var lineCount = Math.ceil(this.properties.columnCount / 3)
    var l = 0
    if (this.attributes.selectable) {
      a = 1
    }
    html = '<div class="row">'
    for (var n = 0; n < 3; n++) {
      html += '<div class="col-md-4">'
      for (var i = 0; i < lineCount; i++) {
        column = columns[a + l++]
        checked = column.getAttribute('data-column-hidden') === 'true' ? '' : ' checked'
        html += '<div class="checkbox"><label><input onchange="tableResults.hideColumn(' + l + ')" type="checkbox"' + checked + ' value="' + i + '">' + column.textContent + '</label></div>'
        if (l === this.properties.columnCount) {
          i = Infinity
        }
      }
      html += '</div>'
    }
    html += '</div>'
    var dialog = bootbox.dialog({
      backdrop: true,
      message: html,
      title: 'Column Visibility'
    })
  },
  // Fetch any queries on the system
  doQuery: function () {
    var c
    var q
    this.limit = 25;
    if(document.getElementById('result-limit')) {
    	 this.limit = document.getElementById('result-limit').value
    }
    this.properties.parameters = new Map()
    if (this.attributes.queryable) {
      q = document.getElementById(this.controls.queryInput).value
      if (q !== '') {
        this.properties.parameters.set('query', document.getElementById(this.controls.subjectInput).value + '|' + q)
      }
    }
    if (this.properties.filters.size > 0) {
      c = []
      this.properties.filters.forEach(function (value, key) {
        if (value !== '') {
          c.push(key + '|' + value)
        }
      })
      if (c.length > 0) {
        this.properties.parameters.set('filter', c.join(';'))
      }
    }
    if (this.properties.sortables.size > 0) {
      c = []
      this.properties.sortables.forEach(function (value, key) {
        c.push(key + '|' + value)
      })
      this.properties.parameters.set('order', c.join(';'))
    }
    this.getRows()
  },
  // Filter the Table based on checkbox selection
  filter: function (e, unset) {
    var name = e.target.name
    var value = e.target.value
    if (unset === undefined) {
      unset = false
    } else {
      unset = true
    }
    if (!unset && this.properties.filters.has(name)) {
      this.properties.filters.delete(name)
    } else {
      this.properties.filters.set(name, value)
    }
    this.doQuery()
  },
  get: function (attribute) {
    return this.attributes[attribute]
  },
  getRows: function (page) {
    var parameters = []
    page = page === undefined ? 1 : page
    this.properties.parameters.set('page', page)
    this.properties.parameters.set('limit',this.limit)
    this.properties.parameters.set('ab', document.getElementById(this.containers.anyBegin).value)
   
    this.properties.parameters.forEach(function (value, key) {
      parameters.push(key + '=' + value)
    })
    this.makeRequest(this.attributes.url + '?' + parameters.join('&'))
  },
  getSelected: function () {
    return this.properties.selectedCache
  },
  getSelectedIds: function () {
    var ids = []
    tableResults.getSelected().forEach(function (value, key) {
      ids.push(key)
    })
    return ids
  },
  getSortables: function () {
    var sorts = []
    for (var [key, value] of this.properties.sortables) {
      sorts.push(key + '|' + value)
    }
    return sorts.join(';')
  },
  gotoPage: function (p) {
    this.getRows(p)
  },
  hideColumn: function (columnIndex) {
    var display
    var e
    var hide
    if (this.attributes.selectable) {
      columnIndex++
    }
    e = document.getElementById(this.containers.resultsTable).querySelector('tr th:nth-child(' + columnIndex + ')')
    hide = e.getAttribute('data-column-hidden') === 'true' ? 'false' : 'true'
    display = hide === 'true' ? 'none' : 'table-cell'
    e.style.display = display
    e.setAttribute('data-column-hidden', hide)
    e = document.getElementById(this.containers.resultsTable).querySelectorAll('tr td:nth-child(' + columnIndex + ')')
    for (var i = 0, j = e.length; i < j; i++) {
      e[i].style.display = display
      e[i].setAttribute('data-column-hidden', hide)
    }
  },
  hideSelected: function (event) {
    this.renderRows(this.properties.resultsCache)
    document.getElementById(this.containers.paging).innerHTML = this.properties.pagingCache
    this.properties.showingSelected = false
  },
  init: function () {
    this.clearCache()
    // this.attributes.url = cleanUrl(this.attributes.url)
    // this.properties.currentUrl = cleanUrl(this.properties.currentUrl)
    this.properties.columnCount = document.getElementById(this.containers.results).parentElement.querySelectorAll('th').length
    if (this.attributes.selectable) {
      this.properties.columnCount--
    }
    this.nextPrevPageUrl = this.attributes.url
    if (this.attributes.selectable) {
      this.addSelectAllButton()
    }
    this.setFilters()
    this.columnHiding()
    this.sorting()
    this.doQuery()
  },
  makeRequest: function (url) {
    var self = this
    this.spinnerIsOn(true)
    $.form({
      method: 'GET',
      url: cleanUrl(url),
      success: function (response) {
        self.properties.currentUrl = url
        self.properties.resultsCache.clear()
        for (var i = 0, j = response.data.length; i < j; i++) {
          self.properties.resultsCache.set('' + response.data[i][self.attributes.objectIdProperty], response.data[i])
        }
        self.renderCounts(response)
        self.renderPaging(response)
        self.renderRows(self.properties.resultsCache)
        self.spinnerIsOn(false)
      },
      error: function (response) {
        console.log(response)
        self.spinnerIsOn(false)
      }
    })
  },
  queryChange: function (e) {
    var self = this
    window.clearTimeout(this.keyDelayTimeout)
    this.keyDelayTimeout = window.setTimeout(function () { self.doQuery() }, self.attributes.delay)
  },
  queryFilterChange: function (e) {
    var q = document.getElementById(this.controls.queryInput).value
    if (q !== '') {
      this.doQuery()
    }
  },
  refresh: function () {
    if (this.properties.currentUrl !== null) {
      this.makeRequest(this.properties.currentUrl)
    }
  },
  renderCounts: function (results) {
    if (this.attributes.showCounts === true) {
      document.getElementById(this.containers.counts).innerHTML = (results.from === null ? 0 : results.from) + ' to ' + (results.to === null ? 0 : results.to) + ' of ' + results.total
      document.getElementById(this.containers.perPage).innerHTML = results.per_page
    }
  },
  renderPaging: function (results) {
    if (this.attributes.showPaging) {
      var i, j
      var html
      var p
      i = results.current_page - 4 > 1 ? results.current_page - 4 : 1
      j = i + 8 < results.last_page ? i + 8 : results.last_page
      i--
      html = '<nav><ul class="pagination">'
      if (results.prev_page_url === null) {
        html += '<li class="disabled not-allowed"><a><i class="fa fa-step-backward" aria-hidden="true"></i></a></li>'
        html += '<li class="disabled not-allowed"><a><i class="fa fa-chevron-left" aria-hidden="true"></i></a></li>'
      } else {
        html += '<li><a class="link" onclick="tableResults.gotoPage(1)"><i class="fa fa-step-backward" aria-hidden="true"></i></a></li>'
        html += '<li><a class="link" onclick="tableResults.gotoPage(' + (results.current_page - 1) + ')"><i class="fa fa-chevron-left" aria-hidden="true"></i></a></li>'
      }
      for (;i < j; i++) {
        p = i + 1
        if (p === results.current_page) {
          html += '<li class="active"><a>' + p + '</a></li>'
        } else {
          html += '<li><a class="link" onclick="tableResults.gotoPage(' + p + ')">' + p + '</a></li>'
        }
      }
      if (results.next_page_url === null) {
        html += '<li class="disabled not-allowed"><a><i class="fa fa-chevron-right" aria-hidden="true"></i></a></li>'
        html += '<li class="disabled not-allowed"><a><i class="fa fa-step-forward" aria-hidden="true"></i></a></li>'
      } else {
        html += '<li><a class="link" onclick="tableResults.gotoPage(' + (results.current_page + 1) + ')"><i class="fa fa-chevron-right" aria-hidden="true"></i></a></li>'
        html += '<li><a class="link" onclick="tableResults.gotoPage(' + (results.last_page) + ')"><i class="fa fa-step-forward" aria-hidden="true"></i></a></li>'
      }
      html += '</ul></nav>'
      document.getElementById(this.containers.paging).innerHTML = html
    }
  },
  renderRows: function (rows) {
    var button = document.getElementById(this.properties.selectAllButton)
    var checkedCount = 0
    var html = ''
    var self = this
    var state = 'all'
    if(rows.size === 0) {
      html += '<tr><td class="text-center" colspan="' + this.properties.columnCount + '">no results</td></tr>'
    } else {
      rows.forEach(function (row, key, map) {
        // Row Creations
        html += '<tr data-row-id="' + key + '"' + (self.attributes.clickFn !== null ? ' onclick="tableResults.rowClicked(event)"' : '') + '>'
        // Selectable ALL
        if (self.attributes.selectable) {
          html += '<td><input'
          if (self.properties.selectedCache.has(key)) {
            html += ' checked'
            checkedCount++
          }
          html += ' type="checkbox" value="' + key + '">'
        }

        html += self.attributes.rowRendererFn(row) + '</tr>'
      })
    }
    document.getElementById(this.containers.results).innerHTML = html
    if (checkedCount === rows.size) {
      state = 'none'
    }
    if (button !== null) {
      button.setAttribute('data-state', state)
      button.innerHTML = state
    }
  },
  reset: function () {
    document.getElementById(this.controls.queryInput).value = ''
    this.nextPrevPageUrl = this.attributes.url
    this.resetSortables()
    this.doQuery()
  },
  resetSortables: function () {
    var sortables = document.querySelectorAll('[data-sort-by]')
    for (var i = 0, j = sortables.length; i < j; i++) {
      sortables[i].innerHTML = '<i class="fa fa-sort sort-inactive" aria-hidden="true"></i>'
      sortables[i].setAttribute('data-sort-order', 0)
    }
    this.properties.sortables.clear()
  },
  rowClicked: function (e) {
    var row
  
    if (e.target.tagName === 'INPUT') {
      var ot = e.target
      // row = this.properties.resultsCache[ot.value]
      if (ot.checked) {
        this.selectAdd(ot.value)
      } else {
        this.selectRemove(ot.value)
      }
    } else if (this.attributes.rowButtonClickFn !== null && e.target.tagName === 'BUTTON') {
      row = this.properties.resultsCache.get(e.currentTarget.attributes['data-row-id'].value)
      this.attributes.rowButtonClickFn(row)
    } else if (this.attributes.rowClickFn) {
      row = this.properties.resultsCache.get(e.currentTarget.attributes['data-row-id'].value)
      this.attributes.rowClickFn(row)
    }
  },
  selectAdd: function (objectdId) {
    var row = this.properties.resultsCache.get(objectdId)
    this.properties.selectedCache.set(objectdId, row)
    this.updateSelectedCount()
  },
  selectAll: function (event) {
    var button = document.getElementById(this.properties.selectAllButton)
    var t = document.getElementById(this.containers.results).querySelectorAll('tr td:first-child input[type="checkbox"]')
    var state = button.getAttribute('data-state') === 'all' ? 'none' : 'all'
    for (var i = 0, j = t.length; i < j; i++) {
      if (state === 'none') {
        this.selectAdd(t[i].value)
        t[i].setAttribute('checked', 'checked')
      } else {
        this.selectRemove(t[i].value)
        t[i].removeAttribute('checked')
      }
      button.setAttribute('data-state', state)
      button.innerHTML = state
    }
    this.updateSelectedCount()
  },
  selectRemove: function (objectdId) {
    this.properties.selectedCache.delete(objectdId)
    this.updateSelectedCount()
  },
  set: function (attribute, value) {
    if (Object.keys(this.attributes).indexOf(attribute) === -1) {
      throw new Error('Could not set attribute [' + attribute + '] - not found.')
    } else {
      this.attributes[attribute] = value
    }
  },
  setC: function (attributes) {
    var self = this
    Object.keys(attributes).forEach(function (attribute) {
      self.set(attribute, attributes[attribute])
    })
  },
  setFilters: function () {
    var filter
    for (var i = 0, j = this.attributes.filterDefault.length; i < j; i++) {
      filter = this.attributes.filterDefault[i]
      this.properties.filters.set(filter, document.getElementById('filter-' + filter).getAttribute('data-filter-default'))
    }
  },
  showSelected: function (e) {
    if (!this.properties.showingSelected) {
      this.properties.pagingCache = document.getElementById(this.containers.paging).innerHTML
    }
    this.properties.showingSelected = true
    this.renderRows(this.properties.selectedCache)
    document.getElementById(this.containers.paging).innerHTML = '<button class="btn btn-default" onclick="tableResults.hideSelected()" role="button">back</button>'
  },
  sku2Active: function(event){
    var ids = []
    tableResults.getSelected().forEach(function (value, key) {
      ids.push(key)
    })
    if(ids.length === 0)
    {
      alert('No SKU given to change status')
      return ;
    } else {
      var self = this
      $.form({
        method: 'POST',
        url: "/admin/stock/sku-active",
        data: { ids },
        success: function (response){
          // alert("Activating the following IDs: " + ids)
          self.refresh();
          self.clearSelection(event);
        },
        error: function (response) {
          alert("Failed to change the following: " + ids)
          console.log(response)
        }
      })
    }    
  },
  sku2InActive: function(event){
    var ids = []
    tableResults.getSelected().forEach(function (value, key) {
      ids.push(key)
    })
    if(ids.length === 0)
    {
      alert('No SKU given to change status')
      return ;
    } else {
      var self = this
      $.form({
        method: 'POST',
        url: "/admin/stock/sku-inactive",
        data: { ids },  
        success: function (response){
          // alert("Deactivating the following IDs: " + ids)
          self.refresh();
          self.clearSelection(event);
        },
        error: function (response) {
          alert("Failed to change the following: " + ids)
          console.log(response)
        }
      })
    }    
  },
  sortBy: function (e) {
    var sortOrder = e.target.getAttribute('data-sort-order')
    this.resetSortables()
    if (sortOrder === '0') {
      sortOrder = 'asc'
      e.target.setAttribute('data-sort-order', 1)
      e.target.innerHTML = '<i class="fa fa-sort-asc sort-active" aria-hidden="true"></i>'
    } else if (sortOrder === '1') {
      sortOrder = 'desc'
      e.target.setAttribute('data-sort-order', 2)
      e.target.innerHTML = '<i class="fa fa-sort-desc sort-active" aria-hidden="true"></i>'
    } else {
      e.target.setAttribute('data-sort-order', 0)
      e.target.innerHTML = '<i class="fa fa-sort sort-inactive" aria-hidden="true"></i>'
      sortOrder = ''
    }
    if (sortOrder !== '') {
      this.properties.sortables.set(e.target.getAttribute('data-sort-by'), sortOrder)
    }
    this.doQuery()
  },
  sorting: function () {
    var field
    var sortables = document.querySelectorAll('[data-sortable]')
    for (var i = 0, j = sortables.length; i < j; i++) {
      field = sortables[i].getAttribute('data-sortable')
      sortables[i].innerHTML += '<button class="btn btn-mini" data-sort-by="' + field + '" data-sort-order="0" onclick="tableResults.sortBy(event)"><i class="fa fa-sort sort-inactive" aria-hidden="true"></i></button>'
    }
  },
  spinnerIsOn: function (p) {
    if (p) {
      document.getElementById(this.containers.spinner).className = 'search-spinner'
    } else {
      document.getElementById(this.containers.spinner).className = ''
    }
  },
  updateSelectedCount: function () {
    document.getElementById(this.containers.selectedCount).innerHTML = this.properties.selectedCache.size
  }
}
