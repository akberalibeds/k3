Import CSV
==========

__File:__ import-csv.js

Settings
--------
    afterColumnHeadingDropFn: function(cols, data)
     cols - array of column names to the csv columns
     data - array of the csv data
     Called after a column heading is dropped onto the table.

    afterTableFn: function(data)
     data - array of the csv data
     Called after the table has been rendered

    beforeUploadFn: function(data)
     data - array of the csv data
     returns boolean true - continue with upload, false - stop upload
     Called before the the data is uploaded.

    headings: h
     h - array of headings    

    headingsUrl:
     The URL to get the headings.  If the 'headings' is set then this will be ignored.

    onSetIgnoreFn(i int, b boolean)
     i - the row index
     b - boolean representing the checkbox state-info-bg
     Called when the 'ignore checkbox' is clicked line on the CSV table

    uiControlsFn:
     Called before the ui controls are rendered.  Give the developer a chance  to add some more.

    uploadUrl:
     The URL for the upload destination
