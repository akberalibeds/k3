// @author Giomani Designs (Development Team)
//
(function ($) {
  $.fn.uploadDrop = function (options) {
    var duid = this.attr('data-upload-identifier')
    var fileTypeIndex
    var mousePointer
    var self = this
    this.chunkSize = 0
    this.description = ''
    this.extension = ''
    this.fileData = null
    this.fileType = -1
    this.finalResponse = null
    this.imageId = ''
    this.progress = 0
    this.settings = $.extend({
      chunkSize: 131072, // needs to be a power of 2
      fileIsReadyFn: null,
      fileTypes: [],
      finishedFn: null,
      icons: [],
      messageElement: null,
      placeholder: 'Drop file here.',
      progressElement: null,
      url: 'upload'
    }, options)
    this.totalChunks = 0
    this.transmitChunks = function () {
      $(self.settings.progressElement).parent().css('background-color', '#F09622')
      self.progress = 0
      transmitChunk(0)
    }
    this.html('<p class="placeholder">' + this.settings.placeholder + '</p>')
    this.on('drop', function (e) {
      var eo
      var file
      e.preventDefault()
      e.stopPropagation()
      eo = e.originalEvent
      $(self.settings.messageElement).html('')
      if (eo.dataTransfer !== null) {
        file = eo.dataTransfer.files[0]
        if (file === undefined) {
          return
        }
        resetProgress()
        self.fileType = file.type
        fileTypeIndex = fileType(self.fileType)
        if (fileTypeIndex > -1) {
          self.filename = file.name.split('/').shift()
          self.extension = file.name.split('.').pop()
          self.html(self.settings.icons[fileTypeIndex])
          addFile(file)
        } else {
          message('bad file type')
        }
      }
    })
    this.on('dragover', function (ev) {
      ev.preventDefault()
      // ev.stopPropagation()
      //
      // event = ev.originalEvent
      //
      // if (event.dataTransfer !== null) {
      //   if (!isAcceptableFile(event.dataTransfer.files[0])) {
      //     this.css('border', 'not-allowed')
      //   }
      // }
    })
    function addFile (file) {
      self.progress = 0
      var reader = new FileReader()
      reader.addEventListener('load', function () {
        self.chunkSize = self.settings.chunkSize
        self.description = $('#description').val()
        self.fileData = reader.result.split(',').pop()
        self.fileId = Date.now() + '-' + Math.floor((Math.random() * 1000) + 1)
        self.totalChunks = Math.ceil(self.fileData.length / self.chunkSize)
        message('Ready.')
        if (self.settings.fileIsReadyFn) {
          self.settings.fileIsReadyFn()
        }
      }, false)
      reader.readAsDataURL(file)
    }
    function fileType (type) {
      return self.settings.fileTypes.indexOf(type)
    }
    function message (message) {
      $(self.settings.messageElement).html(message)
    }
    function progressUpload () {
      self.progress += ((1 / self.totalChunks) * 100)
      $(self.settings.progressElement).css('width', self.progress + '%')
      $(self.settings.progressElement).html(self.progress.toFixed(1) + '%')
    }
    function resetProgress () {
      $(self.settings.progressElement).html('0%')
      $(self.settings.progressElement).css('width', '0%')
    }
    function transmitChunk (chunkIndex) {
      var cEnd
      var cStart
      var data
      var fileChunk
      var url
      if (chunkIndex === self.totalChunks) {
        return
      }
      cStart = chunkIndex * self.chunkSize
      cEnd = cStart + self.chunkSize
      fileChunk = self.fileData.slice(cStart, cEnd)
      data = {
        'chunk': fileChunk,
        'chunkIndex': chunkIndex,
        'extension': self.extension,
        'fileId': self.fileId,
        'description': self.description,
        'name': self.filename,
        'totalChunks': self.totalChunks
      }
      url = cleanUrl(self.settings.url + '/read')
      $.form({
        data: data,
        method: 'POST',
        url: url,
        success: function (response) {
          progressUpload()
          if (chunkIndex + 1 === self.totalChunks) {
            if (self.settings.finishedFn !== null) {
              self.settings.finishedFn(response)
            }
          } else {
            transmitChunk(chunkIndex + 1)
          }
        }
      })
    }
    return this
  }
})(jQuery)
