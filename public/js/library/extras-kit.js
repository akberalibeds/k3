// @author Giomani Designs
//
function backButton () {
  window.history.back()
}
function cleanUrl (url) {
  url = url.replace('//', '/')
  url = url.replace('/\?', '\?')
  return url.replace(/\/+$/g, '')
}
function emptyNotSet (mightNotBeSet) {
  return (mightNotBeSet === null || mightNotBeSet === '') ? '[not set]' : mightNotBeSet
}
function getMuStash (stashName) {
  var html = document.querySelector('[data-stash-name="' + stashName + '"]')
  return html
}
function money (monies) {
  if (!isANum(monies)) {
    return '[nan]'
  }
  monies = parseFloat(monies).toFixed(2)
  return monies.toLocaleString()
}
function moneyWithColor (monies) {
  if (!isANum(monies)) {
    return '[nan]'
  }
  monies = parseFloat(monies).toFixed(2)
  return '<span class="' + (monies < 0 ? 'money-in-debit' : (monies > 0 ? 'money-in-credit' : '')) + '">' + monies.toLocaleString() + '</span>'
}
function moneyZeroDash (monies) {
  if (!isANum(monies)) {
    return '[nan]'
  }
  if (monies === 0) {
    return '-.--'
  }
  return money(monies)
}
function isANum (n) {
  return n !== null && !isNaN(n)
}
function nullIsADash (mightBeNull) {
  return mightBeNull === null || mightBeNull === undefined || mightBeNull === '' ? '-' : mightBeNull
}
function paymentIcon (paymentType) {
  var icon
  paymentType = paymentType === null ? '' : paymentType
  switch (paymentType.toLowerCase()) {
    case 'amazon' :
      icon = '<i class="fa fa-amazon" aria-hidden="true"></i>'
      break
    case 'card' :
      icon = '<i class="fa fa-credit-card" aria-hidden="true"></i>'
      break
    case 'cash' :
      icon = '<i class="fa fa-money" aria-hidden="true"></i>'
      break
    case 'credit' :
      icon = '<i class="fa fa-plus-square" aria-hidden="true"></i>'
      break
    case 'exchange' :
      icon = '<i class="fa fa-exchange" aria-hidden="true"></i>'
      break
    case 'paypal' :
      icon = '<i class="fa fa-paypal" aria-hidden="true"></i>'
      break
    default :
      icon = ' '
  }
  return icon + ' ' + paymentType
}
function postcode (postcode) {
  return (nullIsADash(postcode)).toUpperCase()
}
function tickOrCross (bool) {
  var b
  if (typeof bool === 'boolean') {
    b = bool
  } else {
    b = parseInt(bool, 10)
  }
  return (b ? '<i class="color-success fa fa-check" aria-hidden="true"></i>' : '<i class="color-danger fa fa-times" aria-hidden="true"></i>')
}
function yesOrNo (bool) {
  return (parseInt(bool, 10) ? 'Yes' : 'No')
}
