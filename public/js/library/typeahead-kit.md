Typeahead-Kit
=============

A _wrap-around_ for Twitter Typeahead https://twitter.github.io/typeahead.js/

Properties
----------
    display: string
    The key/property of the object to show in the typeahead dropdown.  Can be substituted for a function.

    name: string
    the name of this typeahead (needs to be unique)

    objectIdProperty: string
    The property to use

    pagingControls: string
    The element (jQuery selector) of the paging controls (if any).

    querySubjectFn: function()
    If the queryUrl has `%SUBJECT` this function is called.  The return value of this function is substituted for `%SUBJECT`.

    queryUrl: string
    The URL to search (eg `stock/search/item-code/%QUERY` or `stock/search/%SUBJECT/%QUERY`)

    resultsLimit: int
    The maximum number of suggestions in the typeahead dropdown.  If you're having issues with the suggestions not showing try reducing this value.

    resultsElement: null, // '#stock-item-table',

    rowClickFn: function()
    The function called when a selection is made from the typeahead dropdown

    rowElementFn: ?

    spinner: string
    The element (jQuery selector) to insert the spinner

Using %SUBJECT
--------------

example

    <select>
      <option value="orders.id;oid">Order ID</option>
      <option value="dBusinessName;dBusinessName">Name</option>
      <option value="email1;email1">Email</option>
    </select>

The option value has the format:

    "column1 [| column2 [| columns3 [ .. ]]];display"

`column1, column2, column3`

Are the columns that will be searched in the database.

`display`

The property that will be used in the suggestions.
