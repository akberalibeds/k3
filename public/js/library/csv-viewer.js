// @author Jason Fleet (jason.fleet@googlemail.com)
//
var csvViewer = {
  MODE: {
    DISABLED: 0,
    DONE: 'done',
    DOWNLOAD: 'download',
    UPLOAD: 'store',
    VERIFY: 'verify'
  },
  attributes: {
    downloadOnComplete: false,
    hasHeadings: true,
    headings: null,
    highlightSelect: true,
    inputs: null,
    profile: '',
    readyFn: null,
    selectHide: false,
    selectIgnore: false,
    url: null
  },
  containers: {
    csvTable: 'csv-view-table',
    csvImport: 'csv-import-container',
    headings: 'headings-container',
    importButton: 'import-button',
    singles: 'singles-container',
    uploadButton: 'upload-button',
    upload: 'upload-container'
  },
  properties: {
    originalSheet: null,
    sheet: null
  },
  mode: 0,
  //
  columnHide: function (columnIndex) {
    var columns = document.querySelectorAll('[data-col-index="' + columnIndex + '"]')
    for (var i = 0, j = columns.length; i < j; i++) {
      columns[i].setAttribute('hidden', 'hidden')
    }
  },
  dragHeading: function (e) {
    e.dataTransfer.setData('heading', e.originalTarget.attributes.id.value)
    e.dataTransfer.effectAllowed = 'move'
  },
  dragoverHeading: function (e) {
    e.preventDefault()
  },
  dropHeading: function (e) {
    var id
    var heading
    if (e.dataTransfer !== null) {
      id = e.dataTransfer.getData('heading')
      if (id !== '') {
        if (id === 'heading-d') {
          if (e.target.innerHTML !== '') {
            e.preventDefault()
            heading = document.getElementById(e.target.getAttribute('data-heading-id'))
            heading.className = 'draggable'
            heading.setAttribute('draggable', 'true')
            e.target.removeAttribute('data-heading-id')
            e.target.removeAttribute('data-column-name')
            e.target.innerHTML = ''
          }
        } else {
          if (e.target.innerHTML === '') {
            e.preventDefault()
            heading = document.getElementById(id)
            heading.className = 'draggable-not'
            heading.setAttribute('draggable', 'false')
            e.target.setAttribute('data-heading-id', id)
            e.target.setAttribute('data-column-name', heading.getAttribute('data-column-name'))
            e.target.innerHTML = document.getElementById(id).attributes['data-column-heading'].value
          }
        }
      }
    }
  },
  getColumnValues: function (columnName) {
    var i, j
    var columnIndex
    var columns
    var columnValues
    var row
    columns = document.getElementById('headings-row').childNodes
    for (i = 0, j = columns.length; i < j; i++) {
      if (columns[i].innerText === columnName) {
        j = 0
        columnIndex = i
      }
    }
    if (j === 0) {
      columnValues = []
      row = document.querySelectorAll('#csv-view-table tbody tr')[0]
      while (row) {
        if (!row.hasAttribute('hidden')) {
          columns = row.childNodes
          columnValues.push({ row: row.getAttribute('data-row-index'), text: columns[columnIndex].innerText })
        }
        row = row.nextSibling
      }
    }
    return columnValues
  },
  getCsv: function () {
    var i, j
    var columnIndexes = []
    var columns = []
    var node
    var row
    var rowIndex
    var rows = []
    var singles = []
    var tableColumns
    var tableSingles
    var tableRow
    tableSingles = document.getElementById('singles-container').querySelectorAll('input')
    for (i = 0, j = tableSingles.length; i < j; i++) {
      singles.push({
        name: tableSingles[i].getAttribute('name'),
        value: tableSingles[i].value
      })
    }
    tableColumns = document.getElementById('headings-row').childNodes
    for (i = 0, j = tableColumns.length; i < j; i++) {
      if ((tableColumns[i].innerText).trim() !== '') {
        columnIndexes.push(i)
        columns.push(tableColumns[i].getAttribute('data-column-name'))
      }
    }
    tableRow = document.querySelectorAll('#csv-view-table tbody tr')[0]
    while (tableRow) {
      tableRow.className.replace(/(?:^|\s)row-.*(?!\S)/g, '')
      if (!tableRow.hasAttribute('hidden')) {
        tableColumns = tableRow.childNodes
        row = []
        rowIndex = tableRow.getAttribute('data-row-index')
        for (i = 0, j = columnIndexes.length; i < j; i++) {
          node = tableColumns[columnIndexes[i]].firstChild
          if (node !== null) {
            if (node.nodeType === 1) {
              row.push(node.getElementsByTagName('input')[1].value)
            } else {
              row.push(node.nodeValue)
            }
          }
        }
        rows.push({ index: rowIndex, rowData: row })
      }
      tableRow = tableRow.nextSibling
    }
    return {
      columns: columns,
      rows: rows,
      singles: singles
    }
  },
  getHeadings: function () {
    var self = this
    var url
    appIsBusy(true)
    url = cleanUrl(this.attributes.url + '/headings/' + self.attributes.profile)
    $.form({
      method: 'GET',
      url: url,
      success: function (response) {
        self.attributes.headings = response.headings
        self.attributes.inputs = (response.inputs || [])
        self.attributes.singles = (response.singles || [])
        self.renderSingles()
        self.renderHeadings()
        self.attributes.readyFn()
        appIsBusy(false)
      },
      error: function (response) {
        appIsBusy(false)
      }
    })
  },
  getSendUrl: function () {
    return cleanUrl(this.attributes.url + '/store/' + this.attributes.profile)
  },
  init: function (sheet) {
    this.properties.originalSheet = sheet
    this.properties.sheet = sheet
    this.setMode(this.MODE.DISABLED)
    if (this.attributes.hasHeadings) {
      this.getHeadings()
    }
  },
  renderHeadings: function () {
    var i, j
    var heading
    var headings
    var html
    var name
    headings = this.attributes.headings
    html = ''
    for (i = 0, j = headings.length; i < j; i++) {
      heading = headings['' + i].heading
      name = headings['' + i].name
      html += '<div class="draggable" data-column-name="' + name + '"data-column-heading="' + heading + '" draggable="true" id="heading-' + i + '" ondragstart="csvViewer.dragHeading(event)">' + heading + '</div>'
    }
    html += '<div class="draggable" data-column-heading="" draggable="true" id="heading-d" ondragstart="csvViewer.dragHeading(event)"><i class="fa fa-trash" aria-hidden="true"></i></div>'
    document.getElementById(this.containers.headings).innerHTML = html
  },
  renderSingles: function () {
    var i, j
    var html
    var singles
    singles = this.attributes.singles
    html = ''
    for (i = 0, j = singles.length; i < j; i++) {
      html += '<label for="exampleInputFile">' + singles[i].label + '</label>&nbsp;'
      html += '<input type="' + singles[i].text + '" id="' + singles[i].name + '" name="' + singles[i].name + '">'
    }
    document.getElementById(this.containers.singles).innerHTML = html
  },
  renderTable: function () {
    var c, i, j, m, n
    var container
    var csvData = this.properties.sheet
    var csvDatum
    var datum
    var html
    var name
    c = 0
    container = document.getElementById(this.containers.csvTable)
    for (i = 0, j = csvData.length; i < j; i++) {
      m = csvData[i].length
      c = m > c ? m : c
    }
    html = '<thead>'
    if (this.attributes.selectIgnore || this.attributes.selectHide) {
      html += '<tr><td></td>'
      html += '<td></td>'.repeat(this.attributes.inputs.length)
      for (i = 0, j = c; i < j; i++) {
        html += '<td data-col-index="' + i + '">'
        html += '<button class="btn btn-link btn-xs" onclick="csvViewer.columnHide(' + i + ')"><i class="fa fa-minus-square" aria-hidden="true"></i></button>'
        html += '</td>'
      }
      html += '</tr>'
    }
    html += '<tr id="headings-row">'
    if (this.attributes.selectIgnore || this.attributes.selectHide) {
      html += '<th>&nbsp;</th>'
    }
    for (i = 0, j = this.attributes.inputs.length; i < j; i++) {
      html += '<th data-column-name="' + this.attributes.inputs['' + i].name + '">' + this.attributes.inputs['' + i].heading + '</th>'
    }
    for (i = 0, j = c; i < j; i++) {
      html += '<th data-col-index="' + i + '" ondragover="csvViewer.dragoverHeading(event)" ondrop="csvViewer.dropHeading(event)"></th>'
    }
    html += '</tr></thead><tbody>'
    for (i = 0, j = csvData.length; i < j; i++) {
      csvDatum = csvData[i]
      html += '<tr data-row-index="' + i + '">'
      if (this.attributes.selectIgnore || this.attributes.selectHide) {
        html += '<td><button class="btn btn-link btn-xs" onclick="csvViewer.rowHide(' + i + ')"><i class="fa fa-minus-square" aria-hidden="true"></i></button></td>'
      }
      for (m = 0, n = this.attributes.inputs.length; m < n; m++) {
        name = this.attributes.inputs['' + m].name
        html += '<td><input name="' + name + '-' + i + '" placeholder="item code" style="width:200px !important" type="text"></td>'
      }
      for (m = 0, n = csvDatum.length; m < n; m++) {
        datum = ''
        if (csvDatum[m] !== undefined && csvDatum[m] !== null) {
          if (typeof csvDatum[m] === 'object') {
            datum = csvDatum[m].date.split(' ').shift()
          } else {
            datum += csvDatum[m]
          }
        }
        html += '<td data-col-index="' + m + '">' + datum.trim() + '</td>'
      }
      for (n = c; m < n; m++) {
        html += '<td data-col-index="' + m + '"></td>'
      }
      html += '</tr>'
    }
    html += '</tbody>'
    container.innerHTML = html
    this.setMode(this.MODE.VERIFY)
  },
  resetView: function () {
    var i, j
    var elements
    elements = document.getElementById(this.containers.csvTable).querySelectorAll('[hidden]')
    for (i = 0, j = elements.length; i < j; i++) {
      elements[i].removeAttribute('hidden')
    }
    // elements = document.getElementById(this.containers.csvTable).querySelectorAll('th')
    // for (i = this.attributes.inputs.length + 1, j = elements.length; i < j; i++) {
    //   elements[i].innerHTML = ''
    // }
    // elements = document.getElementById(this.containers.headings).childNodes
    // for (i = 0, j = elements.length; i < j; i++) {
    //   elements[i].className = 'draggable'
    //   elements[i].setAttribute('draggable', 'true')
    // }
  },
  rowHide: function (rowIndex) {
    var row = document.querySelector('[data-row-index="' + rowIndex + '"]')
    row.setAttribute('hidden', 'hidden')
  },
  sendCsv: function () {
    var c
    var csvData = {
      data: this.getCsv(),
      mode: this.mode,
      profile: this.attributes.profile
    }
    console.log(csvData)
    var self = this
    var url = this.getSendUrl()
    $.form({
      data: csvData,
      method: 'POST',
      url: url,
      success: function (response) {
        var i, j
        var tableRow
        appIsBusy(false)
        c = 'row-accepted'
        if (response.rejects.count === 0) {
          if (self.mode === self.MODE.UPLOAD) {
            c = 'row-uploaded'
          }
        }
        for (i = 0, j = response.accepts.count; i < j; i++) {
          tableRow = document.querySelector('tr[data-row-index="' + (response.accepts.rows[i]).index + '"]')
          tableRow.className = tableRow.className.replace(/(?:^|\s)(row-.+?)(?!\S)/g, '')
          tableRow.className = tableRow.className += c
        }
        for (i = 0, j = response.rejects.count; i < j; i++) {
          tableRow = document.querySelector('tr[data-row-index="' + (response.rejects.rows[i]).index + '"]')
          tableRow.className = tableRow.className.replace(/(?:^|\s)(row-.+?)(?!\S)/g, '')
          tableRow.className = tableRow.className += 'row-rejected'
        }
        if (self.mode === self.MODE.UPLOAD) {
          self.setMode(self.MODE.DONE)
          if (self.attributes.downloadOnComplete) {

          }
        } else {
          self.setMode(self.MODE.UPLOAD)
        }
      },
      error: function (response) {
        console.log('error')
        appIsBusy(false)
      }
    })
  },
  set: function (attribute, value) {
    if (Object.keys(this.attributes).indexOf(attribute) === -1) {
      throw new Error('Attribute [' + attribute + '] not found.')
    } else {
      this.attributes[attribute] = value
    }
  },
  setC: function (attributes) {
    var self = this
    Object.keys(attributes).forEach(function (attribute) {
      self.set(attribute, attributes[attribute])
    })
  },
  setMode: function (mode) {
    var button = document.getElementById(this.containers.importButton)
    this.mode = mode
    switch (mode) {
      case this.MODE.DONE:
        button.innerHTML = 'Done'
        button.setAttribute('disabled', 'disabled')
        break
      case this.MODE.DISABLED:
        button.innerHTML = 'Ready'
        button.setAttribute('disabled', 'disabled')
        break
      case this.MODE.UPLOAD:
        button.innerHTML = 'Upload'
        button.removeAttribute('disabled')
        break
      case this.MODE.VERIFY:
        button.innerHTML = 'Verify'
        button.removeAttribute('disabled')
        break
    }
  }
}
//
// http://www.bennadel.com/blog/1504-ask-ben-parsing-csv-strings-with-javascript-exec-regular-expression-command.htm
function csvToArray (strData, strDelimiter) {
  strDelimiter = (strDelimiter || ',')
  var objPattern = new RegExp(
    (
      // Delimiters.
      '(\\' + strDelimiter + '|\\r?\\n|\\r|^)' +
      // Quoted fields.
      '(?:\"([^\"]*(?:\"\"[^\"]*)*)\"|' +
      // Standard fields.
      '([^\"\\' + strDelimiter + '\\r\\n]*))'
    ),
    'gi'
  )
  var arrData = [[]]
  var arrMatches = null
  var strMatchedDelimiter
  var strMatchedValue
  while ((arrMatches = objPattern.exec(strData))) {
    strMatchedDelimiter = arrMatches[1]
    if (strMatchedDelimiter.length && (strMatchedDelimiter !== strDelimiter)) {
      arrData.push([])
    }
    // Now that we have our delimiter out of the way,
    // let's check to see which kind of value we
    // captured (quoted or unquoted).
    if (arrMatches[2]) {
      // We found a quoted value. When we capture
      // this value, unescape any double quotes.
      strMatchedValue = arrMatches[2].replace(new RegExp('\"\"', 'g'), '\"')
    } else {
      // We found a non-quoted value.
      strMatchedValue = arrMatches[3]
    }
    // Now that we have our value string, let's add
    // it to the data array.
    arrData[ arrData.length - 1 ].push(strMatchedValue)
  }
  // Return the parsed data.
  return (arrData)
}
