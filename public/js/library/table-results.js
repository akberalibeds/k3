// Giomani Designs (Development Team)
//
var tableResults = {
  attributes: {
    delay: 750,
    objectIdProperty: 'id',
    queryable: true,
    queryUrl: '',
    rowButtonClickFn: null,
    rowClickFn: null,
    rowRendererFn: null,
    selectable: false,
    url: ''
  },
  containers: {
    paging: 'paging-controls-container',
    perPage: 'per-page-count',
    counts: 'result-counts',
    filters: 'filters',
    results: 'result-container',
    selectedCount: 'selected-count',
    spinner: 'search-spinner'
  },
  controls: {
    queryInput: 'search-input',
    subjectInput: 'subject-select'
  },
  keyDelayTimeout: null,
  properties: {
    filter: new Map(),
    nextPrevPageUrl: '',
    pagingCache: '',
    resultsCache: [],
    selectedCache: new Map(),
    showingSelected: false,
    sortables: new Map()
  },
  //
  clearCache: function () {
    this.properties.resultsCache = []
    this.properties.resultsCache.length = 0
    this.clearSelection
  },
  clearSelection: function (e) {
    document.querySelectorAll('#' + this.containers.results + ' :checked').forEach(function (obj) {
      obj.checked = false
    })
    this.properties.selectedCache.clear()
    document.getElementById(this.containers.selectedCount).innerHTML = this.properties.selectedCache.size
  },
  clearFilters: function (e) {
    this.properties.filter.forEach(function (value, key) {
      document.querySelectorAll('[name="' + key + '"]').forEach(function (f) {
        f.checked = false
      })
    })
    this.properties.filter.clear()
    this.reset()
  },
  doQuery: function () {
    var filter = []
    var q
    var url
    q = document.getElementById(this.controls.queryInput).value
    if (q !== '') {
      url = this.attributes.queryUrl
      url = url.replace('%SUBJECT', document.getElementById(this.controls.subjectInput).value)
      url = url.replace('%QUERY', q)
    } else {
      url = this.attributes.url
    }
    this.properties.filter.forEach(function (value, key) {
      filter.push(value)
    })

    url = url.replace('%FILTER', filter.join('|'))
    this.keyDelayTimeout = null
    this.nextPrevPageUrl = url
    this.getRows(url)
  },
  filter: function (e) {
    this.properties.filter.set(e.originalTarget.name, e.originalTarget.value)
    this.doQuery()
  },
  get: function (attribute) {
    return this.attributes[attribute]
  },
  getRows: function (url, page) {
    this.spinnerIsOn(true)
    page = page === undefined ? 1 : page
    var self = this
    $.form({
      method: 'GET',
      url: url + '?page=' + page,
      success: function (response) {
        self.properties.resultsCache = response.data
        self.render(response)
        self.spinnerIsOn(false)
      },
      error: function (response) {
        console.log(response)
        self.spinnerIsOn(false)
      }
    })
  },
  getSelected: function () {
    return this.properties.selectedCache
  },
  getSelectedIds: function () {
    var ids = []

    tableResults.getSelected().forEach(function (value, key) {
      ids.push(key)
    })
    return ids
  },
  getSortables: function () {
    var sorts = []
    for (var [key, value] of this.properties.sortables) {
      sorts.push(key + '|' + value)
    }
    return sorts.join(';')
  },
  gotoPage: function (p) {
    this.getRows(this.nextPrevPageUrl, p)
  },
  hideSelected: function (e) {
    this.renderRows(this.properties.resultsCache)
    document.getElementById(this.containers.paging).innerHTML = this.properties.pagingCache
    this.properties.showingSelected = false
  },
  init: function () {
    this.clearCache()
    this.nextPrevPageUrl = this.attributes.url
    if (this.attributes.sortable) {
      this.sorting()
    }
    this.doQuery()
  },
  queryChange: function (e) {
    var self = this
    window.clearTimeout(this.keyDelayTimeout)
    this.keyDelayTimeout = window.setTimeout(function () { self.doQuery() }, self.attributes.delay)
  },
  render: function (results) {
    this.renderCounts(results)
    this.renderPaging(results)
    this.renderRows(results.data)
  },
  renderCounts: function (results) {
    document.getElementById(this.containers.counts).innerHTML = (results.from === null ? 0 : results.from) + ' to ' + (results.to === null ? 0 : results.to) + ' of ' + results.total + ','
    document.getElementById(this.containers.perPage).innerHTML = results.per_page
  },
  renderPaging: function (results) {
    var html
    var i = results.current_page - 4 > 0 ? results.current_page - 4 : 1
    var j = i + 8 < results.last_page ? i + 8 : results.last_page
    html = '<nav><ul class="pagination">'
    if (results.prev_page_url === null) {
      html += '<li class="disabled not-allowed"><a><i class="fa fa-angle-double-left" aria-hidden="true"></i></a></li>'
    } else {
      html += '<li><a class="link" onclick="tableResults.gotoPage(' + (results.current_page - 1) + ')"><i class="fa fa-angle-double-left" aria-hidden="true"></i></a></li>'
    }
    for (;i < j; i++) {
      if (i === results.current_page) {
        html += '<li class="active"><a>' + i + '</a></li>'
      } else {
        html += '<li><a class="link" onclick="tableResults.gotoPage(' + i + ')">' + i + '</a></li>'
      }
    }
    if (results.next_page_url === null) {
      html += '<li class="disabled not-allowed"><a><i class="fa fa-angle-double-right" aria-hidden="true"></i></a></li>'
    } else {
      html += '<li><a class="link" onclick="tableResults.gotoPage(' + (results.current_page + 1) + ')"><i class="fa fa-angle-double-right" aria-hidden="true"></i></a></li>'
    }
    html += '</ul></nav>'
    document.getElementById(this.containers.paging).innerHTML = html
  },
  renderRows: function (rows) {
    var html = ''
    var self = this
    rows.forEach(function (row, i) {
      html += '<tr data-row-index="' + i + '" data-row-id="' + rowId + '"' + (self.attributes.clickFn !== null ? ' onclick="tableResults.rowClicked(event)"' : '') + '>'
      if (self.attributes.selectable) {
        var rowId = row[self.attributes.objectIdProperty]
        html += '<td><input' + (self.properties.selectedCache.has(rowId) ? ' checked' : '') + ' type="checkbox" value="' + i + '"></td>'
      }
      html += self.attributes.rowRendererFn(row) + '</tr>'
    })
    document.getElementById(this.containers.results).innerHTML = html
  },
  reset: function () {
    document.getElementById(this.controls.queryInput).value = ''
    this.nextPrevPageUrl = this.attributes.url
    this.doQuery()
  },
  rowClicked: function (e) {
    var row
    if (e.originalTarget.tagName === 'INPUT') {
      var ot = e.originalTarget
      row = this.properties.resultsCache[ot.value]
      if (ot.checked) {
        this.properties.selectedCache.set(row[this.attributes.objectIdProperty], row)
      } else {
        this.properties.selectedCache.delete(row[this.attributes.objectIdProperty])
      }
      document.getElementById(this.containers.selectedCount).innerHTML = this.properties.selectedCache.size
    } else if (this.attributes.rowButtonClickFn !== null && e.originalTarget.tagName === 'BUTTON') {
      row = this.properties.resultsCache[e.currentTarget.attributes['data-row-index'].value]
      this.attributes.rowButtonClickFn(row)
    } else {
      row = this.properties.resultsCache[e.currentTarget.attributes['data-row-index'].value]
      this.attributes.rowClickFn(row)
    }
  },
  set: function (attribute, value) {
    if (Object.keys(this.attributes).indexOf(attribute) === -1) {
      throw new Error('Could not set attribute [' + attribute + '] - not found.')
    } else {
      this.attributes[attribute] = value
    }
  },
  setC: function (attributes) {
    var self = this
    Object.keys(attributes).forEach(function (attribute) {
      self.set(attribute, attributes[attribute])
    })
  },
  showSelected: function (e) {
    if (!this.properties.showingSelected) {
      this.properties.pagingCache = document.getElementById(this.containers.paging).innerHTML
    }
    this.properties.showingSelected = true
    this.renderRows(this.properties.selectedCache)
    document.getElementById(this.containers.paging).innerHTML = '<button class="btn btn-default" onclick="tableResults.hideSelected()" role="button">back</button>'
  },
  sortBy: function (e) {
    this.properties.sortables.set(e.target.getAttribute('data-sort-by'), e.target.getAttribute('data-sort-order'))
  },
  sorting: function () {
    var field
    var sortables = document.querySelectorAll('[data-sort-by]')
    for (var i = 0, j = sortables.length; i < j; i++) {
      field = sortables[i].getAttribute('data-sort-by')
      sortables[i].innerHTML += ' <a onclick="tableResults.sortBy(event)"><i class="fa fa-sort sort-inactive" aria-hidden="true" data-sort-by="\'' + field + '\'" data-sort-order="0"></i>'
    }
  },
  spinnerIsOn: function (p) {
    if (p) {
      document.getElementById(this.containers.spinner).className = 'search-spinner'
    } else {
      document.getElementById(this.containers.spinner).className = ''
    }
  }
}
