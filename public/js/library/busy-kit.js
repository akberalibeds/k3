/** @author Giomani Designs **/
//
var $busy = $('<div>', {'class': 'busy-loading'}).html('<div class="busy-spinner"><div class="cube1"></div><div class="cube2"></div></div><div id="busy-loading-message"></div>')
$('body').prepend($busy)
$busy.hide()
//
function appIsBusy (busy, message) {
  if (undefined !== message)
  {
    $('#busy-loading-message').html(message)
  }
  if (busy) {
    $busy.show()
  } else {
    $busy.hide()
    $('#busy-loading-message').html('')
  }
}
