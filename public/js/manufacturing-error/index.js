// @author Giomani Designs (Development Team)
//
tableResults.setC({
  objectIdProperty: 'id',
  rowRendererFn: function (row) {
    return renderRow(row)
  },
  rowClickFn: function (row) {
	    return onClick(row)
	  },
  selectable: true,
  url: '/manufacturing-error'
})
tableResults.init()
function renderRow (error) {
  return '<td>' + nullIsADash(error.created_at) + '</td>' +
    '<td>' + nullIsADash(error.itemCode) + '</td>' +
    '<td>' + error.problem + '</td>' +
    '<td class="text-right">' + error.cost + '</td>' +
    '<td class="text-right">' + error.order_id + '</td>';
}
function onClick (error) {
		
	window.open('/manufacturing-error/'+error.id);

}
