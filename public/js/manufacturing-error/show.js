
//
$(function () {
  $('#driver-images').bxSlider({
    mode: 'horizontal',
    preloadImages: 'visible',
    slideWidth: 400,
    useCSS: true
  })

  $('#upload-drop').uploadDrop({
    chunkSize: 1024 * 1024,
    uploadUrl: '/manufacturing-error/upload/'+ERRORID
  })
})
