// @author Giomani Designs (Development Team)
//
tableResults.setC({
  objectIdProperty: 'id',
  rowClickFn: function (row) {
    clickOnRow(row)
  },
  rowRendererFn: function (row) {
    return renderRow(row)
  },
  selectable: false,
  url: '/bulletins'
})
tableResults.init()
function clickOnRow (id) {
  window.open('/bulletins/' + id + '/show', '_blank')
}
function createBulletin () {
  $('#bulletin-create-modal').modal('show')
}
function renderRow (bulletin) {
  return '<td>' + bulletin.title + '</td>' +
    '<td>' + bulletin.message + '</td>' +
    '<td>' + bulletin.user_id + '</td>' +
    '<td>' + bulletin.created_at + '</td>' +
    '<td>' + bulletin.expires + '</td>'
}
function saveBulletin (e, url) {
  $.form({
    data: {
      expires: document.querySelector('input[name="expires"]').value,
      message: document.getElementById('message').value,
      title: document.getElementById('title').value
    },
    method: 'POST',
    url: url,
    success: function (response) {
      $('#bulletin-create-modal').modal('hide')
    },
    error: function (response) {
      $('#bulletin-create-modal').modal('hide')
      errorLogger(response)
    }
  })
}

$(function () {
  $('#expires').pickadate({
    container: '#bulletin-create-modal',
    formatSubmit: 'yyyy-mm-dd',
    hiddenName: true,
    inline: true,
    minDate: moment().add(30, 'm'),
    sideBySide: true
  })
})
