// @author Giomani Designs (Development Team)
//
tableResults.setC({
  objectIdProperty: 'id',
  rowRendererFn: function (row) {
    return renderRow(row)
  },
  selectable: false,
  url: '/driver-mates'
})
tableResults.init()
function renderRow (object) {
  return '<td>' + object.name + '</td>' +
    '<td>' + object.num + '</td>' +
    '<td class="text-right">' + object.rate + '</td>'
}
