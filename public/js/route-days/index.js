// @author Giomani Designs (Development Team)
//
tableResults.setC({
  objectIdProperty: 'id',
  rowRendererFn: function (row) {
    return renderRow(row)
  },
  selectable: false,
  url: '/dispatch/deliveries/days/'
})
tableResults.init()
function renderRow (route) {
  if (route.days === null) {
    route.days = ''
  }
  var days = (route.days).toLowerCase()

  return '<td>' + route.route + '</td>' +
    '<td>' + nullIsADash(route.postcodes) + '</td>' +
    '<td class="text-right">' + route.max + '</td>' +
    '<td class="text-right">' + route.van + '</td>' +
    '<td class="text-center">' + tickOrCross(days.includes('monday')) + '</td>' +
    '<td class="text-center">' + tickOrCross(days.includes('tuesday')) + '</td>' +
    '<td class="text-center">' + tickOrCross(days.includes('wednesday')) + '</td>' +
    '<td class="text-center">' + tickOrCross(days.includes('thursday')) + '</td>' +
    '<td class="text-center">' + tickOrCross(days.includes('friday')) + '</td>' +
    '<td class="text-center">' + tickOrCross(days.includes('saturday')) + '</td>' +
    '<td class="text-center">' + tickOrCross(days.includes('sunday')) + '</td>'
}
