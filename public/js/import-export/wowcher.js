//
var $ = $ || {}
//
//
$('#import-container').importCsv({
  afterColumnHeadingDropFn: function (csvColumns, data) {
    var i, j, ci
    var $e
    var column

    if ((ci = csvColumns.indexOf('deal_id')) !== -1) {
      ci--
      for (i = 0, j = data.length; i < j; i++) {
        column = data[i][ci]
        $e = $('#sku-code-row-' + i)
        $e.children('td:first').html(column)
        $e.find('input[data-key]').attr('data-key', column)
      }
    }
  },
  afterTableFn: function (data) {
    var $e, $f
    var i, j

    for (i = 0, j = data.length; i < j; i++) {
      $e = $('<tr>', {'data-ignore': '0', 'id': 'sku-code-row-' + i}).append($('<td>'))
      $f = $('<input>', {'data-key': ''})
      $e.append($('<td>', {'class': 'td-typeahead'}).append($f))
      $('#sku-codes-table').append($e)
    }

    $('#sku-codes-table input').typeaheadKit({
      display: 'itemCode',
      name: 'itemCode',
      objectIdProperty: 'id',
      queryUrl: '/stock/search/item-code/%QUERY',
      resultsLimit: 15,
      rowClickFn: function (object, e) {
        $('[data-key="' + $(e.currentTarget).attr('data-key') + '"]').typeahead('val', object.itemCode)
      },
    })
  },
  beforeUploadFn: function (data) {
    $('#sku-codes-table tr[data-ignore="0"]').each(function (i, obj) {
      data[$(obj).children('td:first').html()] = $(obj).find('input:first').val()
    })
    return true
  },
  headingsUrl: '/wholesale/sellers/order-import/wowcher-orders/headings',
  onSetIgnoreFn: function (index, yes) {
    if (yes) {
      $('#sku-code-row-' + index).attr('data-ignore', '1').addClass('row-ignore')
    } else {
      $('#sku-code-row-' + index).attr('data-ignore', '0').removeClass('row-ignore')
    }
  },
  uploadUrl: '/wholesale/sellers/order-import/wowcher-orders/store'
})

$('#ttip-0').tooltip({
  'html': true,
  'placement': 'bottom',
  'title': function (self) {
    return '<ul>' +
      '<li>' + 'A new customer is generated for each order.' + '<li>' +
      '<li>' + 'The quantity is always 1 per order.' + '<li>' +
      '</ul>'
  }
})
