// @author Giomani Designs (Development Team)
//
tableResults.setC({
  objectIdProperty: 'id',
  rowRendererFn: function (row) {
    return renderRow(row)
  },
  selectable: false,
  url: '/vehicles/'
})
tableResults.init()
function clickOnRow (vehicle) {
  window.open('/vehicles/' + vehicle.id + '/show', '_blank')
}
function renderRow (vehicle) {
  return '<td>' + vehicle.reg + '</td>' +
    '<td class="text-right">' + vehicle.height + '</td>' +
    '<td class="text-right">' + vehicle.width + '</td>' +
    '<td class="text-right">' + vehicle.depth + '</td>' +
    '<td class="text-right">' + nullIsADash(vehicle.description) + '</td>'
}
