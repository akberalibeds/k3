//
//
$.form = function (options) {
  var settings = $.extend ({
    method: 'POST',
    contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
    data: null,
    postParams: [],
    url: null,
    success: null,
    token: _token,
    error: null
  }, options)

  $.ajax({
    beforeSend: function (xhr) {
      xhr.setRequestHeader('X-CSRF-TOKEN', settings.token)
      for (var key in settings.postParams) {
        xhr.setRequestHeader(key, settings.postParams[key])
      }
    },
    contentType: settings.contentType,
    data: settings.data,
    method: settings.method,
    url: settings.url,
    success: function (data, status, xhr) {
      if (settings.success) {
        settings.success(data)
      }
    },
    error: function (jqXHR, textStatus, errorThrown) {
      var m
      if (settings.error) {
        settings.error(jqXHR)
      }
      if (jqXHR.status === 401) {
        m = 'You are not logged in. <p><a href="/" target="_blank">Click here to open the login in a new window.</a></p>'
      } else if (jqXHR.status === 404) {
        m = jqXHR.responseText
      } else {
        m = 'There was an error, try again later.'
      }
      bootbox.alert(m)
    }
  })
  return this
}



/*
 *	TABLE SEARCH FUNCTION
 */

$.fn.search =  function ( options ) {

 var settings = $.extend ({
    input : null,
  }, options)

	var searchBox = null;
	var $rows = $(this).find('tbody tr');

	if(!settings.input){
		var row = $('<tr><th colspan="5"><input class="form-control ' + settings.class + ' searchBox" placeholder="Search"></th></tr>');
		searchBox = $(row).find('.searchBox');
		$(this).find('thead').prepend(row);
	}
	else{
		searchBox = $(settings.input);
	}

	searchBox.keyup(function() {
		var val = $.trim($(this).val()).replace(/ +/g, ' ').toLowerCase();
		$rows.show().filter(function() {
			var text = $(this).text().replace(/\s+/g, ' ').toLowerCase();
			return !~text.indexOf(val);
		}).hide();
	});

}
