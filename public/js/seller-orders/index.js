// Giomani Designs (Development Team)
//
tableResults.setC({
  objectIdProperty: 'id',
  rowClickFn: function (row) {
    clickOnRow(row)
  },
  rowRendererFn: function (row) {
    return renderRow(row)
  },
  selectable: false,
  url: '/seller-orders/'
})
tableResults.init()
function clickOnRow (seller) {
  window.open('/seller-orders/' + seller.id + '/show', '_blank')
}
function renderRow (seller) {
  var paid = (seller.orders_paid === null ? 0 : seller.orders_paid).toFixed(2)
  var total = (seller.orders_total === null ? 0 : seller.orders_total).toFixed(2)
  return '<td>' + seller.name + ' (' + seller.iName + ')</td>' +
    '<td class="text-right">' + seller.orders_paid_count + '</td>' +
    '<td class="text-right">' + moneyWithColor(paid) + '</td>' +
    '<td class="text-right">' + (seller.orders_count - seller.orders_paid_count) + '</td>' +
    '<td class="text-right">' + moneyWithColor(total - paid) + '</td>' +
    '<td class="text-right">' + seller.orders_count + '</td>' +
    '<td class="text-right">' + moneyWithColor(total) + '</td>'
}
