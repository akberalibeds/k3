// @author Giomani Designs
//
tableResults.setC({
  objectIdProperty: 'oid',
  rowClickFn: function (row) {
    rowClicked(row)
  },
  rowRendererFn: function (row) {
    return renderRow(row)
  },
  selectable: true,
  url: '/seller-orders/' + sellerId + '/show/'
})
tableResults.init()
function exportSelected () {
  var oids
  var selected = tableResults.getSelectedIds()

  if (selected.length === 0) {
    bootbox.alert('You have not selected any rows to export.')
    return
  }
  oids = selected.join('|')
  document.location.href = '/seller-orders/' + sellerId + '/export/' + oids
}
function markSelectedAsPaid () {
  var oids
  var selected = tableResults.getSelectedIds()
  if (selected.length === 0) {
    bootbox.alert('You have not selected any rows to mark.')
    return
  }
  oids = selected.join('|')
  appIsBusy(true)
  $.form({
    method: 'PUT',
    contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
    data: oids,
    url: '/seller-orders/' + sellerId + '/map/' + oids,
    success: function (respone) {
      appIsBusy(false)
      tableResults.refresh()
    },
    error: function (response) {
      appIsBusy(false)
      console.log(response)
    }
  })
}
function markSelectedAsUnPaid() {
  var oids
  var selected = tableResults.getSelectedIds()
  if (selected.length === 0) {
    bootbox.alert('You have not selected any rows to mark.')
    return
  }
  oids = selected.join('|')
  appIsBusy(true)
  $.form({
    method: 'PUT',
    contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
    data: oids,
    url: '/seller-orders/' + sellerId + '/maup/' + oids,
    success: function (respone) {
      appIsBusy(false)
      tableResults.refresh()
    },
    error: function (response) {
      appIsBusy(false)
      console.log(response)
    }
  })
}
function printSelectedInvoice() {
  var ids = tableResults.getSelectedIds()
  if (ids.length === 0) {
    bootbox.alert('You have not selected any rows to mark.')
    return
  }
  window.open('/invoice/' + (ids.join('|')), '_blank')
}
function renderRow (order) {
  var items = (order.items).split(',')
  return '<td>' + order.oid + '</td>' +
    '<td>' + nullIsADash(order.iNo) + '</td>' +
    '<td>' + nullIsADash(order.userID) + '</td>' +
    '<td>' + nullIsADash(order.dBusinessName) + '</td>' +
    '<td>' + nullIsADash(order.postcode).toUpperCase() + '</td>' +
    '<td>' + order.ref + '</td>' +
    '<td>' + order.orderType + '</td>' +
    '<td>' + order.order_status + '</td>' +
    '<td>' + order.startDate + '</td>' +
    '<td class="text-right">' + moneyWithColor(order.total) + '</td>' +
    '<td class="text-right">' + moneyWithColor(order.paid) + '</td>' +
    '<td class="text-right">' + moneyWithColor(order.paid - order.total) + '</td>' +
    '<td class="text-center">' + tickOrCross(order.seller_paid) + '</td>' +
    '<td class="text-center">' + tickOrCross(order.done) + '</td>' +
    '<td>' + items[0] + ' x ' + items[1] + '</td>' +
    '<td>' + nullIsADash(order.voucher_code) + '</td>' +
    '<td class="text-center">' + tickOrCross(order.priority) + '</td>' 
}
function rowClicked (row) {
  window.open('/orders/' + row.oid, '_blank')
}
