// @author Giomani Designs (Development Team)
//
tableResults.setC({
  objectIdProperty: 'id',
  rowClickFn: function (row) {
    clickOnRow(row.id)
  },
  rowRendererFn: function (row) {
    return renderRow(row)
  },
  selectable: false,
  url: '/stock-categories/'
})
tableResults.init()
function clickOnRow (id) {
  window.open('/stock-category-items/' + id, '_blank')
}
function renderRow (category) {
  return '<td>' + category.name + '</td><td>' + nullIsADash(category.description) + '</td>'
}
