// @author Giomani Designs (Development Team)
//
tableResults.setC({
  objectIdProperty: 'id',
  rowRendererFn: function (row) {
    return renderRow(row)
  },
  selectable: false,
  url: '/drivers'
})
tableResults.init()
function renderRow (driver) {
  return '<td>' + nullIsADash(driver.name) + '</td>' +
    '<td>' + nullIsADash(driver.uName) + '</td>' +
    '<td>' + driver.num + '</td>' +
    '<td class="text-right">' + driver.rate + '</td>' +
    '<td class="text-right">' + driver.score + '</td>' +
    '<td class="text-center">' + (driver.active === '1' ? '<i class="color-success fa fa-check" aria-hidden="true"></i>' : '<i class="color-danger fa fa-times" aria-hidden="true"></i>') + '</td>'
}
