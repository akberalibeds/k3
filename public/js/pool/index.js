// authopr Giomani Designs
//
function viewOnMap (routId) {
  var halt = false
  var div = $(this).closest('.orders')
  var tbody = div.find('table tbody')
  var rows = $('tr', tbody)
  var items = {}

  $.each(rows, function (i, v) {
    if ($(v).find('span').hasClass('label-success')) {
      items[i + 1] = {}
      items[i + 1].id = $(v).attr('id')
      items[i + 1].lat = $(v).attr('lat')
      items[i + 1].lng = $(v).attr('lng')
      items[i + 1].items = $(v).find('.items').html()
      items[i + 1].pc = $(v).find('.postcode').html()
    } else {
      halt = true
      $(v).addClass('danger')
    }
  })

  if(!halt) {
    var json = JSON.stringify(items)
    window.open('map.php?orders=' + encodeURIComponent(json) + '&dispRoute=' + encodeURIComponent($('#' + window.route).attr('name')), '_Blank')
  } else {
    alert('Highlighted orders have stock Issue')
  }
}

$('[id^="collapse_"]').on('show.bs.collapse', function () {
  
})
