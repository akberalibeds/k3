// @author Giomani Designs (Development Team)
//
tableResults.setC({
  objectIdProperty: 'oid',
  rowClickFn: function (row) {
    rowClicked(row)
  },
  rowRendererFn: function (row) {
    return renderRow(row)
  },
  selectable: false,
  showCounts: true,
  url: '/orders/after-sales'
})
tableResults.init()

function rowClicked (row) {
  window.open('/orders/' + row.oid, '_blank')
}

function renderRow (order) {
  return '<td>' + order.oid + '</td>' +
    '<td>' + nullIsADash(order.iNo) + '</td>' +
    '<td>' + nullIsADash(order.userID) + '</td>' +
    '<td>' + nullIsADash(order.supplier_name) + '</td>' +
    '<td>' + nullIsADash(order.dBusinessName) + '</td>' +
    '<td>' + nullIsADash(order.postcode).toUpperCase() + '</td>' +
    '<td>' + order.ref + '</td>' +
    '<td>' + order.orderType + '</td>' +
    '<td>' + order.order_status + '</td>' +
    '<td>' + order.startDate + '</td>' +
    '<td class="text-right">' + moneyWithColor(order.total) + '</td>' +
    '<td class="text-right">' + moneyWithColor(order.paid) + '</td>' +
    '<td class="text-right">' + moneyWithColor(order.paid - order.total) + '</td>' +
    '<td class="text-center">' + tickOrCross(order.done) + '</td>' +
    '<td class="text-center">' + tickOrCross(order.priority) + '</td>'
}
