// @author Giomani Designs (Development Team)
//
tableResults.setC({
  objectIdProperty: 'oid',
  rowClickFn: function (row) {
    rowClicked(row)
  },
  rowRendererFn: function (row) {
    return renderRow(row)
  },
  selectable: false,
  url: '/orders/retention'
})
tableResults.init()

function rowClicked (row) {
  window.open('/orders/' + row.oid, '_blank')
}

function rowClicked (order) {
  window.open('/orders/' + order.oid, '_blank')
}

function renderRow (order) {
  return '<td>' + order.oid + '</td>' +
  '<td class="text-center">' + tickOrCross(order.priority) + '</td>' +
    '<td class="text-capitalize">' + order.name + '</td>' +
    '<td>' + (order.postcode).toUpperCase() + '</td>' +
    '<td>' + order.itemCode + '</td>'
  }
