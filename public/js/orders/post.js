// @author Giomani Designs (Development Team)
//
tableResults.setC({
  objectIdProperty: 'oid',
  rowClickFn: function (row) {
    rowClicked(row)
  },
  rowRendererFn: function (row) {
    return renderRow(row)
  },
  selectable: true,
  url: '/orders/post'
})
tableResults.init()

function printLabels () {
  var ids = tableResults.getSelectedIds()
  window.open('/print-labels/post/' + ids.join('|'))
}
function printManifest () {
	  var ids = tableResults.getSelectedIds()
	  window.open('/orders/post/manifest/' + ids.join('|'))
}
function removeFromPost () {
  var ids = tableResults.getSelectedIds()
  $.form({
    method: 'PUT',
    data: { 'ids': ids.join('|') },
    url: '/orders/post/remove',
    success: function (response) {
      appIsBusy(false)
    },
    error: function (response) {
      appIsBusy(false)
    }
  })
}

function renderRow (order) {
  return '<td>' + order.oid + '</td>' +
    '<td>' + order.startDate + '</td>' +
    '<td>' + order.name + '</td>' +
    '<td>' + (order.postcode).toUpperCase() + '</td>' +
    '<td>' + order.postLoc + '</td>' +
    '<td class="text-center">' + tickOrCross(order.priority) + '</td>'
}

function rowClicked (row) {
  window.open('/orders/' + row.oid, '_blank')
}
