// @author Giomani Designs (Development Team)
//
tableResults.setC({
  objectIdProperty: 'oid',
  rowClickFn: function (row) {
    rowClicked(row)
  },
  rowRendererFn: function (row) {
    return renderRow(row)
  },
  selectable: false,
  url: '/orders/delivery-issue'
})
tableResults.init()

function rowClicked (row) {
  window.open('/orders/' + row.oid, '_blank')
}

function renderRow (order) {
  return '<td>' + order.oid + '</td>' +
    '<td class="text-center">' + tickOrCross(order.priority) + '</td>' +
    '<td>' + nullIsADash(order.iNo) + '</td>' +
    '<td>' + (order.postcode).toUpperCase() + '</td>' +
    '<td>' + nullIsADash(order.driver) + '</td>' +
    '<td>' + nullIsADash(order.telephone) + '</td>'
}
