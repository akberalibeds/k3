// @author Giomani Designs (Development Team)
//
tableResults.setC({
  objectIdProperty: 'oid',
  rowClickFn: function (row) {
    rowClicked(row)
  },
  rowRendererFn: function (row) {
    return renderRow(row)
  },
  selectable: false,
  url: '/orders/refunds'
})
tableResults.init()

function rowClicked (row) {
  window.open('/orders/' + row.oid, '_blank')
}

function renderRow (order) {
  return '<td>' + order.oid + '</td>' +
    '<td>' + order.name + '</td>' +
    '<td>' + (order.postcode).toUpperCase() + '</td>' +
    '<td>' + paymentIcon(order.paymentType) + '</td>' +
    '<td>' + order.companyName + '</td>' +
    '<td class="text-right">' + moneyWithColor(order.total) + '</td>' +
    '<td class="text-center">' + tickOrCross(order.priority) + '</td>'
}
