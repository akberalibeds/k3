// @author Giomani Designs (Development Team)
//
tableResults.setC({
  objectIdProperty: 'oid',
  rowClickFn: function (row) {
    rowClicked(row)
  },
  rowRendererFn: function (row) {
    return renderRow(row)
  },
  selectable: true,
  url: '/orders/warehouse-errors'
})
tableResults.init()

function rowClicked (order) {
  window.open('/orders/' + order.oid, '_blank')
}

function renderRow (order) {
  return '<td>' + order.oid + '</td>' +
  	'<td>' + nullIsADash(order.deliverBy) + '</td>' +
    '<td>' + order.name + '</td>' +
    '<td>' + (order.dPostcode).toUpperCase() + '</td>' +
    '<td>' + nullIsADash(order.driver) + '</td>' +
    '<td>' + nullIsADash(order.loader) + '</td>' +
    '<td>' + nullIsADash(order.picker) + '</td>' +
    '<td>' + nullIsADash(order.item) + '</td>' +
    '<td>' + nullIsADash(order.note) + '</td>'
}

function exportCSV(){
	var ids = tableResults.getSelectedIds();
	if(!ids.length){
		return alert('Select rows!!');
	}
	window.open("/orders/warehouse-errors/csv?ids="+ids.join("|"));	
}
function removeErrors(){
	var ids = tableResults.getSelectedIds();
	if(!ids.length){
		return alert('Select rows!!');
	}
	$.get("/orders/warehouse-errors/remove?ids="+ids.join("|"),function(resp){
		tableResults.doQuery();
	});
	
}