
$('body').on('click','.addSearch',function(e) {
	$('.searches').append(addSearch2('',''));	
});

$('body').on('click','.remSearch',function(e) {
	$(this).closest('.searchHere').remove();
});


$('body').on('dblclick','.table>tbody>tr',function(e) {
	window.open('/orders/'+$(this).attr('id'));
});

$('body').on('click','.delSearch',function(e){
	$(this).closest('span').remove();
});

$('body').on('click','.addSearch',function(e) {
		$(".dataTables_filter").find('label').append('<span><br>search: <select aria-controls="DataTables_Table_0" class="sel input input-sm">"'+opts+'"</select>&nbsp;&nbsp;<input class="input search" type="text"></input><button class="btn btn-danger btn-sm delSearch"><i class="fa fa-trash-o"></i></button></span>');
});


$('body').on('keyup','.search',function(e){
	if(e.keyCode==13){
		
		var data = {};
		$('.searchHere').each(function(i, d) {
				data[$(d).find('.field').val()]=$(d).find('.search').val();
		});
		
		
		getOrders(JSON.stringify(data));
			
	}
	
});


$('body').on('click','.pageClick',function(e){
	
	e.preventDefault();
	
	var data = {};
		$('.searchHere').each(function(i, d) {
			
			if($(d).find('.search').val()!=''){			
				data[$(d).find('.field').val()]=$(d).find('.search').val();
			}
	
		});
	
	if(data.length==0){
		data=false;
	}
	else{
		data = JSON.stringify(data);	
	}
	
	console.log($(this).attr('ref'));
	getOrders(data,$(this).attr('ref'));
	
});


var searchCall = false;


function getOrders(search,page){
	

	var data='';
	if(search){
		data='&search='+search;
	}
	var pageNum ='';
	if(page){
		pageNum='&page_num='+page;
	}
	
	$('#orders-table tbody').html('<tr><td colspan="13" align="center"><i class="fa fa-spinner fa-2x fa-spin"></i> Loading</td></tr>');
	$('.pageHere').html('');
	
	if(searchCall){
		searchCall.abort();	
	}
	
	searchCall = $.get("/orders/search",data+pageNum,function(data){
			
			
			
			$('#orders-table tbody').html('');	
			data = $.parseJSON(data);
			
			var orders = data.orders;
			
			if(orders){
				$.each(orders,function(i,row){
						$('#orders-table tbody').append(addRow(row));
				});
				$('.pageHere').html(data.pages);	
			}
			else{
				$('#orders-table tbody').html('<tr><td colspan="13" align="center">No matching records found</td></tr>');
			}
			
			
			
	}).error(function(status){
		if(parseInt(status.status)==401){
			$('.danger-modal').click()
		}
	})
	
}


function addRow(row){
	var $row = row;
				       var tr= "<tr id='"+$row.id+"'>"+
								"<td><span class='label label-"+getStock($row.stockCount)+"'>&nbsp;&nbsp;&nbsp;</span></td>"+
								"<td>"+$row.id+"</td>"+
								"<td>"+$row.iNo+"</td>"+
								"<td>"+ (($row.userID!=null) ? $row.userID : '')  +"</td>"+
								"<td>"+$row.name+"</td>"+
								"<td>"+$row.postcode+"</td>"+
								"<td>"+(($row.ouref!=null) ? $row.ouref : '') +"</td>"+
								"<td>"+$row.orderType+"</td>"+
								"<td>"+$row.orderStatus+"</td>"+
								"<td>"+$row.startDate+"</td>"+
								"<td><span class='label label-primary'>" + parseFloat($row.total).toFixed(2) + "</span></td>";
								
								var label  = ($row.owed>1) ?  'danger' : 'success';
								
								tr+= "<td><span class='label label-"+label+"'>"+ parseFloat($row.owed).toFixed(2) + "</span></td>";
								tr+= ($row.done==1) ? "<td><span class='label label-success'>yes</span></td>" : "<td><span class='label label-danger'>no</span></td>" ;
								tr+= "</tr>";		
	return tr;
}



function addSearch(key,val){
	
	var list = [['o.id','Order'],['o.iNo','Invoice'],['c.userID','Buyer ID'],['c.dBusinessName','Name'],['c.dPostcode','Postcode'],['o.ouref','Ref'],['o.orderType','Type'],['o.orderStatus','Status'],['o.startDate','Date'],['o.beds','Bed ID'],['direct','direct'],['o.companyName','Company'],['c.email1','Email']];
	
	
	
	
	var option='';
	for(var i=0; i<list.length;i++){
			option+='<option '+((key==list[i][0]) ? "selected" : ' ' )+' value="'+list[i][0]+'">'+list[i][1]+'</option>';
	}
	
						return '<div class="searchHere">'+
                              		'<div class="col-md-4">'+ 
									'<select class="form-control field">'+
										option +
									'</select>'+
								  '</div>'+
								  '<div class="col-md-7">'+
									 '<div class="input-group">'+
										'<input value="'+val+'" type="text" class="form-control search">'+
										'<span class="input-group-addon green addSearch">'+           
										  '<i class="fa fa-plus"></i>'+
										'</span>'+
									  '</div>'+
                            	'</div>'+
                            '<div class="clearfix"></div>';
}



function addSearch2(key,val){
		
	var list = [['o.id','Order'],['o.iNo','Invoice'],['c.userID','Buyer ID'],['c.dBusinessName','Name'],['c.dPostcode','Postcode'],['o.ouref','Ref'],['o.orderType','Type'],['o.orderStatus','Status'],['o.startDate','Date'],['o.beds','Bed ID'],['direct','direct'],['o.companyName','Company'],['c.email1','Email']];
	var option='';
	for(var i=0; i<list.length;i++){
			option+='<option '+((key==list[i][0]) ? "selected" : ' ' )+' value="'+list[i][0]+'">'+list[i][1]+'</option>';
	}
						return '<div class="searchHere">'+
                              		'<div class="col-md-4">'+ 
									'<select class="form-control field">'+
										option +
									'</select>'+
								  '</div>'+
								  '<div class="col-md-7">'+
									  '<div class="input-group">'+
										'<input  value="'+val+'" type="text" class="form-control search">'+
										'<span class="input-group-addon red remSearch">'+           
										  '<i class="fa fa-trash-o"></i>'+
										'</span>'+
									  '</div>'+
                            	  '</div>'+
								'</div>'+
                            '<div class="clearfix"></div>';
}


function getStock($row){
	
	//$res = mysqli_query($link,"select group_concat(actual) as i from orderItems join stock_items on orderItems.itemid=stock_items.id where oid=$id)");
	//$row = mysqli_fetch_assoc($res);	
	var $str = $row.split(",");
	var $r=0;
	var $g=0;
	for(var $zz=0;$zz<$str.length;$zz++){
			if($str[$zz]>0){ $g++; }
			if($str[$zz]<1){ $r++;	}
	}
	
	if($r>0 && $g>0)	{ return 'warning'; }
	else if($r>0)		{ return 'danger'; }
	else if($g>0)		{ return 'success'; }
	else				{ return 'danger'; }

}
