// @author Giomani Designs (Development Team)
//
tableResults.setC({
  objectIdProperty: 'id',
  rowClickFn: function (row) {
    rowClicked(row)
  },
  rowRendererFn: function (row) {
    return renderRow(row)
  },
  selectable: true,
  url: '/orders/unconfirmed'
})
tableResults.init()


function rowClicked (order) {
  window.open('/orders/' + order.id, '_blank')
}

function renderRow (order) {
  
  return '<td>' + order.id + '</td>' +
	     '<td>' + order.name + '</td>' +
	     '<td>' + (order.postcode).toUpperCase() + '</td>' +
	     '<td>' + order.startDate + '</td>';
  }
