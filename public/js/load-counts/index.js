// Giomani Designs (Development Team)
//
var today = new Date()
var $picker = $('#load-counts-date').pickadate({
  container: 'body',
  editable: false,
  hiddenName: true,
  min: -14,
  max: 14,
})

var datePicker = $picker.pickadate('picker')
datePicker.set('select', today)

function csvLoadCounts () {
  var date = datePicker.get('select', 'dd-mmm-yyyy')
  var type = document.getElementById('load-counts-type').value
  var warehouse = document.getElementById('warehouse').value
  var stockParts = document.getElementById('stock-parts').checked;
  console.log(stockParts);
  document.location.href = '/load-counts/csv/' + type + '/' + date + '/' + warehouse + '/?parts=' + stockParts
}

function htmlLoadCounts () {
  var date = datePicker.get('select', 'dd-mmm-yyyy')
  var type = document.getElementById('load-counts-type').value
  var warehouse = document.getElementById('warehouse').value
  var stockParts = document.getElementById('stock-parts').checked;
  console.log(stockParts);
  document.location.href = '/load-counts/show/' + type + '/' + date + '/' + warehouse + '/?parts=' + stockParts;
}

function printLoadCounts () {
  var date = datePicker.get('select', 'dd-mmm-yyyy')
  var size = document.getElementById('print-size-select').value
  var type = document.getElementById('load-counts-type').value
  var warehouse = document.getElementById('warehouse').value
  var stockParts = document.getElementById('stock-parts').checked;
  console.log(stockParts);
  window.open('/load-counts/print/' + type + '/' + date + '/' + warehouse + '/' + size + '/?parts=' + stockParts);
}

function loadCounts () {
  console.log(datePicker.get('select').pick)
  appIsBusy(true)
  $.form({
    method: 'GET',
    url: '/load-counts/day/' + (datePicker.get('select').pick) + '/',
    success: function (response) {
      console.log(response)
      appIsBusy(false)
    },
    error: function (response) {
      console.log(response)
      appIsBusy(false)
    }
  })

}


$("#load-counts-type").on("change", function() {
  if ($(this).val() === "2") {
      $("#stock-parts").attr("disabled", "disabled");
      $("#stock-parts").prop("checked", false);
  } else {
      $("#stock-parts").removeAttr("disabled");
      $("#stock-parts").prop("checked", false);
  }
});

var form = document.getElementById('load-counts-type');

