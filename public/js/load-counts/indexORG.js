// Giomani Designs (Development Team)
//
var today = new Date()
var $picker = $('#load-counts-date').pickadate({
  container: 'body',
  editable: false,
  hiddenName: true,
  min: [2013, 12, 2]
})
var datePicker = $picker.pickadate('picker')
datePicker.set('select', today)

function csvLoadCounts () {
  var date = datePicker.get('select', 'dd-mmm-yyyy')
  var type = document.getElementById('load-counts-type').value
  var warehouse = document.getElementById('warehouse').value
  // window.open('/load-counts/csv/' + type + '/' + date + '/' + warehouse)
  document.location.href = '/load-counts/csv/' + type + '/' + date + '/' + warehouse
}


function printLoadCounts () {
  var date = datePicker.get('select', 'dd-mmm-yyyy')
  var size = document.getElementById('print-size-select').value
  var type = document.getElementById('load-counts-type').value
  var warehouse = document.getElementById('warehouse').value
  window.open('/load-counts/print/' + type + '/' + date + '/' + warehouse + '/' + size, '_blank')
}

function loadCounts () {
  console.log(datePicker.get('select').pick)
  appIsBusy(true)
  $.form({
    method: 'GET',
    url: '/load-counts/day/' + (datePicker.get('select').pick) + '/',
    success: function (response) {
      console.log(response)
      appIsBusy(false)
    },
    error: function (response) {
      console.log(response)
      appIsBusy(false)
    }
  })

}
function loadCountsM () {

}
