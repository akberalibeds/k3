// @author Giomani Designs (Development Team)
//
tableResults.setC({
  objectIdProperty: 'id',
  rowClickFn: function (row) {
    clickOnRow(row)
  },
  rowRendererFn: function (row) {
    return renderRow(row)
  },
  selectable: false,
  url: '/stock-category-items/' + categoryId
})
tableResults.init()

function clickOnRow (stockItem) {
  window.open('/stock/' + stockItem.id + '/show', '_blank')
}

function renderRow (object) {
  return '<td>' + object.itemCode + '</td>' +
    '<td>' + object .itemDescription + '</td>' +
    '<td class="text-right">' + object.actual + '</td>' +
    '<td class="text-right">' + object.qty_on_so + '</td>' +
    '<td class="text-right">' + (object.actual - object.qty_on_so) + '</td>' +
    '<td class="text-right">' + object.cost + '</td>' +
    '<td class="text-right">' + object.retail + '</td>' +
    '<td class="text-right">' + object.wholesale + '</td>' +
    '<td class="text-right">' + object.pieces + '</td>' +
    '<td class="text-right">' + object.isMulti + '</td>' +
    '<td class="text-center">' + tickOrCross(object.blocked) + '</td>' +
    '<td class="text-center">' + tickOrCross(object.withdrawn) + '</td>'
}
