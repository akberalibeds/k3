// @author Giomani Designs (Development Team)
//
tableResults.setC({
  objectIdProperty: 'id',
  rowClickFn: function (row) {
    clickOnRow(row)
  },
  rowRendererFn: function (row) {
    return renderRow(row)
  },
  selectable: false,
  url: '/suppliers'
})
tableResults.init()
function clickOnRow (suppplier) {
  window.open('/suppliers/' + suppplier.id + '/orders')
}
function renderRow (supplier) {
  return '<td>' + supplier.supplier_name + '</td>' +
    '<td class="text-right">' + supplier.days + '</td>'
}
