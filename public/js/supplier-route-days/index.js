// @author Giomani Designs (Development Team)
//
$('#typeahead-route-days-name').typeaheadKit({
  display: 'name',
  name: 'name',
  objectIdProperty: 'id',
  pagingControls: '#paging-controls',
  queryUrl: '/suppliers/route-days/search/name/%QUERY',
  resultsLimit: 15,
  resultsElement: '#route-days-table',
  rowElementFn: function (row) {
    return drawRow(row)
  }
})

$.paging({
  pagingControls: '#paging-controls',
  resultCounts: '#result-counts',
  resultsElement: '#route-days-table',
  rowElementFn: function (row) {
    return drawRow(row)
  },
  url: '/suppliers/' + supplierId + '/route-days'
})

var DAY_NAMES = ['monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday']

function drawRow (row) {
  var days = row.days ? row.days.toLowerCase().split(';') : []
  var daysCells = ''

  for (var i = 0; i < 7; i++) {
    daysCells += '<td class="text-center">'
    if (days.indexOf(DAY_NAMES[i]) !== -1) {
      daysCells += '<i class="color-success fa fa-check" aria-hidden="true"></i>'
    } else {
      daysCells += '<i class="color-danger fa fa-times" aria-hidden="true"></i>'
    }
    daysCells += '</td>'
  }

  return '<td>' + row.name + '</td>' +
    '<td class="text-right">' + row.max + '</td>' +
    '<td class="text-right">' + row.van + '</td>' +
    daysCells
}
