// @author Giomani Designs (Development Team)
//
tableResults.setC({
  objectIdProperty: 'oid',
  queryUrl: '/testing/' + sellerId + '/search/%SUBJECT/%QUERY/%FILTER',
  rowClickFn: function (row) {
    rowClicked(row)
  },
  rowRendererFn: function (row) {
    return renderRow(row)
  },
  selectable: true,
  url: '/testing/' + sellerId + '/show/%FILTER'
})
tableResults.init()

function renderRow (order) {
  var bal = order.paid - order.total

  return '<td>' + order.oid + '</td>' +
    '<td>' + nullIsADash(order.iNo) + '</td>' +
    '<td>' + nullIsADash(order.userID) + '</td>' +
    '<td>' + order.dBusinessName + '</td>' +
    '<td>' + (order.postcode).toUpperCase() + '</td>' +
    '<td>' + order.ref + '</td>' +
    '<td>' + order.orderType + '</td>' +
    '<td>' + order.order_status + '</td>' +
    '<td>' + order.startDate + '</td>' +
    '<td class="text-right">' + (order.total).toFixed(2) + '</td>' +
    '<td class="text-right">' + (order.paid).toFixed(2) + '</td>' +
    '<td class="text-right">' + bal.toFixed(2) + '</td>' +
    '<td class="text-center">' + tickOrCross(order.seller_paid) + '</td>' +
    '<td>' + tickOrCross(order.done) + '</td>' +
    '<td>' + nullIsADash(order.voucher_code) + '</td>'
}

function rowClicked (row) {
  window.open('/orders/' + row.oid, '_blank')
}
