// @author Giomani Designs (Development Team)
//
tableResults.setC({
  objectIdProperty: 'id',
  rowButtonClickFn: function (row) {
    clickOnSO(row.id)
  },
  rowClickFn: function (row) {
    clickOnRow(row.id)
  },
  rowRendererFn: function (row) {
    return renderRow(row)
  },
  selectable: true,
  url: '/admin/stock'
})
tableResults.init()
function clickOnRow (stockId) {
  window.open('/admin/stock/' + stockId + '/show', '_blank')
}
function clickOnSO (stockId) {
  window.open('/admin/stock-orders/' + stockId, '_blank')
}
function exportCsvSelected () {
  var selected = tableResults.getSelectedIds()
  if (selected < 1) {
    notEnoughSelected()
    return
  }
  document.getElementById('csv-ids').value = selected.join('|')
  bootbox.prompt({
    title: 'CSV Export',
    inputType: 'checkbox',
    inputOptions: [{
      text: 'Include .... &apos;Part n of m&apos;',
      value: '1'
    }],
    callback: function (result) {
      document.getElementById('csv-with-parts').value = result.length
      document.getElementById('csv-export-form').submit()
    }
  })
}
function exportExcelSelected () {
  var selected = tableResults.getSelectedIds()
  if (selected < 1) {
    notEnoughSelected()
    return false
  }
  document.getElementById('excel-ids').value = selected.join('|')
  bootbox.prompt({
    title: 'Excel Export',
    inputType: 'checkbox',
    inputOptions: [{
      text: 'Include .... &apos;Part n of m&apos;',
      value: '1'
    }],
    callback: function (result) {
      document.getElementById('excel-with-parts').value = result.length
      document.getElementById('excel-export-form').submit()
    }
  })
}
function notEnoughSelected () {
  bootbox.alert('You must select at least one stock item.')
}
function renderRow (object) {
  var html
  html = 
    '<td>' + object.itemCode + '</td>' +
    '<td>' + nullIsADash(object.sales_category) + '</td>' +
    '<td class="text-right">' + object.actual + '</td>' +
    '<td class="text-right">'
  if (object.qty_on_so > 0) {
    html += '<button class="btn btn-default btn-mini" data-ref="">' + object.qty_on_so + '</button>'
  } else {
    html += object.qty_on_so
  }
  html += '</td>' +
    
    '<td class="text-right">' + (object.actual - object.qty_on_so) + '</td>' +
    '<td class="text-right">' + yesOrNo(object.label) + '</td>'+
    '<td class="text-right">' + object.warehouse + '</td>'+
    '<td class="text-right">' + object.pieces + '</td>' +
    '<td class="text-right">' + nullIsADash(object.isMulti) + '</td>' +
    '<td class="text-center">' + tickOrCross(object.isActive) + '</td>' +
    '<td class="text-center">' + tickOrCross(object.blocked) + '</td>' +
    '<td class="text-center">' + tickOrCross(object.withdrawn) + '</td>' +
    '<td class="text-center">' + tickOrCross(object.needs_attention) + '</td>' +
    '<td class="text-center">' + tickOrCross(object.always_in_stock) + '</td>'
  return html
}
