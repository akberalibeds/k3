// Giomani Designs (Development Team)
//
function addToStockCount (id, itemCode) {
  bootbox.dialog({
    message: '' +
      '<div class="form-inline">' +
        '<div class="form-group">' +
          '<label for="add-stock-count">Amount to add to stock </label> ' +
          '<input type="text" class="form-control" id="add-stock-count" name="add-stock-count" placeholder="">' +
          '&nbsp;<button class="btn btn-default" onclick="addStockCount(' + id + ')">Add To Stock Count</button>' +
        '</div>' +
      '</div>' +
      '<span class="text-danger" id="error-count"></span>',
    title: 'Add to stock for item <strong>&apos;' + itemCode + '&apos;</strong>',
    buttons: {
      cancel: {
        label: 'Cancel',
        className: 'btn-defualt',
        callback: function () {
          return true
        }
      }
    }
  })
}

function adjustStockCount (id, itemCode) {
  bootbox.dialog({
    message: '' +
      '<div class="form-inline">' +
        '<div class="form-group">' +
          '<label for="adjust-stock-count">Adjust stock count to </label> ' +
          '<input type="text" class="form-control" id="adjust-stock-count" name="adjust-stock-count" placeholder="">' +
          '&nbsp;<button class="btn btn-default" onclick="setStockCount(' + id + ')">Set Stock Count</button>' +
        '</div>' +
      '</div>' +
      '<span class="text-danger" id="error-count"></span>',
    title: 'Add to stock for <strong>&apos;' + itemCode + '&apos;</strong>',
    buttons: {
      cancel: {
        label: 'Cancel',
        className: 'btn-defualt',
        callback: function () {
          return true
        }
      }
    }
  })
}

function addStockCount (id) {
  $.form({
    method: 'PUT',
    data: { count: document.getElementById('add-stock-count').value },
    url: '/admin/stock/' + id + '/add-stock',
    success: function (response) {
      document.getElementById('actual').innerHTML = response.actual
      document.getElementById('qty-on-so').innerHTML = response.qtyOnSO
      document.getElementById('qty-available').innerHTML = parseInt(response.actual, 10) - parseInt(response.qtyOnSO, 10)
      bootbox.hideAll()
      appIsBusy(false)
    },
    error: function (response) {
      var errors
      appIsBusy(false)
      if (response.status === 422) {
        response = response.responseJSON
        errors = Object.keys(response)
        for (var error in errors) {
          $('#error-' + errors[error]).html(response[errors[error]].pop())
        }
      }
    }
  })
}

function setStockCount (id) {
  $.form({
    method: 'PUT',
    data: {count: document.getElementById('adjust-stock-count').value},
    url: '/admin/stock/' + id + '/adjust-stock',
    success: function (response) {
      document.getElementById('actual').innerHTML = response.actual
      document.getElementById('qty-on-so').innerHTML = response.qtyOnSO
      document.getElementById('qty-available').innerHTML = parseInt(response.actual, 10) - parseInt(response.qtyOnSO, 10)
      bootbox.hideAll()
      appIsBusy(false)
    },
    error: function (response) {
      var errors
      appIsBusy(false)
      if (response.status === 422) {
        response = response.responseJSON
        errors = Object.keys(response)

        for (var error in errors) {
          $('#error-' + errors[error]).html(response[errors[error]].pop())
        }
      }
    }
  })
}
