// @author Giomani Designs (Development Team)
//
var uploader = $('#upload-drop').uploadDrop({
  fileIsReadyFn: function () {
    readyToUpload()
  },
  fileTypes: [
    'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
    'application/vnd.ms-excel'
  ],
  finishedFn: function (response) {
    complete(response)
  },
  icons: [
    '<i class="fa fa-file-excel-o fa-5x" aria-hidden="true"></i>',
    '<i class="fa fa-file-excel-o fa-5x" aria-hidden="true"></i>'
  ],
  messageElement: 'upload-messages',
  placeholder: 'Drop data file here.',
  progressElement: '#progress-bar',
  url: '/admin/stock-costs-import'
})
function readyToUpload () {
}
function readStockCosts () {
  uploader.transmitChunks()
}
function sheetIsReady () {
}
function complete (response) {
  var i, j, m, n
  var data = response.sheet.shift()
  var keys
  var row
  var sheet = []
  for (i = 0, j = data.length; i < j; i++) {
    keys = Object.keys(data[i])
    row = []
    for (m = 0, n = keys.length; m < n; m++) {
      row.push(data[i][keys[m]])
    }
    sheet.push(row)
  }
  csvViewer.setC({
    readyFn: function () {
      sheetIsReady()
    },
    selectHide: true,
    selectIgnore: false,
    url: '/admin/stock-costs-import'
  })
  csvViewer.init()
  csvViewer.renderSheet(sheet)
}
