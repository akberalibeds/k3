// @author Giomani Designs
//
function allowDropOnRoles (ev) {
  ev.preventDefault()
}

function allowDropOnUser (ev) {
  ev.preventDefault()
}

function drag (ev) {
  ev.dataTransfer.setData('text', ev.target.id)
}

function dropOnRoles (ev) {
  ev.preventDefault()
  var data = ev.dataTransfer.getData('text')
  ev.target.appendChild(document.getElementById(data))
}

function dropOnUser (ev) {
  ev.preventDefault()
  var data = ev.dataTransfer.getData('text')
  ev.target.appendChild(document.getElementById(data))
}

function updateUserRoles (id) {
  var add = []
  var remove = []

  appIsBusy(true)

  $('#user-roles span[data-was="role"]').each(function (index) {
    add.push($(this).attr('data-id'))
  })

  $('#roles span[data-was="user"]').each(function (index) {
    remove.push($(this).attr('data-id'))
  })

  $.form({
    method: 'PUT',
    contentType: 'application/json',
    data: JSON.stringify({
      'add': add,
      'remove': remove
    }),
    url: '/admin/users/' + id + '/update-roles',
    success: function () {
      $('#user-roles span[data-was]').attr('data-was', 'user')
      $('#roles span[data-was]').attr('data-was', 'role')
      appIsBusy(false)
    },
    error: function () {
      appIsBusy(false)
    }
  })
}

// window.onbeforeunload = function (e) {
//   var message = "Your confirmation message goes here."
//   var e = e || window.event
//   // For IE and Firefox
//   if (e) {
//     e.returnValue = message
//   }
//
//   // For Safari
//   return message;
// }
