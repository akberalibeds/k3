// @author Giomani Designs
//
function changePassword (userId, userName) {
  bootbox.dialog({
    buttons: {
      cancel: {
        label: 'Cancel',
        className: 'btn-default',
        callback: function () {
          return true
        }
      },
      save: {
        label: 'Change Password',
        className: 'btn-primary',
        callback: function () {
          var p = $('#password').val()
          var pc = $('#password_confirmation').val()
          var self = this

          if (pc !== p) {
            $('#message').html('Passwords do not match.')
          } else if (false) { // pc.length > 6) {
            $('#message').html('Passwords must be at least 6 characters.')
          } else {
            appIsBusy(true)

            $.form({
              method: 'PUT',
              contentType: 'application/json',
              data: JSON.stringify({password: p, password_confirmation: pc}),
              url: '/admin/users/' + userId + '/update-password',
              success: function (msg) {
                self.modal('hide')
                appIsBusy(false)
              },
              error: function (msg) {
                $('#message').html(msg.responseJSON.password[0])
                appIsBusy(false)
              }
            })
          }
          return false
        }
      }
    },
    closeButton: false,
    message: '<form class="form-horizontal">' +
      '<div class="form-group">' +
        '<div class="col-md-4">' +
          '<label for="password" class="control-label">Password</label>' +
        '</div>' +
        '<div class="col-md-4">' +
          '<input class="form-control" id="password" name="password" required type="password" value="">' +
        '</div>' +
        '<div class="col-md-4">' +
          '<span class="text-warning" id="message"></span>' +
        '</div>' +
      '</div>' +
      '<div class="form-group">' +
        '<div class="col-md-4">' +
          '<label for="password_confirmation" class="control-label">Confirm Password</label>' +
        '</div>' +
        '<div class="col-md-4">' +
          '<input class="form-control" id="password_confirmation" name="password_confirmation" required type="password" value="">' +
        '</div>' +
      '</div>' +
    '</form>',
    title: 'Change Password for user - ' + userName
  })
}


function suspend (userId)
{
  appIsBusy(true)

  $.form({
    method: 'PUT',
    contentType: 'application/json',
    data: '',
    url: '/admin/users/' + userId + '/suspend',
    success: function (msg) {
      $('#user-status').html(msg.status)
      $('#suspend-button').remove()
      appIsBusy(false)
    },
    error: function (msg) {
      appIsBusy(false)
    }
  })
}
