// @author Giomani Designs (Development Team)
//
tableResults.setC({
  objectIdProperty: 'id',
  rowClickFn: function (row) {
    rowClicked(row)
  },
  rowRendererFn: function (row) {
    return renderRow(row)
  },
  selectable: false,
  url: '/admin/stock-orders/' + stockItemId
})
tableResults.init()

function rowClicked (row) {
  window.open('/orders/' + row.id, '_blank')
}

function renderRow (order) {
  return '<td>' + order.id + '</td>' +
  '<td>' + order.iNo + '</td>' +
  '<td>' + nullIsADash(order.userID) + '</td>' +
  '<td>' + order.dBusinessName + '</td>' +
  '<td>' + (order.dPostcode).toUpperCase() + '</td>' +
  '<td>' + order.staffName + '</td>' +
  '<td>' + order.orderType + '</td>' +
  '<td>' + order.startDate + '</td>' +
  '<td>' + order.orderStatus + '</td>' +
  '<td class="text-right">' + moneyWithColor(order.total) + '</td>' +
  '<td class="text-right">' + moneyWithColor(order.paid) + '</td>' +
  '<td class="text-right">' + moneyWithColor(order.paid - order.total) + '</td>' +
  '<td class="text-center">' + tickOrCross(order.done) + '</td>'
}
