// @author Giomani Designs (Development Team)
//
tableResults.setC({
  objectIdProperty: 'id',
  rowClickFn: function (row) {
    clickOnRow(row.id)
  },
  rowRendererFn: function (row) {
    return renderRow(row)
  },
  selectable: false,
  url: '/admin/suppliers'
})
tableResults.init()
function clickOnRow (id) {
  window.open('/admin/suppliers/' + id + '/show', '_blank')
}
function renderRow (supplier) {
  return '<td>' + supplier.supplier_name + '</td>' +
    '<td class="text-right">' + supplier.days + '</td>' +
    '<td class="text-center">' + tickOrCross(supplier.print_orders) + '</td>'
}
