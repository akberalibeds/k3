// author Giomani Designs
$.paging({
  pagingControls: '#paging-controls',
  resultCounts: '#result-counts',
  resultsElement: '#invoices-table',
  rowElementFn: function (seller) {
    return drawRow(seller)
  },
  url: '/suppliers/' + supplierId + '/invoices'
})

function drawRow (invoice) {
  var row = '<td>' + invoice.date + '</td><td>' +
    '<div class="btn-toolbar" role="toolbar" aria-label="invoice tools">' +
    '<div class="btn-group" role="group" aria-label="invoice">' +
    '<a class="btn btn-default" href="/suppliers/' + supplierId + '/invoices/' + invoice.id + '/csv" type="button">CSV</a>' +
    '<button class="btn btn-default" onclick="window.open(\'/suppliers/' + supplierId + '/invoices/' + invoice.id + '/orders\')" type="button">Orders</button>' +
    '<button class="btn btn-default" onclick="window.open(\'/suppliers/' + supplierId + '/invoices/' + invoice.id + '/view\')" type="button">View</button>' +
    '</div>' +
    '<div class="btn-group" role="group" aria-label="invoice">'

  if (invoice.paid) {
    row += paidText(parseInt(invoice.time, 10))
  } else {
    row += '<button class="btn btn-default" onclick="paidInvoice(this, ' + invoice.id + ')" type="button">Paid</button>'
    row += '<span id="typeahead-spinner"></span>'
  }
  row += '</div></td>'
  return row
}

function paidInvoice(self, invoiceId) {
  var parent = $(self).parent()

  parent.empty()

  $(parent).append('<i class="busy-spinner fa fa-cog fa-2x" aria-hidden="true"></i>')

  $.form({
    'contentType': 'application/json',
    'error': function(msg) {
      parent.empty()
      parent.append(msg.error)
    },
    'method': 'PUT',
    'success': function (msg) {
      parent.empty()
      parent.append(paidText(parseInt(msg.timePaid, 10)))
    },
    'url': '/suppliers/' + supplierId + '/invoices/' + invoiceId + '/paid'
  })
}

function paidText(time) {
  return '&nbsp;&nbsp;Paid: ' + moment.unix(parseInt(time, 10)).format('Do MMM YYYY')
}
