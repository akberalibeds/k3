tableResults.setC({
  objectIdProperty: 'id',
  rowClickFn: function (row) {
    clickOnRow(row)
  },
  rowRendererFn: function (row) {
    return renderRow(row)
  },
  selectable: false,
  url: '/admin/seller-items'
})
tableResults.init()
function clickOnRow (item) {
  var itemCode = nullIsADash(item.itemCode)
  bootbox.dialog({
    buttons: {
      cancel: {
        label: 'Cancel',
        className: 'btn-default',
        callback: function () {
        }
      },
      save: {
        label: 'Save',
        className: 'btn-primary',
        callback: function () {
          var id = document.getElementById('input-id').value
          var data = {
            cost: document.getElementById('input-cost').value
          }
          $.form({
            method: 'PUT',
            data: data,
            postParams: [],
            url: '/admin/seller-items/' + id + '/update',
            success: function (response) {
              tableResults.refresh()
            },
            error: function (response) {
              console.log(response)
            }
          })
        }
      }
    },
    message:
      '<table class="table">' +
        '<tr>' +
          '<th class="text-right">Cost</th>' +
          '<th class="text-right">Beds</th>' +
          '<th class="text-right">Ebay</th>' +
          '<th class="text-right">Retail</th>' +
          '<th class="text-right">Wholesale</th>' +
          '<th class="text-right">Seller</th>' +
        '</tr>' +
        '<tr>' +
          '<td class="text-right">' + moneyWithColor(item.cost) + '</td>' +
          '<td class="text-right">' + money(item.beds_price) + '</td>' +
          '<td class="text-right">' + money(item.ebay_price) + '</td>' +
          '<td class="text-right">' + money(item.retail) + '</td>' +
          '<td class="text-right">' + money(item.wholesale) + '</td>' +
          '<td class="text-right">' +
            '<input class="text-right" id="input-cost" name="input-cost" type="text" value="' + money(item.w_cost) + '">' +
            '<input id="input-id" name="input-id" type="hidden" value="' + item.id + '">' +
          '</td>' +
        '<tr>' +
      '</table>',
    size: 'large',
    title: 'Item: <strong>' + item.itemCode + '</strong> for <strong>' +  nullIsADash(item.name) + '</strong>'
  })
}
function renderRow (item) {
  var seller = nullIsADash(item.name)
  return '<td>' + item.itemCode + '</td>' +
    '<td class="text-right">' + moneyWithColor(item.cost) + '</td>' +
    '<td class="text-right">' + moneyWithColor(item.beds_price) + '</td>' +
    '<td class="text-right">' + moneyWithColor(item.ebay_price) + '</td>' +
    '<td class="text-right">' + moneyWithColor(item.retail) + '</td>' +
    '<td class="text-right">' + moneyWithColor(item.wholesale) + '</td>' +
    '<td class="text-right">' + (seller === '-' ? '(' + item.id_seller + ')' : seller) + '</td>' +
    '<td class="text-right">' + moneyWithColor(item.w_cost) + '</td>'
}
