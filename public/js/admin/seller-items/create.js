// @author Giomani Designs (Development Team)
//
tableResults.setC({
  objectIdProperty: 'id',
  rowClickFn: function (row) {
    clickOnRow(row)
  },
  rowRendererFn: function (row) {
    return renderRow(row)
  },
  selectable: false,
  url: '/stock'
})
tableResults.init()
function clickOnRow (row) {
  var html =
  '<div class="row">' +
    '<input id="id_stock" name="id_stock" type="hidden" value="' + row.id + '">' +
    '<div class="col-md-6">' +
      '<label for="seller">Seller</label>&nbsp;' +
      '<br>' +
      '<select class="selectpicker" data-size="12" id="id_seller" name="id_seller" title="Choose a seller...">'
      for (var i = 0, j = sellers.length; i < j; i++) {
        html += '<option value="' + sellers[i].id + '">' + sellers[i].name + '</option>'
      }
      html +=
      '</select>' +
    '</div>' +
    '<div class="col-md-6">' +
      '<span id="error-id_seller"></span>' +
    '</div>' +
  '</div>' +
  '<div class="row">' +
    '<div class="col-md-6">' +
      '<label for="cost">Cost&nbsp;</label>' +
      '<br>' +
      '<div class="input-group">' +
        '<span class="input-group-addon">&pound;</span>' +
        '<input class="form-control" id="cost" name="cost" type="text">' +
      '</div>' +
    '</div>' +
    '<div class="col-md-6">' +
      '<span id="error-cost"></span>' +
    '</div>' +
  '</div>'
  bootbox.dialog({
    buttons: {
      cancel: {
        label: 'cancel',
        className: 'btn-default'
      },
      save: {
        callback: function () {
          var data = {
            cost: document.getElementById('cost').value,
            id_seller: document.getElementById('id_seller').value,
            id_stock: document.getElementById('id_stock').value
          }
          $.form({
            method: 'POST',
            data: data,
            postParams: [],
            url: '/admin/seller-items/store',
            success: function (response) {

            },
            error: function (response) {
              console.log(response)
            }
          })
        },
        className: 'btn-primary',
        label: 'save'
      }
    },
    message: html,
    title: 'Add wholesale cost for item <strong>' + row.itemCode + '</strong>'
  })
  $('.selectpicker').selectpicker('render')
}
function renderRow (item) {
  return '<td>' + item.itemCode + '</td>' +
    '<td class="text-right">' + money(item.cost) + '</td>' +
    '<td class="text-right">' + money(item.beds_price) + '</td>' +
    '<td class="text-right">' + money(item.ebay_price) + '</td>' +
    '<td class="text-right">' + money(item.retail) + '</td>' +
    '<td class="text-right">' + moneyZeroDash(item.wholesale) + '</td>' +
    '<td class="text-center">' + tickOrCross(item.withdrawn) + '</td>'
}
function calculateDiscount () {
  var discountPercent
  var discountValue
  var sellerPrice = parseFloat($('#discount').val())
  var retail

  if (!isNaN(sellerPrice) && selectedStockItem !== undefined && !isNaN(selectedStockItem.retail) && selectedStockItem.retail > 0) {
    discountPercent = (100 - parseFloat((sellerPrice / selectedStockItem.retail) * 100)).toFixed(2)
    discountValue = parseFloat(selectedStockItem.retail - sellerPrice).toFixed(2)
    $('#dlg-discount-value').html('£' + discountValue + ' (' + discountPercent + ' %)')
  }
}
