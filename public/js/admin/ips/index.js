// Giomani Designs (Development Team)
//
tableResults.setC({
  objectIdProperty: 'id',
  queryable: false,
  rowClickFn: function (row) {
    clickOnRow(row.id)
  },
  rowRendererFn: function (row) {
    return renderRow(row)
  },
  selectable: false,
  url: '/admin/ips'
})
tableResults.init()
function clickOnRow (id) {
  appIsBusy(true)
  $.form({
    contentType: 'application/json',
    method: 'GET',
    url: '/admin/ips/' + id + '/show',
    success: function (response) {
      appIsBusy(false)
      bootbox.dialog({
        message: getMuStash('stash-ip-form').innerHTML,
        title: 'Edit - ' + response.address,
        buttons: {
          cancel: {
            className: 'btn-default',
            label: 'cancel'
          },
          delete: {
            className: 'btn-default',
            label: 'delete'
          },
          save: {
            className: 'btn-primary',
            label: 'save'
          }
        }
      })
    },
    error: function (response) {
      var errors
      appIsBusy(false)
      if (response.status === 422) {
        response = response.responseJSON
        errors = Object.keys(response)
        for (var error in errors) {
          $('#error-' + errors[error]).html(response[errors[error]].pop())
        }
      }
    }
  })
}
function renderRow (object) {
  return '<td>' + object.address + '</td>' +
  '<td>' + object.description + '</td>' +
  '<td>' + object.created_at + '</td>'
}
