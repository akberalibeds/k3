// @author Giomani Designs (Development Team)
//
tableResults.setC({
  filterDefault: ['startDate'],
  objectIdProperty: 'id',
  rowClickFn: function (row) {
    // clickOnRow(row)
  },
  rowRendererFn: function (row) {
    return renderRow(row)
  },
  selectable: false,
  url: '/admin/eod'
})
tableResults.init()
function renderRow (stat) {
  return '<td>' + stat.staffName + '</td>' +
    '<td>' + stat.startDate + '</td>' +
    '<td class="text-right">' + stat.count + '</td>'
}
