var selectedStockItem

function addSellerItem () {
  bootbox.dialog({
    title: 'Add/Edit Discounted Item to Seller <span id="dlg-seller-item-title"></span>',
    message:
      '<div class="row">' +
      '  <div class="col-md-12">' +
      '    <div class="form-group input-group-typeahead">' +
      '      <label class="col-md-4 control-label" for="item-code">Stock Item Code</label>' +
      '      <div class="col-md-6">' +
      '        <input class="form-control typeahead" name="item-code" id="typeahead-stock-item-code" placeholder="item code" type="search">' +
      '        <span class="tt-badge" id="item-code-spinner"></span>' +
      '      </div>' +
      '    </div>' +
      '  </div>' +
      '</div>' +
      '<div class="row">' +
      '  <div class="col-md-offset-4 col-md-8">' +
      '      <p id="item-cost">' +
      '      </p>' +
      '  </div>' +
      '</div>' +
      '<div class="row">' +
      '  <div class="col-md-12">' +
      '    <div class="form-group"> ' +
      '      <label class="col-md-4 control-label" for="discount">Discount price</label>' +
      '      <div class="col-md-6">' +
      '        <input class="form-control text-right" id="discount" name="discount" onchange="calculateDiscount()">' +
      '      </div> ' +
      '    </div> ' +
      '  </div>' +
      '</div>' +
      '<div class="row">' +
      '  <div class="col-md-12">' +
      '    <div class="form-group"> ' +
      '      <label class="col-md-4 control-label" for="discount">Make available to seller</label>' +
      '      <div class="col-md-6">' +
      '        <input id="available-to-seller" name="available-to-seller" type="checkbox">' +
      '      </div> ' +
      '    </div> ' +
      '  </div>' +
      '</div>' +
      '<div class="row">' +
      '  <div class="col-md-12">' +
      '    Discount value: <span id="dlg-discount-value"></span>' +
      '  </div>' +
      '</div>',
    buttons: {
      cancel: {
        label: 'cancel',
        className: 'btn-default',
        callback: function () {
        }
      },
      success: {
        label: 'Save',
        className: 'btn-primary',
        callback: function () {
          calculateDiscount()
          var sellerPrice = parseFloat($('#discount').val())
          if (isNaN(sellerPrice)) {
            return false
          } else {
            $.form({
              method: 'POST',
              data: {
                available_to_seller: $('#available-to-seller').val() === 'on' ? 1 : 0,
                cost: sellerPrice,
                id_seller: sellerId,
                id_stock: selectedStockItem.id
              },
              url: '/admin/seller-items/store',
              success: function (response) {
                console.log(response)
              },
              error: function (response) {
                console.log(response)
              }
            })
          }
        }
      }
    }
  })
}
// @author Giomani Designs (Development Team)
//
tableResults.setC({
  objectIdProperty: 'id',
  rowClickFn: function (row) {
    clickOnRow(row.id)
  },
  rowRendererFn: function (row) {
    return renderRow(row)
  },
  selectable: false,
  url: '/admin/sellers/' + sellerId + '/show/seller-items'
})
tableResults.init()
function clickOnRow (id) {
  window.open('/admin/stock-categories/' + id + '/edit', '_blank')
}
function renderRow (category) {
  return '<td>' + category.name + '</td>' +
    '<td>' + nullIsADash(category.description) + '</td>' +
    '<td class="text-center">' + tickOrCross(category.always_in_stock) + '</td>'
}


// $('#typeahead-stock-item-code').typeaheadKit({
//   display: 'itemCode',
//   name: 'itemCode',
//   objectIdProperty: 'id',
//   pagingControls: '#paging-controls',
//     queryUrl: '/stock/search/item-code/%QUERY',
//   resultsLimit: 15,
//   resultsElement: '#stock-item-table',
//   rowClickFn: function (item) {
//     selectedStockItem = item
//     $('#item-cost').html('Cost: ' + item.cost + ', Retail: ' + item.retail)
//   }
// })

function calculateDiscount () {
  var discountPercent
  var discountValue
  var sellerPrice = parseFloat($('#discount').val())
  var retail

  if (!isNaN(sellerPrice) && selectedStockItem !== undefined && !isNaN(selectedStockItem.retail) && selectedStockItem.retail > 0) {
    discountPercent = (100 - parseFloat((sellerPrice / selectedStockItem.retail) * 100)).toFixed(2)
    discountValue = parseFloat(selectedStockItem.retail - sellerPrice).toFixed(2)
    $('#dlg-discount-value').html('£' + discountValue + ' (' + discountPercent + ' %)')
  }
}
