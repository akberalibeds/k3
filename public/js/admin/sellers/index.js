// @author Giomani Designs (Development Team)
//
tableResults.setC({
  objectIdProperty: 'id',
  rowClickFn: function (row) {
    clickOnRow(row)
  },
  rowRendererFn: function (row) {
    return renderRow(row)
  },
  selectable: false,
  url: '/admin/sellers'
})
tableResults.init()
function clickOnRow (seller) {
  window.open('/admin/sellers/' + seller.id + '/show', '_blank')
}
function clickFn (seller) {
  window.open('/admin/sellers/' + seller.id + '/show', '_blank')
}
function renderRow (seller) {
  return '<td>' + seller.name + ' (' + seller.iName + ')</td>' +
    '<td>' + nullIsADash(seller.email) + '</td>' +
    '<td>' + postcode(seller.pc) + '</td>' +
    '<td class="text-right">' + seller.account_limit + '</td>' +
    '<td class="text-center">' + tickOrCross(seller.force_pay) + '</td>'
}
