// @author Giomani Designs (Development Team)
//
tableResults.setC({
  objectIdProperty: 'id',
  rowRendererFn: function (row) {
    return renderRow(row)
  },
  selectable: true,
  url: '/drivers',
  rowClickFn: function (row) {
		window.open('/admin/drivers/'+row.id+'/show');  
  },
  rowButtonClickFn:function(row){
	 return deleteRow(row);
  }
})
tableResults.init()
function renderRow (driver) {
  return '<td>' + nullIsADash(driver.name) + '</td>' +
    '<td>' + nullIsADash(driver.uName) + '</td>' +
    '<td>' + driver.num + '</td>' +
    '<td class="text-right">' + driver.rate + '</td>' +
    '<td class="text-right">' + driver.score + '</td>' +
    '<td class="text-center">' + (driver.active === '1' ? '<i class="color-success fa fa-check" aria-hidden="true"></i>' : '<i class="color-danger fa fa-times" aria-hidden="true"></i>') + '</td>' +
    '<td class="text-right"><button><i class="text-danger fa fa-trash"></i></button></td>'
}
function deleteRow(row){
	bootbox.confirm("Delete driver "+row.name+"?",function(result){
			
		if(result){
			$.get('/admin/drivers/'+row.id+'/delete',function(data){
				$('tr[data-row-id="' + data + '"]').remove();
			});
		}
	
	});
}
