// @author Giomani Designs (Development Team)
//
var uploader = $('#upload-drop').uploadDrop({
  fileIsReadyFn: function () {
    readyToUpload()
  },
  fileTypes: [
    'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
    'application/vnd.ms-excel'
  ],
  finishedFn: function (response) {
    complete(response)
  },
  icons: [
    '<i class="fa fa-file-excel-o fa-5x" aria-hidden="true"></i>',
    '<i class="fa fa-file-excel-o fa-5x" aria-hidden="true"></i>'
  ],
  messageElement: 'upload-messages',
  placeholder: 'Drop data file here.',
  progressElement: '#progress-bar',
  url: '/admin/stock-upload/'
})
function getItemCodes () {
  var i, j
  var cells
  var modelNumbers = []
  var rowIndexes = []
  var text
  cells = csvViewer.getColumnValues('MODEL NO.')
  for (i = 0, j = cells.length; i < j; i++) {
    text = cells[i].text
    if (text !== undefined && text !== null) {
      modelNumbers.push(text)
      rowIndexes[text] = cells[i].row
    }
  }
  modelNumbers = modelNumbers.join('|')
  appIsBusy(true)
  $.form({
    method: 'GET',
    url: '/stock/model-numbers/' + modelNumbers,
    success: function (response) {
      console.log(response)
      console.log(rowIndexes)
      var stockItems = response.stockItems
      for (i = 0, j = stockItems.length; i < j; i++) {
        console.log(stockItems[i].model_no + ', ' + rowIndexes[stockItems[i].model_no])
        document.querySelector('input[name="item_code-' + rowIndexes[stockItems[i].model_no] + '"]').value = stockItems[i].itemCode
      }
      appIsBusy(false)
    },
    error: function (response) {
      appIsBusy(false)
    }
  })
}
function readyToUpload () {
}
function sheetIsReady () {
  csvViewer.renderTable()
  var itemCodes = new Bloodhound({
    datumTokenizer: Bloodhound.tokenizers.obj.whitespace('value'),
    queryTokenizer: Bloodhound.tokenizers.whitespace,
    prefetch: null,
    remote: {
      url: '/stock/search/item-code/%QUERY',
      wildcard: '%QUERY'
    }
  })
  $('[name^="itemCode"]').typeahead(
    {
      classNames: {
        hint: 'form-control typeahead',
        input: 'form-control typeahead'
      },
      name: 'item-codes',
      source: itemCodes
    },
    {
      display: 'itemCode',
      limit: 20,
      name: 'item-codes',
      source: itemCodes
    }
  )
}
function uploadStockFile () {
  uploader.transmitChunks()
}
function complete (response) {
  var i, j, m, n
  var data = response.sheet.shift()
  var keys
  var row
  var sheet = []
  for (i = 0, j = data.length; i < j; i++) {
    keys = Object.keys(data[i])
    row = []
    for (m = 0, n = keys.length; m < n; m++) {
      row.push(data[i][keys[m]])
    }
    sheet.push(row)
  }
  csvViewer.setC({
    readyFn: function () {
      sheetIsReady()
    },
    selectHide: true,
    selectIgnore: false,
    url: '/admin/stock-upload/'
  })
  csvViewer.init(sheet)
}
