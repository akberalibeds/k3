// @author Giomani Designs (Development Team)
//
tableResults.setC({
  objectIdProperty: 'id',
  rowClickFn: function (row) {
    clickOnRow(row)
  },
  rowRendererFn: function (row) {
    return renderRow(row)
  },
  selectable: false,
  url: '/dispatch/deliveries/days/'
})
tableResults.init()
function clickOnRow (row) {
 window.open('/admin/routes/' + row.id + '/edit', '_blank')
}
function renderRow (routeDay) {
  if (routeDay.days === null) {
    routeDay.days = ''
  }
  var days = (routeDay.days).toLowerCase()

  return '<td>' + routeDay.name + '</td>' +
    '<td>' + nullIsADash(routeDay.postcodes) + '</td>' +
    '<td class="text-center">' + tickOrCross(routeDay.msg) + '</td>' +
    '<td class="text-center">' + tickOrCross(routeDay.is_wholesale) + '</td>' +
    '<td class="text-right">' + routeDay.max + '</td>' +
    '<td class="text-right">' + routeDay.van + '</td>' +
    '<td class="text-center">' + tickOrCross(days.includes('monday')) + '</td>' +
    '<td class="text-center">' + tickOrCross(days.includes('tuesday')) + '</td>' +
    '<td class="text-center">' + tickOrCross(days.includes('wednesday')) + '</td>' +
    '<td class="text-center">' + tickOrCross(days.includes('thursday')) + '</td>' +
    '<td class="text-center">' + tickOrCross(days.includes('friday')) + '</td>' +
    '<td class="text-center">' + tickOrCross(days.includes('saturday')) + '</td>' +
    '<td class="text-center">' + tickOrCross(days.includes('sunday')) + '</td>'
}
