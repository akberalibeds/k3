// @author Giomani Designs (Development Team)
//
function clickedOnDay (event, cell) {
  var changed
  var classes = cell.className
  var dataCurrent = !(cell.getAttribute('data-current') === '1')
  var dataOriginal = (cell.getAttribute('data-original') === '1')
  cell.setAttribute('data-current', (dataCurrent ? '1' : '0'))
  if (dataCurrent !== dataOriginal) {
    classes += ' cell-change'
  } else {
    classes = classes.slice(0, classes.search(' cell-change'))
  }
  if (dataCurrent) {
    cell.innerHTML = '<i class="color-success fa fa-check" aria-hidden="true"></i>'
  } else {
    cell.innerHTML = '<i class="color-danger fa fa-times" aria-hidden="true"></i>'
  }
  cell.className = classes
  changed = document.querySelector('.cell-change')
  if (changed !== null) {
    document.getElementById('save-changes-button').removeAttribute('disabled')
  } else {
    document.getElementById('save-changes-button').setAttribute('disabled', 'disabled')
  }
}
function saveChanges (event) {
  var additions
  var cell
  var changed
  var dataCurrent
  var dataOriginal
  var removes
  var routeDay
  var routeId
  var row
  additions = []
  cell
  changed = document.querySelectorAll('.cell-change')
  dataCurrent
  dataOriginal
  removes = []
  routeDay
  row = document.getElementById('route-row')
  routeId = row.getAttribute('data-route-id')

  for (var i = 0, j = changed.length; i < j; i++) {
    cell = changed[i]
    dataCurrent = cell.getAttribute('data-current')
    dataOriginal = cell.getAttribute('data-original')
    routeDay = cell.getAttribute('data-day')
    if (dataCurrent === '1') {
      additions.push(routeDay)
    } else {
      removes.push(routeDay)
    }
  }
  $.form({
    method: 'POST',
    data: {
      additions: additions,
      removes: removes
    },
    url: '/admin/routes/' + routeId + '/store',
    success: function (response) {
    },
    error: function (response) {
    }
  })
}
