// @author Giomani Designs (Development Team)
//
tableResults.setC({
  objectIdProperty: 'id',
  rowClickFn: function (row) {
    clickOnRow(row)
  },
  rowRendererFn: function (row) {
    return renderRow(row)
  },
  selectable: false,
  url: '/admin/stock-log/'
})

tableResults.init()

function clickOnRow (row) {
  var more = row.more.split(';')
  window.open('/admin/stock/' + more[0] + '/show', '_blank')
}

function renderRow (object) {
  var more = object.more.split(';')
  return '<td>' + stockActions.get(object.action) + '</td>' +
    '<td>' + more[2] + '</td>' +
    '<td class="text-right">' + more[3] + '</td>' +
    '<td class="text-right">' + more[1] + '</td>' +
    '<td>' + object.email + '</td>' +
    '<td>' + object.created_at + '</td>' +
    '<td>' + object.status + '</td>'
}
