// author Giomani Designs (Development Team)
//
function addDocument () {
  bootbox.dialog({
    size: 'medium',
    title: 'Add Document',
    message: '' +
      '<div class="row">' +
        '<div class="col-md-12">' +
          '<form class="form-horizontal">' +
            '<div class="form-group">' +
              '<div class="col-md-6">' +
                '<div class="file-drop-area" id="upload-drop" data-upload-identifier="dashboard"></div>' +
              '</div>' +
              '<div class="col-md-6">' +
                '<label form="name">Name</label>' +
                '<input id="name" name="name" type="text" placeholder="name" class="form-control">' +
              '</div>' +
            '</div>' +
          '</form>' +
        '</div>' +
      '</div>' +
      '<div>' +
        '<p id="upload-message"></p>' +
      '</div>' +
      '<div class="progress">' +
        '<div class="progress-bar" id="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width:0%"></div>' +
      '</div>',
    buttons: {
      cancel: {
        className: 'btn-default',
        label: 'Cancel'
      },
      success: {
        callback: function () {
            uploader.name = $('#name').val()
            uploader.transmitChunks(function () {
              bootbox.hideAll()
            })
          return false
        },
        className: 'btn-primary',
        label: 'Upload'
      }
    }
  })
  var uploader = $('#upload-drop').uploadDrop({
    fileTypes: [
      'application/pdf',
      'image/bmp',
      'image/jpeg',
      'image/jpg',
      'image/png'
    ],
    icons: [
      '<i class="fa fa-file-pdf-o fa-5x" aria-hidden="true"></i>',
      '<i class="fa fa-file-image-o fa-5x" aria-hidden="true"></i>',
      '<i class="fa fa-file-image-o fa-5x" aria-hidden="true"></i>',
      '<i class="fa fa-file-image-o fa-5x" aria-hidden="true"></i>'
    ],
    messageElement: 'upload-messages',
    placeholder: 'Drop image here.',
    progressElement: '#progress-bar',
    url: '/admin/dashboard/document/store'
  })
}
