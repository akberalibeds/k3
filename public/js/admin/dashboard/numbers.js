// author Giomani Designs
//
function addInfo () {
  bootbox.dialog({
    size: 'large',
    title: 'Add Telephone Numbers',
    message: '<div class="row">  ' +
      '<div class="col-md-12"> ' +
        '<table class="table ">' +
          '<thead>' +
            '<tr>' +
              '<th>Description</th>' +
              '<th>Number</th>' +
              '<th class="text-right"><button class="btn btn-default btn-xs" onclick="addInfoInputRow()"><i class="fa fa-plus" aria-hidden="true"></i></button></th>' +
            '</tr>' +
          '</thead>' +
          '<tbody id="info-input-numbers-table">' +
            '<tr>' +
              '<td><input type="text" placeholder="description" class="form-control input-md"></td>' +
              '<td colspan="2"><input type="text" placeholder="number" class="form-control input-md"></td>' +
            '</tr>' +
          '<tbody>' +
        '</table>' +
      '</div>' +
    '</div>',
    buttons: {
      success: {
        className: 'btn-default',
        callback: function () {
          var numbers = []
          var inputs

          $('#info-input-numbers-table tr').each(function (index) {
            inputs = $(this).find('input')
            numbers.push({
              'description': $(inputs[0]).val(),
              'number': $(inputs[1]).val()
            })
          })
          appIsBusy(true)
          $.form({
            method: 'PUT',
            contentType: 'application/json',
            data: JSON.stringify({'numbers': numbers}),
            url: '/admin/dashboard/numbers/store',
            success: function (msg) {
              addInfoRow(msg)
              appIsBusy(false)
            }
          })
        },
        label: 'Save'
      }
    }
  })
}

function addInfoInputRow() {
  $('#info-inputs-table').prepend('<tr>' +
      '<td><input class="form-control input-md" placeholder="description" type="text"></td>' +
      '<td colspan="2"><input class="form-control input-md" placeholder="number" type="text"></td>' +
    '</tr>')
}

function addInfoRow(row) {
  for (var i = 0, j = row.length; i < j; i++) {
    $('#info-table').prepend('<tr>' +
        '<td>' + row[i].description + '</td>' +
        '<td>' + row[i].datum + '</td>' +
        '<td class="td-buttons">' +
        '<div class="btn-group btn-group-xs" role="group" aria-label="...">' +
        '<button class="btn btn-default btn-xs" data-info-id="' + row[i].id + '" role="button" type="button"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button>' +
        '<button class="btn btn-default btn-xs" data-info-id="' + row[i].id + '" role="button" type="button"><i class="fa fa-trash-o" aria-hidden="true"></i></button>' +
        '</div>' +
      '</tr>')
  }
}
