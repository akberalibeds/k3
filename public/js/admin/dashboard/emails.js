// author Giomani Designs
//
function addInfo () {
  bootbox.dialog({
    size: 'large',
    title: 'Add Emails',
    message: '<div class="row">  ' +
      '<div class="col-md-12"> ' +
        '<table class="table">' +
          '<thead>' +
            '<tr>' +
              '<th>Description</th>' +
              '<th>Email</th>' +
              '<th class="text-right"><button class="btn btn-default btn-xs" onclick="addInfoInputRow()"><i class="fa fa-plus" aria-hidden="true"></i></button></th>' +
            '</tr>' +
          '</thead>' +
          '<tbody id="info-input-emails-table">' +
            '<tr>' +
              '<td><input type="text" placeholder="description" class="form-control input-md"></td>' +
              '<td colspan="2"><input type="text" placeholder="email address" class="form-control input-md"></td>' +
            '</tr>' +
          '<tbody>' +
        '</table>' +
      '</div>' +
    '</div>',
    buttons: {
      success: {
        className: 'btn-default',
        callback: function () {
          var emails = []
          var inputs

          $('#info-input-emails-table tr').each(function (index) {
            inputs = $(this).find('input')
            emails.push({
              'description': $(inputs[0]).val(),
              'email': $(inputs[1]).val()
            })
          })
          appIsBusy(true)
          $.form({
            method: 'POST',
            contentType: 'application/json',
            data: JSON.stringify({'emails': emails}),
            url: '/admin/dashboard/emails/store',
            success: function (msg) {
              if (msg.rejects.length != 0) {

              }
              addInfoRows(msg.accepts)
              appIsBusy(false)
            }
          })
        },
        label: 'Save'
      }
    }
  })
}

function addInfoInputRow() {
  $('#info-inputs-table').prepend('<tr>' +
      '<td><input class="form-control input-md" placeholder="description" type="text"></td>' +
      '<td colspan="2"><input class="form-control input-md" placeholder="email address" type="text"></td>' +
    '</tr>')
}

function addInfoRows(row) {
  for (var i = 0, j = row.length; i < j; i++) {
    $('#info-table').prepend('<tr>' +
        '<td>' + row[i].description + '</td>' +
        '<td>' + row[i].datum + '</td>' +
        '<td class="td-buttons">' +
        '<div class="btn-group btn-group-xs" role="group" aria-label="...">' +
        '<button class="btn btn-default btn-xs" data-info-id="' + row[i].id + '" role="button" type="button"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button>' +
        '<button class="btn btn-default btn-xs" data-info-id="' + row[i].id + '" role="button" type="button"><i class="fa fa-trash-o" aria-hidden="true"></i></button>' +
        '</div>' +
      '</tr>')
  }
}
