// @author Giomani Designs (Development Team)
//
tableResults.setC({
  objectIdProperty: 'id',
  rowClickFn: function (row) {
    clickOnRow(row.id)
  },
  rowRendererFn: function (row) {
    return renderRow(row)
  },
  selectable: false,
  url: '/admin/stock-categories/'
})
tableResults.init()
function clickOnRow (id) {
  window.open('/admin/stock-categories/' + id + '/edit', '_blank')
}
function renderRow (category) {
  return '<td>' + category.name + '</td>' +
    '<td>' + nullIsADash(category.description) + '</td>' +
    '<td class="text-center">' + tickOrCross(category.always_in_stock) + '</td>'
}
