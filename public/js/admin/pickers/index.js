// @author Giomani Designs (Development Team)
//
tableResults.setC({
  objectIdProperty: 'id',
  rowRendererFn: function (row) {
    return renderRow(row)
  },
  selectable: true,
  url: '/admin/pickers',
  rowClickFn: function (row) {
		window.open('/admin/pickers/'+row.id+'/show');  
  },
  rowButtonClickFn:function(row){
		 return deleteRow(row);
	  }
})
tableResults.init()
function renderRow (driver) {
  return '<td>' + nullIsADash(driver.name) + '</td>' +
    '<td>' + driver.num + '</td>' +
    '<td class="text-center">' + (driver.active === '1' ? '<i class="color-success fa fa-check" aria-hidden="true"></i>' : '<i class="color-danger fa fa-times" aria-hidden="true"></i>') + '</td>' +
    '<th class="text-right"><button><i class="fa fa-trash text-danger"></i></button></th>'
}
function deleteRow(row){
	bootbox.confirm("Delete picker "+row.name+"?",function(result){
			
		if(result){
			$.get('/admin/pickers/'+row.id+'/remove',function(data){
				$('tr[data-row-id="' + data + '"]').remove();
			});
		}
	
	});
}
