// @author Giomani Designs (Development Team)
//

function addType() {
  bootbox.dialog({
    message:
      '<div class="row">' +
        '<div class="col-md-6">' +
          '<label for="type">Cost type</label>' +
          '<input class="form-control" id="cost-type" name="cost-type" type="text">' +
        '</div>' +
        '<br>' +
        '<div class="col-md-6">' +
          '<span id="error-type"></span>' +
        '</div>' +
      '</div>' +

      '<div class="row">' +
        '<div class="col-md-6">' +
          '<label for="description">Description</label>' +
          '<textarea class="form-control" id="cost-description" name="cost-description" type="text"></textarea>' +
        '</div>' +
        '<div class="col-md-6">' +
        '<br>' +
          '<span id="error-description"></span>' +
        '</div>' +
      '</div>',
    title: 'Add vehicle cost type',
    buttons: {
      cancel: {
        label: 'cancel',
        className: 'btn-default'
      },
      save: {
        label: 'Save',
        className: 'btn-primary',
        callback: function() {
          var costDescription = $('#cost-description').val()
          var costType = $('#cost-type').val()
          var error = false

          $('#cost-description-error').html('')
          $('#cost-type-error').html('')

          if (costDescription.length < 1) {
            $('#cost-description-error').html('You must enter a &apos;Cost Description&apos;')
             error = true
          }
          if (costType.length < 1) {
            $('#cost-type-error').html('You must enter a &apos;Cost Type&apos;')
             error = true
          }
          if (error) {
            return false
          }

          appIsBusy(true)
          $.form({
            contentType: 'application/json',
            data: JSON.stringify({
              'description': costDescription,
              'type': costType
            }),
            // dataType: 'json',
            type: 'POST',
            url: '/admin/vehicle-cost-types/store',
            success: function (response) {
              appIsBusy(false)
              bootbox.hideAll()
              newRow(response)
            },
            error: function (response) {
              appIsBusy(false)

              if (response.status === 422) {
                response = response.responseJSON
                errors = Object.keys(response)

                for (var error in errors) {
                  $('#error-' + errors[error]).html(response[errors[error]].pop())
                }
              }
            }
          })
          return false
        }
      }
    }
  })
}

function newRow(row) {
  $('#vehicles-table').prepend('<tr><td>' + row.type  + '</td><td>' + row.description + '</td></tr>')
}
