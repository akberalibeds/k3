// @author Giomani Designs (Development Team)
//

function addType() {

  bootbox.dialog({
    message:
      '<div class="row">' +
        '<div class="col-md-6">' +
          '<label for="type">Payment type</label>' +
          '<input class="form-control" id="payment_type" name="payment_type" type="text">' +
        '</div>' +
        '<br>' +
        '<div class="col-md-6">' +
          '<span id="error-payment-type"></span>' +
        '</div>' +
      '</div>' +

      '<div class="row">' +
        '<div class="col-md-6">' +
          '<label for="description">Description</label>' +
          '<textarea class="form-control" id="description" name="description" type="text"></textarea>' +
        '</div>' +
        '<div class="col-md-6">' +
        '<br>' +
          '<span id="error-description"></span>' +
        '</div>' +
      '</div>',
    title: 'Add order payment type',
    buttons: {
      cancel: {
        label: 'cancel',
        className: 'btn-default'
      },
      save: {
        label: 'Save',
        className: 'btn-primary',
        callback: function() {
          var description = $('#description').val()
          var payment_type = $('#payment_type').val()
          var error = false

          $('#error-description').html('')
          $('#error-payment_type').html('')

          if (description.length < 1) {
            $('#error-description').html('You must enter a &apos;Description&apos;')
             error = true
          }
          if (payment_type.length < 1) {
            $('#error-payment_type').html('You must enter a &apos;Type&apos;')
             error = true
          }
          if (error) {
            return false
          }

          appIsBusy(true)
          $.form({
            data: {
              'description': description,
              'payment_type': payment_type
            },
            // dataType: 'json',
            type: 'POST',
            url: '/admin/order-payment-types/store',
            success: function (response) {
              appIsBusy(false)
              bootbox.hideAll()
              newRow(response)
            },
            error: function (response) {
              var errors

              appIsBusy(false)

              if (response.status === 422) {
                response = response.responseJSON
                errors = Object.keys(response)

                for (var error in errors) {
                  $('#error-' + errors[error]).html(response[errors[error]].pop())
                }
              }
            }
          })
          return false
        }
      }
    }
  })
}

function newRow(row) {
  $('#payment-types-table').prepend('<tr><td>' + row.payment_type  + '</td><td>' + row.description + '</td></tr>')
}
