// @author Giomani Designs (Development Team)
//
tableResults.setC({
  objectIdProperty: 'id',
  rowButtonClickFn: function (row) {
    clickOnSO(row.id)
  },
  rowClickFn: function (row) {
    clickOnRow(row.id)
  },
  rowRendererFn: function (row) {
    return renderRow(row)
  },
  selectable: false,
  url: '/admin/vehicles/'
})
tableResults.init()
function clickOnRow (id) {
  window.open('/admin/vehicles/' + id + '/show', '_blank')
}
function renderRow (vehicle) {
  var costs
  var createdOn = moment(vehicle.created_on)
  var totalCost = 0

  if (vehicle.costs) {
    costs = vehicle.costs.split(',')

    for (var i = 0, j = costs.length; i < j; i++) {
      totalCost += parseFloat(costs[i])
    }
  }

  return '<td>' + vehicle.reg + '</td>' +
    '<td class="text-right">' + vehicle.height + '</td>' +
    '<td class="text-right">' + vehicle.width + '</td>' +
    '<td class="text-right">' + vehicle.depth + '</td>' +
    '<td class="text-right">' + (vehicle.mileage === null ? '-' : (vehicle.mileage + ' [' + createdOn.format('DD-MM-YYYY')) + ']') + '</td>' +
    '<td class="text-right">' + totalCost.toFixed(2) + '</td>' +
    '<td class="text-right">' + nullIsADash(vehicle.description) + '</td>'
}
