// @author Giomani Designs (Development Team)
//
(function () {
  function drawChart (ctx, data) {
    var myChart = new Chart(ctx, {
      type: 'line',
      data: {
        labels: data.labels,
        datasets: [{
          data: data.costs,
          label: 'Costs',
          options: []
        },
        {
          data: data.mileage,
          label: 'Mileage',
          options: {
            scales: {
              yAxes: [{
                scale: 0.1
              }]
            }
          }
        }
      ]
      },
      options: {
        scales: {
          yAxes: [{
            ticks: {
              beginAtZero: true
            }
          }]
        }
      }
    })
  }
  function getData () {
    var costs = []
    var labels = []
    var mileage = []

    for (var i = 0, j = chartData.length; i < j; i++) {
      costs.push(chartData[i].cost)
      mileage.push(chartData[i].mileage)
      labels.push(chartData[i].date_incurred)
    }
    return {
      costs: costs,
      labels: labels,
      mileage: mileage
    }
  }
  function initialise () {
    var data = getData()
    drawChart(document.getElementById('canvas-chart'), data)
  }

  initialise()
})()
