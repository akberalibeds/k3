// @author Giomani Designs (Development Team)
//
function aggregateReportData ()
{
  appIsBusy(true)
  $.form({
    method: 'GET',
    contentType: 'application/json',
    data: null,
    url: '/admin/reports/aggregate-data',
    success: function (data) {
      appIsBusy(false)
      bootbox.alert('Data updated successfully.<br>Results:<br>Items: ' + data['count-items'] + ' rows, ' + data['time-elapse-items'] + 'seconds.<br>Orders: ' + data['count-orders'] + ' rows, ' + data['time-elapse-orders'] + ' seconds.')
      $('#last-updated').html(data['last-updated'])
    },
    error: function (data) {
      appIsBusy(false)
      bootbox.alert('Data NOT updated successfully.  There may be errors!')
    }
  })
}
