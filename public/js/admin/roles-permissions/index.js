// @author Giomani Designs
//
function editRP (self, permissionId, roleId) {
  var permission = $(self).attr('data-value')
  if (permission === '1') {
    $(self).html('<i class="color-danger fa fa-times" aria-hidden="true"></i>')
    $(self).attr('data-value', '0')
  } else {
    $(self).html('<i class="color-success fa fa-check" aria-hidden="true"></i>')
    $(self).attr('data-value', '1')
  }
  if (permission === $(self).attr('data-original-value')) {
    $(self).addClass('cell-change')
    $(self).attr('data-changed', '1')
  } else {
    $(self).removeClass('cell-change')
    $(self).attr('data-changed', '0')
  }
}

function saveChanges () {
  var add = []
  var changes = {}
  var remove = []
  appIsBusy(true)
  $('[data-changed="1"]').each(function (i, element) {
    if ($(element).attr('data-value') === '1') {
      add.push($(element).attr('data-role-id') + ',' + $(element).attr('data-permission-id'))
    } else {
      remove.push($(element).attr('data-role-id') + ',' + $(element).attr('data-permission-id'))
    }
  })
  changes.add = add
  changes.remove = remove
  $.form({
    method: 'PUT',
    data: { changes: changes },
    url: '/admin/roles-permissions/update',
    success: function (data) {
      $('[data-changed="1"]').each(function (i, element) {
        $(element).attr('data-original-value', $(element).attr('data-value')).attr('data-changed', '0').removeClass('cell-change')
      })
      appIsBusy(false)
    },
    error: function () {
      bootbox.alert('There was a problem with the changes.  Plesae inform an administrator')
      appIsBusy(false)
    }
  })
}

function showDescription () {

}
