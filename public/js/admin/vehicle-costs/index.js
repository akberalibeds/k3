// @author Giomani Designs (Development Team)
//
tableResults.setC({
  objectIdProperty: 'id',
  rowClickFn: function (row) {
    clickOnRow(row)
  },
  rowRendererFn: function (row) {
    return renderRow(row)
  },
  selectable: false,
  url: '/admin/vehicle-costs'
})
tableResults.init()
var $datePicker = $('#date_incurred').pickadate({
  container: '#modal-vehicle-cost',
  format: 'dd mmm yyyy'
})
function clickOnRow (vehicle) {
  window.open('/admin/vehicles/' + vehicle.vehicle_id + '/show', '_blank')
}
function renderRow (vehicle) {
  return '<td>' + vehicle.reg + '</td>' +
    '<td>' + nullIsADash(vehicle.type) + '</td>' +
    '<td>' + vehicle.notes + '</td>' +
    '<td class="text-right">' + nullIsADash(vehicle.mileage) + '</td>' +
    '<td class="text-right">' + moneyWithColor(vehicle.cost) + '</td>' +
    '<td>' + nullIsADash(vehicle.date_incurred) + '</td>'
}
function save () {
  sendData()
}
function saveAndClose () {
  sendData()
  $('#modal-vehicle-cost').modal('hide')
}
function sendData () {
  var data = {
    cost: $('[name="cost"]').val(),
    date_incurred: $datePicker.pickadate('get', 'value'),
    mileage: $('[name="mileage"]').val(),
    notes: $('[name="notes"]').val(),
    vehicle_cost_type_id: $('[name="vehicle_cost_type_id"]').val(),
    vehicle_id: $('[name="vehicle_id"]').val()
  }

  appIsBusy(true)
  $.form({
    data: data,
    type: 'POST',
    url: '/admin/vehicle-costs/store',
    success: function (response) {
      appIsBusy(false)
      tableResults.refresh()
    },
    error: function (response) {
      appIsBusy(false)
      if (response.status === 422) {
        response = response.responseJSON
        errors = Object.keys(response)
        for (var error in errors) {
          $('#error-' + errors[error]).html(response[errors[error]].pop())
        }
      }
    }
  })
}
