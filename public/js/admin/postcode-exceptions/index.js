function addPostcodeException() {
  bootbox.prompt(
    'Enter the postcode part to add',
    function (result) {
      if (result !== null) {
        $.form({
          method: 'POST',
          data: { postcode: result },
          url: '/admin/postcode-exceptions/store',
          success: function (response) {
          },
          error: function (response) {
            console.log(response)
          }
        })
      }
    }
  )
}
