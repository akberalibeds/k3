// @author Giomani Designs (Development Team)
//


function tickTock () {
  $('#time-now').html(moment().format('ddd, DD MMM YYYY HH:mm:ss'))
}

setInterval(tickTock, 1000)
