// Giomani Designs (Development Team)
//
function runVerify () {
  appIsBusy(true)
  var level = $('input[name="level"]:checked').val()
  $.form({
    method: 'GET',
    data: null,
    url: '/admin/log/verify/' + level,
    success: function (response) {
      var html
      if (undefined !== response[0]) {
        html = response[0][0] + ', ' + response[0][1]
      } else {
        html = '-'
      }
      $('#result-0').html(html)

      for (var i = 1, j = 6; i < j; i++) {
        if (undefined !== response[i]) {
          html = 'count :' + response[i].count + ', passes: ' + response[i].passes.length + ', fails: ' + response[i].fails.length
        } else {
          html = '-'
        }
        $('#result-' + i).html(html)
      }
    }
  })
  appIsBusy(false)
}
tableResults.setC({
  objectIdProperty: 'id',
  rowClickFn: function (row) {
    clickOnRow(row)
  },
  rowRendererFn: function (row) {
    return renderRow(row)
  },
  selectable: false,
  url: '/admin/log/'
})

tableResults.init()

function clickOnRow (row) {
  window.open('/admin/log/' + row.id + '/show', '_blank')
}

function renderRow (object) {
  return '<td>' + actions.get(object.action) + '</td>' +
    '<td>' + object.more + '</td>' +
    '<td>' + object.email + '</td>' +
    '<td>' + object.created_at + '</td>'
}
