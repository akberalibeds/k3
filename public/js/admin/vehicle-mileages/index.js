// @author Giomani Designs (Development Team)
//

function saveMileages() {
  var data = {}
  var mileages = []
  var v

  $('#vehicles-table input').each(function (i, obj) {
    v = $(obj).val()
    if (!isNaN(parseFloat(v))) {
      mileages.push({ vehicleId: $(obj).attr('data-vehicle-id'), mileage: v })
    }
  })

  if (mileages.length > 0) {
    $.form({
      method: 'POST',
      data: { mileages: mileages },
      url: '/admin/vehicle-mileage/store',
      success: function (response) {
        console.log('done')
      },
      error: function (response) {
        console.log('not done')
      }
    })
  }
}
