// @author Giomani Designs (Development Team)
//
tableResults.setC({
  objectIdProperty: 'id',
  rowClickFn: function (row) {
    clickOnRow(row)
  },
  rowRendererFn: function (row) {
    return renderRow(row)
  },
  selectable: true,
  url: '/dispatch/dispatched'
})
tableResults.init()
//
function asCsv () {
  var ids = tableResults.getSelectedIds()

  if (ids.length < 1) {
    selectARoute()
  } else {
    document.location.href = '/dispatch/dispatched/orders/csv/' + ids.join('|')
  }
}
function clickOnRow (row) {
  $('#select-driver').selectpicker('val', drivers.get(row.driver))
  $('#select-mate').selectpicker('val', driverMates.get(row.mate))
  $('#select-loader').selectpicker('val', pickers.get(row.loader))
  var picker = (row.picker) ? row.picker.split(",") : "";
  if(picker.length==1){
	  $('#select-picker').selectpicker('val', pickers.get(row.picker))
  }
  else{
	  $('#select-picker').selectpicker('val', pickers.get(picker))
  }
  $('#select-picker').selectpicker('val', "");
  $('#select-vehicle').selectpicker('val', row.vehicle)
  $('#dispatch-detail-modal').modal('show')
  $('#start-time').val(row.startTime)
  $('#route-orders-table').empty()
  $('#dispatch-detail-title').html(row.route)
  selectedRoute = row.id;

  $.form({
    method: 'GET',
    // the route name (row.route) is only needed as long as the route ids are not
    // being filled in the databse
    url: '/dispatch/dispatched/' + row.id + '/show/' + row.date + '/' + row.route,
    success: function (data) {
      var i, j
      var datum
      var html
      for (i = 0, j = data.length; i < j; i++) {
        datum = data[i]
        var clas = (datum.extra=='A') ? 'warning' : '';
        clas = (datum.orderStatus=='Delivered') ? 'success' : clas;
        html = ''
        html += '<tr class="'+clas+'" onclick="window.open(\'/orders/' + datum.oid + '\', \'_blank\')" data-order-id="' + datum.oid + '">'
        html += '<td>' + datum.delOrder + '</td>'
        html += '<td>' + datum.dBusinessName + '</td>'
        html += '<td>' + datum.dPostcode + '</td>'
        html += '<td>' + datum.orderStatus + '</td>'
        html += '<td>' + datum.tel + '</td>'
        html += '<td class="text-center">' + (datum.txt === '1' ? '<i class="color-success fa fa-check" aria-hidden="true"></i>' : '<i class="color-danger fa fa-times" aria-hidden="true"></i>') + '</td>'
        html += '</tr>'
        document.getElementById('route-orders-table').innerHTML += html
      }
      document.getElementById('dispatch-detail-title').innerHTML = row.route + ' (' + i + ' orders)';
      
        var button = $('.sorter');
		$(button).removeClass('btn-danger');
		$(button).addClass('btn-success');
		$(button).html('Start Sort');
		
		$('#route-orders-table').sortable( "destroy" );
    
    },
    error: null
  })
}
var genNum = 0;
function generateBarcodes () {
  var ids = tableResults.getSelectedIds()
  if (ids.length !== 0) {
	  
	    var dialog = bootbox.dialog({
	        message: '<h3 class="text-center">Generating Barcodes '+(genNum+1)+' / '+ids.length + '</h3><p class="text-center"><i class="fa fa-spinner fa-spin fa-2x"></i></p>',
	        closeButton: false
	    });
	         
	  //  appIsBusy(true, 'Generating '+(genNum+1)+' / '+ids.length);
	    $.form({
	      method: 'GET',
	      url: cleanUrl('/dispatch/dispatched/orders/generate-barcodes/' + ids[genNum++]/*ids.join('|')*/),
	      success: function (response) {
	    	 if(genNum < ids.length){
	    		 generateBarcodes();
	    		//appIsBusy(false);
	    		 dialog.modal('hide');
	    	 } 
	    	 else{
	    	  //appIsBusy(false);
	    	  dialog.modal('hide');
	    	  genNum=0;
	    	 }
	      },
	      error: function (response) {
	    	  genNum=0;
	       // appIsBusy(false)
	    	  dialog.modal('hide');
	        console.log('error: ' + response)
	      }
	    });
	
    
  }
}
function renderRow (object) {
  return '<td>' + object.route + '</td>' +
    '<td>' + object.date + '</td>' +
    '<td class="select-driver">' + object.driver + '</td>' +
    '<td class="select-mate">' + nullIsADash(object.mate) + '</td>' +
    '<td class="select-vehicle">' + nullIsADash(object.vehicle) + '</td>' +
    '<td>' + object.staff + '</td>' +
    '<td class="select-loader">' + nullIsADash(object.loader) + '</td>' +
    '<td class="select-picker">' + nullIsADash(object.picker) + '</td>' +
    '<td class="text-right">' + moneyWithColor(object.payments) + '</td>' +
    '<td class="text-right a-startTime">' + object.startTime + '</td>' +
    '<td class="text-center">' + tickOrCross(object.priority) + '</td>'
}
function markAsDelivered () {
  var ids = tableResults.getSelectedIds()

  if (ids.length !== 0) {
    window.open('/dispatch/dispatched/orders/mark-as-delivered/' + ids.join('|'), '_blank')
  }
}
function printBarcodes () {
  var ids = tableResults.getSelectedIds()

  if (ids.length !== 0) {
    window.open('/dispatch/dispatched/orders/print-labels/' + ids.join('|'), '_blank')
  }
}
function printCodOrders () {
  window.open('/dispatch/dispatched/orders/print-cod-orders', '_blank')
}
function printDispatchNotes () {
  var ids = tableResults.getSelectedIds()

  if (ids.length !== 0) {
    window.open('/dispatch/dispatched/orders/print-dispatch-notes/' + ids.join('|'), '_blank')
  }
}
function printLoadSheets () {
  var ids = tableResults.getSelectedIds()

  if (ids.length !== 0) {
    window.open('/dispatch/dispatched/orders/print-load-counts/' + ids.join('|'), '_blank')
  }
}
function printSupplierOrders () {
  window.open('/dispatch/dispatched/orders/print-supplier-orders', '_blank')
}
function selectARoute () {
  bootbox.alert('You must select at least one route.')
}
function textCod () {
	alert('Error !!');
}
function setOptions(item) {
	console.log(item);
	var option = {};
	option.action 	 = item.id;
	option.id  	 	 = (item.id=="select-picker") ? $(item).val() : item.value;
	console.log(option);
	if($.isArray(option.id)){
		console.log('isArray');
		option.text = [];
		$(item).children("option").filter(":selected").each(function(){
			option.text.push($(this).text());
		})
	}
	else{	
		console.log('Not Array');
		option.text  = ("text" in item) ? item.text : $(item).children("option").filter(":selected").text();
	}
	
	
	option.route	 = selectedRoute; 
	
	$.form({
		method : 'POST',
		url:'dispatched/setOptions',
		data: option,
		success: function(data){
			$('tr[data-row-id="' + selectedRoute + '"]').find('.'+item.id).html(option.text);
			$.notify({ icon: 'fa fa-check', type: 'success', message: "Route update" }); 
		}
	});

}

function textShifts () {
   var ids = tableResults.getSelectedIds()
  
   if (ids.length < 1) {
     selectARoute()
   } else {
     $.form({
       method: 'GET',
       contentType: 'application/json',
       data: null,
       url: '/dispatch/dispatched/orders/text-shifts/' + ids,
       success: function (data) {
         console.log(data)
         bootbox.alert('Text Sent');
       },
       error: function (data) {
         console.log(data)
         bootbox.alert('An Error Occurred');
       }
     })
   }
}
var request;
function search(term){
	if(request)
		request.abort();
	if(term.length<3){
		$('.dropdown-box').html('<ul class="list-group"></ul>');
	}
	if(term.length>2){
		request = $.get("/stock/search/item-code/"+term,function(data){
			console.log(data)
			$('.dropdown-box').html('<ul class="list-group"></ul>');
			$.each(data,function(i,v){
					$('.dropdown-box ul').append('<li class="list-group-item" onclick="selected($(this).html());">'+v.itemCode+'</li>');
					console.log(v.itemCode);
			});
		},'JSON');
	}
}

function oos(){
	
	var search = '<input class="form-control" onKeyUp="search($(this).val())" id="search"><div class="dropdown-box" style="max-height:200px; width:570px; overflow-y: 400px;overflow-y: auto;" ></div>';
	
	var textArea = '';
	
	$.each(items,function(i,item){
		textArea+='<tr><td>'+item+'</td><td class="text-right"><button onclick="removeItem($(this).closest(\'tr\'),\''+item+'\')"><i class="fa fa-trash"></i></button></td></tr>';
	});
	
	var dialog = bootbox.dialog({
	    message: search+'<table class="table table-bordered table-striped table-hover array-items">'+textArea+'</table>',
	    closeButton: true,
	    buttons:{
	    	Cancel:function(){
	    		
	    	},
	    	OK:function(){
	    		window.open('/dispatch/dispatched/oos/'+JSON.stringify(items));
	    	}
	    }
	});
	
	
	
	//
}
function selected(item){
	$('.dropdown-box').html('<ul class="list-group"></ul>');
	$('.array-items').append('<tr><td>'+item+'</td><td class="text-right"><button onclick="removeItem($(this).closest(\'tr\'),\''+item+'\')"><i class="fa fa-trash"></i></button></td></tr>');
	items.push(item);
}
function removeItem(row,item){
	$(row).remove()
	for(var i=0;i<items.length;i++){
		if(item==items[i]){
			items.splice(i, 1);
			break;
		}
	}
}
function tox(){
	window.open('/dispatch/dispatched/tox');
}
