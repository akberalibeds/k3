//
//
$('#typeahead-supplier-invoice-orders-name').typeaheadKit({
  display: 'name',
  name: 'name',
  objectIdProperty: 'id',
  pagingControls: '#paging-controls',
  queryUrl: '/suppliers/search/name/%QUERY',
  resultsLimit: 15,
  resultsElement: '#suppliers-table',
  rowClickFn: function (seller) {
    clickFn(seller.id)
  },
  rowElementFn: function (seller) {
    return drawRow(seller)
  }
})

$.paging({
  objectIdProperty: 'oid',
  pagingControls: '#paging-controls',
  resultCounts: '#result-counts',
  resultsElement: '#supplier-orders-table',
  rowClickFn: function rowClick (id) {
    clickFn(id)
  },
  rowElementFn: function (supplier) {
    return drawRow(supplier)
  },
  url: '/suppliers/' + supplierId + '/invoices/' + invoiceMade + '/orders'
})

function clickFn (id) {
  window.open('/orders/' + id, '_blank')
}

function drawRow (order) {
  var notes = order.notes.split(/;/gi)
  var notes_ = ''

  for (var i = 0, j = notes.length; i < j; i++) {
    notes_ += '<p class="td-wrap">' + notes[i] + '</p>'
  }

  return '<td>' + order.oid + '</td>' +
    '<td>' + order.businessName + '</td>' +
    '<td>' + (order.itemCode ? order.itemCode.replace(/;/gi, '<br>') : 'none') + '</td>' +
    '<td class="text-center">' + order.Qty + '</td>' +
    '<td class="text-right">' + (order.cost).toFixed(2) + '</td>' +
    '<td class="text-right">' + (order.lineTotal).toFixed(2) + '</td>' +
    '<td>' + notes_ + '</td>'
}
