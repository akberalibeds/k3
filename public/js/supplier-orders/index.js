// @author Giomani Designs (Development Team)
//
tableResults.setC({
  objectIdProperty: 'oid',
  rowClickFn: function (row) {
    rowClicked(row)
  },
  rowRendererFn: function (row) {
    return renderRow(row)
  },
  selectable: true,
  url: '/suppliers/' + supplierId + '/orders/'
})
tableResults.init()
function rowClicked (row) {
  window.open('/orders/' + row.oid, '_blank')
}
function exportCsvSelected () {
  exportSelected('csv')
}
function exportExcelSelected () {
  exportSelected('excel')
}
function exportSelected (type) {
  var selected = tableResults.getSelectedIds()
  if (selected < 1) {
    notEnoughSelected()
    return
  }
  bootbox.confirm({
    message: 'Customer address.',
    buttons: {
      confirm: {
        label: 'Just Postcode',
        className: 'btn-success'
      },
      cancel: {
        label: 'Full Address',
        className: 'btn-success'
      }
    },
    callback: function (result) {
      if (result) {
        document.location.href = '/suppliers/' + supplierId + '/orders/export/' + type + '?ids=' + selected.join('|')
      } else {
        document.location.href = '/suppliers/' + supplierId + '/orders/export/' + type + '/full?ids=' + selected.join('|') 
      }
    }
  })

}
function getOrdersList () {
  var list = '<ul class="list-orderids">'
  var selected = tableResults.getSelectedIds()
  list += '<li>Order Ids</li>'
  for (var i = 0, j = selected.length; i < j; i++) {
    list += '<li>' + selected[i] + '</li>'
  }
  list += '</ul>'
  return list
}
function renderRow (order) {
  return '<td>' + order.oid + '</td>' +
    '<td>' + order.iNo + '</td>' +
    '<td>' + order.startDate + '</td>' +
    '<td>' + order.deliverBy + '</td>' +
    '<td>' + order.dBusinessName + '</td>' +
    '<td>' + order.route_name + '</td>' +
    '<td>' + (order.dPostcode).toUpperCase() + '</td>' +
    '<td>' + paymentIcon(order.paymentType) + '</td>' +
    '<td class="text-right">' + moneyWithColor(order.paid) + '</td>' +
    '<td class="text-right">' + moneyWithColor(order.total) + '</td>' +
    '<td class="text-right">' + moneyWithColor(order.paid - order.total) + '</td>' +
    '<td>' + (order.itemCode ? order.itemCode : 'none') + '</td>' +
    '<td class="text-center">' + tickOrCross(order.processed) + '</td>' +
    '<td class="text-center">' + tickOrCross(order.ordered) + '</td>' +
    '<td class="text-center">' + tickOrCross(order.done) + '</td>' +
    '<td class="text-center">' + tickOrCross(order.priority) + '</td>'
}
function moveSupplier () {
  var i, j
  var selectedIds
  var selected
  var list
  selectedIds = tableResults.getSelectedIds()
  if (selectedIds < 1) {
    notEnoughSelected()
    return
  }
  select = '<select autofill="off" id="supplier-select">'
  for (i = 0, j = suppliers.length; i < j; i++) {
    select += '<option value="' + suppliers[i].id + '">' + suppliers[i].supplier_name + '</option>'
  }
  select += '</select>'
  list = getOrdersList()
  bootbox.dialog({
    buttons: {
      cancel: {
        className: 'btn-default',
        label: 'Cancel'
      },
      success: {
        className: 'btn-default',
        callback: function () {
          $.form({
            method: 'PUT',
            contentType: 'application/json',
            url: '/suppliers/' + supplierId + '/orders/' + selectedIds.join('|') + '/move-supplier/' + document.getElementById('supplier-select').value,
            success: function (response) {
              tableResults.refresh()
            },
            error: function (response) {
              console.log(response)
            }
          })
        },
        label: 'Move'
      }
    },
    message: select + list,
    title: 'Move to supplier...'
  })
  $('#supplier-select').selectpicker({
    size: 4
  })
}
function notEnoughSelected () {
  bootbox.alert('You must select at least one order.')
}
function printLabels () {
  window.open('/print-labels/' + tableResults.getSelectedIds().join('|'), '_blank')
}
function setAsOrdered () {
  var selected
  selected = tableResults.getSelectedIds()
  if (selected.size < 1) {
    notEnoughSelected()
    return
  }
  $.form({
    method: 'PUT',
    data: { orderIds: selected.join('|') },
    url: '/suppliers/' + supplierId + '/orders/ordered',
    success: function (response) {
      tableResults.refresh()
      console.log('success: ' + response)
    },
    error: function (response) {
      console.log('failure:')
    }
  })
}
function setAsProcessed () {
  var selected
  selected = tableResults.getSelectedIds()
  if (selected.size < 1) {
    notEnoughSelected()
    return
  }
  $.form({
    method: 'PUT',
    data: { orderIds: selected.join('|') },
    url: '/suppliers/' + supplierId + '/orders/process',
    success: function (response) {
      tableResults.refresh()
      console.log('success: ' + response)
    },
    error: function (response) {
      console.log('failure:')
    }
  })
}
function setAsUnOrdered () {
  var selected
  selected = tableResults.getSelectedIds()
  if (selected.size < 1) {
    notEnoughSelected()
    return
  }
  $.form({
    method: 'PUT',
    data: { orderIds: selected.join('|') },
    url: '/suppliers/' + supplierId + '/orders/un-ordered',
    success: function (response) {
      tableResults.refresh()
      console.log('success: ' + response)
    },
    error: function (response) {
      console.log('failure:')
    }
  })
}
function setAsUnProcessed () {
  var selected
  selected = tableResults.getSelectedIds()
  if (selected.size < 1) {
    notEnoughSelected()
    return
  }
  $.form({
    method: 'PUT',
    data: { orderIds: selected.join('|') },
    url: '/suppliers/' + supplierId + '/orders/un-process',
    success: function (response) {
      tableResults.refresh()
      console.log('success: ' + response)
    },
    error: function (response) {
      console.log('failure:')
    }
  })
}
function showInvoices () {
  window.open('/suppliers/' + supplierId + '/invoices')
}
function showRouteDays () {
  window.open('/suppliers/' + supplierId + '/route-days')
}
