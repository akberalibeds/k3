//
// deliveries.js
// Giomani Designs
// 2015.03.07
//






var routes = routes || {}

var ItemD = class {
  constructor () {
    this.items = {}
    this.backup = null
  }
  addItem (item, stock) {
    if (undefined === this.items[item]) {
      this.items[item] = stock
    }
  }
  backup () {
    this.backup = JSON.stringify(this.items)
  }

  decItem (item) {
    var stock = parseInt(this.items[item], 10) - 1
    this.items[item] = stock
    return stock
  }

  getItemStockCount (item) {
    return parseInt(this.items[item], 10)
  }

  incItem (item) {
    var stock = parseInt(this.items[item], 10) + 1
    this.items[item] = stock
    return stock
  }

  restore () {
    this.items = JSON.parse(this.backup)
  }
}

function advanceProgressBar () {
  var progressbar
  var width

  width = (100 / routes.length) * routesDone

  progressbar = document.getElementById('progress-bar')

  progressbar.style.ariaValuenow = width
  progressbar.style.width = width + '%'
  progressbar.innerHTML = routesDone + ' of ' + routes.length

  if (routesDone === routes.length) {
    progressBarComplete()
  }
}

function checkRouteStock (routeId) {
  var stockIsOk
  var orders
  var routeObject

  routeObject = routeObjects[routeId]

  orders = routeObject.orders

  stockIsOk = true

  for (var key in orders) {
    order = orders[key]
    if (order._visible && order._stockStatus != 0) {
      stockIsOk = false
      break
    }
  }

  return stockIsOk
}

function clickChangeRoute (self) {
  var i, j
  var element
  var elements
  var elementsToMove
  var orders
  var routeId
  var routeObject
  var routeToObject
  var tableElement

  routeObject = routeObjects[self.getAttribute('data-data')]

  tableElement = self.parentNode.parentNode.parentNode.parentNode

  elements = tableElement.getElementsByTagName('input')

  elementsToMove = []
  orderIdsToMove = []

  for (i = 0, j = elements.length; i < j; i++) {
    element = elements[i]
    if (element.getAttribute('name') === 'change-route' && element.checked) {
      orderIdsToMove.push(element.getAttribute('data-order-id'))
      elementsToMove.push(element.parentNode.parentNode)
    }
  }

  if (orderIdsToMove.length === 0) {
    showModalPopupMessage('Select order(s) to transfer.')
  } else {
    showModalPopupMessage('Transferring orders.', 0)

    // gets for the first select in the table
    element = tableElement.getElementsByTagName('select')[0]
    routeId = element.options[element.selectedIndex].value;

    communicate('api.php?action=changeRouteB&c=Courier&routeId=' + routeId + '&oids=' + orderIdsToMove.join(','), {}, function (reply) {
      routeObject = routeObjects[self.getAttribute('data-data')]  // get orders to move
      routeToObject = routeObjects[routeId]

      orders = []
      for (key in routeToObject.orders) {
        orders.push(routeToObject.orders[key])
      }

      for (i = 0, j = orderIdsToMove.length; i < j; i++) {
        orders.push(routeObject.orders[orderIdsToMove[i]])
        delete routeObject.orders[orderIdsToMove[i]]

        element = elementsToMove.pop()
        element.parentNode.removeChild(element)
      }

      setRouteDetail(routeId, orders)

      showModalPopupMessage('Orders transferred.')

    }, function (reply) {
      showModalPopupMessage('Orders not transferred.')
    })
  }
}

function clickDoubleOnOrder(self, orderId) {
    window.open('orderDetail.php?id='+orderId)
}

function clickOnRouteBox(self, routeId) {

    var data
    var element
    var html
    var message
    var routeObject

    routeObject = routeObjects[routeId]

    if (self.getElementsByClassName('fa-spin').length > 0) {
        showModalPopup('Route: ' + routeObject.name, 'The route data is not ready.', null, '', '', routeId)
    } else {
        if (routeObject.orders) {
            data = {
                orders: routeObject.orders,
                routeId: routeId
            }

            html = renderHandlebarsView(document.getElementById('route-popup-template'), data)
            showModalPopup('Route: ' + routeObject.name, html, null, '', 'large', routeId)

            drawOrders(data)

        } else {
            showModalPopup('Route: ' + routeObject.name, 'There are no orders for this route.', null, '', '', routeId)
        }
    }
}

function clickOnDispatchOrders (self, routeId) {
  var routeObject

  routeObject = routeObjects[routeId]

  if (route._dispatched) {
    showModalPopupMessage('This route has already been dispatched.')
  } else {
    dispatchRoute(routeId)
  }
}

function clickOnUndoDispatchOrders (self, routeId) {
  var routeObject

  routeObject = routeObjects[routeId]

  if (route._dispatched) {
    dispatchRoute(routeId, true)
  } else {
    showModalPopupMessage('This route has not been dispatched.')
  }
}

function clickOnViewOnMap (self, routeId) {
  var element
  var index
  var items
  var order
  var orders
  var routeObject
  var routeId

  routeObject = routeObjects[routeId]
  orders = routeObject.orders
  showMap(routeObject.orders)
}

function dispatchRoute (routeId, undo) {

    var item
    var order
    var routeObject
    var stockStatus
    var stockCounts

    if (undefined == undo) {
        undo = false
    }

    routeObject = routeObjects[routeId]
    stockStatus = 0

    for (var orderKey in routeObject.orders) {
        order = routeObject.orders[orderKey]
        if (order._visible) {
            stockCounts = []
            for (var itemKey in order._items) {
                item = order._items[itemKey]
                if (undo) {
                    itemD.incItem(item)
                } else {
                    itemD.decItem(item)
                }
            }
            order._dispatched = !undo
        }
    }

    for (var orderKey in routeObject.orders) {
        order = routeObject.orders[orderKey]
        stockCounts = []
        for (var itemKey in order._items) {
            item = order._items[itemKey]
            stockCounts.push(itemD.getItemStockCount(item))
        }

        s = getStockStatus(stockCounts)
        order._stockStatus = s
        stockStatus = stockStatus > s ? stockStatus : s
    }

    routeObject._dispatched = !undo
    routeObject._stockStatus = stockStatus

    data = {
        orders: routeObject.orders,
        routeId: routeId
    }

    drawOrders(data)
}

function drawOrders(data) {

    var html

    html = renderHandlebarsView(document.getElementById('orders-popup-template'), data)
    document.getElementById('route-orders').innerHTML = html
}

function focusOnOrder(self, orderId) {

    var focussedItems
    var focussedOrder
    var id
    var item
    var items
    var orders
    var order
    var routeId
    var row
    var rows
    var sameOrderIds
    var tableElement

    self.style.backgroundColor = HIGHLIGHT_ORDER_COLOR

    routeId = self.getAttribute('data-route-id')
    route = routeObjects[routeId]

    orders = route.orders

    focussedItems = []
    focussedOrder = orders[orderId]

    for (var i = 0, j = focussedOrder.items.length; i < j; i++) {
        item = focussedOrder.items[i]
        if (isAnItem(item)) {
            focussedItems.push(item)
        }
    }

    sameOrderIds = []

    for (key in orders) {
        order = orders[key]
        if (order.id != orderId) {
            for (var i = 0, j = order.items.length; i < j; i++) {
                item = order.items[i]
                if (focussedItems.indexOf(item) != -1) {
                    sameOrderIds.push(order.id)
                }
            }
        }
    }

    tableElement = self.parentNode
    rows = tableElement.getElementsByTagName('tr')

    for (var i = 0, j = rows.length; i < j; i++) {
        row = rows[i]
        id = row.getAttribute('data-order-id')
        if (!isNaN(id)) {
            if (sameOrderIds.indexOf(id) != -1) {
                row.style.backgroundColor = HIGHLIGHT_ORDER_COLOR
            }
        }
    }
}

function focusOnRouteBox(self, routeId) {

    var element
    var focussedRouteItems
    var focussedRoute
    var route
    var routeItems

    focussedRoute = routeObjects[routeId]

    if (undefined == focussedRoute.allItems) {
        return
    }

    self.style.boxShadow = '0 0 6px 4px ' + self.querySelector('.route-box-heading').style.backgroundColor //HIGHLIGHT_ROUTE_BOX_COLOR

    focussedRouteItems = focussedRoute.allItems

    for (var key in routeObjects) {

        route = routeObjects[key]

        if (route.id == routeId || undefined == route.allItems) {
            continue
        }

        routeItems = route.allItems

        for (var i = 0, j = routeItems.length; i < j; i++) {
            element = document.getElementById('route-' + route.id)
            if (focussedRouteItems.indexOf(routeItems[i]) != -1) {
                element.style.boxShadow = '0 0 4px 2px ' + element.querySelector('.route-box-heading').style.backgroundColor //HIGHLIGHT_ROUTE_BOX_COLOR
            }
        }
    }
}

function focusOutOrder(self, orderId) {

    tableElement = self.parentNode

    rows = tableElement.getElementsByTagName('tr')

    for (var i = 0, j = rows.length; i < j; i++) {
        rows[i].style.backgroundColor = ''
    }
}

function focusOutRouteBox(self) {
    var elements

    elements = document.getElementsByClassName('route-box')

    for (var i = 0, j = elements.length; i < j; i++) {
        elements[i].style.boxShadow = ''
    }
}

/**
 * @return 0 - success
 *         1 - warning
 *         2 - danger
 */
function getStockStatus(stockCounts) {

    var color
    var g
    var r

    g = 0
    r = 0

    for(var i = 0; i < stockCounts.length; i++){
        if(stockCounts[i] > 0) {
            g++;
        }
        if(stockCounts[i] < 1) {
            r++;
        }
    }

    color = 0   // danger

    if (r > 0 && g > 0) {
        color = 1       // warning
    } else if (r > 0) {
        color =  2      // danger
    } else if (g > 0) {
        color =  0      // success
    }
    return color
}

function isAnItem(item) {

    var isAnItem = true

    item = item.toLowerCase()

    for (var i = 0, j = NOT_ITEMS_WORDS.length; i < j; i++) {
        if (item.includes(NOT_ITEMS_WORDS[i])) {
            isAnItem = false
            break
        }
    }
    return isAnItem
}

function missedRoutesFilter(routeId) {
    return routesWithOrders.indexOf(routeId) == -1
}

function processRouteIds(routes) {

    var id
    var ids

    ids = []
    routeObjects = {}

    for (var i = 0, j = routes.length; i < j; i++) {
        id = routes[i].id
        ids.push(id)
        routeObjects[id] = routes[i]
    }
    return ids
}

function progressBarComplete() {
    allRoutesLoaded = true
}

function selectAll(self) {

    var checked
    var element
    var elements

    checked = self.checked
    elements = self.parentNode.parentNode.parentNode.parentNode.getElementsByTagName('input')

    for (var i = 0, j = elements.length; i < j; i++) {
        element = elements[i]
        if (element.getAttribute('type') == "checkbox") {
            element.checked = checked
        }
    }
}

function setRouteDetail(routeId, orders) {

    var allItems
    var done
    var element
    var id
    var items
    var stockLabels
    var orderId
    var orderObjects
    var orders
    var paid
    var s
    var state
    var stockCounts
    var stockStatus
    var v

    allItems = []
    done = 0
    paid = 0
    orderObjects = {}
    stockStatus = 0

    for(var i = 0, j = orders.length; i < j; i++) {

        v = orders[i]

        if (undefined != v.items && typeof v.items == 'string') {
            items = v.items.split(',')
            stockCounts = v.stockCount.split(',')

            v.items = items
            v._items = []
            v._nonItems = []
            v._dispatched = false
        }


        for (m = 0, n = items.length; m < n; m++) {
            item = items[m]
            if (isAnItem(item)) {
                if (allItems.indexOf(item) == -1) {
                    allItems.push(item)
                    itemD.addItem(item, stockCounts[m])
                }
                v._items.push(item)
            } else {
                v._nonItems.push(item)
            }
        }

        if (v.p == '1') {
            paid++
        }
        if (v.done == '1') {
            done++
        }

        s = getStockStatus(stockCounts)

        v._stockStatus = s
        v._statusBgColor = (s == 0 ? 'success' : (s == 1 ? 'warning' : 'danger'))
        v._visible = true
        v._dispatched = false

        orderId = v.id
        orderObjects[orderId] = v
        stockStatus = stockStatus > s ? stockStatus : s
    }
    routeObjects[routeId]._dispatched = false
    routeObjects[routeId]._stockStatus = stockStatus
    routeObjects[routeId].allItems = allItems
    routeObjects[routeId].orders = orderObjects

    routeContainer = document.getElementById('route-' + routeId)

    document.getElementById('route-' + routeId).querySelector('[data-route-total]').innerHTML = orders.length

    if (done != orders.length) {
        routeContainer.querySelector('[data-done]').innerHTML = '<i class="fa fa-times-circle"></i>'
        routeContainer.querySelector('[data-done]').style.color = STATUS_COLORS['danger']
    } else {
        routeContainer.querySelector('[data-done]').innerHTML = '<i class="fa fa-check-circle"></i>'
        routeContainer.querySelector('[data-done]').style.color = STATUS_COLORS['success']
    }

    if (paid != orders.length) {
        routeContainer.querySelector('[data-paid]').innerHTML = '<i class="fa fa-times-circle"></i>'
        routeContainer.querySelector('[data-paid]').style.color = STATUS_COLORS['danger']
    } else {
        routeContainer.querySelector('[data-paid]').innerHTML = '<i class="fa fa-check-circle"></i>'
        routeContainer.querySelector('[data-paid]').style.color = STATUS_COLORS['success']
    }

    element = routeContainer.querySelector('[data-loading]')
    if (undefined != element) {
        element.parentNode.removeChild(element)
    }

    element = routeContainer.querySelector('.route-box-heading')
    element.style.backgroundColor = (stockStatus == 0 ? STATUS_COLORS['success'] : (stockStatus == 1 ? STATUS_COLORS['warning'] : STATUS_COLORS['danger']))
}

function showMap(orders) {
    index = 0

    items = {}
    //
    items[index]      = {}
    items[index].id   = 0
    items[index].lat  = ''
    items[index].lng  = ''
    items[index].items = ''
    items[index].pc   = 'WV14 7HZ'

    for (var key in orders) {
        order = orders[key]
        if (order._visible) {
            items[++index] = {}
            items[index].id    = order.id
            items[index].lat   = order.lat
            items[index].lng   = order.lng
            items[index].items = order.items
            items[index].pc    = order.postcode
        }
    }

    window.open('map.php?orders=' + encodeURIComponent(JSON.stringify(items)) + '&dispRoute=' + route.name,'_blank')
}

function toggleOrderVisibility(self, routeId, orderId) {

    var element
    var routeObject

    routeObject = routeObjects[routeId]

    if (routeObject._dispatched) {
        showModalPopupMessage('This route has already been dispatched.')
    } else {

        order = routeObject.orders[orderId]

        order._visible = !order._visible

        element = self.getElementsByTagName('i')[0]

        if (order._visible) {
            element.setAttribute('class', 'fa fa-trash-o')
            element.style.color = 'red'
            self.parentNode.parentNode.style.color = '#000000'
        } else {
            element.setAttribute('class', 'fa fa-plus')
            element.style.color = 'grey'
            self.parentNode.parentNode.style.color = '#c5c5c5'
        }
    }
}

var HIGHLIGHT_EFFECTED_ORDER_COLOR = 'blue'
var HIGHLIGHT_ORDER_COLOR = 'red'
var HIGHLIGHT_ROUTE_BOX_COLOR = 'red'
var NOT_ITEMS_WORDS = ['cancel', 'charge', 'collect', 'deliver', 'missed', 'parcel', 'refund', 'test']
var STATUS_COLORS = []

STATUS_COLORS['danger'] = '#d9534f'
STATUS_COLORS['success'] = '#5cb85c'
STATUS_COLORS['warning'] = '#f0ad4e'

var allRoutesLoaded
var itemD
var routeIds
var routeObjects
var routesDone
var routesWithOrders

allRoutesLoaded = false
itemD = new ItemD
routeIds = processRouteIds(routes)

$.form({
  method: 'GET',
  data: null,
  url: null,
  success: function (data) {

  }
})

communicate('api.php?action=getRouteData&c=Courier&nod=' + 2 + '&rids=' + routeIds.join(), {}, function(reply) {

    var countCollection
    var countObject
    var routePanel
    var route
    var routeId
    var gTotals

    gTotals = reply.gTotals

    routesDone = 0
    routesWithOrders = []

    for (var i = 0, j = gTotals.length; i < j; i++) {
        route = gTotals[i]

        routeId = route.id_delivery_route
        routeName = route.name

        routesWithOrders.push(routeId)

        document.getElementById('route-' + routeId).querySelector('[data-route-total]').innerHTML = route.cnt

        communicate('api.php?action=getRouteDels&c=Courier&route=' + routeId + '&name=' + name, {}, function(reply) {
            setRouteDetail(reply.routeId, reply.orders)
            routesDone++
            advanceProgressBar()
    	})
    }

    missedRoutes = routeIds.filter(missedRoutesFilter)

    routesDone += missedRoutes.length

    missedRoutes.forEach(function(routeId) {
        advanceProgressBar()

        routeContainer = document.getElementById('route-' + routeId)

        element = routeContainer.querySelector('[data-loading]')
        element.parentNode.removeChild(element)

        e0 = routeContainer.querySelector('.route-box-heading')
        e0.style.backgroundColor = '#777'
    })
})


$('*').iCheck('destroy')
