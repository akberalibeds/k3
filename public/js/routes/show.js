tableResults.setC({
  objectIdProperty: 'id',
  queryUrl: '/routes/' + routeId + '/show-orders/search/%SUBJECT/%QUERY',
  rowClickFn: function (row) {
    rowClicked(row)
  },
  rowRendererFn: function (row) {
    return renderRow(row)
  },
  selectable: false,
  url: '/routes/' + routeId + '/show-orders/'
})
tableResults.init()

function rowClicked (row) {
  window.open('/orders/' + row.id, '_blank')
}

function renderRow (order) {
  return '<td>' + order.id + '</td>' +
  '<td>' + order.iNo + '</td>' +
  '<td>' + nullIsADash(order.userID) + '</td>' +
  '<td>' + order.dBusinessName + '</td>' +
  '<td>' + (order.dPostcode).toUpperCase() + '</td>' +
  '<td>' + order.staffName + '</td>' +
  '<td>' + order.orderType + '</td>' +
  '<td>' + order.orderStatus + '</td>' +
  '<td>' + order.startDate + '</td>' +
  '<td class="text-right">' + moneyWithColor(order.total) + '</td>' +
  '<td class="text-right">' + moneyWithColor(order.paid) + '</td>' +
  '<td class="text-right">' + moneyWithColor(order.paid - order.total) + '</td>' +
  '<td class="text-center">' + tickOrCross(order.done) + '</td>'
}
