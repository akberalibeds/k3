// @author Giomani Designs (Development Team)
//
tableResults.setC({
  objectIdProperty: 'id',
  rowClickFn: function (row) {
    clickOnRow(row.id)
  },
  rowRendererFn: function (row) {
    return renderRow(row)
  },
  selectable: false,
  url: '/routes/'
})
tableResults.init()
function clickOnRow (id) {
  window.open('/routes/' + id + '/show', '_blank')
}
function renderRow (route) {
  return '<td>' + route.name + '</td>' +
    '<td class="text-right">' + route.max + '</td>' +
    '<td class="text-right">' + route.van + '</td>' +
    '<td class="text-center">' + tickOrCross(route.msg) + '</td>' +
    '<td class="text-center">' + tickOrCross(route.is_wholesale) + '</td>' +
    '<td class="text-center">' + tickOrCross(route.in_load_count) + '</td>' +
    '<td class="text-center">' + tickOrCross(route.is_route_route) + '</td>'
}
