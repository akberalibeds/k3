// @author Giomani Designs (Development Team)
//
var columns = [
  { heading: 'Date', type: 'string' },
  { heading: 'Company', type: 'string' },
  { heading: 'Count', type: 'number' },
  { heading: 'Type', type: 'string' },
  { heading: 'Paid', type: 'number' },
  { heading: 'VAT', type: 'number' },
  { heading: 'Total', type: 'number' }
]

function getItemsData ()
{
  var companies = ''
  var seperator
  var url

  seperator = ''
  for (var company in params.companies)
  {
    if (params.companies[company])
    {
      companies += seperator + (company)
      seperator = '|'
    }
  }

  url = '/reports/data/orders/' + params.dateFrom + '/' + params.dateTo + '/' + companies

  appIsBusy(true, 'Loading data...')

  $.form({
    method: 'GET',
    contentType: 'application/json',
    data: null,
    url: url,
    success: function (message) {
      appIsBusy(false)

      addMessages('loaded ' + message.count + ' rows.')

      switch (params.period) {
        case 'daily' :
          makeDailyTable(message.data, message.count)
          break
        case 'weekly' :
          makePeriodicTable(message.data, message.count, 'w')
          break
        case 'monthly' :
          makePeriodicTable(message.data, message.count, 'm')
          break
        case 'quarterly' :
          makePeriodicTable(message.data, message.count, 'q')
          break
      }
    },
    error: function (message) {
        appIsBusy(false)
      }
  })
}

function makeDailyTable (data, count)
{
  var date
  var company
  var ordersType
  var paid
  var pCompany = ''
  var pDate = ''
  var rows = []
  var total
  var vat

  for (var i = 0; i < count; i++)
  {
    date = data[i].orders_date

    if (date === pDate)
    {
      date = ''
    } else {
      pDate = date
    }

    company = data[i].company
    if (company === null)
    {
      company = 'other'
    }

    if (company === pCompany)
    {
      company = ''
    } else {
      pCompany = company
    }

    ordersType = data[i].orders_type
    if (ordersType === null)
    {
      ordersType = 'other'
    }

    paid = parseFloat(data[i].paid)
    vat = parseFloat(data[i].vat)
    total = parseFloat(data[i].total)

    paid = isNaN(paid) ? 0 : paid
    vat = isNaN(vat) ? 0 : vat
    total = isNaN(total) ? 0 : total

    rows.push([
      date,
      pCompany,
      data[i].count,
      ordersType.toLowerCase(),
      paid,
      vat,
      total
    ])
  }
  drawTable({
    columns: columns,
    rows: rows
  })
}

function makePeriodicTable (data, count, period)
{
  var company
  var date
  var datum
  var paid
  var periodEnd = moment(params.dateFrom)
  var periodRows = {}
  var row
  var rows = []
  var strdate
  var total
  var vat

  switch (period) {
    case 'w' :
      periodEnd.day('sunday')
      if (periodEnd.isBefore(moment(params.dateFrom)))
      {
        periodEnd.day(7)
      }
      break
    case 'm' :
      periodEnd.date(1).month(periodEnd.month() + 1).subtract(1, 'd')
      break
    case 'q' :
      var m = (periodEnd.quarter() * 3) + 1
      periodEnd.date(1).month(m).subtract(1, 'd')
      break
  }

  console.log('periodEnd: ' + periodEnd.format('YYYY-MM-DD'))
  progressBarReset(count)

  for (var i = 0; i < count; i++)
  {
    datum = data[i]
    date = moment(datum.orders_date)
    console.log('date: ' + date.format('YYYY-MM-DD'))

    if (date.isAfter(periodEnd))
    {
      strdate = periodEnd.format('YYYY-MM-DD')
      console.log(periodRows)
      for (company in periodRows)
      {
        row = periodRows[company]
        rows.push([strdate, company, row.count, '(todo)', row.paid, row.vat, row.total])
        console.log([strdate, company, row.count, '(todo)', row.paid, row.vat, row.total])
      }
      periodRows = []
      switch (period) {
        case 'w' :
          periodEnd.add(7, 'd')
          break
        case 'm' :
          periodEnd.add(1, 'd').add(1, 'M').subtract(1, 'd')
          break
        case 'q' :
          periodEnd.add(1, 'd').add(3, 'M').subtract(1, 'd')
          break
      }
      console.log('period end: ' + periodEnd.format('YYYY-MM-DD'))
    }

    if (datum.company === null)
    {
      datum.company = 'other'
    }
    if (periodRows[datum.company] === undefined)
    {
      periodRows[datum.company] = {
        count: 0,
        paid: 0,
        total: 0,
        vat: 0
      }
    }

    count = parseInt(datum.count, 10)
    paid = parseFloat(datum.paid)
    total = parseFloat(datum.total)
    vat = parseFloat(datum.vat)

    count = isNaN(count) ? 0 : count
    paid = isNaN(paid) ? 0 : paid
    total = isNaN(total) ? 0 : total
    vat = isNaN(vat) ? 0 : vat

    periodRows[datum.company].count = Number(periodRows[datum.company].count) + count
    periodRows[datum.company].paid = Number(periodRows[datum.company].paid) + paid
    periodRows[datum.company].total = Number(periodRows[datum.company].total) + total
    periodRows[datum.company].vat = Number(periodRows[datum.company].vat) + vat

    progressProgressBar()
  }

  strdate = periodEnd.format('YYYY-MM-DD')

  for (company in periodRows)
  {
    row = periodRows[company]
    rows.push([strdate, company, row.count, '(todo)', row.paid, row.vat, row.total])
    console.log([strdate, company, row.count, '(todo)', row.paid, row.vat, row.total])
  }

  drawTable({
    columns: columns,
    numberColumns: [4, 5, 6],
    rows: rows
  })
}
