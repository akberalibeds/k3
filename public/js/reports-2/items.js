// @author Giomani Designs (Development Team)
//
//
var tableData
function downloadAsPdf () {
  $('#transfer-cache [name="headers"]').val(tableData.headers)
  $('#transfer-cache [name="rows"]').val(tableData.rows)
  $('#transfer-cache [name="title"]').val(params.title)
}
function downloadAsExcel () {
  $('#transfer-cache').attr('action', 'http://giomani.local/reports-2/excel')
  $('#transfer-cache [name="headers"]').val(tableData.headers)
  $('#transfer-cache [name="rows"]').val(tableData.rows)
  $('#transfer-cache [name="title"]').val(params.title)
}
function getItemsData () {
  var companies
  appIsBusy(true, 'Loading data...')
  $.form({
    method: 'GET',
    contentType: 'application/json',
    data: null,
    url: '/reports-2/data/items/' + params.dateFrom + '/' + params.dateTo,
    success: function (message) {
      addMessages('loaded ' + message.count + ' rows.')
      addMessages('assembling table.')
      companies = []
      for (var c in params.companies) {
        if (params.companies[c]) {
          companies.push(c.toLowerCase())
        }
      }
      makeDailyTable(message.data, message.count, companies)
      // switch (params.period) {
      //   case 'daily' :
      //     makeDailyTable(message.data, message.count, companies)
      //     break
      //   case 'weekly' :
      //     makePeriodicTable(message.data, message.count, 'w')
      //     break
      //   case 'monthly' :
      //     makePeriodicTable(message.data, message.count, 'm')
      //     break
      //   case 'quarterly' :
      //     makePeriodicTable(message.data, message.count, 'q')
      //     break
      // }
      showDownloadButtons(true)
      appIsBusy(false)
    },
    error: function (message) {
      appIsBusy(false)
    }
  })
}
function addPeriodicRows (periodRows, strDate) {
  var count
  var pItemCode = ''
  var periodItemCount = 0
  var rows = []
  for (var itemCode in periodRows) {
    for (var company in periodRows[itemCode]) {
      count = periodRows[itemCode][company]
      rows.push([strDate, itemCode, company, count])
      strDate = ''
      periodItemCount += count
    }
    rows.push(['******', itemCode + 'Total = ', '', periodItemCount])
    periodItemCount = 0
  }
  return rows
}
function makeDailyTable (data, count, companies) {
  var column
  var columns = []
  var company
  var totalCount
  var date
  var fDate
  var headings = []
  var headers = []
  var i, j
  var itemCode
  var itemCodes = []
  var itemCounts = []
  var pDate = ''
  var row
  var rows = []
  headings.push({ heading: 'SKU', type: 'string' })
  headers.push('SKU')
  for (i = 0; i < count; i++) {
    company = data[i].company
    if (company === null || company === '') {
      company = 'other'
    }
    company = company.toLowerCase()
    if (companies.indexOf(company) !== -1) {
      date = data[i].date
      if (date !== pDate) {
        columns.push(date)
        pDate = date
        fDate = moment(pDate).format('MMM D')
        headings.push({ heading: fDate, type: 'number' })
        headers.push(fDate)
      }
      column = columns.indexOf(pDate)
      itemCode = data[i].item_code
      if (itemCode === null || itemCode === '') {
        itemCode = '[unknown]'
      }
      if (itemCodes.indexOf(itemCode) === -1) {
        itemCodes.push(itemCode)
        itemCounts[itemCode] = []
      }
      if (itemCounts[itemCode][column] === undefined) {
        itemCounts[itemCode][column] = 0
      }
      itemCounts[itemCode][column] += parseInt(data[i].count, 10)
    }
  }
  itemCodes.forEach(function (itemCode) {
    totalCount = 0
    row = []
    row.push(itemCode)
    for (i = 0, j = headings.length - 1; i < j; i++) {
      if (itemCounts[itemCode][i] === undefined) {
        itemCounts[itemCode][i] = 0
      }
      row.push(itemCounts[itemCode][i])
      totalCount += itemCounts[itemCode][i]
    }
    if (headings.length > 2) {
      row.push(totalCount)
    }
    rows.push(row)
  })
  setTableRowsCount (rows.length - 1)
  if (headings.length > 2) {
    headings.push({ heading: 'Totals', type: 'number' })
    headers.push('Totals')
  }
  tableData = {
    headers: headers,
    rows: rows
  }
  drawTable({
    columns: headings,
    rows: tableData.rows
  })
}
