// @author Giomani Designs (Development Team)
//
$(function () {
  if (unreadBulletins.length > 0) {
    var ul = '<ul id="bulletins-confirmed-read">'
    for (var i = 0, j = unreadBulletins.length; i < j; i++) {
      ul += '<li><strong>' + unreadBulletins[i].title + '</strong>'
      ul += '<input data-bulletin-id="' + unreadBulletins[i].id + '" style="float:right" type="checkbox">'
      ul += '<br>' + unreadBulletins[i].message + '<hr>'
    }

    bootbox.dialog({
      message: '' +
        '<div class="row">' +
          '<div class="col-md-12">' +
            ul +
          '<div>' +
        '</div>',
      size: 'large',
      title: 'Bulletins',
      buttons: {
        success: {
          label: 'Confirm as read',
          className: 'btn-default',
          callback: function () {
            var all = true
            var bIds = []
            var cBoxes = document.querySelectorAll('#bulletins-confirmed-read input[type="checkbox"]')

            console.log(cBoxes)
            for (var i = 0; i < cBoxes.length; ++i) {
              if (!cBoxes[i].checked) {
                all = false
              } else {
                bIds.push(cBoxes[i].dataset.bulletinId)
              }
            }
            if (all) {
              $.form({
                method: 'PUT',
                contentType: 'application/json',
                url: '/bulletins/' + bIds.join('|') + '/confirm-as-read'
              })
            }
            return all
          }
        }
      }
    })
  }
})
