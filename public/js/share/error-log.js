// @author Giomani Designs (Development Team)
//
function errorLogger (error) {
  $.notify({
    message: '<p>There was an error.</p><p>' + error + '</p><p>Please inform an administrator.</p>'
  }, {
    delay: forever
  })

}
