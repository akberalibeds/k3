// @author Giomani Designs
//
function addBookmark () {
  var currentUrl = window.location.href
  var value = ''

  if (document.getElementsByTagName('title').length > 0) {
    value = document.getElementsByTagName('title')[0].innerHTML
  }

  bootbox.prompt({
    callback: function (result) {
      if (result !== null) {
        $.form({
          method: 'PUT',
          contentType: 'application/json',
          data: JSON.stringify({url: currentUrl, description: result}),
          url: '/profile/add-bookmark',
          success: function (msg) {
            $('#user-bookmarks').append($('<a>', {class: 'bookmark', href: msg.url}).append($('<li>', {class: 'message'}).html(msg.description)))
          },
          error: null
        })
      }
    },
    title: 'Description for &apos;' + currentUrl,
    value: value

  })
}
