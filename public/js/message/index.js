// @author Jason Fleet (jason.fleet@googlemail.com)
//
tableResults.setC({
  objectIdProperty: 'id',
  rowClickFn: function (row) {
    clickOnRow(row.conversation_id)
  },
  rowRendererFn: function (row) {
    return renderRow(row)
  },
  selectable: true,
  showCounts: false,
  selectControls: false,
  url: '/message/show'
})
tableResults.init()
//
var conversationId
var toIds
//
function clickOnRow (id) {
  showConversation(id)
}
function composeMessage (to) {
  var message =
    '<form class="form-horizontal">' +
    '  <div class="form-group">' +
    '    <label for="compose-message-to-address" class="col-sm-2 control-label">To</label>' +
    '    <div class="col-sm-10">' +
    getAddressBook() +
    '    </div>' +
    '  </div>' +
    '  <div class="form-group">' +
    '    <label for="subject" class="col-sm-2 control-label">Subject</label>' +
    '    <div class="col-sm-10">' +
    '      <input class="form-control" id="subject-text" name="subject-text" placeholder="subject">' +
    '    </div>' +
    '  </div>' + messageInputHtml() +
    '</form>'
  bootbox.dialog({
    message: message,
    title: 'Compose new message',
    buttons: {
      cancel: {
        label: 'Cancel',
        className: 'btn-defult',
        callback: function () {
          return
        }
      },
      send: {
        label: 'Send',
        className: 'btn-primary',
        callback: function () {
          var message = $('#compose-message-text').val()
          var sendTo = []
          var subject = $('#subject-text').val()
          appIsBusy(true)
          $('#address-book').each(function (i, obj) {
            sendTo.push($(obj).val())
          })
          $.form({
            method: 'POST',
            contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
            data: {
              'conversation': conversationId,
              'message': message,
              'subject': subject,
              'to': sendTo.join('|')
            },
            url: '/message/send',
            success: function (response) {
              appIsBusy(false)
            },
            error: function (response) {
              appIsBusy(false)
            }
          })
        }
      }
    }
  }).init(function () {
    $('#address-book').selectpicker({
      style: 'btn-default',
      size: 4
    })
  })
}
function getAddressBook () {
  var ab
  if (toIds === undefined) {
    toIds = []
  }
  ab = '<select class="form-control" data-live-search="true" data-none-selected-text="select addressees" data-size="12" id="address-book" multiple>'
  for (var i = 0, j = addressBook.length; i < j; i++) {
    ab += '<option data-subtext="' + addressBook[i].email + '"'
    if (toIds.indexOf(addressBook[i].id) != -1) {
      ab += ' selected'
    }
    ab += ' value="' + addressBook[i].id + '">' + addressBook[i].name + '</option>'
  }
  ab += '</select>'
  return ab
}
function getRow (message) {
  var reads = (message.is_reads).split(';')
  var css

  if (reads.indexOf('0') !== -1) {
    css = 'active strong'
  } else {
    css = 'active'
  }
  return {
    html: '<td data-from_id="' + message.from_id + '">' + message.from_name + '&nbsp(' + message.count + ')</td><td>' + message.subject + '</td><td>' + message.message + '</td>',
    rowClass: css
  }
}
function messageInputHtml () {
  return '' +
  '<div class="form-group">' +
    '<label for="message" class="col-sm-2 control-label">Message</label>' +
    '<div class="col-sm-10">' +
      '<textarea class="form-control" id="compose-message-text" name="compose-message-text" placeholder="message" style="height:296px"></textarea>' +
    '</div>' +
  '</div>'
}
function getDate(date){
	
	
	date = new Date(date);
	var weekday=new Array(7);
	  weekday[0]="Sunday";
	  weekday[1]="Monday";
	  weekday[2]="Tuesday";
	  weekday[3]="Wednesday";
	  weekday[4]="Thursday";
	  weekday[5]="Friday";
	  weekday[6]="Saturday";
	
	var today = new Date();
	var todayString = today.getDate() + today.getMonth() + today.getFullYear();
	var dateString =  date.getDate() + date.getMonth() + date.getFullYear();
	
	
	if(todayString==dateString){
		var hours = date.getHours().toString();
		hours = hours.length == 1 ? "0"+hours : hours;
		var minutes = date.getMinutes().toString();
		minutes = minutes.length == 1 ? "0"+minutes : minutes;
		
		return "Today " + hours + ":" + minutes;
	}
	
	var hours = date.getHours().toString();
	hours = hours.length == 1 ? "0"+hours : hours;
	var minutes = date.getMinutes().toString();
	minutes = minutes.length == 1 ? "0"+minutes : minutes;
	
	return weekday[date.getDay()] + " " +hours + ":" + minutes;
}
function renderRow (row) {
	
  var subject = (row.subject) ? row.subject : 'No Subject';
  var read = row.is_read ? '' : '<i class="fa fa-envelope" aria-hidden="true"></i>';
  var message = row.message.substr(0, 25)+'...'; 


  return '<td>' + read + '</td>'+
  		 '<td class="text-primary"><big>' + row.from_email + '</big></td>' +
  		 '<td>' + '<b>' + subject + '</b> - ' + message + '</td>' +
	     '<td class="text-right">' + getDate(row.created_at) + '</td>'
}
function showConversation (id) {
  appIsBusy(true)
  $.form({
    method: 'GET',
    url: '/message/read/' + id,
    success: function (response) {
      var message
      var messageHtml = '<ul class="list-unstyled">'
      toIds = []
      for (var i = 0, j = response.length; i < j; i++) {
        message = response[i]
        conversationId = message.conversation_id
        toIds.push(message.from_id)
        toIds.push(message.to_id)
        messageHtml += '<li>From: ' + message.from_name + ' (' + message.from_email + ')</li>'
        messageHtml += '<small style="display:block;float:right">' + (i + 1) + '</small>'
        messageHtml += '<li>Sent: ' + message.created_at + '</li>'
        messageHtml += '<li>Subject: ' + message.subject + '</li>'
        messageHtml += '<li><p>' + message.message + '</p></li>'
        messageHtml += '<li><hr></li>'
      }
      messageHtml += '</ul>'
      appIsBusy(false)
      bootbox.dialog({
        message: messageHtml,
        title: 'Message',
        buttons: {
          close: {
            label: 'Close',
            className: 'btn-default',
            callback: function () {
              return true
            }
          },
          reply: {
            label: 'Relpy',
            className: 'btn-default',
            callback: function (self) {
              console.log(self)
              composeMessage()
              return true
            }
          }
        }
      })
    },
    error: function (response) {
      console.log(response)
      appIsBusy(false)
    }
  })
}
