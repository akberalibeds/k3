//
//
$('#import-container').importCsv({
  afterTablefn: null,
  beforeUploadFn: function (data) {
    data.delivery_message = $('#delivery_message').val()
    return true
  },
  headingsUrl: '/tracking/bedtastic-tracking/headings',
  uiControlsFn: function () {
    return '<div class="checkbox">' +
        '<label>' +
          'delivery <input id="delivery_message" name="delivery_message" type="text">' +
        '</label>' +
      '</div>'
  },
  uploadUrl: '/tracking/bedtastic-tracking/store'
})
