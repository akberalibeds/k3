-- MySQL dump 10.13  Distrib 5.7.21, for Linux (x86_64)
--
-- Host: localhost    Database: giomani
-- ------------------------------------------------------
-- Server version	5.7.21-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `description` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `display_name` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `level` int(3) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `roles_name_unique` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` VALUES (1,'Root','Root',NULL,'root',NULL,NULL),(2,'User is allowed to manage and edit other users','User Administrator',NULL,'admin',NULL,NULL),(3,'Basic permissions, enough to handle callers.','Call Staff',NULL,'call_staff',NULL,NULL),(4,'Can view code that is in development.','Developer',NULL,'developer',NULL,NULL),(5,'Can view management reports, administrate users and post bulletins.','Manager',NULL,'manager',NULL,NULL),(6,'Basic user permissions (change password etc.)','Staff',NULL,'staff',NULL,NULL),(7,'Manage drivers and deliveries','Deliveries',NULL,'deliveries',NULL,NULL),(8,'TODO','Wholesale',NULL,'wholesale',NULL,NULL),(9,'A seller - they can manage their own account','A Seller',NULL,'seller',NULL,NULL),(10,'TODO','Deliveries Manager',NULL,'deliveries_manager',NULL,NULL),(11,'People can see paypal','Paypal Group',NULL,'paypal_group',NULL,NULL),(12,'Used for testing new stuff.','Tester Account',NULL,'tester',NULL,NULL),(13,'Able to manage the post orders','Manage post',NULL,'post',NULL,NULL),(14,'Administers system properties.','System Administrator',NULL,'sys_admin',NULL,NULL),(15,'Back office admin functions.','Back Office',NULL,'back_office',NULL,NULL),(16,'Administrate Orders (set processed etc)','Order Admin',NULL,'order_admin',NULL,NULL),(17,'Reports, bulletins, etc','Front Office',NULL,'front_office',NULL,NULL),(18,'Use this to test Permissions.','Testing ',NULL,'testing',NULL,NULL),(19,'Sort Deliveries Order','Delivery Sorting',NULL,'del_sort',NULL,NULL);
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permissions`
--

DROP TABLE IF EXISTS `permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permissions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `permissions_name_unique` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permissions`
--

LOCK TABLES `permissions` WRITE;
/*!40000 ALTER TABLE `permissions` DISABLE KEYS */;
INSERT INTO `permissions` VALUES (2,'change_password','Change password.','User is allowed to change own password.',NULL,NULL),(3,'user_admin','User Administrator','Can create and delete users.',NULL,NULL),(4,'stock_view','Stock View','Can view stock and stock categories',NULL,NULL),(5,'in_dev','In Development','Code that is &apos;in development&apos;',NULL,NULL),(6,'reports_view','Reports View','User can view management reports.',NULL,NULL),(7,'bulletin_create','Bulletins Create','Create a Bulletin broadcast message.',NULL,NULL),(8,'manage_perms_roles','Permissions and Roles Management','Manage the Permission and Roles',NULL,NULL),(9,'driver_show','Drivers View','Manage the drivers and driver mates',NULL,NULL),(10,'deliveries','Deliveries','Manage deliveries',NULL,NULL),(11,'seller_view','Seller View','Can view seller accounts',NULL,NULL),(12,'seller_admin','Seller Administrator','Can administrate seller accounts',NULL,NULL),(13,'seller_account','Seller','Seller account (manage their own)',NULL,NULL),(14,'stock_admin','Stock Administrator','Can block, unblock, edit and change quantities of stock items.',NULL,NULL),(15,'seller_orders_import','Seller Import Orders','Import Seller orders from CSV.',NULL,NULL),(16,'vehicle_view','Vehicle View','Can view vehicle details',NULL,NULL),(17,'driver_admin','Driver Adminstrator','Can add, edit and delete drivers',NULL,NULL),(18,'vehicle_admin','Vehicle Administrator','Can add, delete and edit vehicle details',NULL,NULL),(19,'post_show','Post View','Can view post',NULL,NULL),(20,'post_admin','Post Administrator','Can administrate Post',NULL,NULL),(21,'supplier_view','Supplier View','Can view suppliers',NULL,NULL),(22,'supplier_admin','Supplier Administrator','Can add, edit and delete suppliers, and set invoices as paid',NULL,NULL),(23,'dashboard_admin','Dashboard Administrator','Adminstrate the dashboard content',NULL,NULL),(24,'maillist_create','Mail-List Create','Crate a mail-list from orders',NULL,NULL),(25,'paypal','Paypal','Permission',NULL,NULL),(26,'tester','Tester','For tester to gain access to functionalty in test.',NULL,NULL),(28,'ip_whitelisting','Manage IP Filtering','Can manage the ip addresses the IP whitelist.',NULL,NULL),(29,'process_orders','Process Orders','Process / UnProcess Orders',NULL,NULL),(30,'dispatch_view','Dispatch View','Can view dispatch links',NULL,NULL),(31,'new_sale','New Sale','Can make a new sale',NULL,NULL),(32,'admin','Admin','Access to Admin Dashboard',NULL,NULL),(33,'cancelled_orders_view','Cancelled Orders View','View the cancelled orders',NULL,NULL),(34,'order_priority','Order priority','Can change the priority of an order',NULL,NULL),(35,'delivery_sort','Sort Delivery Order','Can chang the order of deliveries in route',NULL,NULL);
/*!40000 ALTER TABLE `permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permission_role`
--

DROP TABLE IF EXISTS `permission_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permission_role` (
  `permission_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`permission_id`,`role_id`),
  KEY `permission_role_role_id_foreign` (`role_id`),
  CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permission_role`
--

LOCK TABLES `permission_role` WRITE;
/*!40000 ALTER TABLE `permission_role` DISABLE KEYS */;
INSERT INTO `permission_role` VALUES (2,1),(3,1),(4,1),(5,1),(6,1),(7,1),(8,1),(9,1),(10,1),(11,1),(12,1),(13,1),(14,1),(15,1),(16,1),(17,1),(18,1),(19,1),(20,1),(21,1),(22,1),(23,1),(24,1),(25,1),(26,1),(28,1),(29,1),(30,1),(31,1),(32,1),(33,1),(34,1),(3,2),(23,2),(32,2),(10,3),(30,3),(31,3),(5,4),(32,4),(7,5),(23,5),(33,5),(2,6),(4,6),(29,6),(10,7),(11,8),(12,8),(15,8),(21,8),(32,8),(13,9),(15,9),(9,10),(16,10),(17,10),(18,10),(30,10),(35,10),(26,12),(19,13),(20,13),(8,14),(11,15),(12,15),(14,15),(16,15),(18,15),(21,15),(22,15),(23,15),(32,15),(33,15),(22,16),(29,16),(20,17),(21,17),(22,17),(30,17),(32,17);
/*!40000 ALTER TABLE `permission_role` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-01-30 11:25:09
