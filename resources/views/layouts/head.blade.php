<head>
  <title>@yield('title')</title>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  @foreach ($headers as $key => $value)
  <meta name="{{ $key }}" content="{{ $value }}">
  @endforeach
  <!-- CSS Libs -->
  <link rel="stylesheet" type="text/css" href="{{ URL::asset('css/giomani.min.css') }}">
  <link rel="stylesheet" type="text/css" href="{{ URL::asset('css/bootstrap-select.css') }}">
  <link rel="stylesheet" type="text/css" href="{{ URL::asset('css/bootstrap-switch.min.css') }}">
  <link rel="stylesheet" type="text/css" href="{{ URL::asset('css/font-awesome.min.css') }}">
  <!-- CSS App -->
  <link rel="stylesheet" type="text/css" href="{{ URL::asset('css/style.css') }}">
  <link rel="stylesheet" type="text/css" href="{{ URL::asset('css/themes/flat-blue.css') }}">
  <link rel="stylesheet" type="text/css" href="{{ URL::asset('css/jquery.dataTables.min.css') }}">

  <link rel="stylesheet" href="{{ URL::asset('js/picker/themes/default.css') }}">
  <link rel="stylesheet" href="{{ URL::asset('js/picker/themes/default.date.css') }}">
  <link rel="stylesheet" href="{{ URL::asset('css/bootstrap-notify.css') }}" rel="stylesheet">
  <link rel="stylesheet" href="{{ URL::asset('css/checkbox.css') }}" rel="stylesheet">
  <link rel="stylesheet" href="{{ URL::asset('css/pnotify.custom.min.css') }}" rel="stylesheet">
  @yield('css')
  
  @stack('headlinks')
  
 <script>
 (function (a, c, b, e) { a[b] = a[b] || {}; a[b].initial = { accountCode: "GIOMA11113", host: "GIOMA11113.pcapredict.com" }; a[b].on = a[b].on || function () { (a[b].onq = a[b].onq || []).push(arguments) }; var d = c.createElement("script"); d.async = !0; d.src = e; c = c.getElementsByTagName("script")[0]; c.parentNode.insertBefore(d, c) })(window, document, "pca", "//GIOMA11113.pcapredict.com/js/sensor.js");
 </script>

</head>
