<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Fonts -->
  <link href='http://fonts.googleapis.com/css?family=Roboto+Condensed:300,400' rel='stylesheet' type='text/css'>
  <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700,900' rel='stylesheet' type='text/css'>
  <!-- CSS Libs -->
  <link rel="stylesheet" type="text/css" href="{{ URL::asset('css/giomani.min.css') }}">
  <link rel="stylesheet" type="text/css" href="{{ URL::asset('css/font-awesome.min.css') }}">
  <!-- CSS App -->
  @stack('headlinks')

</head>

<body>
  @yield('content')
</body>
