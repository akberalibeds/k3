@include($view)
<?php $modal_id = 'dialog-'.str_replace( '.','-',$view);?>
<div id = "{{$modal_id}}" class="modal fade" role="dialog" tabindex="-1">

  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" 
          data-dismiss="modal" 
          aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title"  id="uploadModalLabel">@yield('title')</h4>
      </div>
      <div class="modal-body">
          @yield('form')
             </div>
      <div class="modal-footer">
@yield('buttons')
      </div>
    </div>
  </div>
</div>
<div class="processing" style="position:absolute;top:50%;z-index: 1000000;left:50%;background-color: white;border:solid black 1px; padding: 20px;display: none;">
    <h2>Processing...</h2>
<img src="/img/ajax-loader.gif" />
</div>
@push('js')
<script>
  (function ($) {
  var spinner = $('.processing').hide().detach().appendTo('body');
  $('.modal').each(function(n,modal) {
    function bindForm(form) {
      $(modal).find('.modal-footer button[type=submit]').click(function(){
        form.submit();
      });
      form.submit(function(e) {
        e.preventDefault();
        spinner.show();
        var formData = new FormData(this);
        $.ajax({
          data:formData,
          type: 'post',
          url: $(this).attr('action'),
          cache:false,
          contentType:false,
          processData:false,
          success: function(response) {
              spinner.hide();
              
              $(modal).find('.modal-body').html(response);
              $(modal).find('.modal-footer').empty().html($(modal).find('.buttons').detach().html());
              $(modal).find('.modal-title').html($(modal).find('.title').detach().html());
              bindForm($(modal).find('form'));
          }
        });
      });
    }
    bindForm($(modal).find('form'));
  });
  $('.modal').on('show.bs.modal', function () {
    $('.modal .modal-body').css('overflow-y', 'auto'); 
    $('.modal .modal-body').css('max-height', $(window).height() * 0.7);
  });
  })(jQuery);
  </script>
@endpush
