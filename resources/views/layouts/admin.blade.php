<!DOCTYPE html>
<html lang="en">
@include('layouts.head')
<body class="flat-blue">
  <div class="app-container">
    <div class="row content-container">
      <nav class="navbar navbar-inverse navbar-fixed-top navbar-top">
        <div class="container-fluid">
          <div class="navbar-header">
            <button type="button" class="navbar-expand-toggle">
              <i class="fa fa-bars icon"></i>
            </button>
            <ol class="breadcrumb navbar-breadcrumb">
              <li class="active">Giomani Designs</li>
            </ol>
            <button type="button" class="navbar-right-expand-toggle pull-right visible-xs">
              <i class="fa fa-th icon"></i>
            </button>
          </div>
          <ul class="nav navbar-nav navbar-right">
            <button type="button" class="navbar-right-expand-toggle pull-right visible-xs">
              <i class="fa fa-times icon"></i>
            </button>
            <li class="dropdown profile">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">{{ Auth::user()->name }} ({{ Auth::user()->email }})<span class="caret"></span></a>
              <ul class="dropdown-menu animated fadeInDown">
                <li>
                  <div class="profile-info">
                    <h4 class="username">{{ Auth::user()->name }}</h4>
                    <p>{{ Auth::user()->email }}</p>
                    <div class="btn-group margin-bottom-2x" role="group">
                      <a class="btn btn-default" href="{{ url('/profile') }}" type="button"><i class="fa fa-user"></i> Profile</a>
                      <a class="btn btn-default" href="{{ url('/logout') }}" type="button"><i class="fa fa-sign-out"></i> Logout</a>
                    </div>
                  </div>
                </li>
              </ul>
            </li>
          </ul>
        </div>
      </nav>
      <div class="side-menu sidebar-inverse">
        <nav class="navbar navbar-default" role="navigation">
          <div class="side-menu-container">
            <div class="navbar-header navbar-admin-header">
              <a class="navbar-brand" href="#">
                <div class="icon fa fa-paper-plane"></div>
                <div class="title"></div>
              </a>
              <button type="button" class="navbar-expand-toggle pull-right visible-xs">
                <i class="fa fa-times icon"></i>
              </button>
            </div>
            <ul class="nav navbar-nav">
              <li class="">
                <a href="{{ route('admin::index') }}">
                  <span class="icon fa fa-tachometer" aria-hidden="true"></span><span class="title">Admin</span>
                </a>
              </li>
              <li class="panel panel-default dropdown">
                <a data-toggle="collapse" href="#dropdown-api">
                  <span class="icon fa fa-key" aria-hidden="true"></span><span class="title">API Keys</span>
                </a>
                 <div id="dropdown-api" class="panel-collapse collapse">
                  <div class="panel-body">
                    <ul class="nav navbar-nav">
                      <li>
                        <a href="{{ route('admin::ebay::index') }}">
                          <span class="icon fa fa-file-text-o" aria-hidden="true"></span><span class="title">Ebay API</span>
                        </a>
                      </li>
                      <li>
                        <a href="{{ route('admin::maps::index') }}">
                          <span class="icon fa fa-map-marker" aria-hidden="true"></span><span class="title">Maps API</span>
                        </a>
                      </li>
                    </ul>
                  </div>
                </div>
              </li>
              <li class="panel panel-default dropdown">
                <a data-toggle="collapse" href="#dropdown-admin">
                  <span class="icon fa fa-list" aria-hidden="true"></span><span class="title">Dashboard</span>
                </a>
                <!-- Dropdown level 1 -->
                <div id="dropdown-admin" class="panel-collapse collapse">
                  <div class="panel-body">
                    <ul class="nav navbar-nav">
                      <li>
                        <a href="{{ route('admin::dashboard::show', ['info' => 'documents']) }}">
                          <span class="icon fa fa-file-text-o" aria-hidden="true"></span><span class="title">Documents</span>
                        </a>
                      </li>
                      <li>
                        <a href="{{ route('admin::dashboard::show', ['info' => 'emails']) }}">
                          <span class="icon fa fa-envelope" aria-hidden="true"></span><span class="title">Email Contacts</span>
                        </a>
                      </li>
                      <li>
                        <a href="{{ route('admin::dashboard::show', ['info' => 'links']) }}">
                          <span class="icon fa fa-link" aria-hidden="true"></span><span class="title">Links</span>
                        </a>
                      </li>
                      <li>
                        <a href="{{ route('admin::dashboard::show', ['info' => 'numbers']) }}">
                          <span class="icon fa fa-phone" aria-hidden="true"></span><span class="title">Telephone Numbers</span>
                        </a>
                      </li>
                    </ul>
                  </div>
                </div>
              </li>
              <li class="panel panel-default dropdown">
                <a data-toggle="collapse" href="#dropdown-deliveries">
                  <span class="icon fa fa-map-signs" aria-hidden="true"></span><span class="title">Deliveries</span>
                </a>
                <!-- Dropdown level 1 -->
                <div id="dropdown-deliveries" class="panel-collapse collapse">
                  <div class="panel-body">
                    <ul class="nav navbar-nav">
                      <li>
                        <a href="{{ route('admin::free-saturdays::index') }}">
                          <span class="icon fa fa-check" aria-hidden="true"></span><span class="title">Free Saturdays</span>
                        </a>
                      </li>
                      <li>
                        <a href="{{ route('admin::postcode-exceptions::index') }}">
                          <span class="icon fa fa-times" aria-hidden="true"></span><span class="title">Postcode Exceptions</span>
                        </a>
                      </li>
                      <li>
                        <a href="{{ route('admin::routes::index') }}">
                          <span class="icon fa fa-map-signs" aria-hidden="true"></span><span class="title">Routes</span>
                        </a>
                      </li>
                    </ul>
                  </div>
                </div>
              </li>
              <li class="">
                <a href="{{ route('admin::tasks::index') }}">
                  <span class="icon fa fa-clock-o" aria-hidden="true"></span><span class="title">Cron Tasks</span>
                </a>
              </li>
              <li class="">
                <a href="{{ route('manufacturing-error::index') }}">
                  <span class="icon fa fa-clock-o" aria-hidden="true"></span><span class="title">Manufacturing Errors</span>
                </a>
              </li>
              <li class="panel panel-default dropdown">
                <a data-toggle="collapse" href="#dropdown-orders">
                  <span class="icon fa fa-truck" aria-hidden="true"></span><span class="title">Delivery Staff</span>
                </a>
                <!-- Dropdown level 1 -->
                <div id="dropdown-orders" class="panel-collapse collapse">
                  <div class="panel-body">
                    <ul class="nav navbar-nav">
                      <li>
                        <a href="{{ route('admin::drivers::index') }}">
                          <span class="icon fa fa-user" aria-hidden="true"></span><span class="title">Driver</span>
                        </a>
                      </li>
                      <li>
                        <a href="{{ route('admin::driver-mates::index') }}">
                          <span class="icon fa fa-user" aria-hidden="true"></span><span class="title">Driver Mates</span>
                        </a>
                      </li>
                      <li>
                        <a href="{{ route('admin::pickers::index') }}">
                          <span class="icon fa fa-user" aria-hidden="true"></span><span class="title">Pickers</span>
                        </a>
                      </li>
                    </ul>
                  </div>
                </div>
              </li>
              <li class="panel panel-default dropdown">
                <a data-toggle="collapse" href="#dropdown-orders">
                  <span class="icon fa fa-th" aria-hidden="true"></span><span class="title">Orders</span>
                </a>
                <!-- Dropdown level 1 -->
                <div id="dropdown-orders" class="panel-collapse collapse">
                  <div class="panel-body">
                    <ul class="nav navbar-nav">
                      <li>
                        <a href="{{ route('admin::order-payment-types::index') }}">
                          <span class="icon fa fa-usd" aria-hidden="true"></span><span class="title">Payment Types</span>
                        </a>
                      </li>
                    </ul>
                  </div>
                </div>
              </li>
              <li class="panel panel-default dropdown">
                <a data-toggle="collapse" href="#dropdown-sellers">
                  <span class="icon fa fa-usd" aria-hidden="true"></span><span class="title">Sellers</span>
                </a>
                <div id="dropdown-sellers" class="panel-collapse collapse">
                  <div class="panel-body">
                    <ul class="nav navbar-nav">
                      <li>
                        <a href="{{ route('admin::sellers::index') }}">
                          <span class="icon fa fa-usd" aria-hidden="true"></span><span class="title">Sellers</span>
                        </a>
                      </li>
                      <li>
                        <a href="{{ route('admin::seller-items::index') }}">
                          <span class="icon fa fa-usd" aria-hidden="true"></span><span class="title">Seller Items</span>
                        </a>
                      </li>
                    </ul>
                  </div>
                </div>
              </li>
              <li class="panel panel-default dropdown">
                <a data-toggle="collapse" href="#dropdown-suppliers">
                  <span class="icon fa fa-battery-three-quarters" aria-hidden="true"></span><span class="title">Suppliers</span>
                </a>
                <!-- Dropdown level 1 -->
                <div id="dropdown-suppliers" class="panel-collapse collapse">
                  <div class="panel-body">
                    <ul class="nav navbar-nav">
                      <li>
                        <a href="{{ route('admin::suppliers::index') }}">
                          <span class="icon fa fa-battery-three-quarters" aria-hidden="true"></span><span class="title">Suppliers</span>
                        </a>
                      </li>
                    </ul>
                  </div>
                </div>
              </li>
              <li class="panel panel-default dropdown">
                <a data-toggle="collapse" href="#dropdown-stock">
                  <span class="icon fa fa-cubes" aria-hidden="true"></span><span class="title">Stock</span>
                </a>
                <div id="dropdown-stock" class="panel-collapse collapse">
                  <div class="panel-body">
                    <ul class="nav navbar-nav">
                      <li>
                        <a href="{{ route('admin::stock::index') }}">
                          <span class="icon fa fa-cubes" aria-hidden="true"></span><span class="title">Stock</span>
                        </a>
                      </li>
                      <li>
                        <a href="{{ route('admin::stock-categories::index') }}">
                          <span class="icon fa fa-cube" aria-hidden="true"></span><span class="title">Sales Categories</span>
                        </a>
                      </li>
                      <li>
                        <a href="{{ route('admin::stock-stock-categories::index') }}">
                          <span class="icon fa fa-cube" aria-hidden="true"></span><span class="title">Stock Categories</span>
                        </a>
                      </li>
                      <li>
                        <a href="{{ route('admin::stock-log::index') }}">
                          <span class="icon fa fa-tree" aria-hidden="true"></span><span class="title">Log</span>
                        </a>
                      </li>
                      <li>
                        <a href="{{ route('admin::stock-costs-import::index') }}">
                          <span class="icon fa fa-upload" aria-hidden="true"></span><span class="title">Import Costs</span>
                        </a>
                      </li>
                      <li>
                        <a href="{{ route('admin::stock-upload::index') }}">
                          <span class="icon fa fa-upload" aria-hidden="true"></span><span class="title">Upload</span>
                        </a>
                      </li>
                      
                      <li>
                        <a href="/admin/ebay/listings">
                          <span class="icon fa fa-upload" aria-hidden="true"></span><span class="title">Ebay Listings</span>
                        </a>
                      </li>
                    </ul>
                  </div>
                </div>
              </li>
              <li class="panel panel-default dropdown">
                <a data-toggle="collapse" href="#dropdown-users">
                  <span class="icon fa fa-users" aria-hidden="true"></span><span class="title">User Admin</span>
                </a>
                <div id="dropdown-users" class="panel-collapse collapse">
                  <div class="panel-body">
                    <ul class="nav navbar-nav">
                      <li>
                        <a href="{{ route('admin::users::index') }}">
                          <span class="icon fa fa-file-text-o" aria-hidden="true"></span><span class="title">Users</span>
                        </a>
                      </li>
                      <li>
                        <a href="{{ route('admin::roles-permissions::index') }}">
                          <span class="icon fa fa-circle-o" aria-hidden="true"></span><span class="title">Roles &amp; Permissions</span>
                        </a>
                      </li>
                      <li>
                        <a href="{{ route('admin::ips::index') }}">
                          <span class="icon fa fa-circle-o" aria-hidden="true"></span><span class="title">IP Address Whitelist</span>
                        </a>
                      </li>
                    </ul>
                  </div>
                </div>
              </li>
              @permission('vehicle_admin')
              <li class="panel panel-default dropdown">
                <a data-toggle="collapse" href="#dropdown-vehicles">
                  <span class="icon fa fa-truck" aria-hidden="true"></span><span class="title">Vehicles</span>
                </a>
                <div id="dropdown-vehicles" class="panel-collapse collapse">
                  <div class="panel-body">
                    <ul class="nav navbar-nav">
                      <li>
                        <a href="{{ route('admin::vehicles::index') }}">
                          <span class="icon fa fa-truck" aria-hidden="true"></span><span class="title">Vehicles</span>
                        </a>
                      </li>
                      <li>
                        <a href="{{ route('admin::vehicle-costs::index') }}">
                          <span class="icon fa fa-clone" aria-hidden="true"></span><span class="title">Vehicle Costs</span>
                        </a>
                      </li>
                      <li>
                        <a href="{{ route('admin::vehicle-costs::index') }}">
                          <span class="icon fa fa-clone" aria-hidden="true"></span><span class="title">Vehicle Costs</span>
                        </a>
                      </li>
                    </ul>
                  </div>
                </div>
              </li>
              @endpermission
              @role('root')
              <li class="">
                <a href="{{ route('admin::eod::index') }}">
                  <span class="icon fa fa-moon-o" aria-hidden="true" aria-hidden="true"></span><span class="title">End of Day Summary</span>
                </a>
              </li>
              <li><hr></li>
              <li class="">
                <a href="{{ route('admin::iam::index') }}">
                  <span class="icon fa fa-eye" aria-hidden="true" aria-hidden="true"></span><span class="title">Iam</span>
                </a>
              </li>
              <li class="">
                <a href="{{ route('admin::log::index') }}">
                  <span class="icon fa fa-tree" aria-hidden="true" aria-hidden="true"></span><span class="title">Log</span>
                </a>
              </li>
              <li><hr></li>
              @endrole
              <li class="">
                <a href="{{ route('home::index') }}">
                  <span class="icon fa fa-arrow-left" aria-hidden="true" aria-hidden="true"></span><span class="title">back to app</span>
                </a>
              </li>

            </ul>
          </div>
        </nav>
      </div>
      <div class="container-fluid">
        <div class="side-body padding-top">
          @yield('content')
        </div>
      </div>
      @include('layouts.footer')
    </div>
  </div>
  <div class="stash">
    @stack('html_stash')
  </div>
  @include('layouts.script')
</body>
</html>
