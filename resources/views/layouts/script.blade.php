@include('share.bulletins')
<script type="text/javascript" src="{{ URL::asset('js/jquery.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/library/moment.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/bootstrap-switch.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/jquery.matchHeight-min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/app.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/ajax.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/library/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/bootbox/bootbox.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/picker/picker.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/picker/picker.date.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/picker/legacy.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/bootstrap-notify.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/bootstrap-select/bootstrap-select.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/library/busy-kit.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/library/extras-kit.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/pnotify.custom.min.js') }}"></script>
@if (config('other.bulletins'))
<script type="text/javascript" src="{{ URL::asset('js/share/bulletins.js') }}"></script>
@endif
<script>
var _token='{{ csrf_token() }}'
/*
 *
Notification
 *
 */
function notify(msg,type) {
  type = type ? type : 'info'
  $.notify({
    message: msg
  },
  {
    type: type,
  })
}



function getNewMessages(){

	if(!document.hasFocus()){
		console.log("page in background!!")
		window.setTimeout(function(){
			getNewMessages();
		},30000);
		return;
	}
	
	$.get("/message/new",function(data){

			var shown = [];
			if(data.length){
				$.each(data,function(index,message){
						var time = message.created_at;
							time = time.split(" ");
							time = time[1];
					    new PNotify({
					      title: '<div class="pull-right"><small>'+time+'</small></div>'+message.from_name,
					      text: '<h5>'+message.message+'...</h5><a href="#" onclick="openMessage(this)">View</a>',
					      styling: 'bootstrap3',
					      type: 'info',
					      icon: false,
					      hide: false,
					      buttons:{
					      	sticker: false
					      },
					      desktop:{
						      	desktop: true
						  }
					    });
					
					shown.push(message.id);
				});
			}

			$.get("/message/new/"+shown.join(':'));

			console.log(data);
			window.setTimeout(function(){
				getNewMessages();
			},30000);
	});
}


function openMessage(message){
	$(message).closest('.ui-pnotify').find('.ui-pnotify-closer').click();
	window.open('/message');
}

$(document).ready(function(e){
	window.setTimeout(function(){
		getNewMessages();
	},3000);
});
</script>

@yield('js')
@stack('js')
