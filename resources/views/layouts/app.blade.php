<!DOCTYPE html>
<html lang="en">
@include('layouts.head')
<body class="flat-blue">
  <div class="app-container">
    <div class="row content-container">
      <nav class="navbar navbar-inverse navbar-fixed-top navbar-top">
        <div class="container-fluid">
          <div class="navbar-header">
            @if (Auth::check())
            <button type="button" class="navbar-expand-toggle">
              <i class="fa fa-bars icon"></i>
            </button>
            @endif
            <ol class="breadcrumb navbar-breadcrumb">
              <li class="active">Giomani Designs</li>
            </ol>
            <button type="button" class="navbar-right-expand-toggle pull-right visible-xs">
              <i class="fa fa-th icon"></i>
            </button>
          </div>
          @if (Auth::check())
          <ul class="nav navbar-nav navbar-right">
            <button type="button" class="navbar-right-expand-toggle pull-right visible-xs">
              <i class="fa fa-times icon"></i>
            </button>
            @permission('tester')
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" title="mail-lists" aria-expanded="false"><i class="fa fa-envelope-o" aria-hidden="true"></i></a>
            </li>
            @endpermission
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" title="bookmarks" aria-expanded="false"><i class="fa fa-bookmark" aria-hidden="true"></i></a>
              @include('share.bookmarks')
            </li>
            <!-- li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-comments-o"></i></a>
              <ul class="dropdown-menu animated fadeInDown">
                
                <li class="list-group-item">
                  <div class="pull-right" style="color:#808080;"><small>time</small></div>sender<br>
                  <small><span style="color:#808080;">message here. blah blah blah</span></small>
                </li>
                
                <li class="list-group-item">
                  <div class="pull-right" style="color:#808080;"><small>time</small></div>sender<br>
                  <small><span style="color:#808080;">message here. blah blah blah</span></small>
                </li>
                
              </ul>
            </li -->
            
            <li class="dropdown profile">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">{{ Auth::user()->name }} ({{ Auth::user()->email }})<span class="caret"></span></a>
              <ul class="dropdown-menu animated fadeInDown">
                <li>
                  <div class="profile-info">
                    <h4 class="username">{{ Auth::user()->name }}</h4>
                    <p>{{ Auth::user()->email }}</p>
                    <div class="btn-group margin-bottom-2x" role="group">
                      @role('staff')
                      <a class="btn btn-default" href="{{ url('/profile') }}" type="button"><i class="fa fa-user"></i> Profile</a>
                      @endrole
                      <a class="btn btn-default" href="{{ url('/logout') }}" type="button"><i class="fa fa-sign-out"></i> Logout</a>
                    </div>
                  </div>
                </li>
              </ul>
            </li>
          </ul>
          @else
          @include('share.not-logged-in')
          @endif
        </div>
      </nav>
      @if (Auth::check())
      <div class="side-menu sidebar-inverse">
        <nav class="navbar navbar-default" role="navigation">
          <div class="side-menu-container">
            <div class="navbar-header">
              <a class="navbar-brand" href="#">
                <div class="icon fa fa-paper-plane"></div>
                <div class="title"></div>
              </a>
              <button type="button" class="navbar-expand-toggle pull-right visible-xs">
                <i class="fa fa-times icon"></i>
              </button>
            </div>
            <ul class="nav navbar-nav">
              <li class="">
                <a href="{{ url('/') }}">
                  <span class="icon fa fa-tachometer" aria-hidden="true"></span><span class="title">Dashboard</span>
                </a>
              </li>
              @permission('new_sale')
              <li class="">
                <a href="{{ url('/sale') }}">
                  <span class="icon fa fa-desktop" aria-hidden="true"></span><span class="title">New Sale</span>
                </a>
              </li>
              @endpermission
              @permission('process_orders')
              <li class="panel panel-default dropdown">
                <a data-toggle="collapse" href="#dropdown-orders">
                  <span class="icon fa fa-th" aria-hidden="true"></span><span class="title">Orders</span>
                </a>
                <div id="dropdown-orders" class="panel-collapse collapse">
                  <div class="panel-body">
                    <ul class="nav navbar-nav">
                      <li>
                        <a href="{{ url('/orders ') }}">
                          <span class="icon fa fa-th" aria-hidden="true" aria-hidden="true"></span><span class="title">Orders</span>
                        </a>
                      </li>
                      <li>
                        <a href="{{ url('/orders/after-sales') }}">
                          <span class="icon fa fa-ship" aria-hidden="true" aria-hidden="true"></span><span class="title">After Sales</span>
                        </a>
                      </li>
                      @permission('cancelled_orders_view')
                      <li>
                        <a href="{{ url('orders/cancelled') }}">
                          <span class="icon fa fa-times" aria-hidden="true" aria-hidden="true"></span><span class="title">Cancelled Orders</span>
                        </a>
                      </li>
                      @endpermission
                      <li>
                        <a href="{{ url('/orders/collect') }}">
                          <span class="icon fa fa-bars fa-rotate-90" aria-hidden="true" aria-hidden="true"></span><span class="title">Collections</span>
                        </a>
                      </li>
                      <li>
                        <a href="{{ url('/orders/collected') }}">
                          <span class="icon fa fa-bars" aria-hidden="true" aria-hidden="true"></span><span class="title">Collected</span>
                        </a>
                      </li>
                      <li>
                        <a href="{{ url('/orders/escalations') }}">
                          <span class="icon fa fa-angle-double-up fa-2x" aria-hidden="true" aria-hidden="true"></span><span class="title">Escalations</span>
                        </a>
                      </li>
                      <li>
                        <a href="{{ url('/orders/feedback') }}">
                          <span class="icon fa fa-quote-right" aria-hidden="true" aria-hidden="true"></span><span class="title">Feedback</span>
                        </a>
                      </li>
                      <li>
                        <a href="{{ url('/orders/not-loaded') }}">
                          <span class="icon fa fa-clock-o" aria-hidden="true" aria-hidden="true"></span><span class="title">Not Loaded</span>
                        </a>
                      </li>
                      <li>
                        <a href="{{ url('/orders/on-hold') }}">
                          <span class="icon fa fa-hand-paper-o" aria-hidden="true" aria-hidden="true"></span><span class="title">On Hold</span>
                        </a>
                      </li>
                      <li>
                        <a href="{{ url('/orders/paypal') }}">
                          <span class="icon fa fa-paypal" aria-hidden="true" aria-hidden="true"></span><span class="title">PayPal</span>
                        </a>
                      </li>
                      @permission('post_show')
                      <li>
                        <a href="{{ url('/orders/post') }}">
                          <span class="icon fa fa-envelope-o" aria-hidden="true"></span><span class="title">Post</span>
                        </a>
                      </li>
                      @endpermission
                      <li>
                        <a href="{{ url('/orders/refunds') }}">
                          <span class="icon fa fa-compress" aria-hidden="true" aria-hidden="true"></span><span class="title">Refunds</span>
                        </a>
                      </li>
                      <li>
                        <a href="{{ url('/orders/retention') }}">
                          <span class="icon fa fa-pause" aria-hidden="true" aria-hidden="true"></span><span class="title">Retention</span>
                        </a>
                      </li>
                      <li>
                        <a href="{{ url('/orders/tuffnells') }}">
                          <span class="icon fa fa-truck" aria-hidden="true" aria-hidden="true"></span><span class="title">Tuffnells</span>
                        </a>
                      </li>
                      <li>
                        <a href="{{ url('/orders/urgent') }}">
                          <span class="icon fa fa-clock-o" aria-hidden="true" aria-hidden="true"></span><span class="title">Urgent</span>
                        </a>
                      </li>
                      <li>
                        <a href="{{ url('/orders/warehouse-errors') }}">
                          <span class="icon fa fa-building-o" aria-hidden="true" aria-hidden="true"></span><span class="title">Warehouse Errors</span>
                        </a>
                      </li>
                      <li>
                        <a href="{{ url('/orders/wholesale-errors') }}">
                          <span class="icon fa fa-circle-o" aria-hidden="true" aria-hidden="true"></span><span class="title">Wholesale Errors</span>
                        </a>
                      </li>
                       <li>
                        <a href="{{ url('/orders/delivery-issue') }}">
                          <span class="icon fa fa-circle-o" aria-hidden="true" aria-hidden="true"></span><span class="title">Delivery Issue</span>
                        </a>
                      </li>
                      <li>
                        <a href="{{ url('/orders/unconfirmed') }}">
                          <span class="icon fa fa-circle-o" aria-hidden="true" aria-hidden="true"></span><span class="title">Unconfirmed</span>
                        </a>
                      </li>
                    </ul>
                  </div>
                </div>
              </li>
              @endpermission
              @permission('dispatch_view')
              <li class="panel panel-default dropdown">
                <a data-toggle="collapse" href="#dropdown-dispatch">
                  <span class="icon glyphicon glyphicon-road" aria-hidden="true"></span><span class="title">Dispatch</span>
                </a>
                <div id="dropdown-dispatch" class="panel-collapse collapse">
                  <div class="panel-body">
                    <ul class="nav navbar-nav">
                      <li>
                        <a href="{{ url('/dispatch/deliveries') }}">
                          <span class="icon fa fa-car" aria-hidden="true"></span><span class="title">Deliveries</span>
                        </a>
                      </li>
                      <li>
                        <a href="{{ url('/dispatch/deliveries/days') }}">
                          <span class="icon fa fa-sun-o" aria-hidden="true"></span><span class="title">Days</span>
                        </a>
                      </li>
                      <li>
                        <a href="{{ url('/dispatch/deliveries/pool') }}">
                          <span class="icon fa fa-square-o" aria-hidden="true"></span><span class="title">Pool</span>
                        </a>
                      </li>
                      <li>
                        <a href="{{ url('/dispatch/dispatched') }}">
                          <span class="icon fa fa-truck" aria-hidden="true"></span><span class="title">Dispatched</span>
                        </a>
                      </li>
                      <!--li>
                        <a href="{{ route('tracking::index') }}">
                          <span class="icon glyphicon glyphicon-import" aria-hidden="true"></span><span class="title">Import Tracking</span>
                        </a>
                      </li-->
                      <li>
                        <a href="{{ url('/load-counts') }}">
                          <span class="icon glyphicon glyphicon-import" aria-hidden="true"></span><span class="title">Load Counts</span>
                        </a>
                      </li>
                      <li>
                        <a href="{{ url('/routes') }}">
                          <span class="icon fa fa-map-signs" aria-hidden="true"></span><span class="title">Routes</span>
                        </a>
                      </li>
                      <li>
                        <a href="/review/routes">
                          <span class="icon fa fa-check-square-o" aria-hidden="true"></span><span class="title">Mark deliveries</span>
                        </a>
                      </li>
                    </ul>
                  </div>
                </div>
              </li>
              @endpermission
              @permission('maillist_create')
              <li class="">
                <a href="{{ url('/mail-lists') }}">
                  <span class="icon fa fa-envelope-o" aria-hidden="true"></span><span class="title">Mail Lists</span>
                </a>
              </li>
              @endpermission
              @permission(['driver_show','vehicle_view'])
              <li class="panel panel-default dropdown">
                <a data-toggle="collapse" href="#dropdown-drivers">
                  <span class="icon fa fa-truck"></span><span class="title">Transport</span>
                </a>
                <div id="dropdown-drivers" class="panel-collapse collapse">
                  <div class="panel-body">
                    <ul class="nav navbar-nav">
                      @permission('driver_show')
                      <li>
                        <a href="{{ url('/drivers') }}">
                          <span class="icon fa fa-user" aria-hidden="true" aria-hidden="true"></span><span class="title">Drivers</span>
                        </a>
                      </li>
                      <li>
                        <a href="{{ url('/driver-mates') }}">
                          <span class="icon fa fa-user" aria-hidden="true" aria-hidden="true"></span><span class="title">Driver Mates</span>
                        </a>
                      </li>
                      @endpermission
                      @permission('vehicle_view')
                      <li>
                        <a href="{{ url('/vehicles') }}">
                          <span class="icon fa fa-truck" aria-hidden="true" aria-hidden="true"></span><span class="title">Vehicles</span>
                        </a>
                      </li>
                      @endpermission
                    </ul>
                  </div>
                </div>
              </li>
              @endpermission
              @permission('reports_view')
              <li class="panel panel-default dropdown">
                <a data-toggle="collapse" href="#dropdown-reports-2">
                  <span class="icon fa fa-table"></span><span class="title">Reports</span>
                </a>
                <div id="dropdown-reports-2" class="panel-collapse collapse">
                  <div class="panel-body">
                    <ul class="nav navbar-nav">
                      <li>
                        <a href="{{ route('reports-2::items') }}">
                          <span class="icon fa fa-user" aria-hidden="true" aria-hidden="true"></span><span class="title">Items</span>
                        </a>
                      </li>
                      <li>
                        <a href="{{ route('due-report::index') }}">
                          <span class="icon fa fa-align-justify" aria-hidden="true" aria-hidden="true"></span><span class="title">Due Report</span>
                        </a>
                      </li>
                      <li>
                        <a href="/orders/duplicates">
                          <span class="icon fa fa-align-justify" aria-hidden="true" aria-hidden="true"></span><span class="title">Duplicate orders</span>
                        </a>
                      </li>
                      <li>
                        <a href="/orders/authorisations/refunds">
                          <span class="icon fa fa-align-justify" aria-hidden="true" aria-hidden="true"></span><span class="title">Refunded orders</span>
                        </a>
                      </li>
                      <li>
                        <a href="/orders/authorisations/discounts">
                          <span class="icon fa fa-align-justify" aria-hidden="true" aria-hidden="true"></span><span class="title">Discounted orders</span>
                        </a>
                      </li>
                    </ul>
                  </div>
                </div>
              </li>
              @endpermission
              @permission('stock_view')
              <li class="panel panel-default dropdown">
                <a data-toggle="collapse" href="#dropdown-stock">
                  <span class="icon fa fa-cubes" aria-hidden="true" aria-hidden="true"></span><span class="title">Stock</span>
                </a>
                <div id="dropdown-stock" class="panel-collapse collapse">
                  <div class="panel-body">
                    <ul class="nav navbar-nav">
                      <li>
                        <a href="{{ url('/stock') }}">
                          <span class="icon fa fa-cubes" aria-hidden="true"></span><span class="title">Stock</span>
                        </a>
                      </li>
                      <li>
                        <a href="{{ url('/stock-categories') }}">
                          <span class="icon fa fa-cube" aria-hidden="true"></span><span class="title">Categories</span>
                        </a>
                        <a href="{{ url('/containers') }}">
                          <span class="icon fa fa-cube" aria-hidden="true"></span><span class="title">Containers</span>
                        </a>
                      </li>
                    </ul>
                  </div>
                </div>
              </li>
              @endpermission
              @permission('supplier_view')
              <!-- <li class="panel panel-default dropdown">
                <a data-toggle="collapse" href="#dropdown-suppliers">
                  <span class="icon fa fa-battery-three-quarters" aria-hidden="true"></span><span class="title">Suppliers</span>
                </a>
                <div id="dropdown-suppliers" class="panel-collapse collapse">
                  <div class="panel-body">
                    <ul class="nav navbar-nav">
                      <li>
                        <a href="{{ route('suppliers::index') }}">
                          <span class="icon fa fa-battery-three-quarters" aria-hidden="true"></span><span class="title">Suppliers</span>
                        </a>
                      </li>
                      <li>
                        <a href="{{ route('suppliers::index') }}">
                          <span class="icon fa fa-user" aria-hidden="true"></span><span class="title">Suppliers Orders</span>
                        </a>
                      </li>
                    </ul>
                  </div>
                </div>
              </li> -->
              <li>
                <a href="{{ route('suppliers::index') }}">
                  <span class="icon fa fa-battery-three-quarters" aria-hidden="true"></span><span class="title">Suppliers</span>
                </a>
              </li>
              @endpermission
              @permission('seller_view')
              <li class="panel panel-default dropdown">
                <a data-toggle="collapse" href="#dropdown-wholesale">
                  <span class="icon fa fa-circle-o" aria-hidden="true"></span><span class="title">Sellers</span>
                </a>
                <div id="dropdown-wholesale" class="panel-collapse collapse">
                  <div class="panel-body">
                    <ul class="nav navbar-nav">
                      <li>
                        <a href="{{ route('order-import::index') }}">
                          <span class="icon fa fa-download" aria-hidden="true"></span><span class="title">Imports</span>
                        </a>
                      </li>
                      <li>
                        <a href="{{ url('/sellers') }}">
                          <span class="icon fa fa-user" aria-hidden="true"></span><span class="title">Sellers</span>
                        </a>
                      </li>
                      <li>
                        <a href="{{ url('/seller-orders') }}">
                          <span class="icon fa fa-user" aria-hidden="true"></span><span class="title">Seller Orders</span>
                        </a>
                      </li>
                    </ul>
                  </div>
                </div>
              </li>
              @endpermission
              @permission('bulletin_create')
              <li><hr></li>
              <li class="">
                <a href="{{ url('/bulletins') }}">
                  <span class="icon fa fa-newspaper-o" aria-hidden="true"></span><span class="title">Bulletins</span>
                </a>
              </li>
              @endpermission
              <li class="">
                <a href="{{ route('notifications.index') }}">
                  <span class="icon fa fa-bell" aria-hidden="true"></span><span class="title">Notifications</span>
                </a>
              </li>
              <li class="">
                <a href="{{ route('message::index') }}">
                  <span class="icon fa fa-comments-o" aria-hidden="true"></span><span class="title">Messaging</span>
                </a>
              </li>

              @permission('tester')
              <li><hr></li>
              <li class="panel panel-default dropdown">
                <a data-toggle="collapse" href="#dropdown-testing">
                  <span class="icon fa fa-question-circle" aria-hidden="true"></span><span class="title">Testing</span>
                </a>
                <div id="dropdown-testing" class="panel-collapse collapse">
                  <div class="panel-body">
                    <ul class="nav navbar-nav">
                      <li class="">
                        <a href="{{ route('testing::call-lists::index') }}">
                          <span class="icon fa fa-phone-square" aria-hidden="true"></span><span class="title">Call Lists</span>
                        </a>
                      </li>
                    </ul>
                  </div>
                </div>
              </li>
              @endpermission
              <li>
                <a href="{{ url('/credits') }}">
                  <span class="title" style="color:rgba(0,0,0,0)">.</span>
                </a>
              </li>
            </ul>
          </div>
        </nav>
      </div>
      @endif
      <div class="container-fluid">
        <div class="side-body padding-top">
          @yield('content')
        </div>
      </div>
      @include('layouts.footer')
    </div>
  </div>
  <div class="stash">
    @stack('html_stash')
  </div>
  @include('layouts.script')
</body>
