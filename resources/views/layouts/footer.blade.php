<br class="clearfix">
<br class="clearfix">
<footer class="app-footer">
  <div class="wrapper">
    <div class="row">
      <div class="col-md-6 col-sm-6 col-xs-12">
        &copy; <?php echo date('Y'); ?> Copyright.
      </div>
      <div class="col-md-6 col-sm-6 col-xs-12 text-right">
        {{ @getHostByName(getHostName()) }} || {{ request()->ip() }} || <?php echo App\IamCookie::getCookie(); ?>
      </div>
    </div>
  </div>
</footer>
<div style="z-index:999999;" class='notifications top-right'></div>
