@extends('layouts.app')

@section('content')
<div class="row">
  <div class="col-md-8">

      @menubutton("['deliveries.php', 'Delivered']")

      <div class="col-sm-6 col-md-3">
          <a class="button-tile" href="dispatched.php">
              Dispatched
          </a>
      </div>

      <div class="col-sm-6 col-md-3">
          <a class="button-tile" onclick="window.open('pool.php')">
              Pool
          </a>
      </div>

      <div class="col-sm-6 col-md-3">
          <a class="button-tile" onclick="window.open('loadsCount.php')">
              Load Count
          </a>
      </div>

      <div class="col-sm-6 col-md-3">
          <a class="button-tile" href="days.php">
              Days
          </a>
      </div>

      <div class="col-sm-6 col-md-3">
          <a class="button-tile" href="routes.php">
              Routes
          </a>
      </div>

      <div class="col-sm-6 col-md-3">
          <a class="button-tile" href="drivers.php">
              Drivers
          </a>
      </div>

      <div class="col-sm-6 col-md-3">
          <a class="button-tile" onclick="$('.freeBut').click();">
              Free Saturdays
          </a>
      </div>

  </div>
</div>
@endsection
