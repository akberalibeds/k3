@extends('layouts.app')




@section('content')
<style>
	[class^="col-md-"]{
		margin-top:10px;	
	}
</style>

<div class="row">


								<div class="col-md-12" id="modalDefault">
                                            <div class="card">   
                                                    <div class="card-body">
                                                       <div class="progress">
                                                            <div id="progress-bars" class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
                                                        </div>
                                                        <div class="text-center"><h5>Calculating Route <span class="percent">0%</span></h5></div>
                                                    </div>
                                                </div>
                                          
                                        </div>
                                    


<div class="col-md-12">
	<div class="card">
    	<div class="card-body">
         	<div class="col-md-6 col-md-offset-3">
                <div class="col-md-2">
                	<button class="btn btn-default btn-block" onClick="directions(0, false, false, false, false)"><i class="fa fa-globe"></i> Calculate</button> 
                </div>
                <div class="col-md-2">
                	<button class="btn btn-default btn-block" onClick='startOver()'><i class="fa fa-refresh"></i> Restart</button> 
                </div>
                <div class="col-md-2">
                	<button onClick="changeTime()" class="btn btn-default btn-block"><i class="fa fa-clock-o"></i> Change Time</button> 
                </div>
                <div class="col-md-2">
                	<button class="btn btn-default btn-block" onClick='reverseRoute()'><i class="fa fa-arrows-h"></i> Reverse</button> 
                </div>
                <div class="col-md-2">
                	<button class="btn btn-default btn-block" onClick='reverseRout()'><i class="fa fa-check"></i> Use Route</button> 
                </div>
    		</div>
            <div class="clearfix"></div>
        </div>
    </div>
    
</div>



<div class="col-md-3">

	<div class="card card-info">
   <div class="card-header">
    	<div class="card-title blue" data-toggle="collapse" data-target="#one">
            <div class="title">Edit Route</div>
        </div>
     </div>
     <div id="one" class="">	
        <div class="card-body">
        	 <div id="routeDrag"></div>
        </div>
      </div>  
    </div>
    
    
    <div class="card card-info">
    <div class="card-header">
    	<div class="card-title blue" data-toggle="collapse" data-target="#two">
            <div class="title">Destinations</div>
        </div>
     </div>
     <div id="two" class="collapse">	
    	<div class="card-body">
            <div id="bulkLoader">
            <form id="listOfLocations" name="listOfLocations" onSubmit="clickedAddList(); return false;">
                <textarea class="form-control" name="inputList" rows="10" cols="70">@foreach($orders as $order){{ $order['postcode']."\n" }}@endforeach WV147HZ</textarea><br>
                <button type="button" class="btn btn-default btn-block" onClick="clickedAddList(); return false;">Add list of locations</button>
            </form>
            
            </div>
		</div>
    </div>    
    </div>
</div>




<div class="col-md-9">
	<div class="card map">
    	<div class="card-body myMap" id="map"></div>
    </div>
    
    <div class="card-footer">
    	<div id="path" class="pathdata"></div>
    </div>
    
</div>

</div>

<div>
                                        <!-- Button trigger modal -->
                                        <button style="visibility:hidden;" type="button" id="modalProgressBtn" class="btn btn-primary loading" data-toggle="modal" data-target="#modalDefaults" data-backdrop="static" data-keyboard="false">
                                            modal
                                        </button>
                                        <!-- Modal -->
                                        <div style="display: none;" class="modal fade" id="modalDefaults" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h4 class="modal-title" id="myModalLabel">Dispatching Route...</h4>
                                                    </div>
                                                    <div class="modal-body loaders" align="center">
															<i class=" fa fa-spinner fa-spin fa-2x"></i>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>



@endsection

@section('js')
<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<script type="text/javascript" src="{{ URL::asset('js/maps/BpTspSolver.js?1381134857') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/maps/directions-export.js?1327916622') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/maps/tsp.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/font-awesome/fontawesome-markers.min.js') }}"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/Sortable/1.4.2/Sortable.min.js"></script>

<script>
var items = [];
var test;

$(document).ready(function(e) {
    $('#modalDefault').hide();	
	var div = $('.myMap').offset();
	$('.myMap').css('height',($(window).height()-div.top-20)+'px');
	window.allPoints = {!! @json_encode($orders) !!};
	
	$.each(window.allPoints,function(index,row){
		items[row.id]={};
		items[row.id].items = row.items.split(",");
	});
	
	
	window.oidArray=[];
	window.lats=[];
	window.lngs=[];
	window.pc = [];
	$.each(window.allPoints,function(i,o){
		window.oidArray.push(o.id);
		window.lats.push(o.lat);
		window.lngs.push(o.lng);
		window.pc.push(o.postcode);		
	});
	
	
	window.actualTime = new Date(0,0,0,6,00,00);
	window.dispRoute='{{ $route }}';
	document.title='Map | '+window.dispRoute;
	
	google.load("maps", "3", { callback: init, other_params:"key={{ $maps_key }}" } );
	

	setTimeout(function(){ clickedAddList(); },1000);

});



  function init() {
  	if (google.loader.ClientLocation != null) {
		latLng = new google.maps.LatLng(google.loader.ClientLocation.latitude, google.loader.ClientLocation.longitude);
		loadAtStart(google.loader.ClientLocation.latitude, google.loader.ClientLocation.longitude, 8);
	} else {
		loadAtStart(52.57111, -2.0237600000000384, 8);
	}
}


function changeTime() {

var time = prompt("Please Enter Start Time","0:00");

if (time!=null && time!='0:00')
  	{

		time = time.split(":");
	var hours = time[0];
	var mins = time[1];

	  window.actualTime = new Date(0,0,0,hours,mins,00);
	  var perm = $("#sortable li");
	  var numPerm = new Array(perm.length + 2);
	  numPerm[0] = 0;
	  for (var i = 0; i < perm.length; i++) {
		numPerm[i + 1] = parseInt(perm[i].id);
	  }
	  numPerm[numPerm.length - 1] = numPerm.length - 1;
	  window.time=new Date(window.actualTime);
	  tsp.reorderSolution(numPerm, onSolveCallback);

	}

}

</script>


@endsection