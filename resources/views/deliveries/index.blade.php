@extends('layouts.app')
@section('title')
Deliveries
@endsection
@section('content')
<div class="row">
  <div class="col-md-4">
    <div class="card card-info">
      <div class=" card-header blue">
        <div class="card-title">
          <div class="title">Deliveries</div>
        </div>
      </div>
      <div class="card-body">
        <div class="table-responsive">
          <table class="table routes table-bordered table-hover table-striped routes table-condensed">
            <thead>
              <tr>
                <th>Route</th><th>Total</th>
                @for ($i = 1; $i < 4; $i++)
                <th class="{{ @Time::date()->days($i)->get('d-M-Y') }}">{{ @Time::date()->days($i)->get('d-M-Y') }}</th>
                @endfor
              </tr>
            </thead>
            <tbody>
            @foreach ($routes as $route)
            <tr onClick="" id="{{ $route->id }}"><td>{{ $route->name }}</td><td align="right">-</td>
              @for($i = 1; $i < 4; $i++)
              <td class="{{ @Time::date()->days($i)->get('d-M-Y') }}">-</td>
              @endfor
            </tr>
            @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
  <div class="col-md-8">
    <div class="card card-info">
      <div class=" card-header blue">
        <div class="card-title">
          <div class="title route_title">Route</div>
        </div>
      </div>
      <div class="card-body">
        <div class="table-responsive">
        	<table class="table result table-condensed table-striped table-bordered">
            	 <thead>
                  <tr class="topRow">
                    <th colspan="6">
                      <button class="btn btn-success viewMap">View Map</button>
                    <th class="selectHere" colspan="3">
                      <select data-live-search="true" class="route_change">
                        @foreach($routes as $route)
                        <option value="{{ $route->id }}">{{ $route->name }}</option>
                        @endforeach
                      </select>
                    </th>
                    <th>
                      <input class="check_all" type="checkbox">
                    </th>
                    <th colspan="3"><button class="btn btn-success dispatchRoute">Dispatch Route</button><button onclick="clearRoute();" class="btn btn-danger clearRoute pull-right">Clear Route</button></th>
                   
                  </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
          </div>
      </div>
    </div>
  </div>
</div>


<!-- Modal -->
<div class="modal fade" id="model-label" tabindex="-1" role="dialog" aria-labelledby="model-label">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="modal-label">Modal title</h4>
      </div>
      <div class="modal-body" id="modal-title">

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>
@endsection

@section('js')

<script>
	var deliveries = {!! $deliveries !!};

	$(window).load(function(e) {
		$('.routes').search();

		$('.topRow').hide();
        allRoutes();

    });


	function allRoutes(){
		$.each(deliveries,function(i,d){
			console.log(i+': '+d.id_delivery_route);
			var row = $('#' + d.id_delivery_route);
			var td = row.find('td').eq(1);
			var total = ($.isNumeric(parseInt(td.html())) ? parseInt(td.html()) : 0)+d.delCount;
			td.html(total);
			var str = d.deliverBy.split("-");
			if(str.length==3){
				row.find('.' + $.trim(d.deliverBy)).html(d.delCount);
			}
		});
	}



	$('.routes tbody tr').click(function(e) {
		var id = this.id;
		var route = $(this).find('td').eq(0).html();
		$('.route_title').html(route);

		if(route=='TNT Black' || route=='TNT Brown'){
			$('.dispatchRoute').show();
			console.log('show');
		}
		else{
			$('.dispatchRoute').hide();
			console.log('hide');
		}

		if(route=='Beds Blocked' || route=='Double Orders'){
			$('.clearRoute').show();
			console.log('show');
		}
		else{
			$('.clearRoute').hide();
			console.log('hide');
		}
		

		
		$('.result tbody').html('<tr><td colspan="12" align="center"><i class="fa fa-spinner fa-spin fa-3x"></i></td></tr>');
    	$.form({
			url:'/dispatch/deliveries/routeDels',
			data: 'id='+id,
			success: function(data){

				showItems($.parseJSON(data))

			}
		});

    });


var searching

function showItems(data) {
	var done
	var items
	var paid
	$('.topRow').show();
	$('#delivery-route').html(data.routeName)

	

	$('.result tbody').html('')

	$.each(data, function(i, v) {

		paid = v.p == 1 ? '<i style="color:#039;" class="fa fa-paypal"></i>' : ''
		done = v.done == 1  ? '<i style="color:green;" class="fa fa-check-circle"></i>' : ''



		items = ""
		v.items = v.items.split(",");
		for(var i=0; i < v.items.length; i++) {
			items += v.items[i] + "<br>"
		}

		$('.result tbody').append(
			'<tr lat="'+ v.lat +'" lng="' + v.lng+'" oid="' + v.id + '">' +
				'<td>' + v.dispatchType + '</td>' +
				'<td>' + paid +'</td>' +
				'<td>' + done +'</td>' +
        		'<td>' + v.companyName + '</td>' +
				'<td>' + v.startDate + '</td>' +
				'<td>' + v.deliverBy + '</td>' +
				'<td>' + v.name + '</td>' +
				'<td>' + v.town + '</td>' +
				'<td class="pc">' + v.postcode + '</td>' +
				'<td><input type="checkbox" class="orders" id=' + v.id + ' /></td>' +
				'<td>' + items + '</td>' +
				'<td><span class="label label-' + getStock(v.stockCount) + '">&nbsp;&nbsp;&nbsp;</span></td>' +
				'<td><i style="color:red;" class="fa fa-trash-o fa-lg remRow"></i></td>' +
			'</tr>')
	})
}

function getStock(str){

	str = str.split(",");

	var r=0,g=0;
	for(var zz=0;zz<str.length;zz++){
			if(str[zz]>0){ g++; }
			if(str[zz]<1){ r++;	}
	}

	if(r>0 && g>0)	{ return 'warning'; }
	else if(r>0)		{ return 'danger'; }
	else if(g>0)		{ return 'success'; }
	else			{ return 'danger';}
}


$('.check_all').change(function(e) {
    $('.orders').prop('checked',$(this).prop('checked'));
});


$('.route_change').change(function(e) {
    var route = this.value
	var id = [];
	$('.orders:checked').each(function(index, element) {
         id.push ($(element).attr('id'));
	});

	var data = {};
	data.orders = id;
	data.route = route;

		$.form({
			url:"/dispatch/deliveries/changeroute_mass",
			data: { data },
			success: function(resp){

			$('.orders:checked').each(function(index, element) {
				$(element).closest('tr').remove();
			});

			}
		});
});


$('body').on('click','.remRow',function(e){
	$(this).closest('tr').remove();
});

$('.viewMap').click(function(e) {

	var items={}
	var halt=false
	var string=''
	/*
	items[0]={}
	items[0].id=0
	items[0].lat=''
	items[0].lng=''
	items[0].items=''
	items[0].pc='WV14 7HZ'
	*/
	var orders = Array();
	$('.result tbody tr').each(function(i, element) {

		if($(element).find('span').hasClass('label-success')) {
			orders.push($(element).attr('oid'));
			/*
			items[i]={};
			items[i].id=$(element).attr('oid')
			items[i+1].lat=$(element).attr('lat')
			items[i+1].lng=$(element).attr('lng')
			items[i+1].items=$(element).find('.items').html()
			items[i+1].pc=$(element).find('.pc').html()
			*/

		} else {
			halt=true
			$(element).addClass('danger')
		}
	});

	if(!halt){
	var json = JSON.stringify(orders);
		window.open('/dispatch/deliveries/map/'+encodeURIComponent(json)+'/'+$('.route_title').html(),'_Blank')
	}
	else{
		 alert('Highlighted orders have stock Issue');
	}

});


$('body').on('dblclick','.result tbody tr',function(e) {
    	var id = $(this).attr('oid')
    	window.open('/orders/'+id)
});

function clearRoute(){
	$.get("deliveries/clear/"+$('.route_title').html(),function(response){
		
	});

}
</script>

<script src="{{ URL::asset('js/deliveries/deliveries.js') }}" type="text/javascript"></script>


@endsection
