@extends('layouts.app')
@section('title')
Reports 2
@endsection
@section('content')
<div class="row">
  	
    <div class="card card-info">
        <div class="card-header blue">
          <div class="card-title">
          	<div class="title">
            	Daily Report
            </div>
           </div>
        </div>
        <div class="card-body">
          
          <table class="table table-striped">
          <thead>
          	<tr>
            	<th>Date</th>
                <th>Orders</th>
                <th>Total</th>
                <th>Received</th>
            </tr>
          </thead>
          <tbody>
         	
              @foreach($orders as $row) 
              <tr>
                  <td>{{ $row->startDate }}</td>
                  <td>{{ $row->count }}</td>
                  <td>{{ @number_format($row->total,2) }}</td>
                  <td>{{ @number_format($row->paid,2) }}</td>
                  
              </tr>
              @endforeach
              <tr>
                  <td>Total</td>
                  <td>{{ $totals['count'] }}</td>
                  <td>{{ @number_format($totals['total'],2) }}</td>
                  <td>{{ @number_format($totals['paid'],2) }}</td>
              </tr>
              
          </tbody>
          </table>
          
          
          
        </div>
      </div>
    
    
</div>
@endsection

@section('js')

@endsection
