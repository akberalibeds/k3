@extends('layouts.app')
@section('title')
Reports 2
@endsection
@section('content')
<div class="row">
  <div class="card card-info">
    <div class="card-header blue">
      <div class="card-title">
        <div class="title">Reports - Items</div>
      </div>
    </div>
    <div class="card-body">
      <div class="row">
        <div class="col-md-12">
          <strong>Last Update:</strong> {{ $lastUpdated }}
          <hr>
        </div>
      </div>
      <div class="row" id="reports-ui">
        <div class="col-md-2 col-sm-4 col-xs-6">
          <div id="companies">
            <strong>Companies</strong>
            <br>
            <button class="btn btn-default btn-xs" data-state="all" onclick="allCompanies(this)" type="button">all</button>
            <br>
            @foreach ($companies as $company)
            <label class="checkbox-inline">
              <input autocomplete="off" type="checkbox" value="{{ $company->company_name }}">&nbsp;{{ $company->company_name }}
            </label>
            <br>
            @endforeach
            <label class="checkbox-inline">
              <input autocomplete="off" type="checkbox" value="other">&nbsp;Other
            </label>
            <br>
          </div>
        </div>

        <!--div class="col-md-2 col-sm-4 col-xs-6">
          <div id="period">
            <strong>Period</strong>&nbsp;<span class="glyphicon glyphicon-question-sign" data-toggle="tooltip" data-placement="right" title="Data will be to nearest, inclusive of the date"></span><br>
            <label class="radio-inline">
              <input autocomplete="off" checked name="period" type="radio" value="daily">&nbsp;Daily
            </label>
            <br>
            <label class="radio-inline">
              <input autocomplete="off" name="period" type="radio" value="weekly">&nbsp;Weekly &nbsp;<span class="glyphicon glyphicon-question-sign" data-toggle="tooltip" data-placement="right" title="week is Monday to Sunday"></span><br>
            </label>
            <br>
            <label class="radio-inline">
              <input autocomplete="off" name="period" type="radio" value="monthly">&nbsp;Monthly
            </label>
            <br>
            <label class="radio-inline">
              <input autocomplete="off" name="period" type="radio" value="quarterly">&nbsp;Quarterly
            </label>
            <br>
            <label class="radio-inline">
              <input autocomplete="off" name="period" type="radio" value="yearly">&nbsp;Yearly
            </label>
            <br>
          </div>
        </div-->

        <div class="col-md-3">
          <label for="date-from">From</label>&nbsp;<span class="glyphicon glyphicon-question-sign" data-toggle="tooltip" data-placement="right" title="dates are inclisive"></span>
          <div class="input-group">
            <input autocomplete="off" class="form-control" id="date-from" placeholder="Date from" type="datetime">
            <span class="input-group-btn">
              <button class="btn btn-default" onclick="setLastStartDate()" role="button">last</button>
            </span>
          </div>
          <br>
          <label for="date-to">To</label>&nbsp;<span class="glyphicon glyphicon-question-sign" data-toggle="tooltip" data-placement="right" title="dates are inclisive"></span>
          <div class="input-group">
            <input autocomplete="off" class="form-control" id="date-to" placeholder="Date to" type="datetime">
            <span class="input-group-btn">
              <button class="btn btn-default" onclick="setSameEndDate()" role="button">same day</button>
            </span>
          </div>
        </div>

        <div class="col-md-2 col-sm-4 col-xs-6">
          <!--div id="display">
            <strong>Display</strong><br>
            <label class="radio-inline">
              <input checked name="chart-type" type="radio" value="chart">&nbsp;Chart
            </label>
            <br>
            <label class="radio-inline">
              <input name="chart-type" type="radio" value="table">&nbsp;Table
            </label>
            <br>
            <hr>
          </div-->
          <div>
            <button class="btn btn-default btn-sm" onclick="draw()" type="button">Draw Table</button>
          </div>
        </div>

        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="messages" id="messages">
          </div>
        </div>

      </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-md-10">
    <div class="h4" id="table-title"></div>
  </div>
  <div class="col-md-2 text-right">
    <form action="{{ route('reports-2::print') }}" accept-charset="UTF-8" class="form-horizontal hidden" id="transfer-cache" method="POST" target="_blank">
      {{ method_field('PUT') }}
      {{ csrf_field() }}
      <input name="headers" type="hidden">
      <input name="rows" type="hidden">
      <input name="title" type="hidden">
      <button class="btn btn-default" id="download-pdf-button" onclick="downloadAsPdf()" role="submit">View as PDF</button>
      @permission('in_dev')
      <button class="btn btn-default" id="download-excel-button" onclick="downloadAsExcel()" role="submit">Export to Excel</button>
      @endpermission
    </form>
  </div>
</div>

<div class="row">
  <div class="col-md-12">
    Showing <span id="table-rows-count"></span> rows
  </div>
</div>

<div class="container">
  <div class="row" id="dashboard_div">
    <div id="control_div"></div>
    <div id="table_div" style="max-height:400px;height:400px"></div>
  </div>
</div>
@endsection

@section('js')
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript" src="{{ URL::asset('js/reports-2/ui.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/reports-2/items.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/reports-2/tables.js') }}"></script>
@endsection
