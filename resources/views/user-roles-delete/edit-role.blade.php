@extends('layouts.app')

@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="card card-info">
      <div class=" card-header blue">
        <div class="card-title">
          <div class="title">
            <button class="btn btn-default btn-xs" onclick="javascript:window.history.back()" type="button"><i class="fa fa-arrow-left"></i></button>
            Edit User
          </div>
        </div>
      </div>
      <div class="card-body">
        <form action="{{ url('/roles/' . $user->id . '/update') }}" class="form-horizontal" method="post">
          <input type="hidden" name="id" value="{{ $user->id }}">
          {{ method_field('PUT') }}
          {!! csrf_field() !!}
          <div class="col-md-4">
            <p><strong>User</strong></p>
            <p><strong>ID: </strong>{{ $user->id }}</p>
            <p><strong>Name: </strong>{{ $user->name }}</p>
            <p><strong>Login: </strong>{{ $user->email }}</p>
            <br>
          </div>
          <div class="col-md-4">
            <strong>Roles</strong>
            @foreach ($userRoles as $role)
            <div class="checkbox">
              <label>
                <input name="{{ $role->name }}" type="checkbox" checked>
                {{ $role->display_name }}
              </label>
            </div>
            @endforeach
            @foreach ($diffRoles as $role)
            <div class="checkbox">
              <label>
                <input name="{{ $role->name }}" type="checkbox">
                {{ $role->display_name }}
              </label>
            </div>
            @endforeach
          </div>
        </form>
      </div>
      <div class="card-footer">
        <div class="text-right">
          <button class="btn btn-default" type="reset" role="button">back</button>
          &nbsp;
          <button class="btn btn-default" type="reset" role="button">reset</button>
          &nbsp;
          <button class="btn btn-default" type="submit" role="button">save</button>
        </div>
      </div>
  </div>
</div>
@endsection
