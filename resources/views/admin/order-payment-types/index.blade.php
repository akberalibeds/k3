@extends('layouts.admin')
@section('title')
Admin Order Payment Types
@endsection
@section('content')
<div class="row">
  <div class="card card-info">
    <div class="card-header blue">
      <div class="card-title">
        <div class="title">Admin Order Payment Types</div>
      </div>
    </div>
    <div class="card-body">

      <div class="col-md-12 text-right">
        <button class="btn btn-default" onclick="addType()" role="button" type="button">Add Type</button>
      </div>

      <div class="row">
        <div class="col-md-12">
          <div class="table-responsive">
            <table class="table table-condensed table-striped">
              <thead>
                <tr>
                  <th>Type</th>
                  <th>Description</th>
                </tr>
              </thead>
              <tbody id="payment-types-table">
                @foreach ($paymentTypes as $paymentType)
                <tr>
                  <td>{{ $paymentType->payment_type }}</td>
                  <td>{{ $paymentType->description }}</td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
@push('js')
<script type="text/javascript" src="{{ URL::asset('js/admin/order-payment-types/index.js') }}"></script>
@endpush
