@extends('layouts.admin')
@section('title')
Admin Stock Log
@endsection
@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="card card-info">
      <div class=" card-header blue">
        <div class="row">
          <div class="card-title">
            <div class="title">Admin Stock Log</div>
          </div>
        </div>
      </div>
      <div class="card-body">
        <div class="row">
          <div class="col-md-6 col-sm-12 col-xs-12">
            @include('share.search-bar', [
              'options' => [
                ['value' => 'itemCode', 'title' => 'Item Code'],
              ],
              'selectable' => false,
              'selectControls' => false,
            ])
          </div>
        </div>
        <div class="row">
          <div class="table-responsive">
            <table class="small table table-striped table-no-wrap table-hover" id="result-container">
              <thead>
                <tr>
                  <th>Action</th>
                  <th>Item Code</th>
                  <th class="text-right">From</th>
                  <th class="text-right">To</th>
                  <th>User</th>
                  <th>Date and Time</th>
                  <th>Status</th>
                </tr>
              </thead>
              <tbody class="table-clickable" id="table-results-container">
              </tbody>
            </table>
          </div>
        </div>
      </div>
      <div class="card-footer">
        <div class="row">
          <div class="col-md-12 text-center" id="paging-controls-container">
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
@push('js')
<script type="text/javascript">
var stockActions = new Map()
@foreach ($stockActions as $action => $description)
stockActions.set('{{ $action }}', '{!! $description !!}')
@endforeach
</script>
<script type="text/javascript" src="{{ URL::asset('js/library/table-results_0.2.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/admin/stock-log/index.js') }}"></script>
@endpush
