@extends('layouts.admin')

@section('title')
Admin User - {{ $user->name }}
@endsection

@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="card card-info">
      <div class=" card-header blue">
        <div class="card-title">
          <div class="title">
            Admin User - {{ $user->name }}
          </div>
        </div>
      </div>
      <div class="card-body">

        <div class="row">
          <div class="col-md-12">
            <div class="btn-group" role="group" aria-label="">
              <a class="btn btn-default" href="{{ route('admin::users::edit', ['id' => $user->id]) }}" type="button">Edit</a>
              <button class="btn btn-default" onclick="changePassword({{ $user->id }}, '{{ $user->name }}')" type="button">
                Change Password
              </button>
              @if ($userIsActive)
              <button class="btn btn-default" id="suspend-button" onclick="suspend({{ $user->id }})" type="button">
                Suspend
              </button>
              @endif
              <a class="btn btn-default" href="{{ route('admin::users::roles', ['id' => $user->id]) }}" type="button">
                Roles
              </a>
            </div>
          </div>
        </div>

        <div class="row">
          <div class="col-md-6">
            <dl class="dl-horizontal">
              <dt>ID<dt>
              <dd>{{ $user->id }}</dd>
              <dt>Name<dt>
              <dd>{{ $user->name }}</dd>
              <dt>Username<dt>
              <dd>{{ $user->email }}</dd>
              <dt>Status<dt>
              <dd id="user-status">@userstatus($user->status)</dd>
              <dt>Roles</td>
              <dd>
                @foreach ($userRoles as $role)
                  {{ $role->display_name }},&nbsp;
                @endforeach
              </dd>
              <dt>Last updated</td>
              <dd>@str2Datetime($user->updated_at)</dd>
            </dl>
          </div>
        </div>
      </div>
      <div class="card-footer">
      </div>
    </div>
  </div>
</div>
@endsection

@section('js')
<script type="text/javascript" src="{{ URL::asset('js/admin/users/show.js') }}"></script>
@endsection
