@extends('layouts.admin')

@section('title')
Admin User - Edit ({{ $user->email }})
@endsection

@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="card card-info">
      <div class=" card-header blue">
        <div class="card-title">
          <div class="title">
            Admin User - Edit ({{ $user->email }})
          </div>
        </div>
      </div>
      <div class="card-body">
        <div class="row">

          {!! Form::model($user, array('method' => 'put', 'route' => array('admin::users::update', $user->id))) !!}

            <div class="form-group">
              {!! Form::label('name', 'Name') !!}
              {!! Form::text('name') !!}
              {!! Form::error('name') !!}
            </div>

            <div class="form-group">
              {!! Form::label('status', 'Status') !!}
              {!! Form::select('status', App\User::$statuses) !!}
              {!! Form::error('status') !!}
            </div>

            {!! Form::buttons() !!}

          {!! Form::close() !!}

        </div>
      </div>
      <div class="card-footer">
      </div>
    </div>
  </div>
</div>
@endsection

@section('js')
<script>
$('input[type="checkbox"]').bootstrapSwitch()
</script>
@endsection
