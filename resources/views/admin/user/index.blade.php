@extends('layouts.admin')
@section('title')
Admin Users
@endsection
@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="card card-info">
      <div class=" card-header blue">
        <div class="card-title">
          <div class="title">Admin Users</div>
        </div>
      </div>
      <div class="card-body">
        <div class="row">
          <div class="col-md-6 col-sm-12 col-xs-12">
            @include('share.search-bar', [
              'options' => [
                ['value' => 'name-email', 'title' => 'Name'],
                ['value' => 'id', 'title' => 'ID'],
              ],
              'selectable' => false
            ])
          </div>
          <div class="col-md-6 col-sm-12 col-xs-12">
            <a class="btn btn-default" href="{{ route('admin::users::create') }}">Add User</a>
          </div>
        </div>
        <div class="row">
          <div class="table-responsive">
            <table class="small table table-striped table-no-wrap table-hover" id="table-results-container">
              <thead>
                <tr>
                  <th>ID</th>
                  <th>Name</th>
                  <th>Username</th>
                  <th>Roles</th>
                  <th>Status</th>
                  <th>Created</th>
                  <th>Last Updated</th>
                </tr>
              </thead>
              <tbody class="table-clickable" id="result-container">
              </body>
            </table>
          </div>
        </div>
      </div>
      <div class="card-footer">
        <div class="row">
          <div class="col-md-12 text-center" id="paging-controls-container">
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
@section('js')
<script type="text/javascript" src="{{ URL::asset('js/library/table-results_0.2.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/admin/users/index.js') }}"></script>
@endsection
