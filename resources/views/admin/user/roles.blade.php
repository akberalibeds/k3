@extends('layouts.admin')

@section('title')
Admin User Roles and Permissions for user: {{ $user->email }}
@endsection

@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="card card-info">
      <div class=" card-header blue">
        <div class="card-title">
          <div class="title">
            Admin Roles and Permissions for user: {{ $user->email }}
          </div>
        </div>
      </div>
      <div class="card-body">
        <div class="row">
          <div class="col-md-4">
            <strong>User Roles</strong>
            <div class="bordered box" id="user-roles" ondrop="dropOnUser(event)" ondragover="allowDropOnUser(event)">
              @foreach ($userRoles as $userRole)
              <span class="draggable" data-id="{{ $userRole->id }}" data-was="user" draggable="true" id="role-{{ $userRole->id }}" ondragstart="drag(event)">{{ $userRole->display_name }}</span>
              @endforeach
            </div>
          </div>
          <div class="col-md-4">
            <strong>Roles</strong>
            <div class="bordered box" id="roles" ondrop="dropOnRoles(event)" ondragover="allowDropOnRoles(event)">
              @foreach ($roles as $role)
              <span class="draggable" data-id="{{ $role->id }}" data-was="role" draggable="true" id="role-{{ $role->id }}" ondragstart="drag(event)">{{ $role->display_name }}</span>
              @endforeach
            </div>
          </div>
          <div class="col-md-4">
            <button class="btn btn-default" onclick="updateUserRoles({{ $user->id }})" type="button" role="button">Save Changes</button>
          </div>
        </div>
      </div>
      <div class="card-footer">
      </div>
    </div>
  </div>
</div>
@endsection

@section('js')
<script type="text/javascript" src="{{ URL::asset('js/admin/users/roles.js') }}"></script>
@endsection
