@extends('layouts.admin')

@section('title')
Admin Users Add
@endsection

@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="card card-info">
      <div class=" card-header blue">
        <div class="card-title">
          <div class="title">
            Admin Users Add
          </div>
        </div>
      </div>
      <div class="card-body">
        <div class="row">
          <div class="col-md-12">
            @include ('share.password-policy')
          </div>
        </div>
        <div class="row">
          <div class="col-md-12">

            {!! Form::open(array('method' => 'post', 'route' => array('admin::users::store'))) !!}

              <div class="form-group">
                {!! Form::label('name', 'Name') !!}
                {!! Form::text('name') !!}
                {!! Form::error('name') !!}
              </div>

              <div class="form-group">
                {!! Form::label('email', 'Username') !!}
                {!! Form::text('email') !!}
                {!! Form::error('email') !!}
              </div>

              <div class="form-group">
                {!! Form::label('status', 'Status') !!}
                {!! Form::select('status', App\User::$statuses) !!}
                {!! Form::error('status') !!}
              </div>

              <div class="form-group">
                {!! Form::label('password', 'Password') !!}
                {!! Form::password('password') !!}
                {!! Form::error('password') !!}
              </div>

              <div class="form-group">
                {!! Form::label('password_confirmation', 'Confirm Password') !!}
                {!! Form::password('password_confirmation') !!}
                {!! Form::error('password_confirmation') !!}
              </div>

              {!! Form::buttons() !!}

            {!! Form::close() !!}

          </div>
      </div>
      <div class="card-footer">
      </div>
    </div>
  </div>
</div>
@endsection

@push('headlinks')
<link rel="stylesheet" href="{{ URL::asset('css/strength.css') }}" rel="stylesheet">
@endpush

@section('js')
<script type="text/javascript" src="/js/library/strength.min.js"></script>
<script type="text/javascript" src="/js/library/password-strength.js"></script>
<script>
$('input[type="checkbox"]').bootstrapSwitch()
</script>
@endsection
