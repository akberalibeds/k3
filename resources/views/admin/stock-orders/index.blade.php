@extends('layouts.admin')

@section('title')
Stock Item Orders for &apos;{{ $stockItem['itemCode'] }}&apos;
@endsection

@section('content')
<div class="row">
  <div class="card card-info">
    <div class="card-header blue">
      <div class="card-title">
        <div class="title">Stock Item Orders for &apos;{{ $stockItem['itemCode'] }}&apos;</div>
      </div>
    </div>
    <div class="card-body">
      <div class="row">
        <div class="col-md-4 col-sm-6 col-xs-12">
          @include('share.search-bar', [
            'options' => [
              ['value' => 'orders.id', 'title' => 'Order ID'],
              ['value' => 'dPostcode', 'title' => 'Postcode'],
            ],
            'selectable' => false
          ])
        </div>
      </div>
      <div class="table-responsive">
        <table class="small table table-striped table-no-wrap table-clickable table-hover" id="table-results-container">
          <thead>
            <tr>
              <th>Order</th>
              <th>Invoice</th>
              <th>Buyer</th>
              <th>Customer</th>
              <th>Postcode</th>
              <th>Ref</th>
              <th>Type</th>
              <th>Status</th>
              <th>Date</th>
              <th class="text-right">Total</th>
              <th class="text-right">Paid</th>
              <th class="text-right">Balance</th>
              <th class="text-center">Done</th>
            </tr>
          </thead>
          <tbody id="result-container">
          </tbody>
        </table>
      </div>
    </div>
    <div class="card-footer">
      <div class="row">
        <div class="col-md-12 text-center" id="paging-controls-container">
        </div>
      </div>
    </div>
  </div>
</div>
@endsection

@push('js')
<script>
var stockItemId = {{ $stockItem['id'] }}
</script>
<script type="text/javascript" src="{{ URL::asset('js/library/table-results_0.2.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/admin/stock-orders/index.js') }}"></script>
@endpush
