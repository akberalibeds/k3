@extends('layouts.admin')
@section('title')
Admin Roles and Permissions
@endsection
@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="card card-info">
      <div class=" card-header blue">
        <div class="card-title">
          <div class="title">Admin Roles and Permissions</div>
        </div>
      </div>
      <div class="card-body">
        <div class="table-responsive">
          <table class="small table table-condensed table-striped table-no-wrap table-v-striped">
            <thead class="rotate">
              <tr>
                <th>
                  <button class="btn btn-default" onclick="saveChanges()" role="button" type="button">save Changes</button>
                </th>
                @foreach ($permissions as $permission)
                <th>
                  <div>
                    <span title="{{ $permission->name }}">{{ $permission->display_name }}</span>
                  </div>
                </th>
                @endforeach
              </tr>
            </thead>
            <tbody>
              @foreach ($rolesPermissions as $rolePermission)
              <tr>
                <td title="{{ $rolePermission->role_name }}">
                  {{ $rolePermission->role_display_name }}
                  <a href="#" onclick="showDescription({{ $rolePermission->id }})"><i class="fa fa-info-circle" aria-hidden="true"></i></a>
                </td>
                @foreach ($permissions as $permission)
                <?php $perms = explode(',', $rolePermission->permissions); ?>
                @if (in_array($permission->name, $perms))
                <td class="cell-clickable text-center" data-changed="0" data-original-value="1" data-permission-id="{{ $permission->id }}" data-role-id="{{ $rolePermission->role_id }}" data-value="1" onclick="editRP(this, {{ $permission->id }}, {{ $rolePermission->role_id }})">
                  @tick
                </td>
                @else
                <td class="cell-clickable text-center" data-changed="0" data-original-value="0" data-permission-id="{{ $permission->id }}" data-role-id="{{ $rolePermission->role_id }}" data-value="0" onclick="editRP(this, {{ $permission->id }}, {{ $rolePermission->role_id }})">
                  @cross
                </td>
                @endif
                @endforeach
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
      <div class="card-footer">
      </div>
    </div>
  </div>
</div>
@endsection
@section('js')
<script type="text/javascript">
var descriptions = new Map()
@foreach ($rolesPermissions as $rolesPermission)
descriptions.set({{ $rolesPermission->role_id }}, '{{ $rolesPermission->role_description }}')
@endforeach
</script>
<script type="text/javascript" src="{{ URL::asset('js/admin/roles-permissions/index.js') }}"></script>
@endsection
