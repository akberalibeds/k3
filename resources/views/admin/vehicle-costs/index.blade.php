@extends('layouts.admin')
@section('title')
Vehicles Costs
@endsection
@section('content')
<div class="row">
  <div class="card card-info">
    <div class="card-header blue">
      <div class="card-title">
        <div class="title">
          Vehicle Costs
        </div>
      </div>
    </div>
    <div class="card-body">
      <div class="row">
        <div class="col-md-6">
          @include ('share.search-bar', [
            'options' => [
              ['value' => 'reg', 'title' => 'Vehicle'],
              ['value' => 'date_incurred', 'title' => 'Date'],
              ['value' => 'type', 'title' => 'Cost Type'],
            ],
            'selectable' => false,
          ])
        </div>
        <div class="col-md-6 text-right">
          <div class="btn-group" role="group" aria-label="tools">
            <button class="btn btn-default" data-toggle="modal" data-target="#modal-vehicle-cost" type="button">Add cost</button>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <div class="table-responsive">
            <table class="small table table-striped table-no-wrap table-hover" id="table-results-container">
              <thead>
                <tr>
                  <th data-sortable="reg">Vehicle</th>
                  <th>Cost Type</th>
                  <th>Notes</th>
                  <th class="text-right">Mileage</th>
                  <th class="text-right">Cost</th>
                  <th data-sortable="date_incurred">Date</th>
                </tr>
              </thead>
              <tbody class="table-clickable" id="result-container">
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
    <div class="card-footer">
      <div class="row">
        <div class="col-md-12 text-center" id="paging-controls-container">
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
@push('html_stash')
<div class="modal fade" id="modal-vehicle-cost" tabindex="-1" role="dialog" aria-labelledby="modal-label-vehicle-cost">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="modal-label-vehicle-cost">Add vehicle cost</h4>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="form-group col-md-4">
            <label for="vehicle_id">Reg</label><br>
            <select class="selectpicker" data-container="#modal-vehicle-cost" data-live-search="true" id="vehicle_id" name="vehicle_id">
              @foreach ($vehicles as $vehicle)
              <option value="{{ $vehicle->id }}">{{ $vehicle->reg }}</option>
              @endforeach
            </select>
          </div>
          <div class="form-group col-md-4">
            <label for="vehicle_cost_type_id">Cost type</label><br>
            <select class="selectpicker" data-container="#modal-vehicle-cost" data-live-search="true" data-live-search="true" id="vehicle_cost_type_id" name="vehicle_cost_type_id">
              @foreach ($vehicleCostsTypes as $vehicleCostsType)
              <option value="{{ $vehicleCostsType->id }}">{{ $vehicleCostsType->type }}</option>
              @endforeach
            </select>
          </div>
          <div class="form-group col-md-4">
            <label for="date_incurred">Date Incurred</label><br>
            <input type="text" class="form-control" id="date_incurred" name="date_incurred" placeholder="" type="text">
          </div>
        </div>
        <div class="row">
          <div class="form-group col-md-4">
            <label for="cost">Cost</label><br>
            <input class="form-control" id="cost" name="cost" placeholder="" type="text">
          </div>
          <div class="form-group col-md-4">
            <label for="mileage">Mileage</label><br>
            <input class="form-control" id="mileage" name="mileage" placeholder="" type="text">
          </div>
        </div>
        <div class="row">
          <div class="form-group col-md-12">
            <label for="notes">Notes</label><br>
            <textarea class="form-control" id="notes" name="notes" placeholder="" rows="4" type="text"></textarea>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal" onclick="">Close</button>
        <button type="button" class="btn btn-primary" onclick="saveAndClose()">Save &amp; Close</button>
        <button type="button" class="btn btn-primary" onclick="save()">Save</button>
      </div>
    </div>
  </div>
</div>
@endpush
@push('js')
<script src="{{ URL::asset('js/library/table-results_0.2.js') }}"></script>
<script src="{{ URL::asset('js/admin/vehicle-costs/index.js') }}"></script>
@endpush
