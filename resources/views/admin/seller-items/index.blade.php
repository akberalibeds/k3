@extends('layouts.admin')
@section('title')
Admin Seller Items
@endsection
@section('content')
<div class="row">
  <div class="card card-info">
    <div class="card-header blue">
      <div class="card-title">
        <div class="title">Admin Seller Items</div>
      </div>
    </div>
    <div class="card-body">
      <div class="row">
        <div class="col-md-12 text-right">
          <div class="btn-group" role="group">
            <a class="btn btn-default" href="{{ route('admin::seller-items::create') }}">Add Item</a>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-6">
          @include('share.search-bar', [
            'options' => [
              ['value' => 'itemCode', 'title' => 'Item Code'],
              ['value' => 'itemDescription', 'title' => 'Description'],
            ],
            'selectable' => false,
          ])
        </div>
        <div class="col-md-6 text-right">
          <div class="form-group">
            <label for="id_seller">Seller</label>
            <select autocomplete="off" class="selectpicker" data-container="body" data-dropdown-align-right="true" id="select-seller" name="id_seller" onchange="tableResults.filter(event, false)">
              <option value="">All</option>
              @foreach ($sellers as $seller)
              <option value="{{ $seller->id }}">{{ $seller->name }}({{ $seller->iName }})</option>
              @endforeach
            </select>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <div class="table-responsive">
            <table class="small table table-striped table-no-wrap table-hover" id="table-results-container">
              <thead>
                <tr>
                  <th>SKU / Item Code</th>
                  <th class="text-right">Cost</th>
                  <th class="text-right">Beds</th>
                  <th class="text-right">Ebay</th>
                  <th class="text-right">Retail</th>
                  <th class="text-right">Wholesale</th>
                  <th class="text-right">Seller</th>
                  <th class="text-right">Price</th>
                </tr>
              </thead>
              <tbody class="table-clickable" id="result-container">
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
    <div class="card-footer">
      <div class="row">
        <div class="col-md-12 text-center" id="paging-controls-container">
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
@section('js')
<script src="{{ URL::asset('js/library/table-results_0.2.js') }}"></script>
<script src="{{ URL::asset('js/admin/seller-items/index.js') }}"></script>
<script>
$(function () {
  $('[data-toggle="tooltip"]').tooltip()
})
</script>
@endsection
