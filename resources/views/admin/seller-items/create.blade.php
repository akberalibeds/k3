@extends('layouts.admin')
@section('title')
Admin Seller Items - Add
@endsection
@section('content')
<div class="row">
  <div class="card card-info">
    <div class="card-header blue">
      <div class="card-title">
        <div class="title">Admin Seller Items - Add</div>
      </div>
    </div>
    <div class="card-body">
      <div class="row">
        <div class="col-md-6">
          @include('share.search-bar', [
            'options' => [
              ['value' => 'itemCode', 'title' => 'Item Code'],
              ['value' => 'itemDescription', 'title' => 'Description'],
            ],
            'selectable' => false,
          ])
        </div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <div class="table-responsive">
            <table class="small table table-striped table-no-wrap table-clickable table-hover" id="table-results-container">
              <thead>
                <tr>
                  <th>SKU / Item Code</th>
                  <th class="text-right">Cost</th>
                  <th class="text-right">Beds</th>
                  <th class="text-right">Ebay</th>
                  <th class="text-right">Retail</th>
                  <th class="text-right">Wholsale</th>
                  <th class="text-center">Withdrawn</th>
                </tr>
              </thead>
              <tbody class="table-clickable" id="result-container">
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
    <div class="card-footer">
      <div class="row">
        <div class="col-md-12 text-center" id="paging-controls-container">
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
@section('js')
<script src="{{ URL::asset('js/library/table-results_0.2.js') }}"></script>
<script src="{{ URL::asset('js/admin/seller-items/create.js') }}"></script>
<script>
$(function () {
  $('[data-toggle="tooltip"]').tooltip()
})
var sellers = []
@foreach ($sellers as $seller)
sellers.push({ id: '{{ $seller->id }}', name: '{{ $seller->name }}' })
@endforeach
</script>
@endsection
