@extends('layouts.admin')
@section('title')
Admin Seller - {{ $seller->name }} ({{ $seller->iName }})
@endsection
@section('content')
<div class="row">
  <div class="card card-info">
    <div class="card-header blue">
      <div class="card-title">
        <div class="title">Admin Seller - {{ $seller->name }} ({{ $seller->iName }})</div>
      </div>
    </div>
    <div class="card-body">
      <div class="row">
        <div class="col-md-12">
          <dl class="dl-horizontal">
            <dt>Account Limit</dt>
            <dd>&pound;{{ $seller->account_limit }}</dd>
            <dt>Discount</dt>
            <dd>{{ $seller->discount }}%</dd>
            <dt>Email</dt>
            <dd>
              <a href="mailto:{{ $seller->email }}">{{ $seller->email }}</a>
            </dd>
            <dt>iName</dt>
            <dd>{{ $seller->iName }}</dd>
            <dt>Name</dt>
            <dd>{{ $seller->name }}</dd>
            <dt>Address</dt>
            <dd>
              {{ $seller->num}} {{ $seller->street }}<br>
              {{ $seller->street }}<br>
              {{ $seller->town }}
            </dd>
            <dt>Postcode</dt>
            <dd>{{ $seller->pc }}</dd>
            <dt>Telephone</dt>
            <dd>{{ $seller->tel }}</dd>
            <dt>Force Pay</dt>
            <dd>@if ($seller->force_pay) Yes @else No @endif</dd>
          </dl>
          <a class="btn btn-default" href="{{ route('admin::sellers::edit', ['id' => $seller->id]) }}" role="button">Edit</a>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
@section('js')
<script>
var sellerId = {{ $seller->id }}
</script>
<script type="text/javascript" src="{{ URL::asset('js/library/table-results_0.2.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/admin/sellers/show.js') }}"></script>
@endsection
