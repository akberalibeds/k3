@extends('layouts.admin')
@section('title')
Admin Free Saturdays
@endsection
@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="card card-info">
      <div class=" card-header blue">
        <div class="card-title">
          <div class="title">
            Admin Free Saturdays
          </div>
        </div>
      </div>
      <div class="card-body">
        <div class="row">
          <div class="col-md-12 text-right">
            <button class="btn btn-default" onclick="saveSaturdays()">Save changes</button>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12">
            <div class="table-responsive">
              <table class="small table table-condensed table-striped table-hover table-no-wrap" id="table-results-container">
                <thead>
                  <tr>
                    <th>Date</th>
                    <th>Created by</th>
                    <th>Created at</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach ($freeSaturdays as $freeSaturday)
                  <tr>
                    <td>{{ $freeSaturday['date'] }}</td>
                    <td>{{ $freeSaturday['user_id'] or '-'}}</td>
                    <td>{{ $freeSaturday['created_at'] or '-' }}</td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
