@extends('layouts.admin')

@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="card card-info">
      <div class=" card-header blue">
        <div class="card-title">
          <div class="title">
            Picker - Edit
          </div>
        </div>
      </div>
      <div class="card-body">
        {!! Form::model($picker, array('method' => 'post', 'route' => array('admin::pickers::update', $picker->id))) !!}

          <div class="form-group">
            {!! Form::label('name', 'Name') !!}
            {!! Form::text('name') !!}
            {!! Form::error('name') !!}
          </div>

          <div class="form-group">
            {!! Form::label('num', 'Telephone') !!}
            {!! Form::tel('num') !!}
            {!! Form::error('num') !!}
          </div>
          
          
          <div class="form-group">
            {!! Form::label('uName', 'Username') !!}
            {!! Form::text('uName') !!}
            {!! Form::error('uName') !!}
          </div>
          
          <div class="form-group">
            {!! Form::label('pwd', 'Password') !!}
            {!! Form::password('pwd') !!}
            {!! Form::error('pwd') !!}
          </div>

          

          <div class="form-group">
            {!! Form::label('active', 'Active') !!}
            {!! Form::checkbox('active') !!}
            {!! Form::error('active') !!}
          </div>

          {!! Form::buttons() !!}

        {!! Form::close() !!}
      </div>
      <div class="card-footer">
      </div>
    </div>
  </div>
</div>
@endsection

@section('js')
<script>
$('input[type="checkbox"]').bootstrapSwitch()
</script>
@endsection
