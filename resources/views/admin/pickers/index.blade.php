@extends('layouts.admin')
@section('title')
Admin Pickers
@endsection
@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="card card-info">
      <div class=" card-header blue">
        <div class="card-title">
          <div class="title">Pickers</div>
        </div>
      </div>
      <div class="card-body">
        <div class="row">
        
        
          <div class="col-md-6 col-sm-12 col-xs-12">
            @include('share.search-bar', [
              'options' => [
                ['value' => 'name', 'title' => 'Picker'],
              ],
              'selectable' => true
            ])
          </div>
          <div class="col-md-6">
          	<a onClick="window.open('/admin/pickers/print')" class="btn btn-primary pull-right">Print List</a> <a onClick="window.open('/admin/pickers/create')" class="btn btn-primary pull-right">Add Picker</a>
          </div>
          
        </div>
        <div class="row">
          <div class="col-md-12">
            <div class="table-responsive">
              <table class="small table table-condensed table-striped table-no-wrap table-hover" id="table-results-container">
                <thead>
                  <tr>
                  	<th id="select-all-container"></th>
                    <th data-sortable="name">Picker</th>
                    <th data-sortable="num">Telephone</th>
                    <th class="text-center">Active</th>
                    <th class="text-right"><i class="fa fa-trash text-danger"></i></th>
                  </tr>
                </thead>
                <tbody id="result-container">
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
      <div class="card-footer">
        <div class="row">
          <div class="col-md-12 text-center" id="paging-controls-container">
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection

@section('js')
<script type="text/javascript" src="{{ URL::asset('js/library/typeahead.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/library/typeahead-kit.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/library/paging.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/library/table-results_0.2.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/admin/pickers/index.js') }}"></script>
@endsection
