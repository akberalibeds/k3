@extends('layouts.admin')

@section('title')
Admin Pickers
@endsection

@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="card card-info">
      <div class=" card-header blue">
        <div class="card-title">
          <div class="title">
            Admin Pickers
          </div>
        </div>
      </div>
      <div class="card-body">
        <div class="row">
          <div class="col-md-5">
            <dl class="dl-horizontal">
              <dt>ID</dt>
              <dd>{{ $picker->id }}</dd>
              <dt>Name</dt>
              <dd>{{ $picker->name }}</dd>
              <dt>Tel</dt>
              <dd>{{ $picker->num }}</dd>
              <dt>Username</dt>
              <dd>{{ $picker->uName }}</dd>
              <dt>Password</dt>
              <dd>{{ $picker->pwd }}</dd>
              <dt>Active</dt>
              <dd>@yesOrNo($picker->active)</dd>
            </dl>
          </div>
          <div class="col-md-1">
            <ul class="list-unstyled">
              <li>
                <a class="btn btn-default" href="{{ route('admin::pickers::edit', ['id' => $picker->id]) }}" role="button">Edit</a>
              </li>
            </ul>
          </div>
          <div class="col-md-6">
            <div class="file-drop-area" id="upload-drop" data-upload-identifier="{{ $picker->id }}">
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12">
            <ul class="bxslider" id="driver-mate-images">
              @foreach ($driverMateFiles as $file)
              <li><img src="/drivers/image/{{ $file }}" style="height:auto"></li>
              @endforeach
            </ul>
            <!--button id="fullscreen">Fullscreen</button-->
          </div>
        </div>
      </div>
      <div class="card-footer">
      </div>
    </div>
  </div>
</div>
@endsection

@push('headlinks')
<link href="{{ URL::asset('/css/jquery.bxslider.css') }}" rel="stylesheet" />
@endpush

@section('js')
<script type="text/javascript" src="{{ url ('/js/library/jquery.bxslider.min.js') }}"></script>
<script type="text/javascript" src="{{ url ('/js/library/upload.js') }}"></script>
<script type="text/javascript" src="{{ url ('/js/pickers/show.js') }}"></script>
@endsection
