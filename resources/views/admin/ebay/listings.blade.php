@extends('layouts.admin')

@section('title')
Admin Dashboard
@endsection

@section('content')


  <div class="col-md-6">
    <div class="card card-info">
      <div class=" card-header blue">
        <div class="card-title">
          <div class="title">Linked Listings</div>
        </div>
         <span class="pull-right"><a href="#" class="btn btn-primary">refresh</a></span>
      </div>
      <div class="card-body">

		
		
		<table class="table table-condensed table-dynamic table-striped table-hover main">
                                	<thead>
                                    	<tr><th>SKU</th><th>Listing ID</th><th>Qty</th><th>update</th><th></th></tr>
                                    </thead>
                                    <tbody>
                                    <?php $skus=[]; $items=[];?>
                                    @foreach($stock as $s)
                                    
										<?php 
											$items[$s->itemID] = 1;
											$skus[] = [ "sku"=> html_entity_decode($s->itemCode),'itemid' => $s->itemID ]; 
										?>
                                         
                                        <tr id='{{ $s->id }}'><td>{{ $s->itemCode }}</td><td>{{ $s->itemID }}</td><td>{{ $s->itemQty }}</td>

										 {!! ($s->updateStock==1) ? "<td>yes</td>" : "<td>no</td>" !!}
										 <td><button class="btn btn-sm" onclick="edit({{ $s->id }})"><i class="fa fa-pencil-square-o"></i></button></td>
										</tr>

									@endforeach

                        			</tbody>
                                    </table>
		
		
		
		     

      </div>
    </div>
    <br>
       <div class="row">
                                    	<div class="col-md-6">

                                    		<div class="card card-info">
                                            	<div class=" card-header blue">
											        <div class="card-title">
											          <div class="title">SKUS not on ebay</div>
											        </div>
											         <span class="pull-right"><a href="#" class="btn btn-primary">Remove</a></span>
											      </div>
                                                <div class="card-body">
                                                    <table class="table notEbay">
                                                    	<thead>
                                                        	<tr><th></th><th></th></tr>
                                                        </thead>
                                                        <tbody>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                    	</div>

                                        <div class="col-md-6">
                                        	<div class="card card-info">
 	                                            <div class=" card-header blue">
											        <div class="card-title">
											          <div class="title">SKUS not on System</div>
											        </div>
											         <span class="pull-right"><a href="#" class="btn btn-primary">Add</a></span>
											      </div>
                                            	<div class="card-body">
                                                    <table class="table notSystem">
                                                    	<thead>
                                                        	<tr><th></th><th></th></tr>
                                                        </thead>
                                                        <tbody>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                </div>
                

  
  
  
  
  
  <div class="col-md-6">
    <div class="card card-info">
      <div class=" card-header blue">
        <div class="card-title">
          <div class="title">Ebay Listings</div>
        </div>
         <span class="pull-right"><a href="#" class="btn btn-primary">refresh</a></span>
      </div>
     		<div class="card-body">
				<table class="table list">
					<thead>
						<tr><th><div class="pull-left">Linked</div><div class="text-center">Item</div></th></tr>
					</thead>
					<tbody>
						<tr><td class="text-center"><i class="fa fa-spinner fa-spin fa-2x"></i></td></tr>
					</tbody>
				</table>
			</div>
			
		 </div>
		
		     

     
   
  </div>




@endsection



@section('js')
<script>
var systemSkus = $.parseJSON('{!! @json_encode($skus) !!}');
var allItems = $.parseJSON('{!! @json_encode($items) !!}');
var allvariations = {};
var sCount = 0;

  $('.main').DataTable();

	$(window).load(function(e){
		getListings();

	});

	function getListings(){
		$.get("",function(data){
			$('.list tbody').html('');
			$.each(data,function(index,item){
				$('.list tbody').append(parseListings(index,item));
			});
			$('.list').DataTable();
			var buttons = '<div class="btn-group "><button class="btn btn-danger red">Not Linked</button><button class="btn btn-success green">Linked</button><button class="btn btn-info all">All</button></div>';
			
			$('.list').closest('.card-body').find('.dataTables_length').append('&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+buttons);
			checkListings();
		});
	}

	function parseListings(index,item){
		var skuCount = 0;
		var options = '<select class="form-control">';
		var exists = allItems.hasOwnProperty(item.ItemID) ? true : false;
		
		if(!$.isArray(item.Variations.Variation)){
			if(item.Variations.Variation.SKU!=undefined){
				options +='<option>'+item.Variations.Variation.SKU+'</option>';
				skuCount++;
				allvariations[sCount] = {};
				allvariations[sCount].sku=item.Variations.Variation.SKU;
				allvariations[sCount].id=item.ItemID;
				sCount++;
			}
		}
		else{
			$.each(item.Variations.Variation,function(i,listing){
				if(listing.SKU!=undefined){
					options +='<option>'+listing.SKU+'</option>';
					skuCount++;
					allvariations[sCount] = {};
					allvariations[sCount].sku=listing.SKU;
					allvariations[sCount].id=item.ItemID;
					sCount++;
				}
			});
			
		}					

		options +='</select>';
		var button = '<button class="pull-right btn btn-danger " onclick="unlink(this);">Un-Link</button>';
		var icon = '<i class="fa fa-check-circle fa-2x text-success"></i>';
		var clas = 'linked';
		 if(!exists){
			button='<button class="pull-right btn btn-success " onclick="link(this);">Link</button>';
			icon = '<i class="fa fa-times-circle fa-2x text-danger"></i>';
			clas = 'notlinked'; 
		}
			var html = '<tr id="'+item.ItemID+'" class="'+clas+'"><td>'+
							'<div class="col-md-1 text-center">'+
								icon +
							'</div>'+
							'<div class="col-md-2 text-center">'+
								'<img width="100%" src="'+item.PictureDetails.GalleryURL+'" />'+
							'</div>'+
							'<div class="col-md-9"><div class="row">'+
								'<div class="col-md-12">'+
									'<h4 class="list-group-item-heading">'+item.ItemID+'</h4>'+
									'<p class="list-group-item-text">'+item.Title+'</p>'+
								'</div>'+
								'<div class="col-md-4">'+
									options +
								'</div>'+
								'<div class="col-md-8">'+
									button +
									'<p>variations: ' + skuCount + '</p>' +
								'</div>'+
							'</div></div>'+
							
							'<div class="clearfix"></div>'+
						'</td></tr>';

			return html;
			
		
		
	}

	function checkListings(){

		$.each(systemSkus,function(i, row) {
				
				var sku = row.sku
				var id = row.itemid;
				var same=false;

				$.each(allvariations,function(z,item){

					//console.log(sku+'=='+item.sku+' && '+id+'=='+item.id);
					if(sku==item.sku && id==item.id){
							same=true;
					}


				});

					if(!same){
							$('.notEbay tbody').append('<tr class="danger"><td>'+sku+'</td><td>'+id+'</td></tr>');
					}

				//console.log(i);
        });

		$('.notEbay').dataTable();



		$.each(allvariations,function(i, row) {
				
				var sku = row.sku
				var id = row.id;
				var same=false;

				$.each(systemSkus,function(z,item){

					//console.log(sku+'=='+item.sku+' && '+id+'=='+item.id);
					if(sku==item.sku && id==item.itemid){
						///console.log(sku+'=='+item.sku+' && '+id+'=='+item.itemid);
							same=true;
					}


				});

					if(!same){
							$('.notSystem tbody').append('<tr class="danger"><td>'+sku+'</td><td>'+id+'</td></tr>');
							//console.log()
					}

				//console.log(i);
        });

		$('.notSystem').dataTable();

}

	function edit(id){
	
		var html = '<p>Quantity: <input type="number" min="0" class="form-control qty"><p>';
		var dialog = bootbox.dialog({
		    message: html,
		    closeButton: false,
		    buttons:{
				cancel: function(){
					
				},
				save: function(){
					var qty =$('.qty').val();
					$.get("listings/ebaystock/"+id+"/"+qty,function(data){
							$('#'+id).find('td').eq(2).html(qty);
					})
				}
		    }
		});
		
	}

	function link(item){

		var listing = {}
		var row =$(item).closest('tr');
		var id = row.attr('id');
		listing.id=id;
		var variations={};

		var vars = row.find('select').find('option');

		$.each(vars,function(i,v){
				variations[i]=$(v).html();
		});

		listing.variations = variations;

		$.get("listings/ebaystock/add/"+JSON.stringify(listing),function(data){
			row.find('td').eq(0).find('.btn').removeClass('btn-success');
			row.find('td').eq(0).find('.btn').addClass('btn-danger');
			row.find('td').eq(0).find('.btn').attr('onclick','unlink(this);');
			row.find('td').eq(0).find('.btn').html('Un-Link');
			
			row.find('td').eq(0).find('i').removeClass('fa-times-circle');
			row.find('td').eq(0).find('i').addClass('fa-check-circle');
			row.find('td').eq(0).find('i').removeClass('text-danger');
			row.find('td').eq(0).find('i').addClass('text-success');
			
			 $('.main').dataTable().fnClearTable();

			 //$.each(data,function(i,row){
				 //$('.main').dataTable().fnAddData([[row.itemCode,row.itemID,row.itemQty,(row.updateStock=='1') ? 'yes' : 'no','']]);
			// });
			$('.main').dataTable().fnAddData(data);
		});
	}

	function unlink(item){
		
		var row =$(item).closest('tr');
		var id = row.attr('id');

		$.get("listings/ebaystock/remove/"+id,function(data){

			row.find('td').eq(0).find('.btn').removeClass('btn-danger');
			row.find('td').eq(0).find('.btn').addClass('btn-success');
			row.find('td').eq(0).find('.btn').attr('onclick','link(this);');
			row.find('td').eq(0).find('.btn').html('Link');
			
			row.find('td').eq(0).find('i').removeClass('fa-check-circle');
			row.find('td').eq(0).find('i').addClass('fa-times-circle');
			row.find('td').eq(0).find('i').removeClass('text-success');
			row.find('td').eq(0).find('i').addClass('text-danger');
			
			row.find('i').attr('onclick','link(this);')
			
			

				$('.main tbody tr').each(function(index, row) {
					var tr = $(row);
						//console.log(tr.find('td').eq(1).html()+"=="+listing.id);
					if(tr.find('td').eq(1).html()==id){
						$('.main').dataTable().fnDeleteRow(tr[0]);
					}
				});

		});
		
	}
	
	/***	Filtering Functions	 ***/

	$('body').on('click','.all',function(e) {
		 $.fn.dataTableExt.afnFiltering.splice(0,1);
	  	 $('.list').dataTable().fnDraw();
	});


	$('body').on('click','.red',function(e) {
		 $.fn.dataTableExt.afnFiltering.splice(0,1);
	    $.fn.dataTableExt.afnFiltering.push(
	    function( oSettings, aData, iDataIndex ) {
	        var row = oSettings.aoData[iDataIndex].nTr;
	        return $(row).hasClass('linked') ? false : true;
	    });

		$('.list').dataTable().fnDraw();

	});

	$('body').on('click','.green',function(e) {
		$.fn.dataTableExt.afnFiltering.splice(0,1);
	    $.fn.dataTableExt.afnFiltering.push(
	    function( oSettings, aData, iDataIndex ) {
	        var row = oSettings.aoData[iDataIndex].nTr;
	        return $(row).hasClass('notlinked') ? false : true;
	    });

		$('.list').dataTable().fnDraw();
	});
</script>
@endsection