@extends('layouts.admin')

@section('title')
Admin Dashboard
@endsection

@section('content')


@if(isset($save))
	<div class="alert alert-success"> Settings Saved </div>
@endif

  <div class="col-md-12">
    <div class="card card-info">
      <div class=" card-header blue">
        <div class="card-title">
          <div class="title">Ebay Settings</div>
        </div>
      </div>
      <div class="card-body">

		        
			
           {!! Form::open() !!} 
         
          <div class="form-group col-md-12">
            {!! Form::label('name', 'Name') !!}
            {!! Form::text('name',$ebay['name']) !!}
            {!! Form::error('name') !!}
          </div>   
           
            
           <div class="form-group col-md-12">
            {!! Form::label('appID', 'App ID') !!}
            {!! Form::text('appID',$ebay['appID']) !!}
            {!! Form::error('appID') !!}
           </div> 
          
           <div class="form-group col-md-12">
            {!! Form::label('cerID', 'Cert ID') !!}
            {!! Form::text('certID',$ebay['certID']) !!}
            {!! Form::error('certID') !!}
          </div> 
          
          
          <div class="form-group col-md-12">
            {!! Form::label('devID', 'Dev ID') !!}
            {!! Form::text('devID',$ebay['devID']) !!}
            {!! Form::error('devID') !!}
          </div> 
          
          
          <div class="form-group col-md-12">
            {!! Form::label('compat_level', 'Compatibility Level') !!}
            {!! Form::text('compat_level',$ebay['compat_level']) !!}
            {!! Form::error('compat_level') !!}
          </div> 
          
          
           <div class="form-group col-md-12">
            {!! Form::label('token', 'Token') !!}
            {!! Form::textArea('token',$ebay['token']) !!}
            {!! Form::error('token') !!}
          </div> 
          
		
        {!! Form::buttons() !!}
        
        {!! Form::close() !!} 
        

      </div>
    </div>
  </div>



<style>
	.th {
		background-color:lightgrey;
	}
</style>
@endsection
