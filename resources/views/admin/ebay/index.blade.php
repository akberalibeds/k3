@extends('layouts.admin')

@section('title')
Admin Dashboard
@endsection

@section('content')

  <div class="col-md-12">
    <div class="card card-info">
      <div class=" card-header blue">
        <div class="card-title">
          <div class="title">Ebay Settings</div>
        </div>
         <span class="pull-right"><a href="/admin/ebay/edit" class="btn btn-primary">Edit</a></span>
      </div>
      <div class="card-body">

		<table class="table table-responsive">        
			
          @foreach($ebay as $key => $val)  
      		
            <tr><td class="th"><div>{{ $key }}</div></td><td><div>{{ $val }}</div></td></tr>
      		
          @endforeach  	 
		
        </table>      

      </div>
    </div>
  </div>



<style>
	.th {
		background-color:lightgrey;
		width:10%;
		
	}
	
	td div {
		padding:10px;
	}
	.table {
    table-layout: fixed;
    word-wrap: break-word;
}
</style>
@endsection
