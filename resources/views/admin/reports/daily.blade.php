@extends('layouts.admin')

@section('title')
Admin Dashboard
@endsection

@section('content')



<div class="col-md-3">
    <div class="card card-info">
      <div class=" card-header blue">
        <div class="card-title">
          <div class="title">Search</div>
        </div>
      </div>
      <div class="card-body">
		<form action="/admin/reports/search">
             <div class="form-group">
                <select name="type" class="form-control">
                    <option>Monthly</option>
                    <option selected>Daily</option>
                    <option>Items</option>
                </select>
             </div>
            
            <div class="col-md-3">
                 <div class="form-group">
                    <div class="checkbox">
                        <label class="control-label">
                        <input class="checkbox" {{ null!==(request('delivered')) ? "checked" : "" }} name="delivered" type="checkbox" />Delivered</label>
                    </div>
                 </div>
            </div> 
            <div class="col-md-3">
                 <div class="form-group">   
                    <div class="checkbox">
                        <label class="control-label">
                        <input class="checkbox" {{ null!==(request('allocated')) ? "checked" : "" }}  name="allocated" type="checkbox" />Allocated</label>
                    </div>
                 </div> 
             </div>
             <div class="col-md-3">
                 <div class="form-group">   
                    <div class="checkbox">
                        <label class="control-label">
                        <input class="checkbox" name="refund" {{ null!==(request('refund')) ? "checked" : "" }} type="checkbox" />Refunds</label>
                    </div>
                 </div> 
             </div>
             
             
                          
             <div class="form-group">
                <input class="form-control pickadate" value="{{ request('from') }}" placeholder="from" name="from" />
             </div>
             
               <div class="form-group">
                <input class="form-control pickadate" value="{{ request('to') }}" placeholder="to" name="to" />
             </div>
             
             
             <div class="form-group">
                <button class="btn btn-primary pull-right">Go</button>
             </div>
            <div class="clearfix"></div>
       </form> 
      </div>
    </div>
  </div>




  <div class="col-md-9">
  
  	 <div class="card card-info">
      <div class=" card-header blue">
        <div class="card-title">
          <div class="title">Today</div>
        </div>
      </div>
      <div class="card-body">

		<table class="table table-responsive table-bordered table-striped">        
		
          	<tr><th>Date</th><th>Orders</th><th>Subotal</th><th>VAT</th><th>Total</th><th></th></tr>	
                @foreach($reports as $report)
                
                <tr>
                    <td>{{ $report['date'] }}</td>
                    <td>{{ $report['count'] }}</td>
                    <td>{{ number_format($report['subtotal'],2) }}</td>
                    <td>{{ number_format($report['VAT'],2) }}</td>
                    <td>{{ number_format($report['total'],2) }}</td>
                    <td align="right"><button class="btn btn-info btn-sm">More &rarr;</button></td>
                </tr>
                
                @endforeach
		
        </table>      
		
         
		
      </div>
    </div>
    
@endsection



@section('js')
<script>
	$('.pickadate').pickadate({
		format: 'dd mmm yyyy'	
	});
</script>
@endsection
