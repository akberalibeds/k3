@extends('layouts.admin')

@section('title')
Admin Dashboard
@endsection

@section('content')



<div class="col-md-3">
    <div class="card card-info">
      <div class=" card-header blue">
        <div class="card-title">
          <div class="title">Search</div>
        </div>
      </div>
      <div class="card-body">
		<form action="/admin/reports/search">
             <div class="form-group">
                <select name="type" class="form-control">
                    <option>Monthly</option>
                    <option>Daily</option>
                    <option>Items</option>
                </select>
             </div>
             
                          
             <div class="form-group">
                <input class="form-control pickadate" placeholder="from" name="from" />
             </div>
             
               <div class="form-group">
                <input class="form-control pickadate" placeholder="to" name="to" />
             </div>
             
             
             <div class="form-group">
                <button class="btn btn-primary pull-right">Go</button>
             </div>
            <div class="clearfix"></div>
       </form> 
      </div>
    </div>
  </div>




  <div class="col-md-9">
  
  	 <div class="card card-info">
      <div class=" card-header blue">
        <div class="card-title">
          <div class="title">Today</div>
        </div>
      </div>
      <div class="card-body">

		<table class="table table-responsive table-bordered table-striped">        
		
          	<tr><th>Date</th><th>Orders</th><th>Subotal</th><th>VAT</th><th>Total</th></tr>	
                @foreach($reports['today'] as $report)
                
                <tr>
                    <td>{{ $report['date'] }}</td>
                    <td>{{ $report['count'] }}</td>
                    <td>{{ number_format($report['subtotal'],2) }}</td>
                    <td>{{ number_format($report['VAT'],2) }}</td>
                    <td>{{ number_format($report['total'],2) }}</td>
                </tr>
                
                @endforeach
		
        </table>      
		
        <hr />

		<table class="table table-responsive table-bordered table-striped">        
		
          	<tr><th>Company</th><th>Orders</th><th>Subotal</th><th>VAT</th><th>Total</th></tr>	
                @foreach($reports['retail'] as $report)
                
                    @if($report['total']>0)
                        <tr>
                            <td>{{ $report['company'] }} {!! ($report->manual_order) ? "(Phone)" : "" !!}</td>
                            <td>{{ $report['count'] }}</td>
                            <td>{{ number_format($report['subtotal'],2) }}</td>
                            <td>{{ number_format($report['VAT'],2) }}</td>
                            <td>{{ number_format($report['total'],2) }}</td>
                        </tr>
                    @endif
                    
                @endforeach
		
        </table>      
		
      </div>
    </div>
    
    
  
  
  
  <div class="clearfix"></div>
  <br>
  
    <div class="card card-info">
      <div class=" card-header blue">
        <div class="card-title">
          <div class="title">Monthly</div>
        </div>
      </div>
      <div class="card-body">

		<table class="table table-responsive table-bordered table-striped">        
		
          	<tr><th>Date</th><th>Orders</th><th>Subotal</th><th>VAT</th><th>Total</th></tr>	
                @foreach($reports['monthly'] as $report)
                
                <tr>
                    <td>{{ $report['date'] }}</td>
                    <td>{{ $report['count'] }}</td>
                    <td>{{ number_format($report['subtotal'],2) }}</td>
                    <td>{{ number_format($report['VAT'],2) }}</td>
                    <td>{{ number_format($report['total'],2) }}</td>
                </tr>
                
                @endforeach
		
        </table>      

      </div>
    </div>
  </div>




@endsection



@section('js')
<script>
	$('.pickadate').pickadate({
		format: 'dd mmm yyyy'	
	});
</script>
@endsection
