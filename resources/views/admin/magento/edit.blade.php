@extends('layouts.admin')

@section('title')
Admin Dashboard
@endsection

@section('content')


@if(isset($save))
	<div class="alert alert-success"> Settings Saved </div>
@endif

  <div class="col-md-12">
    <div class="card card-info">
      <div class=" card-header blue">
        <div class="card-title">
          <div class="title">Magento Settings</div>
        </div>
      </div>
      <div class="card-body">

		        
			
           {!! Form::open() !!} 
         
          <div class="form-group col-md-12">
            {!! Form::label('user', 'User') !!}
            {!! Form::text('user',$magento->user) !!}
            {!! Form::error('user') !!}
          </div>   
           
            
           <div class="form-group col-md-12">
            {!! Form::label('password', 'Password') !!}
            {!! Form::text('password',$magento->password) !!}
            {!! Form::error('password') !!}
           </div> 
          
           <div class="form-group col-md-12">
            {!! Form::label('url', 'API URL') !!}
            {!! Form::text('url',$magento->url) !!}
            {!! Form::error('url') !!}
          </div> 
          
          
          <div class="form-group col-md-12">
            {!! Form::label('api_version', 'API Version') !!}
            {!! Form::text('api_version',$magento->api_version) !!}
            {!! Form::error('api_version') !!}
          </div> 
          
          
          <div class="form-group col-md-12">
            {!! Form::label('active', 'active') !!}
            {!! Form::text('active',$magento->active) !!}
            {!! Form::error('active') !!}
          </div> 
          
          
           
		
        {!! Form::buttons() !!}
        
        {!! Form::close() !!} 
        

      </div>
    </div>
  </div>



<style>
	.th {
		background-color:lightgrey;
	}
</style>
@endsection
