@extends('layouts.admin')

@section('title')
Admin Dashboard
@endsection

@section('content')

  <div class="col-md-12">
    <div class="card card-info">
      <div class=" card-header blue">
        <div class="card-title">
          <div class="title">Magento Settings</div>
        </div>
      </div>
      <div class="card-body">

		<table class="table table-responsive table-bordered table-striped">        
		<thead>
        	<th></th><th>User</th><th>Password</th><th>API Url</th><th>API Version</th><th>Active</th><th></th>
        </thead>
          @foreach($magentos as $magento)  
      		
            <tr><td><div>{{ $magento->id }}</div></td><td><div>{{ $magento->user }}</div></td><td><div>{{ $magento->password }}</div></td><td><div>{{ $magento->url }}</div></td><td><div>{{ $magento->api_version }}</div></td><td><div>{{ $magento->active }}</div></td><td>  <span class="pull-right"><a href="/admin/magento/edit/{{ $magento->id }}" class="btn btn-primary">Edit</a></span></td></tr>
      		
          @endforeach  	 
		
        </table>      

      </div>
    </div>
  </div>




@endsection
