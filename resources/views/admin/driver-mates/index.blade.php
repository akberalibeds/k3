@extends('layouts.admin')
@section('title')
Admin Driver - Mates
@endsection
@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="card card-info">
      <div class=" card-header blue">
        <div class="card-title">
          <div class="title">Driver - Mates</div>
        </div>
      </div>
      <div class="card-body">
        <div class="row">
        
        
          <div class="col-md-6 col-sm-12 col-xs-12">
            @include('share.search-bar', [
              'options' => [
                ['value' => 'name', 'title' => 'Driver'],
                ['value' => 'num', 'title' => 'Telephone'],
                ['value' => 'uName', 'title' => 'Username'],
              ],
              'selectable' => false
            ])
          </div>
          <div class="col-md-6">
          	<a onClick="window.open('/admin/driver-mates/print')" class="btn btn-primary pull-right">Print List</a> <a onClick="window.open('/admin/driver-mates/create')" class="btn btn-primary pull-right">Add Driver-Mate</a>
          </div>
          
        </div>
        <div class="row">
          <div class="col-md-12">
            <div class="table-responsive">
              <table class="small table table-condensed table-striped table-no-wrap table-hover" id="table-results-container">
                <thead>
                  <tr>
                    <th data-sortable="name">Driver</th>
            
                    <th data-sortable="num">Telephone</th>
                    <th class="text-right" data-sortable="rate">Rate</th>
                   
                    <th class="text-center">Active</th>
                    <th class="text-right"><i class="fa fa-trash text-danger"></i></th>
                  </tr>
                </thead>
                <tbody id="result-container">
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
      <div class="card-footer">
        <div class="row">
          <div class="col-md-12 text-center" id="paging-controls-container">
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection

@section('js')
<script type="text/javascript" src="{{ URL::asset('js/library/typeahead.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/library/typeahead-kit.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/library/paging.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/library/table-results_0.2.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/admin/driver-mates/index.js') }}"></script>
@endsection
