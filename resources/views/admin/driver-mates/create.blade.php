@extends('layouts.admin')

@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="card card-info">
      <div class=" card-header blue">
        <div class="card-title">
          <div class="title">
            Driver Mate - Create
          </div>
        </div>
      </div>
      <div class="card-body">
        {!! Form::open(array('method' => 'post', 'route' => array('admin::driver-mates::store'))) !!}

          <div class="form-group">
            {!! Form::label('name', 'Name') !!}
            {!! Form::text('name') !!}
            {!! Form::error('name') !!}
          </div>

          <div class="form-group">
            {!! Form::label('num', 'Telephone') !!}
            {!! Form::tel('num') !!}
            {!! Form::error('num') !!}
          </div>

          <div class="form-group">
            {!! Form::label('rate', 'Rate') !!}
            {!! Form::money('rate') !!}
            {!! Form::error('rate') !!}
          </div>

          <div class="form-group">
            {!! Form::label('active', 'Active') !!}
            {!! Form::checkbox('active') !!}
            {!! Form::error('active') !!}
          </div>

          {!! Form::buttons() !!}

        {!! Form::close() !!}
      </div>
      <div class="card-footer">
      </div>
    </div>
  </div>
</div>
@endsection

@section('js')
<script>
$('input[type="checkbox"]').bootstrapSwitch()
</script>
@endsection
