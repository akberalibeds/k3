@extends('layouts.admin')

@section('title')
Admin Dashboard
@endsection

@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="card card-info">
      <div class=" card-header blue">
        <div class="card-title">
          <div class="title">Admin</div>
        </div>
      </div>
      <div class="card-body">
        <div class="row">
          <div class="col-md-12">
            <dl class="dl-horizontal">
              <dt>Your IP Address&nbsp;</dt><dd>{{ $currentIpa }}</dd>
              <dt>Iam number&nbsp;</dt><dd>{{ $iam }}</dd>
              <dt>Timezone&nbsp;</dt><dd><?php echo date_default_timezone_get(); ?>.</dd>
              <dt>Server Datetime&nbsp;</dt><dd><?php echo date('D, d M Y H:i:s', time()); ?> (page request time)</dd>
              <dt>Local Datetime&nbsp;</dt><dd id="time-now"></dd>
            <dl>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection

@push('js')
<script type="text/javascript" src="{{ URL::asset('/js/admin/index.js') }}"></script>
@endpush
