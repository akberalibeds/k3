@extends('layouts.admin')
@section('title')
Admin Delivery Route Edit - {{ $route->name }}
@endsection
@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="card card-info">
      <div class=" card-header blue">
        <div class="card-title">
          <div class="title">
            Admin Delivery Route Edit - {{ $route->name }}
          </div>
        </div>
      </div>
      <div class="card-body">
        {!! Form::model($route, array('method' => 'put', 'route' => array('admin::routes::update', $route->id))) !!}
        <div class="form-group">
          {!! Form::label('name', 'Name') !!}
          {!! Form::text('name') !!}
          {!! Form::error('name') !!}
        </div>
        <div class="form-group">
          {!! Form::label('max', 'Deliveries') !!}
          {!! Form::number('max') !!}
          {!! Form::error('max') !!}
        </div>
        <div class="form-group">
          {!! Form::label('van', 'Vans') !!}
          {!! Form::number('van') !!}
          {!! Form::error('van') !!}
        </div>
        <div class="form-group">
          {!! Form::label('msg', 'Can send message') !!}
          {!! Form::checkbox('msg') !!}
          {!! Form::error('msg') !!}
        </div>
        <div class="form-group">
          {!! Form::label('is_wholesale', 'Is Wholesale') !!}
          {!! Form::checkbox('is_wholesale') !!}
          {!! Form::error('is_wholesale') !!}
        </div>
        <div class="form-group">
          {!! Form::label('in_load_count', 'Is in Load Count') !!}
          {!! Form::checkbox('in_load_count') !!}
          {!! Form::error('in_load_count') !!}
        </div>
        <div class="form-group">
          {!! Form::label('is_route_route', 'Is in \'Route\' route') !!}
          {!! Form::checkbox('is_route_route') !!}
          {!! Form::error('is_route_route') !!}
        </div>
        <hr>
        <p><strong>Delivery Days</strong></p>
        <div class="form-group">
          {!! Form::label('monday', 'Monday') !!}
          {!! Form::checkbox('monday') !!}
          {!! Form::error('monday') !!}
        </div>
        <div class="form-group">
          {!! Form::label('tuesday', 'Tuesday') !!}
          {!! Form::checkbox('tuesday') !!}
          {!! Form::error('tuesday') !!}
        </div>
        <div class="form-group">
          {!! Form::label('wednesday', 'Wednesday') !!}
          {!! Form::checkbox('wednesday') !!}
          {!! Form::error('wednesday') !!}
        </div>
        <div class="form-group">
          {!! Form::label('thursday', 'Thursday') !!}
          {!! Form::checkbox('thursday') !!}
          {!! Form::error('thursday') !!}
        </div>
        <div class="form-group">
          {!! Form::label('friday', 'Friday') !!}
          {!! Form::checkbox('friday') !!}
          {!! Form::error('friday') !!}
        </div>
        <div class="form-group">
          {!! Form::label('saturday', 'Saturday') !!}
          {!! Form::checkbox('saturday') !!}
          {!! Form::error('saturday') !!}
        </div>
        <div class="form-group">
          {!! Form::label('sunday', 'Sunday') !!}
          {!! Form::checkbox('sunday') !!}
          {!! Form::error('sunday') !!}
        </div>
        {!! Form::buttons() !!}
        {!! Form::close() !!}
      </div>
      <div class="card-footer">
      </div>
    </div>
  </div>
</div>
@endsection
@push('js')
<script>
$('input[type="checkbox"]').bootstrapSwitch()
</script>
<script type="text/javascript" src="{{ URL::asset('js/admin/route/edit.js') }}"></script>
@endpush
