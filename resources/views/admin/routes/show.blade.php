@extends('layouts.admin')

@section('title')
Admin Delivery Route
@endsection

@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="card card-info">
      <div class=" card-header blue">
        <div class="card-title">
          <div class="title">
            Admin Delivery Route
          </div>
        </div>
      </div>
      <div class="card-body">
        <div class="row">
          <div class="col-md-5">
            <dl class="dl-horizontal">
              <dt>ID</dt>
              <dd>{{ $route->id }}</dd>
              <dt>Name</dt>
              <dd>{{ $route->name }}</dd>
              <dt>Deliveries</dt>
              <dd>{{ $route->max }}</dd>
              <dt>Vans</dt>
              <dd>{{ $route->van }}</dd>

              <dt>Can send messages</dt>
              <dd>@yesOrNo($route->msg)</dd>

              <dt>Is Wholesale</dt>
              <dd>@yesOrNo($route->is_wholesale)</dd>

              <dt>Is in Load Count</dt>
              <dd>@yesOrNo($route->in_load_count)</dd>

              <dt>is &apos;Route&apos; route</dt>
              <dd>@yesOrNo($route->is_route_route)</dd>
            </dl>
          </div>
          <div class="col-md-1">
            <ul class="list-unstyled">
              <li>
                <a class="btn btn-default" href="{{ route('admin::routes::edit', ['id' => $route->id]) }}" role="button">Edit</a>
              </li>
            </ul>
          </div>
        </div>
      </div>
      <div class="card-footer">
      </div>
    </div>
  </div>
</div>
@endsection

@push('headlinks')
<link href="{{ URL::asset('/css/jquery.bxslider.css') }}" rel="stylesheet" />
@endpush

@section('js')
<script type="text/javascript" src="{{ url ('/js/library/jquery.bxslider.min.js') }}"></script>
<script type="text/javascript" src="{{ url ('/js/library/upload.js') }}"></script>
<script type="text/javascript" src="{{ url ('/js/routes/show.js') }}"></script>
@endsection
