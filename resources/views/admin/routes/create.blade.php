@extends('layouts.admin')
@section('title')
Admin Route - Add
@endsection
@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="card card-info">
      <div class=" card-header blue">
        <div class="card-title">
          <div class="title">
            Admin Routes - Add
          </div>
        </div>
      </div>
      <div class="card-body">
        {!! Form::open(array('method' => 'post', 'route' => array('admin::routes::store'))) !!}
          <div class="form-group">
            {!! Form::label('name', 'Name') !!}
            {!! Form::text('name') !!}
            {!! Form::error('name') !!}
          </div>
          <div class="form-group">
            {!! Form::label('max', 'Deliveries') !!}
            {!! Form::number('max') !!}
            {!! Form::error('max') !!}
          </div>
          <div class="form-group">
            {!! Form::label('van', 'Vans') !!}
            {!! Form::number('van') !!}
            {!! Form::error('van') !!}
          </div>
          <div class="form-group">
            {!! Form::label('msg', 'Can send message') !!}
            {!! Form::checkbox('msg') !!}
            {!! Form::error('msg') !!}
          </div>
          <div class="form-group">
            {!! Form::label('is_wholesale', 'Is Wholesale') !!}
            {!! Form::checkbox('is_wholesale') !!}
            {!! Form::error('is_wholesale') !!}
          </div>
          <div class="form-group">
            {!! Form::label('in_load_count', 'Is in Load Count') !!}
            {!! Form::checkbox('in_load_count') !!}
            {!! Form::error('in_load_count') !!}
          </div>
          <div class="form-group">
            {!! Form::label('is_route_route', 'Is in \'Route\' route') !!}
            {!! Form::checkbox('is_route_route') !!}
            {!! Form::error('is_route_route') !!}
          </div>
          {!! Form::buttons() !!}
        {!! Form::close() !!}
      </div>
      <div class="card-footer">
      </div>
    </div>
  </div>
</div>
@endsection
@section('js')
<script>
$('input[type="checkbox"]').bootstrapSwitch()
</script>
@endsection
