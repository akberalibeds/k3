@extends('layouts.admin')

@section('title')
Admin Vehicle Cost Types
@endsection

@section('content')
<div class="row">
  <div class="card card-info">
    <div class="card-header blue">
      <div class="card-title">
        <div class="title">Admin Vehicle Cost Types</div>
      </div>
    </div>
    <div class="card-body">

      <div class="col-md-12 text-right">
        <button class="btn btn-default" onclick="addType()" role="button" type="button">Add Type</button>
      </div>

      <div class="row">
        <div class="col-md-12">
          <div class="table-responsive">
            <table class="small table table-striped table-no-wrap table-hover" id="table-results-container">
              <thead>
                <tr>
                  <th>Type</th>
                  <th>Description</th>
                </tr>
              </thead>
              <tbody id="vehicles-table">
                @foreach ($vehicleCostTypes as $vehicleCostType)
                <tr>
                  <td>{{ $vehicleCostType->type }}</td>
                  <td>{{ $vehicleCostType->description }}</td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection

@push('js')
<script type="text/javascript" src="{{ URL::asset('js/admin/vehicle-cost-types/index.js') }}"></script>
@endpush
