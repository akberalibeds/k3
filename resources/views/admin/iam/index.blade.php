@extends('layouts.admin')

@section('title')
Admin IP Addresses
@endsection

@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="card card-info">
      <div class=" card-header blue">
        <div class="row">
          <div class="card-title">
            <div class="title">Admin Iam</div>
          </div>
        </div>
      </div>
      <div class="card-body">

        {!! Form::open(array('method' => 'post', 'route' => array('admin::iam::store'))) !!}

          <div class="form-group">
            {!! Form::text('iam') !!}
          </div>

          <div class="form-group">
            {!! Form::submit('Make Me') !!}
          </div>

        {!! Form::close() !!}

      </div>
      <div class="card-footer">
      </div>
    </div>
  </div>
</div>
@endsection
