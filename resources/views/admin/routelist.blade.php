@extends('layouts.admin')

@section('title')
Route List
@endsection

@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="card card-info">
      <div class=" card-header blue">
        <div class="card-title">
          <div class="title">Route List</div>
        </div>
      </div>
      <div class="card-body">
        
      
        <table class="table table-bordered table-striped table-hover">
        <thead>
         <tr>
             <td><h4>HTTP Method</h4></td>
             <td><h4>Route</h4></td>
             <td><h4>Name</h4></td>
             <td><h4>Corresponding Action</h4></td>
         </tr>
        </thead>
        @foreach ($routes as $value) 
            <tr>
                 <td>{{ $value->getMethods()[0] }}</td>
                 <td>{{ $value->getPath() }}</td>
                 <td>{{ $value->getName() }}</td>
                 <td>{{ $value->getActionName() }}</td>
            </tr>
        @endforeach
    </table>
        
        
      </div>
    </div>
  </div>
</div>
@endsection

@push('js')
<script type="text/javascript" src="{{ URL::asset('/js/admin/index.js') }}"></script>
@endpush
