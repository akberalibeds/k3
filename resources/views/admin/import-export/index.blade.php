@extends('layouts.admin')
@section('title')
Admin Import / Export Profile
@endsection
@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="card card-info">
      <div class=" card-header blue">
        <div class="row">
          <div class="card-title">
            <div class="title">Admin Import / Export Profile</div>
          </div>
        </div>
      </div>

      <div class="card-body">
        <div class="row">
          <div class="col-md-2">
            <strong>Exports</strong>
            <ul class="list-unstyled">
            @foreach ($exports as $export => $obj)
              <li>{{ $obj['name'] }}</li>
            @endforeach
            </ul>
          </div>
          <div class="col-md-2">
            <strong>Imports</strong>
            <ul class="list-unstyled">
              @foreach ($imports as $import => $obj)
                <button class="btn btn-default" onclick="import('{{ $import }}')" role="button">{{ $obj['name'] }}</button><br>
              @endforeach
          </ul>
          </div>
        </div>
      </div>
      <div class="card-footer">
      </div>
    </div>
  </div>
</div>
@endsection
@push('js')
<script type="text/javascript" src="{{ URL::asset('js/admin/import-export/index.js') }}"></script>
@endpush
