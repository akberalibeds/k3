@extends('layouts.app')
@section('title')
Wowcher Order Import
@endsection
@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="card card-info">
      <div class=" card-header blue">
        <div class="card-title">
          <div class="title">Wowcher Order Import <sup><i class="info-tool-tip fa fa-info-circle" id="ttip-0" aria-hidden="true"></i></sup></div>
        </div>
      </div>
      <div class="card-body">
        <div class="row">
          <div class="col-md-3">
            <table class="table table-bordered small">
              <thead>
                <tr>
                  <td>Deal ID</td>
                  <td>SKU Code</td>
                </tr>
              </thead>
              <tbody class="table-typeahead" id="sku-codes-table">
              </tbody>
            </table>
          </div>
          <div class="col-md-9">
            <div id="import-container">
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
@section('js')
<script type="text/javascript" src="{{ URL::asset('js/library/typeahead.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/library/typeahead-kit.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/library/import-csv.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/order-imports/wowcher.js') }}"></script>
@endsection
