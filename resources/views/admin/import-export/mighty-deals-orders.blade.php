@extends('layouts.app')
@section('title')
MightyDeals Order Import
@endsection
@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="card card-info">
      <div class=" card-header blue">
        <div class="card-title">
          <div class="title">MightyDeals Order Import <sup><i class="info-tool-tip fa fa-info-circle" id="ttip-0" aria-hidden="true"></i></sup></div>
        </div>
      </div>
      <div class="card-body">
        <div class="row">
          <div class="col-md-12">
            <div id="import-container">
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
@section('js')
<script type="text/javascript" src="{{ URL::asset('js/library/import-csv.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/import-export/mighty-deals.js') }}"></script>
@endsection
