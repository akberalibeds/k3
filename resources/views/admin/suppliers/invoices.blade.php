@extends('layouts.admin')

@section('title')
Admin Invoices - {{ $supplier->supplier_name }}
@endsection

@section('content')
<div class="row">
  <div class="card card-info">
    <div class="card-header blue">
      <div class="card-title">
        <div class="title">Admin Invoices - {{ $supplier->supplier_name }}</div>
      </div>
    </div>
    <div class="card-body">
      <div class="thin-row">
        <div class="col-md-12">
          <small id="result-counts"></small>
        </div>
      </div>

      <div class="row">
        <div class="col-md-12">

          <div class="table-responsive">
            <table class="table table-condensed table-striped small">
              <tbody id="invoices-table">
              </tbody>
            </table>
          </div>

        </div>
      </div>
    </div>
    <div class="card-footer">
      <div class="row">
        <div class="col-md-12 text-center" id="paging-controls">
        </div>
      </div>
    </div>
  </div>
</div>
@endsection

@section('js')
<script>
var supplierId = {{ $supplier->id }}
</script>
<script type="text/javascript" src="{{ URL::asset('js/library/busy-kit.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/library/typeahead.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/library/typeahead-kit.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/library/paging.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/suppliers/invoices.js') }}"></script>
@endsection
