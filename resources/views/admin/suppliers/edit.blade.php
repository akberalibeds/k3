@extends('layouts.admin')

@section('title')
Admin Suppliers - Edit
@endsection

@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="card card-info">
      <div class=" card-header blue">
        <div class="card-title">
          <div class="title">
            Admin Suppliers - Edit
          </div>
        </div>
      </div>
      <div class="card-body">

        {!! Form::model($supplier, array('method' => 'put', 'route' => array('admin::suppliers::update', $supplier->id))) !!}

          <div class="form-group">
            {!! Form::label('supplier_name', 'Supplier Name') !!}
            {!! Form::text('supplier_name') !!}
            {!! Form::error('supplier_name') !!}
          </div>

          <div class="form-group">
            {!! Form::label('days', 'Days') !!}
            {!! Form::number('days') !!}
            {!! Form::error('days') !!}
          </div>

          <div class="form-group">
            {!! Form::label('print_orders_on_dispatch', 'Print Orders On Dispatch') !!}
            {!! Form::checkbox('print_orders_on_dispatch') !!}
            {!! Form::error('print_orders_on_dispatch') !!}
          </div>

          {!! Form::buttons() !!}

        {!! Form::close() !!}

      </div>
      <div class="card-footer">
      </div>
    </div>
  </div>
</div>
@endsection

@section('js')
<script>
$('input[type="checkbox"]').bootstrapSwitch()
</script>
@endsection
