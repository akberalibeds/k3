@extends('layouts.admin')

@section('title')
Admin Supplier - {{ $supplier->supplier_name }}
@endsection

@section('content')
<div class="row">
  <div class="card card-info">
    <div class="card-header blue">
      <div class="card-title">
        <div class="title">Admin Supplier - {{ $supplier->supplier_name }}</div>
      </div>
    </div>
    <div class="card-body">
      <div class="row">
        <div class="col-md-4">
          <dl class="dl-horizontal">
            <dt>Id</dt>
            <dd>{{ $supplier->id }}</dd>
            <dt>Name</dt>
            <dd>{{ $supplier->supplier_name }}</dd>
            <dt>Days</dt>
            <dd>
              {{ $supplier->days }}</a>
            </dd>
            <dt>Print Orders On Dispatch</dt>
            <dd>
              @yesOrNo($supplier->print_orders_on_dispatch)</a>
            </dd>
          </dl>
        </div>
        <div class="col-md-1">
          <ul class="list-unstyled">
            <li>
              <a class="btn btn-default" href="{{ route('admin::suppliers::edit', ['id' => $supplier->id]) }}" role="button">Edit</a>
            </li>
          </ul>
        </div>

      </div>
    </div>
    <div class="card-footer">
    </div>
  </div>
</div>
@endsection
