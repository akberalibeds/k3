@extends('layouts.admin')

@section('title')
Admin Vehicles Mileages
@endsection

@section('content')
<div class="row">
  <div class="card card-info">
    <div class="card-header blue">
      <div class="card-title">
        <div class="title">Admin Vehicle Mileages</div>
      </div>
    </div>
    <div class="card-body">
      <div class="table-responsive">
        <table class="table table-condensed table-striped">
          <thead>
            <tr>
              <th>Registration</th>
              <th class="text-right">Last Recorded Mileage</th>
              <th class="text-right">Mileage</th>
            </tr>
          </thead>
          <tbody id="vehicles-table">
            @foreach ($vehicles as $vehicle)
            <tr>
              <td>{{ $vehicle->reg }}</td>
              <td class="text-right">
                {{ $vehicle->mileage or '-' }}
              </td>
              <td class="text-right">
                <input class="text-right" data-vehicle-id="{{ $vehicle->id }}" name="mileage-{{ $vehicle->id }}" type="number">
              </td>
            </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
    <div class="card-footer">
      <div class="role">
        <div class="col-md-12 text-right">
          <button class="btn btn-primary" onclick="saveMileages()" role="button" type="button">Save Mielages</button>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection

@push('js')
<script type="text/javascript" src="{{ URL::asset('js/admin/vehicle-mileages/index.js') }}"></script>
@endpush
