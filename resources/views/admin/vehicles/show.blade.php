@extends('layouts.admin')
@section('title')
Admin Vehicle - {{ $vehicle->reg }}
@endsection
@section('content')
<div class="row">
  <div class="card card-info">
    <div class="card-header blue">
      <div class="card-title">
        <div class="title">Admin Vehicle - {{ $vehicle->reg }}</div>
      </div>
    </div>
    <div class="card-body">
      <div class="row">
        <div class="col-md-6">
          <dl class="dl-horizontal">
            <dt>Registration</dt>
            <dd>{{ $vehicle->reg }}</dd>
            <dt>Height</dt>
            <dd>{{ $vehicle->height }}</dd>
            <dt>Width</dt>
            <dd>{{ $vehicle->width }}</dd>
            <dt>Depth</dt>
            <dd>{{ $vehicle->depth }}</dd>
            <dt>Last known mileage</dt>
            <dd>{{ $vehicle->mileage }}</dd>
            <dt>Description</dt>
            <dd>{{ $vehicle->description }}</dd>
          </dl>
        </div>
        <div class="col-md-6">
          <ul class="list-unstyled">
            <li>
              <a class="btn btn-default" href="{{ route('admin::vehicles::edit', ['id' => $vehicle->id]) }}" role="button">Edit</a>
            </li>
          </ul>
        </div>
      </div>
      <div class="row">
        <div class="tab-content">
          <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="#costs" aria-controls="costs" role="tab" data-toggle="tab">Costs and Mileage</a></li>
            <li role="presentation"><a href="#chart" aria-controls="chart" role="tab" data-toggle="tab">Chart</a></li>
          </ul>
          <div role="tabpanel" class="tab-pane fade in active" id="costs">
            <div class="table-responsive">
              <table class="small table table-striped table-no-wrap table-hover">
                <thead>
                  <tr>
                    <th>Type</th>
                    <th>Note</th>
                    <th class="text-right">Mileage</th>
                    <th class="text-right">Cost</th>
                    <th class="text-right">Date</th>
                  <tr>
                </thead>
                <tbody>
                  @foreach ($history as $cost)
                  <tr>
                    <td>{{ $cost->type }}</td>
                    <td>{{ $cost->notes }}</td>
                    <td class="text-right">{{ $cost->mileage or '-'}}</td>
                    <td class="text-right">{{ $cost->cost }}</td>
                    <td class="text-right">{{ $cost->date_incurred }}</td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
          <div role="tabpanel" class="tab-pane fade" id="chart">
            <canvas id="canvas-chart" width="400" height="200"></canvas>
          </div>
        </div>
      </div>
    </div>
    <div class="card-footer">
    </div>
  </div>
</div>
@endsection
@section('js')
<script>
var chartData = {!! json_encode($history) !!}
</script>
<script src="/js/library/Chart.min.js"></script>
<script src="/js/admin/vehicles/show.js"></script>
@endsection
