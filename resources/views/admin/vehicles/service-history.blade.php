@extends('layouts.admin')

@section('title')
Admin Vehicle Service History
@endsection

@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="card card-info">
      <div class=" card-header blue">
        <div class="card-title">
          <div class="title">
            Admin Vehicle Service History
          </div>
        </div>
      </div>
      <div class="card-body">
      </div>
      <div class="card-footer">
      </div>
  </div>
</div>
@endsection
