@extends('layouts.admin')

@section('title')
Admin Vehicle Costs History
@endsection

@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="card card-info">
      <div class=" card-header blue">
        <div class="card-title">
          <div class="title">
            Admin Vehicle Costs History
          </div>
        </div>
      </div>
      <div class="card-body">
        <div class="table-responsive">
          <table class="">
          <thead>
          </thead>
          <tbody id="result-container">
          </tbody>
          </tabe>
        </div>
      </div>
      <div class="card-footer">
        <div class="row">
          <div class="col-md-12 text-center" id="paging-controls-container">
          </div>
        </div>
      </div>
  </div>
</div>
@endsection
