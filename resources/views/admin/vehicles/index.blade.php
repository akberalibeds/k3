@extends('layouts.admin')

@section('title')
Admin Vehicles
@endsection

@section('content')
<div class="row">
  <div class="card card-info">
    <div class="card-header blue">
      <div class="card-title">
        <div class="title">Admin Vehicles</div>
      </div>
    </div>
    <div class="card-body">
      <div class="row">
        <div class="col-md-6 col-sm-12 col-xs-12">
          @include('share.search-bar', [
            'options' => [
              ['value' => 'reg', 'title' => 'Registration'],
            ],
            'selectable' => false
          ])
        </div>
        @permission('vehicle_view')
        <div class="col-md-6 text-right">
          <a class="btn btn-default" href="{{ route('admin::vehicles::create') }}" role="button" type="button">Add Vehicle</a>
        </div>
        @endpermission
      </div>
      <div class="table-responsive">
        <table class="small table table-striped table-no-wrap table-hover" id="table-results-container">
          <thead>
            <tr>
              <th>Registration</th>
              <th class="text-right">Height</th>
              <th class="text-right">Width</th>
              <th class="text-right">Depth</th>
              <th class="text-right">Last known mileage</th>
              <th class="text-right">Costs to date</th>
              <th class="text-right">Description</th>
            </tr>
          </thead>
          <tbody class="table-clickable" id="result-container">
          </tbody>
        </table>
      </div>
    </div>
    <div class="card-footer">
      <div class="row">
        <div class="col-md-12 text-center" id="paging-controls-container">
        </div>
      </div>
    </div>
  </div>
</div>
@endsection

@section('js')
<script type="text/javascript" src="{{ URL::asset('js/library/table-results_0.2.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/admin/vehicles/index.js') }}"></script>
@endsection
