@extends('layouts.admin')

@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="card card-info">
      <div class=" card-header blue">
        <div class="card-title">
          <div class="title">
            New Vehicle
          </div>
        </div>
      </div>
      <div class="card-body">

        {!! Form::open(array('method' => 'post', 'route' => array('admin::vehicles::store'))) !!}

          <div class="form-group">
            {!! Form::label('reg', 'Registration') !!}
            {!! Form::text('reg') !!}
            {!! Form::error('reg') !!}
          </div>

          <div class="form-group">
            {!! Form::label('height', 'Height') !!}
            {!! Form::metres('height') !!}
            {!! Form::error('height') !!}
          </div>

          <div class="form-group">
            {!! Form::label('width', 'Width') !!}
            {!! Form::metres('width') !!}
            {!! Form::error('width') !!}
          </div>

          <div class="form-group">
            {!! Form::label('depth', 'Depth') !!}
            {!! Form::metres('depth') !!}
            {!! Form::error('depth') !!}
          </div>

          <div class="form-group">
            {!! Form::label('weight', 'Weight') !!}
            {!! Form::weight('weight') !!}
            {!! Form::error('weight') !!}
          </div>

          <div class="form-group">
            {!! Form::label('description', 'Description') !!}
            {!! Form::textarea('description') !!}
            {!! Form::error('description') !!}
          </div>

          {!! Form::buttons() !!}

        {!! Form::close() !!}
      </div>
      <div class="card-footer">
      </div>
  </div>
</div>
@endsection

@section('js')
<script>
$('input[type="checkbox"]').bootstrapSwitch()
</script>
@endsection
