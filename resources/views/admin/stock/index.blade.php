@extends('layouts.admin')
@section('title')
Admin Stock
@endsection
@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="card card-info">
      <div class=" card-header blue">
        <div class="row">
          <div class="card-title">
            <div class="title">Admin Stock</div>
          </div>
        </div>
      </div>
      <div class="card-body">
        <div class="row">
          <div class="col-md-12 text-right">
            <button class="btn btn-default" onclick="exportCsvSelected(event)" role="button">Export as CSV</button>
            <button class="btn btn-default" onclick="return exportExcelSelected(event)" role="submit">Export as Excel</button>
            <a class="btn btn-default" href="{{ route('admin::stock::create') }}" role="button">Add New Stock Item</a>
          </div>
        </div>
        <div class="row">
          <div class="col-md-6">
            @include('share.search-bar', [
              'options' => [
                ['value' => 'itemCode', 'title' => 'Item Code'],
                ['value' => 'itemDescription', 'title' => 'Description'],
                ['value' => 'model_no', 'title' => 'Model Number'],
              ],
              'selectable' => true,
            ])
          </div>
          <div class="col-md-2">
            <div class="form-group">
              <input autocomplete="off" class="selectpicker" value="0" id="checkbox-hide-withdrawn" name="withdrawn" onchange="tableResults.filter(event)" type="checkbox">
              <label for="select-supplier">Hide Withdrawn</label>
              <br>
              <input autocomplete="off" class="selectpicker" value="1" id="checkbox-show-needs-attention" name="needs_attention" onchange="tableResults.filter(event)" type="checkbox">
              <label for="select-supplier">Show &apos;Needs Attention&apos;</label>
              <br>
              <input autocomplete="off" class="selectpicker" value="1" id="checkbox-show-always-in-stock" name="stock_items.always_in_stock" onchange="tableResults.filter(event)" type="checkbox">
              <label for="select-supplier">Show &apos;Always in stock&apos;</label>
              <br>
              <input autocomplete="off" class="selectpicker" value="1" checked id="checkbox-hidden-sku-codes"  name="stock_items.isActive" onchange="tableResults.filter(event)" type="checkbox">
              <label for="select-supplier">Show Inactive SKUs</label>
            </div>
          </div>
          <div class="col-md-2">
            <div class="form-group">
              <label for="select-supplier">Category</label>
              <select autocomplete="off" class="selectpicker" data-container="body" data-dropdown-align-right="auto" id="select-supplier" name="category_id" onchange="tableResults.filter(event, false)">
                <option selected="selected" value="">All</option>
                @foreach ($categories as $key => $name)
                <option value="{{ $key }}">{{ $name }}</option>
                @endforeach
              </select>
            </div>
          </div>
          <div class="col-md-2">
            <div class="form-group">
              <label for="select-supplier">Stock Category</label>
              <select autocomplete="off" class="selectpicker" data-container="body" id="select-stock-category" name="stock-category_id" onchange="tableResults.filter(event, false)">
                <option selected="selected" value="">All</option>
                @foreach ($stockCategories as $stockCategory)
                <option value="{{ $stockCategory->id }}">{{ $stockCategory->name }}</option>
                @endforeach
              </select>
            </div>
          </div>
        </div>
        <div class="table-responsive">
          <table class="small table table-striped table-no-wrap table-hover" id="table-results-container">
            <thead>
              <tr>
                <th id="select-all-container"></th>
                <th data-sortable="itemCode">SKU</th>
                <th>Sales-Category</th>
                <th class="text-right">In Stock</th>
                <th class="text-right">On SO</th>
                <th class="text-right">Available</th>
                <th class="text-center">Label</th>
                <th class="text-center">Warehouse</th>
                <th class="text-right">Boxes</th>
                <th class="text-right">Parts</th>
                <th class="text-right font-weight-bold">Active SKU</th>
                <th class="text-center">Sends Text</th>
                <th class="text-center">Withdrawn</th>
                <th class="text-center">Needs Attention</th>
                <th class="text-center">Always in Stock</th>
              </tr>
            </thead>
            <tbody class="table-clickable" id="result-container">
            </tbody>
          </table>
        </div>
      </div>
      <div class="card-footer">
        <div class="row">
          <div class="col-md-12 text-center" id="paging-controls-container">
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
@push('js')
<script type="text/javascript" src="{{ URL::asset('js/library/table-results_0.2.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/admin/stock/index.js') }}"></script>
@endpush
