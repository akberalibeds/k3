@extends('layouts.admin')

@section('title')
Edit In Stock Count - {{ $stockItem->itemCode }}
@endsection

@section('content')
<div class="row">
  <div class="col-md-12">
    <form action="{{ url('/stock/' . $stockItem->id . '/update') }}" class="form-horizontal" method="post">
      <input type="hidden" name="id" value="{{ $stockItem->id }}">
      {{ method_field('PUT') }}
      {!! csrf_field() !!}
      <div class="card card-info">
        <div class=" card-header blue">
          <div class="card-title">
            <div class="title">
              Edit In Stock Count - {{ $stockItem->itemCode }}
            </div>
          </div>
        </div>
        <div class="card-body">
          <dl class="dl-horizontal">
            <dt>SKU / Code</dt>
            <dd>{{ $stockItem->itemCode }}</dd>
            <dt>Item Description</dt>
            <dd>{{ $stockItem->itemDescription }}</dd>
            <dt>Quatity in stock</dt>
            <dd>{{ $stockItem->actual }}</dd>
            <dt>Quatity on Sale Orders</dt>
            <dd>{{ $stockItem->qty_on_so }}</dd>
          </dl>
          <div class="row">
            <div class="col-md-3">
              <div class="input-group">
                <div class="input-group-addon">Add</div>
                <input class="form-control" name="actual" type="number" value="0">
                <div class="input-group-addon">to stock</div>
              </div>
            </div>
          </div>
        </div>
        <div class="card-footer">
          <div class="text-right">
            <button class="btn btn-default" onclick="javascript:window.history.back()" type="button">Cancel</button>&nbsp;<button class="btn btn-primary" type="submit">Update Stock</button>
          </div>
        </div>
      </div>
    </form>
  </div>
</div>
@endsection
