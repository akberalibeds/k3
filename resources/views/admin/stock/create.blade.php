@extends('layouts.admin')
@section('title')
Admin Add Stock Item
@endsection
@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="card card-info">
      <div class=" card-header blue">
        <div class="card-title">
          <div class="title">
            Admin Add Stock Item
          </div>
        </div>
      </div>
      <div class="card-body">
        {!! Form::open(array('method' => 'post', 'route' => array('admin::stock::store'))) !!}
          <div class="form-group">
            {!! Form::label('model_no', 'Model Number') !!}
            {!! Form::text('model_no') !!}
            {!! Form::error('model_no') !!}
          </div>
          <p>The model number is not a required field.</p>
          <div class="form-group">
            {!! Form::label('itemCode', 'SKU / Item Code', [], 'Required') !!}
            {!! Form::text('itemCode') !!}
            {!! Form::error('itemCode') !!}
          </div>
          <button class="btn btn-default" onclick="skuGenerator(); return false">Generate SKU</button>
          <p><em>Note:</em> A SKU code with the word 'collect' in it will be flagged on Dispatch / Delivery notes as a COLLECT item.</p>
          <div class="form-group">
            {!! Form::label('itemDescription', 'Item Description') !!}
            {!! Form::textarea('itemDescription') !!}
            {!! Form::error('itemDescription') !!}
          </div>
          <div class="form-group">
            {!! Form::label('category_id', 'Category', [], 'Required') !!}
            {!! Form::select('category_id', $categoryOptions) !!}
            {!! Form::error('category_id') !!}
          </div>
          <div class="form-group">
            {!! Form::label('stock_category_id', 'Stock Category') !!}
            {!! Form::select('stock_category_id', $stockCategoryOptions) !!}
            {!! Form::error('stock_category_id') !!}
          </div>
          <div class="form-group">
            {!! Form::label('actual', 'Stock count') !!}
            {!! Form::number('actual') !!}
            {!! Form::error('actual') !!}
          </div>
          <div class="form-group">
            {!! Form::label('pieces', 'Boxes', [], 'Required') !!}
            {!! Form::number('pieces') !!}
            {!! Form::error('pieces') !!}
          </div>
          <div class="form-group">
            {!! Form::label('isMulti', 'Items per box') !!}
            {!! Form::number('isMulti') !!}
            {!! Form::error('isMulti') !!}
          </div>
          <div class="form-group">
            {!! Form::label('weight', 'Weight') !!}
            {!! Form::weight('weight') !!}
            {!! Form::error('weight') !!}
          </div>
          <div class="form-group">
            {!! Form::label('label', 'Label') !!}
            {!! Form::checkbox('label') !!}
            {!! Form::error('label') !!}
          </div>
          <div class="form-group">
            {!! Form::label('warehouse', 'Warehouse') !!}
            {!! Form::number('warehouse') !!}
            {!! Form::error('warehouse') !!}
          </div>
          <div class="form-group">
            {!! Form::label('seller', 'Available to seller') !!}
            {!! Form::checkbox('seller') !!}
            {!! Form::error('seller') !!}
          </div>
          <div class="form-group">
            {!! Form::label('cost', 'Cost price') !!}
            {!! Form::money('cost') !!}
            {!! Form::error('cost') !!}
          </div>
          <div class="form-group">
            {!! Form::label('ebay_price', 'Ebay price') !!}
            {!! Form::money('ebay_price') !!}
            {!! Form::error('ebay_price') !!}
          </div>
          <div class="form-group">
            {!! Form::label('wholesale', 'Wholesale price') !!}
            {!! Form::money('wholesale') !!}
            {!! Form::error('wholesale') !!}
          </div>
          <div class="form-group">
            {!! Form::label('retail', 'Retail price') !!}
            {!! Form::money('retail') !!}
            {!! Form::error('retail') !!}
          </div>
          <div class="form-group">
            {!! Form::label('blocked', 'Blocked') !!}
            {!! Form::checkbox('blocked') !!}
            {!! Form::error('blocked') !!}
          </div>
          <div class="form-group">
            {!! Form::label('withdrawn', 'Withdrawn') !!}
            {!! Form::checkbox('withdrawn') !!}
            {!! Form::error('withdrawn') !!}
          </div>
          <div class="form-group">
            {!! Form::label('always_in_stock', 'Always in stock') !!}
            {!! Form::checkbox('always_in_stock') !!}
            {!! Form::error('always_in_stock') !!}
          </div>
          {!! Form::buttons() !!}
        {!! Form::close() !!}
      </div>
      <div class="card-footer">
      </div>
    </div>
  </div>
</div>
@endsection
@push('js')
<script>
$('[type="checkbox"]').bootstrapSwitch()
$('[data-toggle="tooltip"]').tooltip();
</script>
<script type="text/javascript" src="{{ URL::asset('js/library/upload.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/admin/stock/create.js') }}"></script>
@endpush
