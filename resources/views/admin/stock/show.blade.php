@extends('layouts.admin')
@section('title')
Admin Stock Item - {{ $stockItem->itemCode }}
@endsection
@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="card card-info">
      <div class=" card-header blue">
        <div class="card-title">
          <div class="title">
            Admin Stock Item - {{ $stockItem->itemCode }}
          </div>
        </div>
      </div>
      <div class="card-body">
        <div class="row">
          <div class="col-md-4">
            <table class="table">
              <tr><td>Item code / SKU</td><td>{{ $stockItem->itemCode }}</td></tr>
              <tr><td>Model Number</td><td>{{ $stockItem->model_no or '[tbd]' }}</td></tr>
              <tr><td>Description</td><td>{{ $stockItem->itemDescription }}</td></tr>
              <tr><td>Category</td><td>{{ $stockItem->cat }}</td></tr>
              <tr><td>Category (by id)</td><td>{{ $stockItem->category->name or '[tbd]' }}</td></tr>
              <tr><td>Stock Category</td><td>{{ $stockItem->stock_category_name }}</td></tr>
              <tr><td>Actual in stock</td><td id="actual">{{ $stockItem->actual }}</td></tr>
              <tr><td>Quantity on Sales Order</td><td id="qty-on-so">{{ $stockItem->qty_on_so }}</td></tr>
              <tr><td>Quantity Available</td><td id="qty-available">{{ $stockItem->actual - $stockItem->qty_on_so }}</td></tr>
              <tr><td>Boxes</td><td>{{ $stockItem->pieces }}</td></tr>
              <tr><td>Parts</td><td>{{ $stockItem->isMulti }}</td></tr>
              <tr><td>Weight</td><td>{{ $stockItem->weight }} Kg</td></tr>
              <tr><td>Label</td><td>@if ($stockItem->label) Yes @else No @endif</td></tr>
              <tr><td>Warehouse</td><td>{{ $stockItem->warehouse }}</td></tr>
              <tr><td>Available to seller</td><td>@if ($stockItem->seller) Yes @else No @endif</td></tr>
              <tr><td>Cost</td><td>&pound;{{ number_format($stockItem->cost, 2) }}</td></tr>
              <tr><td>Beds</td><td>&pound;{{ number_format($stockItem->beds_price, 2) }}</td></tr>
              <tr><td>Ebay</td><td>&pound;{{ number_format($stockItem->ebay_price, 2) }}</td></tr>
              <tr><td>Retail</td><td>&pound;{{ number_format($stockItem->retail, 2) }}</td></tr>
              <tr><td>Wholesale</td><td>&pound;{{ number_format($stockItem->wholesale, 2) }}</td></tr>
              <tr><td>Blocked to texts</td><td>@if ($stockItem->blocked) Yes @else No @endif</td></tr>
              <tr><td>Withdrawn from sale</td><td>@if ($stockItem->withdrawn) Yes @else No @endif</td></tr>
              <tr><td>Needs attention</td><td>@if ($stockItem->needs_attention) Yes @else No @endif</td></tr>
              <tr><td>Always in stock</td><td>@if ($stockItem->always_in_stock) Yes @else No @endif</td></tr>
            </table>
          </div>
          <div class="col-md-6">
            <ul class="list-unstyled">
              <li>
                <button class="btn btn-default" onclick="addToStockCount({{ $stockItem->id }}, '{{ $stockItem->itemCode }}')" role="button">Add To Stock Count</button>
              </li>
              <li>
                <button class="btn btn-default" onclick="adjustStockCount({{ $stockItem->id }}, '{{ $stockItem->itemCode }}')" role="button">Adjust Stock Count</button>
              </li>
              <li>
                <a class="btn btn-default" href="{{ route('admin::stock::edit', ['id' => $stockItem->id]) }}" role="button">Edit</a>
              </li>
              <li>
                <a class="btn btn-default" href="@if ($stockItem->blocked == 1) {{ route('admin::stock::unblock', ['id' => $stockItem->id]) }} @else {{ route('admin::stock::block', ['id' => $stockItem->id]) }} @endif" role="button">@if ($stockItem->blocked == 1) Unblock @else Block @endif</a>
              </li>
              <li>
                <a class="btn btn-default" href="@if ($stockItem->withdrawn == 1) {{ route('admin::stock::unwithdraw', ['id' => $stockItem->id]) }} @else {{ route('admin::stock::withdraw', ['id' => $stockItem->id]) }} @endif" role="button">@if ($stockItem->withdrawn == 1) Un-withdraw @else Withdraw @endif from Sale</a>
              </li>
              <li>
                <a class="btn btn-default" href="@if ($stockItem->needs_attention == 1) {{ route('admin::stock::un-needs-attention', ['id' => $stockItem->id]) }} @else {{ route('admin::stock::needs-attention', ['id' => $stockItem->id]) }} @endif" role="button">@if ($stockItem->needs_attention == 1) Un-needs  @else Needs @endif Attention</a>
              </li>
            </ul>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12">
          </div>
        </div>
      </div>
      <div class="card-footer">
      </div>
    </div>
  </div>
</div>
@endsection
@section('js')
<script type="text/javascript" src="{{ URL::asset('js/admin/stock/show.js') }}"></script>
@endsection
