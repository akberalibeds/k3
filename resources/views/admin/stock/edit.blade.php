@extends('layouts.admin')
@section('title')
Admin Edit Stock Item
@endsection
@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="card card-info">
      <div class=" card-header blue">
        <div class="card-title">
          <div class="title">
            Admin Edit Stock
          </div>
        </div>
      </div>
      <div class="card-body">
        <div class="row">
          <div class="col-md-2 col-sm-2">
            <strong>SKU / Code</strong>
          </div>
          <div class="col-md-10">
            <strong>{{ $stockItem->itemCode or '(no sku)' }}</strong> <strong>({{ $stockItem->model_no or 'no model number' }})</strong>
            <br class="clearfix">
            <br class="clearfix">
          </div>
        </div>
        {!! Form::model($stockItem, array('method' => 'put', 'route' => array('admin::stock::update', $stockItem->id))) !!}
          @if (!isset($stockItem->itemCode) || $stockItem->itemCode == '')
          <div class="form-group">
            {!! Form::label('itemCode', 'SKU') !!}
            {!! Form::text('itemCode') !!}
            {!! Form::error('itemCode') !!}
          </div>
          @endif
          @if (!isset($stockItem->model_no) || $stockItem->model_no == '')
          <div class="form-group">
            {!! Form::label('model_no', 'Model Number') !!}
            {!! Form::text('model_no') !!}
            {!! Form::error('model_no') !!}
          </div>
          @endif
          <div class="form-group">
            {!! Form::label('itemDescription', 'Item Description') !!}
            {!! Form::textarea('itemDescription') !!}
            {!! Form::error('itemDescription') !!}
          </div>
          <div class="form-group">
            {!! Form::label('category_id', 'Category') !!}
            {!! Form::select('category_id', $categoryOptions) !!}
            {!! Form::error('category_id') !!}
          </div>
          <div class="form-group">
            {!! Form::label('stock_category_id', 'Stock Category') !!}
            {!! Form::select('stock_category_id', $stockCategoryOptions) !!}
            {!! Form::error('stock_category_id') !!}
          </div>
          <div class="form-group">
            {!! Form::label('actual', 'Stock count') !!}
            {!! Form::number('actual') !!}
            {!! Form::error('actual') !!}
          </div>
          <div class="form-group">
            {!! Form::label('pieces', 'Boxes') !!}
            {!! Form::number('pieces') !!}
            {!! Form::error('pieces') !!}
          </div>
          <div class="form-group">
            {!! Form::label('isMulti', 'Items per box') !!}
            {!! Form::number('isMulti') !!}
            {!! Form::error('isMulti') !!}
          </div>
          <div class="form-group">
            {!! Form::label('weight', 'Weight') !!}
            {!! Form::weight('weight') !!}
            {!! Form::error('weight') !!}
          </div>
          <div class="form-group">
            {!! Form::label('label', 'Label') !!}
            {!! Form::checkbox('label') !!}
            {!! Form::error('label') !!}
          </div>
          <div class="form-group">
            {!! Form::label('warehouse', 'Warehouse') !!}
            {!! Form::number('warehouse') !!}
            {!! Form::error('warehouse') !!}
          </div>
          <div class="form-group">
            {!! Form::label('seller', 'Available to seller') !!}
            {!! Form::checkbox('seller') !!}
            {!! Form::error('seller') !!}
          </div>
          <div class="form-group">
            {!! Form::label('cost', 'Cost') !!}
            {!! Form::money('cost') !!}
            {!! Form::error('cost') !!}
          </div>
          <div class="form-group">
            {!! Form::label('ebay_price', 'Ebay price') !!}
            {!! Form::money('ebay_price') !!}
            {!! Form::error('ebay_price') !!}
          </div>
          <div class="form-group">
            {!! Form::label('wholesale', 'Wholesale price') !!}
            {!! Form::money('wholesale') !!}
            {!! Form::error('wholesale') !!}
          </div>
          <div class="form-group">
            {!! Form::label('retail', 'Retail price') !!}
            {!! Form::money('retail') !!}
            {!! Form::error('retail') !!}
          </div>
          <div class="form-group">
            {!! Form::label('blocked', 'Blocked') !!}
            {!! Form::checkbox('blocked') !!}
            {!! Form::error('blocked') !!}
          </div>
          <div class="form-group">
            {!! Form::label('withdrawn', 'Withdrawn') !!}
            {!! Form::checkbox('withdrawn') !!}
            {!! Form::error('withdrawn') !!}
          </div>
          <div class="form-group">
            {!! Form::label('needs_attention', 'Needs Attention') !!}
            {!! Form::checkbox('needs_attention') !!}
            {!! Form::error('needs_attention') !!}
          </div>
          <div class="form-group">
            {!! Form::label('always_in_stock', 'Always in stock') !!}
            {!! Form::checkbox('always_in_stock') !!}
            {!! Form::error('always_in_stock') !!}
          </div>
          {!! Form::buttons() !!}
        {!! Form::close() !!}
      </div>
      <div class="card-footer">
      </div>
  </div>
</div>
@endsection
@section('js')
<script>
$('input[type="checkbox"]').bootstrapSwitch()
</script>
@endsection
