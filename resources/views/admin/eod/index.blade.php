@extends('layouts.admin')
@section('title')
Admin - End of Day Summary
@endsection
@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="card card-info">
      <div class=" card-header blue">
        <div class="card-title">
          <div class="title">Admin - End of Day Summary</div>
        </div>
      </div>
      <div class="card-body">
        <div class="row">
          <div class="col-md-6 col-sm-12 col-xs-12">
            @include('share.search-bar', [
              'options' => [
                ['value' => 'name', 'title' => 'Name'],
              ],
              'selectable' => false,
              'selectControls' => false,
            ])
          </div>
          <div class="col-md-6 col-sm-12 col-xs-12">
            <select autocomplete="off" class="selectpicker" data-container="body" data-filter-default="{{ $dates[0] }}" data-size="5" data-width="fit" id="filter-startDate" name="startDate" onchange="tableResults.filter(event, false)" >
              @foreach ($dates as $date)
              <option value="{{ $date }}">{{ $date }}</option>
              @endforeach
            </select>
          </div>
        </div>
        <div class="table-responsive">
          <table class="small table table-condensed table-striped" id="table-results-container">
            <thead>
              <tr>
                <th>Name</th>
                <th>Date</th>
                <th class="text-right">Count</th>
              </tr>
            </thead>
            <tbody id="result-container">
            </tbody>
          </table>
        </div>
      </div>
      <div class="card-footer">
        <div class="card-footer">
          <div class="row">
            <div class="col-md-12 text-center" id="paging-controls-container">
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
@section('js')
<script type="text/javascript" src="{{ URL::asset('js/library/table-results_0.2.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/admin/eod/index.js') }}"></script>
@endsection
