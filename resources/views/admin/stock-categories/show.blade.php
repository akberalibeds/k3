@extends('layouts.admin')
@section('title')
Admin Stock Category
@endsection
@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="card card-info">
      <div class=" card-header blue">
        <div class="card-title">
          <div class="title">
            Admin Stock Category
          </div>
        </div>
      </div>
      <div class="card-body">
        <div class="row">
          <div class="col-md-6">
            <dl class="dl-horizontal">
              <dt>Id</dt>
              <dd>{{ $category->id }}</dd>
              <dt>Name</dt>
              <dd>{{ $category->name }}</dd>
              <dt>Description</dt>
              <dd>{{ $category->description }}</dd>
            </dl>
          </div>
          <div class="col-md-6">
            <ul class="list-unstyled">
              <li>
                <a class="btn btn-default" href="{{ route('admin::stock-categories::edit', ['id' => $category->id]) }}" role="button">Edit</a>
              </li>
            </ul>
          </div>
        </div>
      </div>
      <div class="card-footer">
      </div>
    </div>
  </div>
</div>
<div class="row">
  <div class="col-md-12">
    <div class="card card-info">
      <div class=" card-header blue">
        <div class="row">
          <div class="card-title">
            <div class="title">Stock</div>
          </div>
        </div>
      </div>
      @include ('share.stock-list')
    </div>
  </div>
</div>
@endsection

@push('js')
<script>
var categoryId = {{ $category->id }}
var itemCodeQueryUrl = '/admin/stock-categories/' + categoryId + '/search/item-code/%QUERY'
var itemDescriptionQueryUrl = '/admin/stock-categories/' + categoryId + '/search/item-description/%QUERY'
var pagingUrl = '/admin/stock-categories/' + categoryId + '/items'
var stockItemUrl = '/admin/stock/'
</script>
@endpush
