@extends('layouts.admin')

@section('title')
Admin Stock Category - Edit
@endsection

@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="card card-info">
      <div class=" card-header blue">
        <div class="card-title">
          <div class="title">
            Admin Stock Category - Edit
          </div>
        </div>
      </div>
      <div class="card-body">
        {!! Form::model($stockCategory, array('method' => 'put', 'route' => array('admin::stock-categories::update', $stockCategory->id))) !!}
          <div class="form-group">
            {!! Form::label('name', 'Name') !!}
            {!! Form::text('name') !!}
            {!! Form::error('name') !!}
          </div>
          <div class="form-group">
            {!! Form::label('description', 'Description') !!}
            {!! Form::text('description') !!}
            {!! Form::error('description') !!}
          </div>
          <div class="form-group">
            {!! Form::label('always_in_stock', 'Always in stock') !!}
            {!! Form::checkbox('always_in_stock') !!}
            {!! Form::error('always_in_stock') !!}
          </div>
          {!! Form::buttons() !!}
        {!! Form::close() !!}
      </div>
      <div class="card-footer">
      </div>
    </div>
  </div>
</div>
@endsection

@section('js')
<script>
$('input[type="checkbox"]').bootstrapSwitch()
</script>
@endsection
