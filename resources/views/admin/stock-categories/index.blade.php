@extends('layouts.admin')

@section('title')
Admin Stock Categories
@endsection

@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="card card-info">
      <div class=" card-header blue">
        <div class="row">
          <div class="card-title">
            <div class="title">Admin Stock Categories</div>
          </div>
        </div>
      </div>
      <div class="card-body">
        <div class="row">
          <div class="col-md-4 col-sm-6 col-xs-12">
            @include('share.search-bar', [
              'options' => [
                ['value' => 'name', 'title' => 'Name'],
              ],
              'selectable' => false,
              'selectControls' => false,
            ])
          </div>
        </div>
        <div class="row">
          <div class="table-responsive" id="table-results-container">
            <table class="small table table-striped table-no-wrap table-hover" id="table-results-container">
              <thead>
                <tr>
                  <th>Category</th>
                  <th>Description</th>
                  <th class="text-center">Always in stock</th>
                </tr>
              </thead>
              <tbody class="table-clickable" id="result-container">
              </tbody>
            </table>
          </div>
        </div>
      </div>
      <div class="card-footer">
        <div class="row">
          <div class="col-md-12 text-center" id="paging-controls-container">
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
@push('js')
<script type="text/javascript" src="{{ URL::asset('js/library/table-results_0.2.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/admin/stock-categories/index.js') }}"></script>
@endpush
