@extends('layouts.admin')
@section('title')
Admin Postcode Exceptions
@endsection
@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="card card-info">
      <div class=" card-header blue">
        <div class="card-title">
          <div class="title">Admin Postcode Exceptions</div>
        </div>
      </div>
      <div class="card-body">
        <div class="row">
          <div class="col-md-12 text-right">
            <button class="btn btn-default" onclick="addPostcodeException(event)" role="button" type="button">Add Postcode</button>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12">
            <?php $a = ''; ?>
            @foreach ($postcodes as $postcode)
            @if ($a !== substr($postcode, 0, 1))
            <?php $a = substr($postcode, 0, 1); ?>
            <h4 class="bg-info">{{ $a }}</h4>
            @endif
            <?php echo $postcode; ?>
            @endforeach
          </div>
        </div>
      </div>
      <div class="card-footer">
      </div>
    </div>
  </div>
</div>
@endsection
@section('js')
<script type="text/javascript" src="{{ URL::asset('js/admin/postcode-exceptions/index.js') }}"></script>
@endsection
