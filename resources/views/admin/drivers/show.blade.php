@extends('layouts.admin')
@section('title')
Admin Driver - Show
@endsection
@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="card card-info">
      <div class=" card-header blue">
        <div class="card-title">
          <div class="title">
            Admin Driver - Show
          </div>
        </div>
      </div>
      <div class="card-body">
        <div class="row">
          <div class="col-md-5">
          <a href="/admin/drivers/{{ $driver->id }}/edit" class="btn btn-primary">edit</a>
            <dl class="dl-horizontal">
              <dt>Name</dt>
              <dd>{{ $driver->name }}</dd>
              <dt>Score</dt>
              <dd>{{ $driver->score }}</dd>
              <dt>Telephone</dt>
              <dd>{{ $driver->num }}</dd>
              <dt>Rate</dt>
              <dd>£{{ $driver->rate }}</dd>
              <dt>Active</dt>
              <dd>@if ($driver->active) Yes @else No @endif</dd>
               <dt>Username</dt>
              <dd>{{ $driver->uName }}</dd>
               <dt>Password</dt>
              <dd>{{ $driver->pwd }}</dd>
           	</dl>
          </div>
          <div class="col-md-6">
            <div class="file-drop-area" id="upload-drop" data-upload-identifier="{{ $driver->id }}">
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12">
            <ul class="bxslider" id="driver-images">
              @foreach ($driverFiles as $file)
              <li><img src="/drivers/image/{{ $file }}" style="height:auto"></li>
              @endforeach
            </ul>
          </div>
        </div>
      </div>
      <div class="card-footer">
      </div>
    </div>
  </div>
</div>
@endsection
@push('headlinks')
<link href="{{ URL::asset('/css/jquery.bxslider.css') }}" rel="stylesheet" />
@endpush
@section('js')
<script type="text/javascript" src="{{ url ('/js/library/jquery.bxslider.min.js') }}"></script>
<script type="text/javascript" src="{{ url ('/js/library/upload.js') }}"></script>
<script type="text/javascript" src="{{ url ('/js/drivers/show.js') }}"></script>
@endsection
