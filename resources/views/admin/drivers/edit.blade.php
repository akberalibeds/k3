@extends('layouts.admin')
@section('title')
Admin Driver - Edit
@endsection
@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="card card-info">
      <div class=" card-header blue">
        <div class="card-title">
          <div class="title">
            Admin Driver - Edit
          </div>
        </div>
      </div>
      <div class="card-body">
        <div class="row">
          <div class="col-md-2">
          </div>
        </div>
        {!! Form::model($driver, array('method' => 'put', 'route' => array('admin::drivers::update', $driver->id))) !!}
          <div class="form-group">
            {!! Form::label('name', 'Name') !!}
            {!! Form::text('name') !!}
            {!! Form::error('name') !!}
          </div>
          <div class="form-group">
            {!! Form::label('score', 'Score') !!}
            {!! Form::number('score') !!}
            {!! Form::error('score') !!}
          </div>
          <div class="form-group">
            {!! Form::label('num', 'Telephone') !!}
            {!! Form::text('num') !!}
            {!! Form::error('num') !!}
          </div>
          <div class="form-group">
            {!! Form::label('rate', 'Rate') !!}
            {!! Form::money('rate') !!}
            {!! Form::error('rate') !!}
          </div>
          <div class="form-group">
            {!! Form::label('active', 'Active') !!}
            {!! Form::checkbox('active') !!}
            {!! Form::error('active') !!}
          </div>
          
           <div class="form-group">
            {!! Form::label('uName', 'Username') !!}
            {!! Form::text('uName') !!}
            {!! Form::error('uName') !!}
          </div>
          
          	<div class="alert alert-info col-md-6">
          		<i class="fa fa-exclamation-triangle"></i> Leave password blank if you do not wish to change.
          	</div>	
          	<div class="clearfix"></div>
           
           <div class="form-group">
            {!! Form::label('pwd', 'Password') !!}
            {!! Form::password('pwd') !!}
            {!! Form::error('pwd') !!}
          </div>
          {!! Form::buttons() !!}
        {!! Form::close() !!}
      </div>
      <div class="card-footer">
      </div>
    </div>
  </div>
</div>
@endsection
@section('js')
<script>
$('input[type="checkbox"]').bootstrapSwitch()
</script>
@endsection
