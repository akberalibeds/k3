@extends('layouts.admin')
@section('title')
Admin Driver - Create
@endsection
@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="card card-info">
      <div class=" card-header blue">
        <div class="card-title">
          <div class="title">
            Admin Driver - Create
          </div>
        </div>
      </div>
      <div class="card-body">
        {!! Form::open(array('method' => 'post', 'route' => array('admin::drivers::store'))) !!}
          <div class="form-group">
            {!! Form::label('name', 'Name') !!}
            {!! Form::text('name') !!}
            {!! Form::error('name') !!}
          </div>
          <div class="form-group">
            {!! Form::label('uName', 'Username') !!}
            {!! Form::text('uName') !!}
            {!! Form::error('uName') !!}
          </div>
          <div class="form-group">
            {!! Form::label('pwd', 'Password') !!}
            {!! Form::password('pwd') !!}
            {!! Form::error('pwd') !!}
          </div>
          <div class="form-group">
            {!! Form::label('score', 'Score') !!}
            {!! Form::number('score') !!}
            {!! Form::error('score') !!}
          </div>
          <div class="form-group">
            {!! Form::label('num', 'Telephone') !!}
            {!! Form::tel('num') !!}
            {!! Form::error('num') !!}
          </div>
          <div class="form-group">
            {!! Form::label('rate', 'Rate') !!}
            {!! Form::money('rate') !!}
            {!! Form::error('rate') !!}
          </div>
         
          {!! Form::buttons() !!}
        {!! Form::close() !!}
      </div>
      <div class="card-footer">
      </div>
    </div>
  </div>
</div>
@endsection
@section('js')
<script>
$('input[type="checkbox"]').bootstrapSwitch()
</script>
@endsection
