@extends('layouts.admin')
@section('title')
Admin Driver - Show
@endsection
@section('content')
<div class="row">


 <div class="col-md-3">
 	<div class="row">
	 	
	        		
			        	<div class="list-group">
			        	
			        	@foreach($drivers as $driver)
						  <div class="list-group-item list-{{ $driver->driver_id }}  @if(!$driver->active) alert-danger alert @endif">
						   
						    	<div class="btn-group pull-right">
								  <a href="#" class=" dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bars fa-lg"></i></a>
								  <ul class="dropdown-menu">
								    <li><a href="#" onclick="google.maps.event.trigger(markers[{{ $driver->driver_id }}],'click')">Show on Map</a></li>
								    <li><a class="current-del" href="#" onclick="current('{{ $driver->order_id }}');">Current Delivery</a></li>
								   
								   @if($driver->route)
								   		<li><a class="view-route" href="#" onclick="route('{{ $driver->route->id }}','{{ $driver->route->route }}','{{ $driver->route->date }}');">View Route</a></li>
								   @endif
								    <li><a href="#" onclick="reloadRoute({{ $driver->driver_id }})">Refresh Route</a></li>
								    <li class="divider"></li>
								    <li><a href="#" onclick="logout({{ $driver->driver_id }})">Logout Driver</a></li>
								  </ul>
								</div>
														
							 <div id="">
						    	<h4 class="list-group-item-heading name">{{ $driver->name }}</h4>
						    	@if($driver->active || $driver->route)
						    		<p class="list-group-item-text body">Delivery: {{ $driver->del }}<br>{{ $driver->updated_at }}</p>
						    	@endif
						    </div>	
								
						     <div class="clearfix"></div>
						  </div>
						@endforeach
						</div>
	        	
	      
 	</div>
 </div>


  <div class="col-md-9">
    <div class="card card-info">
      <div class="card-body map" id="map">
        
        	
        
      </div>
      <div class="card-footer">
      </div>
    </div>
  </div>
</div>
@endsection
@push('headlinks')
<link href="{{ URL::asset('/css/jquery.bxslider.css') }}" rel="stylesheet" />
@endpush
@section('js')
<script src="https://maps.googleapis.com/maps/api/js?key={{ $maps_key }}"></script>
<script src="/js/markerAnimate.js"></script>


<script>
var drivers  = $.parseJSON('{!! @json_encode($drivers) !!}');
var markers = {};
var path;
var map;

$(window).load(function(e) {
	$('.map').css('height',$(window).height()-90);
      initializeMaps();
      addMarkers();
      setTimeout(getLocations,10000);
});



var style = 

	[
	    {
	        "stylers": [
	            {
	                "hue": "#ff1a00"
	            },
	            {
	                "invert_lightness": true
	            },
	            {
	                "saturation": -100
	            },
	            {
	                "lightness": 33
	            },
	            {
	                "gamma": 0.5
	            }
	        ]
	    },
	    {
	        "featureType": "water",
	        "elementType": "geometry",
	        "stylers": [
	            {
	                "color": "#2D333C"
	            }
	        ]
	    }
	];

function initializeMaps() {

	console.log('started');
	var myOptions = {
		mapTypeId: google.maps.MapTypeId.ROADMAP,
		mapTypeControl: false,
		disableDoubleClickZoom : true,
		styles: style
	};
	var i;
	map = new google.maps.Map(document.getElementById("map"),myOptions);
	window.map=map;
	 infowindow = new google.maps.InfoWindow();
/*
	 var poly = new google.maps.Polyline({
		    strokeColor: '#42adf4',
		    strokeOpacity: 2.0,
		    strokeWeight:4,
		    map:map
		  });
		// Get the path form polyLine
	 path = poly.getPath();
*/
	 bounds = new google.maps.LatLngBounds();

	 var pos = new google.maps.LatLng(52.57111, -2.0237600000000384);
		//bounds.extend(pos);
		
		/*marker = new google.maps.Marker({
			position: pos,
			map: map,
			title:'Giomani',
			icon: 'https://chart.apis.google.com/chart?chst=d_map_spin&chld=1.0|0|FFFF42|13|b|'
		});

		google.maps.event.addListener(marker, 'click', (function(marker, i) {
			return function() {
				infowindow.setContent("Giomani");
				infowindow.open(map, marker);
			}

		})(marker, i));*/
		//map.fitBounds(bounds);
}

/*
*
* Add all deliveries to map
*
*/
function addMarkers(){

	$.each(drivers,function(z,data){
			//if(!routeColor.hasOwnProperty(data.delRoute)){
			//		setColor(data.delRoute);
			//	}
			addMarker(data.lat,data.lng,data.name,data.driver_id,data.updated_at,'https://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=|00F|FFF'/*99ff66|FFF'*/,z);
	});

}



/*
 *
 * Add Marker Function
 *
 */
function addMarker(lat,lng,title,id,content,icon,i){


		var pos = new google.maps.LatLng(lat,lng);
		bounds.extend(pos);
		marker = new google.maps.Marker({
			position: pos,
			map: map,
			title: ""+title,
			icon: icon
		});

		marker.set("id",id);
		var html ='<div id="infowindow"><div id="info'+id+'"><strong>'+title+'</strong><br>'+content+'</div></div>';
		infowindow.setOptions({content: html});
        infowindow.open(map, marker);
		/*
		Marker Right Click Function
		*/
		google.maps.event.addListener(marker, 'click', (function(marker, id) {
        return function() {

			var html ='<div id="infowindow"><div id="info'+id+'"><strong>'+title+'</strong><br>'+content+'</div></div>';
			infowindow.setOptions({content: html});
            infowindow.open(map, marker);
            
        }

		})(marker, i));


	map.fitBounds(bounds);
	markers[id] = marker;

	//path.push(pos);
	
	
}


function getLocations(){

	$.get("",function(data){

		$.each(data,function(key,driver){

			var pos = new google.maps.LatLng(driver.lat, driver.lng);

			markers[driver.driver_id].animateTo(pos);
			
			$('#info'+driver.driver_id).html('<strong>'+driver.name+'</strong><br>'+driver.updated_at);
			$('.list-'+driver.driver_id+' .name').html(driver.name);
			$('.list-'+driver.driver_id+' .body').html('Delivery: '+driver.del+"<br>"+driver.updated_at);
			$('.list-'+driver.driver_id+' .current-del').attr('onclick',"current('"+driver.order_id+"')");
			//bounds.extend(pos);
			//path.push(pos);
			if(driver.route){
				$('.list-'+driver.driver_id+' .body').prepend(driver.route.route+'<br>');
				$('.list-'+driver.driver_id+' .view-route').attr('onclick',"route('"+driver.route.id+"','"+driver.route.route+"','"+driver.route.date+"')");
			}
			
			
		});

		//map.fitBounds(bounds);
		setTimeout(getLocations,10000);
	});
	
}

function current(order){

	if(order){
		window.open("/orders/"+order);	
	}
}

function logout(driver){

	$.get("/admin/drivers/map/send/"+driver+"/logout",function(data){
		
	});
	
}

function notdelivered(driver){

	$.get("/admin/drivers/map/send/"+driver+"/notdelivered",function(data){
		
	});
	
}


function reloadRoute(driver){

	$.get("/admin/drivers/map/send/"+driver+"/reloadRoute",function(data){
		
	});
	
}

function route(id,route,date){

	$.form({
	    method: 'GET',
	    // the route name (row.route) is only needed as long as the route ids are not
	    // being filled in the databse
	    url: '/dispatch/dispatched/' + id + '/show/' + date + '/' + route,
	    success: function (data) {
	      var i, j
	      var datum
	      var html ='';
	      for (i = 0, j = data.length; i < j; i++) {
	        datum = data[i]
			var clas = 'danger';
			if(datum.orderStatus=='Delivered'){
				clas='success';
			}
	        
	        html += '<tr class="'+clas+'" onclick="window.open(\'/orders/' + datum.oid + '\')">'
	        html += '<td>' + datum.delOrder + '</td>'
	        html += '<td>' + datum.dBusinessName + '</td>'
	        html += '<td>' + datum.dPostcode + '</td>'
	        html += '<td>' + datum.orderStatus + '</td>'
	        html += '<td>' + datum.tel + '</td>'
	        html += '<td class="text-center">' + (datum.txt === '1' ? '<i class="color-success fa fa-check" aria-hidden="true"></i>' : '<i class="color-danger fa fa-times" aria-hidden="true"></i>') + '</td>'
	        html += '</tr>';
	      }


	      var dialog = bootbox.dialog({
	    	    message: '<table class="table">'+html+'</table>',
	    	    title: route,
	    	    buttons:{
					Close: function(){

					}
	    	    }
	    	});

			
		     

	      
	    },
	    error: null
	  })	

}
</script>

@endsection
