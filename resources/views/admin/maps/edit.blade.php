@extends('layouts.admin')

@section('title')
Admin Dashboard
@endsection

@section('content')


@if(isset($save))
	<div class="alert alert-success"> Settings Saved </div>
@endif

  <div class="col-md-12">
    <div class="card card-info">
      <div class=" card-header blue">
        <div class="card-title">
          <div class="title">Maps Settings</div>
        </div>
      </div>
      <div class="card-body">

		        
			
           {!! Form::open() !!} 
         
          <div class="form-group col-md-12">
            {!! Form::label('maps_key', 'Maps API KEY') !!}
            {!! Form::text('maps_key',$settings['maps_key']) !!}
            {!! Form::error('maps_key') !!}
          </div>   
          
		
        {!! Form::buttons() !!}
        
        {!! Form::close() !!} 
        

      </div>
    </div>
  </div>



<style>
	.th {
		background-color:lightgrey;
	}
</style>
@endsection
