@extends('layouts.admin')

@section('title')
Admin Add Ip Address
@endsection

@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="card card-info">
      <div class=" card-header blue">
        <div class="card-title">
          <div class="title">
            Admin Add Ip Address
          </div>
        </div>
      </div>
      <div class="card-body">

        <div class="row">
          <div class="col-md-12">
            Your IP Address - {{ $currentIpa }}
          </div>
        </div>

        <div class="row">
          <div class="col-md-12">
            {!! Form::open(array('method' => 'post', 'route' => array('admin::ips::store'))) !!}
              <div class="form-group">
                {!! Form::label('address', 'IP Address') !!}
              </div>
                <input min="0" max="255" size="3" maxlength="3" name="ip0" value="" id="ip0" type="number">
                <input min="0" max="255" size="3" maxlength="3" name="ip1" value="" id="ip1" type="number">
                <input min="0" max="255" size="3" maxlength="3" name="ip2" value="" id="ip2" type="number">
                <input min="0" max="255" size="3" maxlength="3" name="ip3" value="" id="ip3" type="number">
              {!! Form::buttons() !!}
            {!! Form::close() !!}
          </div>
        </div>
      </div>
      <div class="card-footer">
      </div>
    </div>
  </div>
</div>
@endsection
