@extends('layouts.admin')
@section('title')
Admin IP Addresses
@endsection
@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="card card-info">
      <div class=" card-header blue">
        <div class="row">
          <div class="card-title">
            <div class="title">Admin IP Addresses</div>
          </div>
        </div>
      </div>
      <div class="card-body">
        <div class="row">
          <div class="col-md-12">
            Your IP Address - {{ $currentIpa }}
          </div>
        </div>
        <div class="row">
          <div class="col-md-6">
            @include('share.search-bar', [
              'options' => [
                ['value' => 'address', 'title' => 'IP Address'],
              ],
              'selectable' => false,
            ])
          </div>
          <div class="col-md-6 text-right">
            <a class="btn btn-default" href="{{ route('admin::ips::create') }}">Add IP Address</a>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12">
            <div class="table-responsive">
              <table class="small table table-striped table-no-wrap table-hover" id="table-results-container">
                <thead>
                  <tr>
                    <th>Address</th>
                    <th>Description</th>
                    <th>Created</th>
                  </tr>
                </thead>
                <tbody class="table-clickable" id="result-container">
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
      <div class="card-footer">
        <div class="row">
          <div class="col-md-12 text-center" id="paging-controls-container">
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
@section('js')
<script type="text/javascript" src="{{ URL::asset('js/library/table-results_0.2.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/admin/ips/index.js') }}"></script>
@endsection
@push('html_stash')
<span data-stash-name="stash-ip-form">
  <div class="row">
    {!! Form::number('ip0', null, ['min' => '0', 'max' => '255', 'size' => '3', 'maxlength' => '3'], [3, 3]) !!}
    {!! Form::number('ip1', null, ['min' => '0', 'max' => '255', 'size' => '3', 'maxlength' => '3'], [3, 3]) !!}
    {!! Form::number('ip2', null, ['min' => '0', 'max' => '255', 'size' => '3', 'maxlength' => '3'], [3, 3]) !!}
    {!! Form::number('ip3', null, ['min' => '0', 'max' => '255', 'size' => '3', 'maxlength' => '3'], [3, 3]) !!}
  </div>
  <div class="row">
    {!! Form::label('description', 'Description') !!}
    {!! Form::textarea('description', '', [], [12, 12]) !!}
  </div>
</span>
@endpush
