@extends('layouts.admin')

@section('title')
Admin Edit seller - {{ $seller->name }} ({{ $seller->iName }})
@endsection

@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="card card-info">
      <div class=" card-header blue">
        <div class="card-title">
          <div class="title">
            Admin Edit seller - {{ $seller->name }} ({{ $seller->iName }})
          </div>
        </div>
      </div>
      <div class="card-body">

        {!! Form::model($seller, array('method' => 'put', 'route' => array('admin::sellers::update', $seller->id))) !!}

          <div class="form-group">
            {!! Form::label('name', 'Name') !!}
            {!! Form::text('name') !!}
            {!! Form::error('name') !!}
          </div>

          <div class="form-group">
            {!! Form::label('iName', 'iName') !!}
            {!! Form::text('iName') !!}
            {!! Form::error('iName') !!}
          </div>

          <div class="form-group">
            {!! Form::label('account_limit', 'Account Limit') !!}
            {!! Form::money('account_limit') !!}
            {!! Form::error('account_limit') !!}
          </div>

          <div class="form-group">
            {!! Form::label('discount', 'Discount') !!}
            {!! Form::percent('discount') !!}
            {!! Form::error('discount') !!}
          </div>

          <div class="form-group">
            {!! Form::label('email', 'Email') !!}
            {!! Form::email('email') !!}
            {!! Form::error('email') !!}
          </div>

          <div class="form-group">
            {!! Form::label('num', 'House Number') !!}
            {!! Form::text('num') !!}
            {!! Form::error('num') !!}
          </div>

          <div class="form-group">
            {!! Form::label('street', 'Street') !!}
            {!! Form::text('street') !!}
            {!! Form::error('street') !!}
          </div>

          <div class="form-group">
            {!! Form::label('town', 'Town') !!}
            {!! Form::text('town') !!}
            {!! Form::error('town') !!}
          </div>

          <div class="form-group">
            {!! Form::label('pc', 'Postcode') !!}
            {!! Form::text('pc') !!}
            {!! Form::error('pc') !!}
          </div>

          <div class="form-group">
            {!! Form::label('tel', 'Telephone') !!}
            {!! Form::tel('tel') !!}
            {!! Form::error('tel') !!}
          </div>

          <div class="form-group">
            {!! Form::label('force_pay', 'Force Pay') !!}
            {!! Form::checkbox('force_pay') !!}
            {!! Form::error('force_pay') !!}
          </div>
          
          <div class="form-group">
            {!! Form::label('custom_price', 'Use Custom Price') !!}
            {!! Form::checkbox('custom_price') !!}
            {!! Form::error('custom_price') !!}
          </div>
          
          <div class="alert alert-info col-md-6">
          	<i class="fa fa-exclamation-triangle"></i> Leave password blank if you do not wish to change
          </div>
          <div class="clearfix"></div>
          
          <div class="form-group">
            {!! Form::label('password', 'Password ') !!}
            {!! Form::password('password') !!} <div class="pull-left"><button type="button" class="btn btn-default" onClick="generate();">Generate</button></div>
            {!! Form::error('password') !!}
          </div>
          
          <div class="form-group">
            {!! Form::label('password_confirmation', 'Confirm Password') !!}
            {!! Form::password('password_confirmation') !!}
            {!! Form::error('password_confirmation') !!}
          </div>

          {!! Form::buttons() !!}

        {!! Form::close() !!}

      </div>
      <div class="card-footer">
      </div>
    </div>
  </div>
</div>
@endsection

@section('js')
<script>
$('input[type="checkbox"]').bootstrapSwitch();

function generate(){
	var hash = code();
	$('#password').attr('type','text');
	$('#password').val(hash);	
	$('#password_confirmation').val(hash);	
}


function code()
{
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for( var i=0; i < 8; i++ )
        text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
}

</script>
@endsection
