@extends('layouts.admin')
@section('title')
Admin Sellers
@endsection
@section('content')
<div class="row">
  <div class="card card-info">
    <div class="card-header blue">
      <div class="card-title">
        <div class="title">Admin Sellers</div>
      </div>
    </div>
    <div class="card-body">
      <div class="row">
      
        <div class="col-md-6 col-sm-12 col-xs-12">
          @include('share.search-bar', [
            'options' => [
              ['value' => 'name', 'title' => 'Name'],
              ['value' => 'email', 'title' => 'Email'],
              ['value' => 'pc', 'title' => 'Postcode'],
            ],
            'selectable' => false,
            'selectControls' => false,
          ])
        </div>
        <div class="col-md-6" align="right">
        	<button class="btn btn-info" onClick="addSeller();">Add Seller</button>
        </div>
      </div>
      <div class="table-responsive">
        <table class="small table table-condensed table-striped" id="table-results-container">
          <thead>
            <tr>
              <th>Name (iName)</th>
              <th>Email</th>
              <th>Postcode</th>
              <th class="text-right">Account Limit (&pound;)</th>
              <th class="text-center">Force Pay</th>
            </tr>
          </thead>
          <tbody id="result-container">
          </tbody>
        </table>
      </div>
    </div>
    <div class="card-footer">
      <div class="row">
        <div class="col-md-12 text-center" id="paging-controls-container">
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
@section('js')
<script type="text/javascript" src="{{ URL::asset('js/library/table-results_0.2.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/admin/sellers/index.js') }}"></script>
<script>
	function addSeller(){
		window.open('/admin/sellers/create');	
	}
</script>

@endsection
