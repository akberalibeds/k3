@extends('layouts.admin')
@section('title')
Admin Show Log
@endsection
@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="card card-info">
      <div class=" card-header blue">
        <div class="card-title">
          <div class="title">
            Admin Show Log
          </div>
        </div>
      </div>
      <div class="card-body">
        <div class="row">
          <div class="col-md-6">
            <div class="table-responsive">
              <table class="table">
                <tr><th>Action</th><td>{{ $log->action }}</td></tr>
                <tr><th>Hash</th><td>{{ $log->hash }}</td></tr>
                <tr><th>Hash state</th><td>{{ $hashState }}</td></tr>
                <tr><th>More</th><td>{{ $log->more }}</td></tr>
                <tr><th>User</th><td>{{ $log->email }}</td></tr>
                <tr><th>Term</th><td>{{ $log->term }}</td></tr>
                <tr><th>U</th><td>{{ $log->u }}</td></tr>
                <tr><th>Created</th><td>{{ $log->created_at }}</td></tr>
                <tr><th>Updated</th><td>{{ $log->updated_at }}</td></tr>
              </table>
            </div>
          </div>
        </div>
      </div>
      <div class="card-footer">
      </div>
    </div>
  </div>
</div>
@endsection
@section('js')
<script type="text/javascript" src="{{ URL::asset('js/admin/stock/show.js') }}"></script>
@endsection
