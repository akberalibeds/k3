@extends('layouts.admin')

@section('title')
Admin Log
@endsection

@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="card card-info">
      <div class=" card-header blue">
        <div class="row">
          <div class="card-title">
            <div class="title">Admin Log</div>
          </div>
        </div>
      </div>
      <div class="card-body">
        <div class="row">
          <div class="col-md-4">
            <strong>Level</strong>
            <br>
            0&nbsp;-&nbsp;<input autocomplete="off" checked name="level" type="radio" value="0"><br>
            @for ($i = 1; $i < $maxLevel; $i++)
            {{ $i }}&nbsp;-&nbsp;<input autocomplete="off" name="level" type="radio" value="{{ $i }}"><br>
            @endfor
          </div>
          <div class="col-md-4">
            <button class="btn btn-defualt" onclick="runVerify()" role="button">Verify</button>
            <br>
            <span id="verify-message"></span>
          </div>
          <div class="col-md-4">
            <strong>Result</strong>
            <br>
            <ul class="list-unstyled" id="verify-results">
              0&nbsp;-&nbsp;<span id="result-0"></span><br>
              @for ($i = 1; $i < $maxLevel; $i++)
              {{ $i }}&nbsp;-&nbsp;<span id="result-{{ $i }}"></span><br>
              @endfor
            </ul>
          </div>
        </div>
        <br>
        <div class="row">
          <div class="col-md-6 col-sm-12 col-xs-12">
            @include('share.search-bar', [
              'options' => [
                ['value' => 'itemCode', 'title' => 'Item Code'],
              ],
              'selectable' => false,
              'selectControls' => false,
            ])
          </div>
        </div>
        <div class="row">
          <div class="table-responsive">
            <table class="small table table-striped table-no-wrap table-hover" id="table-results-container">
              <thead>
                <tr>
                  <th>Action</th>
                  <th>More</th>
                  <th>User</th>
                  <th>Date and Time</th>
                </tr>
              </thead>
              <tbody class="table-clickable" id="result-container">
              </tbody>
            </table>
          </div>
        </div>
      </div>
      <div class="card-footer">
        <div class="row">
          <div class="col-md-12 text-center" id="paging-controls-container">
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
@push('js')
<script type="text/javascript">
var actionGroups = new Map()
@foreach ($actionGroups as $action => $description)
actionGroups.set('{{ $action }}', '{!! $description !!}')
@endforeach
var actions = new Map()
@foreach ($actions as $action => $description)
actions.set('{{ $action }}', '{!! $description !!}')
@endforeach
</script>
<script type="text/javascript" src="{{ URL::asset('js/library/table-results_0.2.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/admin/log/index.js') }}"></script>
@endpush
