@extends('layouts.admin')

@section('title')
Warehouse Stock
@endsection

@section('css')
<link href="{{ asset('dist/css/app.css') }}" rel="stylesheet">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
				    <div id="app">
@include('layouts.modal',['view'=>'admin.warehouse.import'])
								<warehouse-component
								:warehouse="{{ json_encode($warehouse) }}"
								:warehouses="{{ json_encode($warehouses) }}"
								>
								</warehouse-component>
						</div>
				</div>
		</div>

	<!-- <div class="row">
		<div class="card card-info">
			<div class="card-header blue">
			<div class="pull-right"><button onclick="exportCSV()" class="btn btn-primary">Export</button></div>
				<div class="card-title">
					<div class="title">{{ $warehouse->location }}</div>
				</div>
			</div>
			<div class="card-body">
				
				<div class="table-responsive">
					<table class="table table-condensed table-striped">
						<thead>
							<tr>
								<th>SKU</th>
								<th>Part</th>
								<th  class="text-center">Total</th>
								<th class="text-center">{{ $warehouse->location }}</th>
								<th class="text-center">In</th>
								<th class="text-center">Out</th>
							</tr>
						</thead>
						<tbody id="vehicles-table">
							
							
						</tbody>
					</table>
				</div>
			</div>
			<div class="card-footer">
				
			</div>
		</div>
	</div> -->

<script src="{{ asset('dist/js/app.js') }}"></script>
@endsection

@push('js')
<!-- <script>
	var warehouses = {!! @json_encode($warehouses) !!};

	var reason = '<select class="form-control reason">';

	$.each(warehouses,function(i,w){
		reason+='<option value="'+w.location+'">'+w.location+'</option>';
	});
	
	reason+='</select>';
	
	var dt = $('.table').DataTable({
        processing: true,
        serverSide: true,
        ajax: '',
        columns: [
            { data: 'itemCode', name: 'stock_items.itemCode' },
            { data: 'part', name: 'stock_parts.part',  searchable: false,  orderable: false },
            { data: 'quantity', name: 'quantity',  searchable: false ,  orderable: false, class:'text-center' },
            { data: 'warehouse_id', name: 'warehouse_stock.warehouse_id',  searchable: false,  orderable: false , class:'text-center',
                 render: function ( data, type, row ) {
                 	return row.location[{{ $warehouse->id }}];
             	}  
         	},
         	{ data: 'warehouse_id', name: 'warehouse_stock.warehouse_id',  searchable: false,  orderable: false , class:'text-center',
                render: function ( data, type, row ) {
                	return  '<button class="btn btn-success btn-sm" onclick="addPart('+ row.stock_parts_id +',\''+row.itemCode+' '+row.part+'\')">Part</button> | <button onclick="addItem('+row.parent_id+',\''+row.itemCode+'\')" class="btn btn-success btn-sm">Full Item</button>'
            	}  
        	},
        	{ data: 'warehouse_id', name: 'warehouse_stock.warehouse_id',  searchable: false,  orderable: false , class:'text-center',
                render: function ( data, type, row ) {
                	return '<button class="btn btn-danger btn-sm" onclick="removePart('+ row.stock_parts_id +',\''+row.itemCode+' '+row.part+'\')">Part</button> | <button onclick="removeItem('+row.parent_id+',\''+row.itemCode+'\')" class="btn btn-danger btn-sm">Full Item</button>'
            	}  
        	}
        ]
    });

	

	
	function addPart(id,title){
		var html = '<h4>'+title+' - ONLY</h4><h4>Part Came From:</h4>'+reason+'<br>In:<input class="form-control qty" type="number">';
		bootbox.confirm(html,function(result){
			if(result){
				var log = $('.reason').val();
				var qty = $('.qty').val();
				$.get("part/add?log="+log+"&qty="+qty+"&part="+id+"&warehouse={{ $warehouse->id }}",function(response){
                                  if (response.error) {
                                    alert(response.error);
                                  } else {
                                    dt.ajax.reload(null,false);
                                  }
				});	
			}
			console.log($('.reason').val()+' '+$('.qty').val()+' '+result);
		});
	}
	function removePart(id,title){
		var html = '<h4>'+title+' - ONLY</h4><h4>Part Going To:</h4>'+reason+'<br>Out:<input class="form-control qty" type="number">';
		bootbox.confirm(html,function(result){
			if(result){
				var log = $('.reason').val();
				var qty = $('.qty').val();
				$.get("part/remove?log="+log+"&qty="+qty+"&part="+id+"&warehouse={{ $warehouse->id }}",function(response){
                                  if (response.error) {
                                    alert(response.error);
                                  } else {
                                    dt.ajax.reload(null,false);
                                  }						
				});	
			}
			console.log($('.reason').val()+' '+$('.qty').val()+' '+result);
		});
	}
	function addItem(id,title){
		var html = '<h4>'+title+'</h4><h4>Item Came From:</h4>'+reason+'<br>In:<input class="form-control qty" type="number">';
		bootbox.confirm(html,function(result){

			if(result){
				var log = $('.reason').val();
				var qty = $('.qty').val();
				$.get("item/add?log="+log+"&qty="+qty+"&item="+id+"&warehouse={{ $warehouse->id }}",function(response){
                                  if (response.error) {
                                    alert(response.error);
                                  } else {
                                    dt.ajax.reload(null,false);
                                  }						
				});	
			}
			console.log($('.reason').val()+' '+$('.qty').val()+' '+result);
		});
	}
	function removeItem(id,title){
		var html = '<h4>'+title+'</h4><h4>Item Going To:</h4>'+reason+'<br>Out:<input class="form-control qty" type="number">';
		bootbox.confirm(html,function(result){

			if(result){
				var log = $('.reason').val();
				var qty = $('.qty').val();
				$.get("item/remove?log="+log+"&qty="+qty+"&item="+id+"&warehouse={{ $warehouse->id }}",function(response){
                                  if (response.error) {
                                    alert(response.error);
                                  } else {
                                    dt.ajax.reload(null,false);
                                  }
				});	
			}
			console.log($('.reason').val()+' '+$('.qty').val()+' '+result);
		});
	}
</script> -->
@endpush
