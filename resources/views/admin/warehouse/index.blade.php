@extends('layouts.admin')

@section('title')
Warehouse Stock
@endsection

@section('content')
<div class="row">
  <div class="card card-info">
    <div class="card-header blue">
    <div class="pull-right"><button onclick="exportCSV()" class="btn btn-primary">Export</button></div>
      <div class="card-title">
        <div class="title">Warehouse Stock</div>
      </div>
    </div>
    <div class="card-body">
      
      <div class="table-responsiv">
        <table class="table table-condensed table-striped">
          <thead>
            <tr>
              <th>SKU</th>
              <th>Part</th>
              <th  class="text-center">Full</th>
              <th  class="text-center">Total</th>
            	@foreach($warehouses as $w)
              		<th class="text-center" onclick="view({{ $w->id }})">{{ $w->location }}</th>
            	@endforeach
            </tr>
          </thead>
          <tbody id="vehicles-table">
           	<?php /*
           	@foreach($stock as $s)
           	
           		<tr>
           			<td>{{ $s->itemCode }}</td>
           			<td>{{ $s->part }}</td>
           			<td>{{ @min($s->location) }}</td>
           			@foreach($warehouses as $w)
              			<td>{{ $s->location[$w->id] }}</td>
            		@endforeach
           			
           		</tr>
           	@endforeach
           	
           	*/
           	?>
           	
          </tbody>
        </table>
      </div>
    </div>
    <div class="card-footer">
      
    </div>
  </div>
</div>
<style>
	
	.table th:hover{
		cursor:pointer;
	}
		
</style>
@endsection

@push('js')
<script>
	var warehouses = {!! @json_encode($warehouses) !!};


	$(window).load(function(e){
		
	});
	
	$('.table').DataTable({
        processing: true,
        serverSide: true,
        ajax: '',
        columns: [
            { data: 'itemCode', name: 'stock_items.itemCode' },
            { data: 'part', name: 'stock_parts.part',  searchable: false,  orderable: false },
            { data: 'full', name: 'full',  searchable: false,  orderable: false , class:'text-center',
	            render: function ( data, type, row ) {
	            	return data;//+' ('+ row +')';
	        	}  
	    	},
            { data: 'quantity', name: 'quantity',  searchable: false ,  orderable: false, class:'text-center' },
            { data: 'warehouse_id', name: 'warehouse_stock.warehouse_id',  searchable: false,  orderable: false , class:'text-center',
                 render: function ( data, type, row ) {
                 	return row.location[1];//+' ('+ row +')';
             	}  
         	},
            { data: 'warehouse_id', name: 'warehouse_stock.warehouse_id',  searchable: false,  orderable: false , class:'text-center',
                 render: function ( data, type, row ) {
                 	return row.location[2];//+' ('+ row +')';
             	}  
         	},
            { data: 'warehouse_id', name: 'warehouse_stock.warehouse_id',  searchable: false,  orderable: false , class:'text-center',
                 render: function ( data, type, row ) {
                 	return row.location[8];//+' ('+ row +')';
             	}  
         	},
            { data: 'warehouse_id', name: 'warehouse_stock.warehouse_id',  searchable: false,  orderable: false , class:'text-center',
                 render: function ( data, type, row ) {
                 	return row.location[9];//+' ('+ row +')';
             	}  
         	},
            { data: 'warehouse_id', name: 'warehouse_stock.warehouse_id',  searchable: false,  orderable: false , class:'text-center',
                 render: function ( data, type, row ) {
                 	return row.location[10];//+' ('+ row +')';
             	}  
         	},
            { data: 'warehouse_id', name: 'warehouse_stock.warehouse_id',  searchable: false,  orderable: false , class:'text-center',
                 render: function ( data, type, row ) {
                 	return row.location[11];//+' ('+ row +')';
             	}  
         	},
            { data: 'warehouse_id', name: 'warehouse_stock.warehouse_id',  searchable: false,  orderable: false , class:'text-center',
                 render: function ( data, type, row ) {
                 	return row.location[12];//+' ('+ row +')';
             	} 

            },
            { data: 'warehouse_id', name: 'warehouse_stock.warehouse_id',  searchable: false,  orderable: false , class:'text-center',
                render: function ( data, type, row ) {
                	return row.location[13];//+' ('+ row +')';
            	} 

           },
        ]
    });

	function view(id){
		window.open("stock/"+id);
	}
	
	function exportCSV(){
		window.open("stock/export");
	}
</script>
@endpush
