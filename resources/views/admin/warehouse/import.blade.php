@section('title')
Upload CSV
@endsection
@section('form')
{!!Form::open(array('url' => '/admin/warehouse/stock/'.$warehouse->id . '/upload','files'=>'true','id'=>'uploadForm'))!!}
<div class="form-group">
{!! Form::label('csv', 'CSV file')!!}
{!! Form::file('csv')!!}
</div>
<div class="form-group">
    <div class="col-md-2 col-sm-2"><label>Direction</label></div>
  <div class="col-md-8 col-sm-8">
      <input type="radio" name="direction" value="out" id="direction-out" checked="checked"/> <label for="direction-out">Out of warehouse {{$warehouse->location}}</label><br/>
      <input type="radio" name="direction" value="in" id="direction-in"/> <label for="direction-in">Into warehouse {{$warehouse->location}}</label>
  </div>
</div>
<div class="form-group">
  <div class="col-md-2 col-sm-2"><label>To/From</label></div>
{!! Form::select('thatWarehouse',array_column($warehouses->toArray(),'location','location'))!!}
</div>
<br/>
<br/>
<br/>
<br/>
<br/>
        <span class="pull-right">
</span>
{!!Form::close()!!}
@endsection
@section('buttons')
<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
<button type='submit' class='btn btn-primary'>Run Import</button>
@endsection