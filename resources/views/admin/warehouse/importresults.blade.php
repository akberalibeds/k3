@section('title')
Importing Stock CSV
@endsection
@section('title-test')
Importing Stock CSV - trial run
@endsection
@section('success-message')
Successfully imported {{count($action->getSuccess())}} stock item movements
@endsection
@section('test-success-message')
Tested import of {{count($action->getSuccess())}} - no problems found
@endsection
@section('test-failure-message')
<b>{{count($action->getSuccess())}}</b> items will import correctly, but <b>{{count($action->getFailures())}}</b> items have problems and will not import
@endsection
@section('failure-message')
<b>{{count($action->getSuccess())}}</b> items were imported successfully, but <b>{{count($action->getFailures())}}</b> items were not imported
@endsection
@include('bulk.results')