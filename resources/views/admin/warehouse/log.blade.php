
<table class="table table-condensed table-striped">
    <legend>Stock movements for {{$itemcode}} {{$part}} in {{$location}}</legend>
    <thead>
    <tr>
    <th>Date</th>
    <th>User</th>
    <th>Action</th>
    </tr>
    </thead>      
    <tbody>
  @foreach($logs as $log)
  <tr>
      <td>{{$log->updated_at}}</td>
      <td>{{$log->user->name}}</td>
      <td>{{$log->message}}</td>
  </tr>
  @endforeach
  </tbody>
</table>