@extends('layouts.admin')

@section('title')
Admin Dashboard
@endsection

@section('content')

  <div class="col-md-12">
    <div class="card card-info">
      <div class=" card-header blue">
        <div class="card-title">
          <div class="title">Ebay Settings</div>
        </div>
         <span class="pull-right"><a href="/admin/ebay/edit" class="btn btn-primary">Edit</a></span>
      </div>
      <div class="card-body">

		<table class="table table-responsive">        
			
          @foreach($pda as $key => $val)  
      		
            <tr><td><div>{{ $key }}</div></td><td><div>{{ $val }}</div></td></tr>
      		
          @endforeach  	 
		
        </table>      

      </div>
    </div>
  </div>





@endsection
