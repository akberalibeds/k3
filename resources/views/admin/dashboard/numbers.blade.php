@extends('layouts.admin')

@section('title')
Admin Dashboard Telephone Numbers
@endsection

@section('content')
<div class="row">
  <div class="card card-info">
    <div class="card-header blue">
      <div class="card-title">
        <div class="title">Admin Dashboard Telephone Numbers</div>
      </div>
    </div>
    <div class="card-body">
      <div class="row">
        <div class="col-md-12">
          @if (count($numbers) === 0)
          There are no telephone numbers - <button class="btn btn-default btn-xs" onclick="addInfo()" role="button" type="button">Add numbers</button>
          @else
          <div class="table-responsive">
            <table class="small table table-condensed table-striped table-no-wrap">
              <thead>
                <tr>
                  <th>Description</th>
                  <th>Number</th>
                  <th class="td-buttons"><button class="btn btn-default btn-xs" onclick="addInfo()" role="button" type="button">Add numbers</button></th>
                </tr>
              </thead>
              <tbody id="info-table">
                @foreach ($numbers as $number)
                <tr>
                  <td>
                    {{ $number->description }}
                  </td>
                  <td>
                    {{ $number->datum }}
                  </td>
                  <td class="td-buttons">
                    <div class="btn-group btn-group-xs" role="group" aria-label="...">
                      <button class="btn btn-default btn-xs" data-info-id="{{ $number->id }}" role="button" type="button"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button>
                      <button class="btn btn-default btn-xs" data-info-id="{{ $number->id }}" role="button" type="button"><i class="fa fa-trash-o" aria-hidden="true"></i></button>
                    </div>
                  </td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
          @endif
        </div>
      </div>
    </div>
    <div class="card-footer">
      <div class="row">
      </div>
    </div>
  </div>
</div>
@endsection

@section('js')
<script type="text/javascript" src="{{ URL::asset('js/admin/dashboard/numbers.js') }}"></script>
@endsection
