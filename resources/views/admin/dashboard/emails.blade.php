@extends('layouts.admin')

@section('title')
Admin Dashboard Email Addresses
@endsection

@section('content')
<div class="row">
  <div class="card card-info">
    <div class="card-header blue">
      <div class="card-title">
        <div class="title">Admin Dashboard Email Addresses</div>
      </div>
    </div>
    <div class="card-body">
      <div class="row">
        <div class="col-md-12">
          @if (count($emails) === 0)
          There are no email addresses - <button class="btn btn-default btn-xs" onclick="addInfo()" role="button" type="button">Add email</button>
          @else
          <div class="table-responsive">
            <table class="small table table-condensed table-striped table-no-wrap">
              <thead>
                <tr>
                  <th>Description</th>
                  <th>Number</th>
                  <th class="td-buttons"><button class="btn btn-default btn-xs" onclick="addInfo()" role="button" type="button">Add emails</button></th>
                </tr>
              </thead>
              <tbody id="info-table">
                @foreach ($emails as $email)
                <tr>
                  <td>
                    {{ $email->description }}
                  </td>
                  <td>
                    {{ $email->datum }}
                  </td>
                  <td class="td-buttons">
                    <div class="btn-group btn-group-xs" role="group" aria-label="...">
                      <button class="btn btn-default btn-xs" data-info-id="{{ $email->id }}" role="button" type="button"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button>
                      <button class="btn btn-default btn-xs" data-info-id="{{ $email->id }}" role="button" type="button"><i class="fa fa-trash-o" aria-hidden="true"></i></button>
                    </div>
                  </td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
          @endif
        </div>
      </div>
    </div>
    <div class="card-footer">
      <div class="row">
      </div>
    </div>
  </div>
</div>
@endsection

@section('js')
<script type="text/javascript" src="{{ URL::asset('js/admin/dashboard/emails.js') }}"></script>
@endsection
