@extends('layouts.admin')

@section('title')
Admin Dashboard Links
@endsection

@section('content')
<div class="row">
  <div class="card card-info">
    <div class="card-header blue">
      <div class="card-title">
        <div class="title">Admin Dashboard Links</div>
      </div>
    </div>
    <div class="card-body">
      <div class="row">
        <div class="col-md-12">
          @if (count($links) === 0)
          There are no links - <button class="btn btn-default btn-xs" onclick="addInfo()" role="button" type="button">Add link</button>
          @else
          <div class="table-responsive">
            <table class="small table table-striped table-no-wrap">
              <thead>
                <tr>
                  <th>Links</th>
                  <th>Description</th>
                  <th><button class="btn btn-default btn-xs" onclick="addInfo()" role="button" type="button">Add link</button></th>
                </tr>
              </thead>
              <tbody id="info-table">
                @foreach ($links as $link)
                <tr>
                  <td>
                    {{ $link->datum }}
                  </td>
                  <td>
                    {{ $link->description }}
                  </td>
                  <td>
                    <button class="btn btn-default btn-xs" data-info-id="{{ $link->id }}" role="button" type="button"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button>
                    <button class="btn btn-default btn-xs" data-info-id="{{ $link->id }}" role="button" type="button"><i class="fa fa-trash-o" aria-hidden="true"></i></button>
                  </td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
          @endif
        </div>
      </div>
    </div>
    <div class="card-footer">
      <div class="row">
      </div>
    </div>
  </div>
</div>
@endsection

@section('js')
<script type="text/javascript" src="{{ URL::asset('js/admin/dashboard/links.js') }}"></script>
@endsection
