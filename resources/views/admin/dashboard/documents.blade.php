@extends('layouts.admin')

@section('title')
Admin Dashboard Documents
@endsection

@section('content')
<div class="row">
  <div class="card card-info">
    <div class="card-header blue">
      <div class="card-title">
        <div class="title">Admin Dashboard Documents</div>
      </div>
    </div>
    <div class="card-body">
      <div class="row">
        <div class="col-md-10">
          @if (count($documents) === 0)
          There are no documents - <button class="btn btn-default btn-xs" onclick="addDocument()" role="button" type="button">Add document</button>
          @else
          @include('share.documents-list')
          @endif
        </div>
        <div class="col-md-2">
          <button class="btn btn-default btn-xs" onclick="addDocument()" role="button" type="button">Add Document</button>
        </div>
      </dviv>
    </div>
    <div class="card-footer">
      <div class="row">
      </div>
    </div>
  </div>
</div>
@endsection

@section('js')
<script type="text/javascript" src="{{ URL::asset('js/library/upload.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/admin/dashboard/documents.js') }}"></script>
@endsection
