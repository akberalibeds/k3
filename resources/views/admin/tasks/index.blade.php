@extends('layouts.admin')

@section('title')
Admin Cron Tasks
@endsection

@section('content')

  <div class="col-md-12">
    <div class="card card-info">
      <div class=" card-header blue">
        <div class="card-title">
          <div class="title">Cron Task Settings</div>
        </div>
      </div>
      <div class="card-body">

		<table class="table table-responsive">        
			<thead>	
                <tr><th>Active</th><th>Name</th><th>Description</th><th>Log</th></tr>
             </thead>
             <tbody>
              @foreach($crons as $c)  
                
                <tr><td><input type="checkbox" class="flag" id="{{ $c->id }}" @if($c->value) checked @endif></td><td>{{ $c->name }}</td><td>{{ $c->description }}</td><td>{{ $c->log }}</td></tr>
                
              @endforeach  	 
             </tbody>
        </table>      

      </div>
    </div>
  </div>



<style>
	.th {
		background-color:lightgrey;
		width:10%;
		
	}
	
	td div {
		padding:10px;
	}
	.table {
    table-layout: fixed;
    word-wrap: break-word;
}
</style>
@endsection


@section('js')
<script>
	$('.flag').change(function(e) {
		var data = {}
		data.val = $(this).prop('checked') ? 1 : 0;
		data.id = this.id;
        $.form({
				url:'/admin/tasks/update',
				method: 'POST',
				data: { data },
			  });
    });	
</script>
@endsection