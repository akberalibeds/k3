@extends('layouts.app')

@section('content')
<div class="row">
  <div class="card card-info">
    <div class="card-header blue">
      <div class="card-title">
        <div class="title">Sellers</div>
      </div>
    </div>
    <div class="card-body">
      <div class="row">
        <div class="col-md-6 col-sm-12 col-xs-12">
          @include('share.search-bar', [
            'options' => [
             ['value' => 'name', 'title' => 'Name'],
              ['value' => 'iName', 'title' => 'Invoice Name'],
              ['value' => 'email', 'title' => 'Email'],
              ['value' => 'postcode', 'title' => 'Postcode'],
            ],
            'selectable' => false,
            'selectControls' => false,
          ])
        </div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <div class="table-responsive">
            <table class="small table table-striped table-no-wrap table-hover" id="table-results-container">
              <thead>
                <tr>
                  <th>Name (iName)</th>
                  <th>Email</th>
                  <th>Postcode</th>
                  <th class="text-right">Account Limit (&pound;)</th>
                  <th class="text-center">Force Pay</th>
                </tr>
              </thead>
              <tbody id="result-container">
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
    <div class="card-footer">
      <div class="row">
        <div class="col-md-12 text-center" id="paging-controls-container">
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
@section('js')
<script type="text/javascript" src="{{ URL::asset('js/library/table-results_0.2.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/sellers/index.js') }}"></script>
@endsection
