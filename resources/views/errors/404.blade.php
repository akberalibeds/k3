<!DOCTYPE html>
<html>
  <head>
    <title>Giomani Deisgns - 404</title>
    <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Roboto+Condensed:300,400" rel="stylesheet" type="text/css">
    <style>
      html, body {
        height: 100%;
      }
      body {
        margin: 0;
        padding: 0;
        width: 100%;
        color: #B0BEC5;
        display: table;
        font-weight: 100;
        font-family: 'Roboto Condensed', sans-serif;
      }
      .container {
        text-align: center;
        display: table-cell;
        vertical-align: middle;
      }
      .content {
        text-align: center;
        display: inline-block;
      }
      .link {
        color: inherit;
        font-weight: bold;
        text-decoration: none;
      }
      .title {
        font-size: 72px;
        margin-bottom: 40px;
      }
    </style>
  </head>
  <body>
    <div class="container">
      <div class="content">
        <div class="title">Giomani Designs</div>
        <div class="titie"><script>document.write(window.location.href)</script></div>
        <div class="title">404</div>
        <a class="link" href="javascript:window.history.back()">&lt;- Back</div>
      </div>
    </div>
  </body>
</html>
