@extends('layouts.app')

@section('content')

  <div class="row">
    <div class="col-md-12">
      <div class="card card-info">
        <div class="card-header blue">
        	<div class="card-title">
            <div class="title">Orders</div>
          </div>
        </div>
        <div class="card-body">
          <div class="pull-right col-md-4 searches">
          </div>
          <div class="clearfix"><br><br><br><br></div>
          <div class="table-responsive">


            <table id="orders-table" class="table table-striped table-no-wrap">

              <thead>
                <tr>
                    <th></th>
                    <th>Order</th>
                    <th>Invoice</th>
                    <th>Buyer</th>
                    <th>Customer</th>
                    <th>Postcode</th>
                    <th>Ref</th>
                    <th>Type</th>
                    <th>Status</th>
                    <th>Date</th>
                    <th>Total</th>
                    <th>Owed</th>
                    <th>Done</th>
                </tr>
              </thead>
              <tbody>
              </tbody>
            </table>
          </div>
        </div>
      </div>
      <div class="panel-footer">
        <div class="pageHere"></div>
      </div>
    </div>
  </div>





 <!-- Button trigger modal -->
<button type="button" style="visibility:hidden;" class="btn btn-primary btn-danger danger-modal" data-toggle="modal" data-target="#modalDanger">
    Modal Danger
</button>

<!-- Modal -->
<div class="modal fade modal-danger" id="modalDanger" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Modal Danger</h4>
            </div>
            <div class="modal-body">
                YOU HAVE BEEN LOGGED OUT!!
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" onclick="location.reload();"  data-dismiss="modal">Login</button>
            </div>
        </div>
    </div>
</div>
@endsection


@section('js')


<script>


	@if($admin)
	var list = [['o.id','Order'],['o.iNo','Invoice'],['c.userID','Buyer ID'],['c.dBusinessname','Name'],['c.dPostcode','Postcode'],['o.ouref','Ref'],['o.orderType','Type'],['o.orderStatus','Status'],['o.startDate','Date'],['o.beds','Bed ID'],['direct','direct'],['o.companyName','Company'],['c.email1','Email'],['o.code','Consignment']];
	@else
	var list = [['c.dPostcode','Delivery Postcode'],['o.id','Order'],['c.userID','Buyer ID'],['c.businessName','Invoice Name'],['c.dBusinessName','Delivery Name'],['o.startDate','Date'],['o.deliverBy','Delivery Date'],['c.postcode','Invoice Postcode'],['o.ouref','Ref'],['o.beds','Bed ID'],['c.email1','Email'],['c.mob','Mobile'],['c.tel','Tel'],['o.last_trans_id','Paypal ID'],['o.code','Consignment']];
	@endif


$(window).load(function(e) {

	$('.searches').append(addSearch('',''));
	//getOrders();
	$('#orders-table tbody').html('<tr><td colspan="13" align="center">Search Orders</td></tr>');
	

});



</script>

<script src="{{ URL::asset('js/orders/orders2.js') }}"></script>

@endsection
