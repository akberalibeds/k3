@extends('layouts.app')
@section('title')
Cancelled Orders
@endsection
@section('content')
<div class="row">
  <div class="card card-info">
    <div class="card-header blue">
      <div class="card-title">
        <div class="title">Cancelled Orders</div>
      </div>
    </div>
    <div class="card-body">
      <div class="row">
        <div class="col-md-6">
          @include('share.search-bar', [
            'options' => [
              ['value' => 'cancelled_orders.id', 'title' => 'Order ID'],
              ['value' => 'userID', 'title' => 'Buyer'],
              ['value' => 'deleted_by', 'title' => 'Cancelled by'],
              ['value' => 'delete_time', 'title' => 'Cancel Datetime'],
              ['value' => 'dBusinessName', 'title' => 'Customer'],
              ['value' => 'startDate', 'title' => 'Date'],
              ['value' => 'iNo', 'title' => 'Invoice'],
              ['value' => 'dPostcode', 'title' => 'Postcode'],
              ['value' => 'staffName', 'title' => 'Ref'],
              ['value' => 'supplier_name', 'title' => 'Supplier'],
            ],
            'selectable' => false,
            'selectControls' => false,
          ])
        </div>
      </div>
      <div class="table-responsive" id="table-results-container">
        <table class="small table table-striped table-no-wrap table-hover">
          <thead>
            <tr>
              <th data-sortable="oid">Order</th>
              <th data-sortable="deleted_by">Cancelled By</th>
              <th data-sortable="delete_time">Cancel Datetime</th>
              <th data-sortable="iNo">Invoice</th>
              <th data-sortable="userID">Buyer</th>
              <th data-sortable="supplier_name">Supplier</th>
              <th data-sortable="dBusinessName">Customer</th>
              <th data-sortable="dPostcode">Postcode</th>
              <th>Ref</th>
              <th>Type</th>
              <th>Status</th>
              <th data-sortable="startDate">Date</th>
              <th class="text-right">Total</th>
              <th class="text-right">Paid</th>
              <th class="text-right">Balance</th>
              <th class="text-center">Done</th>
            </tr>
          </thead>
          <tbody id="result-container">
          </tbody>
        </table>
      </div>
    </div>
    <div class="card-footer">
      <div class="row">
        <div class="col-md-12 text-center" id="paging-controls-container">
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
@section('js')
<script type="text/javascript" src="{{ URL::asset('js/library/table-results_0.2.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/orders/cancelled.js') }}"></script>
@endsection
