@extends('layouts.app')
@section('title')
Orders - On Hold
@endsection
@section('content')
<div class="row">
  <div class="card card-info">
    <div class="card-header blue">
      <div class="card-title">
        <div class="title">On Hold</div>
      </div>
    </div>
    <div class="card-body">
      <div class="row">
        <div class="col-md-6 col-xs-12">
          @include('share.search-bar', [
            'options' => [
              ['value' => 'orders.id', 'title' => 'Order ID'],
              ['value' => 'userID', 'title' => 'Buyer'],
              ['value' => 'dBusinessName', 'title' => 'Customer'],
              ['value' => 'iNo', 'title' => 'Invoice Number'],
              ['value' => 'dPostcode', 'title' => 'Postcode'],
              ['value' => 'staffName', 'title' => 'Ref'],
            ],
            'selectable' => false,
          ])
        </div>
      </div>
      <div class="thin-row">
        <div class="col-md-6">
          <small id="result-counts"></small>
        </div>
      </div>
      <div class="table-responsive" id="table-results-container">
        <table class="small table table-striped table-no-wrap table-hover">
          <thead>
            <tr>
              <th data-sortable="oid">Order</th>
              <th data-sortable="iNo">Invoice</th>
              <th data-sortable="userID">Buyer</th>
              <th data-sortable="dBusinessName">Customer</th>
              <th data-sortable="dPostcode">Postcode</th>
              <th data-sortable="staffName">Ref</th>
              <th>Type</th>
              <th>Status</th>
              <th data-sortable="startDate">Date</th>
              <th class="text-right">Total</th>
              <th class="text-right">Paid</th>
              <th class="text-right">Balance</th>
              <th class="text-center">Done</th>
              <th class="text-center">Priority</th>
            </tr>
          </thead>
          <tbody class="table-clickable" id="result-container">
          </tbody>
        </table>
      </div>
    </div>
    <div class="card-footer">
      <div class="row">
        <div class="col-md-12 text-center" id="paging-controls-container">
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
@section('js')
<script type="text/javascript" src="{{ URL::asset('js/library/table-results_0.2.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/orders/on-hold.js') }}"></script>
@endsection
