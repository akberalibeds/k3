@extends('layouts.app')

@section('title')
Orders - No Order
@endsection

@section('content')
<div class="row">
  <div class="col-md-6 col-md-offset-3">

        <div class="panel panel-default">

            <div class="panel-body" align="center">

                <h2><span class="label label-danger">Error</span></h2>
                	<br />
                <h2>Order not found</h2>

            </div>

        </div>

  </div>
</div>
@endsection
