@extends('layouts.app')
@section('title')
Tuffnells
@endsection
@section('content')
<div class="row">
  <div class="card card-info">
    <div class="card-header blue">
      <div class="card-title">
        <div class="title">Tuffnells</div>
      </div>
    </div>
    <div class="card-body">
      <div class="row">
        <div class="col-md-6">
          @include('share.search-bar', [
            'options' => [
              ['value' => 'orders.id', 'title' => 'Order ID'],
              ['value' => 'userID', 'title' => 'Buyer'],
              ['value' => 'startDate', 'title' => 'Date'],
              ['value' => 'dBusinessName', 'title' => 'Name'],
              ['value' => 'postcode-dPostcode', 'title' => 'Postcode'],
            ],
            'selectable' => false,
          ])
        </div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <div class="table-responsive">
            <table class="small table table-striped table-no-wrap table-hover" id="table-results-container">
              <thead>
                <tr>
                  <th data-sortable="oid">Order</th>
                  <th data-sortable="iNo">Invoice</th>
                  <th>Buyer</th>
                  <th data-sortable="supplier_name">Supplier</th>
                  <th data-sortable="dBusinessName">Customer</th>
                  <th data-sortable="postcode">Postcode</th>
                  <th>Ref</th>
                  <th>Type</th>
                  <th>Status</th>
                  <th data-sortable="startstamp">Date</th>
                  <th data-sortable="total" class="text-right">Total</th>
                  <th data-sortable="paid" class="text-right">Paid</th>
                  <th class="text-right">Balance</th>
                  <th class="text-center">Done</th>
                  <th class="text-center">Priority</th>
                </tr>
              </thead>
              <tbody class="table-clickable" id="result-container">
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
    <div class="card-footer">
      <div class="row">
        <div class="col-md-12 text-center" id="paging-controls-container">
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
@section('js')
<script type="text/javascript" src="{{ URL::asset('js/library/table-results_0.2.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/orders/tuffnells.js') }}"></script>
@endsection
