@extends('layouts.app')
@section('title')
Orders - Warehouse Errors
@endsection
@section('content')
<div class="row">
  <div class="card card-info">
    <div class="card-header blue">
      <div class="card-title">
        <div class="title">Warehouse Errors</div>
      </div>
    </div>
    <div class="card-body">
      <div class="row">
      	<div class="pull-right" style="padding-right:10px;">
      		<button onclick="exportCSV();" class="btn btn-default">export csv</button>
      		<button onclick="removeErrors();" class="btn btn-default">Clear</button>
      	</div>
        <div class="col-md-6 col-xs-12">
          @include('share.search-bar', [
            'options' => [
              ['value' => 'orders.id', 'title' => 'Order ID'],
              ['value' => 'deliverBy', 'title' => 'Delivery Date'],
              ['value' => 'dPostcode', 'title' => 'Postcode'],
              ['value' => 'name', 'title' => 'Route'],
            ],
            'selectable' => true,
          ])
        </div>
      </div>
      <div class="thin-row">
        <div class="col-md-12">
          <small id="result-counts"></small>
        </div>
      </div>
      <div class="table-responsive" id="table-results-container">
        <table class="small table table-striped table-no-wrap table-hover">
          <thead>
            <tr>
              <th id="select-all-container"></th>
              <th data-sortable="oid">Order</th>
              <th data-sortable="deliverBy">Delivery</th>
              <th data-sortable="name">Route</th>
              <th data-sortable="dPostcode">Postcode</th>
              <th>Driver</th>
              <th>Loader</th>
              <th>Picker</th>
              <th data-sortable="item">Item</th>
              <th>Note</th>
            </tr>
          </thead>
          <tbody class="table-clickable" id="result-container">
          </tbody>
        </table>
      </div>
    </div>
    <div class="card-footer">
      <div class="row">
        <div class="col-md-12 text-center" id="paging-controls-container">
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
@section('js')
<script type="text/javascript" src="{{ URL::asset('js/library/table-results_0.2.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/orders/warehouse-errors.js') }}"></script>
<script>
	
</script>
@endsection
