@extends('layouts.app')
@section('title')
Orders - Refunds
@endsection
@section('content')
<div class="row">
  <div class="card card-info">
    <div class="card-header blue">
      <div class="card-title">
        <div class="title">Delivery Date Not Selected</div>
      </div>
    </div>
    <div class="card-body">
      <div class="row">
        <div class="col-md-6 col-xs-12">
          @include('share.search-bar', [
            'options' => [
              ['value' => 'orders.id', 'title' => 'Order ID'],
              ['value' => 'companyName', 'title' => 'Company'],
              ['value' => 'orders.paymentType', 'title' => 'Payment'],
              ['value' => 'dPostcode', 'title' => 'Postcode'],
            ],
            'selectable' => true,
            'limits'	 => [10,15,25,50,100]
          ])
        </div>
      </div>
      <div class="table-responsive">
        <table class="small table table-striped table-no-wrap table-hover" id="table-results-container">
          <thead>
            <tr>
              <th id="select-all-container"></th>
              <th data-sortable="id">Order</th>
              <th data-sortable="name">Name</th>
              <th data-sortable="postcode">Postcode</th>
              <th>Date</th>
            </tr>
          </thead>
          <tbody class="table-clickable" id="result-container">
          </tbody>
        </table>
      </div>
    </div>
    <div class="card-footer">
      <div class="row">
        <div class="col-md-12 text-center" id="paging-controls-container">
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
@section('js')
<script type="text/javascript" src="{{ URL::asset('js/library/table-results_0.2.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/orders/unconfirmed.js') }}"></script>
@endsection
