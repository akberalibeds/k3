@extends('layouts.app')
@section('content')
<div class="row">
  <div class="col-md-3">
    <div class="row">
      <div class="col-md-12">
        <div class="panel panel-default">
          <div class="panel-body">
            <h3 class="panel-title">Order : <span class="text-primary pull-right">{{ $order->id }}</span></h3>
            <h3 class="panel-title">Invoice : <span class="text-primary pull-right">{{ $order->iNo }}</span></h3>
            <h3 class="panel-title">Date : <span class="text-primary pull-right">{{ @date('d-M-Y H:i', $order->startStamp) }}</span></h3>
            @if ($order->customer->userID!='')
            <h3 class="panel-title">Buyer : <span class="text-primary pull-right">{{ $order->customer->userID }}</span></h3>
            @endif
            @if ($order->ebayID!='')
            <h3 class="panel-title">eBay ID : <span class="text-primary pull-right ebayLink"><small>{{ $order->ebayID }}</small></span></h3>
            @endif
            @if ($order->beds!='0')
            <h3 class="panel-title">Beds Order ID : <span class="text-primary pull-right bedsLink"><small>{{ $order->beds }}</small></span></h3>
            @endif
            @if ($order->last_trans_id!='0')
            <h3 class="panel-title">Paypal ID : <span class="text-primary pull-right"><small>{{ $order->last_trans_id }}</small></span></h3>
            @endif
            <h3 class="panel-title">Consignment : <span class="text-primary pull-right premdels">{{ $order->code }}</span></h3>
            @if ($order->id_supplier!=0)
            <h3 class="panel-title">Supplier : <span class="text-primary pull-right">{{ $suppliers[$order->id_supplier]['supplier_name'] }}</span></h3>
            @endif
            @if ($order->p!=0)
            <h3 class="panel-title">Paid Delivery : <span class="text-primary pull-right">Customer Paid for delivery</span></h3>
            @endif
            @if ($order->is_split)
            <span class='btn btn-warning btn-block'>Split Order</span>
            @endif
            @if ($order->priority == '1')
            <span class='btn btn-warning btn-block'>Priority Order</span>
            @endif
            <hr />
            <h3 class="panel-title">Status :
              <div class="pull-right">
                <select class="form-control status" data-color="btn-danger">
                  <?php
                  foreach($status as $s){
                    echo ($s->order_status==$order->orderStatus) ? "<option selected='selected' value='$s->order_status'>$s->order_status</option>" : "<option value='$s->order_status'>$s->order_status</option>";
                  }
                  ?>
                </select>
              </div>
              <div class="clearfix"></div>
            </h3>
            <br />
            <h3 class="panel-title">Route :
              <div class="pull-right">
                <select class="form-control delRoute" data-live-search="true" data-color="btn-danger">
                  @foreach ($routes as $route)
                  <option {{ $route->id == $order->id_delivery_route ? 'selected="selected" ' : '' }} value="{{ $route->id }}">{{ $route->name }}</option>
                  @endforeach
                </select>
              </div>
              <div class="clearfix"></div>
            </h3>
            <br />
            <div class="btn-group pull-right">
              <a href="#" class="btn btn-default dropdown-toggle" data-toggle="dropdown">Flags <span class="caret"></span></a>
              <ul class="dropdown-menu">
                <li><div class="col-md-12"><input type="checkbox" <?php echo ($order->defects==1) ? ' checked ' : '' ?> id="defects"  class="update" /> Defects</div></li>
                <li><div class="col-md-12"><input type="checkbox" <?php echo ($order->delissue==1) ? ' checked ' : '' ?> id="delissue"  class="update"></span> Delivery Issue</div></li>
                <li><div class="col-md-12"><input type="checkbox" <?php echo ($order->oos==1) ? ' checked ' : '' ?> id="oos"  class="update"></span> O.O.S</div></li>
                <li class="divider"></li>
                <li><div class="col-md-12"><input type="checkbox" <?php echo ($order->mgmt==1) ? ' checked ' : '' ?> id="mgmt"  class="update"></span> Management</div></li>
              </ul>
            </div>
            <div class="clearfix"></div>
            <hr />
            <h3 class="panel-title">Payment Type: <span class="pull-right">{{ @number_format($order->paymentType, 2) }}</span></h3>
            <h3 class="panel-title">Sub Total: <span class="pull-right">{{ @number_format($order->subTotal, 2) }}</span></h3>
            <h3 class="panel-title">Vat: <span class="pull-right">{{ @number_format($order->VAT, 2) }}</span></h3>
            <h3 class="panel-title"><big>Total:</big> <span class="pull-right"><big><span class="label label-primary">{{ @number_format($order->total, 2) }}</span></big></span></h3>
            <h3 class="panel-title"><big>Paid:</big> <span class="pull-right"><big><span class="label label-success">{{ @number_format($order->paid, 2) }}</span></big></span></h3>
            <div class="clearfix"></div>
            @if ($order->paid < $order->total)
            <span class='btn btn-danger btn-block'> Outstanding: {{ @number_format(($order->total - $order->paid), 2) }}</span>
            @endif
          </div>
        </div>
      </div>
    </div>
    <div class="panel panel-default">
      <div class="panel-body">
        <table class="table table-bordered paymentTable">
          <thead>
            <tr>
            <th>Date</th>
            <th>Amount</th>
            <th>Type</th>
            <th>Staff</th>
            </tr>
          </thead>
          <tbody>
            @foreach($payments as $p)
            <tr>
              <td>{{ @date('d-M-Y',$p->stamp) }}</td>
              <td>{{ @number_format($p->paid, 2) }}</td>
              @if ($p->type == "Card")
              <td>
                <a style="color:black;" rel="tooltip" class="m-b-10" data-toggle="tooltip" data-placement="top" title="Card No: {{ $p->cardNo }} Expiry: {{ $p->exp }} Auth: {{ $p->auth }} ">Card</a>
              </td>
              @else
              <td>{{ $p->type }}</td>
              @endif
              <td>{{ $p->staff }}</td>
            </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
    <div class="panel panel-default">
      <div class="panel-body">
        <div class="col-md-12">
          <button class="btn btn-info btn-block payButton" data-toggle="modal" data-target="#modal-payment"><i class="fa fa-money fa-fw"></i> Add Payment</button>
          <br>
        </div>
        <div class="col-md-12 editOrderDiv">
          <button class="btn btn-info btn-block editOrderBtn"><i class="fa fa-pencil fa-fw"></i> Edit Order</button>
          <br>
        </div>
        <div class="col-md-12">
          <button class="btn btn-info btn-block" data-toggle="modal" data-target="#modal-mail"><i class="fa fa-fw fa-envelope"></i> Send Email</button>
          <br>
        </div>
        <div class="col-md-12">
          <button class="btn btn-info btn-block sendInvoice"><i class="fa fa-fw fa-envelope"></i> Email Invoice</button>
          <br>
        </div>
        <div class="col-md-12">
          <a href="<?php echo ($order->orderType=="retail") ? "/invoice/$order->id" : "/invoice/$order->id" ?>" class="btn btn-info btn-block new-window invoice"> <i class="fa fa-fw fa-file-text"></i>Invoice</a>
          <br>
        </div>
        <div class="col-md-12">
          <button class="btn btn-info btn-block exchange"><i class="fa fa-fw fa-envelop"></i> Exchange</button>
          <br>
        </div>
        <div class="col-md-12">
          <button class="btn btn-info btn-block applyRefund"><i class="fa fa-fw fa-refresh"></i> Refund</button>
          <br>
          </div>
        <div class="col-md-12">
          <button class="btn btn-info btn-block collection" ><i class="fa fa-fw fa-archive"></i> Collection</button>
          <br>
        </div>
        <div class="col-md-12">
          <button class="btn btn-info btn-block missedParts" ><i class="fa fa-fw fa-archive"></i> Missed Part</button>
          <br>
        </div>
        <div class="col-md-12">
          <button class="btn btn-info btn-block" data-toggle="modal" data-target="#modal-discount"><i class="fa fa-fw fa-refresh"></i> Apply Discount</button>
          <br>
        </div>
        <div class="col-md-12">
          <button class="btn btn-info btn-block" data-toggle="modal" data-target="#modal-post"><i class="fa fa-fw fa-archive"></i> Create Post</button>
          <br>
        </div>
        <div class="col-md-12">
          <button class="btn btn-info btn-block resendLink" ><i class="fa fa-fw fa-reply"></i> Resend Delivery Selection</button>
          <br>
        </div>
        @if ($order->processed == 1)
        <div class="col-md-12">
          <button class="btn btn-info btn-block" onclick="unprocessOrder()"> Unprocess Order</button>
          <br>
        </div>
        @endif
        @permission('order_priority')
        @if ($order->priority == '0')
        <div class="col-md-12">
          <button class="btn btn-info btn-block" onclick="makePriorityOrder()"> Make a Priority</button>
          <br>
        </div>
        @endif
        @endpermission
        <div class="col-md-8">
          <select class="form-control supplier">
            <option value="0">None</option>
            @foreach($suppliers as $s)
            <option @if ($order->supplier_id==$s['id']) {{ ' selected="selected" ' }} @endif value="{{ $s['id'] }}">{{ $s['supplier_name'] }}</option>
            @endforeach
          </select>
        </div>
        <div class="col-md-2">
          <button class="btn btn-info changeSupplier">Change</button>
        </div>
      </div>
    </div>
  </div>

  <?php //========================= ?>

  <div class="col-md-6">
    <div class="row">
      <div class="panel panel-default">
        <div class="panel-body">
          <table class="table table-bordered table-responseve">
            <thead>
              <tr>
                <th>Stock</th>
                <th>Company</th>
                <th>Ref</th>
                <th>Type</th>
                <th>Staff</th>
                <th></th>
                <!--th>Warehouse</th>
                <th>Manufacture</th-->
              </tr>
            </thead>
            <tbody>
              <tr>
                <td><span class="label label-{{ $order->stockCount }}">&nbsp;&nbsp;&nbsp;</span></td>
                <td>{{ $order->companyName }}</td>
                <td>{{ $order->ouref }}</td>
                <td>{{ $order->orderType }}</td>
                <td>{{ $order->staffName }}</td>
                <td>
                  <div class="dropdown">
                    <button class="btn btn-default dropdown-toggle" type="button" id="flags-menu-button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                      Flags
                      <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu" id="flags-menu" aria-labelledby="flags-menu-button">
                      <li>
                        <label>
                          &nbsp;<input class="update" id="aftersales" type="checkbox" <?php echo ($order->aftersales==1) ? 'checked' : '' ?>>
                          Aftersales
                        </label>
                      </li>
                      <li>
                        <label>
                          &nbsp;<input class="update" id="wes" type="checkbox" <?php echo ($order->wes==1) ? 'checked' : '' ?>>
                          Wharehouse Error
                        </label>
                      </li>
                      <li>
                        <label>
                          &nbsp;<input class="update" id="mfError" type="checkbox" <?php echo ($order->mfError==1) ? 'checked' : '' ?>>
                          Manufacture Error
                        </label>
                      </li>
                      <li>
                        <label>
                          &nbsp;<input class="update" id="wholeError" type="checkbox" <?php echo ($order->wholeError==1) ? 'checked' : '' ?>>
                          wholesale
                        </label>
                      </li>
                      <li>
                        <label>
                          &nbsp;<input class="update" id="refundBox" type="checkbox" <?php echo ($order->refundBox==1) ? 'checked' : '' ?>>
                          Refund
                        </label>
                      </li>
                      <li>
                        <label>
                          &nbsp;<input class="update" id="feedbackScore" type="checkbox" <?php echo ($order->feedbackScore==1) ? 'checked' : '' ?>>
                          Feedback
                        </label>
                      </li>
                      <li>
                        <label>
                          &nbsp;<input class="update" id="hold" type="checkbox" <?php echo ($order->hold==1) ? 'checked' : '' ?>>
                          Hold
                        </label>
                      </li>
                      <li>
                        <label>
                          &nbsp;<input class="update" id="reopen" type="checkbox" <?php echo ($order->reopen==1) ? 'checked' : '' ?>>
                          Cancelled
                        </label>
                      </li>
                      <li>
                        <label>
                          &nbsp;<input class="update" id="urgent" type="checkbox" <?php echo ($order->urgent==1) ? 'checked' : '' ?>>
                          Urgent
                        </label>
                      </li>
                      <li>
                        <label>
                          &nbsp;<input class="update" id="paypal" type="checkbox" <?php echo ($order->paypal==1) ? 'checked' : '' ?>>
                          Paypal
                        </label>
                      </li>
                      <li>
                        <label>
                          &nbsp;<input class="update" id="retention" type="checkbox" <?php echo ($order->retention==1) ? 'checked' : '' ?>>
                          Retention
                        </label>
                      </li>
                    </ul>
                  </div>
                </td>
              </tr>
            </tbody>
          </table>
          <table class="table table-bordered">
            <thead>
              <tr>
                <th>Deliver By</th>
                <th>Del Date</th>
                <th>Slot</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>
                  <div class="input-group">
                    <input disabled class=" form-control deliverBy" value="{{ $order->deliverBy }}" type="text" placeholder="Select Date" />
                    <input class=" form-control deliverBy2" value="{{ $order->deliverBy }}" type="text" placeholder="Select Date" />
                    <span class="input-group-addon">
                      <a class="editDate"><i class="fa fa-pencil-square-o"></i></a>
                    </span>
                  </div>
                </td>
                <td>{{ $order->endDate }}</td>
                <td>{{ $order->delTime }}</td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
      <div class="panel panel-default">
        <div class="panel-body">
          <div class="list-group">
            <div class="list-group-item info">
              <div class="row">
                <div class="col-md-3">
                  <h4 class="list-group-item-heading">Item Code</h4>
                </div>
                <div class="col-md-4">
                  <h4 class="list-group-item-heading">Item Description</h4>
                </div>
                <div class="col-md-1">
                  <p class="list-group-item-text text-right">Qty</p>
                </div>
                <div class="col-md-2">
                  <p class="list-group-item-text text-right">Price</p>
                </div>
                <div class="col-md-2 text-right">Total</div>
              </div>
            </div>
            @foreach($order->orderItems as $item)
            <div item="<?php echo json_encode($item) ?>" id="{{ $item->id }}" class="list-group-item item">
              <div class="row">
                <div class="col-md-3">
                  <h4>{{ $item->itemCode }}</h4>
                </div>
                <div class="col-md-4">{{ $item->itemDescription }}</div>
                <div class="col-md-1 text-right">{{ $item->Qty }}</div>
                <div class="col-md-2 text-right">{{ @number_format($item->price, 2) }}</div>
                <div class="col-md-2 text-right">{{ @number_format($item->lineTotal, 2) }}</div>
              </div>
              @if ($item->notes != '')
              <div class="row">
                <div class="col-md-11">
                  <h4><span class="label label-primary">{{ $item->notes }}</span></h4>
                </div>
                <div class="clearfix"></div>
              </div>
              @endif
            </div>
            <!-- <input class="form-control input-sm rowNote" placeholder="Note" disabled value="' . $item->notes . '" /> -->
            @endforeach
          </div>
        </div>
      </div>
      <div class="panel panel-default">
        <div class="panel-body">
          <div  style="max-height:400px; overflow-y:scroll;">
            <table class="table table-striped table-bordered">
              <thead>
                <tr>
                  <th>Date</th>
                  <th>Time</th>
                  <th width="60%">Note</th>
                  <th>Staff</th>
                  <th>Driver</th>
                </tr>
              </thead>
              <tbody class="orderNotes">
                <?php
                if(isset($order->orderNotes))  {
                  foreach($order->orderNotes as $note) {
                    echo "<tr id='$note->id'><td>$note->date</td><td>$note->time</td><td>$note->notes</td><td>$note->staff</td><td>";
                    echo '<input type="checkbox" ';
                    echo ($note->driver==1) ? ' checked ' : '';
                    echo ' class="driverN"></span>';
                    echo"</td></tr>";
                  }
                }
                ?>
              </tbody>
            </table>
          </div>
          <div align="center">
            <button class="btn btn-info noteButton" data-backdrop="static" data-keyboard="false"  data-toggle="modal" data-target="#modal-note">Add Note</button>
          </div>
        </div>
      </div>
    </div>
  </div>
  <?php //========================= ?>
  <div class="col-md-3">
    <div class="row">
      <div class="col-md-12">
        <div class="panel panel-default">
          <div class="panel-body">
            <table class="table table-bordered">
              <thead>
                <tr>
                  <th>Invoice Address<span class="pull-right editCustomer"><i class="fa fa-pencil"></i> Edit</span>
                  </th>
                </tr>
              </thead>
            <tbody>
              <tr>
                <td class="invoiceAddress">
                  <?php echo ($order->customer->businessName!="") ? $order->customer->businessName."<br>" : '' ?>
                  <?php echo ($order->customer->number!="")     ? $order->customer->number."<br>" : '' ?>
                  <?php echo ($order->customer->street!="")     ? $order->customer->street."<br>" : '' ?>
                  <?php echo ($order->customer->town!="")     ? $order->customer->town."<br>" : '' ?>
                  <?php echo ($order->customer->postcode!="")   ? $order->customer->postcode."<br>" : '' ?>
                </td>
              </tr>
            </tbody>

            <?php if($order->direct==1) { ?>
            <thead>
              <tr>
                <th>
                  (Direct Delivery) <span class="pull-right"><i class="fa fa-pencil"></i> Edit</span>
                </th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>
                  <?php echo ($order->directDel->name!="")     ? $order->directDel->name."<br>"       : '' ?>
                  <?php echo ($order->directDel->number!="")     ? $order->directDel->number."<br>"       : '' ?>
                  <?php echo ($order->directDel->street!="")     ? $order->directDel->street."<br>"       : '' ?>
                  <?php echo ($order->directDel->town!="")     ? $order->directDel->town."<br>"       : '' ?>
                  <?php echo ($order->directDel->postcode!="")   ? $order->directDel->postcode."<br>"     : '' ?>
                </td>
              </tr>
              </tbody>
              <?php } else { ?>
                <thead>
                  <tr>
                    <th>Delivery Address</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td class="deliveryAddress">
                      <?php echo ($order->customer->dBusinessName!="")  ? $order->customer->dBusinessName."<br>"  : '' ?>
                      <?php echo ($order->customer->dNumber!="")        ? $order->customer->dNumber."<br>"       : '' ?>
                      <?php echo ($order->customer->dStreet!="")       ? $order->customer->dStreet."<br>"       : '' ?>
                      <?php echo ($order->customer->dTown!="")       ? $order->customer->dTown."<br>"       : '' ?>
                      <?php echo ($order->customer->dPostcode!="")     ? $order->customer->dPostcode."<br>"     : '' ?>
                    </td>
                  </tr>
                </tbody>
                <?php } ?>
                <thead>
                  <tr>
                    <th>Contact Details</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td class="contactDetails">
                      <?php echo ($order->customer->tel!="" && $order->customer->tel!=0) ? $order->customer->tel."<br>" : '' ?>
                      <?php echo ($order->customer->mob!="" && $order->customer->mob!=0) ? $order->customer->mob."<br>" : '' ?>
                      <?php echo $order->customer->email1."<br>"  ?>
                      <?php echo ($order->customer->email2!="" && $order->customer->email3!=0) ? $order->customer->email2."<br>" : '' ?>
                      <?php echo ($order->customer->email3!="" && $order->customer->email3!=0) ? $order->customer->email3."<br>" : '' ?>
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
      <div class="panel panel-default">
        <div class="panel-body">
          <button class="btn btn-info genbarcode pull-right">Generate</button>
          <small>
            <table class="table table-striped">
              <thead>
                <tr>
                  <th>Barcode</th>
                  <th>ID</th>
                  <th>Description</th>
                  <th>Loaded</th>
                </tr>
              </thead>
              <tbody>
                <?php
                foreach($codes as $row) {
                  echo "<tr>
                  <td>$row->bar</td>
                  <td>$row->parcelid</td>
                  <td>$row->itemCode</td>";
                  echo "<td>".($row->loaded == '0' ? "No" : "Yes")."</td>";
                  //echo "<td>".($row->delivered == '0' ? "No" : "Yes")."</td>";
                  echo "</tr>";
                }
                ?>
              </tbody>
            </table>
          </small>
        </div>
      </div>
      <div class="panel panel-default">
       <div class="panel-body map">
        </div>
       </div>
       <div class="panel panel-default">
        <div class="panel-body" style="max-height:300px; overflow-x:scroll;">
          <small>
          <table class="table table-striped table-bordered">
            <thead>
              <tr><th>Staff</th>
                <th>Time</th>
              </tr>
            </thead>
            <tbody>
              @foreach($visits as $row)
              <tr>
                <td>{{ $row->name }}</td>
                <td>{{  Time::date($row->time)->get('d-M-Y H:i:s') }}</td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </small>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="modal-note" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel"><strong>Add Note</strong></h4>
      </div>
      <div class="modal-body">
        <div class="form-group">
          <textarea class="form-control note" id="note-textarea" rows="25"></textarea>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger closeC" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-success saveNote" data-dismiss="modal">Save</button>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="modal-payment" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel"><strong>Complete Sale</strong></h4>
      </div>
      <div class="modal-body">
        <div class="form-group col-md-12">
          <label class="control-label">Payment Type</label>
          <select class="form-control paymentType">
            @foreach($paymentType as $type)
              <option value="{{ $type->payment_type }}">{{ $type->payment_type }}</option>
            @endforeach
          </select>
        </div>
        <div class="clearfix"></div>
        <div class="form-group col-md-6">
          <label class="control-label">Total</label>
          <input type="text" disabled="disabled" value="<?php echo $order->total-$order->paid ?>" class="form-control compTotal"  />
        </div>
        <div class="form-group col-md-6">
          <label class="control-label">Owed</label>
          <input type="text" disabled="disabled" value="<?php echo $order->total-$order->paid ?>" class="form-control owed"  />
        </div>
        <div class="clearfix"></div>
        <div class="form-group col-md-12">
          <label class="control-label">Paid</label>
          <input type="text"  value="0" class="form-control input-lg paid"  />
        </div>
        <div class="clearfix"></div>
        <!-- Card Details -->
        <div class="cardDetails">
          <div class="col-md-6 col-md-offset-3">
            <legend>Card Details</legend>
            <div class="form-group col-md-12">
              <label class="control-label">Card No (16 digits)</label>
              <input type="text"  value="" class="form-control input-sm details" row="cardNo" value=""  />
            </div>
            <div class="clearfix"></div>
            <div class="form-group col-md-12">
              <label class="control-label">Expiry</label>
              <input type="text"  value="" class="form-control input-sm details" row="exp" value=""  />
            </div>
            <div class="clearfix"></div>
            <div class="form-group col-md-12">
              <label class="control-label">Auth</label>
              <input type="text"  value="" class="form-control input-sm details" row="auth" value=""  />
            </div>
            <div class="clearfix"></div>
          </div>
          <div class="clearfix"></div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger closeC" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-success addPayment">Complete</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="modal-customer" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel"><strong>Customer</strong></h4>
      </div>
      <div class="modal-body newCustomer">
        <div class="row col-md-6">
          <legend>Invoice Address</legend>
          <div class="form-group col-md-8">
            <label for="name">Name</label>
            <input class="form-control copy" row="businessName" />
          </div>
          <div class="clearfix"></div>
          <div class="form-group col-md-4">
            <label for="number">No</label>
            <input class="form-control copy" row="number" />
          </div>
          <div class="form-group col-md-8">
            <label for="street">Street</label>
            <input class="form-control copy" row="street" />
          </div>
          <div class="form-group col-md-7">
            <label for="town">Town</label>
            <input class="form-control copy" row="town" />
          </div>
          <div class="form-group col-md-5">
            <label for="name">Postcode</label>
            <input class="form-control copy" row="postcode" />
          </div>
        </div>
        <div class="row col-md-6 pull-right">
          <legend>Delivery Address</legend>
          <div class="form-group col-md-8">
            <label for="dBusinessName">Name</label>
            <input class="form-control paste" row="dBusinessName" />
          </div>
          <div class="clearfix"></div>
          <div class="form-group col-md-4">
            <label for="number">No</label>
            <input class="form-control paste" row="dNumber" />
          </div>
          <div class="form-group col-md-8">
            <label for="street">Street</label>
            <input class="form-control paste" row="dStreet" />
          </div>
          <div class="form-group col-md-7">
            <label for="town">Town</label>
            <input class="form-control paste" row="dTown" />
          </div>
          <div class="form-group col-md-5">
            <label for="name">Postcode</label>
            <input class="form-control paste" row="dPostcode" />
          </div>
          <div class="form-group col-md-4">
            <button class="btn btn-info btn-sm sameAdd">Use Invoice Address</button>
          </div>
        </div>
        <div class="clearfix"></div>
          <legend>Contact Details</legend>
            <div class="form-group col-md-4">
              <label for="name">Tel</label>
              <input class="form-control" row="tel" />
            </div>
            <div class="form-group col-md-4">
              <label for="fax">Mob</label>
              <input class="form-control" row="mob" />
            </div>
            <div class="form-group col-md-4">
              <label for="fax">Fax</label>
              <input class="form-control" row="fax" />
            </div>
             <div class="form-group col-md-4">
              <label for="fax">Email 1</label>
              <input class="form-control" row="email1" />
            </div>
             <div class="form-group col-md-4">
              <label for="fax">Email 2</label>
              <input class="form-control" row="email2" />
            </div>
            <div class="form-group col-md-4">
              <label for="fax">Email 3</label>
              <input class="form-control" row="email3" />
            </div>
            <div class="clearfix"></div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default closeNC" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-success saveCustomer" data-dismiss="modal">Save changes</button>
          </div>
      </div>
  </div>
</div>

<div class="modal fade" id="modal-mail" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel"><strong>Send Email</strong></h4>
      </div>
      <div class="modal-body">
        <div class="form-group">
          <label class="control-label">Subject</label>
          <input class="form-control subject"  />
        </div>
        <div class="form-group">
          <textarea style="width:100%;" id="mailBody" rows="25" class="mailBody"></textarea>
        </div>
        <div class="sentInfo"></div>
        <br>
        <br>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger closeMail" data-dismiss="modal">Cancel</button>
        <button type="button" class="btn btn-success sendMail"><i class="fa fa-envelope"></i> Send</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="modal-discount" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel"><strong>Add Discount</strong></h4>
      </div>
      <div class="modal-body spinHere">
        <div class="form-group">
          <input type="text" class="form-control theDiscount">
        </div>
        <br>
        <br>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger closeD" data-dismiss="modal">Cancel</button>
        <button type="button" class="btn btn-success applyDiscount">Apply Discount</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="modal-post" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel"><strong>Creat Post</strong></h4>
      </div>
      <div class="modal-body spinHere">
        <div class="form-group">
          <input type="text" class="form-control missedItem">
        </div>
        <div class="form-group">
          <select class="form-control location">
            <option value="Bilston">Bilston</option>
            <option value="Darlaston">Darlaston</option>
          </select>
        </div>
        <br>
        <br>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger closeD" data-dismiss="modal">Cancel</button>
        <button type="button" class="btn btn-success createPost">Create Post</button>
      </div>
    </div>
  </div>
</div>
<button style="visibility:hidden;" class="btn btn-info custModal" data-toggle="modal" data-target="#modal-customer">Add Note</button>
<button style="visibility:hidden;" class="fake">Fake</button>

@endsection

@section('js')
<script src="//cdn.ckeditor.com/4.5.3/standard/ckeditor.js"></script>
<script src="{{ asset('js/jquery.creditCardValidator.js') }}"></script>
<script src="{{ URL::asset('js/library/typeahead.min.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('js/library/typeahead-kit.js') }}" type="text/javascript"></script>

<script>

var orderID = {{ $order->id }};
var status = '{{ $order->orderStatus }}';
var customer = $.parseJSON('{!! Helper::JSON($order->customer) !!}');
var cid = '{{ $order->customer->id }}';
var validCard=false;

$(window).load(function(e) {

  //$('.status').selectpicker();

  document.title="Order | "+orderID;
  CKEDITOR.replace( 'mailBody' );
  $('.deliverBy2').hide();
  $('.cardDetails').hide();

  $('.editOrderBtn').click(function(e) {
    window.location='/orders/' + orderID + '/edit';
  });

  if(status=='Delivered' || status=='Routed' || status=='Dispatched' || status=='Cancelled'){
    $('.editOrderDiv').hide()
    $('.delRoute').prop('disabled','disabled');
    if(status=='Cancelled')
    {
      $('.status').prop('disabled','disabled');
      $('button').prop('disabled','disabled');
      $('input').prop('disabled','disabled');
      $('.invoice').prop('disabled','disabled');

      // re-enable the flags option
      $('#flags-menu-button').removeAttr('disabled')
      $('#aftersales').removeAttr('disabled')

    }
  }



  $('.pickadate').pickadate({
    format: 'd-mmm-yyyy'
  });

  $('[row="cardNo"]').validateCreditCard(function(result) {
                result.card_type == null ? '-' : result.card_type.name;
                    validCard = (result.valid && result.length_valid && result.luhn_valid);
     });
});


/*
$('.pickadate').datetimepicker({
    format: 'DD-MMM-Y',
    defaultDate: moment('{{ @date("Y-m-d\TH:i:s",strtotime("$order->deliverBy")) }}')
});
*/
   /*
   *
   Duplicate addresses
   *
   */
  $('.sameAdd').click(function(e) {

    var copy = $('.copy');
    var paste = $('.paste');
    for(var i=0; i < copy.length; i++){
      $(paste[i]).val($(copy[i]).val());
    }

    });

  /*
   *
   edit Customer details
   *
   */
  $('body').on('click','.editCustomer',function(e) {

    $('.newCustomer .form-control').each(function(index, element) {
      $(element).val(customer[$(this).attr('row')]);
        });

    $('.custModal').click();
    });



   /*
   *
   save Customer details to database
   *
   */
  $('.saveCustomer').click(function(e) {
    var data = {}

    $('.newCustomer .form-control').each(function(index, element) {
      data[$(this).attr('row')] = ($(this).val()!='') ? $(this).val() : 0;
        });

    data.id=customer.id;
    data = JSON.stringify(data);

    $.form({
      url:"/customer/"+cid+"/update",
      data: 'data='+escape(data),
      success:function(msg){
        addNote('Customer Details Updated');
        getCustomer(cid);
        deliveryData();
      },
      error:function(msg){  /*ERROR*/  }
    });

    });

   /*
   *
   Call to Get Customer Details from Database
   *
   */
   function getCustomer(id){

      $.form({
          url:"/customer/"+id,
          method:'GET',
          success:function(response){
            customer = $.parseJSON(response);
            $('.invoiceAddress').html(customer.businessName+'<br>'+customer.number+'<br>'+customer.street+'<br>'+customer.town+'<br>'+customer.postcode);
            $('.deliveryAddress').html(customer.dBusinessName+'<br>'+customer.dNumber+'<br>'+customer.dStreet+'<br>'+customer.dTown+'<br>'+customer.dPostcode);
            $('.contactDetails').html(((customer.mob!=0) ? customer.mob+'<br>' : '' )+((customer.tel!=0) ? customer.tel+'<br>' : '' )+((customer.email1!=0) ? customer.email1+'<br>' : '' )+((customer.email2!=0) ? customer.email2+'<br>' : '' )+((customer.email3!=0) ? customer.email3+'<br>' : '' ));
          }
      });
   }

/*
 *
 Add OrderNote
 *
 */

$('.saveNote').click(function(e) {
  $(this).html('Saving <i class="fa fa-spinner fa-spin"></i>');
  addNote($('.note').val());

});


/*
 *
 driver Note
 *
 */
$('body').on('click','.driverN',function(e) {

  var val = ($(this).prop('checked')==true) ? '1' : '0';
  var id = $(this).closest('tr').attr('id');

  $.form({
      url: '/notes/'+id+'/update',
      data: 'data='+JSON.stringify({'driver':val}),// "action=driverNotes&c=Orders&val="+val+"&id="+id
  });

});




/*
 *
 aftersales /  warehouse error / wholesale
 *
 */
$('body').on('change','.update',function(e) {
  var val = ($(this).prop('checked') == true) ? '1' : '0';
  var val1 = ($(this).prop('checked') == true) ? 'added to' : 'removed from';
  var field = $(this).attr('id');
  var field1 = (field == 'reopen') ? 'Cancelled' : field;
  var data = {};

  data[field] = val;
  data.id = orderID;
  data = JSON.stringify(data);

  $.form({
    data: { data },
    url:  orderID + '/update',
    success: function( msg ) {
      addNote(val1 + ' ' + field1);
      $('#note-textarea').val(val1 + ' ' + flagsMap.get(field1) + '\n')
      // if(field=="aftersales") {
      $('.noteButton').click();
      // }
      if(field == 'wes' && val == 1) {
        warehouseErrors();
      }
    }
  });
});

var flagsMap = new Map()
flagsMap.set('aftersales', 'Aftersales')
flagsMap.set('wes', 'Warehouse Errors')
flagsMap.set('mfError', 'Manufacture Error')
flagsMap.set('wholeError', 'Wholesale')
flagsMap.set('refundBox', 'Refund')
flagsMap.set('feedbackScore', 'Feedback')
flagsMap.set('hold', 'Hold')
flagsMap.set('reopen', 'Cancelled')
flagsMap.set('urgent', 'Urgent')
flagsMap.set('paypal', 'Paypal')
flagsMap.set('retention', 'Retention')


function warehouseErrors()
{


  var items = '';
  $('.item').each(function(index, element) {
      var item = $.parseJSON($(element).attr('item'));
    items += '<tr><td>' + item.itemCode + '</td><td><input class="warehouseErrors" type="checkbox" ></td></tr>' ;
    });


   bootbox.dialog({
                title: "Select item(s) with issue",
                message: '<div class="row">  ' +
                        '<div class="col-md-12"> ' +
                          '<table class="table table-bordered ">' +
                  items +
                '</table>' +
              '</div>' +
            '</div>',
                buttons: {
          cancel: {
                        label: "cancel",
                        className: "btn-danger",
                    },
                    success: {
                        label: "continue",
                        className: "btn-success",
                        callback: function () {

                warehouseErrorsNote($('.warehouseErrors:checked').closest('tr'));

                        }
                    }
                }
            });

}


function warehouseErrorsNote(items)
{

  var itemCodes = '';
  $.each(items,function(index,element){
    itemCodes+=$(element).find('td').eq(0).html()+",";
  });


  itemCodes= itemCodes.slice(0,-1);
  var data = {};
  data.item = itemCodes;


  bootbox.dialog({
                title: "Note",
                message: '<div class="row">  ' +
                        '<div class="col-md-12"> ' +
                          '<textarea class="errorNote form-control">' +
                '</textarea>' +
              '</div>' +
            '</div>',
                buttons: {
                    success: {
                        label: "continue",
                        className: "btn-success",
                        callback: function () {

              data.note = $('.errorNote').val();

              $.form({
                url: orderID + '/wes',
                data: {data},
                success: function(resp){

                  addNote(data.note);

                }
              });

                        }
                    }
                }
            });

}


/*
 *
Payment Type
 *
 */
$('.paymentType').change(function(event) {

  if($(this).val()=="Card"){
      $('.cardDetails').show();
  }
  else{
      $('.cardDetails').hide();
  }

});



/*
 *
New window popup
 *
 */
$('.new-window').click(function(event) {
    event.preventDefault();
    window.open($(this).attr("href"), "popupWindow", "width=800,height=900,scrollbars=yes");
});


/*
 *
 Add Note function
 *
 */
 function addNote(note){

   $.form({
       url:"/notes/"+orderID+"/addNote",
      data:"note="+encodeURIComponent(note),
      success:function(response){
          //$('.closeC').click();
          $('.saveNote').html('Save');
          var row = $.parseJSON(response);
          var check = (row.driver==1) ? "<input type='checkbox' class='driverN' checked />" : "<input type='checkbox' class='driverN' />";
          var tr = '<tr id="'+row.id+'"><td>'+row.date+'</td>'+'<td>'+row.time+'</td>'+'<td>'+row.notes+'</td>'+'<td>'+row.staff+'</td>'+'<td>'+check+'</td></tr>';
          $('.orderNotes').prepend(tr);
      }
  });
 }


/*
 *
Deliver By
 *
 */
$('.deliverBy').on('change', function (ev) {
  var date= this.value;

  var data = {};
  data.deliverBy=date;
  data = JSON.stringify(data);
    $.form({
        url: orderID + '/update',
        data: { data },
        success: function(response){
              notify("Delivery date changed <i class='fa fa-check-circle'></i>","success");
              addNote("Delivery date change to "+date);
        }
    });

});

/*
 *
Change Status
 *
 */

var blocked = (status == 'Delivered' || status == 'Routed' || status == 'Dispatched') ? true : false;

$('body').on('click', '.status', function (event) {
  console.log('clicked');
});

$('.status').change(function(event) {
  var val = $(this).val();
  var stock=0;

  if ((status == 'Dispatched' || status == 'Delivered') && val == 'Allocated') {
    if (confirm("Re-Allocate items back in to stock")) {
      stock = 1;
    }
  }

  var data = {};

  data.old = status;
  data.new = val;
  data.id = orderID;
  data.stock = stock;
  data = JSON.stringify(data);

  if (blocked == true) {
    setTimeout(function() { $('.fake').click(); } , 100);
    bootbox.prompt({
      title: "Enter Password",
      inputType: "password",
      callback: function(result) {
        if (result === null) {

        } else {
          if (result == 'giomani') {
            //blocked=false;
            blocked = (status=='Delivered' || status=='Routed' || status=='Dispatched') ? true : false;

            $.form({
              url: "/orders/" + orderID + "/changestatus",
              data: { data },
              success: function(response) {
                notify("Order Status Changed <i class='fa fa-check-circle'></i>", "success");
                addNote("Order status changed to "+val);
                status=val;
                blocked = (status=='Delivered' || status=='Routed' || status=='Dispatched') ? true : false;
                if (val != 'Delivered' && val != 'Dispatched' && val != 'Routed'){
                  $('.delRoute').prop('disabled',false);
                } else {
                  $('.delRoute').prop('disabled','disabled');
                }

                if (status == 'Delivered') {
                  $('.editOrderDiv').hide()
                } else {
                  $('.editOrderDiv').show()
                }
              },
            });
          } else {
            $('.status').val(status);
            bootbox.alert("<h3>Wrong Password</h3>");
          }
        }
      }
    })
  } else {
    blocked = (status=='Delivered' || status=='Routed' || status=='Dispatched') ? true : false;

    $.form({
      url: "/orders/" + orderID + "/changestatus",
      data: { data },
      success: function(response) {
        notify("Order Status Changed <i class='fa fa-check-circle'></i>","success");
        addNote("Order status changed to "+val);
        status=val;
        blocked = (status=='Delivered' || status=='Routed' || status=='Dispatched') ? true : false;
        if(val!='Delivered' && val!='Dispatched' && val!='Routed'){
         $('.delRoute').prop('disabled',false);
        } else {
          $('.delRoute').prop('disabled','disabled');
        }
        if(status=='Delivered'){
          $('.editOrderDiv').hide()
        } else {
          $('.editOrderDiv').show()
        }
      },
    });
  }
});


 /*
  *
  Delivery Route
  *
  */
  $('.delRoute').change(function(event) {
    var routeId = this.value;
    var route = $( ".delRoute option:selected" ).text();

    data = {};
    data.delRoute       = route;
    data.id_delivery_route   = routeId;
    data = JSON.stringify(data);

    $.form({
        url: "/orders/" + orderID + "/route",
      data: { data },
      success: function(resp){
             addNote("Delivery route changed to "+route)
              }
    });

  });


  /*
   *
   Paid
   *
   */
  $('body').on('blur','.paid',function(e) {
    if($(this).val()=='') {
          $(this).val('0.00');
          $('.owed').val($('.compTotal').val());
    }
  });

  $('body').on('keyup','.paid',function(e) {

    var total = parseFloat($('.compTotal').val());
    var paid = parseFloat($(this).val());
    var owed = total-paid;
    $('.owed').val(owed.toFixed(2));

    });



  /*
   *
   Payment
   *
   */
  $('body').on('click','.addPayment',function(e) {


    if($('.paymentType').val()=='0'){
      alert('Select Payment Type')
      return false;
    }


    var fals=0;
    if($('.cardDetails').is(":visible")){

        $('.cardDetails .form-control').each(function(index, element) {
                  if($(this).val()==''){ alert('Complete Card Details!!'); fals++; return false;  }
                });

        if(fals){
          return false;
        }

        if(!validCard){
          alert('Card Details not valid!');
          fals++;
          return false;
        }



        if(parseFloat($('.paid').val())==0){
          fals++;
          alert('No Payment Amount Added!!');
          return false;
        }

        if(fals){
          return false;
        }

    }

    if(fals==0){
    var data = {}
    data.paid = $('.paid').val();
    data.pt = $('.paymentType').val();
    data.cardNo='';
    data.exp='';
    data.auth='';

    if (data.pt=="Card"){
    var d = $('.cardDetails .details');
    d.each(function(index, element) {
      data[$(element).attr('row')]=$(element).val();
        });
    }
    data = JSON.stringify(data);



    $.form({
        url: "/orders/" + orderID + "/payment",
        data: { data },
        success: function(resp){

            $('.closeC').click();
            var row = $.parseJSON(resp);

            var tr = '<tr id="'+row.id+'"><td>'+row.stamp+'</td>'+'<td>'+row.paid+'</td>'+'<td>'+row.type+'</td>'+'<td>'+row.staff+'</tr>';
            $('.paymentTable').prepend(tr);

            addNote("Added " + $('.paymentType').val() + " payment of " + $('.paid').val());

        }

     });//form


    }

  });



  $('.sendMail').click(function(e) {
        var btn=this;
    var btnText=this;
    clicked(btn);

    var data     = {};
    data.account   = '{{ $order->mailer_account }}';
    data.body      = CKEDITOR.instances.mailBody.getData();
    data.subject   = $('.subject').val();
    data.toEmail   = customer.email1;
    data.toName     = customer.businessName;
    data       = JSON.stringify(data);

    $.form({
        url: "/mailer/raw",
        data: { data },
        success: function(response){
          $(btn).html(btnText);
          $('.closeMail').click();
          if(response=='Sent'){
            addNote('Sent Email');
            notify('Email Sent',"success");
          }
          else{
            notify(response,"danger");
          }
        }
    });
    });



  /*

  Send Invoice

  */

  $('.sendInvoice').click(function(e) {
      var btnText = $(this).html();
      var btn = this;
      clicked(this);
      var data     = {};
      data.account   = '{{ $order->mailer_account }}';
      data.id       = orderID;
      data.toEmail   = customer.email1;
      data.toName     = customer.businessName;
      data       = JSON.stringify(data);

      $.form({
          url: "/mailer/invoice",
          data: { data },
          success: function(resp){
            $(this).html(btnText);
            notify(resp,"success");
            addNote('Invoice emailed to Customer')
            $(btn).html(btnText);
          }
      });

    });




/*
 *
   refund order
 *
 */
$('.applyRefund').click(function(e) {

    var r=prompt("Enter password to refund order");

    if(r=="00786"){

    $.form({
      method: 'GET',
      url: "/orders/"+orderID+'/refund',
      function(msg){
        location.reload();
      }
    });

    }
    else{
    alert('Wrong Password');
    }

    });



  /*
  *
  Discount
  *
  */

  $('.applyDiscount').click(function(e) {

    $.form({
      url: "/orders/"+orderID+"/discount",
      data: 'discount='+$('.theDiscount').val(),
      success: function(msg){
        location.reload();
      },
    });

  });

  /**
   * post
   */
  $('.createPost').click(function(e) {


    $.form({
        url:'/orders/'+orderID+'/post',
        data: "item="+encodeURIComponent($('.missedItem').val())+"&loc="+$('.location').val(),
        success: function(msg){
          window.location='/orders/'+msg;
        }
    });
  });


  /*
   * Missed Parts
   */
  $('.missedParts').click(function(e) {



  var items = '<tr class="missedParts"><td>Missed Part</td><td><input class="form-control note" value="" placeholder="Description here" /></td></tr>' ;



   bootbox.dialog({
                title: "Missed Parts",
                message: '<div class="row">  ' +
                        '<div class="col-md-12"> ' +
                          '<table class="table table-bordered ">' +
                  items +
                '</table>' +
              '</div>' +
            '</div>',
                buttons: {
          cancel: {
                        label: "cancel",
                        className: "btn-danger",
                    },
                    success: {
                        label: "continue",
                        className: "btn-success",
                        callback: function () {
                var data = {};
                $('.missedParts input').each(function(index, element) {
                                      data[index] = $(element).val();
                                });



                $.form({
                    url:'/orders/'+orderID+'/missedParts',
                    data: { data },
                    success: function(msg){
                      window.location='/orders/'+msg;
                    }
                });

                        }
                    }
                }
            });


});



$('.premdels').click(function(e) {
    var code = $(this).html();
    window.open("http://premierdeliveries.co.uk/?pc=<?php echo ($order->direct==1) ? $order->directDel->postcode : $order->customer->dPostcode ?>&id="+code,'_blank');

    });


$('.ebayLink').click(function(e) {
        e.stopPropagation();
    var url = "http://k2b-bulk.ebay.co.uk/ws/eBayISAPI.dll?MfcISAPICommand=SalesRecordConsole&currentpage=SCSold&pageNumber=1&urlStack=5508|Period_Last90Days|searchField_BuyerName|pageNumber_1|currentpage_SCSold|searchValues_rayyan+farrani|&searchField=BuyerName&searchValues={{ $order->customer->businessName }}&StoreCategory=-4&Status=All&Period=Last90Days&searchSubmit=Search&goToPage=";
    window.open(url);
    });


$('.editDate').click(function(e) {


      var halt = true;

      bootbox.prompt({
      title: "Enter Password",
      inputType: "password",
      callback: function(result) {
      if (result === null) {

      } else {

         if(result =='transport'){
           console.log(false)
           halt = false;
           if(halt){
            console.log(halt);
            return;
          }

           if(!$('.deliverBy').is(':hidden')){
          $(this).html('<i class="fa fa-calendar"></i>');
          $('.deliverBy').hide();
          $('.deliverBy2').show();
           }
           else{
          $(this).html('<i class="fa fa-pencil-square-o"></i>');
          $('.deliverBy').show();
          $('.deliverBy2').hide();
           }
         }
         else{
           bootbox.alert("<h3>Wrong Password</h3>");
         }

      }
      }
    });




});


$('.deliverBy2').blur(function(e) {
    $('.deliverBy').val($(this).val());
  $('.deliverBy').change();
});




function deliveryData(){

  var order={};
    order.lat='';
    order.lng='';

  $.form({
    url:'/orders/'+orderID+'/update',
    data:'data='+JSON.stringify(order),
    success: function(resp){

    }
  });

}



$('.changeSupplier').click(function(e) {

        var supplier = $('.supplier').val();
        var oid = {};
        var data = {};
        oid[0]={}
        oid[0].id=orderID;
        data.supplier=supplier;
        data.ids=oid;
        $.form({
          url: orderID + "/moveSupplier",
          data: { data },
          success: function(response){
            addNote('Changed Supplier to '+ $('.supplier option:selected').text());
            //location.reload();
          }
        });

});



$('.resendLink').click(function(e) {
  var btn = this;
  $(btn).clickLoader('start');
  $.form({
    url: "/orders/" + orderID + "/resendLink",
    method: 'GET',
    success: function(e){
      $(btn).clickLoader('stop');
      notify('Delivery selection message resent <i class="fa fa-check fa-lg"></i>','success');
    }
  });
});




function clicked(item){
    $(item).html("<i class='fa fa-spinner fa-spin fa-lg'></i>");
}



$.fn.clickLoader= function(task){

  var loader = "<i class='fa fa-spinner fa-spin fa-lg'></i>";

  if(task=='start') {
    $(this).attr('btnText',$(this).html());
      $(this).html(loader);
    }
  else{
    $(this).html($(this).attr('btnText'));
  }


};


$('.list-group-item').click(function(e) {
      if($(this).hasClass('active')){
      $(this).removeClass('active');
    }
    else{
      $(this).addClass('active');
    }
});




$('.collection').click(function(e) {

  if($('.list-group-item.active').length==0)
  {
    alert('Select Items to Collect');
    return false;
  }

  var items = '';
  $('.list-group-item.active').each(function(index, element) {
      var item = $.parseJSON($(element).attr('item'));
    items += '<tr class="toCollect"><td>Collection Item</td><td><input class="form-control note" value="' + item.itemCode + '" /></td></tr>' ;
    });


   bootbox.dialog({
                title: "Exchange Items",
                message: '<div class="row">  ' +
                        '<div class="col-md-12"> ' +
                          '<table class="table table-bordered ">' +
                  items +
                '</table>' +
              '</div>' +
            '</div>',
                buttons: {
          cancel: {
                        label: "cancel",
                        className: "btn-danger",
                    },
                    success: {
                        label: "continue",
                        className: "btn-success",
                        callback: function () {
                doExchange($('.toCollect'),{});
                        }
                    }
                }
            });


});


$('.exchange').click(function(e) {

  if($('.list-group-item.active').length==0)
  {
    alert('Select Items to Exchange');
    return false;
  }

  var items = '';
  $('.list-group-item.active').each(function(index, element) {
      var item = $.parseJSON($(element).attr('item'));
    items += '<tr class="toCollect"><td>Collection Item</td><td><input class="form-control note" value="' + item.itemCode + '" /></td></tr>' ;
    });


   bootbox.dialog({
                title: "Exchange Items",
                message: '<div class="row">  ' +
                        '<div class="col-md-12"> ' +
                          '<table class="table table-bordered ">' +
                  items +
                '</table>' +
              '</div>' +
            '</div>',
                buttons: {
          cancel: {
                        label: "cancel",
                        className: "btn-danger",
                    },
                    success: {
                        label: "continue",
                        className: "btn-success",
                        callback: function () {

                exchange_item($('.toCollect'));
                        }
                    }
                }
            });


});





function exchange_item(toCollect){


   bootbox.dialog({
                title: "Exchange Items",
                message: '<div class="row">  ' +
                        '<div class="col-md-12"> ' +
                          '<div class="input-group input-group-typeahead">' +
                  '<input class="form-control" id="items" placeholder="Search Stock" />' +
                  '<span class="tt-badge" id="typeahead-spinner"></span>' +
                '</div> ' +
                '<table class="table table-bordered new-items-here">' +
                  '' +
                '</table>' +
              '</div>' +
            '</div>',
                buttons: {
          cancel: {
                        label: "cancel",
                        className: "btn-danger",
                    },
                    success: {
                        label: "continue",
                        className: "btn-success",
                        callback: function () {
                doExchange(toCollect,$('.new-item'));


            }
                  }
              }
            });




  $('#items').typeaheadKit({
      display: 'itemCode',
      name: 'itemCode',
      //objectIdProperty: 'id',
      queryUrl: '/stock/search/item-code/%QUERY',
      resultsLimit: 15,
      rowClickFn: function (item) {
        item.discount=0;
        $('.new-items-here').append('<tr class="new-item" id="'+item.id+'"><td>' + item.itemCode + '</td><td><input class="form-control note" placeholder /></td><td><input class="form-control qty" value="1"/></td><td><i class="fa fa-trash-o" onClick="$(this).closest(\'tr\').remove()"></i></td></tr>');
       // $('.order_items').append(getRow(item));
       // updateOrder();
        $('#items').typeahead('val','');
      },
      rowElementFn: function (object) {



      }
  });



}

/*
 *  Exchange Items
 */

function doExchange(toCollect,toSend)
{
  var collect = {};
  $.each(toCollect,function(i,v){

    collect[i] = {};
    collect[i].itemCode = $(v).find('td').eq(0).html()
    collect[i].note = $(v).find('.note').val()

  });


  var send = {};
  $.each(toSend,function(i,v){

    send[i] = {};
    send[i].itemCode = $(v).find('td').eq(0).html()
    send[i].note = $(v).find('.note').val()
    send[i].qty = $(v).find('.qty').val()

  });




  var items = {};
  items.collect = collect;
  items.send = send;

  //$.each(toCollect,function(index, element) {
      //items.push([toCollect,toSend]);
    //});

      $.form({
        url:'/orders/'+orderID+'/exchange',
        data: { items },
        success: function(){
            location.reload();
        }
      });

}



$('.genbarcode').click(function(e) {
     var btn=this;
    var btnText=this;
    clicked(btn);
    $.form({
    url: '/orders/'+orderID+'/genbarcode',
    method: 'GET',
    success: function(resp){
      location.reload();
    }
  });
});

function makePriorityOrder () {
  console.log('makePriorityOrder: ' + orderID)
  $.form({
    method: 'PUT',
    url: '/orders/' + orderID + '/make-priority',
    success: function (response) {
    },
    error: function (response) {
      console.log('failure:' + response)
    }
  })
}
function unprocessOrder () {
  console.log('unprocessOrder: ' + orderID)
  $.form({
    method: 'PUT',
    url: '/orders/' + orderID + '/un-process',
    success: function (response) {
    },
    error: function (response) {
      console.log('failure:' + response)
    }
  })
}

$('#modal-note').on('shown.bs.modal', function () {
  $('#note-textarea').val('')
  $('#note-textarea').focus()
})

</script>




<?php

 function getRow($item)
    {

        $str = '<div id="' . $item->id . '" class="list-group-item item" style=" background-color:#F5F5F5;"><div class="row"><div class="col-md-4"><h4 class="list-group-item-heading">' . $item->itemCode . '</h4></div><div class="col-md-4"><p class="list-group-item-text">' . $item->itemDescription. '</p></div><div class="col-md-1">' . $item->Qty . '</div><div class="col-md-2">' . $item->price . '</div><div class="col-md-1"><p class="list-group-item-text total">' . $item['lineTotal'] . '</p>' . '</div> ' . '</div>';

        if ($item->notes != "") {
            $str .= '<div class="row"><div  class="col-md-11"><input class="form-control input-sm rowNote" placeholder="Note" value="' . $item->notes . '" /></div><div class="clearfix"></div></div>';
        }

        $str .= '</div>';

        return $str;
    }

?>

@endsection
