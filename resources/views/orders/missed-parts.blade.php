@extends('layouts.app')
@section('title')
Orders - Delivery Issue
@endsection
@section('content')
<div class="row">
  <div class="card card-info">
    <div class="card-header blue">
      <div class="card-title">
        <div class="title">Missed Parts</div>
      </div>
    </div>
    <div class="card-body">
    <div class="row">
          <div class="col-md-12 text-right">
            <div class="btn-group" role="group" aria-label="utils">
              <button class="btn btn-default" onclick="oos()" type="button">CSV</button>
            </div>
          </div>
        </div>
      <div class="row">
        <div class="col-md-6 col-xs-12">
          @include('share.search-bar', [
            'options' => [
              ['value' => 'orders.id', 'title' => 'Order ID'],
              ['value' => 'dPostcode', 'title' => 'Postcode'],
              ['value' => 'startDate', 'title' => 'Order Date'],
              ['value' => 'deliverBy', 'title' => 'Delivery Date'],
            ],
            'selectable' => false,
            'selectControls' => false,
            'limits' => [10,25,50,100]
          ])
        </div>
      </div>
      <div class="table-responsive" id="table-results-container">
        <table class="small table table-striped table-no-wrap table-hover">
          <thead>
            <tr>
              <th data-sortable="oid">Order</th>
              <th class="text-center">Order Date</th>
              <th class="text-center">Delivery Date</th>
              <th data-sortable="name">Customer</th>
              <th data-sortable="dPostcode">Postcode</th>
              <th data-sortable="dPostcode">Items</th>
            </tr>
          </thead>
          <tbody class="table-clickable" id="result-container">
          </tbody>
        </table>
      </div>
    </div>
    <div class="card-footer">
      <div class="row">
        <div class="col-md-12 text-center" id="paging-controls-container">
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
@section('js')
<script type="text/javascript" src="{{ URL::asset('js/library/table-results_0.2.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/orders/missed-parts.js') }}"></script>
@endsection
