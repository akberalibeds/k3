@extends('layouts.app')
@section('title')
Orders - Post
@endsection
@section('content')
<div class="row">
  <div class="card card-info">
    <div class="card-header blue">
      <div class="card-title">
        <div class="title">Post</div>
      </div>
    </div>
    <div class="card-body">
      <div class="row">
        <div class="col-md-6">
          @include('share.search-bar', [
            'options' => [
              ['value' => 'orders.id', 'title' => 'Order ID'],
              ['value' => 'startDate', 'title' => 'Date'],
              ['value' => 'dPostcode', 'title' => 'Postcode'],
              ['value' => 'name', 'title' => 'Route'],
              ['value' => 'postLoc', 'title' => 'Location'],
            ],
            'selectable' => true,
            'selectControls' => false,
          ])
        </div>
        @permission('post_admin')
        <div class="col-md-6">
          <div class="btn-group" role="group">
            <button class="btn btn-default" onclick="removeFromPost()" role="button">Remove from post</button>
            <button class="btn btn-default" onclick="printLabels()" role="button">Print Labels</button>
            <button class="btn btn-default" onclick="printManifest()" role="button">Print Manifest</button>
          </div>
        </div>
        @endpermission
      </div>
      <div class="row">
        <div class="col-md-12">
          <div class="table-responsive">
            <table class="small table table-striped table-no-wrap table-hover" id="table-results-container">
              <thead>
                <tr>
                  <th id="select-all-container"></th>
                  <th data-sortable="oid">Order</th>
                  <th data-sortable="startDate">Date</th>
                  <th data-sortable="oid">Route</th>
                  <th data-sortable="postcode">Postcode</th>
                  <th data-sortable="postLoc">Location</th>
                  <th class="text-center">Priority</th>
                </tr>
              </thead>
              <tbody class="table-clickable" id="result-container">
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
    <div class="card-footer">
      <div class="row">
        <div class="col-md-12 text-center" id="paging-controls-container">
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
@section('js')
<script type="text/javascript" src="{{ URL::asset('js/library/table-results_0.2.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/orders/post.js') }}"></script>
@endsection
