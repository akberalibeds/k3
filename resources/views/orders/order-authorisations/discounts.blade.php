@extends('layouts.app')
@section('content')

<div class="row">
    <div class="col-md-12">
        <h2>Discounted orders</h2>

        <input id="dateFrom" type="date" value="{{ (new \DateTime($dateFrom))->format('Y-m-d') }}" >
       &mdash;
        <input id="dateTo" type="date" value="{{ (new \DateTime($dateTo))->format('Y-m-d') }}" >

        <button class="btn btn-primary" onclick="changeDate()" >Change date</button>
        
        <a href="/orders/authorisations/discounts?csv=1&dateFrom={{ (new \DateTime($dateFrom))->format('Y-m-d') }}&dateTo={{ (new \DateTime($dateTo))->format('Y-m-d') }}" class="btn btn-primary">Export CSV</a>
        
        <table class="table table-bordered">
            <thead>
            <tr>
                <th>Original Order_ID</th>
                <th>Order_ID</th>
                <th>Timestamp</th>
                <th>Staff name</th>
                <th>Requested by</th>
                <th>Authorised by</th>
                <th>Customer name</th>
                <th>Customer postcode</th>
                <th>Company name</th>
                <th>Reason for discount</th>
                <th>Total discount</th>
                <th>Item Code</th>
            </tr>
            </thead>
            <tbody>
            @foreach($discounts as $discount)
                 <tr>
                    <td>
                        <a href="/orders/{{ $discount->order->parent_order_id  }}" target="_blank">
                            <u>{{ $discount->order->parent_order_id }}</u>
                        </a>
                    </td>
                    <td>
                        <a href="/orders/{{ $discount->order_id }}" target="_blank">
                            <u>{{ $discount->order_id }}</u>
                        </a>
                    </td>
                    <td>{{ $discount->created_at }}</td>
                    <td>{{ $discount->staff->email }}</td>
                    <td>{{ $discount->requestedBy->email }}</td>
                    <td>{{ $discount->authorisedBy->email }}</td>
                    <td>{{ $discount->order->customer->businessName }}</td>
                    <td>{{ $discount->order->customer->postcode }}</td>
                    <td>{{ $discount->order->companyName}}</td>
                    <td>{{ $discount->reason }}</td>
                    <td>{{ -$discount->order->total }}</td>
                    <td>
                        @foreach($discount->order->parentOrder->items as $item)

                            <a href="/stock/{{ $item->stock->id }}/show" target="_blank"> 
                                <u>{{ $item->stock->itemDescription }}</u>
                            </a> ,

                        @endforeach
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</div>

<script>

      function changeDate() {
        var dateFrom = document.getElementById("dateFrom").value;
        var dateTo = document.getElementById("dateTo").value;

        if(!dateFrom || !dateTo) {
               alert('Please select a date!');
               
               return;
           }

           window.location.href = '/orders/authorisations/discounts?dateFrom=' + dateFrom + '&dateTo=' + dateTo;
        }
</script>

@endsection