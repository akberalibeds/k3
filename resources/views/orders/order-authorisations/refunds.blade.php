@extends('layouts.app')
@section('content')

<div class="row">
    <div class="col-md-12">
        <h2>Refunded orders</h2>

        <input id="dateFrom" type="date" value="{{ (new \DateTime($dateFrom))->format('Y-m-d') }}" >
       &mdash;
        <input id="dateTo" type="date" value="{{ (new \DateTime($dateTo))->format('Y-m-d') }}" >

        <button class="btn btn-primary" onclick="changeDate()" >Change date</button>

        <a href="/orders/authorisations/refunds?csv=1&dateFrom={{ (new \DateTime($dateFrom))->format('Y-m-d') }}&dateTo={{ (new \DateTime($dateTo))->format('Y-m-d') }}" class="btn btn-primary">Export CSV</a>

        <table class="table table-bordered">
            <thead>
            <tr>
                <th>Original Order_ID</th>
                <th>Order_ID</th>
                <th>Timestamp</th>
                <th>Staff name</th>
                <th>Requested by</th>
                <th>Authorised by</th>
                <th>Customer name</th>
                <th>Customer postcode</th>
                <th>Company name</th>
                <th>Reason for refund</th>
                <th>Total refund</th>
                <th>Item Code</th>
            </tr>
            </thead>
            <tbody>
            @foreach($refunds as $refund)
                 <tr>
                    <td>
                        <a href="/orders/{{ $refund->order->parent_order_id }}" target="_blank">
                            <u>{{ $refund->order->parent_order_id }}</u>
                        </a>
                    </td>
                    <td>
                        <a href="/orders/{{ $refund->order_id  }}" target="_blank">
                            <u>{{ $refund->order_id }}</u>
                        </a>
                    </td>
                    <td>{{ $refund->created_at }}</td>
                    <td>{{ $refund->staff->email }}</td>
                    <td>{{ $refund->requestedBy->email }}</td>
                    <td>{{ $refund->authorisedBy->email }}</td>
                    <td>{{ $refund->order->customer->businessName }}</td>
                    <td>{{ $refund->order->customer->postcode }}</td>
                    <td>{{ $refund->order->companyName }}</td>
                    <td>{{ $refund->reason }}</td>
                    <td>{{ -$refund->order->total }}</td>
                    <td>
                        @foreach($refund->order->parentOrder->items as $item)

                            <a href="/stock/{{ $item->stock->id }}/show" target="_blank"> 
                                <u>{{ $item->stock->itemDescription }}</u>
                            </a> ,

                        @endforeach
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</div>


<script>

    function changeDate() {
        var dateFrom = document.getElementById("dateFrom").value;
        var dateTo = document.getElementById("dateTo").value;

        if(!dateFrom || !dateTo) {
            alert('Please select a date!');
            
            return;
        }

        window.location.href = '/orders/authorisations/refunds?dateFrom=' + dateFrom + '&dateTo=' + dateTo;
    }
</script>

@endsection