@extends('layouts.app')
@section('content')

<div class="row">
    <div class="col-md-12">
        <h2>Duplicate orders</h2>
        <p>A list of orders that seem to be duplicated.</p>     

        {{ $duplicates->links() }}       

        <table class="table table-bordered">
            <thead>
            <tr>
                <th>Order_id</th>
                <th>Beds ID</th>
                <th>Company name</th>
                <!-- <th>Postcode</th> -->
                <th>Ref</th>
                <th>Type</th>
                <th>Status</th>
                <th>Date</th>
                <th>Total</th>
            </tr>
            </thead>
            <tbody>
            @foreach($duplicates as $duplicate)
                 @if($duplicate->groupnumber %2 == 1)
                  <tr style="background-color:#eee">
                @else
                 <tr style="background-color:#fff">
                @endif
                    <td>
                        <a href="/orders/{{ $duplicate->id}}"> {{ $duplicate->id}} </a>
                    </td>
                    <td>{{ $duplicate->beds}}</td>
                    <td>{{ $duplicate->companyName}}</td>
                    <!-- <td>{{ $duplicate->postLoc }}</td> -->
                    <td>{{ $duplicate->ouref }}</td>
                    <td>{{ $duplicate->orderType }}</td>
                    <td>{{ $duplicate->orderStatus }}</td>
                    <td>{{ $duplicate->startDate }}</td>
                    <td>{{ $duplicate->total }}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</div>

@endsection