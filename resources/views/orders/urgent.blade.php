@extends('layouts.app')
@section('title')
Orders - Urgent
@endsection
@section('content')
<div class="row">
  <div class="card card-info">
    <div class="card-header blue">
      <div class="card-title">
        <div class="title">Urgent</div>
      </div>
    </div>
    <div class="card-body">
      <div class="row">
        <div class="col-md-6 col-xs-12">
          @include('share.search-bar', [
            'options' => [
              ['value' => 'orders.id', 'title' => 'Order ID'],
              ['value' => 'dBusinessName-businessName', 'title' => 'Customer'],
              ['value' => 'startDate', 'title' => 'Date'],
              ['value' => 'iNo', 'title' => 'Invoice'],
              ['value' => 'dPostcode-postcode', 'title' => 'Postcode'],
              ['value' => 'StaffName', 'title' => 'Ref'],
            ],
            'selectable' => false,
            'selectControls' => false,
          ])
        </div>
      </div>
      <div class="table-responsive" id="table-results-container">
        <table class="small table table-striped table-no-wrap table-hover">
          <thead>
            <tr>
              <th data-sortable="oid">Order</th>
              <th data-sortable="iNo">Invoice</th>
              <th data-sortable="userID">Buyer</th>
              <th data-sortable="dBusinessName">Customer</th>
              <th data-sortable="dPostcode">Postcode</th>
              <th data-sortable="StaffName">Ref</th>
              <th data-sortable="orderType">Type</th>
              <th data-sortable="orderStatus">Status</th>
              <th data-sortable="startDate">Date</th>
              <th class="text-right">Paid</th>
              <th class="text-right">Total</th>
              <th class="text-right">Balance</th>
              <th class="text-center">Done</th>
              <th class="text-center">Priority</th>
            </tr>
          </thead>
          <tbody class="table-clickable" id="result-container">
          </tbody>
        </table>
      </div>
    </div>
    <div class="card-footer">
      <div class="row">
        <div class="col-md-12 text-center" id="paging-controls-container">
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
@section('js')
<script type="text/javascript" src="{{ URL::asset('js/library/table-results_0.2.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/orders/urgent.js') }}"></script>
@endsection
