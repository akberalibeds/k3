@extends('layouts.app')
@section('title')
Orders - Retention
@endsection
@section('content')
<div class="row">
  <div class="card card-info">
    <div class="card-header blue">
      <div class="card-title">
        <div class="title">Retention</div>
      </div>
    </div>
    <div class="card-body">
      <div class="row">
        <div class="col-md-6 col-xs-12">
          @include('share.search-bar', [
            'options' => [
              ['value' => 'orders.id', 'title' => 'Order ID'],
              ['value' => 'dBusinessName', 'title' => 'Customer'],
              ['value' => 'itemCode', 'title' => 'Items'],
              ['value' => 'dPostcode', 'title' => 'Postcode'],
            ],
            'selectable' => false,
          ])
        </div>
      </div>
      <div class="table-responsive">
        <table class="small table table-striped table-no-wrap table-hover" id="table-results-container">
          <thead>
            <tr>
              <th data-sortable="oid">Order</th>
              <th class="text-center">Priority</th>
              <th data-sortable="dBusinessName">Name</th>
              <th data-sortable="dPostcode">Postcode</th>
              <th data-sortable="itemCode">Items</th>
            </tr>
          </thead>
          <tbody class="table-clickable" id="result-container">
          </tbody>
        </table>
      </div>
    </div>
    <div class="card-footer">
      <div class="row">
        <div class="col-md-12 text-center" id="paging-controls-container">
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
@section('js')
<script type="text/javascript" src="{{ URL::asset('js/library/table-results_0.2.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/orders/retention.js') }}"></script>
@endsection
