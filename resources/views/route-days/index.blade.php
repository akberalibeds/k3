@extends('layouts.app')
@section('title')
Route Days
@endsection
@section('content')
<div class="row">
  <div class="card card-info">
    <div class="card-header blue">
      <div class="card-title">
        <div class="title">Route Days</div>
      </div>
    </div>
    <div class="card-body">
      <div class="row">
        <div class="col-md-6 col-sm-12 col-xs-12">
          @include('share.search-bar', [
            'options' => [
              ['value' => 'postcode', 'title' => 'Postcode'],
              ['value' => 'name', 'title' => 'Name'],
            ],
            'selectable' => false,
            'selectControls' => false,
          ])
        </div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <div class="table-responsive">
            <table class="small table table-condensed table-striped table-hover table-no-wrap" id="table-results-container">
              <thead>
                <th>Route</th>
                <th>Postcodes</th>
                <th class="text-right">Max</th>
                <th class="text-right">Vans</th>
                <th class="text-center">Monday</th>
                <th class="text-center">Tuesday</th>
                <th class="text-center">Wednesday</th>
                <th class="text-center">Thursday</th>
                <th class="text-center">Friday</th>
                <th class="text-center">Saturday</th>
                <th class="text-center">Sunday</th>
              </thead>
              <tbody id="result-container">
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
    <div class="card-footer">
      <div class="row">
        <div class="col-md-12 text-center" id="paging-controls-container">
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
@section('js')
<script type="text/javascript" src="{{ URL::asset('js/library/table-results_0.2.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/route-days/index.js') }}"></script>
@endsection
