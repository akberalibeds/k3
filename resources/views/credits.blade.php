@extends('layouts.app')
@section('title')
Credits
@endsection
@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="card card-info">
      <div class=" card-header blue">
        <div class="row">
          <div class="card-title">
            <div class="title">
              Credits
            </div>
          </div>
        </div>
      </div>
      <div class="card-body">
        <p>Written by (in random order):</p>
        <ul>
        <script>
        function shuffle(a){for(var r,f,i=a.length;0!==i;)f=Math.floor(Math.random()*i),i-=1,r=a[i],a[i]=a[f],a[f]=r;return a}for(var a=shuffle(["jason.fleet@googlemail.com","Nadeem Rabbi"]),i=0,j=a.length;j>i;i++)document.write("<li>"+a[i]+"</li>");
        </script>
        </ul>
      </div>
      <div class="card-footer">
      </div>
    </div>
  </div>
</div>
@endsection
