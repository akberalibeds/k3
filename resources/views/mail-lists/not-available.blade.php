@extends('layouts.app')

@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="card card-info">
      <div class=" card-header blue">
        <div class="card-title">
          <div class="title">Mail-Lists</div>
        </div>
      </div>
      <div class="card-body">
        <p><strong>Mail-Lists</strong> is currently unavailable.  Please be patient while the developers are <del>booted</del> re-booting.</p>
      </div>
      <div class="card-footer">
      </div>
    </div>
  </div>
</div>
@endsection
