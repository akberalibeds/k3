@extends('layouts.app')
@section('title')
Mail Lists
@endsection
@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="card card-info">
      <div class=" card-header blue">
        <div class="card-title">
          <div class="title">Mail Lists</div>
        </div>
      </div>
      <div class="card-body">
        <div class="thin-row">
          <div class="col-md-12 text-right">
            <a class="btn btn-default" href="{{ route('mail-list::index') }}">Add Mail List</a>
          </div>
        </div>
        <div class="table-responsive">
          <table class="small table table-condensed table-striped">
            <thead>
              <tr>
                <th>Title</th>
                <th class="col-md-4">Message</th>
                <th>Create by</th>
                <th>Created at</th>
                <th>Expires</th>
              </tr>
            </thead>
            <tbody id="bulletins-table">
            </body>
          </table>
        </div>
      </div>
      <div class="card-footer">
      </div>
    </div>
  </div>
</div>
@endsection

@section('js')
@endsection
