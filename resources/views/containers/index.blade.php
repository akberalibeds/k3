@extends('layouts.app')
@section('title')
Stock
@endsection
@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="card card-info">
    <div class="pull-right"><button onclick="showAddDialog();" class="btn btn-success"><i class="fa fa-plus"></i> Container</button>&nbsp;&nbsp;&nbsp;</div>
  
      <div class=" card-header blue">
        <div class="row">
          <div class="card-title">
            <div class="title">Containers</div>
          </div>
        </div>
      </div>
      <div class="card-body">
        
       <form action=""> 
            <div class="row">
                <div class="form-group col-md-1 text-right">
                		<label class="control-label">Search</label>
                	</div>
                <div class="form-group col-md-2">
                		<input class="form-control" name="val">
                </div>
                <div class="form-group col-md-1">
                		<select class="form-control" name="field">
                			<option value="containerID">Container ID</option>
                			<option value="loaded">Load Date</option>
                			<option value="eta">ETA Date</option>
                			<option value="items">Items</option>
                		</select>
                </div>
                		<div class="form-group col-md-1">
                		<button class="btn btn-success">Search</button>
                </div>
                <div class="clearfix"></div>
            </div>
       </form>
       
        
        <div class="table-responsive">
          <table class="table table-striped table-no-wrap table-hover datatable" >
            <thead>
              <tr>
                <th>Container ID</th>
                <th>Loaded</th>
                <th>ETA</th>
                <th>Items</th>
                <th>Received</th>
                <th class="text-right"></th>
              </tr>
            </thead>
            <tbody>
            @foreach($containers as $container)
            		<tr id="{{ $container->id }}">
            			<td>{{ $container->containerID }}</td>
            			<td>{{ $container->loaded }}</td>
					<td>{{ $container->eta }}</td>
					<td data-json='{!! $container->items !!}'> 
						@foreach(json_decode($container->items) as $item)
							@foreach($item as $key => $val)
								<span class="{{ $key }}">{{ $val }}</span> 
							@endforeach
							<br>
						@endforeach
					</td>
					<td> @if($container->rxd)  <i class="fa fa-check text-success"></i> @else <button onclick="rxd({{ $container->id }});" class="btn btn-success">Mark as Recieved</button> @endif </td>
					<td class="text-right">
							<button class="btn btn-info" onclick="edit($(this).closest('tr'))"><i class="fa fa-pencil"></i> edit</button>
							<button class="btn btn-danger" onclick="removeTr({{ $container->id }});"><i class="fa fa-trash"></i> Remove</button>
					</td>
            		</tr>
            	@endforeach	
            </tbody>
          </table>
        </div>
      </div>
      <div class="card-footer">
        <div class="row">
          <div class="col-md-12 text-center">
          	{{ $containers->links() }}
          </div>
        </div>
      </div>
    
  </div>
</div>
@endsection

@section('js')
<script>
var rows = {};
@foreach($containers as $container)
	rows[{{ $container->id }}] = [];
    @foreach(json_decode($container->items) as $item)
        var a ={};
        @foreach($item as $key => $val)
        		a.{{$key}} = '{{ $val }}';
        @endforeach
        rows[{{ $container->id }}].push(a);
    @endforeach
@endforeach



function edit(row){
        	
	var row = $(row);
	var id = $(row).attr('id');
	showAddDialog(id);

	$('input[name="containerID"]').val(row.find('td').eq(0).html());
	$('input[name="loaded"]').val(row.find('td').eq(1).html());
	$('input[name="eta"]').val(row.find('td').eq(2).html());
	
	var items = rows[id];//row.find('td').eq(3).attr('data-json');

	//items = $.parseJSON(items);
	$('.items').html('');
	$.each(items,function(){
		$('.items').append('<tr><td><input name="sku" class="form-control" value="'+this.sku+'" placeholder="SKU"/></td> <td><input value="'+this.colour+'" name="colour" class="form-control" placeholder="Colour"/></td> <td size="25%">'+sizeSelect(this.size)+'</td> <td><input name="qty" value="'+this.qty+'" class="form-control" placeholder="Quantity"/></td><td><i onclick="removeRow(this)" class="fa fa-trash text-danger"></i></td></tr>');
	});
	
}

function showAddDialog(id){
	bootbox.dialog({
		title: 'Add Container',
		message: '<label class="control-label">Container ID</label><input class="form-control" name="containerID" /><br>'+
				 '<label class="control-label">Date Loaded</label><input class="form-control" name="loaded" /><br>'+
				 '<label class="control-label">Arrival Date</label><input class="form-control" name="eta" /><br>'+
				 '<label class="control-label">Items</label><table class="table items"></table><button class="btn btn-success" onclick="addRow();"><i class="fa fa-plus-circle"></i> item</button>',
		buttons: {
				Cancel: function(){

				},
				Save: function(){
					var containerId = $('input[name="containerID"]').val();
					var loaded = $('input[name="loaded"]').val();
					var eta = $('input[name="eta"]').val();
					var post = "containerID="+containerId+"&loaded="+loaded+"&eta="+eta;
					
					$('.table.items tr').each(function(i,row){
						post+="&items["+i+"][sku]="+$(row).find('input[name="sku"]').val();
						post+="&items["+i+"][colour]="+$(row).find('input[name="colour"]').val();
						post+="&items["+i+"][size]="+$(row).find('select[name="size"]').val();
						post+="&items["+i+"][qty]="+$(row).find('input[name="qty"]').val();
					});

					if(id){
							post+='&id='+id;
						}

					
				$.post("/containers/add",encodeURI(post+"&_token={{ csrf_token() }}"),function(response){
						location.reload();
				});
					
				}
		}
	});

	addRow();
}

function sizeSelect(option){
	var html = '<select style="width:100%;" name="size" class="form-control">';
	html+=(option=="3FT") ? "<option selected value='3FT'>3FT</option>" : "<option value='3FT'>3FT</option>";
	html+=(option=="4FT") ? "<option selected value='4FT'>4FT</option>" : "<option value='4FT'>4FT</option>";
	html+=(option=="4FT6") ? "<option selected value='4FT6'>4FT6</option>" : "<option value='4FT6'>4FT6</option>";
	html+=(option=="5FT") ? "<option selected value='5FT'>5FT</option>" : "<option value='5FT'>5FT</option>";
	html+=(option=="6FT") ? "<option selected value='6FT'>6FT</option>" : "<option value='6FT'>6FT</option>";
	html+='</select>';
	return html;
}

function addRow(){
	
	$('.items').append('<tr><td><input name="sku" class="form-control" placeholder="SKU"/></td> <td><input name="colour" class="form-control" placeholder="Colour"/></td> <td>'+sizeSelect()+'</td> <td><input name="qty" type="number" class="form-control" placeholder="Quantity"/></td><td><i onclick="removeRow(this)" class="fa fa-trash text-danger"></i></td></tr>');
}
function removeRow(item){
	$(item).closest('tr').remove();
}

function rxd(id){
	bootbox.confirm("Confirm Received!", function(result){ 

		if(result){
			$.get("/containers/rxd/"+id,function(response){
				location.reload();
			});
		}

	});
}

function removeTr(id){

	bootbox.confirm("Confirm delete!", function(result){ 

		if(result){
			$.get("/containers/remove/"+id,function(response){
				location.reload();
			});
		}

	});
	
	
}

</script>
@endsection
