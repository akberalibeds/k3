@extends('layouts.app')

@section('title')
Reports
@endsection

@section('content')
<div class="row">
  <div class="card card-info">
    <div class="card-header blue">
      <div class="card-title">
        <div class="title">Reports</div>
      </div>
    </div>
    <div class="card-body">
      <div class="row">
        @tilelink(["/reports/items", "Items"])
        @tilelink(["/reports/orders", "Orders"])
      </div>
    </div>
  </div>
</div>

@endsection

@section('js')
<script type="text/javascript" src="{{ URL::asset('js/reports/index.js') }}"></script>
@endsection
