@extends('layouts.app')

@section('title')
Reports
@endsection

@section('content')
<div class="row">
  <div class="card card-info">
    <div class="card-header blue">
      <div class="card-title">
        <div class="title">Reports - Orders</div>
      </div>
    </div>
    <div class="card-body">
      <div class="row">
        <div class="col-md-12">
          <strong>Last Update:</strong> {{ $lastUpdated }}
          <hr>
        </div>
      </div>
      <div class="row" id="reports-ui">
        <div class="col-md-2 col-sm-4 col-xs-6">
          <div id="companies">
            <strong>Companies</strong><br>
            <button class="btn btn-default btn-xs" data-state="all" onclick="allCompanies(this)" type="button">all</button>
            <br>
            @foreach ($companies as $company)
            <label class="checkbox-inline">
              <input type="checkbox" value="{{ strtolower($company->company_name) }}">&nbsp;{{ $company->company_name }}
            </label>
            <br>
            @endforeach
            <label class="checkbox-inline">
              <input type="checkbox" value="other">&nbsp;Other
            </label>
            <br>
          </div>
        </div>

        <div class="col-md-2 col-sm-4 col-xs-6">
          <div id="period">
            <strong>Period</strong>&nbsp;<span class="glyphicon glyphicon-question-sign" data-toggle="tooltip" data-placement="right" title="Data will be to nearest, inclusive of the date"></span><br>
            <label class="radio-inline">
              <input checked name="period" type="radio" value="daily">&nbsp;Daily
            </label>
            <br>
            <label class="radio-inline">
              <input name="period" type="radio" value="weekly">&nbsp;Weekly &nbsp;<span class="glyphicon glyphicon-question-sign" data-toggle="tooltip" data-placement="right" title="week is Monday to Sunday"></span><br>
            </label>
            <br>
            <label class="radio-inline">
              <input name="period" type="radio" value="monthly">&nbsp;Monthly
            </label>
            <br>
            <label class="radio-inline">
              <input name="period" type="radio" value="quarterly">&nbsp;Quarterly
            </label>
            <br>
            <label class="radio-inline">
              <input name="period" type="radio" value="yearly">&nbsp;Yearly
            </label>
            <br>
          </div>
        </div>

        <div class="col-md-2 col-sm-4 col-xs-6">
          <div id="dates">
            <div class="form-group">
              <label for="date-from">From</label>&nbsp;<span class="glyphicon glyphicon-question-sign" data-toggle="tooltip" data-placement="right" title="dates are inclisive"></span>
              <input class="form-control" id="date-from" placeholder="Date from" type="datetime">
            </div>
            <div class="form-group">
              <label for="date-to">To</label>&nbsp;<span class="glyphicon glyphicon-question-sign" data-toggle="tooltip" data-placement="right" title="dates are inclisive"></span>
              <input class="form-control" id="date-to" placeholder="Date to" type="datetime">
            </div>
          </div>
        </div>

        <div class="col-md-2 col-sm-4 col-xs-6">
          <div id="display">
            <strong>Display</strong><br>
            <label class="radio-inline">
              <input checked name="chart-type" type="radio" value="chart">&nbsp;Chart
            </label>
            <br>
            <label class="radio-inline">
              <input name="chart-type" type="radio" value="table">&nbsp;Table
            </label>
            <br>
            <hr>
          </div>
          <div>
            <button class="btn btn-default btn-sm" onclick="draw()" type="button">Draw Data</button>
          </div>
        </div>

        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="messages" id="messages">
          </div>
        </div>

      </div>
    </div>
  </div>
</div>

<div class="row" id="progress-bar">
  <progress class="progress progress-striped progress-info" value="0" max="100">0%</progress>
</div>

<div class="container-fluid">
  <div class="row" id="dashboard_div">
    <div id="control_div"></div>
    <div id="table_div"></div>
  </div>
</div>
@endsection

@section('js')
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript" src="{{ URL::asset('js/reports/ui.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/reports/orders.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/reports/charts.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/reports/tables.js') }}"></script>
@endsection
