@extends('layouts.app')
@section('title')
Due Report
@endsection
@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="card card-info">
      <div class=" card-header blue">
        <div class="card-title">
          <div class="title">Due Report</div>
        </div>
      </div>
      <div class="card-body">
        <div class="row" id="upload-container">
          <div class="col-md-12">
            <div class="file-drop-area" id="upload-drop" data-upload-identifier="excel"></div>
            <div id="upload-messages"><p></p></div>
            <div class="progress">
              <div class="progress-bar" id="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width:0%"></div>
            </div>
            <div class="text-center pull-right">
              <button class="btn btn-primary" onclick="uploadFile(event)">Read</button>
            </div>
          </div>
        </div>
        <div class="row" id="csv-import-container">
          <div class="col-md-6">
            <p><strong>Column Headings</strong></p>
          </div>
          <div class="col-md-6 text-right">
            <a class="btn btn-default" id="reset-button" onclick="csvViewer.download()">download</a>
            <button class="btn btn-default" id="reset-button" onclick="csvViewer.resetView()">Reset</button>
            <button class="btn btn-default" id="import-button" onclick="csvViewer.sendCsv()">Import</button>
          </div>
        </div>
        <div class="row">
           <div class="col-md-12" id="headings-container"></div>
        </div>
        <div class="row">
          <div class="col-md-12">
            <div class="table-responsive">
              <table class="small table table-bordered table-striped table-no-wrap table-hover" id="csv-view-table">
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
@push('html_stash')
<form id="download-container">
  <input name="data">
</form>
@endpush
@section('js')
<script type="text/javascript" src="{{ URL::asset('js/library/upload.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/library/csv-viewer.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/due-report/index.js') }}"></script>
@endsection
