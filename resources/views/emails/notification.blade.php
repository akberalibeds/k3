<!DOCTYPE html>
<html style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6em; margin: 0; padding: 0;">
<head>
<meta name="viewport" content="width=device-width">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title></title>
</head>

<body>
<div>
     Hi,<br><p>Please Click the link below and you will be redirected to our webpage where
     you will be required to input the following information to choose a delivery date.<br>
     <br> <span style='color:red;'>
     <b>Please Note your order will not be Dispatched until you have confirmed a delivery date
     </b></span>:</p> 
    <ul>
        <li>Your Delivery Postcode to login</li>
        <li>Choose <b>ONE</b> of the following options: 
        <br>
        <ol>
            <li>Choose a Delivery date at your address</li>
            <li>Deliver to a neighbour</li>
            <li>Deliver to a Safe Place</li>
            </li>
        </ol>
    </ul>
<br>
<a href="http://premierdeliveries.co.uk?id={{$code}}">http://premierdeliveries.co.uk?id={{$code}}</a>
<br><br>
Many Thanks<br> Premier Deliveries Team


</body>
</html>
