<!DOCTYPE html>
<html style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6em; margin: 0; padding: 0;">
<head>
<meta name="viewport" content="width=device-width">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Beds.co.uk</title>
</head>

<p>Hi,&nbsp;</p>

<p>&nbsp;</p>

<p>Thank you for placing an order with Beds.co.uk,&nbsp;</p>

<p>&nbsp;</p>

<p>We have now processed your order and delivery is usually estimated to take place within 7-10 working days. Our courier does not provide online tracking services, however they will call you to arrange a date for delivery. If the courier is unable to get in touch with you they will assign a delivery date and request that you confirm this via email. Please ensure a mobile number is provided also the contact details you have provided are up to date as incorrect contact details will cause delays to your order.</p>

<p>If you live in Aberdeenshire, Inverness-shire, Devon or Cornwall please allow a few more days for delivery. Deliveries to these areas are once weekly so it&rsquo;s possible your order can miss cut-off for the next visit to the area</p>

<p>&nbsp;</p>

<p>Deliveries are all-day appointments and it&#39;s extremely unlikely that we will be able to narrow this down to an AM or PM slot.</p>

<p>&nbsp;</p>

<p>While almost all deliveries are made within the estimates provided on the order please be advised that these are just estimates and not a delivery deadline, as in some cases although very rare, delays are possible due to vehicle breakdowns, items being damaged in transit or adverse weather conditions.&nbsp;</p>

<p>&nbsp;</p>

<p><strong>PLEASE NOTE!</strong></p>

<p>To cancel deliveries you must contact us a day prior to delivery, rearrangements will take a further 3 - 5 wokring days, deliveries cancelled on the day of dispatch will incur a <strong>&pound;29.00</strong> fee.</p>

<p>If delivery is missed a calling card will be left providing you with instructions on how to rearrange. Missed deliveries will incur a redelivery charge of <strong>&pound;29.00</strong> which must be cleared prior to the delivery being rescheduled.</p>

<p>Please be sure to inspect the goods upon receipt, if they have arrived damaged or you&rsquo;re unhappy with them for any reason please reject delivery and send them back with the courier as it may be a few days before another vehicle is passing your area for collection. We also recommend not disposing of your old bed/mattress until you&rsquo;ve received the new ones.</p>

<p>&nbsp;</p>

<p>If you&rsquo;ve purchased a 4ft (Small Double) mattress or bed, it&rsquo;s very common for the packaging to be labelled 4ft6 as well as 3ft and 2ft6, please be sure to measure the width of the item in these cases before contacting us.</p>

<p>&nbsp;</p>

<p>The delivery drivers aren&#39;t associated with Beds.co.uk and are therfore unlikely to have any product knowledge of the items you have purchased from ourselves. The drivers are insured to make delivery to your front door, any further is solely at the drivers&rsquo; discretion, we cannot legally ask them to take products upstairs, or help assemble them.</p>

<p>&nbsp;</p>

<p>The returns period on your order is 14 days from the date of delivery, within this period we will authorise return requests for any reason and provide a returns address. Upon receipt of the returned item a full refund can be issued. Return costs incurred from cancellations are not covered by the seller. Should you find that the item is faulty or damaged please contact us with images of the problem as evidence is critical for a free collection service, in most cases we can have a replacement arrive within 2 - 5 working days.&nbsp;</p>

<p>&nbsp;</p>

<p><strong>For, delivery queries&nbsp;</strong></p>

<p>&nbsp;</p>

<p>CONTACT: 0121 568 7126&nbsp;</p>

<p>EMAIL: sales@beds.co.uk</p>

<p>&nbsp;</p>

<p>Kind regards<br />
Beds.co.uk</p>


</body>
</html>
