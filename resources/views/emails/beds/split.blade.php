<!DOCTYPE html>
<html style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6em; margin: 0; padding: 0;">
<head>
<meta name="viewport" content="width=device-width">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Beds.co.uk</title>
</head>
<p>Hi,</p>

<p>&nbsp;Thank you for placing an order with beds.co.uk &lt;<a href="http://beds.co.uk">http://beds.co.uk</a>&gt;</p>

<p>&nbsp;It appears from your recent purchase that &lt;http://beds.co.uk/&gt; there are<br />
&nbsp;products on order with varied delivery estimates meaning your goods will<br />
&nbsp;arrive via different couriers. We would like to minimise the number of times<br />
&nbsp;you may have to wait in for the delivery of your goods or avoid any delivery<br />
&nbsp;complications. Therefore in rare circumstances we advise customers to allow<br />
&nbsp;up to 7 - 10 working days for your full consignment to be delivered.</p>

<p>&nbsp;Leather Beds<br />
&nbsp;You will receive a link via email or text message from our delivery partner<br />
&nbsp;Premier Deliveries soon after the order is placed, this will offer you the<br />
&nbsp;service to select a delivery date of your choice. Once you have confirmed a<br />
&nbsp;delivery date Premier Deliveries will again contact you a day prior to<br />
&nbsp;dispatch with a time slot.</p>

<p>&nbsp;Divan Beds<br />
&nbsp;Divan beds can take between 7 - 10 Working days for delivery as the product<br />
&nbsp;is manufactured upon order, the item will be dispatched using a private<br />
&nbsp;courier service. Once your order has been processed you will receive an<br />
&nbsp;acknowledgment email. The suppliers courier will contact you a day or two<br />
&nbsp;prior to dispatch informing you of a time slot as well as any delivery<br />
&nbsp;updates.</p>

<p>&nbsp;Metal and Wooden Beds<br />
&nbsp;On average metal or wooden beds are delivered within 2 - 4 working days<br />
&nbsp;subject to post code and stock availability. You will receive a link via<br />
&nbsp;email or text message soon after the order is placed from Premier Deliveries<br />
&nbsp;to select a delivery date of your choice. Once you have confirmed a delivery<br />
&nbsp;date Premier Deliveries will again contact you a day prior to dispatch with<br />
&nbsp;a time slot.</p>

<p>&nbsp;Fabric Beds<br />
&nbsp;Our fabric beds are manufactured upon order therefore we advise customers<br />
&nbsp;allowing up to 5 - 7 working days for delivery. Goods are only dispatched<br />
&nbsp;with private couriers to help minimise any delivery complications.<br />
&nbsp;Beds.co.uk &lt;http://beds.co.uk/&gt; will contact you to confirm a delivery date<br />
&nbsp;from the options available, you will again receive an email or text message<br />
&nbsp;a day prior to dispatch confirming a time slot.</p>

<p>&nbsp;Mattress - Pocket spring, Pillow top and open coil<br />
&nbsp;Pocket spring mattress may take between 5 - 7 Working days for delivery form<br />
&nbsp;the date processed. Our suppliers will contact you once your order has been<br />
&nbsp;processed, within 48 hours of purchase, providing you with a delivery<br />
&nbsp;update.</p>

<p>&nbsp;Mattress - Memory foam<br />
&nbsp;You will receive a link via email or text message from our delivery partner<br />
&nbsp;Premier Deliveries soon after the order is placed, this will offer you the<br />
&nbsp;service to select a delivery date of your choice. Once you have confirmed a<br />
&nbsp;delivery date Premier Deliveries will again contact you a day prior to<br />
&nbsp;dispatch with a time slot. . Please note deliveries for 6FT memory foam<br />
&nbsp;mattress may take between 2 - 4 working days for delivery.</p>

<p>&nbsp;Bunk beds<br />
&nbsp;Bunk beds have an average delivery time scale of 2 - 4 working days. Soon<br />
&nbsp;after your order has been processed an email from Premier Deliveries will<br />
&nbsp;give you the option to choose a delivery date from the dates available.<br />
&nbsp;Premier Deliveries will again contact you a day prior to dispatch with a<br />
&nbsp;time slot.</p>

<p>&nbsp;Rattan Furniture<br />
&nbsp;You will receive a link via email or text message from our delivery partner<br />
&nbsp;Premier Deliveries soon after the order is placed, this will offer you the<br />
&nbsp;service to select a delivery date of your choice. Once you have confirmed a<br />
&nbsp;delivery date Premier Deliveries will again contact you a day prior to<br />
&nbsp;dispatch with a time slot.</p>

</body>
</html>
