<!DOCTYPE html>
<html style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6em; margin: 0; padding: 0;">
<head>
<meta name="viewport" content="width=device-width">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title></title>
</head>

<body>


	
    
    
<p>Dear {{ $c_name }},</p>

<p>Congratulations! your order is now complete.</p>
<br>
<p>We will be in touch with you as soon as we have a delivery date for your
order.</p>
<br>
<p>If you do need to get in touch with us in the meantime, you can email us
or call us on 0121 568 8878/ 0121 568 7126</p>
<br><br>
<p>Order Number: {{ $oid }}</p>
<br>
<p>Total  : £ {{ $total }}</p>
    


</body>
</html>
