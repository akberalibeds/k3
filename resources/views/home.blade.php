@extends('layouts.app')
@section('title')
Dashboard
@endsection
@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="card card-info">
      <div class=" card-header blue">
        <div class="card-title">
          <div class="title">
            Welcome
          </div>
        </div>
      </div>
      <div class="card-body">
        @if (!Auth::check())
        <p><a href="{{ url('/login') }}">Click here to Login</a></p>
        @else
        <div class="row">
          <div class="col-md-12">
            <ul class="nav nav-tabs">
              <li class="active"><a data-toggle="pill" href="#bulletins">Recent Bulletins</a></li>
              <li><a data-toggle="pill" href="#info">Telephone Numbers &amp; Emails</a></li>
              <li><a data-toggle="pill" href="#documents">Documents &amp; Links</a></li>
              <li><a data-toggle="pill" href="#bookmarks">Bookmarks</a></li>
            </ul>
            <div class="tab-content">
              <div id="bulletins" class="tab-pane fade in active">
                @if (count($bulletins) === 0)
                There are no bulletins.
                @else
                <table class="small table table-condensed table-striped">
                  <thead>
                    <tr>
                      <th class="col-md-2">Title</th>
                      <th class="col-md-3">Message</th>
                      <th class="col-md-2">Expires</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach ($bulletins as $bulletin)
                    <tr>
                      <td>{{ $bulletin->title }}</td>
                      <td>{{ $bulletin->message }}</td>
                      <td>@str2Datetime($bulletin->expires)</td>
                    </tr>
                    @endforeach
                  </tbody>
                </table>
                @endif
              </div>
              <div id="info" class="tab-pane fade">
                <div class="row">
                  <div class="col-md-6 col-sm-6">
                    <p><strong>Emails addresses</strong></p>
                    @if (count($emails) === 0)
                    There are no email addresses.
                    @else
                    <table class="small table table-condensed table-striped table-no-wrap">
                      @foreach ($emails as $email)
                      <tr>
                        <td>{{ $email->description }}</td>
                        <td>{{ $email->datum }}</td>
                      </tr>
                      @endforeach
                    </table>
                    @endif
                  </div>
                  <div class="col-md-6 col-sm-6">
                    <p><strong>Phone numbers</strong></p>
                    @if (count($numbers) === 0)
                    There are no phone numbers.
                    @else
                    <table class="small table table-condensed table-striped table-no-wrap">
                      <tbody>
                        @foreach ($numbers as $number)
                        <tr>
                          <td>{{ $number->description }}</td>
                          <td>{{ $number->datum }}</td>
                        </tr>
                        @endforeach
                      </tbody>
                    </table>
                    @endif
                  </div>
                </div>
              </div>
              <div id="documents" class="tab-pane fade">
                <div class="row">
                  <div class="col-md-6 col-sm-6">
                    <p><strong>Documents</strong></p>
                    @if (count($documents) === 0)
                    There are no documents.
                    @else
                    @include('share.documents-list')
                    @endif
                  </div>
                  <div class="col-md-6 col-sm-6">
                    <p><strong>Links</strong></p>
                    @if (count($links) === 0)
                    There are no links.
                    @else
                    <table class="small table table-condensed table-striped table-no-wrap">
                      @foreach ($links as $link)
                      <tr>
                        <td>{{ $link->description }}</td>
                        <td><a href="{{ $link->datum }}" title="{{ $link->description }}">{{ $link->datum }}</a></td>
                      </tr>
                      @endforeach
                    </table>
                    @endif
                  </div>
                </div>
              </div>
              <div id="bookmarks" class="tab-pane fade">
                <div class="row">
                  <div class="col-md-12 col-sm-12">
                    <p><strong>Your Bookmarks</strong></p>
                    @if (count($bookmarks) === 0)
                    You have no Bookmarks.
                    @else
                    <table class="small table table-condensed table-striped table-no-wrap">
                      @foreach ($bookmarks as $bookmark)
                      <tr>
                        <td>{{ $bookmark->description }}</td>
                        <td>{{ $bookmark->url }}</td>
                      </tr>
                      @endforeach
                    </table>
                    @endif
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        @endif
      </div>
      <div class="card-footer">
      </div>
    </div>
  </div>
</div>
@endsection
