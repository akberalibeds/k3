@extends('layouts.app')
@section('title')
Data
@endsection
@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="card card-info">
      <div class=" card-header blue">
        <div class="card-title">
          <div class="title">Saved</div>
        </div>
      </div>
      <div class="card-body">
      		
      		
      		<div class="responsive">
      	<table class="table table-bordered table-hover table-striped">
      		<thead>
      			<tr>
      				<th>Query</th>
      				<th></th>
      				<th></th>
      			</tr>
      		</thead>
      		<tbody>
      			@if(isset($queries))
      				@foreach($queries as $row)
	      				<tr>
	      					<td>{!! $row->query !!}</td>
	      					<td><form method="post" action="/data">{{ csrf_field() }}<input type="hidden" name="save" value="0"><input type="hidden" name="query" value="{{ $row->query }}"><button class="btn btn-default"><i class="text-warning fa fa-bolt" aria-hidden="true"></i></button></form></td>
	      					<td><form method="post" action="/data/remove"><button class="btn btn-default">{{ csrf_field() }}<input type="hidden" name="id" value="{{ $row->id }}"><i class="text-danger fa fa-trash"></i></button></form></td>
	      				</tr>
      				@endforeach
      			@endif
      		</tbody>
      	</table>
      </div>
      		
      </div>
    </div>
	
	
	
   
    
    
    
    
  </div>

@endsection
@section('js')

@endsection
