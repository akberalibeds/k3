@extends('layouts.app')
@section('title')
Data
@endsection
@section('content')

@if(session('error'))
<div class="alert alert-dismissable alert-danger">
<button type="button" class="close" data-dismiss="alert">&times;</button>
	error {{ session('error') }}
</div>
@endif
<div class="row">
  <div class="col-md-12">
    <div class="card card-info">
      <div class=" card-header blue">
        <div class="card-title">
          <div class="title">Data</div>
        </div>
      </div>
      <div class="card-body">
      <form class="form" action="/data" method="POST">
      {{ csrf_field() }}
      <input class="save" type="hidden" name="save" value="0" >
      <textarea name="query" class="form-control query" rows="5">@if(request('query')) {{ request('query') }} @endif </textarea>
      <span class="length"></span>
       
      </div>
      <div class="card-footer">
      		<div class="pull-right" style="padding:10px;">
      		<button type="button" onclick="$('.form').attr('action','/data').submit();" class="btn btn-default"><i class="text-warning fa fa-bolt" aria-hidden="true"></i></button>
      		<button type="button" onclick="$('.save').val('1');$('.form').attr('action','/data').submit();" class="btn btn-default">
	      		
				  <i class="fa fa-floppy-o f"></i>
				  <i class="fa fa-bolt text-warning"></i>
				
			</button>
      		<button type="button" onclick="$('.form').attr('action','/data/csv').submit();" class="btn btn-default"><i class="fa fa-file-excel-o" aria-hidden="true"></i></button>
      		</div> 
      		<div style="padding:10px;"><a href="/data/saved" class="btn btn-default"><i class="fa fa-bars" aria-hidden="true"></i></a></div>
      		<div class="clearfix"></div> 
      </div>
      	</form>
      </div>
    </div>
	
	<div class="col-md-12" style="height:20px;"></div>
	
    <div class="col-md-12">
    <div class="card card-info">
      <div class="card-body">
      
      <div class="responsive">
      	<table class="table table-bordered table-hover table-striped">
      		<thead>
      			@if(isset($titles))
      				<tr>
      					@foreach($titles as $h)
      						<th>{{ $h }}</th>
      					@endforeach
      				</tr>
      			@endif
      		</thead>
      		<tbody>
      			@if(isset($data))
      				@foreach($data as $row)
	      				<tr>
	      					@foreach($row as $h)
	      						<td>{{ $h }}</td>
	      					@endforeach
	      				</tr>
      				@endforeach
      			@endif
      		</tbody>
      	</table>
      </div>
       
      </div>
      <div class="card-footer">
      		
      </div>
      </div>
    </div>
    
    
    
    
  </div>

@endsection
@section('js')
<script>
	$('.query').keyup(function(e){
		$('.length').html($(this).val().length);
	});
</script>
@endsection
