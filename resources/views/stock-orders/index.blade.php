@extends('layouts.app')
@section('title')
Stock Item Orders for &apos;{{ $stockItem['itemCode'] }}&apos;
@endsection
@section('content')
<div class="row">
  <div class="card card-info">
    <div class="card-header blue">
      <div class="card-title">
        <div class="title">Stock Item Orders for &apos;{{ $stockItem['itemCode'] }}&apos;</div>
      </div>
    </div>
    <div class="card-body">
      <div class="row">
        <div class="col-md-4 col-sm-6 col-xs-12">
          @include('share.search-bar', [
            'options' => [
              ['value' => 'orders.id', 'title' => 'Order ID'],
              ['value' => 'dPostcode', 'title' => 'Postcode'],
            ],
            'selectable' => true,
          ])
        </div>
         <div class="col-md-8">
        	<div class="pull-right">
             	<button class="btn btn-info" data-toggle="modal" data-target="#modal-mail"><i class="fa fa-fw fa-envelope"></i> Send Email</button>
           	</div> 
          </div>
      </div>
      <div class="table-responsive">
        <table id="table-results-container" class="small table table-striped table-no-wrap table-clickable table-hover">
          <thead>
            <tr>
              <th id="select-all-container"></th>
              <th>Order</th>
              <th>Invoice</th>
              <th>Buyer</th>
              <th>Customer</th>
              <th>Postcode</th>
              <th>Ref</th>
              <th>Type</th>
              <th>Status</th>
              <th>Date</th>
              <th class="text-right">Total</th>
              <th class="text-right">Paid</th>
              <th class="text-right">Balance</th>
              <th class="text-center">Done</th>
            </tr>
          </thead>
          <tbody class="table-clickable" id="result-container">
          </tbody>
        </table>
      </div>
    </div>
    <div class="card-footer">
      <div class="row">
        <div class="col-md-12 text-center" id="paging-controls-container">
        </div>
      </div>
    </div>
  </div>
</div>


	<div class="modal fade" id="modal-mail" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title" id="myModalLabel"><strong>Send Email</strong></h4>
                        </div>
                        <div class="modal-body">

                                        	<div class="form-group">
                                            <label class="control-label">Subject</label>
                                            <input class="form-control subject"  />
                                            </div>
                                        	<div class="form-group">
                                            <textarea style="width:100%;" id="mailBody" rows="25" class="mailBody"></textarea>
                                            </div>

                                           

                                            <br>
                                            <br>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-danger closeMail" data-dismiss="modal">Cancel</button>
                            <button type="button" class="btn btn-success" onClick="email();"><i class="fa fa-envelope"></i> Send</button>
                        </div>
                    </div>
                </div>
            </div>
            
            
            
     <button style="visibility: hidden;" class="btn btn-info dialogBtn" data-toggle="modal" data-target="#modal-dialog">a</button>       
            
            <div class="modal fade" id="modal-dialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="myModalLabel"><strong>Sending Emails</strong></h4>
                        </div>
                        <div class="modal-body" align="center">
							 <div class="sentInfo"></div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-success closeDialog" data-dismiss="modal">Done</button>
                        </div>
                    </div>
                </div>
            </div>
            

@endsection
@push('js')
<script>
var stockItemId = {{ $stockItem['id'] }}
</script>
<script type="text/javascript" src="{{ URL::asset('js/library/table-results_0.2.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/stock-orders/index.js') }}"></script>
<script src="//cdn.ckeditor.com/4.5.3/standard/ckeditor.js"></script>
<script>
 	var sent = 0;
 	var total = 0;
	$(window).load(function(e) {
        CKEDITOR.replace( 'mailBody' );
    });
	function email(){
		sent=0;
		total=0;
		$('.closeDialog').hide();
		var selected = tableResults.getSelectedIds()
		$.form({
			 method: 'POST',
			 url: '/stock-orders/' + stockItemId,
			 data: {selected},
			 success: function(data){
				 total = data.length;
				 $('.closeMail').click();
				 $('.dialogBtn').click();
				 
				 $('.sentInfo').html('Sent: ' + sent + ' / ' + total);
				 $.each(data,function(i,v){
						sendMail(v);
				 });
			 }
		});
		
		return ;
	}
	
	function sendMail(data){
		
		var bodys 	 	= CKEDITOR.instances.mailBody.getData();
		var subject 	= $('.subject').val();
		var name 		= data.name;
		var email 		= data.email;
		$.form({
			method: 'Post',
			url:"/mailer/stockMail",
			data:"body="+bodys+"&subject="+subject+"&email="+email+"&name="+name,
			success: function(data){
					sent++;
					if(sent==total){
						$('.closeDialog').show();
						$('.sentInfo').html('Sent: ' + sent + ' / ' + total +'<h3>Completed</h3>');
					}
					else{
						$('.sentInfo').html('Sent: ' + sent + ' / ' + total);
					}
					
			}
		});	
			
	}
</script>
@endpush
