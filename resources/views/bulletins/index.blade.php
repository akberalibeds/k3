@extends('layouts.app')
@section('title')
Bulletins
@endsection
@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="card card-info">
      <div class=" card-header blue">
        <div class="card-title">
          <div class="title">Bulletins</div>
        </div>
      </div>
      <div class="card-body">
        <div class="row">
          <div class="col-md-6">
            @include('share.search-bar', [
              'options' => [
                ['value' => 'message', 'title' => 'Message'],
                ['value' => 'title', 'title' => 'Title'],
              ],
              'selectable' => false,
            ])
          </div>
          <div class="col-md-6 text-right">
            <button class="btn btn-default" data-toggle="modal" data-target="#bulletin-create-modal" type="button">Add Bulletin</button>
          </div>
        </div>
        <div class="row">
          <div class="table-responsive">
            <table class="small table table-striped table-no-wrap table-hover" id="table-results-container">
              <thead>
                <tr>
                  <th>Title</th>
                  <th class="col-md-4">Message</th>
                  <th>Create by</th>
                  <th>Created at</th>
                  <th>Expires</th>
                </tr>
              </thead>
              <tbody class="table-clickable" id="result-container">
              </body>
            </table>
          </div>
        </div>
      </div>
      <div class="card-footer">
        <div class="row">
          <div class="col-md-12 text-center" id="paging-controls-container">
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Modal -->
<div class="modal fade" id="bulletin-create-modal" tabindex="-1" role="dialog" aria-labelledby="dispatch-detail-title">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Creete Bulletin</h4>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-12">
            <p><strong>Note:</strong> Messages cannot be edited after being saved.</p>
            <br>

            <div class="form-horizontal">
              <div class="form-group">
                {!! Form::label('title', 'Title') !!}
                {!! Form::text('title') !!}
                {!! Form::error('title') !!}
              </div>
              <div class="form-group">
                {!! Form::label('message', 'Message') !!}
                {!! Form::textarea('message', null, ['rows' => '5']) !!}
                {!! Form::error('message') !!}
              </div>
              <div class="form-group">
                {!! Form::label('expires', 'Expires') !!}
                {!! Form::calendar('expires') !!}
                {!! Form::error('expires') !!}
              </div>
              <div class="col-md-6 text-right">
                <div class="text-right">
                  <button aria-label="Cancel" class="btn btn-default" data-dismiss="modal" role="cancel" type="button">cancel</button>
                  <button class="btn btn-primary" onclick="saveBulletin(event, '{{ route('bulletins::store') }}')" role="save" type="submit">save</button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
@section('js')
<script type="text/javascript" src="{{ URL::asset('js/library/table-results_0.2.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/bulletins/index.js') }}"></script>
@endsection
