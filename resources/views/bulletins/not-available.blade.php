@extends('layouts.app')
@section('title')
Bulletins - Not Available
@endsection@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="card card-info">
      <div class=" card-header blue">
        <div class="card-title">
          <div class="title">Bulletins - Not Available</div>
        </div>
      </div>
      <div class="card-body">
        <p><strong>Bulletins</strong> is currently unavailable.  Please be patient while the developers are <del>updating</del> being updated.</p>
      </div>
      <div class="card-footer">
      </div>
    </div>
  </div>
</div>
@endsection
