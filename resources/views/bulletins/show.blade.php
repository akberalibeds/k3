@extends('layouts.app')
@section('title')
Bulletins Show {{ $bulletin->user['name'] }}
@endsection
@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="card card-info">
      <div class=" card-header blue">
        <div class="card-title">
          <div class="title">
            Bulletins
          </div>
        </div>
      </div>
      <div class="card-body">
        <div class="row">
          <div class="col-md-6">
            <dl class="dl-horizontal">
              <dt>Title</dt>
              <dd>{{ $bulletin->id }}</dd>
              <dt>Created by</dt>
              <dd>{{ $bulletin->user['name'] }}</dd>
              <dt>Created at</dt>
              <dd>@str2Datetime($bulletin->created_at)</dd>
              <dt>Expires</dt>
              <dd>@str2Datetime($bulletin->expires)</dd>
              <dt>Message</dt>
              <dd>{{ $bulletin->message }}</dd>
            </dl>
          </div>
        </div>
      </div>
      <div class="card-footer">
      </div>
    </div>
  </div>
</div>
@endsection
