@extends('layouts.app')
@section('title')
Dispatched Orders
@endsection
@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="card card-info">
      <div class=" card-header blue">
        <div class="card-title">
          <div class="title">Dispatched</div>
        </div>
      </div>
      <div class="card-body">
        <div class="row">
          <div class="col-md-12 text-right">
            <div class="btn-group" role="group" aria-label="utils">
              <button class="btn btn-default" onclick="generateBarcodes()" type="button">Generate Barcodes</button>
              <button class="btn btn-default" onclick="printBarcodes()" type="button">Print Barcodes</button>
              <button class="btn btn-default" onclick="printDispatchNotes()" type="button">Print dispatch notes</button>
              <button class="btn btn-default" onclick="printLoadSheets()" type="button">Print load sheet</button>
              <button class="btn btn-default" onclick="textShifts()" type="button">Text shifts</button>
              <button class="btn btn-default" onclick="printSupplierOrders()" type="button">Print supplier orders</button>
              <button class="btn btn-default" onclick="printCodOrders()" type="button">Print C.O.D</button>
              <button class="btn btn-default" onclick="textCod()" type="button">Text C.O.D</button>
              <button class="btn btn-default" onclick="markAsDelivered()" type="button">Mark as delivered</button>
              <button class="btn btn-default" onclick="asCsv()" type="button">Export as CSV</button>
              <button class="btn btn-default" onclick="oos()" type="button">Out Of Stock</button>
              <button class="btn btn-default" onclick="tox()" type="button">Date Not Selected</button>
            </div>
          </div>
        </div>
        <br>
        <div class="row">
          <div class="col-md-6 col-sm-12 col-xs-12">
            @include('share.search-bar', [
              'options' => [
                ['value' => 'driver', 'title' => 'Driver'],
                ['value' => 'dispRoute.date', 'title' => 'Date'],
                ['value' => 'mate', 'title' => 'Mate'],
                ['value' => 'disp.route', 'title' => 'Route'],
                ['value' => 'vehicle', 'title' => 'Vehicle'],
              ],
              'selectable' => true,
              'limits' => [10,25,50,100]
            ])
          </div>
        </div>
        <div class="row">
          <div class="col-md-12">
            <div class="table-responsive">
              <table class="small table table-striped table-no-wrap table-hover" id="table-results-container">
                <thead>
                  <tr>
                    <th id="select-all-container"></th>
                    <th data-sortable="route">Route</th>
                    <th data-sortable="date">Date</th>
                    <th data-sortable="driver">Driver</th>
                    <th data-sortable="mate">Mate</th>
                    <th data-sortable="vehicle">Vehicle</th>
                    <th data-sortable="staffName">Dispatched by</th>
                    <th data-sortable="loader">Loader</th>
                    <th data-sortable="picker">Picker</th>
                    <th class="text-right">Payments</th>
                    <th class="text-right">Time</th>
                    <th class="text-center">Priority</th>
                  </tr>
                </thead>
                <tbody class="table-clickable" id="result-container">
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="card-footer">
      <div class="row">
        <div class="col-md-12 text-center" id="paging-controls-container">
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Modal -->
<div class="modal fade" id="dispatch-detail-modal" tabindex="-1" role="dialog" aria-labelledby="dispatch-detail-title">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="dispatch-detail-title"></h4>
      </div>
      <div class="modal-body">
        <div class="col-md-3 form-group">
          <label class="control-label" for="driver">Driver:</label>
          <select class="form-control selectpicker" id="select-driver" name="driver">
            <option value="">No Driver</option>
            @foreach ($drivers as $driver)
            	<option value="{{ $driver->id }}">{{ $driver->name }}</option>
            @endforeach
          </select>
        </div>
        <div class="col-md-3 form-group">
          <label class="control-label">Mate:</label>
          <select class="form-control selectpicker" id="select-mate" name="mate">
            <option value="" selected>No Driver Mate</option>
            @foreach ($driverMates as $driverMate)
           		 <option value="{{ $driverMate->id }}">{{ $driverMate->name }}</option>
            @endforeach
          </select>
        </div>
        <div class="col-md-3 form-group">
          <label class="control-label">No Van</label>
          <select class="form-control selectpicker" id="select-vehicle" name="vehicle">
            <option value="" selected>No Van</option>
            @foreach ($vehicles as $vehicle)
            <option value="{{ $vehicle->id }}">{{ $vehicle->reg }}</option>
            @endforeach
          </select>
        </div>
        <div class="col-md-3 form-group">
          <label class="control-label">Loader</label>
          <select class="form-control selectpicker" id="select-loader" name="loader">
            <option value="" selected>No Loader</option>
            @foreach ($pickers as $picker)
            <option value="{{ $picker->id }}">{{ $picker->name }}</option>
            @endforeach
          </select>
        </div>
        <div class="col-md-3 form-group">
          <label class="control-label">Picker(s):</label>
          <select multiple class="form-control selectpicker" id="select-picker" name="picker[]">
            <option value="" selected>No Picker</option>
            @foreach ($pickers as $picker)
            <option value="{{ $picker->id }}">{{ $picker->name }}</option>
            @endforeach
          </select>
        </div>
        <div class="col-md-3 form-group">
          <label class="control-label">Time:</label>
          <input class="form-control" id="start-time">
        </div>
        
        @if(Auth::user()->hasRole('del_sort'))
        	<div class="col-md-3 form-group">
          		<button onclick="makeSort();" class="btn btn-success sorter">Start Sort</button>
	        </div>
        @endif
        
        <table class="small table table-striped table-no-wrap table-hover">
          <thead>
            <tr>
              <th>Num</th>
              <th>Name</th>
              <th>Postcode</th>
              <th>Status</th>
              <th>Tel</th>
              <th class="text-center">Text</th>
            </tr>
          </thead>
          <tbody class="table-clickable" id="route-orders-table">
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
<script>
var drivers = new Map()
var driverMates = new Map()
var pickers = new Map()
var vehicles = new Map()
var selectedRoute = 0;
@foreach ($drivers as $driver)
drivers.set('{{ $driver->name }}', '{{ $driver->id }}')
@endforeach
@foreach ($driverMates as $driverMate)
driverMates.set('{{ trim($driverMate->name) }}', '{{ $driverMate->id }}')
@endforeach
@foreach ($vehicles as $vehicle)
vehicles.set('{{ trim($vehicle->reg) }}', '{{ trim($vehicle->id) }}')
@endforeach
@foreach ($pickers as $picker)
pickers.set('{{ trim($picker->name) }}', '{{ $picker->id }}')
@endforeach
</script>
@endsection

@section('js')

<script>
 	
	$('#dispatch-detail-modal .selectpicker').change(function(e){
		setOptions(this);
	});

	$('#start-time').change(function(e){
			var item 	= {};
			item.text 	= this.value;
			item.id		= 'a-startTime';
			setOptions(item);
	});

	var items = $.parseJSON('{!! $items !!}');
	
	$(document).ready(function () {
	    $.getScript("https://code.jquery.com/ui/1.9.2/jquery-ui.js");
	});



	function makeSort(){

		var button = $('.sorter');
		$(button).removeClass('btn-success');
		$(button).addClass('btn-danger');
		$(button).html('sorting...');
		

		
		$('#route-orders-table tr').each(function(index,row){
			$(row).removeAttr('onclick');
		});
		
	      $('#route-orders-table').sortable({
	    	  stop:function(event){
	    		  var data = {};
	    		  $('#route-orders-table tr').each(function(index,row){
	    			$(row).find('td').eq(0).html(index+1);
	    			data[$(row).attr('data-order-id')]=index+1;
	    		  });
	    		  
	    		 $.form({
	    			 method: "POST",
	    			 url: "/dispatch/dispatched/sort",
	    			 data: {data}
	    		 })
	    		  
	    	  }
	      }); 
	}
	
</script>
<script type="text/javascript" src="{{ URL::asset('js/library/table-results_0.2.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/dispatched/index.js') }}"></script>



@endsection
