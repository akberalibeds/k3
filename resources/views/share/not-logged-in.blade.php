<?php if (!Route::uses('App\Http\Controllers\Auth\AuthController@showLoginForm')) : ?>
<div style="background-color:#C9F6B5;color:black;left:0;position:absolute;text-align:center;top:0;width:100%;">
  You are not logged in. You will be redirected to the login page shortly or <a href="/login">click here</a>.
</div>
<script>
document.location.href = '/login'
</script>
<?php endif  ?>
