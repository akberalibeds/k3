<div class="modal fade" id="flash-message-modal" tabindex="-1" role="dialog" aria-labelledby="Flash Message">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Bulletin</h4>
      </div>
      <div class="modal-body" data-message>
        ...
      </div>
      <div class="modal-footer">
        <button class="btn btn-default" data-dismiss="modal" onclick="confirmReadMessage(this)" type="button">Confirm as read</button>
      </div>
    </div>
  </div>
</div>

<script>

// var bulletins = {!! Session::get('bulletins') !!}
//
// // $('#flash-message-modal').on('show.bs.modal', function (e) {
// //   $(this).find('[data-title]').html(activeBulletin.title)
// //   $(this).find('[data-message]').html(activeBulletin.message)
// // })
//
// $('#flash-message-modal button').prop('disabled', false)
//
// $('#flash-message-modal').modal({
//   backdrop: 'static',
//   keyboard: false,
//   show: true
// })
//
// for (var i = 0, j = bulletins.length; i < j; i++) {
//   var a, b
//   var $element
//
//   $element = $('<div>').append($('<h1>').html(bulletins.title))
//
//   $('#flash-message-modal [data-message]').append($element)
//   //   $(this).find('[data-title]').html(activeBulletin.title)
//   //   $(this).find('[data-message]').html(activeBulletin.message)
//   // })
// }
//
// function confirmReadMessage(self) {
//   $(self).attr('disabled', true)
//
//   $(self).html('<span class="busy-spinner glyphicon glyphicon-cog" aria-hidden="true"></span>')
//
//   $.form({
//     method: 'POST',
//     url: '/bulletins/' + 23 + '/confirm-as-read',
//     success: function(msg) {
//       $(self).attr('disabled', false)
//       $(self).html('Confirm as read')
//       $('#flash-message-modal').modal('hide')
//     },
//     error: function(msg) {
//     }
//   })
// }
</script>
