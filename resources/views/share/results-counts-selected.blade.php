<div class="thin-row">
  <div class="col-md-12">
    <small>
      showing: <span id="result-counts"></span>, selected: <span id="selected-count" style="display:inline-block;width:3em"></span>
    </small>
    <div class="btn-group" role="group" aria-label="selected rows buttons">
      <button class="btn btd-default btn-xs" onclick="clearSelection()" role="button">&nbsp;&nbsp;clear&nbsp;&nbsp;</button>
      <button class="btn btd-default btn-xs" onclick="showSelected()" role="button">&nbsp;&nbsp;show&nbsp;&nbsp;</button>
    </div>
  </div>
</div>
