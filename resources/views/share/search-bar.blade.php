<div class="row">
  <div class="col-md-12">
    <div class="search-bar">
     @if(isset($limits))
       <select class="form-control selectpicker" data-container="body" data-style="btn-default" data-width="8em" id="result-limit" name="result-limit" onchange="tableResults.queryChange(event)">
        @foreach ($limits as $option)
        <option value="{{ $option }}">{{ $option }}</option>
        @endforeach
      </select>
     @endif
      <select class="form-control selectpicker" data-container="body" data-style="btn-default" data-width="8em" id="subject-select" name="select-search-by" onchange="tableResults.queryFilterChange(event)">
        @foreach ($options as $option)
        <option value="{{ $option['value'] }}">{{ $option['title'] }}</option>
        @endforeach
      </select>
      <select class="form-control selectpicker" data-container="body" data-style="btn-default" data-width="3em" id="any-begin" name="any-begin" onchange="tableResults.queryFilterChange(event)">
        <option value="any">Anywhere</option>
        <option value="begin">Begins</option>
        <option value="end">Ends</option>
      </select>
      <div class="input-group">
        <input autocomplete="off" class="form-control" id="search-input" name="search-term" onkeyup="tableResults.queryChange(event)" placeholder="search" type="search">
        <span class="input-group-btn">
          <button class="btn btn-default" onclick="tableResults.reset(event)" role="button" title="Reset"><i class="fa fa-circle-o" aria-hidden="true"></i></button>
          <button class="btn btn-default" onclick="tableResults.refresh(event)" role="button" title="Refresh"><i class="fa fa-refresh" aria-hidden="true"></i></button>
        </span> 
      </div>
      @include('share.search-spinner')
    </div>
  </div>
</div>
<div class="row">
  @if (isset($selectable) && $selectable)
  <div class="col-md-5">
    <span class="badge" id="selected-count" style="display:inline-block;width:3.5em">0</span> selected
    <div class="btn-group" role="group" aria-label="selected rows buttons">
      <button class="btn btd-default btn-mini" onclick="tableResults.clearSelection(event)" role="button">&nbsp;&nbsp;clear&nbsp;&nbsp;</button>
      <button class="btn btd-default btn-mini" onclick="tableResults.showSelected(event)" role="button">&nbsp;&nbsp;show&nbsp;&nbsp;</button>
    </div>
  </div>
  @endif
 
  <div class="col-md-5">
    <small style="vertical-align:bottom">
      showing <span id="result-counts" style="display:inline-block;min-width:6em">-</span>
      (<span id="per-page-count" style="display:inline-block;">-</span>pp)
    </small>
  </div>
 
  <div class="col-md-2">
    <button class="btn btn-defualt btn-mini" onclick="tableResults.columnVisibility()">Column Visibility</button>
  </div>
  @if(Request::is('admin/stock'))
  <div class="col-md-6">
    <button class="btn btn-block btn-primary" onclick="tableResults.sku2InActive(event)" role="button">Deactivate Stock Item Code</button>
  </div>
  <div class="col-md-6">
    <button class="btn btn-block btn-primary" onclick="tableResults.sku2Active(event)" role="button">Reactivate Stock Item Code</button>
  </div>
  @endif
  <br>
  <br>
</div>
