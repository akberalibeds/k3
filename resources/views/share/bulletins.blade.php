@push('js')
<script>
var unreadBulletins = []
@foreach ($unreadBulletins as $bulletin)
unreadBulletins.push({ id: '{{ $bulletin->id }}', message: '{{ $bulletin->message }}', title: '{{ $bulletin->title }}' })
@endforeach
</script>
@endpush
