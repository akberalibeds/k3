<div class="card-body">
  <div class="row">
    <div class="col-md-4">
      <div class="input-group input-group-typeahead">
        <input class="form-control typeahead" id="typeahead-stock-item-code" placeholder="search by item code / SKU" type="search">
        <span class="tt-badge" id="item-code-spinner"></span>
      </div>
    </div>
    <div class="col-md-4">
        <div class="input-group input-group-typeahead">
          <input class="form-control typeahead" id="typeahead-stock-item-description" placeholder="search by description" type="search">
          <span class="tt-badge" id="description-spinner"></span>
        </div>
    </div>
  </div>
  <div class="thin-row">
    <div class="col-md-12">
      <small id="results-count"></small>
    </div>
  </div>
  <div class="table-responsive">
    <table class="small table table-condensed table-striped table-hover table-no-wrap">
      <thead>
        <tr>
          <th>SKU</th>
          <th>Description</th>
          <th class="text-right">In Stock</th>
          <th class="text-right">On SO</th>
          <th class="text-right">Available</th>
          <th class="text-right">Cost</th>
          <th class="text-right">Retail</th>
          <th class="text-right">Wholesale</th>
          <th class="text-right">Boxes</th>
          <th class="text-right">Pieces</th>
          <th class="text-center">Blocked</th>
        </tr>
      </thead>
      <tbody id="stock-item-table">
      </tbody>
    </table>
  </div>
</div>
<div class="card-footer">
  <div class="row">
    <div class="col-md-12 text-center" id="paging-controls-container">
    </div>
  </div>
</div>

@push('js')
<script type="text/javascript" src="{{ URL::asset('js/library/typeahead.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/library/typeahead-kit.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/library/paging.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/library/import-csv.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/stock/index.js') }}"></script>
@endpush
