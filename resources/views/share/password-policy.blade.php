<p>Ideally your Password will consist of letters (upper and lower case), numbers and symbols (! £ $ % ^ &amp; * _ + - =)</p>
<p>The password will be a minimum of 6 characters.</p>
<p>You should tell no-one your password and not leave it written down in places that other people can see it.</p>

<button class="btn btn-default" onclick="suggestPassword()">Suggest a Password</button>&nbsp;<span id="password-suggestion"></span>
