<ul class="list-unstyled">
  @foreach ($documents as $document)
  <li>
    <a href="{{ route('home::show-document', ['id' => $document->id]) }}">
      {{ $document->description }}</a>&nbsp;<span class="glyphicon glyphicon-download" aria-hidden="true"></span>
  </li>
  @endforeach
</ul>
