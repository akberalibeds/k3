<ul class="dropdown-menu animated fadeInDown list-bookmarks" id="user-bookmarks">
  <li class="">
    &nbsp;<strong>bookmarks&nbsp;</strong>
    <button class="btn btn-xs" onclick="addBookmark()">
      <span class="fa fa-plus"></span>
    </button>
  </li>
  @foreach ($bookmarks as $bookmark)
  <li class="">
    <a class="bookmark" href="{{ $bookmark->url }}" rel="bookmark">
      {{ $bookmark->description }}
    </a>
  </li>
  @endforeach
</ul>

@push('js')
<script type="text/javascript" src="{{ URL::asset('js/share/bookmark.js') }}"></script>
@endpush
