<div class="card-body">
  <div class="row">
    <div class="col-md-6">
      <div class="input-group input-group-typeahead">
        <input class="form-control typeahead" id="typeahead-category-name" placeholder="search" type="search">
        <span class="tt-badge" id="typeahead-spinner"></span>
      </div>
    </div>
  </div>
  <div class="thin-row">
    <div class="col-md-12">
      <small id="result-counts"></small>
    </div>
  </div>
  <div class="table-responsive">
    <table class="small table table-condensed table-striped table-hover table-no-wrap">
      <thead>
        <tr>
          <th>Name</th>
          <th>Description</th>
        </tr>
      </thead>
      <tbody id="categories-table">
      </tbody>
    </table>
  </div>
</div>
<div class="card-footer">
  <div class="row">
    <div class="col-md-12 text-center" id="paging-controls-container">
    </div>
  </div>
</div>

@push('js')
<script type="text/javascript" src="{{ URL::asset('js/library/typeahead.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/library/typeahead-kit.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/library/paging.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/stock-categories/index.js') }}"></script>
@endpush
