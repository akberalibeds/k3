@extends('layouts.app')
@section('title')
Notification Control
@endSection

@section('css')
<link rel="stylesheet" type="text/css" href="{{ URL::asset('css/notifications.css') }}">
@endsection

@section('content')

<div class="row">
    <h3>Notification List</h3>
    <hr>
    <!-- Filter Panel -->
    <div class="col-md-2">
        <div class="row" onclick="alert('Currently Unavaliable')">
            <button class="btn btn-block btn-primary">Create New Notification <i class="fa fa-pencil-square-o" aria-hidden="true"></i></button>
        </div>
        <h6>Filter Notification By:</h6>
        <div class="row">
        <!-- If condition to show all if one below is selected -->
            <a style ="color:white" href="{{route('notifications.index')}}">
                <button class="btn btn-block btn-info">List Notifications</button>
            </a>
            <a style ="color:white" href="{{route('notifications.index', ['type' => 'DeliveryDateRequest'])}}">
                <button class="btn btn-block btn-info">Delivery Date Requested</button>
            </a>
            <a style ="color:white" href="{{route('notifications.index', ['type' => 'DeliveryDateRequestReminder'])}}">
                <button class="btn btn-block btn-info">Delivery Date Request Reminder</button>
            </a>
            <a style ="color:white" href="{{route('notifications.index', ['type' => 'DispatchReady'])}}">
                <button class="btn btn-block btn-info">Dispatch Ready</button>
            </a>
            <a style ="color:white" href="{{route('notifications.index', ['type' => 'DeliveryImminent'])}}">
                <button class="btn btn-block btn-info">To be delivered soon</button>
            </a>
        </div>
        <h6>Filter Notification Status By:</h6>
        <div class="row">
            <a style ="color:white" href="{{route('notifications.sentFlag', ['hasSent' => 1])}}">
                <button class="btn btn-block btn-info">Sent</button>
            </a>
            <a style ="color:white" href="{{route('notifications.showPartial') }}">
                <button class="btn btn-block btn-info">Partial</button>
            </a>
            <a style ="color:white" href="{{route('notifications.sentFlag', ['hasSent' => 0])}}">
                <button class="btn btn-block btn-info">Not Sent</button>
            </a>
        </div>
        <h6>Filter All Notifications By Date:</h6>
        <div class="row">
            <input class="form-control" id="datePicker" type="date" value="{{ (new \DateTime($date))->format('Y-m-d') }}" >
            <button class="btn btn-block btn-info" onclick="getNotificationDates()">Search Date</button>
        </div>

        <!-- To revisit soon for query usage -->
        <!-- <h6>Filter Notifications By Sent:</h6>
        <div class="row">
            <div class="form-control form-check" onclick="getSentFlag()">
                <input class="form-check-input" type="radio" name="sentFlag" id="showAll" value="showAll">
                <label class="form-check-label" for="showAll">
                    Show All
                </label>
            </div>
            <div class="form-control form-check" onclick="getSentFlag()">
                <input class="form-check-input" type="radio" name="sentFlag" id="sent" value="sent">
                <label class="form-check-label" for="sent">
                    Sent Succeeded
                </label>
            </div>
            <div class="form-control form-check" onclick="getSentFlag()">
                <input class="form-check-input" type="radio" name="sentFlag" id="notSent" value="notSent">
                <label class="form-check-label" for="notSent">
                    Sent Failed
                </label>
            </div>
        </div> -->

        <!-- <h6>Filter Notifications Messages By:</h6>
        <div class="row">
            <div class="btn-group btn-group-justified" role="group" aria-label="Notification Example">
                <div class="btn-group" >
                    <button type="button" class="btn btn-secondary btn-info">eBay</button>
                </div>
                <div class="btn-group" >
                    <button type="button" class="btn btn-secondary btn-info">Email<i class="fa fa-envelope" aria-hidden="true"></i></button>
                </div>
                <div class="btn-group" >
                    <button type="button" class="btn btn-secondary btn-info">SMS<i class="fa fa-mobile" aria-hidden="true"></i></button>
                </div>
            </div>
        </div> -->
    </div>
    <div class="col-md-10 table-responsive">
        <table class="table table-bordered">
            <tr>
                <th>Notification ID</th>
                <th>Customer</th>
                <th>Order ID</th>
                <th>Dispatch ID</th>
                <th>Notification Type</th>
                <th>Notification Status</th>
                <th>Notification Created</th>
                <th>Notification Updated</th>
            </tr>
            @forelse($notificationList as $notification)
                <tr class="{{$notification->getCompletion()}}">
                    <td><a href="{{route('notifications.show', $notification->id )}}">{{ $notification->id }}</a></td>
                    <td>{{ $notification->customerID }}</td>
                    <td><a href="{{action('OrdersController@order',$notification->orderID)}}">{{ $notification->orderID }}</a></td>
                    <td>{{ $notification->dispID? $notification->dispID : 'Not Assigned' }}</td>
                    <td>{{ $notification->shortName() }}</td>
                    <td>Sent {{ ucwords($notification->getCompletion()) }}</td>
                    <td>{{ $notification->created_at->format('j F Y H:i') }}</td>
                    <td>{{ $notification->updated_at->diffForHumans() }}</td>
                </tr>
            @empty
                <h1 style="text-align:center; font-weight:bold">No Notifications avaliable</h1>
            @endforelse
        </table>
    </div>
    <div class="text-center">
        {{ $notificationList->appends(request()->except('page'))->links() }}
    </div>
</div>

<script>
    function getNotificationDates() {
        var date = document.getElementById("datePicker").value;
        if(! date ) {
            alert('Please select a date!');
            return;
        }
        window.location.href = '?date=' + date;
    }


    function getSentFlag(){
        var flag = document.getElementsByName("sentFlag");
        for(var i = 0; i <flag.length; i++){
            if( flag[i].checked ){
                
                if( flag[i].value !== "showAll"){
                    window.location.href = '&sendFlag=' + flag[i].value;
                }
                // Set the selected Value as checked
                // document.getElementsByName("sentFlag").value = flag[i].checked
                // document.querySelector('input[name="sendFlag"].value').checked;
                return alert(flag[i].value);
                // flag[i].value; // How to grab to controller
                console.log(flag[i].value);
                // return;
                
            }
        }
    }
</script>
@endSection()
