@extends('layouts.app')
@section('title')
Notification for {{$notification->id}}
@endSection
@section('content')
<div class="row">
    <div class="row">
        <div class="col-md-2">
            <br>
                <a href="{{ route('notifications.index') }}">
                <button class="btn btn-info btn-block" style="justify-items:center;align-items:center">
                    <i class="fa fa-arrow-left"></i> Back
                </button>
            </a>
            <a onclick="alert('Currently Unavaliable')">
                <button class="btn btn-info btn-block" style="justify-items:center;align-items:center">
                    <i class="fa fa-edit"></i> Create New Messages
                </button>
            </a>
        </div>
        <div class="col-md-10">
            <h3>This is related to Notification ID: {{$notification->id}}</h3>
            <div class="row">
                <div class="col-md-12">
                    <p>Delivery Status: {{$notification->shortName()}}</p>
                    <p>Order ID:  <a href="{{action('OrdersController@order',$notification->orderID)}}">{{$notification->orderID}}</a></p>
                    <p>Customer ID: {{$notification->customerID}}</p>
                </div>
            </div>
            <hr>
            <div class="row">
            <!-- Display Messages -->
            @foreach($notification->messages as $notificationMsg)
                    <div class="col-md-4 col-sm-6"> 
                        @if($notificationMsg->is_sent == 0)
                            <h3 style="font-weight:bolder;color:red">Message {{$notificationMsg->id}}    <button class="btn btn-info toggle" onclick="toggle_messages({{$notificationMsg->id}})">Show/Hide</button></h3>
                        @else
                        <h3>Message {{$notificationMsg->id}}    <button class="btn btn-info toggle" onclick="toggle_messages({{$notificationMsg->id}})">Show/Hide</button></h3>
                        @endif
                        <br>
                        <!-- Separated Toggles per message -->
                        <div id="toggleMsg{{$notificationMsg->id}}" style="display:none;">
                            @if($notificationMsg->is_sent == 0)
                                <p style="font-weight:bolder;color:red">Sent: No</p>
                                <p>Errors: {{$notificationMsg->err_message? $notificationMsg->err_message : 'No Errors Found'}}</p>
                                <p>{{ $notificationMsg->date_sent? 'Attempted to send '.$notificationMsg->date_sent->diffForHumans() : 'Not Yet Sent'}}</p>
                            @else
                                <p style="font-weight:bolder;color:green">Sent: Yes</p>
                                <p>Sent {{$notificationMsg->date_sent->diffForHumans()}}</p>
                            @endif
                            <br>
                            <p>Recipient Name: {{$notificationMsg->recipient_name}}</p>
                            <p>Recipient Contact: {{$notificationMsg->recipient}}</p>
                            <p>Method of Message: {{$notificationMsg->shortName()}}</p>
                                <!-- Seconds Needed? -->
                            <p>Message Created at: {{date('j M y, H:i', strtotime($notificationMsg->created_at))}}</p>
                            <p>Updated at: {{date('j M y, H:i', strtotime($notificationMsg->updated_at))}}</p>
                            <br>
                            @if($notificationMsg->is_sent == 0)
                               <a href="{{route('notificaitonMessage.resend', $notificationMsg)}}"><button class="btn btn-danger btn-block">Resend Message</button></a>
                            @endif
                        </div>
                        <hr>
                    </div>
            @endforeach
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    function toggle_messages(notiID) {
        var msg = document.getElementById("toggleMsg"+notiID);
        if (msg.style.display === "none") {
            msg.style.display = "block";
        } else {
            msg.style.display = "none";
        }
    }
</script>
@endSection