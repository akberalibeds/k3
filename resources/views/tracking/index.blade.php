@extends('layouts.app')

@section('content')
<div class="row">
  <div class="col-md-12">
    @tilelink(["/tracking/bedtastic/create", "bedtastic"])
    @tilelink(["/tracking/tnt/create", "TNT"])
  </div>
</div>
@endsection
