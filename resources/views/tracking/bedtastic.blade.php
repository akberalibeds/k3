@extends('layouts.app')

@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="card card-info">
      <div class=" card-header blue">
        <div class="card-title">
          <div class="title">Bedtastic Tracking Data Import</div>
        </div>
        <div class="pull-right">
          <a class="text-info" href="{{ url('mailer/preview/bedtastic.tracking') }}" target="_blank">(click here to preview email)</a>
        </div>
      </div>
      <div class="card-body">
        <div class="row">
          <div class="col-md-12">
            <div id="import-container">
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection

@section('js')
<script type="text/javascript" src="{{ URL::asset('js/library/import-csv.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/tracking/bedtastic-tracking.js') }}"></script>
@endsection
