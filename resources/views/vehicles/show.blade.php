@extends('layouts.app')

@section('content')
<div class="row">
  <div class="card card-info">
    <div class="card-header blue">
      <div class="card-title">
        <div class="title">Vehicle - {{ $vehicle->reg }}</div>
      </div>
    </div>
    <div class="card-body">
      <div class="row">
        <div class="col-md-12">
          <dl class="dl-horizontal">
            <dt>Registration</dt>
            <dd>{{ $vehicle->reg }}</dd>
            <dt>Height</dt>
            <dd>{{ $vehicle->height }}</dd>
            <dt>Width</dt>
            <dd>{{ $vehicle->width }}</dd>
            <dt>iName</dt>
            <dd>{{ $vehicle->depth }}</dd>
            <dt>Description</dt>
            <dd>{{ $vehicle->description }}</dd>
          </dl>
        </div>
      </div>
    </div>
    <div class="card-footer">
    </div>
  </div>
</div>
@endsection

@section('js')
<script src="/js/sellers/show.js" type="text/javascript"></script>
@endsection
