@extends('layouts.app')
@section('title')
Drivers
@endsection
@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="card card-info">
      <div class=" card-header blue">
        <div class="card-title">
          <div class="title">Driver</div>
        </div>
      </div>
      <div class="card-body">
        <div class="row">
          <div class="col-md-6 col-sm-12 col-xs-12">
            @include('share.search-bar', [
              'options' => [
                ['value' => 'name', 'title' => 'Driver'],
                ['value' => 'num', 'title' => 'Telephone'],
                ['value' => 'uName', 'title' => 'Username'],
              ],
              'selectable' => false
            ])
          </div>
        </div>
        <div class="row">
          <div class="col-md-12">
            <div class="table-responsive">
              <table class="small table table-condensed table-striped table-no-wrap table-hover" id="table-results-container">
                <thead>
                  <tr>
                    <th data-sortable="name">Driver</th>
                    <th data-sortable="uName">Username</th>
                    <th data-sortable="num">Telephone</th>
                    <th class="text-right" data-sortable="rate">Rate</th>
                    <th class="text-right" data-sortable="score">Score</th>
                    <th class="text-center">Active</th>
                  </tr>
                </thead>
                <tbody id="result-container">
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
      <div class="card-footer">
        <div class="row">
          <div class="col-md-12 text-center" id="paging-controls-container">
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
@section('js')
<script type="text/javascript" src="{{ URL::asset('js/library/table-results_0.2.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/drivers/index.js') }}"></script>
@endsection
