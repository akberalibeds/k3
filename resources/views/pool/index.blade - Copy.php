@extends('layouts.app')

@section('title')
Dispatch Pool
@endsection

@section('content')
<div class="row">



<div class="col-md-5 col-sm-12">
          


  <div class="card card-info">
    <div class="card-header blue">
      <div class="card-title">
        <div class="title">Pool</div>
      </div>
    </div>
    <div class="card-body">
      <div class="row">
        <div class="col-md-12 col-sm-12">
          <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
            @foreach ($routes as $route)
            <div class="panel panel-default">
              <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse_{{ $route->id }}" aria-expanded="true" aria-controls="collapse_{{ $route->id }}">
                <div class="panel-heading" role="tab" id="heading_{{ $route->id }}">
                  <h4 class="panel-title">
                      {{ $route->name }}
                  </h4>
                </div>
              </a>
              <div id="collapse_{{ $route->id }}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading_{{ $route->id }}">
                <div class="panel-body">
                  <div class="row">
                    <div class="col-md-12 text-right">
                      <button class="btn btn-default btn-sm" onclick="viewOnMap({{ $route->id }})"><i class="fa fa-globe"></i> View Map</button>
                    </div>
                  </div>
                  <div class="table-responsive">
                    <table class="small table table-condensed table-striped table-no-wrap">
                      <thead>
                        <tr>
                          <th></th>
                          <th></th>
                          <th>Paid</th>
                          <th>Done</th>
                          <th>Company</th>
                          <th>Stock</th>
                          <th>Items</th>
                          <th>Postcode</th>
                          <th>Town</th>
                          <th>Order Date</th>
                          <th>Deliver By</th>
                          <th></th>
                        </tr>
                      </thead>
                      <tbody id="order_table{{ $route->id }}">
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
            @endforeach
          </div>
        </div>
        
      </div>
    </div>
  </div>
  
  </div>
  
  
  <div class="col-md-7 col-sm-12">
          <div class="panel panel-default">
            <div class="panel-body map" id="simple-map">
            </div>
          </div>
        </div>
  
  
  
</div>
@endsection

@section('js')
@endsection
