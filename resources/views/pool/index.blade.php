@extends('layouts.app')

@section('title')
Dispatch Pool
@endsection

@section('content')
<div class="row">


 
<div class="col-md-5 col-sm-12">
	
	
	
   <div class="panel panel-default">
   		<div class="panel-body">
   			<form>
	   			<div class="row">
	        		<div class="col-md-12">
	        			<div class="col-md-8">
	        				<input class="form-control" name="d" value="{{ $date }}"  />
	        			</div>
	        			<div class="col-md-3">
	        				<button class="btn btn-default">Go</button>
	        			</div>
	        		</div>
	        	</div>
        	</form>
        	<button class="btn btn-info" onClick="noStocks();">Out Of Stock</button>
            <button class="btn btn-info" onClick="lowStocks();">Low Stock</button>
        </div>
   </div>  
    


  <div class="card card-info">
    <div class="card-header blue">
      <div class="card-title">
        <div class="title">Pool</div>
      </div>
    </div>
    <div class="card-body">
      <div class="row">
        <div class="col-md-12 col-sm-12">
          <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
            @foreach ($routes as $route)
            <div class="panel panel-default">
              <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse_{{ $route->id }}" aria-expanded="true" aria-controls="collapse_{{ $route->id }}">
                <div class="panel-heading" role="tab" id="heading_{{ $route->id }}">
                  <h4 class="panel-title">
                      {{ $route->name }}
                  </h4>
                </div>
              </a>
              <div id="collapse_{{ $route->id }}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading_{{ $route->id }}">
                <div class="panel-body">
                  <div class="row">
                    <div class="col-md-12 text-right">
                      <button class="btn btn-default btn-sm" onclick="viewOnMap({{ $route->id }})"><i class="fa fa-globe"></i> View Map</button>
                    </div>
                  </div>
                  <div class="table-responsive">
                    <table class="small table table-condensed table-striped table-no-wrap table-bordered">
                      <thead>
                        <tr>
                          <th></th>
                          <th></th>
                          <th>Paid</th>
                          <th>Done</th>
                          <th>Company</th>
                          <th>Stock</th>
                          <th>Items</th>
                          <th>Postcode</th>
                          <th>Town</th>
                          <th>Order Date</th>
                          <th>Deliver By</th>
                          <th></th>
                        </tr>
                      </thead>
                      <tbody id="order_table{{ $route->id }}">
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
            @endforeach
          </div>
        </div>

      </div>
    </div>
  </div>

  </div>


  

  <div class="col-md-7 col-sm-12">
          <div class="panel panel-default" >
            <div class="panel-body map" id="simple-map" style="padding:0px;">
            </div>
          </div>
  </div>



</div>
@endsection

@section('js')

<script src="https://maps.googleapis.com/maps/api/js?key={{ $maps_key }}"></script>
<script src="assets/plugins/google-maps/markerclusterer.js"></script>
<script src="assets/plugins/google-maps/gmaps.js"></script>

<script>
var markers         = {!! @json_encode($orders) !!};
var markerArray     = [];
var allRoutes       = {!! @json_encode($routes) !!};
var missing         = {!! @json_encode($missing) !!};
var items           = {!! @json_encode($items) !!};
var stock           = {!! @json_encode($stock) !!};
var quantities      = {!! @json_encode($qty) !!};
var selected_route  = false;
var higlighted 		= false;
var map;
var lowStock		= {};
var noStock			= {};
var routeColor 		= {}; 

function setColor(route){
		routeColor[route] = Math.floor(Math.random()*16777215).toString(16);		
	};


$(window).load(function(e) {
	$('.map').css('height',$(window).height()-80);
      initializeMaps();
	  addMarkers();
});

$('#accordion .panel-heading').click(function(e) {
	selected_route = this.id;
});

   /*
	*
	* Add all deliveries to map
	*
	*/
	function addMarkers(){

		$.each(markers,function(z,data){
				if(!routeColor.hasOwnProperty(data.delRoute)){
						setColor(data.delRoute);
				}
				addMarker(data.lat,data.lng,data.id,"Remove",'https://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=|'+routeColor[data.delRoute]+'|FFF'/*99ff66|FFF'*/,data.delRoute,z);
		});

	}



	/*
	 *
	 * Add Marker Function
	 *
	 */
	function addMarker(lat,lng,title,content,icon,route,i){


			var pos = new google.maps.LatLng(lat,lng);
			bounds.extend(pos);
			marker = new google.maps.Marker({
				position: pos,
				map: map,
				title: ""+title,
				icon: icon
			});

			marker.set("id",marker.title);

			/*
			Marker Right Click Function
			*/
			google.maps.event.addListener(marker, 'rightclick', (function(marker, i) {
            return function() {

				var html ='<div id="infowindow"><div id="info'+i+'"><strong>'+markers[i].id+'</strong><br>'+markers[i].postcode+'<br>'+route+'<br><a href="#" class="dd" id="'+marker.title+'">'+"Remove"+'</a></div></div>';
				infowindow.setOptions({content: html});
                infowindow.open(map, marker);
            }

    		})(marker, i));

			/*
			Marker Left Click Function
			*/
			google.maps.event.addListener(marker, 'click', (function(marker, i) {
				return function() {

					getRow(marker,i)

				}
				})(marker, i));


		map.fitBounds(bounds);
		markerArray.push(marker);



		//console.log(markers[i].route);

	/*
		$.each(allRoutes,function(p,e){

			//console.log(e.name+'=='+markers[i].route)

			if(e.name==markers[i].route){
				//console.log(e.name);
				getRow(marker,i,e.name,false)

			}
		});
	*/
	}

function initializeMaps() {

		console.log('started');
		var myOptions = {
			mapTypeId: google.maps.MapTypeId.ROADMAP,
			mapTypeControl: false,
			disableDoubleClickZoom : true
		};
		var i;
		map = new google.maps.Map(document.getElementById("simple-map"),myOptions);
		window.map=map;
		 infowindow = new google.maps.InfoWindow();


		 bounds = new google.maps.LatLngBounds();

			var pos = new google.maps.LatLng(52.57111, -2.0237600000000384);
			bounds.extend(pos);
			
			/*marker = new google.maps.Marker({
				position: pos,
				map: map,
				title:'Giomani',
				icon: 'https://chart.apis.google.com/chart?chst=d_map_spin&chld=1.0|0|FFFF42|13|b|'
			});

			google.maps.event.addListener(marker, 'click', (function(marker, i) {
				return function() {
					infowindow.setContent("Giomani");
					infowindow.open(map, marker);
				}

			})(marker, i));*/
			map.fitBounds(bounds);
	}


	function getRow(marker,i,route=null,change_route=true){

				//resetMarkers();

				//console.log(i+','+route+','+change_route);
				var routes = (route) ? route : selected_route;

				if(!routes){
						alert('No Route selected!')
				}
				else{

					var routeName = (route==null) ? $('#'+window.route).attr('name') : route ;
					//console.log(routeName);


					$.form({
							method: "GET",
							url: "/dispatch/deliveries/map/api/getRow/"+marker.title,
							success:function(data){
								
								var error = checkStock(data);
								addRow(data, i, error);
								marker.setMap(null);
								
							}
					});






					//marker.setMap(null);


				}

	}


	/*
	 * Reset Marker Color to Red
	 */

	function resetMarkers()
	{

		for(var zz=0;zz<markerArray.length;zz++)
		{

			markerArray[zz].setIcon('https://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=|99ff66|000')

		}

	}




	/*
	 * Highlight same item pins with low stock.
	 */
	function checkStock(data)
	{
		
		var ignore = ['COLLECT MATTRESS 5FT','4FT-SMALLFOAM','4FT6-MEM','4FT6-FOAM','4FT6-SPRING','5FT-FOAM','5FT-MEM','5FT-SPRING','4FT-FOAM','4FT-SPRING','DELIVERY CHARGE','MISSED PARTS'];
		var divan  = ['Divan beds','ExpressMat','AJ Foams','Durest Beds','Collect Durest','Collect Express','Collect BedComapny','Collect DreamBeds'];

		
		//split items //112-5FT-BLK:1,5FT-FOAM:1
		var order_items = data.items.split(",");

		for(var z=0;z<order_items.length;z++)
		{
			//split item //112-5FT-BLK:1
			var order_item = order_items[z].split(":");

			//console.log(order_item[0] + ' deliveries:' + quantities[order_item[0]] + ' in stock:' + stock[order_item[0]]);
			if($.inArray(data.cat,divan)  !== -1 ){
					//return 'info';
			}
			else if($.inArray(order_item[0],ignore)  !== -1 )
			{	
			//	console.log('skipped '+order_item[0]);
			//	console.log(order_items);
				continue;
			}
			else if(stock[order_item[0]] < 1 )
			{	
				noStock[order_item[0]] = '';
					
				console.log('No Stock!');
				notify("Highlighted Deliveries Out of Stock!!");
				console.log('Out Of Stock !!');
				noStock[order_item[0]] = {};
				noStock[order_item[0]][count(noStock)] =  selected_route;
				return 'danger';//highlightMarkers(order_item[0]);
			}
			else if( quantities[order_item[0]] > stock[order_item[0]] ) 
			{
				notify("Highlighted Deliveries have LOW Stock!!"); 
				//highlightMarkers(order_item[0]);
				console.log('Low Stock !!');
				lowStock[order_item[0]] = {};
				lowStock[order_item[0]][count(lowStock)] =  $(selected_route).find('h4').html();
				return 'warning'; 
			}
			else
			{
				console.log('sorted');
				
			}
		}

		return '';
	}

function count(list){
	var i=0;
	$.each(list,function(){
		i++;
	});
	return i;
}


	function highlightMarkers(item,hide=false)
	{
		highlighted = item;
		
		$.each(items[item],function(i,v){
	
			for(var zz=0;zz<markerArray.length;zz++)
			{
				
				if(v==markerArray[zz].id){
					console.log(v);
					markerArray[zz].setIcon('https://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=|ff8533|000')
				}
				else { 
					if(hide){
						//markerArray[zz].setMap(null);	
					}
				}
	
			}
		});	
	}


	/*
	 * Add Row to route
	 */
	function addRow(json,i,error)
	{
		var divan  = ['Divan beds','ExpressMat','AJ Foams','Durest Beds','Collect Durest','Collect Express','Collect BedComapny','Collect DreamBeds'];
		
		var route = selected_route.split("_");
		var tbl = $('#order_table'+route[1])
		var rows 	= $('tr', tbl);
		var index 	= rows.length+1;

		var p= (parseInt(json.p)==1) ? "<i class='fa fa-paypal'></i>" : '';
		var image =	"<span class='label label-"+(json.stockCount)+"'>&nbsp;&nbsp;&nbsp;</span>";
		var paid  = (json.p==1) ? '<i style="color:#039;" class="fa fa-paypal"></i>' : '' ;
		var done  = (json.done==1) ? '<i style="color:green;" class="fa fa-check-circle"></i>' : '';

		var items = json.items;
		var badge = false;
		var order_items = items.split(",");
		for(var i=0;i<order_items.length;i++){
			var item = order_items[i].split(":");
			if(item[0]=="Collection Item"){
				badge=true;
			}
		}

		if($.inArray(json.cat,divan)  !== -1 || badge){
			items = '<big><span class="label label-danger">'+json.items+'</span></big>';
		}
		console.log(items);
		var row="<tr class='"+error+"' id='"+json.id+"' lat='"+json.lat+"' lng='"+json.lng+"' name='"+json.name+"' ><td>"+index+"</td><td>"+json.dispatchType+"</td><td>"+p+"</td><td>"+done+"</td><td>"+json.companyName+"</td><td><span class='label label-"+(json.stockCount)+"'>&nbsp;&nbsp;&nbsp;&nbsp;</span></td><td class='items'>"+items+'</td><td class="postcode">'+json.postcode+'</td><td>'+json.town+'</td><td>'+json.startDate+'</td><td>'+json.deliverBy+'</td><td> <i num="'+i+'" class="rem fa fa-trash-o fa-lg"></i></td></tr>';

		tbl.append(row);

	}
	
	function noStocks()
	{

		var list = '<table class="table table-bordered table-condensed table-striped">';
		$.each(noStock,function(key,value){
			console.log(key);
			$.each(value,function(){
				alert(this);
			})
		});
		
	}
	
	
	function lowStocks()
	{
		alert(lowStock);
	}
	
	

	/*
	 * Highlight same item pins with low stock.
	 */
	function prepStock()
	{
		
		var ignore = ['COLLECT MATTRESS 5FT','4FT-SMALLFOAM','4FT6-MEM','4FT6-FOAM','4FT6-SPRING','5FT-FOAM','5FT-MEM','5FT-SPRING','4FT-FOAM','4FT-SPRING','DELIVERY CHARGE','MISSED PARTS'];
	
		$.each(stock,function(key,value){	
			//split items //112-5FT-BLK:1,5FT-FOAM:1
			var order_items = [key];//data.items.split(",");
	
			for(var z=0;z<order_items.length;z++)
			{
				//split item //112-5FT-BLK:1
				var order_item = order_items[z].split(":");
				
				console.log(order_item[0] + ' deliveries:' + quantities[order_item[0]] + ' in stock:' + stock[order_item[0]]);
				
				if($.inArray(order_item[0],ignore)  !== -1 )
				{
					
				//	console.log('skipped '+order_item[0]);
				//	console.log(order_items);
					continue;
				}
				else if(stock[order_item[0]] < 1 )
				{
					console.log('No Stock!');
					notify("Highlighted Deliveries Out of Stock!!"); 
					highlightMarkers(order_item[0]);
				}
				else if( quantities[order_item[0]] > stock[order_item[0]] ) 
				{
					notify("Highlighted Deliveries have LOW Stock!!"); 
					highlightMarkers(order_item[0]);
				}
				else
				{
					console.log('sorted');	
				}
			}
			
		});

	}

</script>



@endsection
