@extends('layouts.app')
@section('title')
Messaging
@endsection
@section('content')
<div class="row">
  <div class="col-sm-2">
  
  <div class="row">
   <button class="btn btn-info" onclick="composeMessage()">Compose <i class="fa fa-pencil-square-o" aria-hidden="true"></i></button>
  
  	<div class="list-group">
      <a class="list-group-item">Inbox&nbsp;<span class="badge" id="mailbox-count">{{ $totalCount or '?' }}</span></a>
      <a class="list-group-item">Sent</a> 
    </div>
  </div>
  
    
  </div>
  <div class="col-sm-10">
    <div class="card card-info">
      <div class="card-header blue">
       
          <div class="col-md-6" style="padding:5px;">
            @include('share.search-bar', [
              'options' => [
                ['value' => 'name', 'title' => 'Name'],
                ['value' => 'name', 'title' => 'Message'],
              ],
              'limits' => [15,25,50],
              'selectable' => true,
              'selectControls' => false,
              'showCounts' => false
            ])
          </div>
       
      </div>
      <div class="card-body">
        <div class="row">
          <div class="col-md-12">
            <div class="table-responsive">
              <table class="small table table-striped table-no-wrap table-hover" id="table-results-container">
                <thead>
                  <tr>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                </thead>
                <tbody class="table-clickable" id="result-container">
                </tbody>
              </table>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12 text-center" id="paging-controls-container">
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script>
var addressBook = {!! $addressBook !!}
</script>
@endsection
@push('js')
<script type="text/javascript" src="{{ URL::asset('js/library/table-results_0.2.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/message/index.js') }}"></script>
@endpush
