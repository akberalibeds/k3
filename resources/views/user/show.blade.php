@extends('layouts.app')
@section('title')
Profile
@endsection
@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="card card-info">
      <div class=" card-header blue">
        <div class="card-title">
          <div class="title">
            Profile
          </div>
        </div>
      </div>
      <div class="card-body">
        <div class="row">
          <div class="col-md-6">
            <dl class="dl-horizontal">
              <dt>Name<dt>
              <dd id="user-display-name">{{ $user->name }}</dd>
              <dt>Username<dt>
              <dd>{{ $user->email }}</dd>
              <dt>Roles</td>
              <dd>
                @forelse ($userRoles as $role)
                <mark class="bg-success">{{ $role->display_name }}</mark>
                @empty
                <p>(none)</p>
                @endforelse
              </dd>
            </dl>
          </div>
        </div>
        <div class="row">
          <button class="btn btn-default collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#change-password-collapse" aria-expanded="true" aria-controls="change-password-collapse">
            Change Password
          </button>
          <button class="btn btn-default collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#change-name-collapse" aria-expanded="false" aria-controls="change-name-collapse">
            Change Display Name
          </button>
          <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
            <div class="panel panel-default">
              <div id="change-password-collapse" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                <div class="panel-body">
                  <p>This is the password for your user account</p>
                  <p>You are responsible for the actions performed with your user account.</p>
                  @include ('share.password-policy')

                  <div class="form-horizontal" id="cp-form">
                    <div class="form-group">
                      {!! Form::label('old_password', 'Current Password') !!}
                      {!! Form::password('old_password', ['required']) !!}
                      {!! Form::error('old_password') !!}
                    </div>
                    <div class="form-group" id="new-password-container">
                      {!! Form::label('new_password', 'New Password') !!}
                      {!! Form::password('new_password', ['required']) !!}
                      {!! Form::error('new_password') !!}
                    </div>
                    <div class="form-group">
                      {!! Form::label('new_password_confirmation', 'Confirm New Password') !!}
                      {!! Form::password('new_password_confirmation', ['required']) !!}
                      {!! Form::error('new_password_confirmation') !!}
                    </div>
                    <div class="form-group">
                      <div class="col-md-6">
                        <div class="text-right">
                          <input type="button" class="btn btn-default" data-toggle="collapse" data-target="#change-password-collapse" aria-expanded="false" value="Cancel"/>
                          <button type="submit" class="btn btn-primary" onclick="changePassword()">Change Password</button>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="panel panel-default">
              <div id="change-name-collapse" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                <div class="panel-body">
                  <div class="form-horizontal" id="name-form">
                    <p>This is the name shown on the top left of the page (not the name in brackets - that is you username and cannot be changed)</p>
                    <br>
                    <div class="form-group">
                      {!! Form::label('name-change', 'Name') !!}
                      {!! Form::text('name-change', Auth::user()->name, ['required']) !!}
                      {!! Form::error('name-change') !!}
                    </div>
                    <div class="form-group">
                      <div class="col-md-6">
                        <div class="text-right">
                          <input type="button" class="btn btn-default" data-toggle="collapse" data-target="#change-name-collapse" aria-expanded="false" value="Cancel"/>
                          <button type="submit" class="btn btn-primary" onclick="changeName()">Change Name</button>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="card-footer">
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
@push('headlinks')
<link rel="stylesheet" href="{{ URL::asset('css/strength.css') }}" rel="stylesheet">
@endpush
@push('js')
<script type="text/javascript" src="{{ URL::asset('js/users/show.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/library/strength.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/library/password-strength.js') }}"></script>
@endpush
