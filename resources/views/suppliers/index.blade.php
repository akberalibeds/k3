@extends('layouts.app')
@section('title')
Suppliers
@endsection
@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="card card-info">
      <div class="card-header blue">
      <div class="pull-right">
      	<button onClick="window.open('/order-import');"class="btn btn-primary">Upload Tracking</button>
      </div>
        <div class="card-title">
          <div class="title">Suppliers</div>
        </div>
      </div>
      <div class="card-body">
        <div class="row">
          <div class="col-md-6 col-sm-12 col-xs-12">
            @include('share.search-bar', [
              'options' => [
                ['value' => 'supplier_name', 'title' => 'Name'],
              ],
              'selectable' => false,
              'selectControls' => false,
            ])
          </div>
        </div>
        <div class="table-responsive">
          <table class="small table table-striped table-no-wrap table-hover" id="table-results-container">
            <thead>
              <tr>
                <th>Name</th>
                <th class="text-right">Days</th>
              </tr>
            </thead>
            <tbody class="table-clickable" id="result-container">
            </tbody>
          </table>
        </div>
      </div>
      <div class="card-footer">
        <div class="row">
          <div class="col-md-12 text-center" id="paging-controls-container">
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
@section('js')
<script type="text/javascript" src="{{ URL::asset('js/library/table-results_0.2.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/suppliers/index.js') }}"></script>
@endsection
