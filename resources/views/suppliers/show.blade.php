@extends('layouts.app')

@section('content')
<div class="row">
  <div class="card card-info">
    <div class="card-header blue">
      <div class="card-title">
        <div class="title">Supplier - {{ $supplier->supplier_name }}</div>
      </div>
    </div>
    <div class="card-body">
      <div class="row">
        <div class="col-md-12">
          <dl class="dl-horizontal">
            <dt>Id</dt>
            <dd>{{ $supplier->id }}</dd>
            <dt>Name</dt>
            <dd>{{ $supplier->supplier_name }}</dd>
            <dt>Days</dt>
            <dd>
              {{ $supplier->days }}</a>
            </dd>
          </dl>
        </div>
      </div>
    </div>
    <div class="card-footer">
    </div>
  </div>
</div>
@endsection
