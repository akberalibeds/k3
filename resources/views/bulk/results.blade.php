@section('show-failures')
<table class="failures">
    @foreach($action->getFailures() as $failure)
    <tr>
        <td>{{$failure->message}}</td>  
    </tr>
    @endforeach
</table>
@endsection

<div class='title'>
@if ($action->testMode())
  @yield('title-test',$__env->yieldContent('title'))
@else
  @yield('title')
@endif
</div>
<div class='content'>
@if ($action->testMode())
  @if ($action->success())
    @yield('test-success-messsage')
  @else
    @yield('test-failure-message')
  @endif
  @if ($action->testMode() && isset($apply_route))
  <form action="{{$apply_route}}" method="post">
     {{csrf_field()}}
    <input type="hidden" name="sessionid" value="{{$action->sessionid()}}"/>
    <div class='buttons'>
        <button type="submit"  class="btn btn-primary">Apply</button>
    </div>
  </form>
    @yield('show-failures')
  @endif
@else
  @if ($action->success())
    @yield('success-message')
  @else
    @yield('failure-message')
    @yield('show-failures')
  @endif
@endif
</div>

