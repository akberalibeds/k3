@extends('layouts.app')
@section('content')
    <div class="row">
        <div class="col-md-12">

        <input id="datePicker" type="date" value="{{ (new \DateTime($date))->format('Y-m-d') }}" >

        <button class="btn btn-primary" onclick="getOrders({{$route->id}})" >Change date</button>

        @if(count($data))
        <h1>Orders associated with {{ $route->name }} for {{ $date }}</h1>
        
        <button class="btn btn-primary" onclick="markAllDelivered()" >Mark all as delivered</button>
        <button class="btn btn-primary" onclick="markAllAsAllocated()" >Mark all as allocated</button>
        <button class="btn btn-primary" onclick="reset()" >Reset</button>
        
        <hr />

        <form action="/review/routes" method="POST" name="routesForm">
         {{ csrf_field() }}

                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>Num</th>
                            <th>Order ID</th>
                            <th>Name</th>
                            <th>Postcode</th>
                            <th>Status</th>
                            <th>Mark as Delivered</th>
                            <th>Mark as allocated</th>
                            <th>Notes</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($data as $order)
                                <tr>
                                <td> {{ $order->delOrder }} </td>
                                    <td>
                                        <a target="_blank" href="/orders/{{$order->oid}}"><u>{{$order->oid}}<u></a>
                                    </td>

                                    <td>{{$order->dBusinessName}}</td>

                                    <td>{{$order->dPostcode}}</td>

                                    <td>{{$order->orderStatus}}</td>

                                    <td> <input type="radio"  class="yes" value="Yes" name="deliveredStatus[{{$order->oid}}]" ></td>

                                    <td> <input type="radio"  class="no" value="No" name="deliveredStatus[{{$order->oid}}]" ></td>

                                    <td> <input type="text" name="notes[{{$order->oid}}]" > </td>
                                </tr>
                        @endforeach
                    </tbody>
                </table>

            <input class="btn btn-primary" type="submit" value="submit" data-toggle="modal" data-target="#processingModal"/>
        </form>
        @else
        <h1>No dispatched orders associated with {{ $route->name }} found for {{ $date }}</h1>
        @endif
        </div>
    </div>

    <!-- Processing Modal -->
    <div class="modal fade" id="processingModal" role="dialog">
        <div class="modal-dialog">
        
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Processing ...</h4>
            </div>
            <div class="modal-body">
                <p>Please wait ... </p>
            </div>
        </div>
        </div>
    </div>

    <script>
        function getOrders(routeId) {
           var date = document.getElementById("datePicker").value;

           if(!date) {
               alert('Please select a date!');
           }

           window.location.href = '/review/routes/' + routeId + '/orders/' + date;
        }

        function markAllDelivered() {

            var yes =  document.getElementsByClassName("yes");

            for (var i =0; i < yes.length; i++) 
            {
                yes[i].checked = "checked";
            }
        }

         function markAllAsAllocated() {

            var no =  document.getElementsByClassName("no");

            for (var i =0; i < no.length; i++) 
            {
                no[i].checked = "checked";
            }
        }

        function reset() {
            var yes =  document.getElementsByClassName("yes");
            var no =  document.getElementsByClassName("no");

             for (var i =0; i < no.length; i++) 
            {
                no[i].checked = "";
                yes[i].checked = "";
            }
        }

    </script>

@endsection