@extends('layouts.app')
@section('css')
<style>
    .completed, .completed a {
        background-color:#88c425;
        color:white;
        text-decoration:none;
    }

    .partial, .partial a{
        background-color:#ea8f08;
        color:white;
    }

    .none, .none a {
        background-color:#d24242;
        color:white;
    }
    table td {
        padding: 0;
    }
    table a {
        padding: 8px;
        display: block;
    }
</style>
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
        <h1>Routes</h1>

        <input id="datePicker" type="date" value="{{ (new \DateTime($date))->format('Y-m-d') }}" >

        <button class="btn btn-primary" onclick="getRoutes()" >Change date</button>

            <table class="table">
                <thead>
                <tr>
                    <th>Route</th>
                    <th>Deliveries</th>
                    <th>view orders</th>
                </tr>
                </thead>
                <tbody>
                @foreach($routes as $route)
                <tr class="{{ $route->getCompletion($date) }}">
                    <td> 
                        <a href="/review/routes/{{$route->id}}/orders/{{ (new \DateTime($date))->format('Y-m-d') }}">{{$route->name}}</a>
                    </td>
                    <td> 
                     <a href="/review/routes/{{$route->id}}/orders/{{ (new \DateTime($date))->format('Y-m-d') }}">{{  $route->getDispatchedCount($date)  }} </a> 
                     </td>
                    <td>
                        <a href="/review/routes/{{$route->id}}/orders/{{ (new \DateTime($date))->format('Y-m-d') }}">&rarr;</a>
                    </td>
                </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>

      <script>
        function getRoutes() {
           var date = document.getElementById("datePicker").value;

           if(!date) {
               alert('Please select a date!');
           }

           window.location.href = '/review/routes?date=' + date;
        }

    </script>

@endsection