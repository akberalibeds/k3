@extends('layouts.app')
@section('title')
Sellers - Imports - {{ $profileName }}
@endsection
@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="card card-info">
      <div class=" card-header blue">
        <div class="card-title">
          <div class="title">Sellers - Imports - {{ $profileName }}</div>
        </div>
      </div>
      <div class="card-body">
        <div class="row" id="upload-container">
          <div class="col-md-12">
            <div class="file-drop-area" id="upload-drop" data-upload-identifier="excel"></div>
            <div id="upload-messages"><p></p></div>
            <div class="progress">
              <div class="progress-bar" id="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width:0%"></div>
            </div>
            <div class="text-center pull-right">
              <button class="btn btn-primary" onclick="uploadFile(event)">Read</button>
            </div>
          </div>
        </div>
        <div class="row" id="csv-import-container">
          <div class="col-md-12 text-right">
            <button class="btn btn-default" id="reset-button" onclick="csvViewer.resetView()">Reset</button>
            <button class="btn btn-default" id="import-button" onclick="csvViewer.sendCsv()">Import</button>
            <hr>
          </div>
        </div>
        <div class="row">
           <div class="col-md-12">
             <div class="form-inline" id="singles-container"></div>
             <hr>
           </div>
        </div>
        <div class="row">
          <div class="col-md-12">
            <strong>Column Headings</strong>&nbsp;
            <span id="headings-container"></span>
            <p class="help-block">Drag and drop the headings to the table columns below.</p>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12">
            <div class="table-responsive">
              <table class="small table table-bordered table-striped table-no-wrap table-hover" id="csv-view-table">
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
@push('js')
<script>
var profile = '{{ $profile }}'
</script>
@endpush
@section('js')
<script type="text/javascript" src="{{ URL::asset('js/library/upload.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/library/csv-viewer.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/order-import/index.js') }}"></script>
@endsection
