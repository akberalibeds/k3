@extends('layouts.app')
@section('title')
Seller Imports
@endsection
@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="card card-info">
      <div class=" card-header blue">
        <div class="card-title">
          <div class="title">Seller Imports</div>
        </div>
      </div>
      <div class="card-body">
        <div class="row">
          <div class="col-md-12">
            <a class="btn btn-default btn-xlarge" href="{{ route('order-import::create', ['profile' => 'ajfoams-tracking']) }}">AJ Foams</a>
            <a class="btn btn-default btn-xlarge" href="{{ route('order-import::create', ['profile' => 'bedtastic-tracking']) }}">Bedtastic</a>
            <a class="btn btn-default btn-xlarge" href="{{ route('order-import::create', ['profile' => 'mighty-deals-orders']) }}">Mighty Deals</a>
          </div>
        </div>
      </div>
      <div class="card-footer">
      </div>
    </div>
  </div>
</div>
@endsection
