@extends('layouts.app')

@section('title')
Supplier Route Days - {{ $supplier->supplier_name }}
@endsection

@section('content')
<div class="row">
  <div class="card card-info">
    <div class="card-header blue">
      <div class="card-title">
        <div class="title">Route Days - {{ $supplier->supplier_name }}</div>
      </div>
    </div>
    <div class="card-body">
      <div class="row">
        <div class="col-md-12">
          <div class="table-responsive">
            <table class="small table table-condensed table-striped table-hover table-no-wrap" id="table-results-container">
              <thead>
                <th>Route</th>
                <th class="text-right">Max</th>
                <th class="text-right">Vans</th>
                <th class="text-center">Monday</th>
                <th class="text-center">Tuesday</th>
                <th class="text-center">Wednesday</th>
                <th class="text-center">Thursday</th>
                <th class="text-center">Friday</th>
                <th class="text-center">Saturday</th>
                <th class="text-center">Sunday</th>
              </thead>
              <tbody id="route-days-table">
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
    <div class="card-footer">
      <div class="row">
        <div class="col-md-12 text-center" id="paging-controls">
        </div>
      </div>
    </div>
  </div>
</div>
@endsection

@section('js')
<script>
var supplierId = {{ $supplier->id }}
</script>
<script type="text/javascript" src="{{ URL::asset('js/library/typeahead.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/library/typeahead-kit.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/library/paging.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/supplier-route-days/index.js') }}"></script>
@endsection
