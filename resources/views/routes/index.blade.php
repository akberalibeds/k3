@extends('layouts.app')
@section('title')
Routes
@endsection
@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="card card-info">
      <div class=" card-header blue">
        <div class="card-title">
          <div class="title">Routes</div>
        </div>
      </div>
      <div class="card-body">
        <div class="row">
          <div class="col-md-6 col-sm-12 col-xs-12">
            @include('share.search-bar', [
              'options' => [
                ['value' => 'name', 'title' => 'Name'],
              ],
              'selectable' => false
            ])
          </div>
        </div>
        <div class="row">
          <div class="col-md-12">
            <div class="table-responsive">
              <table class="small table table-striped table-no-wrap table-hover" id="table-results-container">
                <thead>
                  <tr>
                    <th>Name</th>
                    <th class="text-right">Deliveries</th>
                    <th class="text-right">Vans</th>
                    <th class="text-center">Can Send Message</th>
                    <th class="text-center">Is Wholesale</th>
                    <th class="text-center">Is in Load Count</th>
                    <th class="text-center">Is in &apos;Route&apos; route</th>
                  </tr>
                </thead>
                <tbody class="table-clickable" id="result-container">
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
      <div class="card-footer">
        <div class="row">
          <div class="col-md-12 text-center" id="paging-controls-container">
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
@section('js')
<script type="text/javascript" src="{{ URL::asset('js/library/table-results_0.2.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/routes/index.js') }}"></script>
@endsection
