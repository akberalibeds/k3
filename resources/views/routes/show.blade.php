@extends('layouts.app')

@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="card card-info">
      <div class=" card-header blue">
        <div class="card-title">
          <div class="title">
            Delivery Route
          </div>
        </div>
      </div>
      <div class="card-body">
        <div class="row">
          <div class="col-md-5">
            <dl class="dl-horizontal">
              <dt>ID</dt>
              <dd>{{ $route->id }}</dd>
              <dt>Name</dt>
              <dd>{{ $route->name }}</dd>
              <dt>Deliveries</dt>
              <dd>{{ $route->max }}</dd>
              <dt>Vans</dt>
              <dd>{{ $route->van }}</dd>
              <dt>Can send messages</dt>
              <dd>@yesOrNo($route->msg)</dd>
              <dt>Is Wholesale</dt>
              <dd>@yesOrNo($route->is_wholesale)</dd>
              <dt>Is in Load Count</dt>
              <dd>@yesOrNo($route->in_load_count)</dd>
              <dt>is &apos;Route&apos; route</dt>
              <dd>@yesOrNo($route->is_route_route)</dd>
            </dl>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-md-12">
    <div class="card card-info">
      <div class=" card-header blue">
        <div class="card-title">
          <div class="title">
            Orders
          </div>
        </div>
      </div>
      <div class="card-body">
        <div class="row">
          <div class="col-md-4 col-sm-6 col-xs-12">
            @include('share.search-bar', [
              'options' => [
                ['value' => 'orders.id', 'title' => 'Order ID'],
                ['value' => 'dPostcode', 'title' => 'Postcode'],
                ['value' => 'name', 'title' => 'Route'],
              ],
              'selectable' => false,
              'selectControls' => false,
            ])
          </div>
        </div>
        <div class="table-responsive">
          <table id="orders-table" class="small table table-striped table-no-wrap table-clickable table-hover">
            <thead>
              <tr>
                <th>Order</th>
                <th>Invoice</th>
                <th>Buyer</th>
                <th>Customer</th>
                <th>Postcode</th>
                <th>Ref</th>
                <th>Type</th>
                <th>Status</th>
                <th>Date</th>
                <th class="text-right">Total</th>
                <th class="text-right">Paid</th>
                <th class="text-right">Balance</th>
                <th class="text-center">Done</th>
              </tr>
            </thead>
            <tbody id="result-container">
            </tbody>
          </table>
        </div>
      </div>
      <div class="card-footer">
        <div class="row">
          <div class="col-md-12 text-center" id="paging-controls-container">
          </div>
        </div>
      </div>
      </div>
    </div>
  </div>
</div>
@endsection
@section('js')
<script>
var routeId = {{ $route->id }}
</script>
<script type="text/javascript" src="{{ URL::asset('js/library/table-results.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/routes/show.js') }}"></script>
@endsection
