@extends('layouts.app')
@section('title')
Load Counts
@endsection
@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="card card-info">
      <div class=" card-header blue">
        <div class="card-title">
          <div class="title">Load Counts</div>
        </div>
      </div>
      <div class="card-body">
        <div class="row">
          <div class="form-group">
            <div class="col-md-4">
              <label for="warehouse">Warehouse Location:</label>
            </div>
            <div class="col-md-4">
              <select class="form-control" name="warehouse" id="warehouse">
                  <option value="all">All Warehouses</option>
                  <option value="per">Per Warehouse</option>
                  @foreach ($warehouses as $warehouse)
                      <option value="{{$warehouse->name}}">{{ $warehouse->location }}</option>
                  @endforeach
              </select>
            </div>
          </div>
          <br><br>
          <div class="form-group">
            <div class="col-md-4">
              <label for="load-counts-date">Date: </label>
            </div>
            <div class="col-md-4">
              <input class="form-control" type="date" name="load-counts-date" id="load-counts-date" min="{{ \Carbon\Carbon::today()->subWeek()->toDateString() }}" max="{{ \Carbon\Carbon::today()->addWeek()->toDateString() }}">
              <!-- <input class="form-control" type="date" name="load-count-date" id="load-counts-date" min="{{ \Carbon\Carbon::today()->subWeek()->toDateString() }}" max="{{ \Carbon\Carbon::today()->addWeek()->toDateString() }}"> -->
            </div>
          </div>
          <br><br>
          <div class="form-group">
            <div class="col-md-4">
              <label for="load-counts-type">Type: </label>
            </div>
            <div class="col-md-4">
              <select class="form-control" name="load-counts-type" id="load-counts-type">
                  <option value="1">Day Load Counts</option>
                  <option value="2">Morning Load Counts</option>
                  <option value="3">Missed</option>
              </select>
            </div>
          </div>
          <br><br>
          <div class="form-group">
            <div class="col-md-4">
              <label for="stock-part">Optional: Show Stock Parts</label>
            </div>
            <div class="col-md 4 checkbox-inline">
              <input type="checkbox" name="stock-parts" id="stock-parts" value="checked" style="margin-left: 0px">
            </div>
          </div>
        <br><br>
        <div class="form-group">
          <div class="col-md-4">
            <label for="print-size-select">Select Print Size</label>
          </div> 
          <div class="col-md-4">
                <select class="form-control" id="print-size-select" name="print-size-select">
                    <option selected value="n">Normal</option>
                    <option selected value="b">Bigger</option>
                </select>
          </div>
        </div>
        <br><br>
        <div class="form-group">
            <div class="col-md-4 col-sm-12 btn-group btn-group-justified">
                <a class="btn btn-primary" onclick="csvLoadCounts(event)">Export as CSV</a>
                <a class="btn btn-primary" onclick="htmlLoadCounts(event)">Show on Webpage</a>
                <!-- <a class="btn btn-primary disabled" onclick="printLoadCounts(event)">Export as PDF Print</a> -->
                <!-- <a class="btn btn-primary" onclick="alert('Currently Unavaliable due to Error Logs')">Export as PDF Print</a> -->
            </div>
        </div>
        </div>
      </div> 
    </div>
  </div>
</div>
@endsection
@push('js')
<script type="text/javascript" src="{{ URL::asset('js/load-counts/index.js') }}"></script>
<script>
  (function($){
      $("#load-counts-type").on("change", function() {
      if ($(this).val() === "2") {
          $("#check1").attr("disabled", "disabled");
      } else {
          $("#check1").removeAttr("disabled");
      }
    });
  });(jQuery)
</script>
@endpush
