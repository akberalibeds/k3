@extends('layouts.app')
@section('title')
Load Counts
@endsection
@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="card card-info">
      <div class=" card-header blue">
        <div class="card-title">
          <div class="title">Load Counts</div>
        </div>
      </div>
      <div class="card-body">
        <div class="row">
          <div class="form-horizontal">
            <div class="form-group">
              {!! Form::label('warehouse', 'Warehouse') !!}
              {!! Form::select('warehouse', $warehouses, null, ['data-container' => 'body'], [4, 10]) !!}
            </div>
            <div class="row">
              <div class="col-md-6 text-right">
                (Enter the date of the Day of dispatch)
              </div>
            </div>
            <div class="form-group">
              {!! Form::label('load-counts-date', 'Date') !!}
              {!! Form::calendar('load-counts-date', null, [], [4, 10]) !!}
            </div>
            <div class="form-group">
              {!! Form::label('load-counts-type', 'Type') !!}
              {!! Form::select('load-counts-type', ['1' => 'Day Load Counts', '2' => 'Morning Load Counts', '3' => 'Missed'], null, ['data-container' => 'body'], [4, 10]) !!}
            </div>
            <div class="col-md-6 col-sm-12 text-right">
              <button class="btn btn-default" onclick="csvLoadCounts(event)">Export as CSV</button><br>
              <button class="btn btn-default" onclick="printLoadCounts(event)">Export as Print</button>
              <select class="selectpicker" data-container="body" id="print-size-select" name="print-size-select">
                <option selected value="n">Normal</option>
                <option selected value="b">Bigger</option>
              </select>
              <br>
            </div>
          </div>
      </div>
    </div>
  </div>
</div>
@endsection
@push('js')
<script type="text/javascript" src="{{ URL::asset('js/load-counts/index.js') }}"></script>
@endpush
