@extends('layouts.app')
@section('title')
    @if($type == '1')
        @if($warehouse != 'all')
            Day Load Counts for Warehouse {{ $warehouse }}
        @elseif($warehouse == 'per')
            Day Load Counts for each Warehouse
        @else
            Day Load Counts for {{ $warehouse }} Warehouses
        @endif
    @elseif($type == '2')
        @if($warehouse != 'all')
            Morning Load Count for Warehouse {{ $warehouse }}
        @elseif($warehouse == 'per')
            Morning Load Count for each Warehouse
        @else
            Morning Load Count for {{ $warehouse }} Warehouses
        @endif
    @else
    @if($warehouse != 'all')
            Missing Load Count for Warehouse {{ $warehouse }}
        @elseif($warehouse == 'per')
            Missing Load Count for each Warehouse
        @else
            Missing Load Count for {{ $warehouse }} Warehouses
        @endif
    @endif
@endsection
@section('content')
    <div class="row">
        <div class="col-sm-12">
            <a href="{{ url('/load-counts') }}">
                <button class="btn btn-danger btn-block" style="justify-items:center;align-items:center">
                    <i class="fa fa-arrow-left"></i> Back
                </button>
            </a>
        </div>

        <div class="col-md-12">
            <h1>
                @if ($type == '1')
                    Day Load Count for 
                    @if ($warehouse != 'all'  && $warehouse != 'per')
                        {{ $warehouseName->location }}
                    @elseif($warehouse == 'per')
                        Each Warehouse
                    @else
                        All Warehouses
                    @endif
                @elseif ($type == '2')
                    Morning Load Count for
                    @if ($warehouse != 'all'  && $warehouse != 'per')
                        {{ $warehouseName->location }}
                    @elseif($warehouse == 'per')
                        Each Warehouse
                    @else
                        All Warehouses
                    @endif
                @elseif ($type == '3')
                    Missing Load Count for 
                    @if($warehouse != 'all' && $warehouse != 'per')
                        {{ $warehouseName->location }}
                    @elseif($warehouse == 'per')
                        Each Warehouse
                    @else
                        All Warehouses
                    @endif
                @else
                    This page isn't avaliable to view
                @endif
            </h1>
            @if ($parts == 'true')
                <h4>Stock Parts Included</h4>
            @endif
            <br>
            <br>
            <div class="table-responsive">
                <table class="table">
                    <thead>
                        <tr>
                            @foreach($tableheaders as $header => $value)
                                <th>{{ $value }}</th>
                            @endforeach
                        </tr>
                    </thead>
                    <tbody>
                        @forelse ($counts as $count)
                            <tr>
                                @if($parts == 'true')
                                    <td>{{ $count->itemCode }}</td>
                                    <td>{{ $count->id }}</td>
                                    <td>{{ $count->part }}</td>
                                    <td>{{ $count->tot }}</td>
                                    @if($warehouse != 'all' && $warehouse == 'per')
                                        <td>{{$count->warehouse}}</td>
                                    @endif
                                    @if($type == '3')
                                        <td>{{ $count->itemCode }}</td>
                                        <td>{{ $count->itemCode }}</td>
                                    @endif
                                @else
                                    <td>{{ $count->itemCode }}</td>
                                    @if($warehouse != 'all' && $warehouse == 'per')
                                        <td>{{$count->warehouse}}</td>
                                    @endif
                                    <td>{{ $count->tot }}</td>
                                    @if($type == '3')
                                        <td>{{ $count->itemCode }}</td>
                                        <td>{{ $count->itemCode }}</td>
                                    @endif
                                @endif
                            </tr>
                        @empty
                            <h1 style="text-align:center; font-weight:bold;">NO DATA AVAILABLE</h1>
                        @endforelse
                    </tbody>
                </table>
            </div>
            <div class="text-center">
                {{ $counts->appends(request()->except('page'))->links() }}
            </div>
        </div>
        </div>
    </div>
@endsection