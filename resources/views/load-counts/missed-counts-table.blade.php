@extends('layouts.app')
@section('title')
{{ $title }}
@endsection
@section('content')
<div class="row">
  <div class="col-md-6">
    <div class="card card-info">
      <div class=" card-header blue">
        <div class="card-title">
          <div class="title">{{ $title }}</div>
        </div>
      </div>
      <div class="card-body">
        <div class="table-responsive">
          <table class="table table-bordered table-striped">
            <thead>
              <tr>
                <th>Item Code</th>
                <th>Order</th>
              </tr>
            </thead>
            <tbody>
              @foreach ($loadCounts as $loadCount)
              <tr>
                <td>{{ $loadCount->notes  }}</td>
                <td class="text-right" style="width:5em">{{ $loadCount->oid }}</td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
