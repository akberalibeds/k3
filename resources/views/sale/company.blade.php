@extends('layouts.app')

@section('content')


<div class="row">


@foreach($companies as $c)

	<div class="col-md-2">
    		
            <a class="btn btn-block btn-info" href="/sale/{{ $c->id }}">{{ $c->company_name }}</a>
            
    </div>
    
@endforeach

</div>



@endsection
