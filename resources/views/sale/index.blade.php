@extends('layouts.app')
@section('content')
<style>
.input-group-typeahead{
  width:100%;
}
.input-group-typeahead input{
  height:48px;
  font:'Lato';
  font-size:16px;
}
</style>
<div class="row">
  <div class="col-md-2">
    <div class="panel panel-default">
      <div class="panel-body">
        <h2 class="">New Sale</h2>
        <hr />
        <h3 class="panel-title" style="width:100%;">Sub Total: <span class="orderSubTotal">0.00</span></h3>
        <h3 class="panel-title" style="width:100%;">VAT (<?php echo $SALE['vatAmount'] ?>%): <span class="orderVAT"></span></h3>
        <h3>Total: <strong><span class="orderTotal">0.00</span></strong></h3>
      </div>
    </div>
    <div class="panel panel-default">
      <div class="panel-body">
        <button class="btn btn-success btn-block completeOrder">Complete Order</button>
        <button class="btn btn-danger btn-block cancel"><i class="fa fa-trash-o"></i> Cancel Order</button>
        <button class="btn btn-primary btn-block" onclick="changePrice();">Change Price</button>
      </div>
    </div>
  </div>
  <div class="col-md-7">
    <div class="panel panel-default">
      <div class="panel-body">
        <div class="form-group">
          <div class="input-group input-group-typeahead" id="scrollable-dropdown-menu">
            <input autocomplete="off" class="form-control" id="items" placeholder="Search Stock" />
            <div id="search-spinner-items" style="width:80px">
              <div class="rect1"></div>
              <div class="rect2"></div>
              <div class="rect3"></div>
              <div class="rect4"></div>
            </div>
          </div>
        </div>
        <div class="list-group order_items">
          <div class="list-group-item info">
            <div class="row">
              <div class="col-md-4">
                <h4 class="list-group-item-heading">Item Code</h4>
              </div>
              <div class="col-md-2">
                <p class="list-group-item-text">Qty</p>
              </div>
              <?php
              /*
              <div class="col-md-1">
                <p class="list-group-item-text">Disc%</p>
              </div>
              */
              ?>
              <div class="col-md-2">
                <p class="list-group-item-text">Price</p>
              </div>
              <div class="col-md-2">
                <p class="list-group-item-text">discount</p>
              </div>
              <div class="col-md-1">
                <p class="list-group-item-text">Stock</p>
              </div>
              <div class="col-md-1">
                <p class="list-group-item-text">Total</p>
              </div>
            </div>
          </div>
          <?php
          if (isset($SALE['items'])) {
            foreach($SALE['items'] as $item) {
              //echo NewSale::getRow($item);
            }
          }
          ?>
        </div>
      </div>
    </div>
  </div>
  <div class="col-md-3">
    <div class="panel panel-default">
      <div class="panel-body">
        <div class="form-group selectCustomer">
          <div class="input-group input-group-typeahead">
            <input autocomplete="off" type="text" class="form-control" id="customers" placeholder="name / postcode"/>
          </div>
          <div id="search-spinner-customer" style="width:80px">
            <div class="rect1"></div>
            <div class="rect2"></div>
            <div class="rect3"></div>
            <div class="rect4"></div>
          </div>
          <br><br>
          <div class="col-md-2" align="center"></div>
          <div class="col-md-8" align="center">
            <button class="btn btn-success btn-block newCust" data-toggle="modal" data-target="#modal-large"><i class="fa fa-plus-square"></i> New Customer</button>
          </div>
          <div class="col-md-2" align="center"></div>
        </div>

        <div class="bd-blue showCustomer">
        </div>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="modal-large" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel"><strong>New Customer</strong></h4>
      </div>
      <div class="modal-body newCustomer">
        <div class="row col-md-6">
          <legend>Invoice Address</legend>
          <div class="form-group col-md-8">
            <label for="name">Name</label>
            <input class="form-control copy" id="name" row="businessName" />
          </div>
          <div class="clearfix"></div>
          <div class="form-group col-md-4">
            <label for="number">No</label>
            <input class="form-control copy" id="number" row="number" />
          </div>
          <div class="form-group col-md-8">
            <label for="street">Street</label>
            <input class="form-control copy" id="street" row="street" />
          </div>
          <div class="form-group col-md-7">
            <label for="town">Town</label>
            <input class="form-control copy" id="town" row="town" />
          </div>
          <div class="form-group col-md-5">
            <label for="name">Postcode</label>
            <input class="form-control copy" id="postcode" row="postcode" />
          </div>
        </div>
        <div class="row col-md-6 pull-right">
          <legend>Delivery Address</legend>
          <div class="form-group col-md-8">
            <label for="dBusinessName">Name</label>
            <input class="form-control paste" id="dName" row="dBusinessName" />
          </div>
          <div class="clearfix"></div>
          <div class="form-group col-md-4">
            <label for="number">No</label>
            <input class="form-control paste" id="dNumber" row="dNumber" />
          </div>
          <div class="form-group col-md-8">
            <label for="street">Street</label>
            <input class="form-control paste" id="dStreet" row="dStreet" />
          </div>
          <div class="form-group col-md-7">
            <label for="town">Town</label>
            <input class="form-control paste" id="dTown" row="dTown" />
          </div>
          <div class="form-group col-md-5">
            <label for="name">Postcode</label>
            <input class="form-control paste" id="dPostcode" row="dPostcode" />
          </div>
          <div class="form-group col-md-4">
            <button class="btn btn-info btn-sm sameAdd">Use Invoice Address</button>
          </div>
        </div>
        <div class="clearfix"></div>
        <legend>Contact Details</legend>
        <div class="form-group col-md-4">
          <label for="name">Tel</label>
          <input class="form-control" row="tel" />
        </div>
        <div class="form-group col-md-4">
          <label for="fax">Mob</label>
          <input class="form-control" row="mob" />
        </div>
        <div class="form-group col-md-4">
          <label for="fax">Fax</label>
          <input class="form-control" row="fax" />
        </div>
        <div class="form-group col-md-4">
          <label for="fax">Email 1</label>
          <input class="form-control" row="email1" />
        </div>
        <div class="form-group col-md-4">
          <label for="fax">Email 2</label>
          <input class="form-control" row="email2" />
        </div>
        <div class="form-group col-md-4">
          <label for="fax">Email 3</label>
          <input class="form-control" row="email3" />
        </div>
        <div class="clearfix"></div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default closeNC" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-success saveCustomer">Save changes</button>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="modal-complete" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel"><strong>Complete Sale</strong></h4>
      </div>
      <div class="modal-body">
        <div class="form-group col-md-6">
          <input type="hidden" value="delivery" class="delType" />
          <label class="control-label">Select Company</label>
          <select name="companyName" class="companyName form-control">
            <option value="{{ $companies->company_name }}">{{ $companies->company_name }}</option>
          </select>
        </div>
        <div class="form-group col-md-6">
          <label class="control-label">Ref</label>
          <input type="text" class="form-control details ouref" row="ouref" value="<?php echo $SALE['ouref'] ?>" />
        </div>
        <div class="form-group col-md-6 bedsdiv">
          <label class="control-label">Beds ID</label>
          <input type="text" class="form-control details beds" row="beds" value="<?php echo ($SALE['beds']=="") ? "0" : $SALE['beds']  ?>" />
        </div>
        <div class="clearfix"></div>
        <div class="form-group col-md-12">
          <label class="control-label">Payment Type</label>
          <select class="form-control paymentType">
            @foreach($paymentType as $p)
            <option value="{{ $p->payment_type }}">{{ $p->payment_type }}</option>
            @endforeach
          </select>
        </div>
        <div class="form-group col-md-6 ppdiv">
          <label class="control-label">Paypal Trans ID</label>
          <input type="text" class="form-control details last_trans_id" row="last_trans_id" value="<?php echo ($SALE['last_trans_id']=="") ? "0" :  $SALE['last_trans_id'] ?>" />
        </div>
        <div class="clearfix"></div>
        <div class="form-group col-md-6">
          <label class="control-label">Total</label>
          <input type="text" disabled="disabled" class="form-control compTotal"  />
        </div>
        <div class="form-group col-md-6">
          <label class="control-label">Owed</label>
          <input type="text" disabled="disabled" class="form-control owed"  />
        </div>
        <div class="clearfix"></div>
        <div class="form-group col-md-12">
          <label class="control-label">Paid</label>
          <input type="text"  value="<?php echo ($SALE['paid']>0) ? $SALE['paid'] : "0.00" ?>" class="form-control input-lg paid"  />
        </div>
        <div class="clearfix"></div>
        <!-- Card Details -->
        <div class="cardDetails">
          <div class="col-md-6 col-md-offset-3">
            <legend>Card Details</legend>
            <div class="form-group col-md-12">
              <label class="control-label">Card No</label>
              <input type="text"  value="" class="form-control input-sm check details cardNo" row="cardNo" value="<?php echo (isset($SALE['cardNo'])) ? $SALE['cardNo'] : "" ?>"  />
            </div>
            <div class="clearfix"></div>
            <div class="form-group col-md-6">
              <label class="control-label">Expiry (MMYY)</label>
              <input type="text"  value="" class="form-control input-sm check details exp" row="exp" value="<?php echo (isset($SALE['exp'])) ? $SALE['exp'] : "" ?>"  />
            </div>
            <div class="form-group col-md-6">
              <label class="control-label">CVC</label>
              <input type="text"  value="" class="form-control input-sm check details cvc" row="cvc" value=""  />
            </div>
            <div class="clearfix"></div>
            <div class="form-group col-md-6">
              <label class="control-label">Auth</label>
              <input type="text"  value="" class="form-control input-sm  details authNo" row="authNo" value="<?php echo (isset($SALE['auth'])) ? $SALE['auth'] : "" ?>"  />
            </div>
            
            <div class="form-group col-md-6">
              <label class="control-label">Merch Ref</label>
              <input type="text"  value="" class="form-control input-sm details merch_ref" row="merch_ref" value="<?php echo (isset($SALE['merch_ref'])) ? $SALE['merch_ref'] : "" ?>"  />
            </div>
            <div class="clearfix"></div>
          </div>
          <div class="clearfix"></div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger closeC" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-success completeSale">Complete</button>
      </div>
    </div>
  </div>
</div>
<button class="btn btn-success btn-block completeOpen"  data-toggle="modal" data-target="#modal-complete" style="visibility:hidden;">Complete Order</button>
<button class="btn btn-success btn-block cancelModalBtn"  data-toggle="modal" data-target="#modalCancel" style="visibility:hidden;" data-backdrop="static" data-keyboard="false">Complete Order</button>
<div class="modal fade" id="modalCancel" tabindex="-1" role="dialog" aria-labelledby="cancelOrder" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-body">
        <div align="center">
          <i class="fa fa-spinner fa-spin fa-2x"></i>
          <br />Cancelling Order
        </div>
      </div>
    </div>
  </div>
</div>
<button class="btn btn-success btn-block loaderBtn"  data-toggle="modal" data-target="#modalLoader" style="visibility:hidden;" data-backdrop="static" data-keyboard="false"></button>
<div class="modal fade" id="modalLoader" tabindex="-1" role="dialog" aria-labelledby="loader" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-body">
        <div align="center">
          <i class="replace_here fa fa-spinner fa-spin fa-2x"></i>
          <br />Saving Order
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
@section('js')
<script src="{{ URL::asset('js/library/typeahead.min.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('js/library/typeahead-kit.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('js/jquery.creditCardValidator.js') }}" type="text/javascript"></script>
<script>
var vatAmount = <?php echo $SALE['vatAmount'] ?>;
var customer;
var editCustomer=false;

$(window).load(function(e) {
  $('.cardDetails').hide();
  $('.ppdiv').hide();

  if ('{{ $companies->company_name }}'=='Beds.co.uk') {
    $('.bedsdiv').show();
  } else {
    $('.bedsdiv').hide();
  }
  setTimeout(function() {
    $('body').append('<style> .pca .pcaautocomplete {   z-index: 99999999; } </style>');
  },
  1000);
});

$('#items').typeaheadKit({
  display: 'itemCode',
  name: 'itemCode',
  //objectIdProperty: 'id',
  spinner: '#search-spinner-items',
  queryUrl: '/stock/search/item-code/%QUERY',
  resultsLimit: 300,
  rowClickFn: function (item) {
    item.discount=0;
    $('.order_items').append(getRow(item));
    updateOrder();
    $('#items').typeahead('val','');
  },
  rowElementFn: function (object) { }
});

$('#customers').typeaheadKit({
  display: 'result',
  minLength: 3,
  name: 'businessName',
  objectIdProperty: 'id',
  spinner: '#search-spinner-customer',
  queryUrl: '/customer/search/name/%QUERY',
  resultsLimit: 60,
  rowClickFn: function (selected_customer) {
    $.form({
      method: 'GET',
      url: '/customer/' + selected_customer.id,
      success: function(selected_customer) {
		  customer = JSON.parse(selected_customer);
        var html = setCustomer(customer);
        $('.showCustomer').html(html);
        $('.showCustomer').show();
        $('.selectCustomer').hide();
        $('#customers').val('');
        $('#customers').typeahead('val','');
      }
    });
  }
});
/*
 *
 Payment
 *
 */
var card_valid=false;

$('body').on('change','.paymentType',function(e) {
  if ($(this).val()=='Card') {
    $('.cardDetails').show();
     $('.cardNo').validateCreditCard(function(result) {
      (result.card_type == null ? '-' : result.card_type.name);
      if (result.valid && result.luhn_valid && result.length_valid) {
        card_valid = true;
      } else {
        card_valid = false;
      }
    });
  } else {
    $('.cardDetails').hide();
  }

  if ($(this).val()=='Paypal') {
    $('.ppdiv').show();
  } else{
    $('.ppdiv').hide();
  }
});
/*
 *
 Paid
 *
 */
$('body').on('blur','.paid',function(e) {
  if ($(this).val()=='') {
    $(this).val('0.00');
    $('.owed').val($('.orderTotal').html());
  }
})
$('body').on('keyup','.paid',function(e) {
  var total = parseFloat($('.orderTotal').html());
  var paid  = parseFloat($(this).val());
  var owed  = total-paid;
  $('.owed').val(owed.toFixed(2));
});
/*
 *
Update Order
 *
 */
function updateOrder() {
  var total = 0.00;
  var discount = 0 ;
  $('.order_item').each(function(index, row) {
    var lineTotal=parseInt($(row).find('.qty').val()) * parseFloat($(row).find('.price').val());
    lineTotal = parseFloat(lineTotal);
    discount = lineTotal / 100 * parseFloat($(row).find('.discount').val());
    lineTotal -= parseFloat(discount);
    total+=lineTotal*1;
    $(row).find('.total').html(lineTotal.toFixed(2));
  });
  var vat = total-(( 100 / ( 100 + vatAmount ) * total))
  var subTotal = total - vat;
  $('.orderSubTotal').html(parseFloat(subTotal).toFixed(2));
  $('.orderVAT').html(parseFloat(vat).toFixed(2));
  $('.orderTotal').html(parseFloat(total).toFixed(2));
}
/*
 *
Edit Order
 *
 */
$('body').on('blur','.edited',function(e) {
  if ($(this).val()=='') { $(this).val(0); }
  var note = "updated "+$(this).attr('row');
  var row = $(this).closest('.order_item');
  var id = row.attr('id');
  var qty = row.find('.qty').val();
  var price = row.find('.price').val();
  var discount = row.find('.discount').val();
  updateOrder();
});
/*
 *
Remove Item from Order
 *
 */
 $('body').on('click','.rem',function(e) {
  var row = $(this).closest('.order_item');
  var qty = row.find('.qty').val();
    row.remove();
    updateOrder();
});
/*
 *
 save Customer details to database
 *
 */
$('.saveCustomer').click(function(e) {
  var data = {}
  $(this).html('<i class="fa fa-spinner fa-spin"></i> Saving');
  $('.newCustomer .form-control').each(function(index, element) {
    data[$(this).attr('row')] = ($(this).val()!='') ? $(this).val() : 0;
  });
  if (editCustomer) {
    data['id']=customer.id;
  }
  data = JSON.stringify(data);
  var ed = (editCustomer) ? '&edit=1' : '';
  console.log(data);
  var url = (editCustomer) ? "/customer/"+customer.id+'/update' : "/customer/add" ;

  $.form({
    url: url,
    data: { data },
    success: function(response) {
      $('.saveCustomer').html('Save changes');
      $('.closeNC').click();
      customer = $.parseJSON(response);
      $('.showCustomer').html(setCustomer(customer));
      $('.selectCustomer').hide();
    }
  });

  //$.post("api.php","action=newCustomer&c=Customers&data="+encodeURIComponent(data)+ed,function(response) {
  //    $('.closeNC').click();
  //    getCustomer(parseInt(response));
  //});
});
/*
 *
 edit Customer details
 *
 */
$('body').on('click','.editCustomer',function(e) {
  $('.newCustomer .form-control').each(function(index, element) {
    $(element).val(customer[$(this).attr('row')]);
      });

  $('.newCust').click();
});
/*
 *
 Remove Customer from Order
 *
 */
$('body').on('click','.removeCustomer',function(e) {
  editCustomer=false;
  customer=null;
  $('.showCustomer').html('');
  $('.selectCustomer').show();

  $('.newCustomer .form-control').each(function(index, element) {
    $(element).val('');
  });
});
/*
 *
 Duplicate addresses
 *
 */
$('.sameAdd').click(function(e) {
  var copy = $('.copy');
  var paste = $('.paste');
  for (var i=0; i < copy.length; i++) {
    $(paste[i]).val($(copy[i]).val());
  }
});
/*
 *
 Complete click
 *
 */
$('body').on('click','.completeOrder',function(e) {
  var items = $('.order_item');
  if (items.length==0) {
    alert('Not Items on Order!!')
  }
  else if (customer==null) {
    alert('No Customer Added!!')
  } else {
    $('.completeOpen').click();
    $('.compTotal').val($('.orderTotal').html());
    $('.owed').val($('.orderTotal').html());
  }
});
/*
 *
 Payment
 *
 */
$('body').on('click','.completeSale',function(e) {
  if ($('.companyName').val()=='0') {
    alert('Select Company!!')
    return false;
  }
  if ($('.paymentType').val()=='0') {
    alert('Select Payment Type')
    return false;
  }
  var fals=0;
  
  if ($('.cardDetails').is(":visible")) {
    $('.cardDetails .check').each(function(index, element) {
      if ($(this).val()=='') { alert('Complete Card Details!!'); fals++; return false;  }
    });
    if (!card_valid) {
      alert('Card Details Invalid'); fals++; return false;
    }
  }

  
  if ($('.cardDetails').is(":visible")) {
	if(fals==0){
		takePayment();
		return false;
	}
  }
  
  if (fals==0) {
    var order = getOrder();
    //console.log(order);
    order = JSON.stringify(order);

    $('.loaderBtn').click();

    $.form({
      url: "/sale/new",
      data: { order },
      success: function(response) {
        if ($.isNumeric(response)) {
          $('.replace_here').removeClass('fa-spinner');
          $('.replace_here').removeClass('fa-spin');
          $('.replace_here').addClass('text-success');
          $('.replace_here').addClass('fa-check');
          window.location='/orders/'+response;
          return;
        }
        $('.completeOpen').click();
      }
    });
  }
});

function takePayment(){
	var data = getOrder();


    var dialog = bootbox.dialog({
        message: '<p class="text-center"><img src="/img/barclays.png" /><br>Processing Payment <i class="fa fa-spinner fa-spin fa-lg"></i></p>',
        closeButton: false
    });
    // do something in the background
    //

	
	
	$.form({
		method: 'POST',
		url: "/barclaycard-epdq",
		data: data,
		success: function(response){

			dialog.modal('hide');
	/*
			response.orderID; //epdq_orders ID need to update both tables with corresponding id
			response.PAYID;	// merch_ref
			response.NCSTATUS;	
			response.NCERROR; // 0 = No Error
			response.ACCEPTANCE; // authNo
			response.STATUS; // 	should be 9 ??
			response.NCERRORPLUS;	// Error Description
	*/
			if(parseInt(response.NCERROR)!=0){

				 bootbox.dialog({
				        message: '<p class="text-center"><img src="/img/barclays.png" /><br>'+response.NCERRORPLUS+'</p>',
				        closeButton: true,
				        buttons:{
							Close:function(){
				
							}
				        }
				 });
				
			}
			else{
				$('.close').click();
				
				bootbox.dialog({
			        message: '<p class="text-center"><img src="/img/barclays.png" /><br>Payment Processed<br><br>Auth: '+response.ACCEPTANCE+'<br>Ref: '+response.PAYID+'<br>Total: '+response.amount+'<br>Type: '+response.BRAND+'<br></p>',
			        closeButton: false,
			        buttons:{
						Ok:function(){
							var order = getOrder();
							order.epdqId = response.epdqId;
						    console.log(order);
						    order = JSON.stringify(order);

						    $('.loaderBtn').click();

						    $.form({
						      url: "/sale/new",
						      data: { order },
						      success: function(response) {
						        if ($.isNumeric(response)) {
						          $('.replace_here').removeClass('fa-spinner');
						          $('.replace_here').removeClass('fa-spin');
						          $('.replace_here').addClass('text-success');
						          $('.replace_here').addClass('fa-check');
						          window.location='/orders/'+response;
						          return;
						        }
						        //$('.completeOpen').click();
						      }
						    });
						}
			        }
			 	});
				$('.merch_ref').val(response.PAYID);
				$('.authNo').val(response.ACCEPTANCE);
				
			}

			
		}
	});
    	
}

function getOrder() {
  var order = {};
  order.cid = customer.id;
  order.total = $('.orderTotal').html();
  order.paid  = $('.paid').val();
  order.vatAmount = vatAmount;
  order.cardNo= $('.cardNo').val();
  order.exp = $('.exp').val();
  order.authNo = $('.authNo').val();
  order.merch_ref= $('.merch_ref').val();
  order.ouref = $('.ouref').val();
  order.companyName =  $('.companyName').val();
  order.paymentType =  $('.paymentType').val();
  order.beds = $('.beds').val();
  order.cvc = $('.cvc').val();
  order.last_trans_id = $('.last_trans_id').val();
  order.items = {};
  $('.order_item').each(function(index, element) {
    var item = $.parseJSON($(element).attr('row'));
    order.items[index] = {};
    order.items[index].itemid = item.id;
    order.items[index].price = $(element).find('.price').val();
    order.items[index].Qty = $(element).find('.qty').val();
    order.items[index].lineTotal =  $(element).find('.total').html();
    order.items[index].discount = $(element).find('.discount').val();
    order.items[index].notes = $(element).find('.note').val();
    order.items[index].currStock = item.itemQty;
  });
  return order;
}

function changePrice(){

    bootbox.prompt({
        title: "Enter Password",
        inputType: 'password',
        callback: function (result) {
            if(result && result=="money"){

            	$('.price').removeAttr('disabled');
            	$('.discount').removeAttr('disabled');

            }
            
        }
    });
	
}

function setCustomer(customer) {
  editCustomer=true;
  return  '<a ddress>'+
            '<strong>Invoice Address</strong><br>'+
            customer.businessName+'<br>'+
            customer.number+'<br>'+
            customer.street+'<br>'+
            customer.town+'<br>'+
            customer.postcode+'<br>'+
          '</address><hr />'+
          '<address>'+
            '<strong>Delivery Address</strong><br>'+
            customer.dBusinessName+'<br>'+
            customer.dNumber+'<br>'+
            customer.dStreet+'<br>'+
            customer.dTown+'<br>'+
            customer.dPostcode+'<br>'+
          '</address><hr/>'+
          '<address>'+
            '<strong>Tel</strong>: '+customer.tel+'<br>'+
            '<strong>Mob</strong>: '+customer.mob+'<br>'+
            '<strong>Fax</strong>: '+customer.fax+'<br>'+
            '<strong>Email</strong>: '+customer.email1+'<br>'+
            '<strong>Email</strong>: '+customer.email2+'<br>'+
            '<strong>Email</strong>: '+customer.email3+'<br>'+
          '</address>'+
          '<div class="col-md-2" align="center"></div>'+
          '<div class="col-md-4" align="center">'+
            '<button class="btn btn-info btn-sm btn-block editCustomer"><i class="fa fa-pencil-square-o"></i> Edit</button>'+
          '</div>'+
          '<div class="col-md-4" align="center">'+
            '<button class="btn btn-danger btn-sm btn-block removeCustomer"><i class="fa fa-trash-o"></i> Remove</button>'+
          '</div>'+
          '<div class="col-md-2" align="center"></div>';
}

function getRow(item) {
  var price;

  if ('{{ $companies->company_name }}' == 'Beds.co.uk') {
    price = item.beds_price;
    //console.log('beds');
  } else if ('{{ $companies->company_name }}' == 'IJ Interiors') {
    price = item.ebay_price;
    //console.log('ebay');
  } else {
    price = item.retail;
    //console.log('normal');
  }

  var str = '<div id="'+item.id+'" class="list-group-item order_item" row=\''+JSON.stringify(item)+'\' style=" background-color:#F5F5F5;"> '+
              '<div class="row">'+
                '<div class="col-md-4">'+
                  '<h4 class="list-group-item-heading">'+item.itemCode+'</h4>'+
                  '<p class="list-group-item-text">'+item.itemDescription+'</p>'+
                '</div>'+
                '<div class="col-md-2">'+
                  '<input class="form-control input-sm qty edited" row="qty" value="' + 1 + '" />'+
                '</div> '+
                /*
                  '<div class="col-md-1">'+
                    '<input class="form-control input-sm disc edited" value="0" disabled />'+
                  '</div> '+
                */
                '<div class="col-md-2">' +
                  '<input class="form-control input-sm price edited" row="price"  value="' + price + '" />' +
                '</div> ' +

                '<div class="col-md-2">' +
                  '<input class="form-control input-sm discount edited" row="discount"  value="' + item.discount + '" />' +
                '</div> ' +

                '<div class="col-md-1">' +
                  '<p class="list-group-item-text stock">' + item.itemQty + '</p>'+
                '</div> ' +

                '<div class="col-md-1">'+
                  '<p class="list-group-item-text total">' + price + '</p>'+
                '</div> ' +
              '</div>' +
              '<div class="row"> ' +
                '<div  class="col-md-11">' +
                  '<input class="form-control input-sm note" placeholder="Note" />' +
                '</div> ' +
                '<div class="col-md-1">' +
                  '<p class="list-group-item-text"><i class="fa fa-times-circle fa-2x rem"></i></p>' +
                '</div>' +
                '<div class="clearfix"></div>' +
              '</div>' +
            '</div>';

  return str;
}
</script>


@endsection
