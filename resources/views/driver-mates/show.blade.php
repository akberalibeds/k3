@extends('layouts.app')

@section('title')
Driver Mate - Show {{ $driverMate->name }}
@endsection

@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="card card-info">
      <div class=" card-header blue">
        <div class="card-title">
          <div class="title">
            Driver Mate - Show
          </div>
        </div>
      </div>
      <div class="card-body">
        <div class="row">
          <div class="col-md-6">
            <dl class="dl-horizontal">
              <dt>ID</dt>
              <dd>{{ $driverMate->id }}</dd>
              <dt>Name</dt>
              <dd>{{ $driverMate->name }}</dd>
              <dt>Score</dt>
              <dd>{{ $driverMate->num }}</dd>
              <dt>Rate</dt>
              <dd>{{ $driverMate->rate }}</dd>
              <dt>Active</dt>
              <dd>@yesOrNo($driverMate->active)</dd>
            </dl>
          </div>
          <div class="col-md-6">
            <div class="file-drop-area" id="upload-drop" data-upload-identifier="{{ $driverMate->id }}">
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12">
            <ul class="bxslider" id="driver-mate-images">
              @foreach ($driverMateFiles as $file)
              <li><img src="/drivers/image/{{ $file }}" style="height:auto"></li>
              @endforeach
            </ul>
            <!--button id="fullscreen">Fullscreen</button-->
          </div>
        </div>
      </div>
      <div class="card-footer">
      </div>
    </div>
  </div>
</div>
@endsection

@push('headlinks')
<link href="{{ URL::asset('/css/jquery.bxslider.css') }}" rel="stylesheet" />
@endpush

@section('js')
<script type="text/javascript" src="{{ url ('/js/library/jquery.bxslider.min.js') }}"></script>
<script type="text/javascript" src="{{ url ('/js/library/upload.js') }}"></script>
<script type="text/javascript" src="{{ url ('/js/driver-mates/show.js') }}"></script>
@endsection
