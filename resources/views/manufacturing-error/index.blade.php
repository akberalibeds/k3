@extends('layouts.app')
@section('title')
Drivers
@endsection
@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="card card-info">
      <div class=" card-header blue">
        <div class="card-title">
          <div class="title">Errors</div>
        </div>
      </div>
      <div class="card-body">
        <div class="row">
          <div class="col-md-6 col-sm-12 col-xs-12">
            @include('share.search-bar', [
              'options' => [
                ['value' => 'itemCode', 'title' => 'ItemCode'],
                ['value' => 'created_at', 'title' => 'Date'],
              ],
              'selectable' => true
            ])
          </div>
        </div>
        <div class="row">
          <div class="col-md-12">
            <div class="table-responsive">
              <table class="small table table-condensed table-striped table-no-wrap table-hover" id="table-results-container">
                <thead>
                  <tr>
                  	<th id="select-all-container"></th>
                    <th data-sortable="name">Date</th>
                    <th data-sortable="uName">Item</th>
                    <th data-sortable="num">Problem</th>
                    <th class="text-right" data-sortable="rate">Cost</th>
                    <th class="text-right" data-sortable="score">Order</th>
                  </tr>
                </thead>
                <tbody id="result-container">
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
      <div class="card-footer">
        <div class="row">
          <div class="col-md-12 text-center" id="paging-controls-container">
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
@section('js')
<script type="text/javascript" src="{{ URL::asset('js/library/table-results_0.2.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/manufacturing-error/index.js') }}"></script>
@endsection
