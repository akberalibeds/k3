@extends('layouts.app')

@section('title')
Manufacturing Error - Show {{ $error->id }}
@endsection

@section('content')

<div class="row">
  <div class="col-md-6">
  
  
    <div class="card card-info">
      <div class=" card-header blue">
        <div class="card-title">
          <div class="title">
            Error - Show
          </div>
        </div>
      </div>
      <div class="card-body">
        <div class="row">
          <div class="col-md-12">
          
          	<form class="formUpload" enctype="multipart/form-data">
          		{{ csrf_field() }}
          		    <label class="btn btn-primary">
				       Upload Image<input type="file" multiple name="files[]" style="display: none;">
				    </label>
          		<input type="hidden" name="id" value="{{ $error->id }}" />
          	</form>
          
            <dl class="dl-horizontal">
              <dt>Item</dt>
              <dd>{{ $error->itemCode }}</dd>
              <dt>Date</dt>
              <dd>{{ date('d-M-Y', strtotime($error->created_at)) }}</dd>
              <dt>Cost</dt>
              <dd>{{ $error->cost }}</dd>
               <dt>Problem</dt>
              <dd>{{ $error->problem }}</dd>
            </dl>
          </div>
          
        </div>
        
        
        <div class="row">
          <div class="col-md-12">
            <ul class="bxslider" id="driver-images">
              @foreach ($errorFiles as $file)
              	<li><img src="images/{{ $file }}" style="height:auto;"></li>
              @endforeach
            </ul>
          </div>
        </div>
        
        
      </div>
    </div>
    
    
    
    
  </div>
  
  
  
  
  <div class="col-md-6">
  
  
    <div class="card card-info">
      <div class=" card-header blue">
      <div class="pull-right"><button class="btn btn-primary" onclick="addNote();">Add</button></div>
        <div class="card-title">
          <div class="title">
           Error - Notes
          </div>
        </div>
      </div>
      <div class="card-body">
        <div class="row">
          <div class="col-md-12">
          
          	<table class="table">
          		<thead>
          			<tr><th>Date</th><th>Note</th><th>Cost</th><th>Staff</th></tr>
          		</thead>
          		<tbody>
          		@foreach($notes as $n)
          			<tr>
          				<td>{{ @date('d-m-y H:i',strtotime($n->created_at)) }}</td>
          				<td>{{ $n->note }}</td>
          				<td>{{ $n->cost }}</td>
          				<td>{{ $n->name }}</td>
          			</tr>
          		@endforeach
          		</tbody>
          	</table>
          		
          
          
          </div>
          
        </div>
        
        
        
        
        
      </div>
    </div>
    
    
    
    
  </div>
  
  
  
  
</div>
@endsection

@push('headlinks')
<link href="{{ URL::asset('/css/jquery.bxslider.css') }}" rel="stylesheet" />
@endpush

@section('js')
<script>
	var ERRORID = {{ $error->id }};
</script>
<script type="text/javascript" src="{{ url ('/js/library/jquery.bxslider.min.js') }}"></script>
<script type="text/javascript" src="{{ url ('/js/library/upload.js') }}"></script>
<script type="text/javascript" src="{{ url ('/js/manufacturing-error/show.js') }}"></script>
<script>
$(document).on('change', ':file', function() {
    var input = $(this),
        numFiles = input.get(0).files ? input.get(0).files.length : 1,
        label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
    input.trigger('fileselect', [numFiles, label]);
    $.ajax({
        url: "/manufacturing-error/upload",
        type: "POST",
        data: new FormData($('.formUpload')[0]), 
        success : function(data){
            //location.reload();
        },
        enctype: 'multipart/form-data',
        processData: false,
        contentType: false,
        cache: false
    });
});
$(document).ready( function() {
    $(':file').on('fileselect', function(event, numFiles, label) {
        console.log(numFiles);
        console.log(label);
    });
});

$('.upload').click(function(e){
	bootbox.dialog({
	    title: 'A custom dialog with init',
	    message: '<form class="form" enctype="multipart/form-data">{{ csrf_field() }}<input type="file" multiple name="files[]" /><input type="hidden" name="id" value={{ $error->id }} ></form>',
	    buttons: { OK: {
			    	label: 'Save',
		      	  	className: "btn-success",
		      	  	callback: function() {
			      	  	  $.ajax({
		        		        url: "/manufacturing-error/upload",
		        		        type: "POST",
		        		        data: new FormData($('.form')[0]), 
	
		        		        success : function(data){
		        		            $.notify("Files Uploaded");
		        		        },
		        		        enctype: 'multipart/form-data',
		        		        processData: false,
		        		        contentType: false,
		        		        cache: false
		        		    });
			      		}
	    			}
	    }
	});
});

function addNote(){
	bootbox.dialog({
	    title: 'Add Note',
	    message: '<form class="form">{{ csrf_field() }}<textarea rows="10" class="form-control" name="note"></textarea><br><input class="form-control" name="cost" value="0.00" /><input type="hidden" name="id" value={{ $error->id }} ><input type="hidden" name="oid" value={{ $error->order_id }} ></form>',
	    buttons: { OK: {
			    	label: 'Save',
		      	  	className: "btn-success",
		      	  	callback: function() {
			      	  	  $.ajax({
		        		        url: "/manufacturing-error/note",
		        		        type: "POST",
		        		        data: $('.form').serialize(),
		        		        success : function(data){
		        		            $.notify("Note Added");
		        		        },
		        		    });
			      		}
	    			}
	    }
	});
}


</script>
@endsection
