@extends('layouts.app')
@section('title')
Seller Orders - Sellers
@endsection
@section('content')
<div class="row">
  <div class="card card-info">
    <div class="card-header blue">
      <div class="card-title">
        <div class="title">Seller Orders - Sellers</div>
      </div>
    </div>
    <div class="card-body">
      <div class="row">
        <div class="col-md-6">
          @include('share.search-bar', [
            'options' => [
              ['value' => 'name', 'title' => 'Name'],
              ['value' => 'iName', 'title' => 'Invoice Name'],
              ['value' => 'email', 'title' => 'Email'],
              ['value' => 'postcode', 'title' => 'Postcode'],
            ],
            'selectable' => false,
            'selectControls' => false,
          ])
        </div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <div class="table-responsive">
            <table class="small table table-striped table-no-wrap table-hover" id="table-results-container">
              <thead>
                <tr>
                  <th rowspan="2">Name (iName)</th>
                  <th colspan="2" class="text-center cell-border-left">Paid</th>
                  <th colspan="2" class="text-center cell-border-left">Unpaid</th>
                  <th colspan="2" class="text-center cell-border-left">Total</th>
                </tr>
                <tr>
                  <th class="text-right cell-border-left">Count</th>
                  <th class="text-right">Value</th>
                  <th class="text-right cell-border-left">Count</th>
                  <th class="text-right">Value</th>
                  <th class="text-right cell-border-left">Count</th>
                  <th class="text-right">Value</th>
                </tr>
              </thead>
              <tbody class="table-clickable" id="result-container">
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
    <div class="card-footer">
      <div class="row">
        <div class="col-md-12 text-center" id="paging-controls-container">
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
@section('js')
<script type="text/javascript" src="{{ URL::asset('js/library/table-results_0.2.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/seller-orders/index.js') }}"></script>
@endsection
