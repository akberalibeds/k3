@extends('layouts.app')
@section('title')
Seller Orders
@endsection
@section('content')
<div class="row">
  <div class="card card-info">
    <div class="card-header blue">
      <div class="card-title">
        <div class="title">Seller Orders - {{ $seller->name }}</div>
      </div>
    </div>
    <div class="card-body">
      <div class="row">
      	<div class="col-md-6">
        	<h4> Selected: <span class="selected_total">0.00</span></h4>
        </div>
        <div class="col-md-6 text-right">
          <div class="btn-group" role="group" aria-label="utils">
            <button class="btn btn-default" onclick="exportSelected()" type="button">Export as CSV</button>
            <button class="btn btn-default" onclick="markSelectedAsPaid()" type="button">Mark as Paid</button>
            <button class="btn btn-default" onclick="markSelectedAsUnPaid()" type="button">Mark as UnPaid</button>
            <button class="btn btn-default" onclick="printSelectedInvoice()" type="button">Print Invoice</button>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-6">
          @include('share.search-bar', [
            'options' => [
              ['value' => 'oid', 'title' => 'Order ID'],
              ['value' => 'dPostcode', 'title' => 'Postcode'],
              ['value' => 'dBusinessName', 'title' => 'Name'],
              ['value' => 'email1', 'title' => 'Email'],
            ],
            'selectable' => true,
            'selectControls' => true,
          ])
        </div>
        <div class="col-md-6 text-right" id="filters">
          Filters:
          <div class="btn-group" role="group" aria-label="utils" style="border: 1px solid #e0e0e0; padding: 3px">
            <input autocomplete="off" name="seller_paid" onclick="tableResults.filter(event, false)" type="radio" value="1">&nbsp;Paid
            <input autocomplete="off" name="seller_paid" onclick="tableResults.filter(event, false)" type="radio" value="0">&nbsp;UnPaid
          </div>
          <div class="btn-group" role="group" aria-label="utils" style="border: 1px solid #e0e0e0; padding: 3px">
            <input autocomplete="off" name="done" onclick="tableResults.filter(event, false)" type="radio" value="1">&nbsp;Done
            <input autocomplete="off" name="done" onclick="tableResults.filter(event, false)" type="radio" value="0">&nbsp;Not Done
          </div>
          <button class="btn btn-default btn-sm" onclick="tableResults.clearFilters()" role="button">clear</button>
        </div>
      </div>
      <div class="table-responsive" id="table-results-container">
        <table class="small table table-striped table-no-wrap table-hover">
          <thead>
            <tr>
              <th id="select-all-container"></th>
              <th>Order</th>
              <th>Invoice</th>
              <th>Buyer</th>
              <th>Customer</th>
              <th>Postcode</th>
              <th>Ref</th>
              <th>Type</th>
              <th>Status</th>
              <th>Date</th>
              <th class="text-right">Total</th>
              <th class="text-right">Paid</th>
              <th class="text-right">Balance</th>
              <th class="text-center">Seller Paid</th>
              <th class="text-center">Done</th>
              <th>Items</th>
              <th>Voucher Code</th>
              <th>Priority</th>
            </tr>
          </thead>
          <tbody class="table-clickable" id="result-container">
          </tbody>
        </table>
      </div>
    </div>
    <div class="card-footer">
      <div class="row">
        <div class="col-md-12 text-center" id="paging-controls-container">
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
@section('js')
<script>
var sellerId = {{ $seller->id }}
</script>
<script type="text/javascript" src="{{ URL::asset('js/library/table-results_0.2.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/seller-orders/show.js') }}"></script>

<script>
	$('body').on('change','.table tbody input',function(e){
		//var row = $(this).closest('tr');
		//console.log(parseFloat(row.find('td').eq(10).find('span').html()));
		var rows = $('.table tbody input:checked');
		var total = 0;
		$.each(rows,function(i,v){
			total+=(parseFloat($(v).closest('tr').find('td').eq(10).find('span').html()));
		});
		$('.selected_total').html(total.toFixed(2));
	});
</script>
@endsection
