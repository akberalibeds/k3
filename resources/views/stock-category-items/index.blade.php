@extends('layouts.app')
@section('title')
Stock for Category - {{ $category->name }}
@endsection
@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="card card-info">
      <div class=" card-header blue">
        <div class="row">
          <div class="col-md-12">
            <div class="card-title">
              <div class="title">
                Stock for Category - {{ $category->name }}
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="row">
  <div class="col-md-12">
    <div class="card card-info">
      <div class="card-body">
        <div class="row">
          <div class="col-md-6 col-sm-6 col-xs-12">
            @include('share.search-bar', [
              'options' => [
                ['value' => 'itemCode', 'title' => 'Item Code'],
                ['value' => 'itemDescription', 'title' => 'Description'],
              ],
              'selectable' => false,
              'selectControls' => false,
            ])
          </div>
        </div>
        <div class="row">
          <div class="col-md-12">
            <div class="table-responsive" id="table-results-container">
              <table class="small table table-striped table-no-wrap table-hover">
                <thead>
                  <tr>
                    <th>SKU</th>
                    <th>Description</th>
                    <th class="text-right">In Stock</th>
                    <th class="text-right">On SO</th>
                    <th class="text-right">Available</th>
                    <th class="text-right">Cost</th>
                    <th class="text-right">Retail</th>
                    <th class="text-right">Wholesale</th>
                    <th class="text-right">Boxes</th>
                    <th class="text-right">Pieces</th>
                    <th class="text-center">Blocked</th>
                    <th class="text-center">Withdrawn</th>
                  </tr>
                </thead>
                <tbody class="table-clickable" id="result-container">
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
      <div class="card-footer">
        <div class="row">
          <div class="col-md-12 text-center" id="paging-controls-container">
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
@push('js')
<script>
var categoryId = {{ $category->id }}
</script>
<script type="text/javascript" src="{{ URL::asset('js/library/table-results_0.2.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/stock-category-items/index.js') }}"></script>
@endpush
