@extends('layouts.app')
@section('title')
Stock
@endsection
@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="card card-info">
      <div class=" card-header blue">
        <div class="row">
          <div class="card-title">
            <div class="title">Stock</div>
          </div>
        </div>
      </div>
      <div class="card-body">
        <div class="row">
          <div class="col-md-6 col-sm-12 col-xs-12">
            @include('share.search-bar', [
              'options' => [
                ['value' => 'itemDescription', 'title' => 'Description'],
                ['value' => 'itemCode', 'title' => 'Item Code'],
                ['value' => 'model_no', 'title' => 'Model No.'],
              ],
              'selectable' => false,
              'selectControls' => false,
            ])
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label for="select-supplier">Category</label>
              <select autocomplete="off" class="selectpicker" data-container="body" id="select-supplier" name="category_id" onchange="tableResults.filter(event, false)">
                <option selected="selected" value="">All</option>
                @foreach ($categories as $key => $name)
                <option value="{{ $key }}">{{ $name }}</option>
                @endforeach
              </select>
            </div>
          </div>
        </div>
        <div class="table-responsive">
          <table class="small table table-striped table-no-wrap table-hover" id="table-results-container">
            <thead>
              <tr>
                <th ></th>
                <th data-sortable="itemCode">SKU</th>
                <th data-sortable="model_no">Model No.</th>
                <th>Description</th>
                <th>Category</th>
                <th class="text-right">In Stock</th>
                <th class="text-right">On SO</th>
                <th class="text-right">Available</th>
                <th class="text-right">Cost</th>
                <th class="text-right">Beds</th>
                <th class="text-right">Ebay</th>
                <th class="text-right">Retail</th>
                <th class="text-right">Wholesale</th>
                <th class="text-right">Boxes</th>
                <th class="text-right">Parts</th>
                <th class="text-center">Sends Text</th>
                <th class="text-center">Withdrawn</th>
                <th class="text-center">Always in stock</th>
              </tr>
            </thead>
            <tbody class="table-clickable" id="result-container">
            </tbody>
          </table>
        </div>
      </div>
      <div class="card-footer">
        <div class="row">
          <div class="col-md-12 text-center" id="paging-controls-container">
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
@push('js')
<script type="text/javascript" src="{{ URL::asset('js/library/table-results_0.2.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/library/import-csv.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/stock/index.js') }}"></script>
@endpush
