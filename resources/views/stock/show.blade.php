@extends('layouts.app')

@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="card card-info">
      <div class=" card-header blue">
        <div class="card-title">
          <div class="title">
            Stock Item
          </div>
        </div>
      </div>
      <div class="card-body">
        <div class="row">
          <div class="col-md-4">
            <table class="table">
              <tr><td>Item code / SKU</td><td>{{ $stockItem->itemCode }}</td></tr>
              <tr><td>Description</td><td>{{ $stockItem->itemDescription }}</td></tr>
              <tr><td>Category</td><td>{{ $stockItem->cat }}</td></tr>
              <tr><td>Category (by id)</td><td>{{ $stockItem->category->name or '(tbd)' }}</td></tr>
              <tr><td>Qty in stock</td><td id="actual">{{ $stockItem->actual }}</td></tr>
              <tr><td>Qty on Sales Order</td><td id="qty-on-so">{{ $stockItem->qty_on_so }}</td></tr>
              <tr><td>Qty Available</td><td id="qty-available">{{ $stockItem->actual - $stockItem->qty_on_so }}</td></tr>
              <tr><td>Boxes</td><td>{{ $stockItem->pieces }}</td></tr>
              <tr><td>Parts</td><td>{{ $stockItem->isMulti }}</td></tr>
              <tr><td>Weight</td><td>{{ $stockItem->weight }} Kg</td></tr>
              <tr><td>Label</td><td>@if ($stockItem->label) Yes @else No @endif</td></tr>
              <tr><td>Warehouse</td><td>{{ $stockItem->warehouse }}</td></tr>
              <tr><td>Available to seller</td><td>@if ($stockItem->seller) Yes @else No @endif</td></tr>
              <tr><td>Cost</td><td>&pound;{{ number_format($stockItem->cos, 2) }}</td></tr>
              <tr><td>Ebay</td><td>&pound;{{ number_format($stockItem->ebay_price, 2) }}</td></tr>
              <tr><td>Retail</td><td>&pound;{{ number_format($stockItem->retail, 2) }}</td></tr>
              <tr><td>Wholesale</td><td>&pound;{{ number_format($stockItem->wholesale, 2) }}</td></tr>
              <tr><td>Blocked</td><td>@if ($stockItem->blocked) Yes @else No @endif</td></tr>
              <tr><td>Always in stock</td><td>@if ($stockItem->always_in_stock) Yes @else No @endif</td></tr>
            </table>
          </div>
        </div>
      </div>
      <div class="card-footer">
      </div>
    </div>
  </div>
</div>
@endsection

@section('js')
<script type="text/javascript" src="{{ URL::asset('js/stock/show.js') }}"></script>
@endsection
