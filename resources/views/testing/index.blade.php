@extends('layouts.app')

@section('title')
Testing Orders
@endsection

@section('content')
<div class="row">
  <div class="card card-info">
    <div class="card-header blue">
      <div class="card-title">
        <div class="title">Testing Orders</div>
      </div>
    </div>
    <div class="card-body">
      <div class="row">
        <div class="col-md-6 col-sm-6 col-xs-12">
          @include('share.search-bar', [
            'options' => [
              ['value' => 'oid', 'title' => 'Order ID'],
              ['value' => 'dPostcode', 'title' => 'Postcode'],
              ['value' => 'dBusinessName', 'title' => 'Name'],
              ['value' => 'email1', 'title' => 'Email']
            ],
            'selectable' => true,
            'selectControls' => true,
          ])
        </div>
        <div class="col-md-4 col-sm-6" id="filters">
          Filters:
          <div class="btn-group" role="group" aria-label="utils" style="border: 1px solid #e0e0e0; padding: 3px">
            <input autocomplete="off" name="paid-unpaid" onclick="tableResults.filter(event)" type="radio" value="paid">&nbsp;Paid
            <input autocomplete="off" name="paid-unpaid" onclick="tableResults.filter(event)" type="radio" value="unpaid">&nbsp;UnPaid
          </div>
          <div class="btn-group" role="group" aria-label="utils" style="border: 1px solid #e0e0e0; padding: 3px">
            <input autocomplete="off" name="done-notdone" onclick="tableResults.filter(event)" type="radio" value="done">&nbsp;Done
            <input autocomplete="off" name="done-notdone" onclick="tableResults.filter(event)" type="radio" value="notdone">&nbsp;Not Done
          </div>
          <button class="btn btn-default btn-sm" onclick="tableResults.clearFilters(event)" role="button">clear</button>
        </div>

      </div>

      <div class="table-responsive">
        <table id="orders-table" class="small table table-striped table-no-wrap table-clickable table-hover">
          <thead>
            <tr>
              <th></th>
              <th>Order</th>
              <th>Invoice</th>
              <th>Buyer</th>
              <th>Customer</th>
              <th>Postcode</th>
              <th>Ref</th>
              <th>Type</th>
              <th>Status</th>
              <th>Date</th>
              <th class="text-right">Total</th>
              <th class="text-right">Paid</th>
              <th class="text-right">Balance</th>
              <th class="text-center">Seller Paid</th>
              <th>Done</th>
              <th>Voucher Code</th>
            </tr>
          </thead>
          <tbody id="result-container">
          </tbody>
        </table>
      </div>
    </div>
    <div class="card-footer">
      <div class="row">
        <div class="col-md-12 text-center" id="paging-controls-container">
        </div>
      </div>
    </div>
  </div>
</div>
@endsection

@section('js')
<script>
var sellerId = {{ $sellerId }}
</script>
<script type="text/javascript" src="{{ URL::asset('js/library/busy-kit.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/library/table-results.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/testing/index.js') }}"></script>
@endsection
