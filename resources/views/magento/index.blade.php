@extends('layouts.app')
@section('content')


<div class="col-md-3">
	<div class="card card-info">
		<div class="card-header blue">
        	<div class="card-title">
            	<div class="title">Search</div>
          	</div>
        </div>
        
		<div class="card-body">
			
				<div class="form-group">
					<label class="control-label">Beds Order ID (one per line)</label>
					<textarea class="form-control id-list"></textarea>
				</div>
			
				<button class="btn btn-primary pull-right" onclick="getOrdersByID();">Go</button>
				<div class="clearfix"></div>
			
			<hr />
				<h4 class="text-center">or</h4>
			<hr />
			
			<form class="byDate">
				<div class="col-md-7">
					<div class="form-group">
						<label class="control-label">Date from</label>
						<input name="datefrom" class="form-control datepicker" />
					</div>
				</div>
				
				<div class="col-md-5">
					<div class="form-group">
						<label class="control-label">Time from</label>
						<input name="timefrom"  class="form-control timepicker" />
					</div>
				</div>
				
				
				<div class="col-md-7">
					<div class="form-group">
						<label class="control-label">Date to</label>
						<input name="dateto"  class="form-control datepicker" />
					</div>
				</div>
				
				<div class="col-md-5">
					<div class="form-group">
						<label class="control-label">Time to</label>
						<input name="timeto" class="form-control timepicker" />
					</div>
				</div>
			
			</form>
			
			<button class="btn btn-primary pull-right" onclick="getOrdersByDate();">Go</button>
			<div class="clearfix"></div>
		</div>
	</div>
</div>

<div class="col-md-7">
	<div class="card card-info">
		<div class="card-header blue">
        	<div class="card-title">
            	<div class="title"></div>
          	</div>
        </div>
        
	<div class="card-body result">
		
	</div>
</div>	

</div>
@endsection

@section('js')
	<link href="/js/picker/themes/default.time.css" rel="stylesheet" type="text/css">
	<style>
		.picker--time .picker__frame {
		    max-height: 600px;
		    overflow: auto;
		}
		
	</style>
	<script src="/js/picker/picker.time.js"></script>
	<script>
	var i = 0;
		$(window).load(function(e){
			$('.timepicker').pickatime({
				format: 'HH:i',
			})
			$('.datepicker').pickadate({
				format: 'yyyy-mm-dd',
			})
		});


		function getOrdersByDate(){
			i = 0;
			$('.result').html('<div class="text-center"><i class="fa fa-spinner fa-spin fa-2x"></i></div>');
			$.ajax({
				url: '/magento/byDate',
				data: $('.byDate').serialize(),
				success: function(data){
					$('.result').html('Orders: ' + data.length)
					
					if(data.length){
						$('.result').append('<br>inserting orders....');
						start(data);
					}
					
					
				}
			});
		}


		function getOrdersByID(){
			i = 0;
			$('.result').html('inserting orders....');
			var d = $('.id-list').val().split('\n');
			console.log(d);
			var data  = [];
			
			$.each(d,function(index,val){
				var dat={};
				dat['increment_id']=val;
				data.push(dat);
			});
			
			start(data);
			
		}

	function start(data){
		$('.result').append('<br>' + data[i]['increment_id'] + " <i class='fa fa-spinner fa-spin'></i>");
		$.ajax({
				url: '/magento/getInfo/'+data[i]['increment_id'],
				success: function(resp){
					$('.fa-spin').removeClass('fa-spinner');
					$('.fa-spin').addClass('fa-check');
					$('.fa-spin').removeClass('fa-spin');
					i++;
					if(i<data.length){
						start(data);
					}
					else{
						$('.result').append('<br>Completed!');
					}
				}
			});
	}
	
		
	</script>
@endsection