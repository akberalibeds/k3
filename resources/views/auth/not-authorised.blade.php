@extends('layouts.app')
@section('title')
not authorised
@endsection
@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="card card-info">
      <div class=" card-header blue">
        <div class="card-title">
          <div class="title">
            Not Authorised
          </div>
        </div>
       </div>
      <div class="card-body">
        <p>You are not authorised.</p>
        @if (!Auth::user())
        @include('share.not-logged-in')
        @endif
      </div>
      <div class="card-footer">
      </div>
    </div>
  </div>
</div>
@endsection
