@extends('layouts.app')

@section('title')
Supplier Invoice Orders - {{ $supplier->supplier_name }}
@endsection

@section('content')
<div class="row">
  <div class="card card-info">
    <div class="card-header blue">
      <div class="card-title">
        <div class="title">Supplier Invoice Orders - {{ $supplier->supplier_name }}</div>
      </div>
    </div>
    <div class="card-body">
      <div class="row">
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="input-group input-group-typeahead">
            <input class="form-control typeahead" id="typeahead-supplier-orders" placeholder="search" type="search">
            <span class="tt-badge" id="typeahead-spinner"></span>
          </div>
        </div>
        <div class="col-md-2 col-sm-6 col-xs-12">
          <select class="form-control selectpicker" id="select-search-by" name="select-search-by">
            <option value="orders.id;oid">Order ID</option>
            <option value="dBusinessName;dBusinessName">Name</option>
            <option value="email1;email1">Email</option>
          </select>
        </div>
      </div>
      <div class="thin-row">
        <div class="col-md-2">
          <small id="result-counts"></small>
        </div>
      </div>
      <div class="table-responsive">
        <table id="orders-table" class="small table table-striped table-no-wrap">
          <thead>
            <tr>
              <th>Order</th>
              <th>Name</th>
              <th>Items</th>
              <th class="text-center">Qty</th>
              <th class="text-right">Cost</th>
              <th class="text-right">Total</th>
              <th class="col-md-3">Notes</th>
            </tr>
          </thead>
          <tbody id="supplier-orders-table">
          </tbody>
        </table>
      </div>
    </div>
    <div class="card-footer">
      <div class="row">
        <div class="col-md-12 text-center" id="paging-controls">
        </div>
      </div>
    </div>
  </div>
</div>
@endsection

@section('js')
<script>
var invoiceMade = {{ $invoiceMade }}
var supplierId = {{ $supplier->id }}
</script>
<script type="text/javascript" src="{{ URL::asset('js/library/busy-kit.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/library/typeahead.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/library/typeahead-kit.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/library/paging.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/supplier-orders/invoiced.js') }}"></script>
@endsection
