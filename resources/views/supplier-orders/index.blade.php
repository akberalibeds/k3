@extends('layouts.app')
@section('title')
Supplier Orders - {{ $supplier->supplier_name }}
@endsection
@section('content')
<div class="row">
  <div class="card card-info">
    <div class="card-header blue">
      <div class="card-title">
        <div class="title">Supplier Orders - {{ $supplier->supplier_name }}</div>
      </div>
    </div>
    <div class="card-body">
      <div class="row">
        <div class="col-md-12 text-right">
          <div class="btn-group" role="group" aria-label="utils">
            <button class="btn btn-default" onclick="showInvoices()" type="button">Invoice</button>
            <button class="btn btn-default" onclick="showRouteDays()" type="button">Route Days</button>
            <button class="btn btn-default" onclick="moveSupplier()" type="button">Move To</button>
          </div>
          <div class="btn-group" role="group" aria-label="utils">
            <button class="btn btn-default" onclick="printLabels()" type="submit">Labels</button>
            <button class="btn btn-default" onclick="exportCsvSelected()" type="button">CSV</button>
            <button class="btn btn-default" onclick="exportExcelSelected()" type="button">Excel</button>
          </div>
          <div class="btn-group" role="group" aria-label="utils">
            <button class="btn btn-default" onclick="setAsProcessed()" type="button">Process</button>
            <button class="btn btn-default" onclick="setAsUnProcessed()" type="button">Unprocess</button>
            <button class="btn btn-default" onclick="setAsOrdered()" type="button">Ordered</button>
            <button class="btn btn-default" onclick="setAsUnOrdered()" type="button">Unorder</button>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-6">
          @include('share.search-bar', [
            'options' => [
              ['value' => 'orders.id', 'title' => 'Order ID'],
              ['value' => 'deliverBy', 'title' => 'Deliver By'],
              ['value' => 'iNo', 'title' => 'Invoice'],
              ['value' => 'dBusinessName', 'title' => 'Name'],
              ['value' => 'email1', 'title' => 'Email'],
              ['value' => 'itemCode', 'title' => 'Items'],
              ['value' => 'routeName.name', 'title' => 'Route'],
              ['value' => 'startDate', 'title' => 'Date'],
            ],
            'selectable' => true,
            'limits' => [10,25,50,100,200,250]
          ])
        </div>
      </div>
      <div class="table-responsive">
        <table id="table-results-container" class="small table table-striped table-no-wrap table-hover">
          <thead>
            <tr>
              <th id="select-all-container"></th>
              <th>Order</th>
              <th>Invoice</th>
              <th>Date</th>
              <th data-sortable="deliverBy">Deliver by</th>
              <th data-sortable="dBusinessName">Name</th>
              <th data-sortable="route_name">Route</th>
              <th>Postcode</th>
              <th>Pay Type</th>
              <th class="text-right">Paid</th>
              <th class="text-right" data-sortable="total">Total</th>
              <th class="text-right">Balance</th>
              <th>Items</th>
              <th class="text-center">Processed</th>
              <th class="text-center">Ordered</th>
              <th class="text-center">Done</th>
              <th class="text-center">Priority</th>
            </tr>
          </thead>
          <tbody class="table-clickable" id="result-container">
          </tbody>
        </table>
      </div>
    </div>
    <div class="card-footer">
      <div class="row">
        <div class="col-md-12 text-center" id="paging-controls-container">
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
@section('js')
<script>
var supplierId = {{ $supplier->id }}
var suppliers = []
@foreach ($suppliers as $supplier)
suppliers.push(JSON.parse('{!! $supplier !!}'))
@endforeach
</script>
<script type="text/javascript" src="{{ URL::asset('js/library/table-results_0.2.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/supplier-orders/index.js') }}"></script>
@endsection
