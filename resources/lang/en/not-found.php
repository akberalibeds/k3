<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Not Found responses
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given when an entity cannot be found.
    |
    */

    'invoice' => 'Could not find invoice with id :id',
    'order' => 'Could not find order with id :id',
    'postcode' => 'Could not find postcode.',
    'role' => 'Could not find role with id :id',
    'route' => 'Could not find route with id :id',
    'supplier' => 'Could not find supplier with id :id',
    'user' => 'Could not find user with id :id',
    'user-role' => 'Could not find role with id :rid user with id :uid',

    'roles' => 'Could not find ALL of the roles.',
];
