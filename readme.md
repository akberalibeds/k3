#Description of the web app
___
**K3**, is a bespoke CRM built for giomani. They sell products online and this system is responsible for assisting their day to day tasks. Among many things users of the system can track, add, cancel and update orders. In addition to that various reports are available for users with special permissions and roles to see and download. Furthermore, an interesting capability of the system is that it automatically triggers various jobs through the day which perfrom certain tasks, such as importing orders from EBAY and Woocommerce, sending notifications to users about their orders etc. This is a brief description of the system and many more features are available.

#Installation
___
1. Install through git. In your terminal run this command:

	- git clone https://giacholr@bitbucket.org/bootcampmedia/k3-laravel.git

#Getting started
___
2. Install php here [https://www.apachefriends.org/download.html].

3. Install composer here [https://getcomposer.org/download/].

4. Install laravel by running the following command in your terminal:

	- composer global require "laravel/installer"

5. Install node.js here [https://nodejs.org/en/].

6. Navigate to the **root** of your application and run the following commands:

	- composer install
	- php artisan key:generate

7. Set up a **Mysql** database.

8. Import the **giomani_empty.sql** file into your database and then run the following command:

	- php artisan migrate