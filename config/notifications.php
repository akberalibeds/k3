<?php
$typeprefix = 'App\\Notifications\\Types';
$methodprefix = 'App\\Notifications\\Methods';
return [
  'types' => [
    "$typeprefix\\DeliveryDateRequest",
    "$typeprefix\\DispatchReady",
//    "$typeprefix\\DeliveryImminent",
  ],
  'methods' => [
    "$methodprefix\\SMS",
    "$methodprefix\\Email",
    "$methodprefix\\Ebay",
  ],
];
