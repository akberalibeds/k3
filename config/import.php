<?php

return [

  /*
  |--------------------------------------------------------------------------
  | General
  |--------------------------------------------------------------------------
  |
  */
  'paths' => [
    'profiles' => realpath(base_path('App/Helper/Import/profiles/')),
    'storage' => realpath(base_path('storage/upload/imports/')),
  ],
  /*
  |--------------------------------------------------------------------------
  | Import profiles
  |--------------------------------------------------------------------------
  |
  */
  'imports' => [
    // 'bedtastic_tracking' => [
    //   'class' => 'BedtasticTracking',
    //   'name' => 'Bedtastic Tracking',
    //   'profile' => 'bedtastic-tracking',
    // ],
    'mighty_deals' => [
      'name' => 'Mighty Deals Order Import',
      'profile' => 'mighty-deals-orders',
    ],
    'stock_manifest' => [
      'name' => 'Stock Container Manifest',
      'profile' => 'manifest',
    ],
    // 'wowcher' => [
    //   'class' => 'WowcherOrders',
    //   'name' => 'Wowcher Orders',
    //   'profile' => 'wowcher-orders',
    // ],
  ],
];
