<?php

  /**
   * @author Giomani Designs (Development Team)
   */
  return [
    'bulletins' => (env('APP_ENV') === 'local'),
  ];
