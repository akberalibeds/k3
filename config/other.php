<?php

/**
 * @author Giomani Designs (Development Team)
 */
return [
  'act' => true,
  'dispatched-routes-count' => 30,
  'edo-order-stats-days' => 30,
];
