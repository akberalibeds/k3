<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, Mandrill, and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],

    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => 'us-east-1',
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model' => App\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],

    'wowcher' => [
        'j_username' => env('WOWCHER_USERNAME'),
        'j_password' => env('WOWCHER_PASSWORD'),
    ],

    'woocommerce' => [

        /**
         * Do not use special characters for vendor/company name.
         * Instead concatenate the words as shown below:
         * E.g Instead of bedland.com write bedlandcom
         */
        'bedlandcom' => [
            'url' => env('WOOCOMMERCE_BEDLAND_URL'),
            'consumer_key' => env('WOOCOMMERCE_BEDLAND_CONSUMER_KEY'),
            'consumer_secret' => env('WOOCOMMERCE_BEDLAND_CONSUMER_SECRET'),
        ],
        'bedscouk' => [
            'url' => env('WOOCOMMERCE_BEDS_URL'),
            'consumer_key' => env('WOOCOMMERCE_BEDS_CONSUMER_KEY'),
            'consumer_secret' => env('WOOCOMMERCE_BEDS_CONSUMER_SECRET'),
        ]
    ],
];
