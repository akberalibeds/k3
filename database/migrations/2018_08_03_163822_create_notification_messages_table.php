<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNotificationMessagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notification_messages', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('notificationID');
            $table->string('recipient');
            $table->string('recipient_name');
            $table->mediumtext('body');
            $table->string('subject')->nullable();
            $table->string('msgMethod');
            $table->boolean('is_sent')->default(0);
            $table->string('err_message')->nullable();
            $table->timestamp('date_sent')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('notification_messages');
    }
}
