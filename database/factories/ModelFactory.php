<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\User::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->safeEmail,
        'password' => bcrypt(str_random(10)),
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\Notification::class, function (Faker\Generator $faker) {

    $customer = App\Customer::create([
        'dBusinessName' => $faker->name,
        'tel' => $faker->e164PhoneNumber,
        'email1' => $faker->safeEmail,
        'userID' => $faker->name,
    ]);

    $types = [
        'App\Notifications\Types\DeliveryDateRequest',
        'App\Notifications\Types\DeliveryDateRequestReminder',
        'App\Notifications\Types\DispatchReady',
        'App\Notifications\Types\DeliveryImminent',
    ];
    $randType = array_rand($types);

    return [
        'customerID' => $customer->id,
        'orderID' => function() use ($faker, $customer){
            return App\Order::create([
                'sender'=>'1',
                'ebayID'=>'99797-9798',
                'cid' => $customer->id,
                'delTime' => Carbon\Carbon::now()->addHours(1)->format('H:m'),
                'code' => 'Test-Code'
            ])->id;
        },
        'dispID' => null,
        'type' => $types[$randType],
        'hasSent' => false,
    ];
});
