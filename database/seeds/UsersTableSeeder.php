<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run() {
    DB::table('users')->insert([
      'password' => '$2y$10$djb4l2xtGDG8BtwZH6GlEugOzJamkNvIy145d3RLqLfhFzb3LNHdO',
      'id' => '18',
      'email' => 'aans',
      'name' => 'aans'
    ]);
    DB::table('users')->insert([
      'password' => Hash::make('53rvym'),
      'id' => '19',
      'email' => 'dan',
      'name' => 'dan'
    ]);
    DB::table('users')->insert([
      'password' => Hash::make('c6xhxc'),
      'id' => '20',
      'email' => 'aamir',
      'name' => 'aamir'
    ]);
    DB::table('users')->insert([
      'password' => '$2y$10$TyhU7Ee.8qiQc/uxMLF2UOQoijiEJdq.ahJhFRtCbxS72gFr9dvOe',
      'id' => '21',
      'email' => 'nad',
      'name' => 'nad'
    ]);
    DB::table('users')->insert([
      'password' => Hash::make('j2fn2j'),
      'id' => '22',
      'email' => 'Awais',
      'name' => 'Awais'
    ]);
    DB::table('users')->insert([
      'password' => '$2y$10$PMNbI4ao/4RRwLa4a0vK1eU7JDTyWxW3F8smeTtftzqk5CHvXNdWa',
      'id' => '23',
      'email' => 'akbar',
      'name' => 'akbar'
    ]);
    DB::table('users')->insert([
      'password' => '$2y$10$LnkdOoU/PhsRtuLvflfQC.71PWeyOa5UH6jv4cemZR1dSMndyRa3K',
      'id' => '24',
      'email' => 'khurshid',
      'name' => 'khurshid'
    ]);
    DB::table('users')->insert([
      'password' => '$2y$10$sT6YAVZziLN/0RjcagSPpelQYPNVeiBkBD1zZFREvr8ulZEBOOFje',
      'id' => '25',
      'email' => 'mohali',
      'name' => 'mohali'
    ]);
    DB::table('users')->insert([
      'password' => Hash::make('es48pe'),
      'id' => '26',
      'email' => 'Zayn',
      'name' => 'Zayn'
    ]);
    DB::table('users')->insert([
      'password' => '$2y$10$9L9FZ38B7nxfSJWgE5vlZu4JOHBsYf2AvqXPCyw.KcIzfewV8nExS',
      'id' => '27',
      'email' => 'Abaz',
      'name' => 'Abaz'
    ]);
    DB::table('users')->insert([
      'password' => '$2y$10$pBSpjCL9BCTo6n86hncCJu2J0UsIFq4IQHQgieC/Co32MJEhtYp2K',
      'id' => '28',
      'email' => 'Abu',
      'name' => 'Abu'
    ]);
    DB::table('users')->insert([
      'password' => Hash::make('j7cuzv'),
      'id' => '30',
      'email' => 'Saf',
      'name' => 'Saf'
    ]);
    DB::table('users')->insert([
      'password' => Hash::make('4rjbgc'),
      'id' => '31',
      'email' => 'Abdul',
      'name' => 'Abdul'
    ]);
    DB::table('users')->insert([
      'password' => '$2y$10$2cb0BG62.RS3yu34pk8qJOJRwUU6AiyGzjQ5ptwuJi5Pz/l7aM2ri',
      'id' => '32',
      'email' => 'Jay',
      'name' => 'Jay'
    ]);
    DB::table('users')->insert([
      'password' => '$2y$10$710w/6Ms06oxw/VLfhb1S.w6LfMakvf2AaggkUjoYepkR7fP41SKm',
      'id' => '33',
      'email' => 'Asad',
      'name' => 'Asad'
    ]);
    DB::table('users')->insert([
      'password' => Hash::make('gx3rws'),
      'id' => '34',
      'email' => 'ali',
      'name' => 'ali'
    ]);
    DB::table('users')->insert([
      'password' => '$2y$10$j9qhDkZCFFEVqLmscgWrterMbNQ6l0dZWcwZMYoNYO/rbKU7tS6Zm',
      'id' => '35',
      'email' => 'adam',
      'name' => 'adam'
    ]);
    DB::table('users')->insert([
      'password' => Hash::make('9j65hh'),
      'id' => '36',
      'email' => 'rambo',
      'name' => 'rambo'
    ]);
    DB::table('users')->insert([
      'password' => Hash::make('5zs55n'),
      'id' => '37',
      'email' => 'rash',
      'name' => 'rash'
    ]);
    DB::table('users')->insert([
      'password' => Hash::make('xgam5j'),
      'id' => '38',
      'email' => 'nabil',
      'name' => 'nabil'
    ]);
    DB::table('users')->insert([
      'password' => Hash::make('q4a5xw'),
      'id' => '39',
      'email' => 'umar',
      'name' => 'umar'
    ]);
    DB::table('users')->insert([
      'password' => Hash::make('fdze94'),
      'id' => '40',
      'email' => 'saif',
      'name' => 'saif'
    ]);
    DB::table('users')->insert([
      'password' => Hash::make('me3x84'),
      'id' => '41',
      'email' => 'hasaam',
      'name' => 'hasaam'
    ]);
    DB::table('users')->insert([
      'password' => Hash::make('hyqh97'),
      'id' => '42',
      'email' => 'zaid',
      'name' => 'zaid'
    ]);
    DB::table('users')->insert([
      'password' => Hash::make('kvdg5g'),
      'id' => '43',
      'email' => 'jiya',
      'name' => 'jiya'
    ]);
    DB::table('users')->insert([
      'password' => Hash::make('nmz5gp'),
      'id' => '44',
      'email' => 'tahir',
      'name' => 'tahir'
    ]);
    DB::table('users')->insert([
      'password' => Hash::make('wt4aya'),
      'id' => '45',
      'email' => 'abid',
      'name' => 'abid'
    ]);
    DB::table('users')->insert([
      'password' => Hash::make('er75fy'),
      'id' => '46',
      'email' => 'mani',
      'name' => 'mani'
    ]);
    DB::table('users')->insert([
      'password' => Hash::make('zm37a9'),
      'id' => '47',
      'email' => 'claire',
      'name' => 'claire'
    ]);
    DB::table('users')->insert([
      'password' => Hash::make('vut8mz'),
      'id' => '48',
      'email' => 'harry',
      'name' => 'harry'
    ]);
    DB::table('users')->insert([
      'password' => Hash::make('q36d7h'),
      'id' => '49',
      'email' => 'terri',
      'name' => 'terri'
    ]);
    DB::table('users')->insert([
      'password' => Hash::make('z6numz'),
      'id' => '50',
      'email' => 'shamas',
      'name' => 'shamas'
    ]);
    DB::table('users')->insert([
      'password' => Hash::make('65eeus'),
      'id' => '51',
      'email' => 'sarah',
      'name' => 'sarah'
    ]);
    DB::table('users')->insert([
      'password' => Hash::make('2n72ks'),
      'id' => '52',
      'email' => 'naz',
      'name' => 'naz'
    ]);
    DB::table('users')->insert([
      'password' => Hash::make('8hsp3w'),
      'id' => '53',
      'email' => 'julie',
      'name' => 'julie'
    ]);
    DB::table('users')->insert([
      'password' => Hash::make('5u4a6q'),
      'id' => '54',
      'email' => 'jason',
      'name' => 'jason'
    ]);
    DB::table('users')->insert([
      'password' => Hash::make('t5k963'),
      'id' => '59',
      'email' => 'kiran',
      'name' => 'kiran'
    ]);
    DB::table('users')->insert([
      'password' => Hash::make('ubuv5f'),
      'id' => '60',
      'email' => 'jaz',
      'name' => 'jaz'
    ]);
    DB::table('users')->insert([
      'password' => Hash::make('ftj2p2'),
      'id' => '61',
      'email' => 'sarina',
      'name' => 'sarina'
    ]);
    DB::table('users')->insert([
      'password' => Hash::make('brk44h'),
      'id' => '62',
      'email' => 'moh',
      'name' => 'moh'
    ]);
    DB::table('users')->insert([
      'password' => Hash::make('4ufmqk'),
      'id' => '65',
      'email' => 'rhianon',
      'name' => 'rhianon'
    ]);
    DB::table('users')->insert([
      'password' => Hash::make('zwj5dw'),
      'id' => '66',
      'email' => 'tasha',
      'name' => 'tasha'
    ]);
    DB::table('users')->insert([
      'password' => Hash::make('bytm2p'),
      'id' => '67',
      'email' => 'charlotte',
      'name' => 'charlotte'
    ]);
    DB::table('users')->insert([
      'password' => Hash::make('x9ew6x'),
      'id' => '68',
      'email' => 'summer',
      'name' => 'summer'
    ]);
    DB::table('users')->insert([
      'password' => Hash::make('y49cf9'),
      'id' => '69',
      'email' => 'diana',
      'name' => 'diana'
    ]);
    DB::table('users')->insert([
      'password' => Hash::make('qnq75w'),
      'id' => '72',
      'email' => 'katy',
      'name' => 'katy'
    ]);
    DB::table('users')->insert([
      'password' => Hash::make('ys82ax'),
      'id' => '73',
      'email' => 'shannon',
      'name' => 'shannon'
    ]);
    DB::table('users')->insert([
      'password' => Hash::make('dj58w9'),
      'id' => '74',
      'email' => 'rav',
      'name' => 'rav'
    ]);
    DB::table('users')->insert([
      'password' => Hash::make('gj75t8'),
      'id' => '75',
      'email' => 'kayleigh',
      'name' => 'kayleigh'
    ]);
    DB::table('users')->insert([
      'password' => Hash::make('c3b6v7'),
      'id' => '76',
      'email' => 'daniel',
      'name' => 'daniel'
    ]);
    DB::table('users')->insert([
      'password' => Hash::make('zv78at'),
      'id' => '77',
      'email' => 'haleem',
      'name' => 'haleem'
    ]);
    DB::table('users')->insert([
      'password' => Hash::make('hv22gh'),
      'id' => '78',
      'email' => 'Lauren',
      'name' => 'Lauren'
    ]);
    DB::table('users')->insert([
      'password' => Hash::make('7x3ffm'),
      'id' => '79',
      'email' => 'sunny',
      'name' => 'sunny'
    ]);
    DB::table('users')->insert([
      'password' => Hash::make('2bp2y5'),
      'id' => '80',
      'email' => 'balal',
      'name' => 'balal'
    ]);
    DB::table('users')->insert([
      'password' => Hash::make('f6n54j'),
      'id' => '81',
      'email' => 'zia',
      'name' => 'zia'
    ]);
    DB::table('users')->insert([
      'password' => Hash::make('33sec7'),
      'id' => '82',
      'email' => 'anna',
      'name' => 'anna'
    ]);
    DB::table('users')->insert([
      'password' => Hash::make('x52f79'),
      'id' => '83',
      'email' => 'nikita',
      'name' => 'nikita'
    ]);
    DB::table('users')->insert([
      'password' => Hash::make('zbw5h7'),
      'id' => '84',
      'email' => 'arjun',
      'name' => 'arjun'
    ]);
    DB::table('users')->insert([
      'password' => '$2y$10$h0TPEFGgOBdr3Yw9AsBcYO2Ad4JROaHEIR3i/NsUmUe0TorfiKfIq',
      'id' => '85',
      'email' => 'shahied',
      'name' => 'shahied'
    ]);
    DB::table('users')->insert([
      'password' => Hash::make('p5pprb'),
      'id' => '86',
      'email' => 'huram',
      'name' => 'huram'
    ]);
    DB::table('users')->insert([
      'password' => Hash::make('8qjrrf'),
      'id' => '87',
      'email' => 'courtney',
      'name' => 'courtney'
    ]);
    DB::table('users')->insert([
      'password' => Hash::make('5j9hvg'),
      'id' => '88',
      'email' => 'chelsea',
      'name' => 'chelsea'
    ]);
    DB::table('users')->insert([
      'password' => Hash::make('9bv7tn'),
      'id' => '89',
      'email' => 'haazim',
      'name' => 'haazim'
    ]);
    DB::table('users')->insert([
      'password' => Hash::make('tp992u'),
      'id' => '90',
      'email' => 'hammad',
      'name' => 'hammad'
    ]);
  }
}
