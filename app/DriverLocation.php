<?php

namespace App;

use App\Helper\Traits\QueryableModel;
use Illuminate\Database\Eloquent\Model;
use Hash;

class DriverLocation extends Model
{

	protected $table = 'driverLocs';
	
	public $fillable = ['driver_id','lat','lng'];
	
	
	
	
	public static function insertLocation($data){
		
		
		$a = self::where('driver_id',$data['driver_id'])->first();
			
		if(!$a){
			$a = new self($data); 
		}
		$a->update($data);
		$a->save();
		
	}
	
	
	
	
	public static function getLocations($date = null){
		
		$date = ($date) ? $date : date('Y-m-d');
		//echo "<br>";
		$routes = DispatchedRoute::select('driver')->where('date','=',date('d-M-Y'))->get();
		
		$drivers = [];
		foreach ($routes as $r){
			
			$drivers[]=$r->driver;
			
		}
		
		$drivers = Driver::select("id","name")->whereIn('name',$drivers)->orderBy('name','asc')->get();
		$locations = [];
		foreach($drivers as $d){
			
			 $a = Device::select(
			 			'drivers.name',
			 			'driverLocs.*',
			 			\DB::raw('if(date(driverLocs.updated_at) = CURDATE(),1,0) as active')
			 		)
					->join('drivers','devices.driver','=','drivers.id')
					->leftJoin('driverLocs','drivers.id','=','driverLocs.driver_id')
					->where("driver",$d->id)
					->first();
			
			if($a){
				$datee 	= date('d-M-Y');
				$route  = DispatchedRoute::select('id','route','date')->where('driver',$d->name)->where('date',$datee)->orderBy('id','desc')->first();
				$dels = null;
				if($route){
				$dels = Dispatched::select('disp.num','disp.oid')
						->join('orders','disp.oid','=','orders.id')
						->where(['route' => $route->route, 'date' => $route->date])
						->groupBy('orders.id')
						->orderBy('orders.delOrder')
						->whereIn('orderStatus',['dispatched','routed'])
						->first();
				}
				
				$a->del = ($dels) ? $dels->num : "Completed";
				$a->order_id = ($dels) ? $dels->oid : null;
				$a->route = $route ? $route : null;
				$locations[$d->id] = $a; 
			}
		}
		
		
		return $locations;
		
	}
	
	/*
	public static function getLocations($date = null){
	
		$date = ($date) ? $date : date('Y-m-d');
		//echo "<br>";
		$drivers = Driver::select("id","name")->orderBy('name','asc')->get();
		$locations = [];
		foreach($drivers as $d){
				
			$a = self::select(
					'drivers.name',
					'driverLocs.*'
					)
					->join('drivers','driverLocs.driver_id','=','drivers.id')
					->where(\DB::raw('date(driverLocs.updated_at)'),'=',\DB::raw('CURDATE()'))
			->where("driver_id",$d->id)
			->first();
				
			if($a){
				$datee 	= date('d-M-Y');
				$route  = DispatchedRoute::select('id','route','date')->where('driver',$d->name)->where('date',$datee)->orderBy('id','desc')->first();
				$dels = null;
				if($route){
					$dels = Dispatched::select('disp.num','disp.oid')
					->join('orders','disp.oid','=','orders.id')
					->where(['route' => $route->route, 'date' => $route->date])
					->groupBy('orders.id')
					->orderBy('orders.delOrder')
					->whereIn('orderStatus',['dispatched','routed'])
					->first();
				}
	
				$a->del = ($dels) ? $dels->num : "Completed";
				$a->order_id = ($dels) ? $dels->oid : null;
				$a->route = $route ? $route : null;
				$locations[$d->id] = $a;
			}
		}
	
	
		return $locations;
	
	}
	*/
	
}
