<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderType extends Model
{
  /**
   * The table associated with the model.
   *
   * @var string
   */
  protected $table = 'order_type';
  
  
  
  
  /*
  	returns id for given type
  */
  
  public static function id_type($type){
	
		$type = OrderType::select('id')->where('order_type',$type)->get()->first();
	  	return $type['id'];
 
  }
  
  
  
   /*
  	returns type for given id
  */
    public static function type($id){
	
		$type = OrderType::select('order_type')->where('id',$id)->get()->first();
	  	return $type['order_type'];
		
  }
  
}
