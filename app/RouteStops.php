<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Routestops extends Model
{
  /**
   * The table associated with the model.
   *
   * @var string
   */
  protected $table = 'routeStops';
  /**
   * @author Giomani Designs (Development Team)
   * @param  string   $postcode a postcode
   * @return
   */
  public static function getByPostcodeArea($postcode) {
    $query = static::queryForWithDays();
    $parts = preg_split("/[0-9]/", $postcode);
    return $query->where('postCode', '=', $parts[0])->get();
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  int $catagoryId
   * @return Illuminate\Database\Query\Builder
   */
   private static function queryForWithDays() {
     return static::select(
       'routeDays.day',
       'routeName.id',
       'routeName.name'
     )
     ->leftJoin('routeName', 'routeName.id', '=', 'routeStops.routeID')
     ->leftJoin('routeDays', 'routeDays.route_id', '=', 'routeStops.routeID');
   }
}
