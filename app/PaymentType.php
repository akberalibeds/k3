<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;

class PaymentType extends Model
{
  /**
   * @var
   */
  public static $messages = [
    'description.required' => 'Required.',

    'payment_type.required' => 'Required.',
    'payment_type.max' => 'Must be less than 48 charatcers',
    'payment_type.min' => 'Must be 4 characters or more',
    'payment_type.unique' => 'There is already a type with that name.',
  ];
  /**
   * The table associated with the model.
   *
   * @var string
   */
  protected $table = 'payment_type';
  /**
   * Get the validation rules that apply to the request.
   *
   * @return array
   */
  public static $rules = [
    'description' => 'required',
    'payment_type' => 'required|min:3|max:48|unique:payment_type,payment_type',
  ];
  /**
   * returns id for given type
   */
  public static function id_type($type) {
    $type = PaymentType::select('id')->where('payment_type',$type)->get()->first();
    return $type['id'];
  }
  /**
   * returns type for given id
   */
  public static function type($id) {
    $type = PaymentType::select('payment_type')->where('id',$id)->get()->first();
    return $type['payment_type'];
  }
  public static function getAll()
  {
		if(Auth::user()->hasRole('manager') || Auth::user()->hasRole('paypal_group')){
			$paymentType = PaymentType::all();
		}
		else {
			$paymentType = PaymentType::where('admin',0)->get();
		}

		return $paymentType;
  }

}
