<?php

namespace App;

use DB;
use App\Order;
use App\Helper\Traits\QueryableModel;
use Illuminate\Database\Eloquent\Model;

class Routes extends Model
{
  use QueryableModel;

  public function orders() 
  {
    return $this->hasMany(Order::class, 'id_delivery_route');
  }

  public function dispatched()
  {
    return $this->hasMany(Dispatched::class, 'route', 'name');
  }

  public function getDispatchedCount($date = NULL)
  {
    $date? $date = (new \DateTime( $date))->format('d-M-Y') : $date  = \Carbon\Carbon::today()->format('d-M-Y');
    
    return  $this->dispatched()->where('date', '=', $date)->count();
  }

  public function getCompletion($date)
  {
    $oids = $this->dispatched()->where('date','=', $date )->pluck('oid');

    $statuses = Order::whereIn('id', $oids)->groupBy('orderStatus')->pluck('orderStatus')->toArray();

    if (count($statuses) == 1 && $statuses[0] == 'Delivered') return 'completed';

    if (! in_array('Delivered', $statuses) ) return 'none';

    return 'partial';

  }

  /**
   * The table associated with the model.
   *
   * @var string
   */
  protected $table = 'routeName';
  /**
   * @author Giomani Designs (Development Team)
   * @param  array   $queryStrings the query string
   * @return Illuminate\Pagination\LengthAwarePaginator
   */
  public static function getAll($queryStrings = []) {
    $query = static::select('*');
    if (count($queryStrings) > 0) {
      $query = parent::modForQueryString($query, $queryStrings = []);
    }
    return $query->orderBY('name', 'asc')->paginate(config('search.rpp'));
  }
  /**
   * get the routes for the route-days page
   *
   * @return Illuminate\Pagination\LengthAwarePaginator
   */
  public static function getRouteRoutes() {
    return Routes::select('id', 'name')
      ->where('is_route_route', '=', '1')
      ->orderBy('id', 'asc')
      ->get();
  }
  /**
   * get the routes for the route-days page
   *
   * @param int $id supplier id
   * @return Illuminate\Pagination\LengthAwarePaginator
   */
  public static function routeDaysSupplierRoutes($id) {
    return Routes::select(DB::raw('group_concat(day separator ";") as days'), 'id', 'name', 'routeName.max as max', 'supplier_id', 'van')
      ->where('name', 'not like', 'Route%')
      ->where(function ($query) use ($id) {
        $query->where('supplier_id', '=', $id)
              ->orWhereNull('supplier_id');
      })
      ->leftJoin('supplier_days', 'routeName.id', '=', 'supplier_days.route_id')
      ->groupBy('name')
      ->paginate(config('search.rpp'));
  }
  /**
   * Get the route data. Includes
   *
   * @param String $route
   * @param  $date
   */
  public static function routeDetails($route, $date) {
    Routes::select('o.id', 'o.lat', 'o.lng', 'o.dispatchType', 'o.p', 'o.done', 'o.companyName', 'o.startDate', 'o.deliverBy')
      ->select(DB::raw('if(o.direct = 1,d.name,c.dBusinessName) as name'))
      ->select(DB::raw('if(o.direct=1, d.town, dTown) as town)'))
      ->select(DB::raw('if(o.direct = 1, d.postcode, dPostcode) as postcode'))
      ->select(DB::raw('group_concat(s.itemCode) as items, group_concat(s.actual) as stockCount'))
      //->join('orders', )
      ->get();

    dd(DB::getQueryLog());
    // $sql .= 'select o.id, o.lat, o.lng, o.dispatchType, o.p, o.done, o.companyName, o.startDate, o.deliverBy, ';
    // $sql .= 'if(o.direct = 1,d.name,c.dBusinessName) as name, if(o.direct=1,d.town,dTown) as town, ';
    // $sql .= 'if(o.direct = 1,d.postcode,dPostcode) as postcode ,group_concat(s.itemCode) as items,group_concat(s.actual) as stockCount ';
    // $sql .= 'from orderItems i join orders o on i.oid=o.id ';
    // $sql .= 'join customers c on o.cid = c.id ';
    // $sql .= 'left join direct d on o.id = d.oid ';
    // $sql .= 'join stock_items s on i.itemid = s.id ';
    // $sql .= 'where o.orderStatus="Allocated" and id_delivery_route=' . $_REQUEST['route'] . ' ' . $date . ' group by o.id';
  }

  /**
   *
   */
  public static function setRouteDays ($id, $days, $max, $route) {
    DB::table('routeDays')->where('route_id', '=', $id)->delete();
    $rows = [];
    foreach ($days as $day) {
      $rows[] = ['route' => $route, 'day' => $day, 'max' => $max, 'route_id' => $id];
    }
    DB::table('routeDays')->insert($rows);
  }
  /*
  	returns id for given route
  */

  public static function id_route($route){

		$type = self::select('id')->where('name',$route)->get()->first();
	  	return $type['id'];

  }


  public static function route($id){

		$type = self::find($id);
	  	return $type->name;

  }

}
