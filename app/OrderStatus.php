<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderStatus extends Model
{
  /**
   * The table associated with the model.
   *
   * @var string
   */
  protected $table = 'order_status';
  
  
  
  
  /*
  	returns id for given status
  */
  
  public static function id_status($status){
	
		$status = self::select('id')->where('order_status',$status)->first();
	  	return $status['id'];
 
  }
  
  
  
   /*
  	returns status for given id
  */
    public static function status($id){
	
		$status = self::select('order_status')->where('id',$id)->first();
	  	return $status['order_status'];
		
  }
  
  
  
  
  
  
}
