<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Yajra\Datatables\Datatables;
use App\Http\Requests;
use Illuminate\Http\Request;
use Auth;

class WarehouseStock extends Model
{
  /**
   * The table associated with the model.
   *
   * @var string
   */
	protected $table = 'warehouse_stock';
	
	public $fillable = ['id', 'stock_parts_id', 'warehouse_id'];
  /**
   * @param  int $uid user id
   * @return Illuminate\Database\Eloquent\Collection
   */
  public $timestamps = false;
  
  public function warehouse() {
    return $this->belongsTo('App\Warehouse','warehouse_id','id');
  }


  public function stock_part() {
		return $this->belongsTo('App\StockParts','stock_parts_id', 'id');
  }
  
  public static function getStockCount(Request $request = null,$warehouse = null, $itemCode = ''){
  	
  	
  	//itemCode total warehouse....
  	
  	 $stocks = self::select(
  				'warehouse_stock.id',
  	 			'warehouse_stock.warehouse_id',
  	 			'warehouse_stock.stock_parts_id',
  				'stock_parts.part',
  	 			'stock_parts.parent_id',
  				'stock_items.itemCode',
  				\DB::raw("group_concat(warehouse_id,'|',warehouse_stock.quantity) as location")
  			)
  			->join('stock_parts','warehouse_stock.stock_parts_id','=','stock_parts.id')
  			->join('stock_items','stock_parts.parent_id','=','stock_items.id')
				->join('warehouses','warehouse_stock.warehouse_id','=','warehouses.id')
				->where('stock_items.isActive', 1)
  			->whereNotIn('stock_items.itemCode',["","0"])
			->groupBy('stock_items.itemCode','part');

  			
  		if($warehouse!=null){
  			$stocks->where('warehouse_stock.warehouse_id',$warehouse);
		  }
		  
		if($itemCode) {
			$stocks->where('stock_items.itemCode', 'LIKE', "%$itemCode%");
		}
  			
  			$stocks =  Datatables::of($stocks)->make(true);
  			
  			$data = $stocks->getData();
  			$item = [];
  			$box=[];
  			$pointer=null;
  			$counter=0;
  			$thisFull=[];
  			foreach($data->data as &$stock){
  				
  				
  				if(!isset($item[$stock->itemCode]) && $pointer){
  					$pointer->full = (count($box)==1) ? array_sum($item[$pointer->itemCode]) : min($thisFull);//min($item[$pointer->itemCode]);
  					$pointer = $stock;
  					$item = [];
  					$item[$stock->itemCode]=[];
  					$box=[];
  					$thisFull = [];
  				}
  				else if(!isset($item[$stock->itemCode]) && !$pointer) {
  					$pointer = $stock;
  					$item = [];
  					$item[$stock->itemCode]=[];
  					$box=[];
  					$thisFull = [];
  				}
  				
  				$box[] = $stock->part;
  				
	  			$locations = explode(",",$stock->location);
	  			$list = [];
	  			foreach($locations as $l){
	  				
	  				$a  = explode("|",$l);
	  				$list[$a[0]] = $a[1];
	  				$item[$stock->itemCode][]=$a[1];
	  			
	  			}
	  
	  			$stock->location = $list;
	  			$stock->quantity = array_sum($list);
	  			$stock->full = "";
	  			$thisFull[] = $stock->quantity;
	  			
	  			$counter++;
	  			if($counter==count($data->data)){
	  				$pointer->full = (count($box)==1) ? array_sum($item[$pointer->itemCode]) : min($thisFull);//min($item[$pointer->itemCode]);
	  			}
	  			
  			}
  			
  			$stocks->setData($data);
  			
  	return $stocks;
  }
  
  public static function map_warehouse_parts($parts,$warehouse) {
    $whparts = self::whereIn('stock_parts_id',array_keys($parts))->where('warehouse_id',$warehouse->id)->get();
    $map = [];
    foreach ($whparts as $part) {
      $map[$part['stock_parts_id']] = $part;
    }
    return $map;
  }
  
  
  public static function moveStockPart($part, $from_warehouse, $to_warehouse, $reason, $qty) {
    $parts = [$part->id => $part];
    if ($from_warehouse) {
      if ($to_warehouse) {
        return static::transferStock($parts, $from_warehouse, $to_warehouse, $qty);
      } else {
        return static::alterStock($parts, $from_warehouse, -$qty, $reason);
      }
    } else {
      return static::alterStock($parts, $to_warehouse, $qty, $reason);
    }
    
  }
  
  public static function canMoveStockItem($stockItem, $from_warehouse, $to_warehouse, $reason, $qty) {
		$parts = static::makeIndexed($stockItem->stockParts);
    $ok = true;
    if ($from_warehouse) {
      $from_warehouse_parts = self::map_warehouse_parts($parts, $from_warehouse);
      foreach ($from_warehouse_parts as $wh_part) {
        if ($wh_part->quantity - $qty < 0) {
          $ok =  false;
        }
      }
    }
    if ($to_warehouse) {
      $to_warehouse_parts = self::map_warehouse_parts($parts, $to_warehouse);
      foreach ($to_warehouse_parts as $wh_part) {
        if ($qty < 0 && $wh_part->quantity + $qty < 0) {
          $ok =  false;
        }
      }
    }
    return $ok;
  }
  
  public static function moveStockItem($stockItem, $from_warehouse, $to_warehouse, $reason, $qty) {
    $parts = static::makeIndexed($stockItem->stockParts);
    if ($from_warehouse) {
      if ($to_warehouse) {
        return static::transferStock($parts, $from_warehouse, $to_warehouse, $qty);
      } else {
        return static::alterStock($parts, $from_warehouse, -$qty, $reason);
      }
    } else {
      return static::alterStock($parts, $to_warehouse, $qty, $reason);
    }
  }
  
  public static function transferStock($stock_parts, $from_warehouse, $to_warehouse, $qty) {
    $stock_parts = static::makeIndexed($stock_parts);
    $staffid = Auth::user() ? Auth::user()->id : null;
    $logs = [];
    try {
    \DB::transaction(function() use ($stock_parts, $from_warehouse, $to_warehouse, $qty, $staffid, &$logs) {
			$from_warehouse_parts = self::map_warehouse_parts($stock_parts, $from_warehouse);
      $to_warehouse_parts = self::map_warehouse_parts($stock_parts, $to_warehouse);
      foreach ($from_warehouse_parts as $partid => $wh_frompart) {
        $part = $stock_parts[$partid];
        
        $wh_topart = $to_warehouse_parts[$partid];
        
        if ($wh_frompart->quantity-$qty < 0 || $wh_topart->quantity + $qty < 0 ) {
          throw new \OutOfBoundsException("Can't make {$part->stockItem->itemCode} negative");
        }
        $wh_frompart->quantity -= $qty;
        $wh_frompart->save();
        $wh_topart->quantity += $qty;
        $wh_topart->save();
        
        $log=new WarehouseLog([
          'warehouse_id' => $from_warehouse->id,
          'warehouse_stock_id' => $wh_frompart->id,
          'staff_id' => $staffid,
          'message' => "Removed part {$part->stockItem->itemCode} ($part->part) qty: $qty To: $to_warehouse->location"
        ]);
        $log->save();
        $logs[]=$log;
        $log=new WarehouseLog([
          'warehouse_id' => $to_warehouse->id,
          'warehouse_stock_id' => $wh_topart->id,
          'staff_id' => $staffid,
          'message' => "Added part {$part->stockItem->itemCode} ($part->part) qty: $qty From: $from_warehouse->location"
        ]);
        $log->save();
        $logs[]=$log;
      }
    });
    } catch (Exception $ex) {
      throw $ex;
    }
    return $logs;
  }
  
  public static function alterStock($stock_parts, $warehouse, $qty, $reason) {
    $stock_parts = static::makeIndexed($stock_parts);
    $staffid = Auth::user() ? Auth::user()->id : null;
    $logs = [];
    \DB::transaction(function() use ($stock_parts, $warehouse, $qty, $reason, $staffid,&$logs) {
      $warehouse_parts = self::map_warehouse_parts($stock_parts, $warehouse);
      foreach ($warehouse_parts  as $partid => $wh_part) {
        $part = $stock_parts[$partid];
        
        //Allow negative stock to be increased, but don't allow stock to be made negative or negatvie stock to be decreased
        if ($qty < 0 && $wh_part->quantity + $qty <0) {
          throw new \OutOfBoundsException("Can't make {$part->stockItem->itemCode} negative");
        }
        
        $wh_part->quantity += $qty;
        $wh_part->save();
        
        if ($qty>0) {
          $message = "Added part {$part->stockItem->itemCode} ($part->part) qty: $qty due to $reason";
        } else {
          $q = -$qty;
          $message = "Removed part {$part->stockItem->itemCode} ($part->part) qty: $q due to $reason";
        }
        
        $log=new WarehouseLog([
          'warehouse_id' => $warehouse->id,
          'warehouse_stock_id' => $wh_part->id,
          'staff_id' => $staffid,
          'message' => $message
        ]);
        $log->save();
        $logs[]=$log;

      }
    });
    return $logs;
  }
  
  public static function addPart($request){
  		
  		$part = self::find($request->get('part'));
  		$part->quantity+=$request->get('qty');
  		$part->save();
  		
  		$stockPart = StockParts::where('id',$part->stock_parts_id)->first();
  		$stock = StockItem::find($stockPart->parent_id);
  		
  		$log = new WarehouseLog();
  		$log->message = "Added part $stock->itemCode ($stockPart->part) qty: ". $request->get('qty') . " From: ".$request->get('log');
  		$log->warehouse_id=$request->get('warehouse');
  		$log->warehouse_stock_id=$request->get('part');
  		$log->staff_id = Auth::user()->id;
  		$log->save();
  		
  }
  
  
  public static function removePart($request){
  
  	$part = self::find($request->get('part'));
  	$part->quantity-=$request->get('qty');
  	$part->save();
  
  	$stockPart = StockParts::where('id',$part->stock_parts_id)->first();
  	$stock = StockItem::find($stockPart->parent_id);
  
  	$log = new WarehouseLog();
  	$log->message = "Removed part $stock->itemCode ($stockPart->part) qty: ". $request->get('qty') . " To: ".$request->get('log');
  	$log->warehouse_id=$request->get('warehouse');
  	$log->warehouse_stock_id=$request->get('part');
  	$log->staff_id = Auth::user()->id;
  	$log->save();
  
  }
  
  
  
  public static function addItem($request){
  	
  	$stock = StockItem::find($request->get("item"));
  	
  	$parts = StockParts::select('id')->where('parent_id',$request->get('item'))->get();
	$ids = [];
  	foreach($parts as $part){	
  		$ids[]=$part->id;
  	}

  	$parts = self::whereIn('stock_parts_id',$ids)->where('warehouse_id',$request->get('warehouse'))->get();
  	
  	foreach ($parts as &$part){
  		$part->quantity+=$request->get('qty');
  		$part->save();
  	}
  
  	$log = new WarehouseLog();
  	$log->message = "Added Item $stock->itemCode qty: ". $request->get('qty') . " From: ".$request->get('log');
  	$log->warehouse_id=$request->get('warehouse');
  	$log->warehouse_stock_id=$request->get('item');
  	$log->staff_id = Auth::user()->id;
  	$log->save();
  
  }
  
  public static function removeItem($request){
  	 
  	$stock = StockItem::find($request->get("item"));
  	 
  	$parts = StockParts::select('id')->where('parent_id',$request->get('item'))->get();
  	$ids = [];
  	foreach($parts as $part){
  		$ids[]=$part->id;
  	}
  
  	$parts = self::whereIn('stock_parts_id',$ids)->where('warehouse_id',$request->get('warehouse'))->get();
  	 
  	foreach ($parts as &$part){
  		$part->quantity-=$request->get('qty');
  		$part->save();
  	}
  
  	$log = new WarehouseLog();
  	$log->message = "Removed Item $stock->itemCode qty: ". $request->get('qty') . " To: ".$request->get('log');
  	$log->warehouse_id=$request->get('warehouse');
  	$log->warehouse_stock_id=$request->get('item');
  	$log->staff_id = Auth::user()->id;
  	$log->save();
  
  }
  
  
  public static function getStockCountCSV(Request $request = null,$warehouse = null){
  	 
  	 set_time_limit(0);
  	//itemCode total warehouse....
  	 
  	$stocks = self::select(
  			'warehouse_stock.id',
  			'warehouse_stock.warehouse_id',
  			'warehouse_stock.stock_parts_id',
  			'stock_parts.part',
  			'stock_parts.parent_id',
  			'stock_items.itemCode',
  			\DB::raw("group_concat(warehouse_id,'|',warehouse_stock.quantity) as location")
  			)
  			->join('stock_parts','warehouse_stock.stock_parts_id','=','stock_parts.id')
  			->join('stock_items','stock_parts.parent_id','=','stock_items.id')
				->join('warehouses','warehouse_stock.warehouse_id','=','warehouses.id')
				->where('stock_items.isActive', 1)
  			->whereNotIn('stock_items.itemCode',["","0"])
  			->groupBy('stock_items.itemCode','part');
  				
  				
  			if($warehouse!=null){
  				$stocks->where('warehouse_stock.warehouse_id',$warehouse);
  			}
  				
  			$stocks =  $stocks->get();
  				
  			$data = $stocks;
  			$item = [];
  			$box=[];
  			$pointer=null;
  			$counter=0;
  			$thisFull=[];
  			foreach($data as &$stock){
  
  
  				if(!isset($item[$stock->itemCode]) && $pointer){
  					$pointer->full = (count($box)==1) ? array_sum($item[$pointer->itemCode]) : min($thisFull);//min($item[$pointer->itemCode]);
  					$pointer = $stock;
  					$item = [];
  					$item[$stock->itemCode]=[];
  					$box=[];
  					$thisFull = [];
  				}
  				else if(!isset($item[$stock->itemCode]) && !$pointer) {
  					$pointer = $stock;
  					$item = [];
  					$item[$stock->itemCode]=[];
  					$box=[];
  					$thisFull = [];
  				}
  
  				$box[] = $stock->part;
  
  				$locations = explode(",",$stock->location);
  				$list = [];
  				foreach($locations as $l){
  						
  					$a  = explode("|",$l);
  				
  					if(isset($a[1])){
	  					$list[$a[0]] = $a[1];
	  					$item[$stock->itemCode][]=$a[1];
  					}
  				}
  				 
  				$stock->location = $list;
  				$stock->quantity = array_sum($list);
  				$stock->full = "";
  				$thisFull[] = $stock->quantity;
  
  				$counter++;
  				if($counter==count($data)){
  					$pointer->full = (count($box)==1) ? array_sum($item[$pointer->itemCode]) : min($thisFull);//min($item[$pointer->itemCode]);
  				}
  
  			}
  				
  			$stocks = $data;
  				
  			return $stocks;
  }
  
  static function makeIndexed($arr) {
    $result = [];
    foreach ($arr as $item) {
      $result[$item->id]=$item;
    }
    return $result;
  }
}
