<?php

namespace App;

use App\Helper\Traits\QueryableModel;
use Illuminate\Database\Eloquent\Model;

class VehicleCost extends Model
{
  use QueryableModel;
  /**
   * @var
   */
  public static $rules = [
    'store' => [
      'cost' => 'required|numeric|min:0|max:10000',
      'date_incurred' => 'required|date',
      'vehicle_cost_type_id' => 'required|numeric',
      'vehicle_id' => 'required|integer',
    ],
    'update' => [
      'id' => 'required|integer',
      'cost' => 'required|numeric|min:0|max:10000',
      'date_incurred' => 'required|date',
      'vehicle_cost_type_id' => 'required|integer',
      'vehicle_id' => 'required|integer',
    ],
  ];
  /**
   * The table associated with the model.
   *
   * @var string
   */
  protected $table = 'vehicle_costs';
  /**
   * @author Giomani Designs
   * @param  array   $queryStrings the query string
   * @return Illuminate\Pagination\LengthAwarePaginator
   */
  public static function getVehicleCosts ($queryStrings = []) {
    $query = static::queryForVehicleCosts();
    if (count($queryStrings) > 0) {
      $query = parent::modForQueryString($query, $queryStrings);
    }
    return $query->orderBy('vehicle_costs.id', 'desc')->paginate(config('search.rpp'));
  }
  /**
   * @author Giomani Designs
   * @return Illuminate\Database\Eloquent\Collection
   */
  public static function queryForVehicleCosts () {
    return static::select(
        'vehicles.reg',
        'vehicle_cost_types.type',
        'vehicle_costs.created_at',
        'vehicle_costs.cost',
        'vehicle_costs.date_incurred',
        'vehicle_costs.id',
        'vehicle_costs.mileage',
        'vehicle_costs.notes',
        'vehicle_costs.vehicle_id'
      )
      ->leftJoin('vehicles', 'vehicles.id', '=', 'vehicle_costs.vehicle_id')
      ->leftJoin('vehicle_cost_types', 'vehicle_cost_types.id', '=', 'vehicle_costs.vehicle_cost_type_id');
  }
  /**
   * @author Giomani Designs
   * @param  int $id vehicle cost id
   * @return Illuminate\Database\Eloquent\Collection
   */
  public static function queryForVehiclesCosts ($id) {
    return static::select(
        'vehicles.id',
        'vehicles.reg',

        'vehicle_cost_types.type',

        'vehicle_costs.created_at',
        'vehicle_costs.date_incurred',
        'vehicle_costs.cost',
        'vehicle_costs.id',
        'vehicle_costs.mileage',
        'vehicle_costs.notes'
      )
      ->leftJoin('vehicles', 'vehicles.id', '=', 'vehicle_costs.vehicle_id')
      ->leftJoin('vehicle_cost_types', 'vehicle_cost_types.id', '=', 'vehicle_costs.vehicle_cost_type_id')
      ->where('vehicle.id', '=', $id);
  }
}
