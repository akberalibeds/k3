<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Container extends Model
{
  /**
   * The table associated with the model.
   *
   * @var string
   */
  protected $table = 'containers';
  
  /**
   * The fillable fields associated with the model.
   *
   * @var array
   */
  public $fillable = ['containerID','loaded','eta','items'];
  
  
}
