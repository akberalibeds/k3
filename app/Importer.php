<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Importer extends Model
{
    protected $fillable = [
        'name',
        'class_name'
    ];

    public function companies()
    {
        return $this->hasMany(Company::class);
    }

    
}
