<?php

function helperGetChainedProperty($object,$chainedProperty) {

    if( is_callable($chainedProperty) ) {

        return $chainedProperty($object);
    }

    $chain = explode('->',$chainedProperty);

    $val = $object;

    foreach ($chain as $property) {
        if (!is_object($val)) {
          return null;
        }
        $val = $val->{$property};
    }
    return $val;
}

