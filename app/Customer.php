<?php

namespace App;

use DB;
use App\Order;
use App\Helper\Traits\QueryableModel;
use Illuminate\Database\Eloquent\Model;
use Symfony\Component\Console\Tests\CustomApplication;

class Customer extends Model
{
  protected $guarded = [];
  
  use QueryableModel;

  public function orders()
  {
    return $this->hasMany(Order::class, 'cid');
  }
  
  /**
   * The table associated with the model.
   *
   * @var string
   */
  protected $table = 'customers';

  public $timestamps = false;

  public static $customerID;

  public static function getCustomer($id=false) {
    $id = ($id) ? $id : self::$customerID;
    if(isset($_REQUEST['oid'])) {
      Order::update(['cid' => $id])->where(['id' => $_REQUEST['oid']]);
    }
    $row = Customer::where(['id' => $id])->first();
    return $row;
  }
  public static function insertIfNotExists(array $data) {
    $id = Customer::select('id')->where($data)->get()->first();
    if(count($id)==0) {
      $data['numerical_pc'] = self::getNumericalValue($data['dPostcode']);
      $id = Customer::insertGetId($data);
    } else {
      $id = $id['id'];
    }

    self::$customerID = $id;
    return new static;
  }
  /**
   * @author Giomani Designs
   */
  public static function nameSearch($query) {
    return static::select(
      'id',
      DB::raw('(concat (dBusinessName, " - ", postcode)) as result')
    )
    ->where('dBusinessName', 'like', $query . '%')
    ->orWhere('businessName', 'like', $query . '%')
    ->get();
  }
  /**
   * @author Giomani Designs
   */
  public static function postcodeSearch($query) {
    return static::select(
      'id',
      // 'postcode as result',
      DB::raw('(concat (postcode, " - ", dBusinessName)) as result')
    )
    ->where('postcode', 'like', $query . '%')
    ->orWhere('dPostcode', 'like', $query . '%')
    ->get();
  }

  public static function updateCustomer(array $data,$id) {
    Customer::where(['id' => $id])->update($data);
    Customer::where(['id' => $id])->update(['numerical_pc' => 0]);
    self::$customerID=$id;
    return new static;
  }
  
  
  public static function getNumericalValue($string)
  {
  
  	$chars = 	['a' => 1 ,'b' => 2,'c' => 3,'d' => 4,'e' => 5,'f' => 6,'g' => 7,'h' => 8,'i' => 9,'j' => 10,'k' => 11,'l' => 12,'m' => 13,'n' => 14,'o' => 15,'p' => 16,'q' =>17,'r' => 18,'s' => 19,'t' => 20,'u' => 21,'v' => 22,'w' => 23,'x' => 24,'y' => 25,'z' => 26];
  	
  	$numerical = '';
  	
  	for($i=0;$i<strlen($string);$i++){
  		if($string[$i]!=" "){
  
  			if(!is_numeric($string[$i])){
  				if(array_key_exists(strtolower($string[$i]), $chars)){
  					$numerical.= $chars[strtolower($string[$i])];
  				}
  			}
  			else{
  				$numerical.= $string[$i];
  			}
  				
  		}
  	}
  
  	return $numerical;
  }
}
