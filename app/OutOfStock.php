<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;

class OutOfStock extends Model
{
  /**
   * The table associated with the model.
   *
   * @var string
   */
  protected $table = 'out_of_stock';



  public static function add($item){
  	
  		$a = new self();
  		$a->item_id=$item;
  		$a->save();
  	
  }

}
