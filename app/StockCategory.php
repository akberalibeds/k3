<?php

namespace App;

use App\Helper\Traits\QueryableModel;
use Illuminate\Database\Eloquent\Model;

class StockCategory extends Model
{
  use QueryableModel;
  /**
   * The table associated with the model.
   *
   * @var string
   */
  protected $table = 'categories';
  /**
   * Get the comments for the blog post.
   */
  public function stockItems() {
   return $this->hasMany('App\StockItem', 'category_id', 'id');
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  array   $queryStrings the query string
   * @return \Illuminate\Http\Response
   */
  public static function getCategories($queryStrings) {
    $query = static::select('*');
    if (count($queryStrings) > 0) {
      $query = parent::modForQueryString($query, $queryStrings);
    }
    return $query->orderBY('name')->paginate(config('search.rpp'));
  }
  
  
  public static function categoryById($id) {
  	
	$row = self::find($id);
	return $row->name;
	
  }
}
