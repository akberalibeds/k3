<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Order;
use App\OrderNotes;

use Auth;

class CreditNote extends Model
{
  /**
   * The table associated with the model.
   *
   * @var string
   */
  protected $table = 'credit_notes';
  
  
  
  
  
  public static function creditOrder($id){
	
	  $order = Order::find($id);
	  
	  $credit = new self;
	  $credit->amount = $order->total;
	  $credit->order_id  = $id;
	  $credit->seller_id = $order->id_seller; 
	  $credit->user_id = Auth::user()->id;
	  $credit->save();
	  
	  OrderNotes::addNote("Credited Order",$id);
	  Order::cancelOrder($id);
  	  
  }
  
  
  public static function getCredits($id){
	  
	  return self::select('id','order_id','amount as total',\DB::raw('amount-used as amount'),'created_at')
	  			  ->where('seller_id',$id)
	  			  ->where(\DB::raw("used"),'<',\DB::raw("amount"))
				  ->get();
	  
  }
  
  
  
  public static function useCredits($order,$id){
	  
	$order 	= Order::find($order);
	$credit 	= self::find($id);
	
	$outstanding = $order->total - $order->paid;
	$available = $credit->amount - $credit->used;
	
	if($outstanding>$available){
		OrderNotes::addNote("Used credit amount $available from $credit->id",$order->id);
		$order->paid += $available;
		$credit->used = $credit->amount;	
	}
	else{ //($outstanding<$available){
		OrderNotes::addNote("Used credit amount $outstanding from $credit->id",$order->id);
		$order->paid   = $order->total;
		$credit->used += $outstanding; 	
	}
	
	
	
	$order->save();
	$credit->save();
	
	
	return ['order' => $order, 'credit' => $credit];  
  }
  
  
}
