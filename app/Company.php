<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class Company extends Model
{
  
  protected $guarded = [];

  /**
   * The table associated with the model.
   *
   * @var string
   */
  protected $table = 'company';

  public function orders()
  {
    return $this->hasMany(Order::class, 'id_company');
  }
  /*
   * returns id for given type
   */
  public static function id_company ($company) {
    // JF 20160825
    // $type = Company::select('id')->where('company_name',$company)->get()->first();
    try {
      $type = Company::select('id')->where('company_name', $company)->firstOrFail();
    } catch (ModelNotFoundException $e) {
      return 5;
    }
    return $type['id'];
  }
  /*
   * returns type for given id
   */
  public static function company ($id) {
    $type = Company::select('company_name')->where('id',$id)->get()->first();
    return $type['company_name'];
  }

  public function importer()
  {
    return $this->belongsTo(Importer::class);
  }
}
