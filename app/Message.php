<?php
namespace App;

use DB;
use Auth;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Message extends Model
{
  use SoftDeletes;
  /**
   * The table associated with the model.
   *
   * @var string
   */
  protected $table = 'messages';
  /**
   * get conversations
   * @author Giomani Designs
   * @return Illuminate\Pagination\LengthAwarePaginator
   */
  public static function getConversations() {
    return static::queryForConversations()
      ->where('to_id', '=', Auth::user()->id)
      ->paginate(config('search.rpp'));
  }
  /**
   * get conversations
   * @author Giomani Designs
   * @param integer $id the id of the conversation
   * @return Illuminate\Pagination\LengthAwarePaginator
   */
  public static function getConversationMessages($id) {
    return static::queryForConversationMessages($id)
      ->where(function ($query) {
        $query
          ->orWhere('from_id', '=', Auth::user()->id)
          ->orWhere('to_id', '=', Auth::user()->id);
      })
      ->get();
  }
  /**
   * get conversations
   * @author Giomani Designs
   * @param array $ids the ids of the read messages
   * @return Illuminate\Pagination\LengthAwarePaginator
   */
  public static function markAsRead($ids) {
    return DB::table('messages')
      ->whereIn('conversation_id', $ids)
      ->update(['is_read' => 1]);
  }
  /**
   * @author Giomani Designs
   * @param integer $id the id of the conversation
   * @return Illuminate\Database\Query\Builder
   */
  public static function queryForConversationMessages($id) {
    return Message::select(
        'messages.id',
        'conversation_id',
        'messages.created_at',
        'from_id',
        'is_read',
        'message',
        'subject',
        'to_id',
        'users.email as from_email',
        'users.name as from_name'
      )
      ->where('conversation_id', '=', $id)
      ->leftJoin('users', 'users.id', '=', 'from_id')
      ->orderBy('created_at', 'desc');
  }
  /**
   * @author Giomani Designs
   * @return Illuminate\Database\Query\Builder
   */
  private static function queryForConversations() {
    return Message::select(
        'messages.id',
        'conversation_id',
        \DB::raw("messages.created_at"),
        'from_id',
        'is_read',
        'message',
        'subject',
        'to_id',
        'users.email as from_email',
        'users.name as from_name',
        DB::raw('GROUP_CONCAT(messages.id separator ";") as ids'),
        DB::raw('GROUP_CONCAT(is_read separator ";") as is_reads'),
        DB::raw('COUNT(messages.id) as count')
      )
      ->leftJoin('users', 'users.id', '=', 'from_id')
      ->groupBy('conversation_id')
      ->orderBy('created_at', 'desc');
  }
  /**
   * @author Giomani Designs
   * @param string  $subject        the subject of the message
   * @param string  $messageText    the text of the message
   * @param string  $to             the user id of the to address
   * @param integer $conversationId the id of the conversation the reply is a part of
   * @return Illuminate\Database\Query\Builder
   */
  public static function send($subject, $messageText, $to, $conversationId) {
    $message = new Message;
    if (!isset($conversationId) || $conversationId == '') {
      $conversationId = 0;
    }
    $message->conversation_id = $conversationId;
    $message->from_id = Auth::user()->id;
    $message->is_read = 0;
    $message->message = $messageText;
    $message->to_id = $to;
    $message->subject = $subject;
    $message->save();
    if ($conversationId == 0) {
      $message->conversation_id = $message->id;
      $message->save();
    }
  }
  
  /**
   * get New
   * @author Giomani Designs
   * @param integer $id the id of the conversation
   * @return Illuminate\Pagination\LengthAwarePaginator
   */
  public static function getNew() {
  	$messages =  static::queryForConversations()
  			->where('to_id',Auth::user()->id)
  			->where('new',1)
  			->get();
  	
  	return $messages;
  }
  
  public static function shownNew($ids) {
  	self::whereIn('id',explode(":", $ids))->update(['new' => 0]);
  	return [];
  }
  
}
