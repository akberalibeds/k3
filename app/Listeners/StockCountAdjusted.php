<?php
// @author Giomani Designs (Development Team)
//
namespace App\Listeners;

use App\ActionLog;
use App\Events\StockCountAdjustment;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class StockCountAdjusted
{
  /**
   * @author Giomani Designs (Development Team)
   * @return void
   */
  public function __construct() {
    //
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  StockLevelAlteration  $event
   * @return boolean
   */
  public function handle(StockCountAdjustment $event) {
    $log = new ActionLog;
    $log->action = ActionLog::STOCK_ITEM_COUNT_ADJUSTMENT;
    $log->more = $event->id . ';' . $event->is . ';' . $event->itemCode . ';' . $event->was;
    $log->save();
  }
}
