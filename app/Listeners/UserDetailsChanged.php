<?php
// @author Giomani Designs (Development Team)
//
namespace App\Listeners;

use App\ActionLog;
use App\Events\UserDetailsChange;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class UserDetailsChanged
{
  /**
   * @author Giomani Designs (Development Team)
   * @return void
   */
  public function __construct() {
    //
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  UserDetailsChange  $event
   * @return void
   */
  public function handle(UserDetailsChange $event) {
    $log = new ActionLog;
    $log->action = ActionLog::USER_CHANGED_DETAILS;
    $log->more = $event->$details . ';' .$event->$details;
    $log->save();
  }
}
