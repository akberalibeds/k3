<?php

namespace App\Listeners;

use Auth;
use App\ActionLog;
use App\Events\UserChangingPassword;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class UserChangedPassword
{
  /**
   * @return void
   */
  public function __construct() {
    //
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  UserChangindPassword  $event
   * @return void
   */
  public function handle(UserChangingPassword $event) {
    $log = new ActionLog;
    $log->action = ActionLog::USER_CHANGED_PASSWORD;
    $log->more = Auth::user()->id;
    $log->save();
  }
}
