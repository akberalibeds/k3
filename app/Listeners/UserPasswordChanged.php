<?php
// @author Giomani Designs (Development Team)
//
namespace App\Listeners;

use App\ActionLog;
use App\Events\UserPasswordChange;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class UserPasswordChanged
{
  /**
   * @author Giomani Designs (Development Team)
   * @return void
   */
  public function __construct() {
    //
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  UserChange  $event
   * @return void
   */
  public function handle(UserPasswordChange $event) {
    $log = new ActionLog;
    $log->action = ActionLog::USER_PASSWORD_CHANGED;
    $log->more = $event->userId;
    $log->save();
  }
}
