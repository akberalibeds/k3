<?php
// @author Giomani Designs (Development Team)
//
namespace App\Listeners;

use App\ActionLog;
use App\Events\InvoiceWasPaid;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class InvoicePaid
{
  /**
   * @author Giomani Designs (Development Team)
   * @return void
   */
  public function __construct() {
    //
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  InvoiceWasPaid  $event
   * @return void
   */
  public function handle(InvoiceWasPaid $event) {
    $log = new ActionLog;
    $log->action = ActionLog::INVOICE_PAID;
    $log->more = $event->invoiceId . ';' . $event->orderId;
    $log->save();
  }
}
