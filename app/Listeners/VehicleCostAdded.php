<?php
// @author Giomani Designs (Development Team)
//
namespace App\Listeners;

use App\ActionLog;
use App\Listeners\BaseListener;

class VehicleCostAdded extends BaseListener
{
  /**
   * @var
   */
  public $kvs = ActionLog::VEHICLE_COST_ADDED;
}
