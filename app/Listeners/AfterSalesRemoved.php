<?php

namespace App\Listeners;

use App\ActionLog;
use App\Events\AfterSalesRemoving;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class AfterSalesRemoved
{
  /**
   * Create the event listener.
   *
   * @return void
   */
  public function __construct() {
    //
  }

  /**
   * Handle the event.
   *
   * @param  AfterSalesRemoving  $event
   * @return void
   */
  public function handle(AfterSalesRemoving $event) {
    $log = new ActionLog;
    $log->action = ActionLog::ORDER_AFTER_SALES_REMOVED;
    $log->more = $event->$orderId;
    $log->save();
  }
}
