<?php
// @author Giomani Designs (Development Team)
//
namespace App\Listeners;

use App\ActionLog;
use App\Events\IpAddressAdding;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class IpAddressAdded
{
  /**
   * @author Giomani Designs (Development Team)
   * @return void
   */
  public function __construct() {
    //
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  IpAddressAdding  $event
   * @return void
   */
  public function handle(IpAddressAdding $event) {
    $log = new ActionLog;
    $log->action = ActionLog::ROUTES_MARKED_AS_DELIVERED;
    $log->more = $event->ipAddress;
    $log->save();
  }
}
