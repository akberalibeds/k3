<?php
// @author Giomani Designs (Development Team)
//
namespace App\Listeners;

use App\ActionLog;
use App\Events\UserRolesPermissionsChange;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class UserRolesPermissionsChanged
{
  /**
   * @author Giomani Designs (Development Team)
   * @return void
   */
  public function __construct() {
    //
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  UserRolesPermissionsChange  $event
   * @return void
   */
  public function handle(UserRolesPermissionsChange $event) {
    $log = new ActionLog;
    $log->action = ActionLog::USER_ROLES_PERMISSIONS_CHANGED;
    $log->more = $event->added . ';' . $event->removed . ';' . $event->userId;
    $log->save();
  }
}
