<?php
// @author Giomani Designs (Development Team)
//
namespace App\Listeners;

use App\ActionLog;
use App\Events\BaseEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class BaseListener
{
  /**
   * @author Giomani Designs (Development Team)
   * @param  BaseEvent  $event
   * @return void
   */
  public function handle(BaseEvent $event) {
    $log = new ActionLog;
    $log->action = property_exists($this, 'kvs') ? $this->kvs : '[]';
    $log->more = implode(';', $event->kvs);
    $log->save();
  }
}
