<?php
// @author Giomani Designs (Development Team)
//
namespace App\Listeners;

use App\ActionLog;
use App\Events\StockCountAdding;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class StockCountAdded
{
  /**
   * @author Giomani Designs (Development Team)
   * @return void
   */
  public function __construct() {
    //
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  StockCountAdding  $event
   * @return void
   */
  public function handle(StockCountAdding $event) {
    $log = new ActionLog;
    $log->action = ActionLog::STOCK_ITEM_COUNT_ADDITION;
    $log->more = $event->id . ';' . $event->is . ';' . $event->itemCode . ';' . $event->was;
    $log->save();
  }
}
