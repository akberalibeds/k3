<?php
// @author Giomani Designs (Development Team)
//
namespace App\Listeners;

use App\ActionLog;
use App\Events\OrderCancellation;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class OrderCancelled
{
  /**
   * @author Giomani Designs (Development Team)
   * @return void
   */
  public function __construct() {
    //
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  OrderCancellation  $event
   * @return void
   */
  public function handle(OrderCancellation $event) {
    $log = new ActionLog;
    $log->action = ActionLog::ORDER_CANCELLED;
    $log->more = $event->orderId;
    $log->save();
  }
}
