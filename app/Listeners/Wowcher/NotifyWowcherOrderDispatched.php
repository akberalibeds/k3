<?php

namespace App\Listeners\Wowcher;

use App\Helper\Wowcher\Wowcher;
use App\Events\Wowcher\OrderDispatched;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class NotifyWowcherOrderDispatched
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  OrderDispatched  $event
     * @return void
     */
    public function handle(OrderDispatched $event)
    {
        (new Wowcher())->updateOrderStatus($event->order,'dispatched');
    }
}
