<?php

namespace App\Listeners\Wowcher;

use App\Helper\Wowcher\Wowcher;
use App\Events\Wowcher\OrderReceivedByMerchant;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class NotifyWowcherOrderCreated
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  OrderCreated  $event
     * @return void
     */
    public function handle(OrderReceivedByMerchant $event)
    {
        (new Wowcher())->updateOrderStatus($event->order,'Processing Order');
    }
}
