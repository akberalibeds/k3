<?php

namespace App\Listeners\Wowcher;

use App\Helper\Wowcher\Wowcher;
use App\Events\Wowcher\OrderReadyForDispatch;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class NotifyWowcherOrderReadyForDispatch
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  OrderReadyForDispatch  $event
     * @return void
     */
    public function handle(OrderReadyForDispatch $event)
    {
        (new Wowcher())->updateOrderStatus($event->order,'ready for dispatch');
    }
}
