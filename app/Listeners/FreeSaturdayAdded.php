<?php
// @author Giomani Designs (Development Team)
//
namespace App\Listeners;

use App\ActionLog;
use App\Events\FreeSaturdayAdding;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class FreeSaturdayAdded
{
  /**
   * @author Giomani Designs (Development Team)
   * @return void
   */
  public function __construct() {
    //
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  FreeSaturdayAdding  $event
   * @return void
   */
  public function handle(FreeSaturdayAdding $event) {
    $log = new ActionLog;
    $log->action = ActionLog::FREE_SATURDAY_ADDED;
    $log->more = $event->saturday;
    $log->save();
  }
}
