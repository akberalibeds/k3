<?php
// @author Giomani Designs (Development Team)
//
namespace App\Listeners;

use App\ActionLog;
use App\Events\MarkingAsDelivered;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class MarkedAsDelivered
{
  /**
   * @author Giomani Designs (Development Team)
   * @return void
   */
  public function __construct() {
    //
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  MarkingAsDelivered  $event
   * @return void
   */
  public function handle(MarkingAsDelivered $event) {
    $log = new ActionLog;
    $log->action = ActionLog::ROUTES_MARKED_AS_DELIVERED;
    $log->more = $event->ids;
    $log->save();
  }
}
