<?php

namespace App\Listeners;

use App\ActionLog;
use App\Events\BulletinCreating;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class BulletinCreated
{
  /**
   * Create the event listener.
   *
   * @return void
   */
  public function __construct() {
    //
  }

  /**
   * Handle the event.
   *
   * @param  BulletinCreating  $event
   * @return void
   */
  public function handle(BulletinCreating $event) {
    $log = new ActionLog;
    $log->action = ActionLog::BULLETIN_ADDED;
    $log->more = $event->bulletinId . ';' . $event->title;
    $log->save();
  }
}
