<?php
// @author Giomani Designs (Development Team)
//
namespace App\Listeners;

use App\ActionLog;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Events\SystemRolesPermissionsChange;

class SystemRolesPermissionsChanged
{
  /**
   * @author Giomani Designs (Development Team)
   * @return void
   */
  public function __construct() {
      //
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  SystemRolesPermissionsChange  $event
   * @return void
   */
  public function handle(SystemRolesPermissionsChange $event) {
    $log = new ActionLog;
    $log->action = ActionLog::SYSTEM_ROLES_PERMISSIONS_CHANGED;
    $log->more = $event->adds. '|' . $event->removes;
    $log->save();
  }
}
