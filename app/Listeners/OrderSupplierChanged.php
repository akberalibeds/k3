<?php

namespace App\Listeners;

use App\ActionLog;
use App\Events\OrderSupplierChanging;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class OrderSupplierChanged
{
  /**
   * @return void
   */
  public function __construct() {

  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  OrderSuplierChanging  $event
   * @return void
   */
  public function handle(OrderSupplierChanging $event) {
    $log = new ActionLog;
    $log->action = ActionLog::ORDER_CHANGED_SUPPLIER;
    $log->more = $event->orderId . ';' . $event->supplierFrom . ';' . $event->supplierTo;
    $log->save();
  }
}
