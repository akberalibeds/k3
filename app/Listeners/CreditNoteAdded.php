<?php
// @author Giomani Designs (Development Team)
//
namespace App\Listeners;

use App\ActionLog;
use App\Events\CreditNoteAdding;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class CreditNoteAdded
{
  /**
   * @author Giomani Designs (Development Team)
   * @return void
   */
  public function __construct() {
    //
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  CreditNoteAdding  $event
   * @return void
   */
  public function handle(CreditNoteAdding $event) {
    $log = new ActionLog;
    $log->action = ActionLog::ORDER_CREDIT_NOTE_ADDED;
    $log->more = $event->saturday . ';' . $event->id . ';' . $event->value . ';' . $event->customer . ';' . $event->orderId;
    $log->save();
  }
}
