<?php
// @author Giomani Designs (Development Team)
//
namespace App\Listeners;

use App\ActionLog;
use App\Events\OrderUnProcessing;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class OrderUnProcessed
{
  /**
   * @author Giomani Designs (Development Team)
   * @return void
   */
  public function __construct() {
    //
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  OrderUnProcessing  $event
   * @return void
   */
  public function handle(OrderUnProcessing $event) {
    $log = new ActionLog;
    $log->action = ActionLog::ORDER_UNPROCESSED;
    $log->more = $event->orderId;
    $log->save();
  }
}
