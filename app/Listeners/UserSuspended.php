<?php

namespace App\Listeners;

use App\ActionLog;
use App\Events\UserSuspending;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class UserSuspended
{
  /**
   * @return void
   */
  public function __construct()
  {
    //
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  UserSuspending  $event
   * @return void
   */
  public function handle(UserSuspending $event) {
    $log = new ActionLog;
    $log->action = ActionLog::USER_SUSPENDED;
    $log->more = $event->userId;
    $log->save();
  }
}
