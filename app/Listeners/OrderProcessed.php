<?php
// @author Giomani Designs (Development Team)
//
namespace App\Listeners;

use App\ActionLog;
use App\Events\OrderProcessing;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class OrderProcessed
{
  /**
   * @author Giomani Designs (Development Team)
   * @return void
   */
  public function __construct() {
    //
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  OrderProcessing  $event
   * @return void
   */
  public function handle(OrderProcessing $event) {
    $log = new ActionLog;
    $log->action = ActionLog::ORDER_PROCESSED;
    $log->more = $event->orderId;
    $log->save();
  }
}
