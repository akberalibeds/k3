<?php
// @author Giomani Designs (Development Team)
//
namespace App\Listeners;

use App\ActionLog;
use App\Events\VehicleCostTypeAdding;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class VehicleCostTypeAdded
{
  /**
   * @author Giomani Designs (Development Team)
   * @return void
   */
  public function __construct() {
      //
  }

  /**
   * @author Giomani Designs (Development Team)
   * @param  VehicleCostTypeAdding  $event
   * @return void
   */
  public function handle(VehicleCostTypeAdding $event) {
    $log = new ActionLog;
    $log->action = ActionLog::VEHICLE_COST_TYPE_ADDED;
    $log->more = $event->description . ';' . $event->type . ';' . $event->id;
    $log->save();
  }
}
