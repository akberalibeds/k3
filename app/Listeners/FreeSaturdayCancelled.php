<?php
// @author Giomani Designs (Development Team)
//
namespace App\Listeners;

use App\ActionLog;
use App\Events\FreeSaturdayCancelling;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class FreeSaturdayCancelled
{
  /**
   * @author Giomani Designs (Development Team)
   * @return void
   */
  public function __construct() {
    //
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  FreeSaturdayCancelling  $event
   * @return void
   */
  public function handle(FreeSaturdayCancelling $event) {
    $log = new ActionLog;
    $log->action = ActionLog::FREE_SATURDAY_CANCELLED;
    $log->more = $event->id . ';' . $event->saturday;
    $log->save();
  }
}
