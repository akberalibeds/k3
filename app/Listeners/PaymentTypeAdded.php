<?php
// @author Giomani Designs (Development Team)
//
namespace App\Listeners;

use App\ActionLog;
use App\Events\PaymentTypeAdding;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class PaymentTypeAdded
{
  /**
   * @author Giomani Designs (Development Team)
   * @return void
   */
  public function __construct() {
    //
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  PaymentTypeAdding  $event
   * @return void
   */
  public function handle(PaymentTypeAdding $event) {
    $log = new ActionLog;
    $log->action = ActionLog::VEHICLE_COST_TYPE_ADDED;
    $log->more = $event->description . ';' . $event->type . ';' . $event->id;
    $log->save();
  }
}
