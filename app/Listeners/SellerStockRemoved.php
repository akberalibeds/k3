<?php
// @author Giomani Designs (Development Team)
//
namespace App\Listeners;

use App\ActionLog;
use App\Events\SellerStockRemoving;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SellerStockRemoved
{
  /**
   * @author Giomani Designs (Development Team)
   * @return void
   */
  public function __construct() {
    //
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  SellerStockRemoving  $event
   * @return void
   */
  public function handle(SellerStockRemoving $event) {
    $log = new ActionLog;
    $log->action = ActionLog::SELLER_REMOVE_STOCK_ITEM;
    $log->more = $event->stockItemId . ';' . $event->seller;
    $log->save();
  }
}
