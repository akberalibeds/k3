<?php
// @author Giomani Designs (Development Team)
//
namespace App\Listeners;

use App\ActionLog;
use App\Events\OrderOrdering;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class OrderOrdered
{
  /**
   * @author Giomani Designs (Development Team)
   * @return void
   */
  public function __construct() {
    //
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  OrderOrdering  $event
   * @return void
   */
  public function handle(OrderOrdering $event) {
    $log = new ActionLog;
    $log->action = ActionLog::ORDER_ORDERED;
    $log->more = $event->orderId;
    $log->save();
  }
}
