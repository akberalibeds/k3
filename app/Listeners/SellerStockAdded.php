<?php
// @author Giomani Designs (Development Team)
//
namespace App\Listeners;

use App\ActionLog;
use App\Events\SellerStockAdding;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SellerStockAdded
{
  /**
   * @author Giomani Designs (Development Team)
   * @return void
   */
  public function __construct() {
    //
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  SellerStockAdding  $event
   * @return void
   */
  public function handle(SellerStockAdding $event) {
    $log = new ActionLog;
    $log->action = ActionLog::SELLER_ADD_STOCK_ITEM;
    $log->more = $event->price . ';' . $event->stockItemId . ';' . $event->seller;
    $log->save();
  }
}
