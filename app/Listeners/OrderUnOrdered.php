<?php
// @author Giomani Designs (Development Team)
//
namespace App\Listeners;

use App\ActionLog;
use App\Events\OrderUnOrdering;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class OrderUnOrdered
{
  /**
   * @author Giomani Designs (Development Team)
   * @return void
   */
  public function __construct() {
    //
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  OrderUnOrdering  $event
   * @return void
   */
  public function handle(OrderUnOrdering $event) {
    $log = new ActionLog;
    $log->action = ActionLog::ORDER_UNORDERED;
    $log->more = $event->orderId;
    $log->save();
  }
}
