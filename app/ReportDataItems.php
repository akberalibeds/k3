<?php
// @author Giomani Designs (Development Team)
//
namespace App;

use DB;
use Storage;
use Illuminate\Database\Eloquent\Model;

class ReportDataItems extends Model
{
  /**
   * 2013-12-02 00:00:00
   * @var int
   */
  const BOT = 1385942400;
  /**
   * The table associated with the model.
   *
   * @var string
   */
  protected $table = 'report_data_items';
  /**
   * @author Giomani Designs
   *
   * @param  string $from date in format 'yyyy-mm-dd'
   * @param  string $to date in format 'yyyy-mm-dd'
   */
  public static function getReportData ($from, $to) {
    return ReportDataItems::select(
        'company',
        'count',
        'date',
        'item_code'
      )
      ->whereBetween('date', [$from, $to])
      ->orderby('date', 'asc')
      ->orderby('item_code', 'asc')
      ->get();
  }
  /**
   * @author Giomani Designs
   * @return \DateTime  last time the database was update
   */
  public static function updateReportItems () {
    $today = new \DateTime('now');
    try {
      $lastUpdated = \DateTime::createFromFormat('l, j F Y', Storage::disk('local')->get('reports.last_dates'));
    } catch (\Exception $e) {
      $lastUpdated = \DateTime::createFromFormat('U', ReportDataItems::BOT);
    }
    if ($today->diff($lastUpdated)->format('%d') > 0) {
      $sql ='';
      $sql .= 'INSERT INTO report_data_items (company, count, `date`, item_code) ';
      $sql .= 'SELECT ';
      $sql .= ' companyName AS company, ';
      $sql .= ' COUNT(*) AS count, ';
      $sql .= ' DATE_FORMAT(FROM_UNIXTIME(startStamp), "%Y-%m-%d") AS orders_date, ';
      $sql .= ' stock_items.itemCode AS item_code ';
      $sql .= 'FROM orders ';
      $sql .= 'JOIN orderItems ON orders.id = oid ';
      $sql .= 'JOIN stock_items ON stock_items.id = orderItems.itemid ';
      $sql .= 'WHERE startStamp BETWEEN ? AND ? ';
      $sql .= 'GROUP BY orders_date, companyName, stock_items.itemCode';
      DB::insert($sql, [$lastUpdated->format('U'), strtotime('today midnight')]);
    }
    Storage::disk('local')->put('reports.last_dates', $today->format('l, j F Y'));

    return $today;
  }
}
