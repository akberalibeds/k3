<?php
// Giomani Designs (Development Team)
//
namespace App;

use App\StockParts;
use DB;
use Event;
use App\WarehouseStock;
use App\Events\StockCountAdjustment;
use App\Helper\Traits\QueryableModel;
use Illuminate\Database\Eloquent\Model;

class StockItem extends Model
{
  use QueryableModel;
  /**
   * @var array
   */
  protected $fillable = ['actual', 'always_in_stock', 'cost', 'description', 'quantity', 'retail', 'sku', 'wholesale', 'beds_price', 'ebay_price', 'isActive', 'pieces'];
  /**
   * @var array
   */
  public static $rules = [
    'count' => [
      'count' => 'required|integer|min:0|max:10000',
    ],
    'create' => [
      'always_in_stock',
      'actual' => 'required|integer|min:0|max:10000',
      'always_in_stock' => 'required|boolean',
      'blocked' => 'required|boolean',
      'category_id' => 'required|numeric|min:1',
      'cost' => 'required|numeric|min:0.00',
      'isMulti' => 'required|integer|min:1',
      'itemCode' => 'required|unique:stock_items',
      'itemDescription' => 'required',
      'label' => 'required|boolean',
      // 'model_no' => 'unique:stock_items', // not sure about the rule here - the model number won't always be present but it must be unique when it is
      'pieces' => 'required|integer|min:1',
      'retail' => 'required|numeric|min:0.00',
      'seller' => 'required|required|boolean',
      'weight' => 'required|numeric|min:0.00',
      'warehouse' => 'required|integer|min:0',
      'wholesale' => 'required|numeric|min:0',
      'withdrawn' => 'required|boolean',
    ],
    'update' => [
      'actual' => 'required|integer|min:0|max:10000',
      'always_in_stock' => 'required|boolean',
      'blocked' => 'required|boolean',
      'category_id' => 'required|numeric|min:0',
      'cost' => 'required|numeric|min:0.00',
      'isMulti' => 'required|integer|min:0',
      'itemDescription' => 'required',
      'label' => 'required|boolean',
      // 'model_no' => 'unique:stock_items', // not sure about the rule here - the model number won't always be present but it must be unique when it is
      'needs_attention' => 'required|boolean',
      'pieces' => 'required|integer|min:1',
      'retail' => 'required|numeric|min:0.00',
      'seller' => 'required|boolean',
      'weight' => 'required|numeric|min:0.00',
      'warehouse' => 'required|integer|min:0',
      'wholesale' => 'required|numeric|min:0',
      'withdrawn' => 'required|boolean',
      'isActive' => 'boolean',
    ],
  ];
  /**
   * @var string
   */
  protected $table = 'stock_items';

  public function items()
  {
    return $this->hasMany(OrderItems::class, 'itemid');
  }

  /**
   * Stock Item Save Method
   *  This is an override of the model that can make any updates on the stocks to create or leave any stock parts and add/leave it on the warehouse stocks
   * @param Array Model Save 
   */
  public function save(array $options = array())
  {
    $status = parent::save($options);
    StockParts::updateStockParts($this);

    if ($this->stockParts != null) {
      foreach ($this->stockParts as $sp) {
        $sp->save();
      }
    }

    StockParts::updateWarehouseStock($this);
    if ($this->stockParts != null) {
      foreach ($this->stockParts as $sp) {
        foreach ($sp->warehouseStocks as $ws) {
          $ws->save();
        }
      }
    }
    return $status;
  }

  /**
   * @author Giomani Designs (Development Team)
   * @param  $id the StockItem id
   * @return void
   */
  public static function block($id)
  {
    $stockItem = StockItem::find($id);
    $stockItem['blocked'] = 1;
    $stockItem->save();
  }
  /**
   * Get the post that owns the comment.
   */
  public function category()
  {
    return $this->belongsTo('App\StockCategory', 'category_id', 'id');
  }

  public function stockParts()
  {
    return $this->hasMany('App\StockParts', 'parent_id', 'id');
  }

  /**
   * Get Stock Quantity FULL
   *  This grabs the relevant stockparts associated to the item and gets the minimum quantity value.
   *  This also determines the maximum amount that an Item can be made with ALL relevant parts.
   * @return Integer  Minimum Stock Part Quantity Amount
   */
  public function getStockQuantity_full() {
    return $this->stockParts->map(function ($part) {return $part->totalStockQuantity();})->min();
  }
  
  /**
   * Get Stock Quantity MAX
   *  This grabs the relevant stockparts associated to the item and gets the maximum quantity value.
   *  This can serve as the highest surplus of an item's part
   * @return Integer  Maximum Stock Part Quantity Amount
   */
  public function getStockQuantity_max() {
    return $this->stockParts->map(function ($part) {return $part->totalStockQuantity();})->max();
    
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  string  $value
   * @return string
   */
  public function getCostAttribute($value)
  {
    return is_numeric($value) ? number_format($value, 2) : '0.00';
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  integer  $id the category id
   * @param  array   $queryStrings the query string
   * @return Illuminate\Pagination\LengthAwarePaginator
   */
  public static function getForCategory($id, $queryStrings)
  {
    $query = static::queryForCategory($id);
    if (count($queryStrings) > 0) {
      $query = parent::modForQueryString($query, $queryStrings);
    }
    return $query->orderBY('itemCode')->paginate(config('search.rpp'));
  }
  /**
   * @param  array $modelNumbers
   * @return Illuminate\Pagination\LengthAwarePaginator
   */
  public static function getForModelNumbers($modelNumbers)
  {
    return static::queryForModelNumbers($modelNumbers)->get();
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  $from   int unix timestamp
   * @param  $to     int unix timestamp
   * @param  $series string
   */
  public static function getItemsReportData($from, $to, $series)
  {
    return Db::table('orders')
      ->select(DB::raw('count(stock_items.itemCode) as itemCount, date_format(from_unixtime(startstamp),"%Y-%m-%d") as orderDate'), 'stock_items.itemCode', 'stock_items.id')
      ->join('orderItems', 'orders.id', '=', 'oid')
      ->join('stock_items', 'stock_items.id', '=', 'orderItems.itemid')
      ->whereBetween('startstamp', [$from, $to])
      ->where('stock_items.cat', '=', $series)
      ->groupBy('itemCode', 'startStamp')
      ->orderBy('orderDate')
      ->get();
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  array   $queryStrings the query string
   * @return Illuminate\Pagination\LengthAwarePaginator
   */
  public static function getStock($queryStrings)
  {
    $query = static::queryForAll();
    if (count($queryStrings) > 0) {
      $query = parent::modForQueryString($query, $queryStrings);
    }
    return $query;
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  $id the StockItem id
   * @return void
   */
  private static function getStockItem($id)
  {
    return static::find($id);
    //return StockItem::findWithCategory($id);
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  array   $queryStrings the query string
   * @return Illuminate\Pagination\LengthAwarePaginator
   */
  public static function getWithCategory($id)
  {
    $query = static::queryForAll();
    $query->where('stock_item.id', '=', $id);
    return $query->get();
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  array    $model
   * @return boolean  success or failure
   */
  public function importManifestRow($model)
  {
    $item = static::where('itemCode', '=', $model['itemCode']);
  }
  /**
   * @author Giomani Designs (Development Team)
   * @return void
   */
  public static function queryForAll()
  {
    return static::select(
      'stock_items.id',
      'stock_items.actual',
      'stock_items.always_in_stock',
      'stock_items.beds_price',
      'stock_items.blocked',
      'stock_items.cost',
      'stock_items.ebay_price',
      'stock_items.isMulti',
      'stock_items.itemCode',
      'stock_items.itemDescription',
      'stock_items.itemName',
      'stock_items.model_no',
      'stock_items.label',
      'stock_items.warehouse',
      'stock_items.needs_attention',
      'stock_items.pieces',
      'stock_items.qty_on_so',
      'stock_items.retail',
      'stock_items.withdrawn',
      'stock_items.wholesale',
      'stock_items.isActive',
      'categories.name as sales_category',

      'stock_categories.name as stock_category'
    )
      ->leftJoin('categories', 'categories.id', '=', 'stock_items.category_id')
      ->leftJoin('stock_categories', 'stock_categories.id', '=', 'stock_items.category_id');
  }
  /**
   * @author Giomani Designs (Development Team)
   * @return void
   */
  public static function queryForItemCodeSearch($query)
  {
    return static::where('itemCode', 'like', '%' . $query . '%');
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  int $catagoryId
   * @return Illuminate\Database\Query\Builder
   */
  public static function queryForCategory($catagoryId)
  {
    return static::where('category_id', '=', $catagoryId);
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  array $modelNumbers
   * @return Illuminate\Database\Query\Builder
   */
  public static function queryForModelNumbers($modelNumbers)
  {
    return static::select(
      'id',
      'itemCode',
      'model_no'
    )
      ->whereIn('model_no', $modelNumbers);
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  $query the string to search for
   * @return Illuminate\Database\Eloquent\Collection
   */
  public static function search($subject, $query)
  {
    return static::modForQuery(static::select('*'), explode('|', $subject), $query)->paginate(config('search.rpp'));
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  $query the string to search for
   * @return Illuminate\Database\Eloquent\Collection
   */
  public static function searchByItemCode($query)
  {
    return static::queryForItemCodeSearch($query)
      ->orderBy('itemCode', 'desc')
      ->where('withdrawn', '=', '0')
      ->get();
  }

  /**
   * @author Giomani Designs (Development Team)
   * @param  $query the string to search for
   * @return Illuminate\Database\Eloquent\Collection
   */
  public static function searchByItemCodeNewSale($query)
  {
    return static::queryForItemCodeSearch($query)
      ->where('withdrawn', '<>', '1')
      ->get();
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  int    $categoryId
   * @param  string $query the string to search for
   * @return Illuminate\Database\Eloquent\Collection
   */
  public static function searchByItemCodeInCategory($categoryId, $query)
  {
    return static::queryForItemCodeSearch($query)
      ->where('category_id', '=', $catagoryId)
      ->get();
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  $id the StockItem id
   * @return void
   */
  public static function unblock($id)
  {
    $stockItem = StockItem::find($id);
    $stockItem['blocked'] = 0;
    $stockItem->save();
  }
  /*
  	@param $code the StockItem itemCode
   */
  public static function itemCode($code, $create = false)
  {
    $item = StockItem::where('itemCode', $code)->first();
    if (!$item && $create) {
      $item = new self;
      $item->itemCode = $code;
      $item->itemDescription = $code;
      $item->itemQty = 10000;
      $item->actual = 10000;
      $item->warehouse = 0;
      $item->label = 0;
      $item->save();
    }
    return $item;
  }

  /*
   * @param $code the StockItem itemCode from Magento
   */
  public static function itemCodeBeds($code, $price)
  {
    $stock = StockItem::where('itemCode', $code)->first();
    if (!$stock) {
      $stock = new self;
      $stock->itemCode = $code;
      $stock->itemDescription = $code;
      $stock->itemQty = 0;
      $stock->actual = 0;
      $stock->warehouse = 4;
      //$stock->save();
    }
    if ($stock->beds_price != $price && $price > 0) {
      $stock->beds_price = $price;
    }
    $stock->save();
    return $stock;
  }


  /*
  	@param $code the StockItem itemCode from eBay
   */
  public static function itemCodeEbay($code, $price)
  {


    $stock = StockItem::where('itemCode', $code)->first();
    if (!$stock) {
      $stock = new self;
      $stock->itemCode = $code;
      $stock->itemDescription = $code;
      $stock->itemQty = 0;
      $stock->actual = 0;
      $stock->warehouse = 0;
			//$stock->save();
    }

    if ($stock->ebay_price != $price && $price > 0) {
      $stock->ebay_price = $price;
    }

    $stock->save();

    return $stock;

  }

  /*
   **************************************************
   *
   * Stock Control functions
   *
   **************************************************
   */
  /**
   * Add stock item(s) stock.
   * (adds $count to 'actual')
   *
   * @author Giomani Designs (Development Team)
   * @param  $id the StockItem id
   * @param  $count the number of items to add - default '1'
   * @return void
   */
  public static function addToStock($id, $count = 1)
  {
    $stockItem = static::getStockItem($id);
    if ($stockItem->always_in_stock !== 1) {
      $stockItem['actual'] += $count;
    }
    $stockItem->save();
  }
  /**
   * Add stock item(s) to an order.
   * (adds $count to 'qty_on_so')
   *
   * @author Giomani Designs (Development Team)
   * @param  $id the StockItem id
   * @param  $count the number of items to add - default '1'
   * @return void
   */
  public static function addToOrder($id, $count = 1)
  {
    $stockItem = static::getStockItem($id);
    $stockItem['qty_on_so'] += $count;
    $stockItem->save();
  }
  /**
   * Dispatch stock item.
   * This is for an order that is being dispatched.
   * (subtracts $count from 'actual')
   * (subtracts $count from 'qty_on_so')
   *
   * @author Giomani Designs (Development Team)
   * @param  $id the StockItem id
   * @param  $count the number of items to add - default '1'
   */
  public static function dispatchToOrder($id, $count = 1)
  {
    $stockItem = static::getStockItem($id);
    if ($stockItem->always_in_stock !== 1) {
      $stockItem['actual'] -= $count;
    }
    $stockItem['qty_on_so'] -= $count;
    $stockItem->save();
  }
  /**
   * Remove a stock item from an order.
   * This is for an order that HAS NOT been dispatched.
   *
   * @author Giomani Designs (Development Team)
   * @param  $id the StockItem id
   * @param  $count the number of items to remove - default '1'
   * @return void
   */
  public static function removeFromOrder($id, $count = 1)
  {
    $stockItem = static::getStockItem($id);
    $stockItem['qty_on_so'] -= $count;
    $stockItem->save();
  }
  /**
   * Remove a stock item from an order.
   * This is for an order that HAS been dispatched.
   *
   * @author Giomani Designs (Development Team)
   * @param  $id the StockItem id
   * @param  $count the number of items to remove - default '1'
   * @return void
   */
  public static function returnItem($id, $count = 1)
  {
    $stockItem = static::getStockItem($id);
    if ($stockItem->always_in_stock !== 1) {
      $stockItem['actual'] += $count;
    }
    $stockItem->save();
  }
// **************************************************
// Stock Item / SKU Flagging System
// **************************************************
  /**
   * SKU2Active
   *  This changes any provided SKUs to being active SKUs
   * 
   * @param Collection Stock Item Code
   */
  public static function sku2Active($icList)
  {

    foreach ($icList as $codeid) {
      $stockItem = StockItem::getStockItem($codeid);
      if ($stockItem->isActive != 1) {
        $stockItem->isActive = 1;
        $stockItem->save();
      }
    }
    return "Done";
  }

  /**
   * SKU2Inactive
   *  This changes any provided SKUs to being inactive SKUs
   * 
   * @param Collection SKU Code
   */
  public static function sku2Inactive($icList)
  {
    foreach ($icList as $codeid) {
      $stockItem = StockItem::getStockItem($codeid);
      if ($stockItem->isActive != 0) {
        $stockItem->isActive = 0; //Disactivate it
        $stockItem->save();
      }
    }
    return "Done";
  }

  /**
   * Adjust Stocks Amount
   *  THis changes the amount of stocks avaliable 
   * @param   Integer Inputted Amount for Stock Count Change
   * @param   Array   The Stock Item and the Original Stock Amount
   */
  public function adjustStocks($count){
    $was = $this->actual;
    $this->actual = $count;
    $this->save();
    Event::fire(new StockCountAdjustment($this->id, $this->actual, $this->itemCode, $was));
    return array($this, $was);
  }
}