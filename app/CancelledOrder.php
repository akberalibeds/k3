<?php

namespace App;

use App\Helper\Traits\QueryableModel;
use Illuminate\Database\Eloquent\Model;

class CancelledOrder extends Model
{
  use QueryableModel;
  /**
   * The table associated with the model.
   *
   * @var string
   */
  protected $table = 'cancelled_orders';
  /**
   * @author Giomani Designs (Development Team)
   * @param  string $queryStrings
   * @return Illuminate\Pagination\LengthAwarePaginator
   */
  public static function getCancellled ($queryStrings) {
    $query = static::queryForCancelled();
    if (count($queryStrings) > 0) {
      $query = parent::modForQueryString($query, $queryStrings);
    }
    return $query->orderBy('cancelled_orders.id', 'desc')->paginate(config('search.rpp'));
  }
  /**
   * @author Giomani Designs (Development Team)
   * @return Illuminate\Database\Query\Builder
   */
  protected static function queryForCancelled () {
    return static::select(
        'cancelled_orders.id as oid',
        'iNo',
        'dBusinessName',
        'postcode',
        'staffName as ref',
        'orderType',
        'order_status',
        'startDate',
        'total',
        'paid',
        'done',
        'userID',
        'supplier_name',
        'deleted_by',
        'delete_time'
      )
      ->leftJoin('customers', 'customers.id', '=', 'cancelled_orders.cid')
      ->join('order_status', 'order_status.id', '=', 'cancelled_orders.id_status')
      ->leftJoin('supplier', 'supplier.id', '=', 'cancelled_orders.id_supplier')
      ->leftJoin('users', 'staffid', '=', 'users.id');
  }
}
