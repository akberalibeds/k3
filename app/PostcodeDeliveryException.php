<?php
// @author Giomani Designs (Development Team)
//
namespace App;

use Illuminate\Database\Eloquent\Model;

class PostcodeDeliveryException extends Model
{
  /**
   * The table associated with the model.
   *
   * @var string
   */
  protected $table = 'postcode_delivery_exceptions';
}
