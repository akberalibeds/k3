<?php

namespace App;

use App\Helper\Traits\QueryableModel;
use Illuminate\Database\Eloquent\Model;

class StockStockCategory extends Model
{
  use QueryableModel;
  /**
   * The table associated with the model.
   *
   * @var string
   */
  protected $table = 'stock_categories';
  /**
   * @author Giomani Designs (Development Team)
   * @param  array   $queryStrings the query string
   * @return \Illuminate\Http\Response
   */
  public static function getCategories($queryStrings) {
    $query = static::select('*');
    if (count($queryStrings) > 0) {
      $query = parent::modForQueryString($query, $queryStrings);
    }
    return $query->orderBY('name')->paginate(config('search.rpp'));
  }
}
