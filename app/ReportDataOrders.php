<?php

namespace App;

use DB;
use Illuminate\Database\Eloquent\Model;

class ReportDataOrders extends Model
{
  /**
   * 2013-12-02 00:00:00
   * @var int
   */
  const BOT = 1385942400;
  /**
   * The table associated with the model.
   *
   * @var string
   */
  protected $table = 'report_data_orders';
  /**
   * @author Giomani Designs
   *
   * @param  string $from date in format 'yyyy-mm-dd'
   * @param  string $to date in format 'yyyy-mm-dd'
   * @param  array  $companies
   */
  public static function getReportData ($from, $to, $companies)
  {
    return ReportDataOrders::select('company', 'count', 'orders_date', 'orders_type', 'paid', 'vat', 'total')
      ->whereBetween('orders_date', [$from, $to])
      ->orderby('orders_date', 'asc')
      ->orderby('company', 'asc')
      ->get();
  }
  /**
   * Removes the old data and inserts the new.
   * The data is up to from the beginning of time until midnight last night.
   *
   * @author Giomani Designs
   *
   */
  public static function initialise ()
  {
    DB::table('report_data_orders')->truncate();

    $sql ='';
    $sql .= 'INSERT INTO report_data_orders (company, count, orders_date, orders_type, paid, total, vat)';
    $sql .= 'SELECT';
    $sql .= ' companyName AS company,';
    $sql .= ' COUNT(total),';
    $sql .= ' DATE_FORMAT(FROM_UNIXTIME(startStamp),"%Y-%m-%d") AS orders_date,';
    $sql .= ' orderType AS orders_type,';
    $sql .= ' TRUNCATE(SUM(paid), 2) AS piad,';
    $sql .= ' TRUNCATE(SUM(total), 2) AS total,';
    $sql .= ' TRUNCATE(SUM((vatAmount / 100) * total), 2) AS vat ';
    $sql .= 'FROM orders ';
    $sql .= 'WHERE startStamp BETWEEN ? AND ? ';
    $sql .= 'GROUP BY company, orders_type, orders_date ';
    $sql .= 'ORDER BY orders_date';

    DB::insert($sql, [ReportDataOrders::BOT, strtotime('today midnight')]);
  }
}
