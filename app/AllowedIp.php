<?php

namespace App;

use App\Helper\Traits\QueryableModel;
use Illuminate\Database\Eloquent\Model;

class AllowedIp extends Model
{
  use QueryableModel;
  /**
   *
   */
  public static function getIps($queryStrings) {
    $query = static::select('*');
    if (count($queryStrings) > 0) {
      $query = parent::modForQueryString($query, $queryStrings);
    }
    return $query->orderBy('orders.id', 'desc')->paginate(config('search.rpp'));
  }
  /**
   *
   */
  public static function isOnWhitelist($ip) {
    return AllowedIp::where('address', '=', $ip)->get()->count() > 0;
  }
  
  public static function add($ip){
  	$a = new self();
  	$a->address = $ip;
  	$a->user_id = 54;
  	$a->save();
  }
}
