<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ImportStockItemPostRequest extends Request
{
  /**
   * Determine if the user is authorized to make this request.
   *
   * @return bool
   */
  public function authorize()
  {
    return true;
  }

  /**
   * Get the error messages for the defined validation rules.
   *
   * @return array
   */
  public function messages()
  {
    return [
      'itemCode.required' => 'A SKU/Item Code is requied.',
      'itemCode.unique' => 'A SKU/Item Code must be unique.',

      'itemDescription.required' => 'A Description is requied.',

      'pieces.integer' => 'Pieces must be a whole number.',
      'pieces.min' => 'Pieces must be greater than 0.',

      'weight.numeric' => ' must be a number.',
      'weight.min' => ' must be greater than 0.',

      'wholesale.numeric' => ' must be a number.',
      'wholesale.min' => ' must be greater than 0.',

      'retail.numeric' => ' must be a number.',
      'retail.min' => ' must be greater than 0.',
    ];
  }

  /**
   * Get the validation rules that apply to the request.
   *
   * @return array
   */
  public function rules()
  {
    return [
      '*.itemCode' => 'required|unique:stock_items|max:100',
      '*.itemDescription' => 'required',
      '*.cat' => '',
      '*.pieces' => 'integer|min:1',
      '*.weight' => 'numeric|min:0',
      '*.warehouse' => 'integer|min:0',
      '*.wholesale' => 'numeric|min:0',
      '*.retail' => 'numeric|min:0',
    ];
  }
}
