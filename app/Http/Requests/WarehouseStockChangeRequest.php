<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

use App\Warehouse;
use App\StockItem;
use App\StockParts;

class WarehouseStockChangeRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function thisWarehouse() {
      return Warehouse::find($this->warehouse);
    }
    
    public function reason() {
      return $this->log;
    }
    
    public function thatWarehouse() {
      return Warehouse::where('location',$this->reason())->first();
    }
    
    public function qty() {
      return $this->qty;
    }
    
    public function stockPart() {
      return StockParts::find($this->part);
    }
        
    public function stockItem() {
      return StockItem::find($this->item);
    }
    
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
          'warehouse' => 'required|exists:warehouses,id',
          'log' => 'required',
          'qty' => 'required|numeric',
          'part'=>'required_without:item',
          'item'=>'required_without:part'
        ];
    }
}
