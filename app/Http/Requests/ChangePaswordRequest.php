<?php

namespace App\Http\Requests;

use Auth;
use Hash;
use App\Http\Requests\Request;

class ChangePasswordRequest extends Request
{
  /**
   * Determine if the user is authorized to make this request.
   *
   * @return bool
   */
  public function authorize() {
    return Auth::attempt(['email' => Auth::user()->email, 'password' => $this->input('password')]);
  }
  /**
   * Get the error messages for the defined validation rules.
   *
   * @return array
   */
  public function messages() {
    return [
      'new_password.confirmed' => 'ppp', // trans('passwords.password'),
      'new_password.min' => 'ppp', // trans('passwords.password'),
      'new_password.required' => 'required',

      'old_password.required' => 'required',
    ];
  }
  /**
   * Get the validation rules that apply to the request.
   *
   * @return array
   */
  public function rules() {
    return [
      'new_password' => 'required|min:6|confirmed',
      'old_password' => 'required',
    ];
  }
}
