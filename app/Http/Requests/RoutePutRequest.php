<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class RoutePutRequest extends Request
{
  /**
   * Determine if the user is authorized to make this request.
   *
   * @return bool
   */
  public function authorize()
  {
    return true;
  }

  /**
   * Get the error messages for the defined validation rules.
   *
   * @return array
   */
  public function messages()
  {
    return [
      'in_load_count.required' => 'A SKU/Item Code is requied',

      'is_route_route.required' => 'A Description is requied',

      'is_wholesale.required' => 'A Description is requied',

      'max.required' => 'A Description is requied',

      'name.required' => 'A Description is requied',

      'van.required' => 'A Description is requied',

    ];
  }

  /**
   * Get the validation rules that apply to the request.
   *
   * @return array
   */
  public function rules()
  {
    return [
      'in_load_count' => 'required|in:1,0',
      'is_route_route' => 'required|in:1,0',
      'is_wholesale' => 'required|in:1,0',
      'max' => 'required|min:0',
      'name' => 'required',
      'van' => 'required|numeric',
    ];
  }
}
