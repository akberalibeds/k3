<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class SupplierPutRequest extends Request
{
  /**
   * Determine if the user is authorized to make this request.
   *
   * @return bool
   */
  public function authorize()
  {
      return true;
  }
  /**
   * Get the error messages for the defined validation rules.
   *
   * @return array
   */
  public function messages()
  {
    return [
    ];
  }
  /**
   * Get the validation rules that apply to the request.
   *
   * @return array
   */
  public function rules()
  {
    return [
      'supplier_name' => 'required|max:255',
      'days' => 'required|integer|min:0',
      'print_orders_on_dispatch' => 'required|boolean',
    ];
  }
}
