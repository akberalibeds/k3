<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;
use Zizaco\Entrust\Middleware\EntrustRole;

class Role extends EntrustRole
{
  /**
   * Creates a new instance of the middleware.
   *
   * @param Guard $auth
   */
  public function __construct(Guard $auth)
  {
    parent::__construct($auth);
  }

  /**
   * Handle an incoming request.
   *
   * @param  \Illuminate\Http\Request $request
   * @param  Closure $next
   * @param  $roles
   * @return mixed
   */
  public function handle($request, Closure $next, $roles)
  {
    if ($this->auth->guest() || !$request->user()->hasRole(explode('|', $roles))) {
      return redirect()->route('not-authorised');
    }

    return $next($request);
  }
}
