<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;
use Zizaco\Entrust\Middleware\EntrustAbility;

class Ability extends EntrustAbility
{
	/**
	 * Creates a new instance of the middleware.
	 *
	 * @param Guard $auth
	 */
	public function __construct(Guard $auth)
	{
		parent::__construct($auth);
	}

	/**
	 * Handle an incoming request.
	 *
	 * @param \Illuminate\Http\Request $request
	 * @param Closure $next
	 * @param $roles
	 * @param $permissions
	 * @param bool $validateAll
	 * @return mixed
	 */
	public function handle($request, Closure $next, $roles, $permissions, $validateAll = false)
	{
		if ($this->auth->guest() || !$request->user()->ability(explode('|', $roles), explode('|', $permissions))) {
			return redirect('not-authorised');
		}

		return $next($request);
	}
}
