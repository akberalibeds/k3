<?php

namespace App\Http\Middleware;

use Closure;

class AfterMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
    	
    	$response = $next($request);
    	/*
    	$queries = \DB::getQueryLog();
    	
    	$logData = '';
    	$skip=["roles","permissions","page_visits","user_bookmarks","bulletins"];
    	
    	foreach($queries as $q){
    	
    		$query = (object)$q;
    		
    		$continue = false;
    		foreach($skip as $s){
	    		if (strpos($query->query, $s) !== false) {
	    			$continue = true;
	    		}
    		}
    		
    		if($continue){
    			continue;
    		}
    		
    		
	    		$full = str_split($query->query,1);
	    		$index = 0;
	    		foreach($full as &$f){
	    			if($f=="?" && isset($query->bindings[$index])){
	    				$f=$query->bindings[$index++];
	    			}
	    		}
	    	
	    		$logData .= date('Y-m-d H:i:s')."\t".implode($full)."\n";
    		
    	}
    	
    	//write if any new data
    	if($logData != ''){
    		//open logs file if exists or create a new one
    		$logFile = fopen(storage_path('logs/query_logs/'.date('Y-m-d').'_query.log'), 'a+');
    		//write log to file
    		fwrite($logFile, $logData);
    		fclose($logFile);
    	}
    	
    	*/
    	return $response;
    }
}
