<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
use App\Order;

Route::get('/clear-cache', function() {
    Artisan::call('cache:clear');
    return "Cache is cleared";
});

/*
 * NO Auth API Access
 */
Route::group(['as' => 'api::', 'middleware' => ['guest'], 'namespace' => 'Api', 'prefix' => 'api'], function () {
	Route::get('/datacheck/{ip}',[ 'as' => 'ip','uses' =>'ApiController@ipStatus']);
	Route::group(['prefix' => 'v1'],function(){
		Route::get('/change_status/{orderID}/{oldstatus}/{newstatus}', 'ApiController@api_change_status');
		Route::get('/delivery-dates', ['as' => 'index', 'uses' => 'ApiController@delivery_dates']);
		Route::get('/barcode/{id}', ['as' => 'index', 'uses' => 'ApiController@barcode']);
	});
});


	/*
	 * NO Auth API Access
	 */
	Route::group(['as' => 'pda::', 'middleware' => ['guest'], 'prefix' => 'pda'], function () {
		Route::group(['prefix' => 'v1'],function(){
			Route::get('/login', 'PdaController@login');
			Route::get('/update/{version}', 'PdaController@updateCheck');
			Route::get('/route-list', 'PdaController@getRouteList');
			Route::get('/update-location', 'PdaController@updateLoc');
			Route::get('/update-token', 'PdaController@updateToken');
			Route::post('/upload-image', 'PdaController@uploadImage');
			Route::post('/upload-status', 'PdaController@uploadStatus');
			Route::post('/upload-note', 'PdaController@uploadNote');
			Route::post('/upload-payment', 'PdaController@uploadPayment');
			Route::get('/app-release.apk', 'PdaController@apk');
		});
	});

	
Route::group(['middleware' => ['guest']], function () {
  /*
   * delivery date used by magento???
   */
  Route::group(['as' => 'delivery-date::', 'namespace' => 'DeliveryDate', 'prefix' => 'delivery-date'], function () {
    	Route::get('/{postcode}/{key}/{days?}', ['as' => 'index', 'uses' => 'DeliveryDateController@index']);	
 	});


});

Route::get('/credits', function () { return view('credits'); });

Route::get('/outage', ['as' => 'fatal-error', 'uses' => 'FatalController@index']);

Route::group(['as' => 'payments::', 'namespace' => 'Payments', 'prefix' => 'payments'], function () {
	  Route::get('/', ['as' => 'index', 'uses' => 'PaymentController@index']);
	  Route::get('/accepted', ['as' => 'index', 'uses' => 'PaymentController@accepted']);
	  Route::get('/declined', ['as' => 'index', 'uses' => 'PaymentController@declined']);
	  Route::get('/exception', ['as' => 'index', 'uses' => 'PaymentController@exception']);
	  Route::get('/cancelled', ['as' => 'index', 'uses' => 'PaymentController@cancelled']);
});


/*
 * auth
 */
Route::auth();

Route::post("/barclaycard-epdq","BarclayCardController@index");

/*
 * not-authorised
 */
Route::get('/not-authorised', ['as' => 'not-authorised', 'uses' => 'Controller@notAuthorised']);


Route::group(['middlewareGroups' => ['web']], function () {


	Route::get('/credit/{id}','OrdersController@getCredits')	;
	Route::get('/credit/{order}/{id}','OrdersController@useCredits')	;
	
	
	Route::group(['as => data::' ,'middleware' => ['permission:admin'] ,'namespace' =>'Data', 'prefix' =>'data'],function(){
			route::get('/','DataController@index');
			route::post('/','DataController@query');
			route::post('/csv','DataController@csv');
			route::get('/saved','DataController@saved');
			route::post('/remove','DataController@remove');
	});

  /*
   * admin
   */
  Route::group(['as' => 'admin::', 'middleware' => ['permission:admin'], 'namespace' => 'Admin', 'prefix' => 'admin'], function () {
    Route::get('/', ['as' => 'index', 'uses' => 'AdminController@index']);
    
    
    Route::get('route-list',function(){
    	return view('admin.routelist',['routes'=> Route::getRoutes()]);
    });
    
    Route::group(['as' => 'pda::', 'namespace' => 'Pda', 'prefix' => 'pda'], function () {
    		Route::get('/','PdaController@index');
   	});
    
    Route::group(['as' => 'warehouse::', 'namespace' => 'Warehouse', 'prefix' => 'warehouse'], function () {
    	Route::get('/stock', ['as' => 'index', 'uses' => 'WarehouseController@stock']);
    	Route::get('/stock/export', ['as' => 'show', 'uses' => 'WarehouseController@export']);
    	Route::get('/stock/{id}/export', ['as' => 'show', 'uses' => 'WarehouseController@export']);
    	Route::get('/stock/{id}', ['as' => 'show', 'uses' => 'WarehouseController@show']);
    	Route::post('/stock/{id}/upload', ['as' => 'upload', 'uses' => 'WarehouseController@upload']);
    	Route::post('/stock/apply', ['as' => 'apply', 'uses' => 'WarehouseController@apply']);
    	Route::get('/stock/part/add', ['as' => 'add', 'uses' => 'WarehouseController@addPart']);
    	Route::get('/stock/part/remove', ['as' => 'add', 'uses' => 'WarehouseController@removePart']);
    	Route::get('/stock/item/add', ['as' => 'add', 'uses' => 'WarehouseController@addItem']);
    	Route::get('/stock/item/remove', ['as' => 'add', 'uses' => 'WarehouseController@removeItem']);
        Route::get('/stock/log/{id}', 'WarehouseController@logItem');
    });
    
    /*
     * Ebay Settings
     */
    Route::group(['as' => 'ebay::', 'namespace' => 'Ebay', 'prefix' => 'ebay'], function () {
      Route::get('/', ['as' => 'index', 'uses' => 'EbayController@index']);
      Route::get('/edit', ['as' => 'edit', 'uses' => 'EbayController@edit']);
      Route::post('/edit', ['as' => 'edit', 'uses' => 'EbayController@update']);
      
      Route::group(['as' => 'listings::', 'prefix' => 'listings'], function () {
      		Route::get('/', ['as' => 'listings', 'uses' => 'EbayController@listings']);
      		Route::get('/ebaystock/remove/{id}', ['as' => 'listings', 'uses' => 'EbayController@removeEbayStock']);
      		Route::get('/ebaystock/add/{data}', ['as' => 'listings', 'uses' => 'EbayController@addEbayStock']);
      		Route::get('/ebaystock/{id}/{qty}', ['as' => 'listings', 'uses' => 'EbayController@updateEbayStock']);
      });
    });
	/*
     * Ebay Settings
     */
    Route::group(['as' => 'magento::', 'namespace' => 'Magento', 'prefix' => 'magento'], function () {
      Route::get('/', ['as' => 'index', 'uses' => 'MagentoController@index']);
      Route::get('/edit/{id}', ['as' => 'edit', 'uses' => 'MagentoController@edit']);
      Route::post('/edit/{id}', ['as' => 'edit', 'uses' => 'MagentoController@update']);
    });
	
	/*
     * Reports Settings
     */
    Route::group(['as' => 'reports::', 'namespace' => 'Reports', 'prefix' => 'reports'], function () {
      Route::get('/', ['as' => 'index', 'uses' => 'ReportController@index']);
	  Route::get('/search', ['as' => 'search', 'uses' => 'ReportController@search']);
    });
    /*
     * Maps Settings
     */
    Route::group(['as' => 'maps::', 'namespace' => 'Maps', 'prefix' => 'maps'], function () {
      Route::get('/', ['as' => 'index', 'uses' => 'MapsController@index']);
      Route::get('/edit', ['as' => 'edit', 'uses' => 'MapsController@edit']);
      Route::post('/edit', ['as' => 'edit', 'uses' => 'MapsController@update']);
    });
    /*
     * Cron Task Settings
     */
    Route::group(['as' => 'tasks::', 'namespace' => 'Tasks', 'prefix' => 'tasks'], function () {
      Route::get('/', ['as' => 'index', 'uses' => 'TaskController@index']);
      Route::post('/update', ['as' => 'update', 'uses' => 'TaskController@update']);
    });
    /*
    /*
     * dashboard
     */
    Route::group(['as' => 'dashboard::', 'namespace' => 'Dashboard', 'prefix' => 'dashboard'], function () {
      Route::get('/', ['as' => 'index', 'uses' => 'DashboardController@index']);
      Route::post('{info}/store', ['as' => 'store', 'uses' => 'DashboardController@store']);
      Route::get('{info}/show', ['as' => 'show', 'uses' => 'DashboardController@show']);
    });
    /*
     * drivers
     */
    Route::group(['as' => 'drivers::', 'namespace' => 'Driver', 'prefix' => 'drivers'], function () {
      Route::get('/', ['as' => 'index', 'uses' => 'DriverController@index']);
      Route::get('create', ['as' => 'create', 'uses' => 'DriverController@create']);
      Route::post('store', ['as' => 'store', 'uses' => 'DriverController@store']);
      Route::get('print', ['as' => 'print', 'uses' => 'DriverController@driverList']);
      
      Route::group(['as' => 'map::',  'prefix' => 'map'], function () {
      	Route::get('/', ['as' => 'index', 'uses' => 'DriverMapController@index']);
      	Route::get('/send/{driver}/{message}', ['as' => 'send', 'uses' => 'DriverMapController@sendMessage']);
      });
      
      Route::get('/{id}/edit', ['as' => 'edit', 'uses' => 'DriverController@edit']);
      Route::get('/{id}/show', ['as' => 'show', 'uses' => 'DriverController@show']);
      Route::put('/{id}/update', ['as' => 'update', 'uses' => 'DriverController@update']);
      Route::get('/{id}/delete', ['as' => 'remove', 'uses' => 'DriverController@remove']);
	  
    });
    /*
     * driver mates
     */
    Route::group(['as' => 'driver-mates::', 'namespace' => 'Driver', 'prefix' => 'driver-mates'], function () {
      Route::get('/', ['as' => 'index', 'uses' => 'DriverMateController@index']);
      Route::get('create', ['as' => 'create', 'uses' => 'DriverMateController@create']);
      Route::post('store', ['as' => 'store', 'uses' => 'DriverMateController@store']);
      Route::get('print', ['as' => 'print', 'uses' => 'DriverMateController@mateList']);
      Route::get('{id}/edit', ['as' => 'edit', 'uses' => 'DriverMateController@edit']);
      Route::get('{id}/show', ['as' => 'show', 'uses' => 'DriverMateController@show']);
      Route::post('{id}/update', ['as' => 'update', 'uses' => 'DriverMateController@update']);
      Route::get('{id}/remove', ['as' => 'remove', 'uses' => 'DriverMateController@remove']);

      // search
      Route::get('search/{subject}/{query}', ['as' => 'search', 'uses' => 'DriverMateController@searchName']);
    });
    	/*
    	 * pickers
    	 */
    	Route::group(['as' => 'pickers::', 'namespace' => 'Driver', 'prefix' => 'pickers'], function () {
    		Route::get('/', ['as' => 'index', 'uses' => 'PickerController@index']);
    		Route::get('create', ['as' => 'create', 'uses' => 'PickerController@create']);
    		Route::post('store', ['as' => 'store', 'uses' => 'PickerController@store']);
    		Route::get('print', ['as' => 'print', 'uses' => 'PickerController@pickerList']);
    		Route::get('{id}/edit', ['as' => 'edit', 'uses' => 'PickerController@edit']);
    		Route::get('{id}/show', ['as' => 'show', 'uses' => 'PickerController@show']);
    		Route::post('{id}/update', ['as' => 'update', 'uses' => 'PickerController@update']);
    		Route::get('{id}/remove', ['as' => 'remove', 'uses' => 'PickerController@remove']);
    		// search
    		Route::get('search/{subject}/{query}', ['as' => 'search', 'uses' => 'PickerController@searchName']);
    	});
    /*
     * eod
     */
    Route::group(['as' => 'eod::', 'namespace' => 'Eod', 'prefix' => 'eod'], function () {
      Route::get('/', ['as' => 'index', 'uses' => 'EodController@index']);
      Route::get('/create', ['as' => 'create', 'uses' => 'EodController@create']);
    });
    /*
     * free-saturdays
     */
    Route::group(['as' => 'free-saturdays::', 'namespace' => 'Deliveries', 'prefix' => 'free-saturdays'], function () {
      Route::get('/', ['as' => 'index', 'uses' => 'FreeSaturdaysController@index']);
      Route::get('cancel', ['as' => 'cancel', 'uses' => 'FreeSaturdaysController@cancel']);
      Route::get('create', ['as' => 'create', 'uses' => 'FreeSaturdaysController@create']);
      Route::post('store', ['as' => 'store', 'uses' => 'FreeSaturdaysController@store']);
    });
    /*
     *  iam
     */
    Route::group(['as' => 'iam::', 'namespace' => 'Iam', 'prefix' => 'iam'], function () {
      Route::get('/', ['as' => 'index', 'uses' => 'IamController@index']);
      Route::post('store', ['as' => 'store', 'uses' => 'IamController@store']);
    });
    /*
     *  import export
     * TO BO DEPRECATED
     */
    Route::group(['as' => 'io::', 'namespace' => 'ImportExport', 'prefix' => 'import-export'], function () {
      Route::get('/', ['as' => 'index', 'uses' => 'ImportExportController@index']);
      Route::get('headings/{profileName}', ['as' => 'profile', 'uses' => 'ImportExportController@headings']);
      Route::post('import', ['as' => 'import', 'uses' => 'ImportExportController@import']);
      Route::get('profile/{mode}/{profileName}', ['as' => 'profile', 'uses' => 'ImportExportController@profile']);
      Route::post('store/{import}', ['as' => 'store', 'uses' => 'ImportExportController@store']);
    });
    /*
     *  ip address whitelist
     */
    Route::group(['as' => 'ips::', 'namespace' => 'Ips', 'prefix' => 'ips'], function () {
      Route::get('/', ['as' => 'index', 'uses' => 'IpsController@index']);
      Route::get('create', ['as' => 'create', 'uses' => 'IpsController@create']);
      Route::get('{id}/show', ['as' => 'show', 'uses' => 'IpsController@show']);
      Route::post('store', ['as' => 'store', 'uses' => 'IpsController@store']);
      Route::put('update', ['as' => 'update', 'uses' => 'IpsController@update']);
    });
    /*
     *  log
     */
    Route::group(['as' => 'log::', 'namespace' => 'Log', 'prefix' => 'log'], function () {
      Route::get('verify/{lvl?}', ['as' => 'verify', 'uses' => 'LogController@verify']);
      Route::get('{id}/show', ['as' => 'verify', 'uses' => 'LogController@show']);
      Route::get('/{query?}/{filter?}/{order?}', ['as' => 'index', 'uses' => 'LogController@index']);
    });
    /*
     *  orders payment types
     */
    Route::group(['as' => 'order-payment-types::', 'namespace' => 'Orders', 'prefix' => 'order-payment-types'], function () {
      Route::get('/', ['as' => 'index', 'uses' => 'OrderPaymentTypesController@index']);
      Route::post('store', ['as' => 'store', 'uses' => 'OrderPaymentTypesController@store']);
    });
    /*
     * postcode exceptions
     */
    Route::group(['as' => 'postcode-exceptions::', 'namespace' => 'Deliveries', 'prefix' => 'postcode-exceptions'], function () {
      Route::get('/', ['as' => 'index', 'uses' => 'PostcodeExceptionsController@index']);
      Route::post('store', ['as' => 'store', 'uses' => 'PostcodeExceptionsController@store']);
      Route::get('show', ['as' => 'show', 'uses' => 'PostcodeExceptionsController@show']);
    });
    /*
     *  roles-permissions
     */
    Route::group(['as' => 'roles-permissions::', 'namespace' => 'RolesPermissions', 'prefix' => 'roles-permissions'], function () {
      Route::get('/', ['as' => 'index', 'uses' => 'RolesPermissionsController@index']);
      Route::put('/update', ['as' => 'update', 'uses' => 'RolesPermissionsController@update']);
    });
    /*
     * routes
     */
    Route::group(['as' => 'routes::', 'namespace' => 'Route', 'prefix' => 'routes'], function () {
      Route::get('create', ['as' => 'create', 'uses' => 'RouteController@create']);
      Route::post('store', ['as' => 'store', 'uses' => 'RouteController@store']);
      Route::get('{id}/edit', ['as' => 'edit', 'uses' => 'RouteController@edit']);
      Route::get('{id}/show', ['as' => 'show', 'uses' => 'RouteController@show']);
      Route::put('{id}/update', ['as' => 'update', 'uses' => 'RouteController@update']);
      Route::get('/{query?}/{filter?}/{order?}', ['as' => 'index', 'uses' => 'RouteController@index']);
    });
    /*
     * sellers
     */
    Route::group(['as' => 'sellers::', 'namespace' => 'Seller', 'prefix' => 'sellers'], function () {
      Route::get('create', ['as' => 'create', 'uses' => 'SellerController@create']);
      Route::post('store', ['as' => 'store', 'uses' => 'SellerController@store']);
      Route::get('{id}/edit', ['as' => 'edit', 'uses' => 'SellerController@edit']);
      Route::get('{id}/show', ['as' => 'show', 'uses' => 'SellerController@show']);
      Route::put('{id}/update', ['as' => 'update', 'uses' => 'SellerController@update']);
      Route::get('/{query?}/{filter?}/{order?}', ['as' => 'index', 'uses' => 'SellerController@index']);
    });
    /*
     * seller-items
     */
    Route::group(['as' => 'seller-items::', 'namespace' => 'Seller', 'prefix' => 'seller-items'], function () {
      Route::get('/create', ['as' => 'create', 'uses' => 'SellerItemsController@create']);
      Route::post('/store', ['as' => 'store', 'uses' => 'SellerItemsController@store']);
      Route::get('{itemId}/edit', ['as' => 'edit', 'uses' => 'SellerItemsController@edit']);
      Route::get('{id}/show', ['as' => 'show', 'uses' => 'SellerItemsController@show']);
      Route::put('{itemId}/update', ['as' => 'update', 'uses' => 'SellerItemsController@update']);
      Route::get('{query?}/{filter?}/{order?}', ['as' => 'index', 'uses' => 'SellerItemsController@index']);
    });
    /*
     * stock
     */
    Route::group(['as' => 'stock::', 'namespace' => 'Stock', 'prefix' => 'stock'], function () {
      Route::get('create', ['as' => 'create', 'uses' => 'StockController@create']);
      Route::post('store', ['as' => 'store', 'uses' => 'StockController@store']);
      Route::post('/export/csv', ['as' => 'csv', 'uses' => 'StockController@exportAsCsv']);
      Route::post('/export/excel', ['as' => 'excel', 'uses' => 'StockController@exportAsExcel']);
      Route::put('{id}/add-stock', ['as' => 'add-stock', 'uses' => 'StockController@addStock']);
      Route::put('{id}/adjust-stock', ['as' => 'adjust-stock', 'uses' => 'StockController@adjustStock']);
      Route::get('{id}/block', ['as' => 'block', 'uses' => 'StockController@block']);
      Route::get('{id}/edit', ['as' => 'edit', 'uses' => 'StockController@edit']);
      Route::get('{id}/needs-attention', ['as' => 'needs-attention', 'uses' => 'StockController@needsAttention']);
      Route::get('{id}/show', ['as' => 'show', 'uses' => 'StockController@show']);
      Route::get('{id}/unblock', ['as' => 'unblock', 'uses' => 'StockController@unblock']);
      Route::put('{id}/update', ['as' => 'update', 'uses' => 'StockController@update']);
      Route::get('{id}/un-needs-attention', ['as' => 'un-needs-attention', 'uses' => 'StockController@unNeedsAttention']);
      Route::get('{id}/unwithdraw', ['as' => 'unwithdraw', 'uses' => 'StockController@unwithdraw']);
      Route::get('{id}/withdraw', ['as' => 'withdraw', 'uses' => 'StockController@withdraw']);
      Route::post('sku-active', ['as' => 'sku-active', 'uses' => 'StockController@changeActiveSKU']);
      Route::post('sku-inactive', ['as' => 'sku-inactive', 'uses' => 'StockController@changeInactiveSKU']);
      Route::get('/{query?}/{filter?}/{order?}', ['as' => 'index', 'uses' => 'StockController@index']);
    });
    /*
     * stock-orders
     */
     Route::group(['as' => 'stock-orders::', 'namespace' => 'Stock', 'prefix' => 'stock-orders'], function () {
       Route::get('{id}/{query?}/{filter?}/{order?}', ['as' => 'index', 'uses' => 'StockOrdersController@index']);
     });
    /*
     * stock categories
     */
    Route::group(['as' => 'stock-categories::', 'namespace' => 'Stock', 'prefix' => 'stock-categories'], function () {
      Route::get('create', ['as' => 'create', 'uses' => 'StockCategoryController@create']);
      Route::post('store', ['as' => 'store', 'uses' => 'StockCategoryController@store']);
      Route::get('{id}/edit', ['as' => 'edit', 'uses' => 'StockCategoryController@edit']);
      Route::get('{id}/show', ['as' => 'show', 'uses' => 'StockCategoryController@show']);
      Route::get('{id}/items', ['as' => 'show-items', 'uses' => 'StockCategoryController@showItems']);
      Route::put('{id}/update', ['as' => 'update', 'uses' => 'StockCategoryController@update']);
      Route::get('/{query?}/{filter?}/{order?}', ['as' => 'index', 'uses' => 'StockCategoryController@index']);
    });
    /*
     * stock stock categories
     */
    Route::group(['as' => 'stock-stock-categories::', 'namespace' => 'Stock', 'prefix' => 'stock-stock-categories'], function () {
      Route::get('create', ['as' => 'create', 'uses' => 'StockStockCategoryController@create']);
      Route::post('store', ['as' => 'store', 'uses' => 'StockStockCategoryController@store']);
      Route::get('{id}/edit', ['as' => 'edit', 'uses' => 'StockStockCategoryController@edit']);
      Route::get('{id}/show', ['as' => 'show', 'uses' => 'StockStockCategoryController@show']);
      Route::get('{id}/items', ['as' => 'show-items', 'uses' => 'StockStockCategoryController@showItems']);
      Route::put('{id}/update', ['as' => 'update', 'uses' => 'StockStockCategoryController@update']);
      Route::get('/{query?}/{filter?}/{order?}', ['as' => 'index', 'uses' => 'StockStockCategoryController@index']);
    });
    /*
     * stock costs import
     */
    Route::group(['as' => 'stock-costs-import::', 'namespace' => 'Stock', 'prefix' => 'stock-costs-import'], function () {
      Route::get('/', ['as' => 'index', 'uses' => 'StockCostsImportController@index']);
      Route::get('/headings', ['as' => 'headings', 'uses' => 'StockCostsImportController@headings']);
      Route::post('/store', ['as' => 'store', 'uses' => 'StockCostsImportController@store']);
      Route::post('/read', ['as' => 'read', 'uses' => 'StockCostsImportController@read']);
    });
    /*
     * stock log
     */
    Route::group(['as' => 'stock-log::', 'namespace' => 'Stock', 'prefix' => 'stock-log'], function () {
      Route::get('/{query?}/{filter?}/{order?}', ['as' => 'index', 'uses' => 'StockLogController@index']);
    });
    /*
     * stock upload
     */
    Route::group(['as' => 'stock-upload::', 'namespace' => 'Stock', 'prefix' => 'stock-upload'], function () {
      Route::get('/', ['as' => 'index', 'uses' => 'StockUploadController@index']);
      Route::get('/headings', ['as' => 'headings', 'uses' => 'StockUploadController@headings']);
      Route::post('store', ['as' => 'store', 'uses' => 'StockUploadController@store']);
      Route::post('/read', ['as' => 'read', 'uses' => 'StockUploadController@read']);
    });
    /*
     * suppliers
     */
    Route::group(['as' => 'suppliers::', 'namespace' => 'Supplier', 'prefix' => 'suppliers'], function () {
      Route::get('/', ['as' => 'index', 'uses' => 'SupplierController@index']);
      Route::get('/create', ['as' => 'create', 'uses' => 'SupplierController@create']);
      Route::post('/store', ['as' => 'store', 'uses' => 'SupplierController@store']);
      Route::get('/{id}/edit', ['as' => 'edit', 'uses' => 'SupplierController@edit']);
      Route::get('/{id}/show', ['as' => 'show', 'uses' => 'SupplierController@show']);
      Route::put('/{id}/update', ['as' => 'update', 'uses' => 'SupplierController@update']);
    });
    /*
     * testing
     */
     Route::group(['middleware' => ['role:tester']], function () {
      /*
       * call lists
       */
      Route::group(['as' => 'call-lists::', 'namespace' => 'CallLists', 'prefix' => 'call-lists'], function () {
        Route::get('/', ['as' => 'index', 'uses' => 'CallListsController@index']);
      });
    });
    /*
     * users
     */
    Route::group(['as' => 'users::', 'namespace' => 'User', 'prefix' => 'users'], function () {
      Route::get('create', ['as' => 'create', 'uses' => 'UserController@create']);
      Route::post('store', ['as' => 'store', 'uses' => 'UserController@store']);
      Route::get('{id}/edit', ['as' => 'edit', 'uses' => 'UserController@edit']);
      Route::get('{id}/show', ['as' => 'show', 'uses' => 'UserController@show']);
      Route::put('{id}/suspend', ['as' => 'suspend', 'uses' => 'UserController@suspend']);
      Route::put('{id}/update', ['as' => 'update', 'uses' => 'UserController@update']);
      Route::put('{id}/update-roles', ['as' => 'update-roles', 'uses' => 'UserController@updateRoles']);
      Route::get('{id}/reset-password', ['as' => 'reset-password', 'uses' => 'UserController@resetPassword']);
      Route::get('{id}/roles', ['as' => 'roles', 'uses' => 'UserController@roles']);
      Route::put('{id}/update-password', ['as' => 'update-password', 'uses' => 'UserController@updatePassword']);
      Route::get('/{query?}/{filter?}/{order?}', ['as' => 'index', 'uses' => 'UserController@index']);
    });
    /**
     *  vehicles
     */
    Route::group(['as' => 'vehicles::', 'namespace' => 'Vehicle', 'prefix' => 'vehicles'], function () {
      Route::get('create', ['as' => 'create', 'uses' => 'VehicleController@create']);
      Route::post('store', ['as' => 'store', 'uses' => 'VehicleController@store']);
      Route::get('{id}/costs-history', ['as' => 'costs-history', 'uses' => 'VehicleController@costsHistory']);
      Route::get('{id}/mileage-history', ['as' => 'mileage-history', 'uses' => 'VehicleController@mileageHistory']);
      Route::get('{id}/service-history', ['as' => 'service-history', 'uses' => 'VehicleController@serviceHistory']);
      Route::get('{id}/edit', ['as' => 'edit', 'uses' => 'VehicleController@edit']);
      Route::put('{id}/update', ['as' => 'update', 'uses' => 'VehicleController@update']);
      Route::get('{id}/show', ['as' => 'show', 'uses' => 'VehicleController@show']);
      Route::get('/{query?}/{filter?}/{order?}', ['as' => 'index', 'uses' => 'VehicleController@index']);
    });
    /**
     *  vehicle-cost-types
     */
    Route::group(['as' => 'vehicle-cost-types::', 'namespace' => 'Vehicle', 'prefix' => 'vehicle-cost-types'], function () {
      Route::get('/', ['as' => 'index', 'uses' => 'VehicleCostTypeController@index']);
      Route::post('store', ['as' => 'store', 'uses' => 'VehicleCostTypeController@store']);
      Route::put('{id}/update', ['as' => 'update', 'uses' => 'VehicleCostTypeController@update']);
    });
    /**
     *  vehicle-mileage
     */
    Route::group(['as' => 'vehicle-mileage::', 'namespace' => 'Vehicle', 'prefix' => 'vehicle-mileage'], function () {
      Route::get('/', ['as' => 'index', 'uses' => 'VehicleMileageController@index']);
      Route::post('store', ['as' => 'store', 'uses' => 'VehicleMileageController@store']);
      Route::get('{id}/edit', ['as' => 'edit', 'uses' => 'VehicleMileageController@edit']);
      Route::put('{id}/update', ['as' => 'update', 'uses' => 'VehicleMileageController@update']);
    });
    /**
     *  vehicle-costs
     */
    Route::group(['as' => 'vehicle-costs::', 'namespace' => 'Vehicle', 'prefix' => 'vehicle-costs'], function () {
      Route::get('/', ['as' => 'index', 'uses' => 'VehicleCostsController@index']);
      Route::post('store', ['as' => 'store', 'uses' => 'VehicleCostsController@store']);
      Route::get('{id}/edit', ['as' => 'edit', 'uses' => 'VehicleCostsController@edit']);
      Route::put('{id}/update', ['as' => 'update', 'uses' => 'VehicleCostsController@update']);
    });
  });


  Route::group(['middleware' => ['role:call_staff']], function () {
    /*
     * home
     */
    Route::group(['as' => 'home::'], function () {
      Route::get('/', ['as' => 'index', 'uses' => 'HomeController@index']);
      Route::get('{id}/show-document', ['as' => 'show-document', 'uses' => 'HomeController@showDocument']);
    });
    /*
     * bulletins
     */
    Route::group(['as' => 'bulletins::', 'namespace' => 'Bulletin', 'prefix' => 'bulletins'], function () {
      Route::get('/', ['as' => 'index', 'uses' => 'BulletinController@index']);
      Route::get('/create', ['as' => 'create', 'uses' => 'BulletinController@create']);
      Route::put('/destroy', ['as' => 'destroy', 'uses' => 'BulletinController@destroy']);
      Route::post('/store', ['as' => 'store', 'uses' => 'BulletinController@store']);
      Route::get('{id}/show', ['as' => 'show', 'uses' => 'BulletinController@show']);
      Route::put('{ids}/confirm-as-read', ['as' => 'confirm', 'uses' => 'BulletinController@confirmAsRead']);
    });
    
    /*
     * Magento
     */
    Route::group(['prefix' => 'magento'], function () {
    	
    	Route::get('/','MagentoController@index');
    	Route::get('/byDate','MagentoController@getOrdersByDate');
    	Route::get('/getInfo/{id}','MagentoController@getInfo');
    	
    });
    /*
     * Orders
     */
    Route::group(['prefix' => 'orders'], function () {

      Route::get('/', 'OrdersController@index');
      Route::get('/list', 'OrdersController@list_orders');
      Route::get('/beds/{address}','OrdersController@magento');
      
	  
      route::get('/unconfirmed','OrdersController@unconfirmed');
      
      //redirect for old links in table
      Route::get('/orderDetail.php', function () {
        return redirect('/orders/' . $_REQUEST['id']);
      });

      Route::get('search', 'OrdersController@search');
      Route::get('search2', 'OrdersController@search2');
      Route::get('{id}', 'OrdersController@order')->where('id', '[0-9]+');
      Route::get('2/{id}', 'OrdersController@order2');
      Route::get('{id}/genbarcode', 'OrdersController@genBarcode');
	  Route::get('{id}/print-label', 'OrdersController@printLabel');
	  Route::get('{id}/credit', 'OrdersController@creditOrder');
      Route::post('{id}/changestatus', 'OrdersController@changestatus');
      Route::post('{id}/route', 'OrdersController@changeroute');
      Route::post('{id}/payment', 'OrdersController@addpayment');
      Route::get('{id}/invoice', 'OrdersController@invoice');
      Route::get('{id}/refund', 'OrdersController@refund');
      Route::post('{id}/refund', 'OrdersController@refund');
      Route::post('{id}/discount', 'OrdersController@discount');
      Route::post('{id}/post', 'OrdersController@createPost');
      Route::post('{id}/missedParts', 'OrdersController@missedParts');
      Route::post('{id}/moveSupplier', 'OrdersController@moveSupplier');
      Route::get('{id}/resendLink', 'OrdersController@resendLink');
      Route::get('{id}/edit', 'OrdersController@edit');
      Route::post('{id}/cancel', 'OrdersController@cancel');
      Route::post('{id}/exchange', 'OrdersController@exchange');
      Route::put('{id}/make-priority', 'OrdersController@makePriority');
      Route::post('{id}/save', 'OrdersController@save');
      Route::post('{id}/set-route', 'OrdersController@SetRoute');
      Route::post('{id}/update', 'OrdersController@update');
      Route::put('{id}/un-process', 'OrdersController@unProcess');
      Route::post('{id}/wes', 'OrdersController@wes');
      Route::get('{id}/add-dispatched-route','OrdersController@addToDispatchedRoute');
      Route::get('/linked/{id}', 'OrdersController@linked');
      Route::get('/dispatched-routes','OrdersController@getDispatchedRoutes');
      Route::get('after-sales/{query?}/{filter?}/{order?}', 'OrdersController@showAfterSales');
      Route::get('cancelled/{query?}/{filter?}/{order?}', 'OrdersController@showCancelled');
      Route::get('collect/{query?}/{filter?}/{order?}', 'OrdersController@showCollect');
      Route::get('collected/{query?}/{filter?}/{order?}', 'OrdersController@showCollected');
      Route::get('escalations/{query?}/{filter?}/{order?}', 'OrdersController@showEscalations');
      Route::get('feedback/{query?}/{filter?}/{order?}', 'OrdersController@showFeedback');
      Route::get('not-loaded/{query?}/{filter?}/{order?}', 'OrdersController@showNotLoaded');
      Route::get('delivery-issue/{query?}/{filter?}/{order?}', 'OrdersController@showDelIssue');
      Route::get('missed-parts/{query?}/{filter?}/{order?}', 'OrdersController@showMissedParts');
      Route::get('on-hold/{query?}/{filter?}/{order?}', 'OrdersController@showOnHold');
      route::get('/unconfirmed/{query?}/{filter?}/{order?}','OrdersController@unconfirmed');
      Route::get('paypal/{query?}/{filter?}/{order?}', 'OrdersController@showPayPal');
      Route::get('post/manifest/{ids?}', 'OrdersController@postManifest');
      Route::get('post/{query?}/{filter?}/{order?}', 'OrdersController@showPost');
      Route::put('post/remove/{ids?}', 'OrdersController@removePost');
      Route::put('post/labels/{ids?}', 'OrdersController@printLabels');
      Route::get('refunds/{query?}/{filter?}/{order?}', 'OrdersController@showRefunds');
      Route::get('retention/{query?}/{filter?}/{order?}', 'OrdersController@showRetention');
      Route::get('tuffnells/{query?}/{filter?}/{order?}', 'OrdersController@showTuffnells');
      Route::get('urgent/{query?}/{filter?}/{order?}', 'OrdersController@showUrgent');
      Route::get('warehouse-errors/csv', 'OrdersController@warehouseErrorsCSV');
      Route::get('warehouse-errors/remove', 'OrdersController@warehouseErrorsRemove');
      Route::get('warehouse-errors/{query?}/{filter?}/{order?}', 'OrdersController@showWarehouseErrors');
      Route::get('wholesale-errors/{query?}/{filter?}/{order?}', 'OrdersController@showWholesaleErrors');
    });

    Route::group(['prefix' => 'notes'], function () {
      //id of order passed
      Route::post('{id}/addNote', 'OrderNotesController@store');

      //id of note passed
      Route::post('{id}/update', 'OrderNotesController@update');
    });
    /*
     * Mailer
     */
    Route::group(['prefix' => 'mailer'], function () {
      Route::get('/', 'MailController@index');
	  Route::post('/stockMail', 'MailController@sendStockMail');
      Route::get('/preview/{template}', 'MailController@preview');
      Route::get('test', 'MailController@test');
      Route::post('raw', 'MailController@raw');
      Route::post('invoice', 'MailController@invoice');
    });
    /*
     * Sale
     */
    Route::group(['prefix' => 'sale'], function () {
      Route::get('/', 'SaleController@company');
      Route::get('/{id}', 'SaleController@index');
      Route::post('/new', 'SaleController@store');
    });
    /**
    * Manufacturing Error
    */
    Route::group(['as' => 'manufacturing-error::', 'namespace' => 'ManufacturingError' ,'prefix' => 'manufacturing-error'], function () {
    	Route::get('/', ['as' => 'index', 'uses' => 'ManufacturingErrorController@index']);
    	Route::post('/save', ['as' => 'save', 'uses' => 'ManufacturingErrorController@saveError']);
    	Route::post('/upload', ['as' => 'upload', 'uses' => 'ManufacturingErrorController@upload']);
    	Route::post('/note', ['as' => 'note', 'uses' => 'ManufacturingErrorController@addNote']);
    	Route::get('/{id}', ['as' => 'show', 'uses' => 'ManufacturingErrorController@show']);
    	Route::get('images/{path}/{id}/{filename}', function ($path,$id,$filename)
    	{
    		
    		$file = Storage::disk('public')->get("$path/$id/$filename");
    		$response = Response::make($file, 200);
    		$response->header("Content-Type", 'image/jpeg');
    		
    		return $response;
    		
    	});
    	
    });
    	
    /*
     * Customers
     */
    Route::group(['prefix' => 'customer'], function () {
      Route::get('/', 'CustomerController@index');
      Route::get('{id}','CustomerController@customer');
      Route::post('{id}/update','CustomerController@update');
      Route::post('add','CustomerController@add');
      Route::get('search/name/{query}', ['as' => 'search', 'uses' => 'CustomerController@searchCustomer']);
    });
    
    /*
     * dispatch
     */
    Route::group(['as' => 'dispatch::', 'middleware' => ['permission:dispatch_view'], 'namespace' => 'Dispatch', 'prefix' => 'dispatch'], function () {
      /*
       * deliveries
       */
      Route::group(['as' => 'deliveries::', 'prefix' => 'deliveries'], function () {
        Route::get('/', ['as' => 'deliveries', 'uses' => 'DeliveryController@index']);
        Route::get('/clear/{route}', ['as' => 'deliveries', 'uses' => 'DeliveryController@clearRoute']);
        Route::get('/days', ['as' => 'days', 'uses' => 'RouteDaysController@index']);
        Route::get('/pool', ['as' => 'days', 'uses' => 'MapController@pool']);
        Route::post('/routeDels', ['as' => 'routeDels', 'uses' => 'DeliveryController@getRouteDels']);
        Route::get('/routeDels/{id}',  'DeliveryController@getRouteDelsTest');
        Route::post('/changeroute_mass', 'DeliveryController@changeroute_mass');
        Route::get('/map/{orders}/{route}', 'MapController@view_map');
        Route::get('/map/api/{function}/{data}', 'MapController@api');
        Route::post('/routing', 'DeliveryController@routing');
        // Route::get('{id}/edit', ['as' => 'edit', 'uses' => 'RouteController@edit']);
        // Route::get('{id}/show', ['as' => 'show', 'uses' => 'RouteController@show']);
      });
      /*
       * dispatched
       */
      Route::group(['as' => 'dispatched::', 'prefix' => 'dispatched'], function () {
        Route::get('/', ['as' => 'index', 'uses' => 'DispatchedController@index']);
        Route::post('sort','RouteController@sortOrder');
        Route::post('/setOptions', ['as' => 'index', 'uses' => 'RouteController@setOptions']);
        Route::get('{routeId}/show/{date}', ['as' => 'show', 'uses' => 'DispatchedController@show']);
        Route::get('orders/csv/{ids}', ['as' => 'csv', 'uses' => 'DispatchedController@getRouteCsv']);
        Route::get('orders/route-csv/{ids}', ['as' => 'csv', 'uses' => 'DispatchedController@getRouteCsv']);
        Route::get('orders/mark-as-delivered/{ids}', ['as' => 'set-as-delivered', 'uses' => 'DispatchedController@markAsDelivered']);
        Route::get('orders/print-cod-orders/{date?}', ['as' => 'print-cod-orders', 'uses' => 'DispatchedController@printCodOrders']);
        Route::get('orders/print-dispatch-notes/{ids}', ['as' => 'print-dispatch-notes', 'uses' => 'DispatchedController@printDispatchNotes']);
        Route::get('orders/generate-barcodes/{ids}', ['as' => 'generate-barcodes', 'uses' => 'DispatchedController@generateBarcodes']);
        Route::get('orders/print-labels/{ids}', ['as' => 'print-labels', 'uses' => 'DispatchedController@printLabels']);
        Route::get('orders/print-load-counts/{ids}', ['as' => 'print-dispatch-notes', 'uses' => 'DispatchedController@printLoadCounts']);
        Route::get('orders/print-supplier-orders/{date?}', ['as' => 'print-supplier-orders', 'uses' => 'DispatchedController@printSupplierOrders']);
        Route::get('orders/suppliers', ['as' => 'suppliers', 'uses' => 'DispatchedController@printSuppliersOrders']);
        Route::get('orders/text-shifts/{ids}', ['as' => 'text-shifts', 'uses' => 'DispatchedController@textShifts']);
        Route::get('oos/{items}', ['as' => 'oos', 'uses' => 'DispatchedController@oos']);
        
        Route::get('tox/', ['as' => 'tox', 'uses' => 'DispatchedController@tox']);
        // this is here until the route_id column if filled
        Route::get('{routeId}/show/{date}/{routeName}', ['as' => 'show2', 'uses' => 'DispatchedController@show']);
      });
      /*
       * load counts
       * DEPRICATED
       */
      Route::group(['as' => 'load-counts::', 'prefix' => 'load-counts'], function () {
        Route::get('/', ['as' => 'index', 'uses' => 'LoadCountsController@index']);
        Route::get('/day/{date}/{warehouse?}', ['as' => 'load-counts', 'uses' => 'LoadCountsController@loadCountsDay']);
        Route::get('/morning/{warehouse?}', ['as' => 'load-counts-morning', 'uses' => 'LoadCountsController@loadCountsMorning']);
        Route::get('/missed', ['as' => 'load-counts-missed', 'uses' => 'LoadCountsController@missed']);
      });
    });
    /*
     * drivers
     */
    Route::group(['as' => 'drivers::', 'namespace' => 'Driver', 'prefix' => 'drivers'], function () {
      Route::get('/', ['as' => 'index', 'uses' => 'DriverController@index']);
      Route::get('{id}/show', ['as' => 'show', 'uses' => 'DriverController@show']);
      Route::get('search/{query}', ['as' => 'search', 'uses' => 'DriverController@searchName']);
    });
    /*
     * driver mates
     */
    Route::group(['as' => 'driver-mates::', 'namespace' => 'Driver', 'prefix' => 'driver-mates'], function () {
      Route::get('/', ['as' => 'index', 'uses' => 'DriverMateController@index']);

      Route::get('{id}', ['as' => 'default', 'uses' => 'DriverMateController@show']);
      Route::get('{id}/show', ['as' => 'show', 'uses' => 'DriverMateController@show']);
      Route::get('search/{query}', ['as' => 'search', 'uses' => 'DriverMateController@searchName']);
    });
    /*
     * due-report
     */
    Route::group(['as' => 'due-report::', 'namespace' => 'Reports', 'prefix' => 'due-report'], function () {
      Route::get('/', ['as' => 'index', 'uses' => 'DueReportController@index']);
      Route::get('/headings/{profile}', ['as' => 'headings', 'uses' => 'DueReportController@getHeadings']);
      Route::post('/read', ['as' => 'read', 'uses' => 'DueReportController@read']);
      Route::post('/download/{profile}', ['as' => 'store', 'uses' => 'DueReportController@download']);
      Route::post('/store/{profile}', ['as' => 'store', 'uses' => 'DueReportController@store']);
    });
    /**
     * invoice
     */
    Route::group(['as' => 'invoice::', 'namespace' => 'Order', 'prefix' => 'invoice'], function () {
      Route::get('/{ids}/{vat?}', ['as' => 'index', 'uses' => 'InvoiceController@index']);
    });
    /*
     * load counts
     */
    Route::group(['as' => 'load-counts::', 'namespace' => 'LoadCounts', 'prefix' => 'load-counts'], function () {
      Route::get('/', ['as' => 'index', 'uses' => 'LoadCountsController@index']);
      Route::get('/show/{type}/{date}/{warehouse}', ['as' => 'show', 'uses' => 'LoadCountsController@htmlCounts']); // Link to Index.blade.HTML
      Route::get('/csv/{type}/{date}/{warehouse}', ['as' => 'print', 'uses' => 'LoadCountsController@csvCounts']);
      Route::get('/print/{type}/{date}/{warehouse}/{size}', ['as' => 'print', 'uses' => 'LoadCountsController@printCounts']);
      // Route::get('/morning/{warehouse?}', ['as' => 'load-counts-morning', 'uses' => 'LoadCountsController@loadCountsMorning']);
      // Route::get('/missed', ['as' => 'load-counts-missed', 'uses' => 'LoadCountsController@missed']);
    });
    /*
     * load sheets
     */
    Route::group(['as' => 'load-sheets::', 'namespace' => 'Dispatch', 'prefix' => 'load-sheets'], function () {
      Route::get('/', ['as' => 'index', 'uses' => 'LoadSheetsController@index']);
      Route::get('/show/{sheet}', ['as' => 'print', 'uses' => 'LoadCountsController@csvCounts']);
      // Route::get('/morning/{warehouse?}', ['as' => 'load-counts-morning', 'uses' => 'LoadCountsController@loadCountsMorning']);
      // Route::get('/missed', ['as' => 'load-counts-missed', 'uses' => 'LoadCountsController@missed']);
    });
    /*
     * mail list
     */
    Route::group(['as' => 'mail-list::', 'namespace' => 'MailList', 'prefix' => 'mail-lists'], function () {
      Route::get('/', ['as' => 'index', 'uses' => 'MailListController@index']);
    });
    /*
     * order import
     */
    Route::group(['as' => 'order-import::', 'namespace' => 'Order', 'prefix' => 'order-import'], function () {
      Route::get('/', ['as' => 'index', 'uses' => 'OrderImportController@index']);
      Route::get('/{profile}/create', ['as' => 'create', 'uses' => 'OrderImportController@create']);
      Route::get('/headings/{profile}', ['as' => 'headings', 'uses' => 'OrderImportController@headings']);
      Route::post('/store/{profile}', ['as' => 'headings', 'uses' => 'OrderImportController@store']);
      Route::post('/read', ['as' => 'read', 'uses' => 'OrderImportController@read']);
    });
    /*
     * print labels
     */
    Route::group(['as' => 'print-labels::', 'namespace' => 'Order', 'prefix' => 'print-labels'], function () {
    	Route::get('/post/{ids}', ['as' => 'post', 'uses' => 'LabelController@post']);
    	Route::get('/{ids}', ['as' => 'index', 'uses' => 'LabelController@index']);
    });
    /*
     * user
     */
    Route::group(['namespace' => 'User', 'prefix' => 'profile'], function () {
      Route::get('/', 'UserController@index');
      Route::put('/add-bookmark', ['as' => 'add-bookmark', 'uses' => 'UserController@addBookmark']);
      Route::put('/update-name', ['as' => 'update-name', 'uses' => 'UserController@updateName']);
      Route::put('/update-password', ['as' => 'update-password', 'uses' => 'UserController@updatePassword']);
    });
    /*
     * reports
     */
    Route::group(['as' => 'reports::', 'namespace' => 'Reports', 'prefix' => 'reports'], function () {
      Route::get('/', ['as' => 'index', 'uses' => 'ReportsController@index']);
      Route::get('/items', ['as' => 'items', 'uses' => 'ReportsController@items']);
      Route::get('/orders', ['as' => 'orders', 'uses' => 'ReportsController@orders']);
      Route::get('data/items/{from}/{to}/{companies}', ['as' => 'data', 'uses' => 'ReportsController@itemsData']);
      Route::get('data/orders/{from}/{to}/{companies}', ['as' => 'data', 'uses' => 'ReportsController@ordersData']);
    });
    /*
     * reports-2
     */
    Route::group(['as' => 'reports-2::', 'namespace' => 'Reports', 'prefix' => 'reports-2'], function () {
      Route::get('/', ['as' => 'index', 'uses' => 'Reports2Controller@index']);
      Route::get('/items', ['as' => 'items', 'uses' => 'Reports2Controller@items']);
      Route::get('/orders', ['as' => 'orders', 'uses' => 'Reports2Controller@orders']);
      Route::post('/excel', ['as' => 'print', 'uses' => 'Reports2Controller@exportToExcel']);
      Route::put('/print', ['as' => 'print', 'uses' => 'Reports2Controller@printToPdf']);
      Route::get('data/items/{from}/{to}', ['as' => 'data', 'uses' => 'Reports2Controller@itemsData']);
      Route::get('/sales/daily', ['as' => 'daily', 'uses' => 'Reports2Controller@daily']);
    });
    /*
     * routes
     */
    Route::group(['as' => 'routes::', 'namespace' => 'Dispatch', 'prefix' => 'routes'], function () {
      Route::get('/', ['as' => 'index', 'uses' => 'RouteController@index']);
      Route::get('search/{query}', ['as' => 'search', 'uses' => 'RouteController@search']);
      Route::get('{id}/show', ['as' => 'show', 'uses' => 'RouteController@show']);
      Route::get('{id}/show-orders', ['as' => 'show', 'uses' => 'RouteController@showOrders']);
    });
    /*
     * sellers
     */
    Route::group(['as' => 'sellers::', 'namespace' => 'Seller', 'prefix' => 'sellers'], function () {
      Route::get('/', ['as' => 'index', 'uses' => 'SellerController@index']);
      Route::get('search/{query}', ['as' => 'search', 'uses' => 'SellerController@searchName']);
      Route::get('{id}/show', ['as' => 'show', 'uses' => 'SellerController@show']);
    });
    /*
     * seller-orders
     */
    Route::group(['as' => 'seller-orders::', 'namespace' => 'Seller', 'prefix' => 'seller-orders'], function () {
      Route::get('/', ['as' => 'index', 'uses' => 'SellerOrdersController@index']);
      Route::get('{id}/show/{query?}/{filter?}/{order?}', ['as' => 'show', 'uses' => 'SellerOrdersController@show']);
      Route::get('{id}/export', ['as' => 'export', 'uses' => 'SellerOrdersController@exportCsv']);
      Route::get('{id}/export/{ids}', ['as' => 'export', 'uses' => 'SellerOrdersController@exportCsv']);
      Route::put('{id}/map/{ids}', ['as' => 'export', 'uses' => 'SellerOrdersController@map']);
      Route::put('{id}/maup/{ids}', ['as' => 'export', 'uses' => 'SellerOrdersController@maup']);
    });
    /*
     * stock
     */
    Route::group(['as' => 'stock::', 'namespace' => 'Stock', 'prefix' => 'stock'], function () {
      Route::get('/model-numbers/{modelNumbers}', ['as' => 'model-numbers', 'uses' => 'StockController@modelNumbers']);
      Route::get('{id}/show', ['as' => 'show', 'uses' => 'StockController@show']);
      Route::get('{id}/show-orders', ['as' => 'show-orders', 'uses' => 'StockController@showOrders']);
      Route::get('/search/item-code/{query}', ['as' => 'index', 'uses' => 'StockController@search']);
      Route::get('/{query?}/{filter?}/{order?}', ['as' => 'index', 'uses' => 'StockController@index']);
    });
    /*
     * stock-orders
     */
     Route::group(['as' => 'stock-orders::', 'namespace' => 'Stock', 'prefix' => 'stock-orders'], function () {
       Route::get('{id}/{query?}/{filter?}/{order?}', ['as' => 'index', 'uses' => 'StockOrdersController@index']);
	   Route::post('{id}', ['as' => 'index', 'uses' => 'StockOrdersController@emailList']);
     });

    /*
     * stock-categories
     */
    Route::group(['as' => 'stock-categories::', 'namespace' => 'Stock', 'prefix' => 'stock-categories'], function () {
      Route::get('/{query?}/{filter?}/{order?}', ['as' => 'index', 'uses' => 'StockCategoryController@index']);
    });
    /*
     * stock-category-items
     */
    Route::group(['as' => 'stock-category-items::', 'namespace' => 'Stock', 'prefix' => 'stock-category-items'], function () {
      Route::get('{id}/{query?}/{filter?}/{order?}', ['as' => 'index', 'uses' => 'StockCategoryItemsController@index']);
    });
    /*
     * suppliers
     */
    Route::group(['as' => 'suppliers::', 'namespace' => 'Supplier', 'prefix' => 'suppliers'], function () {
      Route::get('/', ['as' => 'index', 'uses' => 'SupplierController@index']);
      Route::get('/{id}/show', ['as' => 'show', 'uses' => 'SupplierController@show']);
      Route::post('/{id}/upload/manifest', ['as' => 'upload', 'uses' => 'InvoiceController@upload']);
      /*
       * invoices
       */
      Route::group(['as' => 'invoices::', 'prefix' => '/{id}/invoices'], function () {
        Route::get('/', ['as' => 'index', 'uses' => 'InvoiceController@index']);
        Route::put('/{iid}/paid', ['as' => 'paid', 'uses' => 'InvoiceController@paidInvoice']);
        Route::get('/{iid}/csv', ['as' => 'invoice-orders', 'uses' => 'SupplierOrdersController@exportCsv']);
      });
      /*
       * orders
       */
      Route::group(['as' => 'orders::', 'prefix' => '{id}/orders'], function () {
        Route::get('/export/csv/{full?}', ['as' => 'export-csv', 'uses' => 'SupplierOrdersController@exportCsv']);
        Route::get('/export/excel/{full?}', ['as' => 'export-excel', 'uses' => 'SupplierOrdersController@exportExcel']);
        Route::put('/{ids}/move-supplier/{sid}', ['as' => 'move', 'uses' => 'SupplierOrdersController@moveOrders']);
        Route::put('/ordered', ['as' => 'ordered', 'uses' => 'SupplierOrdersController@ordered']);
        Route::put('/process', ['as' => 'process', 'uses' => 'SupplierOrdersController@process']);
        Route::put('/un-ordered', ['as' => 'un-ordered', 'uses' => 'SupplierOrdersController@unOrdered']);
        Route::put('/un-process', ['as' => 'un-process', 'uses' => 'SupplierOrdersController@unProcess']);
        Route::get('/{query?}/{filter?}/{order?}', ['as' => 'index', 'uses' => 'SupplierOrdersController@index']);
      });
      /*
       * route-days
       */
      Route::group(['as' => 'route-days::', 'prefix' => '{id}/route-days'], function () {
        Route::get('/', ['as' => 'index', 'uses' => 'SupplierRouteDaysController@index']);
      });
    });
    /*
     * tracking
     */
    Route::group(['as' => 'tracking::', 'prefix' => 'tracking'], function () {
      Route::group(['namespace' => 'Tracking'], function () {
        Route::get('/', ['as' => 'index', 'uses' => 'TrackingController@index']);
        Route::get('/{import}/create', ['as' => 'create', 'uses' => 'TrackingController@create']);
      });
      Route::group(['as' => 'import::'], function () {
        Route::get('{import}/headings', ['as' => 'profile', 'uses' => 'CsvImportController@headings']);
        Route::get('{import}/profile', ['as' => 'profile', 'uses' => 'CsvImportController@profile']);
        Route::post('{import}/store', ['as' => 'store', 'uses' => 'CsvImportController@store']);
      });
    });
    /*
     * vehicles
     */
    Route::group(['as' => 'vehicles::', 'prefix' => 'vehicles'], function () {
      Route::get('/', ['as' => 'index', 'uses' => 'VehicleController@index']);
      Route::post('{id}', ['as' => 'default', 'uses' => 'VehicleController@show']);
      Route::get('{id}/show', ['as' => 'show', 'uses' => 'VehicleController@show']);
      Route::get('search/{subject}/{query}', ['as' => 'search', 'uses' => 'VehicleController@searchName']);
    });
    // /**
    //  *  vehicle-costs
    //  */
    // Route::group(['as' => 'vehicle-costs::', 'namespace' => 'Vehicle', 'prefix' => 'vehicle-costs'], function () {
    //   Route::get('/', ['as' => 'index', 'uses' => 'VehicleCostsController@index']);
    //   Route::post('/store', ['as' => 'store', 'uses' => 'VehicleCostsController@store']);
    //   Route::put('{id}/update', ['as' => 'update', 'uses' => 'VehicleCostsController@update']);
    // });
    //
    	
    Route::group(['namespace' => 'Sms', 'prefix' => 'sms'], function () {
        Route::get('/credit', 'SmsController@credit');
    });
    /*
     * testing
     */
    Route::group(['as' => 'testing::', 'middleware' => ['role:tester']], function () {
      /*
       * call lists
       */
      Route::group(['as' => 'call-lists::', 'namespace' => 'CallLists', 'prefix' => 'call-lists'], function () {
        Route::get('/', ['as' => 'index', 'uses' => 'CallListsController@index']);
      });
    });
  });

  Route::group(['middleware' => ['role:staff']], function () {
    /*
     * message
     */
    Route::group(['as' => 'message::', 'namespace' => 'Message', 'prefix' => 'message'], function () {
      Route::get('/', ['as' => 'index', 'uses' => 'MessageController@index']);
      Route::get('read/{id}', ['as' => 'read', 'uses' => 'MessageController@read']);
      Route::get('show/{id?}', ['as' => 'show', 'uses' => 'MessageController@show']);
      Route::post('send', ['as' => 'send', 'uses' => 'MessageController@send']);
      Route::get('new', ['as' => 'new', 'uses' => 'MessageController@getNew']);
      Route::get('new/{ids}', ['as' => 'new', 'uses' => 'MessageController@shownNew']);
    });
  });
  
  
      Route::group(['middleware' => ['role:staff']], function () {
          /*
           * message
           */
          Route::group(['as' => 'containers::', 'prefix' => 'containers'], function () {
              Route::get('/', ['as' => 'index', 'uses' => 'ContainerController@index']);
              Route::post('/add', ['as' => 'index', 'uses' => 'ContainerController@store']);
              Route::get('/rxd/{id}', ['as' => 'index', 'uses' => 'ContainerController@rxd']);
              Route::get('/remove/{id}', ['as' => 'index', 'uses' => 'ContainerController@removeRow']);
          });
      });
  
});

Route::get('/orders/duplicates', 'DuplicateOrdersController@index')->name('duplicate.orders.index');

Route::get('/review/routes', 'ReviewRouteController@index');
// Route::get('/review/routes/{date}', 'ReviewRouteController@index');
Route::get('/review/routes/{route}/orders', 'ReviewRouteController@show');
Route::get('/review/routes/{route}/orders/{date}', 'ReviewRouteController@show');
Route::post('/review/routes', 'ReviewRouteController@store');
Route::get('/review/routes/download/csv', 'ReviewRouteController@downloadCsv');

Route::get('/orders/authorisations/refunds', 'OrderAuthorisationController@refunds');
Route::get('/orders/authorisations/discounts', 'OrderAuthorisationController@discounts');

/**
 * Notifications
 */
Route::get('/notifications/partial', 'NotificationController@sentPartial')->name('notifications.showPartial')->middleware('auth');
Route::get('/notifications/{type?}', 'NotificationController@index')->name('notifications.index')->middleware('auth');
Route::get('/notifications/send/{flag}','NotificationController@sentFlag')->name('notifications.sentFlag')->middleware('auth');
Route::get('/notifications/show/{id}', 'NotificationController@show')->name('notifications.show')->middleware('auth');
  // Messages
Route::get('/notification/messages/{message}/resend','NotificationMessageController@resend')->name('notificaitonMessage.resend')->middleware('auth');