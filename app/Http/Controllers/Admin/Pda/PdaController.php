<?php

namespace App\Http\Controllers\Admin\Pda;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\PDA;

class PdaController extends Controller
{
	

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    	return view('admin.pda.index',['pda' => PDA::getPdaData()]);
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    
    
}
