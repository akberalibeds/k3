<?php

namespace App\Http\Controllers\Admin\Maps;

use Entrust;
use App\Info;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Settings;
use Time;

class MapsController extends Controller
{
  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    return view('bulletins.create');
  }
  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    //
  }
  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit()
  {
    return view('admin.maps.edit', ['settings' => Settings::find(1)->toArray() ]);
  }
  /**
   * Display a listing of the resource.
   *
   * @author Giomani Designs
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    return view('admin.maps.index', [ 'settings' => Settings::find(1)->toArray() ]);
  }

  /**
   * Update the specified resource in storage.
   *
   * @author Giomani Designs
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request)
  {
	
	$data = $request->except('_token');
	
	$settings  = Settings::find(1);
	$settings->update($data);
	
	return view('admin.maps.edit', [ 'settings' => $settings->toArray() , 'save' => 1 ]);
	
  }
  
  
}
