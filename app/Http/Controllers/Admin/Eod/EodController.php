<?php

namespace App\Http\Controllers\Admin\Eod;

use App\Order;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

/**
 * End Of Day Controller
 */
class EodController extends Controller
{
  /**
   * @author Giomani Designs (Development Team)
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function index(Request $request) {
    if ($request->ajax()) {
      return Order::getEod($request->all());
    }
    $dates = [];
    $date = new \Datetime;
    $interval = new \DateInterval('P1D');
    for ($i = 0; $i < config('other.edo-order-stats-days'); $i++) {
      $date->format('d-m-Y');
      $date->sub($interval);
      $dates[] = $date->format('d-M-Y');
    }

    return view('admin.eod.index', ['dates' => $dates]);
  }
}
