<?php
/** @author Giomani Designs **/

namespace App\Http\Controllers\Admin\Dashboard;

use Entrust;
use App\Info;
use App\Helper\Files;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DashboardController extends Controller
{
  /**
   * add file
   *
   * @param $array $documents
   * @param  \Illuminate\Http\Request  $request
   * @return
   */
  private function addDocument($request) {
    $files = new Files;
    $result = $files->recieve($request);

    if ($result !== false) {
      $pathParts = explode('/', $result);
      $filename = array_pop($pathParts);

      $document = Info::addDocument($request->input('name'), $filename);

      return [
        'result' => 'complete',
        'document' => [
          'description' => $document->description,
          'name' => $document->datum,
        ]
      ];
    }
    return [];
  }
  /**
   * add email(s)
   *
   * @param $array $emails
   * @return
   */
  private function addEmails($emails) {
    return Info::addEmails($emails);
  }
  /**
   * add link(s)
   *
   * @param $array $links
   * @return
   */
  private function addLinks($links) {
    return Info::addLinks($links);
  }
  /**
   * add telephone number(s)
   *
   * @param $array $numbers
   * @return
   */
  private function addNumbers($numbers) {
    return Info::addNumbers($numbers);
  }
  /**
   * Store a newly created resource in storage.
   *
   * @param  string $info
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request, $info) {
    if (!Entrust::can('dashboard_admin')) {
      return parent::notAuthorised();
    }

    switch ($info) {
      case 'document':
        return $this->addDocument($request);
        break;
      case 'emails':
        return $this->addEmails($request->emails);
        break;
      case 'links':
        return $this->addLinks($request->links);
        break;
      case 'numbers':
        return $this->addTelephoneNumbers($request->numbers);
        break;
    }
  }
  /**
   * Display the specified resource.
   *
   * @param  string $info
   * @return \Illuminate\Http\Response
   */
  public function show($info) {
    if (!Entrust::can('dashboard_admin')) {
      return parent::notAuthorised();
    }
    switch ($info) {
      case 'documents':
        $view = 'admin.dashboard.documents';
        $data = ['documents' => Info::getDocuments()];
        break;
      case 'emails':
        $view = 'admin.dashboard.emails';
        $data = ['emails' => Info::getEmails()];
        break;
      case 'links':
        $view = 'admin.dashboard.links';
        $data = ['links' => Info::getLinks()];
        break;
      case 'numbers':
        $view = 'admin.dashboard.numbers';
        $data = ['numbers' => Info::getTelephoneNumbers()];
        break;
      default:
        $view = 'home';
    }

    return view($view, $data);
  }
  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id) {
    if (!Entrust::can('dashboard_admin')) {
      return parent::notAuthorised();
    }
  }
}
