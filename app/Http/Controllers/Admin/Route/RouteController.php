<?php

namespace App\Http\Controllers\Admin\Route;

use App\Routes;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Requests\RoutePutRequest;
use App\Http\Requests\RoutePostRequest;
use App\Http\Controllers\Controller;

class RouteController extends Controller
{
  /**
   * @author Giomani Designs
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit ($id) {
    try {
      $route = Routes::findOrFail($id);
    } catch(ModelNotFoundException $e) {
      return parent::notFound('route', ['id' => $id]);
    }
    $route = Routes::getDays($id);
    $days = ['monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday'];
    foreach ($days as $day) {
      if (stristr(strtolower($route->days), $day)) {
        $route[$day] = true;
      } else {
        $route[$day] = false;
      }
    }
    return view('admin.routes.edit', ['route' => $route]);
  }
  /**
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function index (Request $request) {
    if ($request->ajax()) {
      $query = $request->all();
      return Routes::getAllDays($query);
    }
    return view('admin.routes.index');
  }
  /**
   * @author Giomani Designs
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show ($id) {
    try {
      $route = Routes::findOrFail($id);
    } catch (ModelNotFoundException $e) {
      return parent::notFound('route', ['id' => $id]);
    }
    return view('admin.routes.show', ['route' => $route]);
  }
  /**
   * @author Giomani Designs
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store (Request $request) {
    $route = new Routes;
    $inputs = $request->only(['name', 'max', 'van', 'msg', 'is_wholesale', 'in_load_count', 'is_route_route']);
    foreach ($inputs as $input => $value) {
      $route->$input = $value;
    }
    $route->save();
    return view('admin.routes.show', ['route' => $route]);
  }
  /**
   * @author Giomani Designs
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update (Request $request, $id) {
    try {
      $route = Routes::findOrFail($id);
    } catch(ModelNotFoundException $e) {
      return parent::notFound('route', ['id' => $id]);
    }
    $inputs = $request->only(['name', 'max', 'van', 'msg', 'is_wholesale', 'in_load_count', 'is_route_route']);
    foreach ($inputs as $input => $value) {
      $route->$input = $value;
    }
    $route->save();
    $days = ['monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday'];
    $inputs = $request->only($days);
    $deliversOn = [];
    foreach ($days as $day) {
      if ($inputs[$day] == '1') {
        $deliversOn[] = $day;
      }
    }
    Routes::setRouteDays($id, $deliversOn, $route->max, $route->name);

    return view('admin.routes.show', ['route' => $route]);
  }
}
