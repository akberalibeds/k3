<?php

namespace App\Http\Controllers\Admin;

use App\IamCookie;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AdminController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function index(Request $request) {
    $iam = IamCookie::getCookie();
    return view('admin.index', ['currentIpa' => $request->ip(), 'iam' => $iam]);
  }
}
