<?php

namespace App\Http\Controllers\Admin\Iam;

use Entrust;
use App\IamCookie;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class IamController extends Controller
{
  /**
   *
   */
  public function index() {
    if (!Entrust::can('root'))  {
      return parent::notAuthorised();
    }
    return view('admin.iam.index');
  }
  /**
   * @param Illuminate\Http\Request $request
   */
  public function store(Request $request) {
    if (!Entrust::can('root'))  {
      return parent::notAuthorised();
    }
    IamCookie::setCookie(request()->input('iam'));

    return redirect()->route('admin::iam::index');
  }
}
