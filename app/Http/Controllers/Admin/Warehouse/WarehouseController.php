<?php

namespace App\Http\Controllers\Admin\Warehouse;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\WarehouseStock;
use App\Warehouse;
use App\StockItem;
use App\WarehouseLog;
use App\Http\Requests\WarehouseStockChangeRequest;



class WarehouseController extends Controller
{
  
  /**
   * Display a listing of the resource.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function stock(Request $request) {
  	
  	$warehouses = Warehouse::getWarehouses();
  	
  	if($request->ajax())
  		return $stock = WarehouseStock::getStockCount($request);
   	
  	
  	return view('admin.warehouse.index',['warehouses' => $warehouses]);
  	
  }
  
  
  public function show(Request $request,$id) {
  	 
  	$warehouse = Warehouse::find($id);
        $warehouses = Warehouse::getWarehouses()->except($id);
        
  	$warehouses[] = ['location'=>"Container"];
  	$warehouses[] = ['location' => "Delivery To Customer"];
  	$warehouses[] = ['location' => "Return From Customer"];
  	
    if($request->ajax())
  		return $stock = WarehouseStock::getStockCount($request,$id, $request->itemCode);
  
  		
  		return view('admin.warehouse.show',['warehouse' => $warehouse,'warehouses' => $warehouses]);
  		 
  }
  
  
  
  public function export(Request $request =null,$id=null){
  	
  	$table = WarehouseStock::getStockCountCSV($request,$id)->toArray();
  	
  	$filename = storage_path("stock.csv");
  	$handle = fopen($filename, 'w');
  	fputcsv($handle, ['item','box','quantity','complete']);
  	
  	foreach($table as $row) {

  		fputcsv($handle, [$row['itemCode'],$row['part'],$row['quantity'],$row['full']]);

  	}
  	
  	fclose($handle);
  	
  	$headers = array(
  			'Content-Type' => 'text/csv',
  	);
  	
  	return response()->download(storage_path('stock.csv'),'stock.csv', $headers);
  	
  }
  
  
  
  public function addPart(WarehouseStockChangeRequest $request){
    try {
      return WarehouseStock::moveStockPart($request->stockPart(), $request->thatWarehouse(), $request->thisWarehouse(), $request->reason(), $request->qty());
    } catch (\RuntimeException $ex) {
      return ['error'=>$ex->getMessage()];
    }
    
  }
  
  public function removePart(WarehouseStockChangeRequest $request){
    try {
      return WarehouseStock::moveStockPart($request->stockPart(), $request->thisWarehouse(), $request->thatWarehouse(), $request->reason(), $request->qty());
    } catch (\RuntimeException $ex) {
      return ['error'=>$ex->getMessage()];
    }
  }
  
  public function addItem(WarehouseStockChangeRequest $request){
    try {
      return WarehouseStock::moveStockItem($request->stockItem(), $request->thatWarehouse(), $request->thisWarehouse(), $request->reason(), $request->qty());
    } catch (\RuntimeException $ex) {
      return ['error'=>$ex->getMessage()];
    }
  }
  
  public function removeItem(WarehouseStockChangeRequest $request){
    try {
      return WarehouseStock::moveStockItem($request->stockItem(), $request->thisWarehouse(), $request->thatWarehouse(),  $request->reason(), $request->qty());
    } catch (\RuntimeException $ex) {
      return ['error'=>$ex->getMessage()];
    }
  }
  
  public function logItem($id) {
    $wh_stock_item = WarehouseStock::find($id);
    $part = $wh_stock_item->stock_part;
    $item = $part->stock_item;
    $log=WarehouseLog::where('warehouse_stock_id',$id)->orderBy('updated_at','desc')->limit(50)->get();
    $log->load('user');
    return View('admin.warehouse.log',['logs'=>$log,'itemcode'=>$part->stockItem->itemCode,'part'=>$part->part,'location'=>$wh_stock_item->warehouse->location]);
  }
  
  public function upload(Request $request) {
    $this->validate($request, [
      'csv' => 'required',
      'thatWarehouse' => 'required',
      'direction' => 'required',
      ]);
    if (!$request->hasFile('csv') ) {
      return "no file";
    }

    $action = new \App\Actions\UpdateWarehouseStock();
    
    $csvpath=$request->csv->move('../storage/csv')->getRealpath();
    $csv = fopen($csvpath,'r');
    while ($record = fgetcsv($csv)) {
      if (count($record)>1) {
        $itemCode = $record[0];
        $qty=$record[1];
        $action->addItem(['itemCode'=>$itemCode,'qty'=>$qty]);
      }
    }

    $thisWarehouse=Warehouse::find($request->id);
    $thatWarehouse=Warehouse::where('location',$request->thatWarehouse)->first();  

    if ($thatWarehouse) {
      if ($request->direction=='in') {
        $action->fromWarehouse = $thatWarehouse;
        $action->toWarehouse = $thisWarehouse;
      } else {
        $action->fromWarehouse = $thisWarehouse;
        $action->toWarehouse = $thatWarehouse;
      }
      $action->twoway = true;
      
    } else {
      $action->reason = $request->thatWarehouse;
      if ($request->direction == 'in') {
        $action->thisWarehouse = $thisWarehouse;
        $action->sign = 1;
      } else {
        $action->thisWarehouse = $thisWarehouse;
        $action->sign = -1;
      }
      $action->twoway = false;

    }

    $action->testRun();
    if ($action->success()) {
      $action->liveRun();
    }
    $action->save();

    return view('admin.warehouse.importresults',['action'=>$action,'apply_route'=>action('Admin\Warehouse\WarehouseController@apply')]);
    
  }
  
  public function apply() {
    $action = session(request('sessionid'));
    if ($action instanceof \App\Actions\UpdateWarehouseStock) {
      $action->liveRun();
      return view('admin.warehouse.importresults',['action'=>$action]);
    } else {
      dd(session(request('sessionid')));
    }
    
  }
  
  
  function makeIndexed($arr) {
    $result = [];
    foreach ($arr as $item) {
      $result[$item->id]=$item;
    }
    return $result;
  }
}
