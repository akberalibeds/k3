<?php

namespace App\Http\Controllers\Admin\Deliveries;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\PostcodeDeliveryException;
use App\Http\Controllers\Controller;

class PostcodeExceptionsController extends Controller
{
  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id) {
      //
  }
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index() {
    $postcodes = PostcodeDeliveryException::select('postcode')->orderBy('postcode', 'asc')->get();
    return view('admin.postcode-exceptions.index', ['postcodes' => $postcodes->pluck('postcode')]);
  }
  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request) {
    $postcode = $request->input('postcode');
    $postcodeException = new PostcodeDeliveryException();
    $postcodeException->postcode = $postcode;
    $postcodeException->save();
    return $postcode;
  }
}
