<?php

namespace App\Http\Controllers\Admin\Deliveries;

use App\FreeSaturdays;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FreeSaturdaysController extends Controller
{
  /**
   * @author Giomani Designs
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function index (Request $request) {
    $freeSaturdays = FreeSaturdays::getWithStaff();
    return view('admin.free-saturdays.index', ['freeSaturdays' => $freeSaturdays]);
  }
}
