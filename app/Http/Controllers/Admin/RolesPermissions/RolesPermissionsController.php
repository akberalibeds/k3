<?php
namespace App\Http\Controllers\Admin\RolesPermissions;

use DB;
use Auth;
use Event;
use Entrust;
use App\Role;
use App\User;
use App\Permission;
use App\Http\Requests;
use App\PermissionRole;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\UserPostRequest;
use App\Http\Requests\ChangePasswordRequest;
use App\Events\SystemRolesPermissionsChange;

class RolesPermissionsController extends Controller
{
  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create() {
    return view('admin.role.create');
  }
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index() {
    if (!Auth::user()->can('manage_perms_roles')) {
      return parent::notAuthorised();
    }
    $permissions = Permission::orderBy('display_name', 'ASC')->get();
    $rolesPermissions = Role::withPermissions();
    // dump($permissions);
    // dump($rolesPermissions);
    // dd();
    return view('admin.roles-permissions.index', ['rolesPermissions' => $rolesPermissions, 'permissions' => $permissions]);
  }
  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id) {
    if (!Auth::user()->can('manage_perms_roles')) {
      return parent::notAuthorised();
    }
    $user = User::where('id', '=', $id)->first();
    $userRoles = $user->roles;
    return view('user.show', ['user' => $user, 'userRoles' => $userRoles]);
  }
  /**
   * Update resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request) {
    if (!Auth::user()->can('manage_perms_roles')) {
      return parent::notAuthorised();
    }
    $additions = $request->input('changes.add');
    $removes = $request->input('changes.remove');
    $roles = [];
    if (isset($additions)) {
      foreach ($additions as $add) {
        $change = explode(',', $add);
        $roles[$change[0]][] = $change[1];
      }
      foreach ($roles as $roleId => $permissions) {
        PermissionRole::addPermissions($roleId, $permissions);
      }
    }
    if (isset($removes)) {
      $roles = [];
      foreach ($removes as $remove) {
        $change = explode(',', $remove);
        $roles[$change[0]][] = $change[1];
      }
      foreach ($roles as $id => $permissions) {
        PermissionRole::removePermissions($id, $permissions);
      }
    }
    Event::fire(new SystemRolesPermissionsChange($additions, $removes));
  }
}
