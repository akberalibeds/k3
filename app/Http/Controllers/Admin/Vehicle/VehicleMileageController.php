<?php

namespace App\Http\Controllers\Admin\Vehicle;

use Auth;
use Entrust;
use App\Vehicle;
use App\Http\Requests;
use App\VehicleMileage;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class VehicleMileageController extends Controller
{
  /**
   * Show the form for editing the specified resource.
   *
   * @author Giomani Designs
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit($id) {
  }
  /**
   * Display a listing of the resource.
   *
   * @author Giomani Designs
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function index(Request $request) {
    $vehicles = VehicleMileage::getVehicleMileages();

    return view('admin.vehicle-mileage.index', ['vehicles' => $vehicles]);
  }
  /**
   * Store a newly created resource in storage.
   *
   * @author Giomani Designs
   * @param  \App\Http\Requests\VehiclePostRequest  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request) {
    $rows = [];
    $staffId = Auth::user()->id;
    $data = $request->only('mileages')['mileages'];

    // the mass assigning means that the created_on
    // column will not be filfilled by laravel
    $createdOn = date('Y-m-d H:i:s');

    for ($i = 0, $j = count($data); $i < $j; $i++) {
      $datum = $data[$i];
      $rows[] = [
        'created_on' => $createdOn,
        'mileage' => $datum['mileage'],
        'staff_id' => $staffId,
        'vehicle_id' => $datum['vehicleId'],
      ];
    }
    VehicleMileage::massStore($rows);
  }
  /**
   * Update the specified resource in storage.
   *
   * @author Giomani Designs
   * @param  \App\Http\Requests\VehiclePutRequest  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(VehiclePutRequest $request, $id) {
  }
}
