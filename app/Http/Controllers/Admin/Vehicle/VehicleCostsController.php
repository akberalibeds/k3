<?php

namespace App\Http\Controllers\Admin\Vehicle;

use Auth;
use Event;
use Entrust;
use App\Vehicle;
use App\VehicleCost;
use App\Http\Requests;
use App\VehicleCostType;
use Illuminate\Http\Request;
use App\Events\VehicleCostAdding;
use App\Http\Controllers\Controller;

class VehicleCostsController extends Controller
{
  /**
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function index(Request $request) {
    if (!Entrust::can('vehicle_admin'))  {
      return parent::notAuthorised();
    }
    if ($request->ajax()) {
      return VehicleCost::getVehicleCosts($request->all());
    }
    $vehicles = Vehicle::all();
    $vehicleCostsTypes = VehicleCostType::all();

    return view('admin.vehicle-costs.index', ['vehicles' => $vehicles, 'vehicleCostsTypes' => $vehicleCostsTypes]);
  }
  /**
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request) {
    if (!Entrust::can('vehicle_admin')) {
      return parent::notAuthorised();
    }
    $inputs = $request->only(['cost', 'date_incurred', 'mileage', 'notes', 'vehicle_cost_type_id', 'vehicle_id']);
    if (($response = parent::validateForm($inputs, VehicleCost::$rules['store'], $request)) !== true) {
      return $response;
    }
    $vehicleCost = new VehicleCost;
    $inputs['date_incurred'] = date('Y:m:d', strtotime($inputs['date_incurred']));
    $inputs['user_id'] = Auth::user()->id;
    foreach ($inputs as $input => $value) {
      $vehicleCost->$input = $value;
    }
    $vehicleCost->save();

    Event::fire(new VehicleCostAdding([$inputs['cost'], $inputs['vehicle_cost_type_id'], $inputs['vehicle_id']]));

    return $vehicleCost->toArray();
  }
}
