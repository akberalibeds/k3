<?php
namespace App\Http\Controllers\Admin\Vehicle;

use Entrust;
use App\Vehicle;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class VehicleController extends Controller
{
  /**
   * @author Giomani Designs
   * @param  \App\Http\Requests\VehiclePostRequest  $request
   * @return \Illuminate\Http\Response
   */
  public function costsHistory (Request $request, $id) {
    if (!Entrust::can('vehicle_admin'))  {
      return parent::notAuthorised();
    }
    try {
      $vehicle = Vehicle::findOrFail($id);
    } catch (ModelNotFoundException $e) {
      return parent::notFound();
    }
    if ($request->ajax()) {
      return Vehicle::getCosts($id);
    }
    return view('admin.vehicles.costs-history', ['vehicle' => $vehicle]);
  }
  /**
   * @author Giomani Designs
   * @return \Illuminate\Http\Response
   */
  public function create() {
    if (!Entrust::can('vehicle_admin'))  {
      return parent::notAuthorised();
    }
    return view('admin.vehicles.create');
  }
  /**
   * @author Giomani Designs
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit($id) {
    if (!Entrust::can('vehicle_admin'))  {
      return parent::notAuthorised();
    }
    try {
      $vehicle = Vehicle::findOrFail($id);
    } catch(ModelNotFoundException $e) {
      return parent::notFound();
    }
    return view('admin.vehicles.edit', ['vehicle' => $vehicle]);
  }
  /**
   * @author Giomani Designs
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function index(Request $request) {
    if (!Entrust::can('vehicle_view'))  {
      return parent::notAuthorised();
    }
    if ($request->ajax()) {
      return Vehicle::getAll($request->all());
    }
    return view('admin.vehicles.index');
  }
  /**
   * @author Giomani Designs
   * @param  \App\Http\Requests\VehiclePostRequest  $request
   * @return \Illuminate\Http\Response
   */
  public function mileageHistory () {
    if (!Entrust::can('vehicle_admin'))  {
      return parent::notAuthorised();
    }
    return view('admin.vehicles.mileage-history');
  }
  /**
   * @author Giomani Designs
   * @param  \App\Http\Requests\VehiclePostRequest  $request
   * @return \Illuminate\Http\Response
   */
  public function serviceHistory() {
    if (!Entrust::can('vehicle_admin'))  {
      return parent::notAuthorised();
    }
    return view('admin.vehicles.service-history');
  }
  /**
   * Display the specified resource.
   *
   * @author Giomani Designs
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id) {
    if (!Entrust::can('vehicle_view'))  {
      return parent::notAuthorised();
    }
    $vehicle = Vehicle::get($id);
    if ($vehicle->count() === 0) {
      return parent::notFound();
    }
    return view('admin.vehicles.show', ['vehicle' => $vehicle->first(), 'history' => $vehicle]);
  }
  /**
   * @author Giomani Designs
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request) {
    if (!Entrust::can('vehicle_admin'))  {
      return parent::notAuthorised();
    }
    $vehicle = new Vehicle;
    $inputs = $request->only('depth', 'description', 'height', 'reg', 'weight', 'width');
    foreach ($inputs as $input => $value) {
      $vehicle->$input = $value;
    }
    $vehicle->save();
    return view('admin.vehicles.show', ['vehicle' => $vehicle]);
  }
  /**
   * @author Giomani Designs
   * @param  \Illuminate\Http\Request  $request
   * @param  integer  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id) {
    if (!Entrust::can('vehicle_admin'))  {
      return parent::notAuthorised();
    }
    try {
      $vehicle = Vehicle::findOrFail($id);
    } catch(ModelNotFoundException $e) {
      return parent::notFound();
    }
    $inputs = $request->only('depth', 'description', 'height', 'reg', 'weight', 'width');
    foreach ($inputs as $input => $value) {
      $vehicle->$input = $value;
    }
    $vehicle->save();
    return redirect()->route('admin::vehicles::show', ['is' => $id]);
  }
}
