<?php

namespace App\Http\Controllers\Admin\Vehicle;

use Event;
use Validator;
use App\Http\Requests;
use App\VehicleCostType;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Events\VehicleCostTypeAdding;
use App\Http\Requests\Admin\VehicleCostTypePostRequest;

class VehicleCostTypeController extends Controller
{
  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id) {
      //
  }
  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit($id) {
    //
  }
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index() {
    $vehicleCostTypes = VehicleCostType::all();
    return view('admin.vehicle-cost-type.index', ['vehicleCostTypes' => $vehicleCostTypes]);
  }
  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request) {
    $validator = Validator::make($request->all(), VehicleCostType::$rules);

    if ($validator->fails()) {
      return response($validator->errors(), 422);
    }

    $costDescription = $request->input('description');
    $costType = $request->input('type');

    $vehicleCostType = new VehicleCostType;

    $vehicleCostType['description'] = $costDescription;
    $vehicleCostType['type'] = $costType;

    $vehicleCostType->save();

    Event::fire(new VehicleCostTypeAdding($costDescription, $vehicleCostType->id, $costType));

    return [
      'description' => $costDescription,
      'type' => $costType,
    ];
  }
  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id) {
      //
  }
}
