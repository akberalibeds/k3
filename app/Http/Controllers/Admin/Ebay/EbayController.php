<?php

namespace App\Http\Controllers\Admin\Ebay;

use Entrust;
use App\Info;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\EbayAuth;
use Time;
use App\EbayStock;
use App\Helper\Ebay\Ebay;
use Psy\Util\Json;

class EbayController extends Controller
{
  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    return view('bulletins.create');
  }
  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    //
  }
  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit()
  {
    return view('admin.ebay.edit', ['ebay' => EbayAuth::find(1)->toArray() ]);
  }
  /**
   * Display a listing of the resource.
   *
   * @author Giomani Designs
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    return view('admin.ebay.index', ['ebay' => EbayAuth::find(1)->toArray() ]);
  }

  /**
   * Update the specified resource in storage.
   *
   * @author Giomani Designs
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request)
  {
	
	$data = $request->except('_token');
	$data['last_edit'] = Time::date()->get('Y-m-d H:i:s');
	
	//return $data;
	
	$ebay  =  EbayAuth::find(1);
	$ebay->update($data);
	
	return view('admin.ebay.edit', ['ebay' => $ebay->toArray() , 'save' => 1]);
	
  }
  
  /**
   * Update the specified resource in storage.
   *
   * @author Giomani Designs
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function listings(Request $request)
  {
  	if($request->ajax()){
  	
  		$ebay = new Ebay();
  		return $ebay->allPages()->get_Listings()->response;
  	
  	}
  	
  	
  	$stock  = EbayStock::all();
  	return view('admin.ebay.listings',['stock' => $stock]);
  }
  
 
  /**
   * Update the specified resource in storage.
   *
   * @author Giomani Designs
   * @param  int  $data
   * @return \Illuminate\Http\Response
   */
  public function addEbayStock($data){
  	
  
  	$data = json_decode($data);
  	
  	
  	self::removeEbayStock($data->id);
  	foreach($data->variations as $v)
  		EbayStock::add(['itemCode' => $v, 'itemQty' => 3, 'itemID' => $data->id , 'updateStock' => 1, 'ebay' => 'ebay']);
  		
  	$ebay = new Ebay();
  	$ebay->stockControl($data->id);
  	$button = '';
  	return EbayStock::all(
  					\DB::raw("itemCode as '0'"),
  					\DB::raw("itemID as '1'"),
  					\DB::raw("itemQty as '2'"),
  					\DB::raw("if(updateStock=1,'yes','no') as '3'"),
  					\DB::raw("concat('<button class=\"btn btn-sm\" onclick=\"edit(',id,')\"><i class=\"fa fa-pencil-square-o\"></i></button>') as '4'")
  			)
  			->toArray();
  	
  }
  
  /**
   * Update the specified resource in storage.
   *
   * @author Giomani Designs
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function removeEbayStock($id){
  
  	EbayStock::where('itemID',$id)->delete();
  	return 1;
  	
  }
  
  /**
   * Update the specified resource in storage.
   *
   * @author Giomani Designs
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function updateEbayStock($id,$qty){
  		
  	$s = EbayStock::find($id);
  	$s->itemQty = $qty;
  	$s->save();
  	
  }
  
  
}
