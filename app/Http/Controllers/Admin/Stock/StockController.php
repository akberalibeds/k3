<?php
// Giomani Designs (Development Team)
//
namespace App\Http\Controllers\Admin\Stock;

use Excel;
use Event;
use Entrust;
use Validator;
use App\StockItem;
use App\OrderItems;
use App\StockCategory;
use Helper\CsvPeocessor;
use App\StockStockCategory;
use Illuminate\Http\Request;
use App\Helpers\CsvProcessor;
use App\Events\StockCountAdding;
use App\Helper\Traits\StockUtils;
use App\Helper\Traits\OutHeaders;
use App\Events\StockCountAdjustment;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use App\Http\Requests\StockItemPostRequest;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\StockParts;
use App\WarehouseStock;

class StockController extends Controller
{
  use OutHeaders, StockUtils;
  /**
   * @author Giomani Designs
   * @param  \App\Http\Requests\Request  $request
   * @param  int                         $id
   * @return \Illuminate\Http\Response
   */
  public function addStock (Request $request, $id) {
    if (!Entrust::can('stock_admin'))  {
      return parent::notAuthorised();
    }
    $validator = Validator::make($request->all(), StockItem::$rules['count']);
    if ($validator->fails()) {
      return response($validator->errors(), 422);
    }
    $count = $request->input('count');
    try {
      $stockItem = StockItem::findOrFail($id);
    } catch(ModelNotFoundException $e) {
      return parent::notFound();
    }
    $was = $stockItem->actual;
    $stockItem->actual += $count;
    $stockItem->save();
    Event::fire(new StockCountAdding($stockItem->id, $stockItem->actual, $stockItem->itemCode, $was));
    return ['actual' => $stockItem->actual, 'qtyOnSO' => $stockItem->qty_on_so];
  }
  
  /**
   * @author Giomani Designs
   * @param  \App\Http\Requests\Request  $request
   * @param  int                         $id
   * @return \Illuminate\Http\Response
   */
  public function adjustStock(Request $request, $id)
  {
    if (!Entrust::can('stock_admin')) {
      return parent::notAuthorised();
    }
    $validator = Validator::make($request->all(), StockItem::$rules['count']);
    if ($validator->fails()) {
      return response($validator->errors(), 422);
    } else {
      $count = $request->input('count');
      try {
        $stockItem = StockItem::findOrFail($id);
        $stockItemValue = $stockItem->adjustStocks($count);
        return ['actual' => $stockItem->actual, 'qtyOnSO' => $stockItem->qty_on_so];
      } catch (ModelNotFoundException $e) {
        return parent::notFound();
      }
    }
  }
  /**
   * @author Giomani Designs
   * @return \Illuminate\Http\Response
   */
  public function block ($id) {
    return $this->setBlock($id, true);
  }
  /**
   * @author Giomani Designs
   * @return \Illuminate\Http\Response
   */
  public function create () {
    if (!Entrust::can('stock_admin'))  {
      return parent::notAuthorised();
    }
    $categoryOptions = $this->getCategoryOptions();
    $stockCategoryOptions = $this->getStockCategoryOptions();
    return view('admin.stock.create', ['categoryOptions' => $categoryOptions, 'stockCategoryOptions' => $stockCategoryOptions]);
  }

  /**
   * ChangeActiveSKU
   *  Change the selected Item SKU to active/reactive
   * @param Request Collection of an Array of Stock Item Codes
   */
  public function changeActiveSKU(Request $request){
    $icList = null;
    foreach($request->all() as $codes){
      $icList = $codes;
    }
    StockItem::sku2Active($icList);
    return route('admin::stock::sku-active');
  }
  #
  /**
   * ChangeInactiveSKU
   *  Change the selected Item SKU to not active
   * @param Request Collection of an Array of Stock Item Codes
   */
  public function changeInactiveSKU(Request $request){
    $icList = null;
    foreach($request->all() as $codes){
      $icList = $codes;
    }
    StockItem::sku2Inactive($icList);
    return route('admin::stock::sku-inactive');
  }

  /**
   * @author Giomani Designs
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit ($id) {
    if (!Entrust::can('stock_admin'))  {
      return parent::notAuthorised();
    }
    try {
      $stockItem = StockItem::findOrFail($id);
    } catch(ModelNotFoundException $e) {
      return parent::notFound();
    }
    $categoryOptions = $this->getCategoryOptions();
    $stockCategoryOptions = $this->getStockCategoryOptions();
    return view('admin.stock.edit', ['categoryOptions' => $categoryOptions, 'stockCategoryOptions' => $stockCategoryOptions, 'stockItem' => $stockItem]);
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  array                     $ids
   * @param  boolean                   $withParts
   * @return \Illuminate\Http\Response
   */
  private function export($ids, $withParts) {
    if (!Entrust::can('stock_admin'))  {
      return parent::notAuthorised();
    }
    $title = 'Stock';
    return Excel::create($title, function($excel) use($ids, $title, $withParts) {
      $stock = StockItem::whereIn('id', $ids)->get();
      $excel->sheet($title, function($sheet) use($stock, $withParts) {
        $rows = $withParts ? [['Item Code', 'Description', 'Warehouse']] : [['Item Code', 'Description', 'Warehouse', 'Cost']];
        foreach($stock as $item) {
          if ($withParts) {
            for ($i = 0, $j = $item['pieces']; $i < $j; $i++) {
              $rows[] = [$item['itemCode'] . ' Part ' . ($i + 1) . ' of ' . $item['pieces'], $item['itemDescription'], 'w' . $item['warehouse'] . ''];
            }
          } else {
            $rows[] = [$item['itemCode'], $item['itemDescription'], 'w' . $item['warehouse'], $item['cost']];
          }
        }
        $sheet->fromArray($rows, null, 'A1', false, false);
      });
    });
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  \Illuminate\Http\Request  $request
   * @param  string                    $ids
   * @return \Illuminate\Http\Response
   */
  public function exportAsCsv (Request $request) {
    if (!Entrust::can('stock_admin')) {
      return parent::notAuthorised();
    }
    $ids = explode('|', $request->input('ids'));
    $withParts = ($request->input('with-parts') === 1);
    return $this->export($ids, $withParts)->download('csv');
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  \Illuminate\Http\Request  $request
   * @param  string                    $ids
   * @return \Illuminate\Http\Response
   */
  public function exportAsExcel(Request $request) {
    if (!Entrust::can('stock_admin'))  {
      return parent::notAuthorised();
    }
    $ids = explode('|', $request->input('ids'));
    $withParts = ($request->input('with-parts') === 1);
    return $this->export($ids, $withParts)->download('xlsx');
  }
  /**
   * @author Giomani Designs (Development Team)
   * @return Illuminate\Database\Eloquent\Collection
   */
  protected function getStockCategoryOptions() {
    $categoryOptions = ['0' => 'select a category'];
    $categories = StockStockCategory::orderBy('name', 'asc')->get();
    foreach ($categories as $category) {
      $categoryOptions[$category->id] = $category->name;
    }
    return $categoryOptions;
  }
  /**
   * @author Giomani Designs
   * @param  \App\Http\Requests\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function import(Request $request) {
    if (!Entrust::can('stock_admin'))  {
      return parent::notAuthorised();
    }
    $data = $request->get('data');
    $mode = $request->get('mode');
    $csvProcessor = new CsvProcessor('stock-import');
    $csvProcessor->setColumnSpecs($columnIndexes);
    $csvProcessor->processCsv($data);
    $accepts = $csvProcessor->getAccepts();
    $rejects = $csvProcessor->getRejects();
    $reply['data'] = array (
        'accepts' => $accepts,
        'rejects' => $rejects,
    );
    if (strcmp($mode, 'upload') == 0 && count($rejects) == 0) {
      foreach ($accepts as $model) {
        StockItem::create($model);
      }
    }
    return response()->json($reply);
  }
  /**
   * @author Giomani Designs
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function index (Request $request) {
    if (!Entrust::can('stock_admin'))  {
      return parent::notAuthorised();
    }
    
    if ($request->ajax()) {
      $query = $request->all();
      $stockItems = StockItem::getStock($query);
      if(! $request->get('checkbox-hidden-sku-codes') == false){
        $stockItems = $stockItems->where('stock_items.isActive', '=', 1);
      }
      $stockItems = $stockItems->orderBy('itemCode')->paginate(config('search.rpp'));
      return $stockItems;
    }
    return view('admin.stock.index', ['categories' => $this->getCategoryOptions(), 'stockCategories' => StockStockCategory::orderBy('name', 'asc')]);
  }
  /**
   * @author Giomani Designs
   * @param  $id
   * @return \Illuminate\Http\Response
   */
  public function needsAttention ($id) {
    return $this->setNeedsAttention($id, true);
  }
  /**
   * @author Giomani Designs
   * @param  integer    $id
   * @param  boolean    $state
   * @return \Illuminate\Http\Response
   */
  private function setBlock ($id, $state) {
    if (!Entrust::can('stock_admin'))  {
      return parent::notAuthorised();
    }
    try {
      $stockItem = StockItem::findOrFail($id);
    } catch(ModelNotFoundException $e) {
      return parent::notFound();
    }
    $stockItem->blocked = $state ? 1 : 0;
    $stockItem->save();
    return view('admin.stock.show', ['stockItem' => $stockItem]);
  }
  /**
   * @author Giomani Designs
   * @param  $id
   * @return \Illuminate\Http\Response
   */
  private function setNeedsAttention ($id, $state) {
    if (!Entrust::can('stock_admin'))  {
      return parent::notAuthorised();
    }
    try {
      $stockItem = StockItem::findOrFail($id);
    } catch(ModelNotFoundException $e) {
      return parent::notFound();
    }
    $stockItem->needs_attention = $state ? 1 : 0;
    $stockItem->save();
    return view('admin.stock.show', ['stockItem' => $stockItem]);
  }
  /**
   * @author Giomani Designs
   * @param  integer    $id
   * @param  boolean    $state
   * @return \Illuminate\Http\Response
   */
  private function setWithdraw ($id, $state) {
    if (!Entrust::can('stock_admin'))  {
      return parent::notAuthorised();
    }
    try {
      $stockItem = StockItem::findOrFail($id);
    } catch(ModelNotFoundException $e) {
      return parent::notFound();
    }
    $stockItem->withdrawn = $state ? 1 : 0;
    $stockItem->save();
    return view('admin.stock.show', ['stockItem' => $stockItem]);
  }
  /**
   * @author Giomani Designs
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show ($id) {
    if (!Entrust::can('stock_admin'))  {
      return parent::notAuthorised();
    }
    try {
      $stockItem = StockItem::findOrFail($id);
    } catch(ModelNotFoundException $e) {
      return parent::notFound();
    }
    return view('admin.stock.show', ['stockItem' => $stockItem]);
  }
  /**
   * @author Giomani Designs
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store (Request $request) {
    if (!Entrust::can('stock_admin'))  {
      return parent::notAuthorised();
    }
    $inputs = $request->only(['actual', 'always_in_stock', 'blocked', 'category_id', 'cost', 'itemCode', 'isMulti', 'itemDescription', 'label', 'model_no', 'pieces', 'retail', 'seller', 'warehouse', 'weight', 'wholesale', 'withdrawn']);
    if (($response = parent::validateForm($inputs, StockItem::$rules['create'], $request, 'admin::stock::create')) !== true) {
      return $response;
    }
    $stockItem = new StockItem;
    foreach ($inputs as $input => $value) {
      $stockItem->$input = $value;
    }
    
    $stockItem->save();
    echo "Created this new area";
    $parts=[];
    for($i=0;$i<$stockItem->pieces;$i++){
    	$sp = new StockParts;
    	$sp->parent_id = $stockItem->id;
    	$sp->part = "Box ".($i+1);
    	$sp->quantity = 0;
      $sp->save();
    	$parts[]=$sp->id;
    }
      
    $warehouses = [1,2,8,9,10,11,12,13];
    
    foreach($warehouses as $w){ 
    	foreach($parts as $p){
    		$ws = new WarehouseStock();
    		$ws->warehouse_id = $w;
    		$ws->stock_parts_id = $p;
    		$ws->quantity = 0;
    		$ws->save();
    	} 
    }
     
    return view('admin.stock.show', ['stockItem' => $stockItem]);
  }
  /**
   * @author Giomani Designs
   * @return \Illuminate\Http\Response
   */
  public function unblock ($id) {
    return $this->setBlock($id, false);
  }
  /**
   * @author Giomani Designs
   * @param  $id
   * @return \Illuminate\Http\Response
   */
  public function unNeedsAttention ($id) {
    return $this->setNeedsAttention($id, false);
  }
  /**
   * @author Giomani Designs
   * @param  $id
   * @return \Illuminate\Http\Response
   */
  public function unwithdraw ($id) {
    return $this->setWithdraw($id, false);
  }
  /**
   * @author Giomani Designs
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update (Request $request, $id) {
    if (!Entrust::can('stock_admin'))  {
      return parent::notAuthorised();
    }
    try {
      $stockItem = StockItem::findOrFail($id);
    } catch(ModelNotFoundException $e) {
      return parent::notFound();
    }
    $inputs = ['actual', 'always_in_stock', 'blocked', 'category_id', 'cost', 'isMulti', 'itemDescription', 'label', 'model_no', 'needs_attention', 'pieces', 'retail', 'seller', 'warehouse', 'weight', 'wholesale', 'withdrawn'];
    if (!isset($stockItem->itemCode)) {
      $inputs[] = 'itemCode';
    }
    if (!isset($stockItem->model_no)) {
      $inputs[] = 'model_no';
    }
	$inputs = $request->only($inputs);
    $inputs['cat']=StockCategory::categoryById($inputs['category_id']);
	
	if (($response = parent::validateForm($inputs, StockItem::$rules['update'], $request, 'admin::stock::edit', ['id' => $id])) !== true) {
      return $response;
    }
    foreach ($inputs as $input => $value) {
      $stockItem->$input = $value;
    }
    $stockItem->save();
    return redirect()->route('admin::stock::show', ['id' => $id]);
  }
  /**
   * @author Giomani Designs
   * @param  $id
   * @return \Illuminate\Http\Response
   */
  public function withdraw ($id) {
    return $this->setWithdraw($id, true);
  }
}