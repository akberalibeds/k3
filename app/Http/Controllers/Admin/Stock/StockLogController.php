<?php

namespace App\Http\Controllers\Admin\Stock;

use Entrust;
use App\ActionLog;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class StockLogController extends Controller
{
  /**
   * @author Giomani Designs (Development Team)
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function index(Request $request) {
    if (!Entrust::can('stock_admin'))  {
      return parent::notAuthorised();
    }
    if ($request->ajax()) {
      $query = $request->all();
      $logEntries = ActionLog::getForAction(ActionLog::STOCK_ACTIONS, $query);
      foreach ($logEntries as $logEntry) {
        $logEntry->status = ActionLog::checkStatus($logEntry);
      }
      return $logEntries;
    }
    $stockActions = ActionLog::actions();

    return view('admin.stock-log.index', ['stockActions' => $stockActions]);
  }
}
