<?php
// Giomani Designs (Development Team)
//
namespace App\Http\Controllers\Admin\Stock;

use Form;
use Excel;
use Entrust;
use App\Helper\Files;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Helper\Import\StockImport;
use App\Http\Controllers\CsvImportControllerAbstract;

class StockUploadController extends CsvImportControllerAbstract
{
  /**
   * @return \Illuminate\Http\Response
   */
  public function __construct () {
    if (!Entrust::can('stock_admin'))  {
      return parent::notAuthorised();
    }
    $this->fetchImportProfile('manifest-import');
    $this->setCsvProcessor(new StockImport());
  }
  /**
   * @return \Illuminate\Http\Response
   */
  public function headings () {
    if (!Entrust::can('stock_admin'))  {
      return parent::notAuthorised();
    }
    return $this->getImportProfileHeadings();
  }
  /**
   * @return \Illuminate\Http\Response
   */
  public function index () {
    if (!Entrust::can('stock_admin'))  {
      return parent::notAuthorised();
    }
    return view('admin.stock-upload.index');
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function read(Request $request) {
    if (!Entrust::can('stock_admin')) {
      return parent::notAuthorised();
    }
    return $this->readFile($request);
  }
  /**
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store (Request $request) {
    if (!Entrust::can('stock_admin')) {
      return parent::notAuthorised();
    }
    $data = $request->input('data');
    $mode = $request->input('mode');
    return $this->processData($data, $mode);
  }
}
