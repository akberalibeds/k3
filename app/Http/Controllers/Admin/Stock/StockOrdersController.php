<?php

namespace App\Http\Controllers\Admin\Stock;

use Entrust;
use App\StockItem;
use App\OrderItems;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class StockOrdersController extends Controller
{
  /**
   * @author Giomani Designs (Development Team)
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id the id of the stock-item
   * @return \Illuminate\Http\Response
   */
  public function index (Request $request, $id) {
    if (!Entrust::can('stock_admin'))  {
      return parent::notAuthorised();
    }
    try {
      $stockItem = StockItem::findOrFail($id);
    } catch (ModelNotFoundException $e) {
      return parent::notFound();
    }
    if ($request->ajax()) {
      $query = $request->all();
      return OrderItems::getOrdersFor($id, $query);
    }
    return view('admin.stock-orders.index', ['stockItem' => $stockItem]);
  }
}
