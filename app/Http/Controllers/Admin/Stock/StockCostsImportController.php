<?php
// Giomani Designs (Development Team)
//
namespace App\Http\Controllers\Admin\Stock;

use Entrust;
use Illuminate\Http\Request;
use App\Helper\Import\StockCostsImport;
use App\Http\Controllers\CsvImportControllerAbstract;

class StockCostsImportController extends CsvImportControllerAbstract
{
  /**
   * @return \Illuminate\Http\Response
   */
  public function __construct () {
    if (!Entrust::can('stock_admin'))  {
      return parent::notAuthorised();
    }
    $this->fetchImportProfile('stock-costs-import');
    $this->setCsvProcessor(new StockCostsImport());
  }
  /**
   * @return \Illuminate\Http\Response
   */
  public function headings () {
    if (!Entrust::can('stock_admin'))  {
      return parent::notAuthorised();
    }
    return $this->getImportProfileHeadings();
  }
  /**
   * @return \Illuminate\Http\Response
   */
  public function index () {
    if (!Entrust::can('stock_admin'))  {
      return parent::notAuthorised();
    }
    return view('admin.stock-costs-import.index');
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function read(Request $request) {
    if (!Entrust::can('stock_admin')) {
      return parent::notAuthorised();
    }
    return $this->readFile($request);
  }
  /**
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store (Request $request) {
    if (!Entrust::can('stock_admin')) {
      return parent::notAuthorised();
    }
    $data = $request->input('data');
    $mode = $request->input('mode');
    return $this->processData($data, $mode);
  }
}
