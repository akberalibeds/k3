<?php
// Giomani Designs (Development Team)
//
namespace App\Http\Controllers\Admin\Stock;

use Entrust;
use App\StockItem;
use App\StockStockCategory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class StockStockCategoryController extends Controller
{
  /**
   * Show the form for creating a new resource.
   *
   * @author Giomani Designs
   * @return \Illuminate\Http\Response
   */
  public function create() {
    return view('admin.stock-stock-categories.create');
  }
  /**
   * Show the form for editing the specified resource.
   *
   * @author Giomani Designs
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit($id) {
    try {
      $stockCategory = StockStockCategory::findOrFail($id);
    } catch(ModelNotFoundException $e) {
      return parent::notFound();
    }

    return view('admin.stock-stock-categories.edit', ['stockCategory' => $stockCategory]);
  }
  /**
   * Display a listing of the resource.
   *
   * @author Giomani Designs
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function index(Request $request) {
    $categories = StockStockCategory::orderBY('name')->paginate(config('search.rpp'));
    if ($request->ajax()) {
        return $categories;
    }
    return view('admin.stock-stock-categories.index', ['categories' => $categories]);
  }
  /**
   * Search for resources from storage.
   *
   * @author Giomani Designs
   * @param  string  $query
   * @return \Illuminate\Http\Response
   */
  public function searchName($query) {
    $categories = StockStockCategory::where('name', 'like', '%' . $query . '%')->get();
    return response()->json($categories);
  }
  /**
   * Display the specified resource.
   *
   * @author Giomani Designs
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id) {
    try {
      $category = StockStockCategory::findOrFail($id);
    } catch(ModelNotFoundException $e) {
      return parent::notFound();
    }


    return view('admin.stock-stock-categories.show', ['category' => $category]);
  }
  /**
   * Display a listing of the resource.
   *
   * @author Giomani Designs
   * @param  \Illuminate\Http\Request  $request
   * @param  int $id
   * @return array
   */
  public function showItems(Request  $request, $id) {
    return StockItem::getForCategory($id);
  }
  /**
   * Store a newly created resource in storage.
   *
   * @author Giomani Designs
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request) {
    $category = $request->except('_method', '_token');

    $stockCategory = new StockStockCategory();
    $stockCategory->description = $category['description'];
    $stockCategory->name = $category['name'];
    StockStockCategory::insert($category);

    return view('admin.stock-stock-categories.show', ['category' => $category]);
  }
  /**
   * Store a newly created resource in storage.
   *
   * @author Giomani Designs
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id) {
    try {
      $category = StockStockCategory::findOrFail($id);
    } catch(ModelNotFoundException $e) {
      return parent::notFound();
    }

    $category->name = $request->name;

    StockStockCategory::save($category);

    return view('admin.stock-stock-categories.show', ['category' => $category]);
  }
}
