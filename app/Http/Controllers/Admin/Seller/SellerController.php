<?php

namespace App\Http\Controllers\Admin\Seller;

use Entrust;
use App\Order;
use App\Seller;
use App\SellerItem;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\SellerPutRequest;
use App\Http\Requests\SellerPostRequest;


class SellerController extends Controller
{
	
	private $key = '$2y$10$kkR17UlxvHvxQNyVIQOuX.gLQBDRfUPDN0bioXJArW.yqaTQq.jpa';
	private $seller_domain = "https://my.giomani-designs.co.uk";
  /**
   * @return \Illuminate\Http\Response
   */
  public function create () {
    if (!Entrust::can('seller_admin')) {
      return parent::notAuthorised();
    }
    return view('admin.sellers.create');
  }
  /**
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit ($id) {
    if (!Entrust::can('seller_admin')) {
      return parent::notAuthorised();
    }
    $seller = Seller::find($id);
    return view('admin.sellers.edit', ['seller' => $seller]);
  }
  /**
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function index (Request $request) {
    if (!Entrust::can('seller_admin')) {
      return parent::notAuthorised();
    }
    if ($request->ajax()) {
      return Seller::where('active_seller',1)->orderBy('name', 'asc')->paginate(config('search.rpp'));
    }
    return view('admin.sellers.index');
  }
  /**
  * @param  \Illuminate\Http\Request  $request
   * @param  integer  $id
   * @return \Illuminate\Http\Response
   */
  public function show (Request $request, $id) {
    if (!Entrust::can('seller_admin')) {
      return parent::notAuthorised();
    }
    try {
      $seller = Seller::findOrFail($id);
    } catch (ModelNotFoundException $e) {
      return parent::notFound();
    }
    return view('admin.sellers.show', ['seller' => $seller]);
  }
  /**
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store (Request $request) {
	  
	try{ 
	 $this->validate($request, [
            'password' => 'required|min:2|confirmed',
     ]);
	}
	catch(\Illuminate\Foundation\Validation\ValidationException $e){
		return redirect('/admin/sellers/create');
	}
	
	if (!Entrust::can('seller_admin')) {
      return parent::notAuthorised();
    }
	
	$inputs = $request->only('account_limit', 'discount', 'email', 'iName', 'name', 'num', 'pc', 'street', 'tel', 'town', 'force_pay','password');
	$url = "$this->seller_domain/api/v1/hasher/$this->key/$inputs[password]";
	$response = json_decode(file_get_contents($url));
	$seller = new Seller;
    foreach ($inputs as $key => $value) {
      $seller->$key = $value;
    }
	$seller->password = $response->code;
    $seller->save();
    return redirect()->route('admin::sellers::show', ['id' => $seller->id]);
  }
  /**
   * @param  \App\Http\Requests\SellerPostRequest  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update (Request $request, $id) {
    if (!Entrust::can('seller_admin')) {
      return parent::notAuthorised();
    }
	
	try{ 
	 $this->validate($request, [
            'password' => 'min:2|confirmed',
     ]);
	}
	catch(\Illuminate\Foundation\Validation\ValidationException $e){
		return redirect('/admin/sellers/'.$id.'/edit');
	}
	
    $seller = Seller::find($id);
    $inputs = $request->only('account_limit', 'discount', 'email', 'iName', 'name', 'num', 'pc', 'street', 'tel', 'town', 'force_pay','custom_price','password');
    if($inputs['password']==""){
		unset($inputs['password']);	
	}
	else{
	$url = "$this->seller_domain/api/v1/hasher/$this->key/$inputs[password]";
	$response = json_decode(file_get_contents($url));
	$inputs['password'] = $response->code;	
	}
	foreach ($inputs as $key => $value) {
      	$seller->$key = $value;
    }
	
    $seller->save();
    return redirect()->route('admin::sellers::show', ['id' => $seller->id]);
  }
}
