<?php

namespace App\Http\Controllers\Admin\Seller;

use Entrust;
use App\Order;
use App\Seller;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class SellerOrdersController extends Controller
{
  /**
   * Display the specified resource.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function index(Request $request) {
    if (!Entrust::can('seller_view')) {
      return parent::notAuthorised();
    }
    $sellers = Seller::orderBy('name', 'asc')->paginate(config('search.rpp'));

    if ($request->ajax()) {
      return $sellers;
    }

    return view('admin.seller-orders.index', ['sellers' => $sellers]);
  }
  /**
   * Display the specified resource.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show(Request $request, $id)
  {
    try {
      $seller = Seller::findOrFail($id);
    } catch (ModelNotFoundException $e) {
      return parent::notFound();
    }

    if ($request->ajax()) {
      return Order::getWholesale($id);
    }

    return view('admin.seller-orders.show', ['seller' => $seller]);
  }
  /**
   * Store a newly created resource in storage.
   *
   * @param  \App\Http\Requests\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request) {
    if (!Entrust::can('supplier_admin')) {
      return parent::notAuthorised();
    }
    $inputs = $request->only(['supplier_name', 'days']);

    foreach ($inputs as $input => $value) {
      $supplier->$input = $value;
    }
    $supplier->save();
  }
}
