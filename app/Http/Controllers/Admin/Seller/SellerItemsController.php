<?php

namespace App\Http\Controllers\Admin\Seller;

use Entrust;
use Validator;
use App\Order;
use App\Seller;
use App\Wholesale;
use App\StockItem;
use App\SellerItem;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SellerItemsController extends Controller
{
  /**
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function create() {
    if (!Entrust::can('seller_admin')) {
      return parent::notAuthorised();
    }
    $sellers = Seller::getForSelect();
    return view('admin.seller-items.create', ['sellers' => $sellers]);
  }
  /**
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function index(Request $request) {
    if (!Entrust::can('seller_admin')) {
      return parent::notAuthorised();
    }
    if ($request->ajax()) {
      return SellerItem::getAll($request->all());
    }
    $sellers = Seller::getForSelect();
    return view('admin.seller-items.index', ['sellers' => $sellers]);
  }
  /**
   * @param  integer  $id seller item id
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function show(Request $request, $id) {
    try {
      $sellerItem = SellerItem::findOrFail($id);
    } catch (ModelNotFoundException $e) {
      return parent::notFound();
    }
    if ($request->ajax()) {
      return $sellerItem;
    }
    return view('admin.seller-items.show', ['sellerItem' => $sellerItem]);
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $sellerId
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request) {
    $stockItemId = $request->input('id_stock');
    try {
      $stockItem = StockItem::findOrFail($stockItemId);
      $availableToSeller = $request->input('available_to_seller');
      if ($availableToSeller) {
        $stockItem->seller = 1;
        $stockItem->save();
      }
    } catch (ModelNotFoundException $e) {
      return parent::notFound();
    }
    $inputs = $request->only('cost', 'id_seller', 'id_stock');
    $validator = Validator::make($inputs, SellerItem::$rules['store']);
    if ($validator->fails()) {
      return response($validator->errors(), 422);
    }
    $sellerItem = new SellerItem;
    foreach ($inputs as $key => $value) {
      $sellerItem->$key = $value;
    }
    $sellerItem->save();
    return $sellerItem->toArray();
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id) {
    try {
      $sellerItem = SellerItem::find($id);
    } catch (ModelNotFoundException $e) {
      return parent::notFound();
    }
    $sellerItem->cost = $request->cost;
    $sellerItem->save();
    return $sellerItem->toArray();
  }
}
