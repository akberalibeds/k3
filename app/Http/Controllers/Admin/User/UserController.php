<?php
namespace App\Http\Controllers\Admin\User;

use DB;
use Auth;
use Event;
use Entrust;
use App\Role;
use App\User;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Events\UserSuspending;
use App\Events\UserPasswordChange;
use App\Http\Controllers\Controller;
use App\Events\UserRolesPermissionsChange;
use App\Http\Requests\Admin\ChangePasswordRequest;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class UserController extends Controller
{
  /**
   * @return \Illuminate\Http\Response
   */
  public function create() {
    if (!Entrust::can('user_admin'))  {
      return parent::notAuthorised();
    }
    return view('admin.user.create');
  }
  /**
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit($id) {
    if (!Entrust::can('user_admin'))  {
      return parent::notAuthorised();
    }
    try {
      $user = User::findOrFail($id);
    } catch(ModelNotFoundException $e) {
      return parent::notFound('user', ['id' => $uid]);
    }
    $user = User::where('id', '=', $id)->first();
    $userRoles = $user->roles;
    return view('admin.user.edit', ['user' => $user, 'userRoles' => $userRoles]);
  }
  /**
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function index(Request $request) {
    if (!Entrust::can('user_admin'))  {
      return parent::notAuthorised();
    }
    if ($request->ajax()) {
      $query = $request->all();
      return User::getAll($query);
    }
    return view('admin.user.index');
  }
  /**
   * @param  int $id user-id
   * @return \Illuminate\Http\Response
   */
  public function roles($id) {
    if (!Entrust::can('user_admin'))  {
      return parent::notAuthorised();
    }
    try {
      $user = User::findOrFail($id);
    } catch(ModelNotFoundException $e) {
      return parent::notFound('user', ['id' => $uid]);
    }
    $userRoles = $user->roles;
    $roles = Role::forAssigning();
    return view('admin.user.roles', ['user' => $user, 'roles' => $roles->diff($userRoles), 'userRoles' => $userRoles]);
  }
  /**
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id) {
    if (!Entrust::can('user_admin'))  {
      return parent::notAuthorised();
    }
    try {
      $user = User::findOrFail($id);
    } catch(ModelNotFoundException $e) {
      return parent::notFound('user', ['id' => $uid]);
    }
    $userRoles = $user->roles;
    return view('admin.user.show', ['user' => $user, 'userIsActive' => (User::USER_STATUS_ACTIVE == $user->status), 'userRoles' => $userRoles]);
  }
  /**
   * @param  App\Http\Requests\UserPostRequest  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request) {
    if (!Entrust::can('user_admin'))  {
      return parent::notAuthorised();
    }
    $user = new User;
    $inputs = $request->only(['name', 'email', 'status']);
    foreach ($inputs as $input => $value) {
      $user->$input = $value;
    }
    $user->password = bcrypt($request->input('password'));
    $user->save();
    return redirect()->route('admin::users::show', ['id' => $user->id]);
  }
  /**
   * @param  \Illuminate\Http\Request  $request
   * @param  integer  $id the user id
   * @return \Illuminate\Http\Response
   */
  public function suspend(Request $request, $id) {
    if (!Entrust::can('user_admin'))  {
      return parent::notAuthorised();
    }
    try {
      $user = User::findOrFail($id);
    } catch(ModelNotFoundException $e) {
      return parent::notFound('user', ['id' => $id]);
    }
    $user->status = User::USER_STATUS_SUSPENDED;
    $user->save();
    Event::fire(new UserSuspending($id));
    if ($request->ajax()) {
      return ['status' => User::statusText(User::USER_STATUS_SUSPENDED)];
    }
    return redirect()->route('admin::users::show', ['id' => $user->id]);
  }
  /**
   * @param  App\Http\Requests\UserPutRequest  $request
   * @param  int  $uid the user id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id) {
    if (!Entrust::can('user_admin'))  {
      return parent::notAuthorised();
    }
    try {
      $user = User::findOrFail($id);
    } catch(ModelNotFoundException $e) {
      return parent::notFound('user', ['id' => $id]);
    }
    $inputs = $request->only(['name', 'status']);
    foreach ($inputs as $input => $value) {
      $user->$input = $value;
    }
    $user->password = bcrypt($request->input('password'));
    $user->save();
    return redirect()->route('admin::users::show', ['id' => $user->id]);
  }
  /**
   * @param  \App\Http\Requests\Admin\ChangePasswordRequest  $request
   * @param  int  $id the user id
   * @return \Illuminate\Http\Response
   */
  public function updatePassword(Request $request, $id) {
    if (!Entrust::can('user_admin'))  {
      return parent::notAuthorised();
    }
    try {
      $user = User::findOrFail($id);
    } catch(ModelNotFoundException $e) {
      return parent::notFound('user', ['id' => $id]);
    }
    $user->password = bcrypt($request->input('password'));
    $user->save();
    Event::fire(new UserPasswordChange($id));
    if ($request->ajax()) {
      return response(null, 200);
    }
    return redirect()->route('admin::users::show', ['id' => $user->id]);
  }
  /**
   * @param  \Illuminate\Http\Request  $request
   * @param  int $id user-id
   * @return
   */
  public function updateRoles(Request $request, $id) {
    if (!Entrust::can('user_admin'))  {
      return parent::notAuthorised();
    }
    try {
      $user = User::findOrFail($id);
    } catch(ModelNotFoundException $e) {
      return parent::notFound('user', ['id' => $uid]);
    }
    $add = $request->input('add');
    $remove = $request->input('remove');
    $roles = array_merge($add, $remove);
    $count = Role::whereIn('id', $roles)->count();
    if (count($roles) != $count) {
      return parent::notFound('roles', []);
    }
    DB::table('role_user')->where('user_id', '=', $id)->whereIn('role_id', $remove)->delete();
    $adds = [];
    foreach ($add as $a) {
      $adds[] = ['user_id' => $id, 'role_id' => $a];
    }
    DB::table('role_user')->insert($adds);
    Event::fire(new UserRolesPermissionsChange($add, $remove, $id));
  }
}
