<?php
// Giomani Designs (Development Team)
//
namespace App\Http\Controllers\Admin\Log;

use Auth;
use App\Actionlog;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class LogController extends Controller
{
  /**
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function index(Request $request) {
    if (!Auth::user()->hasRole('root')) {
      return parent::notAuthorised();
    }
    if ($request->ajax()) {
      $query = $request->all();
      return ActionLog::getAll($query);
    }
    $actionGroups = ActionLog::actionGroups();
    $actions = ActionLog::actions();
    $maxLevel = Actionlog::VERIFICATION_LEVELS;

    return view('admin.log.index', ['actions' => $actions, 'actionGroups' => $actionGroups, 'maxLevel' => $maxLevel]);
  }
  /**
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function show(Request $request, $id) {
    if (!Auth::user()->hasRole('root')) {
      return parent::notAuthorised();
    }
    try {
      $log = ActionLog::getOrFail($id);
    } catch (ModelNotFoundException $e) {
      return parent::notFound();
    }
    $hashState = ActionLog::checkStatus($log);

    return view('admin.log.show', ['log' => $log, 'hashState' => ($hashState ? 'good' : 'bad')]);
  }
  /**
   * @param  integer  $lvl
   * @return \Illuminate\Http\Response
   */
  public function verify($lvl = 0) {
    if (!Auth::user()->hasRole('root')) {
      return parent::notAuthorised();
    }
    return Actionlog::verify($lvl);
  }
}
