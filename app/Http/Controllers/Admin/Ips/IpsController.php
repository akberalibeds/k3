<?php

namespace App\Http\Controllers\Admin\Ips;

use Auth;
use Event;
use Entrust;
use App\AllowedIp;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Events\IpAddressAdding;
use App\Events\IpAddressDeleting;
use App\Http\Controllers\Controller;

class IpsController extends Controller
{
  /**
   * Show the form for creating a new resource.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function create(Request  $request) {
    if (!Entrust::hasRole('sys_admin')) {
      return parent::notAuthorised();
    }
    return view('admin.ips.create', ['currentIpa' => $request->ip()]);
  }
  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id) {
    if (!Entrust::hasRole('sys_admin')) {
      return parent::notAuthorised();
    }
    try {
      $ipa = AllowedIp::findOrFail($id);
    } catch(ModelNotFoundException $e) {
      return parent::notFound();
    }
    Event::fire(new IpAddressDeleting($id));
    $ipa->delete();
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function index(Request $request) {
    if (!Entrust::hasRole('sys_admin')) {
      return parent::notAuthorised();
    }
    if ($request->ajax()) {
      return AllowedIp::paginate(config('search.rpp'));
    }
    return view('admin.ips.index', ['currentIpa' => $request->ip()]);
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function show(Request $request, $id) {
    if (!Entrust::hasRole('sys_admin')) {
      return parent::notAuthorised();
    }
    try {
      $ip = AllowedIp::findOrFail($id);
    } catch (ModelNotFoundException $e) {
      return parent::notFound();
    }
    return $ip;
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request) {
    if (!Entrust::hasRole('sys_admin')) {
      return parent::notAuthorised();
    }
    $inputs = $request->only(['ip0', 'ip1', 'ip2', 'ip3']);
    $allowedIp = new AllowedIp;
    $allowedIp->address = implode('.', $inputs);
    $allowedIp->user_id = Auth::user()->id;
    $allowedIp->save();
    Event::fire(new IpAddressAdding($allowedIp->address));
    return redirect()->route('admin::ips::index');
  }
}
