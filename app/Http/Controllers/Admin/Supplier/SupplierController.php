<?php
namespace App\Http\Controllers\Admin\Supplier;

use Entrust;
use App\Order;
use App\Routes;
use App\EInvoice;
use App\Supplier;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use \App\Http\Requests\SupplierPutRequest;
use \App\Http\Requests\SupplierPostRequest;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class SupplierController extends Controller
{
  /**
   * @author Giomani Designs (Development Team)
   * @return \Illuminate\Http\Response
   */
  public function create() {
    return view('admin.suppliers.create');
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit($id) {
    try {
      $supplier = Supplier::findOrFail($id);
    } catch(ModelNotFoundException $e) {
      return parent::notFound();
    }
    return view('admin.suppliers.edit', ['supplier' => $supplier]);
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function index(Request $request) {
    if ($request->ajax()) {
      return Supplier::orderBy('supplier_name', 'asc')->paginate(config('search.rpp'));
    }
    return view('admin.suppliers.index');
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id) {
    try {
      $supplier = Supplier::findOrFail($id);
    } catch(ModelNotFoundException $e) {
      return parent::notFound();
    }
    return view('admin.suppliers.show', ['supplier' => $supplier]);
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request) {
     $inputs = $request->only(['supplier_name', 'print_orders_on_dispatch']);
    //if (($response = parent::validateForm($inputs, Supplier::$rules['create'], $request, 'admin::suppliers::create')) !== true) {
    //  return $response;
   // }
   $supplier = new Supplier();
    foreach ($inputs as $input => $value) {
      $supplier->$input = $value;
    }
    $supplier->save();
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  \App\Http\Requests\SupplierPutRequest  $request
   * @param  int                                    $id
   * @return \Illuminate\Http\Response
   */
  public function update(SupplierPutRequest $request, $id) {
    try {
      $supplier = Supplier::findOrFail($id);
    } catch (ModelNotFoundException $e) {
      return parent::notFound();
    }
    $inputs = $request->only(['id', 'supplier_name', 'days', 'print_orders_on_dispatch']);
    if (($result = parent::validate($inputs, Supplier::$rules['create'], $request, redirect()->route('admin::suppliers::edit', ['id' => $inputs['id']]))) !== true) {
      return $result;
    }
    foreach ($inputs as $input => $value) {
      $supplier->$input = $value;
    }
    $supplier->save();
    return view('admin.suppliers.show', ['supplier' => $supplier]);
  }
}
