<?php

namespace App\Http\Controllers\Admin\Supplier;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class InvoiceController extends Controller
{
  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
      //
  }
  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
      //
  }
  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit($id)
  {
      //
  }
  /**
   * Display a listing of the resource.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function index(Request $request, $id)
  {
    try {
      $supplier = Supplier::findOrFail($id);
    } catch(ModelNotFoundException $e) {
      return parent::notFound();
    }
    if ($request->ajax()) {
      return EInvoice::getForSupplier($id);
    }

    return view('suppliers.invoices', ['supplier' => $supplier, 'invoices' => EInvoice::getForSupplier($id)]);
  }
  /**
   * set an invoice as paid
   *
   * @param  int $id supplier id
   * @param  int $id invoice id
   * @return \Illuminate\Http\Response
   */
  public function paidInvoice($id, $iid)
  {
    $time = EInvoice::setAsPaid($id, $iid);
    Order::setInvoicePaid($id, $iid);

    return ['timePaid' => $time];
  }
  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
      //
  }
  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
      //
  }
  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
      //
  }
}
