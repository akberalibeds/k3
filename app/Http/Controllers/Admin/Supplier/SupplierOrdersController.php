<?php

namespace App\Http\Controllers\Admin\Supplier;

use Entrust;
use App\Order;
use App\Supplier;
use App\Http\Requests;
use App\Helper\PdfLabel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class SupplierOrdersController extends Controller
{
  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
      //
  }
  /**
   * Remove the specified resource from storage.
   *
   * @author Giomani Designs
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
      //
  }
  /**
   * Display the specified resource.
   *
   * @author Giomani Designs
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function exportCsv(Request $request, $id, $iid)
  {
    try {
      $supplier = Supplier::findOrFail($id);
    } catch (ModelNotFoundException $e) {
      return parent::notFound();
    }

    $orders = Order::getSupplierInvoicedCsv($iid)->toArray();
    $datetime = new \DateTime;
    $title = $datetime->format('Ymdhis-') . strstr($supplier->supplier_name, ' ', '_');

    header('Content-type: text/csv');
    header('Content-disposition: attachment; filename=' . $title . '.csv');
    // header("Content-transfer-encoding: binary\n");

    $out = fopen('php://output', 'w');

    foreach($orders as $line)
    {
      fputcsv($out, $line);
    }
    fclose($out);
  }
  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit($id)
  {
      //
  }
  /**
   * Display a listing of the resource.
   *
   * @author Giomani Designs
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function index(Request $request)
  {
    $suppliers = Supplier::orderBy('supplier_name', 'asc')->get();
	
    if ($request->ajax()) {
      return $suppliers;
    }
	
    return view('supplier-orders.index', ['suppliers' => $suppliers]);
  }
  /**
   * get the orders on an invoice
   *
   * @author Giomani Designs
   * @param  string  $subject
   * @param  string  $query
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function invoicedOrders(Request $request, $id, $iid) {
    if (!Entrust::can('supplier_view')) {
      return parent::notAuthorised();
    }
    try {
      $supplier = Supplier::findOrFail($id);
    } catch (ModelNotFoundException $e) {
      return parent::notFound();
    }
    if ($request->ajax()) {
      return Order::getSupplierInvoiced($iid);
    }
    return view('supplier-orders.invoiced', ['supplier' => $supplier, 'invoiceMade' => $iid]);
  }
  /**
   * get the refunds orders.
   *
   * @author Giomani Designs
   * @param  string  $subject
   * @param  string  $query
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function invoicedSearchOrders(Request $request, $subject, $query, $id) {
    if (!Entrust::can('supplier_view')) {
      return parent::notAuthorised();
    }
    try {
      $supplier = Supplier::findOrFail($id);
    } catch (ModelNotFoundException $e) {
      return parent::notFound();
    }
    if ($request->ajax()) {
      return Order::searchSupplierInvoiced($id, $subject, $query);
    }
    return view('share.not-ready-yet');
  }
  /**
   * set an invoice as paid
   *
   * @author Giomani Designs
   * @param  int $id supplier id
   * @param  int $id invoice id
   * @return \Illuminate\Http\Response
   */
  public function printLabels($id, $date = null)
  {
    if ($date === null) {
      $date = new \Datetime;
      $date->add(new \DateInterval('P2D'));
    }
    $orders = Order::getSupplierLabels($id, '25-Jun-2016'); // $date->format('Y-M-d'));
    $pdf = new PdfLabel();
    $pdf->barcodeLabels($orders, $date)->Output('D', 'title' . '-' . $date->format('YmdH') . '.pdf');
  }
  /**
   * Search for a supplier
   *
   * @author Giomani Designs
   * @param  int  $id supplier id
   * @param  string  $subject
   * @param  string  $query
   * @return \Illuminate\Http\Response
   */
  public function search($id, $subject, $query)
  {
    if (!Entrust::can('supplier_view')) {
      return parent::notAuthorised();
    }
    $orders = Order::searchSupplier($id, $subject, $query);

    return response()->json($orders);
  }
  /**
   * Display the specified resource.
   *
   * @author Giomani Designs
   * @param  int  $id
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function show(Request $request, $id)
  {
    if (!Entrust::can('supplier_view')) {
      return parent::notAuthorised();
    }
    try {
      $supplier = Supplier::findOrFail($id);
    } catch (ModelNotFoundException $e) {
      return parent::notFound();
    }
    if ($request->ajax()) {
      $orders = Order::getSupplier($id);
      return $orders;
    }
    $searchConfig = [
      'Order ID' => 'orders.id;oid',
      'Name' => 'dBusinessName;dBusinessName',
      'Email' => 'email1;email1',
    ];

    $suppliers = Supplier::where('id', '<>', $id)->orderby('supplier_name', 'asc')->get();

    return view('supplier-orders.show', ['supplier' => $supplier, 'suppliers' => $suppliers]);
  }
  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
      //
  }
  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
      //
  }
}
