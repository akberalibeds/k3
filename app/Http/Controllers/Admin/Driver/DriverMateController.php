<?php

namespace App\Http\Controllers\Admin\Driver;

use Entrust;
use App\DriverMate;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\FilesController;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Helper\Pdfs\PdfTable;

class DriverMateController extends Controller
{
  /**
   * Show the form for creating a new resource.
   *
   * @author Giomani Designs
   * @return \Illuminate\Http\Response
   */
  public function create() {
    return view('admin.driver-mates.create');
  }
  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function remove($id) {
  		$mate  = DriverMate::find($id);
  		$mate->delete();
  		return $id;
  }
  /**
   * Show the form for editing the specified resource.
   *
   * @author Giomani Designs
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit($id) {
    try {
      $driverMate = DriverMate::findOrFail($id);
    } catch(ModelNotFoundException $e) {
      return parent::notFound();
    }

    return view('admin.driver-mates.edit', ['driverMate' => $driverMate]);
  }

  /**
   * Display a listing of the resource.
   *
   * @author Giomani Designs
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function index(Request $request) {
    if ($request->ajax()) {
        return DriverMate::orderBy('name', 'asc')->paginate(config('search.rpp'));
    }

    return view('admin.driver-mates.index');
  }
  /**
   * Update the specified resource in storage.
   *
   * @author Giomani Designs
   * @param  string  $query
   * @return \Illuminate\Http\Response
   */
  public function searchName($query) {
    $drivers = DriverMate::where('name', 'like', '%' . $query . '%')->get();

    return response()->json($drivers);
  }
  /**
   * Display the specified resource.
   *
   * @author Giomani Designs
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id) {
    try {
      $driverMate = DriverMate::findOrFail($id);
    } catch(ModelNotFoundException $e) {
      return parent::notFound();
    }

    $driverMateFiles = [];//FilesController::getFileNames('driver-mate', $id);

    return view('admin.driver-mates.show', ['driverMate' => $driverMate, 'driverMateFiles' => $driverMateFiles]);
  }
  /**
   * Store a newly created resource in storage.
   *
   * @author Giomani Designs
   * @param  App\Http\Requests\DriverMatePutRequest  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request) {
    $driverMate = new DriverMate;

    $inputs = $request->only(['active', 'name', 'num', 'rate']);
    try{
    	$this->validate(new Request($inputs),[
    			'name' => 'unique:mates,name'
    	]);
    }
    catch(\Illuminate\Validation\ValidationException $e){
    	return redirect("/admin/driver-mates/create");
    }
    
   
    foreach ($inputs as $input => $value) {
      $driverMate->$input = $value;
    }
    $driverMate->save();

    return redirect()->route('admin::driver-mates::show', ['id' => $driverMate->id]);
  }
  /**
   * Update the specified resource in storage.
   *
   * @author Giomani Designs
   * @param  App\Http\Requests\DriverMatePostRequest  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id) {
    try {
      $driverMate = DriverMate::findOrFail($id);
    } catch(ModelNotFoundException $e) {
      return parent::notFound();
    }

    $inputs = $request->only(['active', 'name', 'num', 'rate' ]);
    if($inputs['name']==$driverMate->name){ unset($inputs['name']); }
    try{
    	$this->validate(new Request($inputs),[
    			'name' => 'unique:mates,name'
    	]);
    }
    catch(\Illuminate\Validation\ValidationException $e){
    	return redirect("/admin/driver-mates/$id/edit");
    }
    

    foreach ($inputs as $input => $value) {
      $driverMate->$input = $value;
    }
    $driverMate->save();

    return redirect()->route('admin::driver-mates::show', ['id' => $id]);
  }
  
  
  public function mateList() {
  	 
  	$drivers = DriverMate::getActive();//->toArray();
  	$rows = [];
  	foreach ($drivers as $count) {
  		$rows[] = [$count['name'], $count['num']];
  	}
  	 
  	$pdf  = new PdfTable(["Name","Number"], $rows, "Title");
  	 
  	$pdf->table();
  	return $pdf->out();
  }
}
