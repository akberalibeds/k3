<?php

namespace App\Http\Controllers\Admin\Driver;

use Entrust;
use App\DriverMate;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\FilesController;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Helper\Pdfs\PdfTable;
use App\Picker;
use Illuminate\Support\Facades\Crypt;

class PickerController extends Controller
{
  /**
   * Show the form for creating a new resource.
   *
   * @author Giomani Designs
   * @return \Illuminate\Http\Response
   */
  public function create() {
    return view('admin.pickers.create');
  }
  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function remove($id) {
  		$picker = Picker::find($id);
  		$picker->delete();
  		return $id;
  }
  /**
   * Show the form for editing the specified resource.
   *
   * @author Giomani Designs
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit($id) {
    try {
      $picker = Picker::findOrFail($id);
    } catch(ModelNotFoundException $e) {
      return parent::notFound();
    }

    return view('admin.pickers.edit', ['picker' => $picker]);
  }

  /**
   * Display a listing of the resource.
   *
   * @author Giomani Designs
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function index(Request $request) {
    if ($request->ajax()) {
        return Picker::orderBy('name', 'asc')->paginate(config('search.rpp'));
    }

    return view('admin.pickers.index');
  }
  /**
   * Update the specified resource in storage.
   *
   * @author Giomani Designs
   * @param  string  $query
   * @return \Illuminate\Http\Response
   */
  public function searchName($query) {
    $drivers = Picker::where('name', 'like', '%' . $query . '%')->get();

    return response()->json($drivers);
  }
  /**
   * Display the specified resource.
   *
   * @author Giomani Designs
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id) {
    try {
      $picker = Picker::findOrFail($id);
    } catch(ModelNotFoundException $e) {
      return parent::notFound();
    }
    try{
    	$picker->pwd = Crypt::decrypt($picker->pwd);
    }
    catch(\Illuminate\Contracts\Encryption\DecryptException $e){
    		
    }
    $driverMateFiles = [];//FilesController::getFileNames('driver-mate', $id);

    return view('admin.pickers.show', ['picker' => $picker, 'driverMateFiles' => $driverMateFiles]);
  }
  /**
   * Store a newly created resource in storage.
   *
   * @author Giomani Designs
   * @param  App\Http\Requests\DriverMatePutRequest  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request) {
    $picker = new Picker;

    $inputs = $request->only(['active', 'name', 'num', 'pwd','uName']);
    $inputs['pwd'] = Crypt::encrypt($inputs['pwd']);
    try{
    	$this->validate(new Request($inputs),[
    			'name' => 'unique:pickers,name',
    			'uName' => 'unique:pickers,uName'
    	]);
    }
    catch(\Illuminate\Validation\ValidationException $e){
    	return redirect("/admin/pickers/create");
    }
    
   
    foreach ($inputs as $input => $value) {
      $picker->$input = $value;
    }
    $picker->save();

    return redirect()->route('admin::pickers::show', ['id' => $picker->id]);
  }
  /**
   * Update the specified resource in storage.
   *
   * @author Giomani Designs
   * @param  App\Http\Requests\DriverMatePostRequest  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id) {
    try {
      $picker = Picker::findOrFail($id);
    } catch(ModelNotFoundException $e) {
      return parent::notFound();
    }

    $inputs = $request->only(['active', 'name', 'num', 'uName', 'pwd']);
    if($inputs['pwd']==""){ unset($inputs['pwd']); }
    else{ $inputs['pwd'] = Crypt::encrypt($inputs['pwd']); }
    
    if($inputs['name']==$picker->name){ unset($inputs['name']); }
    if($inputs['uName']==$picker->uName){ unset($inputs['uName']); }
    
    
    try{
    	$this->validate(new Request($inputs),[
    			'name' => 'unique:pickers,name',
    			'uName' => 'unique:pickers,uName'
    	]);
    }
    catch(\Illuminate\Validation\ValidationException $e){
    	return redirect("/admin/pickers/$id/edit");
    }
    

    foreach ($inputs as $input => $value) {
      $picker->$input = $value;
    }
    $picker->save();

    return redirect()->route('admin::pickers::show', ['id' => $id]);
  }
  
  
  public function PickerList() {
  	 
  	$pickers = Picker::getActive();//->toArray();
  	$rows = [];
  	foreach ($pickers as $count) {
  		$rows[] = [$count['name'], $count['num']];
  	}
  	 
  	$pdf  = new PdfTable(["Name","Number"], $rows, "Title");
  	 
  	$pdf->table();
  	return $pdf->out();
  }
}
