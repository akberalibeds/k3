<?php

namespace App\Http\Controllers\Admin\Driver;

use Entrust;
use App\Driver;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\FilesController;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Settings;
use App\DriverLocation;
use App\Helper\FCM;
use App\Device;

class DriverMapController extends Controller
{
  /**
   * @author Giomani Designs (Development Team)
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function index(Request $request) {
    if ($request->ajax()) {
      	return DriverLocation::getLocations();
    }
    
    $maps_key = Settings::first()->maps_key;
 
    $drivers = DriverLocation::getLocations();
    return view('admin.drivers.map',['maps_key' => $maps_key , 'drivers' => $drivers]);
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  intiger  $id
   * @return \Illuminate\Http\Response
   */
  public function sendMessage($driver,$message) {
  		
  		$device_token  = Device::getTokenForDriver($driver);
  		FCM::send($message, $device_token);
  }
  
  
}
