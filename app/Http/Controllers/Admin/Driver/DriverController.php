<?php

namespace App\Http\Controllers\Admin\Driver;

use Entrust;
use Crypt;
use Hash;
use Validator;
use App\Driver;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\FilesController;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Helper\Pdfs\PdfTable;

class DriverController extends Controller
{
  /**
   * Show the form for creating a new resource.
   *
   * @author Giomani Designs (Development Team)
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    return view('admin.drivers.create');
  }
  /**
   * Show the form for editing the specified resource.
   *
   * @author Giomani Designs (Development Team)
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit($id) {
    try {
      $driver = Driver::findOrFail($id);
    } catch(ModelNotFoundException $e) {
      return parent::notFound();
    }

    return view('admin.drivers.edit', ['driver' => $driver]);
  }
  /**
   * Display a listing of the resource.
   *
   * @author Giomani Designs (Development Team)
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function index(Request $request) {
    if ($request->ajax()) {
      return Driver::orderBy('name', 'asc')->paginate(config('search.rpp'));
    }

    return view('admin.drivers.index');
  }
  /**
   * Update the specified resource in storage.
   *
   * @author Giomani Designs (Development Team)
   * @param  string  $query
   * @return \Illuminate\Http\Response
   */
  public function searchName($query) {
    $drivers = Driver::where('name', 'like', '%' . $query . '%')->get();

    return response()->json($drivers);
  }
  /**
   * Display the specified resource.
   *
   * @author Giomani Designs (Development Team)
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id) {
    try {
      $driver = Driver::findOrFail($id);
    } catch(ModelNotFoundException $e) {
      return parent::notFound();
    }
	
	/*try{
		$driver->pwd = Crypt::decrypt($driver->pwd);
	}
	catch(\Illuminate\Contracts\Encryption\DecryptException $e){
			
	}*/
	$driverFiles = [];//FilesController::getFileNames('drivers', $id);

    return view('admin.drivers.show', ['driver' => $driver, 'driverFiles' => $driverFiles]);
  }
  /**
   * Display the specified resource.
   *
   * @author Giomani Designs (Development Team)
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request) {
    $driver = new Driver;
	
	try{
		$this->validate($request,[
			'uName' => 	'required|unique:drivers,uName',
			'pwd' 	=> 	'required',
			'name' 	=>	'required',
			'num' 	=>	'required',
		]);
	}
	catch(\Illuminate\Validation\ValidationException $e){
		return redirect("/admin/drivers/create");	
	}
	
    $inputs = $request->only(['name', 'num', 'pwd', 'rate', 'score', 'uName']);
	$inputs['pwd'] = Hash::make($inputs['pwd']);
	
    foreach ($inputs as $input => $value) {
      $driver->{$input} = $value;
    }
    $driver->save();
	
	return redirect("/admin/drivers/$driver->id/show");
    return redirect()->route('admin.drivers::show', ['id' => $driver->id]);
  }
  /**
   * Update the specified resource in storage.
   *
   * @author Giomani Designs (Development Team)
   * @param  \Illuminate\Http\Request  $request
   * @param  int                       $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id) {
    try {
      $driver = Driver::findOrFail($id);
    } catch(ModelNotFoundException $e) {
      return parent::notFound();
    }
	
	
    $inputs = $request->only(['active', 'name', 'num', 'rate', 'score', 'uName','pwd']);
	if($inputs['pwd']==""){ unset($inputs['pwd']); }
	else{ $inputs['pwd'] = Hash::make($inputs['pwd']); }
	
	if($inputs['uName']==$driver->uName){ unset($inputs['uName']); }
	
	try{
	$this->validate(new Request($inputs),[
		'uName' => 'unique:drivers,uName'
	]);
	}
	catch(\Illuminate\Validation\ValidationException $e){
		return redirect("/admin/drivers/$id/edit");	
	}
	
    foreach ($inputs as $input => $value) {
      $driver->{$input} = $value;
    }
    $driver->save();

    return redirect("/admin/drivers/$id/show");
  }
  /**
   * Display the specified resource.
   *
   * @author Giomani Designs (Development Team)
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function driverList() {
  	
  	$drivers = Driver::getActive();//->toArray();
  	$rows = [];
  	foreach ($drivers as $count) {
  		$rows[] = [$count['name'], $count['num']];
  	}
  	
  	$pdf  = new PdfTable(["Name","Number"], $rows, "Title");
  	
  	$pdf->table();
  	return $pdf->out();
  }
  /**
   * Display the specified resource.
   *
   * @author Giomani Designs (Development Team)
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function remove($id) {
  	$d = Driver::find($id);
  	$d->delete();
  	return $id;
  }
  
}
