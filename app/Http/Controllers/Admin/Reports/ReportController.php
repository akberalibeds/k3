<?php

namespace App\Http\Controllers\Admin\Reports;

use Entrust;
use App\Info;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\MagentoAccount;
use Time;
use App\Order;
use App\Classes\Reports;

class ReportController extends Controller
{
  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    return view('bulletins.create');
  }
  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    // 
  }
  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit($id)
  {
    return view('admin.magento.edit', ['magento' => MagentoAccount::find($id) ]);
  }
  /**
   * Display a listing of the resource.
   *
   * @author Giomani Designs
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
	
	return view('admin.reports.index',Reports::getReportDashboard());
	
  }

  /**
   * Update the specified resource in storage.
   *
   * @author Giomani Designs
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function search(Request $request)
  {
	
	return Reports::search($request);

	
  }
  
  
}
