<?php

namespace App\Http\Controllers\Admin\Orders;

use Event;
use Validator;
use App\PaymentType;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Events\PaymentTypeAdding;
use App\Http\Controllers\Controller;

class OrderPaymentTypesController extends Controller
{
  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create() {
      //
  }
  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id) {
      //
  }
  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit($id) {
      //
  }
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index() {
    $paymentTypes = PaymentType::all();

    return view('admin.order-payment-types.index', ['paymentTypes' => $paymentTypes]);
  }
  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id) {
      //
  }
  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request) {
    $validator = Validator::make($request->all(), PaymentType::$rules);

    if ($validator->fails()) {
      return response($validator->errors(), 422);
    }

    $description = $request->input('description');
    $type = $request->input('payment_type');

    $paymentType = new PaymentType;

    $paymentType['description'] = $description;
    $paymentType['payment_type'] = $type;

    $paymentType->save();

    Event::fire(new PaymentTypeAdding($description, $paymentType->id, $type));

    return [
      'description' => $description,
      'payment_type' => $type,
    ];
  }
  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id) {
      //
  }
}
