<?php

namespace App\Http\Controllers\Admin\Magento;

use Entrust;
use App\Info;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\MagentoAccount;
use Time;

class MagentoController extends Controller
{
  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    return view('bulletins.create');
  }
  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    //
  }
  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit($id)
  {
    return view('admin.magento.edit', ['magento' => MagentoAccount::find($id) ]);
  }
  /**
   * Display a listing of the resource.
   *
   * @author Giomani Designs
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    return view('admin.magento.index', ['magentos' => MagentoAccount::all() ]);
  }

  /**
   * Update the specified resource in storage.
   *
   * @author Giomani Designs
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request)
  {
	
	$data = $request->except('_token');
	$data['last_edit'] = Time::date()->get('Y-m-d H:i:s');
	
	//return $data;
	
	$ebay  =  EbayAuth::find(1);
	$ebay->update($data);
	
	return view('admin.ebay.edit', ['ebay' => $ebay->toArray() , 'save' => 1]);
	
  }
  
  
}
