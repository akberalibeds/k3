<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Driver;
use Illuminate\Support\Facades\Crypt;
use PhpParser\Node\Stmt\TryCatch;
use App\DispatchedRoute;
use App\Dispatched;
use App\Device;
use App\DriverLocation;
use File;
use App\Order;
use App\OrderNotes;
use App\PDA;
use App\OrderPayment;

class PdaController extends Controller
{
	private $VERSION = 2.9;
    /**
     * Login
     *
     * @return \Illuminate\Http\Response
     */
    public function login(Request $request)
    {
        //
    	return Driver::login($request);
    	
    }

   

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function updateCheck($version)
    {
    	
    	if($this->getVersion() > $version)
    		return 1;
    	else
    		return 0;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateLoc(Request $request)
    {
    	$driver = Device::getDriverID($request->get("device"));
    	$data = $request->except('device');
    	$data['driver_id']=$driver;
    	return DriverLocation::insertLocation($data);    	
    	
    }
    
    public function updateToken(Request $request)
    {
    	$driver = Device::updateToken($request);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getRouteList(Request $request)
    {
    	
    	$driver = Device::getDriver($request->get("device"));
    	$date 	= date('d-M-Y');
 		$route  = DispatchedRoute::where('driver',$driver)->where('date',$date)->orderBy('id','desc')->first();
 		
 		if(!$route){
 			return ['error'=>'No Route'];
 		}
 		
 		$list = json_encode(Dispatched::select(
 									'orders.id',
 									'orders.delOrder as num',
 									'orders.orderStatus as status',
 									'orders.total',
 									'orders.paid',
 									'orders.paymentType',
 									\DB::raw("REPLACE(REPLACE(dBusinessName,'\t',''),'\n','') as name"),
 									\DB::raw("REPLACE(dNumber,'\t','') as number"),
 									\DB::raw("REPLACE(dStreet,'\t','') as street"),
 									\DB::raw("REPLACE(dTown,'\t','') as town"),
 									\DB::raw("REPLACE(dPostcode,'\t','') as postcode"),
 									\DB::raw("REPLACE(tel,'\t','') as tel"),
 									\DB::raw("REPLACE(mob,'\t','') as mob"),
 									\DB::raw("group_concat(itemCode,':',bar) as items")
 									)
 								->join('orders','disp.oid','=','orders.id')
 								->join('customers','orders.cid','=','customers.id')
 								->join('codes','disp.oid','=','codes.oid')
		 						->where(['route' => $route->route, 'date' => $route->date])
		 						->groupBy('orders.id')
		 						->orderBy('orders.delOrder')
		 						->get());
 		
 		return str_replace("'","\\'",$list);
 		
    	//$oids.= ($i+1)."~$db_field[id]~$db_field[dBusinessName]~$db_field[dPostcode]~$db_field[orderStatus]~".details($link,$db_field['id'])."~".getNotes($link,$db_field['id'])."~".($db_field['total']-$db_field['paid'])."~$db_field[parcelid]~$dt~$db_field[dispatchType]~$db_field[orderType]::";
    
    }
    
    private function getVersion(){
    	//TO DO GET VERSION FROM DB;
    	return $this->VERSION;
    }
    
    
    public function uploadImage(Request $request){
    	
    	$image = base64_decode($request->get("image"));
    	$id = $request->get("id");
    	$note = $request->get("note");
    	$driver = Device::getDriver($request->get('device'));
    	
    	
    	if(!is_dir(public_path("pod"))){
    		File::makeDirectory(public_path("pod"));
    	}
    	
    	$path = public_path("pod/$id");
    	
    	if(!is_dir($path)){
    		File::makeDirectory($path);
    	}
    	
    	$file = time().".jpg";
    	file_put_contents($path."/$file",$image);
    	
    	OrderNotes::addNote("<img src ='/pod/$id/$file' height='175' class='pod' /> <br/>$note", $id, $driver);
   		
    	
    }
    
    
    
    public function uploadStatus(Request $request){
    	 
    	
    	$id = $request->get("id");
    	$status = $request->get("status");
    	$note = $request->get("note");
    	$driver = Device::getDriver($request->get('device'));
    	 
    	Order::changestatus(['old' => 'Dispatched', 'new' => $status], $id);   	 
    	OrderNotes::addNote($note, $id, $driver);
    	 
    }
    
    
    public function uploadNote(Request $request){
    
    	 
    	$id = $request->get("id");
    	$note = $request->get("note");
    	$driver = Device::getDriver($request->get('device'));
    	OrderNotes::addNote($note, $id, $driver);
    
    }
    
    
    public function uploadPayment(Request $request){
        
    	$id = $request->get("id");
    	$paid = $request->get("paid");
    	$driver = Device::getDriver($request->get('device'));
    	OrderNotes::addNote("Customer Paid $paid cash", $id, $driver);
    	
    	$order = Order::find($id);
    	$order->paid = $paid;
    	$order->save();

    	OrderPayment::addPayment(['stamp' => time(), 'paid' => $paid,  'oid' => $id, 'type' => 'Cash', 'staff' => $driver]);
    	
    }
    
    
    public function apk(){
    	$path = public_path("driver-pda/app-release.apk");
    	return response()->file($path ,[
    			'Content-Type'=>'application/vnd.android.package-archive',
    			'Content-Disposition'=> 'attachment; filename="android.apk"',
    	]) ;
    	
    }
    
    
    
    
}
