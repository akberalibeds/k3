<?php
// @author Giomani Designs (Development Team)
//
namespace App\Http\Controllers;

use Auth;
use App\Info;
use App\Bulletin;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Helper\Traits\Document;

class HomeController extends Controller
{
  use Document;
  /**
   * Show the application dashboard.
   *
   * @author Giomani Designs (Development Team)
   * @return \Illuminate\Http\Response
   */
  public function index() {
    $bulletins = Bulletin::select('expires', 'message', 'title')->orderBy('created_at', 'desc')->take(5)->get();

    $info =  Info::select('id', 'datum', 'description', 'type')->get();

    $documents = [];
    $emails = [];
    $links = [];
    $numbers = [];

    foreach ($info as $item) {
      switch ($item->type) {
        case Info::INFO_TYPE_DOCUMENT :
          $documents[] = $item;
          break;
          //
        case Info::INFO_TYPE_EMAIL :
          $emails[] = $item;
          break;
          //
        case Info::INFO_TYPE_NUMBER :
          $numbers[] = $item;
          break;
          //
        case Info::INFO_TYPE_LINK :
          $links[] = $item;
          break;
          //
      }
    }

    return view('home', ['bulletins' => $bulletins, 'documents' => $documents, 'emails' => $emails, 'links' => $links, 'numbers' => $numbers]);
  }
  
  
  
  public function epdq(Request $request){
  	 
  	return $request->all();
  	 
  }
}
