<?php
// @author Jason Fleet (jason.fleet@googlemail.com)
//
namespace App\Http\Controllers;

use Excel;
use App\Helper\Files;
use App\Http\Controllers\Controller;

abstract class CsvImportControllerAbstract extends Controller
{
  /**
   * @var
   */
  protected $accpets;
  /**
   * @var
   */
  protected $accpetsCount;
  /**
   * @var
   */
  protected $columnSpecs;
  /**
   * @var
   */
  protected $csvProcessor;
  /**
   * @var
   */
  protected $profileSpecs;
  /**
   * @var
   */
  protected $mode;
  /**
   * @var
   */
  protected $params;
  /**
   * @var
   */
  protected $rejects;
  /**
   * @var
   */
  protected $rejectsCount;
  /**
   * @var
   */
  protected $singles;
  /**
   * @author Giomani Designs (Development Team)
   * @param  string $success
   */
  public function addAccept($success) {
    $this->successes[] = $success;
  }
  /**
   * add a failure
   *
   * @param  string $failure
   */
  public function addReject($failure) {
    $this->failures[] = $failure;
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  string $profileName the name of the profile to fetch
   * @return \Illuminate\Http\Response
   */
  public function fetchImportProfile ($name) {
    $this->profileSpecs = include(config('import.paths.profiles') . '/' . $name . '.php');
    return $this;
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  string $profileName the name of the profile to fetch
   * @return \Illuminate\Http\Response
   */
  public function getImportProfileHeadings () {
    $headings = [];
    $inputs = [];
    $singles = [];
    foreach ($this->profileSpecs['columns'] as $key => $entity) {
      if (!$entity['nis']) {
        $headings[] = ['heading' => $entity['column_heading'], 'name' => $key];
      }
    }
    foreach ($this->profileSpecs['inputs'] as $key => $entity) {
      $inputs[] = ['heading' => $entity['column_heading'], 'name' => $key];
    }
    foreach ($this->profileSpecs['singles'] as $key => $entity) {
      $singles[] = $entity;
    }
    return ['headings' => $headings, 'inputs' => $inputs, 'singles' => $singles];
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  string $profileName the name of the profile to fetch
   * @return \Illuminate\Http\Response
   */
  public function getImportProfileName () {
    return $this->profileSpecs['name'];
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  string  $param
   * @return mixed
   */
  public function getParam ($param) {
    return $this->params[$param];
  }
  /**
   * @author Giomani Designs (Development Team)
   * @return array  $data
   * @return string  $mode {verify|store}
   */
  public function processData ($data, $mode) {
    $this->verifyCsvData($data);
    $results = [
      'accepts' => [
        'count' => $this->acceptsCount,
        'rows' => $this->accepts,
      ],
      'rejects' => [
        'count' => $this->rejectsCount,
        'rows' => $this->rejects,
      ],
    ];
    if ($mode === 'store' && $this->rejectsCount == 0) {
      $results['strorage'] = $this->storeCsvData();
    }
    return $results;
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function readFile($request) {
  	ini_set('display_errors',1);
    $files = new Files;
    
    $result = $files->recieve($request->input('chunk'), $request->input('chunkIndex'), $request->input('fileId'), $request->input('extension'), $request->input('name'), $request->input('totalChunks'));
    if ($result !== false) {
      $results = Excel::load($result)->all()->toArray();
      return [
        'result' => 'complete',
        'sheet' => $results
      ];
    }
    return ['error' => 'an error occured'];
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  array  $params
   */
  public function setCsvProcessor($csvProcessor) {
    $this->csvProcessor = $csvProcessor;
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  array  $params
   */
  public function setParams($params) {
    $this->params = $params;
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  $data array mixed
   * @return void
   */
   public function storeCsvData() {
     return $this->csvProcessor->processCsv($this->accepts, $this->singles);
   }
  /**
   * @author Giomani Designs (Development Team)
   * @param  $data array mixed
   * @return void
   */
  public function verifyCsvData($data) {
    $this->accepts = [];
    $this->rejects = [];
    $rows = $data['rows'];
    $importColumns = $data['columns'];
    $this->singles = [];
    foreach ($data['singles'] as $single) {
      $this->singles[$single['name']] = $single['value'];
    }
    foreach ($rows as $row) {
      $errorMessages = [];
      $kRow = [];
      $rowIndex = $row['index'];
      if (isset($row['rowData'])) {
        $rowData = $row['rowData'];
        $columnSpecs = $this->profileSpecs['columns'];
        foreach ($columnSpecs as $columnKey => $columnSpec) {
          if (($idx = array_search($columnKey, $importColumns)) !== false) {
            if (!isset($rowData[$idx])) {
              dd($rowData);
            }
            $datum = $rowData[$idx];
            $kRow[$columnKey] = $datum;
            if (isset($columnSpec['validation'])) {
              $rules = $columnSpec['validation'];
              foreach ($rules as $rule => $pred) {
                $error = false;
                if ($rule === 'date') {
                  if (\DateTime::createFromFormat($pred['format'], $datum) === false) {
                    $error = true;
                  }
                } elseif ($rule === 'email') {
                  if (!filter_var($datum, FILTER_VALIDATE_EMAIL)) {
                    $error = true;
                  }
                } elseif ($rule === 'length_equal') {
                  if ($pred['size'] !== strlen($datum)) {
                    $error = true;
                  }
                } elseif ($rule === 'number') {
                  if (!is_numeric($datum)) {
                    $error = true;
                  }
                }
                elseif ($rule === 'not_empty') {
                 if (strlen($datum) === 0) {
                   $error = true;
                 }
                } elseif ($rule === 'postcode') {
                  $postcode = strtoupper(str_replace(' ', '', $datum));
                  if (!(preg_match("/^[A-Z]{1,2}[0-9]{2,3}[A-Z]{2}$/", $postcode) || preg_match("/^[A-Z]{1,2}[0-9]{1}[A-Z]{1}[0-9]{1}[A-Z]{2}$/", $postcode) || preg_match("/^GIR0[A-Z]{2}$/", $postcode))) {
                    $error = true;
                  }
                } elseif ($rule === 'regex') {
                  if (preg_match($pred['expression'], $datum) == 0) {
                    $error = true;
                  }
                }
              }
              if ($error) {
                $errorMessages[] = $columnSpec['column_heading'] . ' ' . $pred['message'];
              }
            }
          } else {
            if ($columnSpec['required']) {
              $errorMessages[] = $columnSpec['column_heading'] . '[' . $columnKey . '] is a required column';
            }
          }
        }
      } else {
        $errorMessages[] = 'Row has no data';
      }
      if (count($errorMessages) > 0) {
        $this->rejects[] = ['row' => $kRow, 'error_messages' => $errorMessages, 'index' => $rowIndex];
      } else {
        $this->accepts[] = ['row' => $kRow, 'index' => $rowIndex];
      }
    }
    $this->acceptsCount = count($this->accepts);
    $this->rejectsCount = count($this->rejects);
  }
}
