<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\OrderAuthorisation as OA;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\StreamedResponse;

class OrderAuthorisationController extends Controller
{
    public function refunds()
    {
        request('dateFrom')? $dateFrom = \Carbon\Carbon::parse( request('dateFrom') ) : $dateFrom  = \Carbon\Carbon::today();
        request('dateTo')? $dateTo = \Carbon\Carbon::parse( request('dateTo') ) : $dateTo  = \Carbon\Carbon::today();

        $refunds = OA::where('action_type', 'refund')->whereBetween(\DB::raw('DATE(created_at)'), array($dateFrom, $dateTo))->get();

        if(! request('csv')) {
    
            return view('orders.order-authorisations.refunds', compact('refunds', 'dateFrom','dateTo'));
        }

        return $this->downloadCSV($refunds, [
            'Original Order_ID'=>'order->parent_order_id', 
            'Order_ID'=>'order->id', 
            'Timestamp'=>'created_at', 
            'Staff name'=>'staff->email',
            'Requested by'=>'requestedBy->email',
            'Authorised By' => 'authorisedBy->email', 
            'Customer name'=> 'order->customer->businessName',
            'Customer postcode' => 'order->customer->postcode',
            'Company name' => 'order->companyName',
            'Reason for refund' => 'reason',
            'Total refund' =>  function($model) {
                return - $model->order->total;
            },
            'Item code' => function($model) {
                $str = '';

                foreach($model->order->parentOrder->items as $item) {
                    $str .= $item->stock->itemDescription. ',';
                }

                return $str;
              }
            ], 
        'refunds');

       
    }

    public function discounts()
    {
        request('dateFrom')? $dateFrom = \Carbon\Carbon::parse( request('dateFrom') ) : $dateFrom  = \Carbon\Carbon::today();
        request('dateTo')? $dateTo = \Carbon\Carbon::parse( request('dateTo') ) : $dateTo  = \Carbon\Carbon::today();
        
        $discounts = OA::where('action_type', 'discount')->whereBetween(\DB::raw('DATE(created_at)'), array($dateFrom, $dateTo))->get();

        if( ! request('csv')) {
            return view('orders.order-authorisations.discounts', compact('discounts', 'dateFrom','dateTo'));
        }

        return $this->downloadCSV($discounts, [
            'Original Order_ID' => 'order->parent_order_id', 
            'Order_ID' => 'order->id', 
            'Timestamp' => 'created_at', 
            'Staff name' => 'staff->email',
            'Requested by' => 'requestedBy->email',
            'Authorised By' => 'authorisedBy->email', 
            'Customer name'=> 'order->customer->businessName',
            'Customer postcode' => 'order->customer->postcode',
            'Company name' => 'order->companyName',
            'Reason for discount' => 'reason',
            'Total refund' => function($model) {
                return - $model->order->total;
            },
            'Item code' => function($model) {
                $str = '';
                foreach($model->order->parentOrder->items as $item) {
                     $str .= $item->stock->itemDescription . ',';
                }

                return $str;
              }
            ], 
        'discounts');
       
    }

    public function downloadCSV($list,$columns, $filename = NULL)
    {
        return new StreamedResponse(
            function () use ($list, $columns, $filename) {
                // A resource pointer to the output stream for writing the CSV to
                $handle = fopen('php://output', 'w');

                fputcsv($handle, array_keys($columns));

                foreach ($list as $item) {

                    $data = [];

                    foreach ($columns as $name => $property) {

                        $data [] = helperGetChainedProperty($item,$property);
                    }
                    // Loop through the data and write each entry as a new row in the csv
                    fputcsv($handle, $data);
                }
                fclose($handle);
            },
            200,
            [
                'Content-type'        => 'text/csv',
                'Content-Disposition' => "attachment; filename={$filename}.csv"
            ]
        );
    }
}
