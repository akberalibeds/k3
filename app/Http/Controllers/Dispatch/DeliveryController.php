<?php

namespace App\Http\Controllers\Dispatch;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Driver;
use App\Routes;
use App\Order;
use App\OrderNotes;
use App\Settings;
use App\OrderItems;
use App\DispatchedRoute;
use App\Dispatched;
use App\ToShip;
use Time;
use Auth;

class DeliveryController extends Controller
{
  /**
   * Show the form for creating a new resource.
   *
   * @author Giomani Designs
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
  }

  /**
   * Remove the specified resource from storage.
   *
   * @author Giomani Designs
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
      //
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @author Giomani Designs
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit($id)
  {
  }

  /**
   * Display a listing of the resource.
   *
   * @author Giomani Designs
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    // $routes = Routes::orderBy('name', 'asc')->get();
    $routes = Routes::select('id','name')->orderBy('name','asc')->get();//Routes::routeDetails('name', 'asc')->get();
	
	
	$deliveries = Order::where(['orderStatus' => 'Allocated', 'processed' => 0])
						->select(\DB::raw('count(*) as delCount, id_delivery_route,deliverBy'))
						->groupBy('id_delivery_route','deliverBy')
						->get();
	

    return view('deliveries.index', ['routes' => $routes, 'deliveries' => $deliveries]);
  
  }

  /**
   * Update the specified resource in storage.
   *
   * @author Giomani Designs
   * @param  \Illuminate\Http\ChangePasswordRequest  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update($request, $id)
  {
      //
  }
  
  
  public function clearRoute($route)
  {
  	$orders = Order::select('id')->where('delRoute',$route)->where('id_status',2)->get();
  	
  	foreach($orders as $order){
  		Order::setDeliveryRoute($order->id);
  	}
  	
  }

	
	
  public static function getRouteDels(Request $request)
    {
    		
			$date = $request->has('date') ? $request->get('date') : false;    
			
			$search = ['o.orderStatus' => "Allocated", 'processed' => 0, 'id_delivery_route' =>  $request->get('id')];
            if($date) {
				 $search["deliverBy"]=$date;
			}

            $rows = OrderItems::where($search)
					->select(\DB::raw('o.id, o.lat, o.lng, o.dispatchType, o.p, o.done, o.companyName, o.startDate, o.deliverBy, 
						if(o.direct = 1,d.name,c.dBusinessName) as name, if(o.direct=1,d.town,dTown) as town, 
						if(o.direct = 1,d.postcode,dPostcode) as postcode ,group_concat(s.itemCode) as items,group_concat(s.actual) as stockCount'))
					->join('orders as o', 'orderItems.oid','=', 'o.id')
					->join('customers as c', 'o.cid', '=', 'c.id')
					->leftJoin('direct as d', 'o.id', '=', 'd.oid')
					->join('stock_items as s', 'orderItems.itemid', '=', 's.id')
					->groupBy('o.id')
					->get()
					->toArray();
			return json_encode($rows);
	}
	
	
	/*
	* CHANGE ROUTE
	* Seperate function to change route for extra functions;
	* @param  \Illuminate\Http\Request  $request
    * @param  int  $id
	*/
	public function changeroute_mass(Request $request){

		$data = $request->get('data');
		Order::changeroute_mass($data);

	}
	
	
	
	public function routing(Request $request)
	{
	
		
		//$request->only('orders','route','driver','tel','mail','time');
		
		$date = Time::date()->days(1)->get('d-M-Y');//('d-M-Y', strtotime(date('d-M-Y'). ' + 1 days'));
		$route = $request->get('route');
		$dater = Time::date()->get('d-M-Y');
		
		$dispRoute = new DispatchedRoute;
		$dispRoute->route = $route;
		$dispRoute->date = $date;
		$dispRoute->driver = $request->get('driver');
		$dispRoute->staff = Auth::user()->name;
		$dispRoute->startTime = Time::date($request->get('time'))->get('H:i');
		$dispRoute->save();
		
		$orders = json_decode($request->get('orders'));
		$sumCount=1; 
		$i=1;
		$dispatchOrder="";
			
		foreach($orders as $order){
			
			// update order add delivery date and slot
			Order::where('id',$order->id)
					->update(['delOrder' => $i, 'delTime' => $order->time , 'extra' => '']);
			
			// add note to orders 
			OrderNotes::addNote('Routed for dispatch',$order->id);
			
			//insert each order in to dispatched table
			$disp = new Dispatched;
			$disp->oid = $order->id; 
			$disp->route = $route;
			$disp->date = $date;
			$disp->num = $sumCount;
			$disp->id_route = $dispRoute->id;
			$disp->save();
			
			// add order to shipping table for texts and email
			$ship = new ToShip;
			$ship->oid = $order->id;
			$ship->tel = $request->get('tel');
			$ship->mail = $request->get('mail');	
			$ship->date = $dater;
			$ship->save();
		
			// change Status on orders
			Order::changeStatus(['new' => "Routed", 'old' => "Allocated"], $order->id);
		
			$dispatchOrder .= $order->id.":";
			$sumCount++;
			$i++;
		}
		
		
		
		
		/*
		$dispatchOrder = rtrim($dispatchOrder,":");
		$data = array();
		$data['route']=		$_REQUEST['route'];
		$data['date'] = 		$_REQUEST['date'];
		$data['driver'] = 	$_REQUEST['driver'];
		$data['mate'] = 		$_REQUEST['mate'];
		$data['vehicle'] = 	$_REQUEST['vehicle'];
		$data['orders'] = 	$dispatchOrder;
		$data = json_encode(array($data));
		*/
		
		return $dispRoute->id;
	}

}
