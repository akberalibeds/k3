<?php

namespace App\Http\Controllers\Dispatch;

use App\Order;
use App\Routes;
use \Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\RoutePutRequest;
use App\Http\Requests\RoutePostRequest;
use App\DispatchedRoute;
use App\Dispatched;

class RouteController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @author Giomani Designs
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function index (Request $request) {
    if ($request->ajax()) {
      $query = $request->all();
      return Routes::getAll($query);
    }
    return view('routes.index');
  }
  /**
   * @author Giomani Designs
   * @param  int  $id the route-id
   * @return \Illuminate\Http\Response
   */
  public function show ($id) {
    try {
      $route = Routes::findOrFail($id);
    } catch(ModelNotFoundException $e) {
      return parent::notFound();
    }
    return view('routes.show', ['route' => $route]);
  }
  /**
   * @author Giomani Designs
   * @param  int  $id the route-id
   * @return \Illuminate\Http\Response
   */
  public function showOrders ($id) {
    return Order::getRoute($id);
  }
  /**
   * @author Giomani Designs
   * @param  int  $id the route-id
   * @return \Illuminate\Http\Response
   */
  public function setOptions (Request $request) {
  	return DispatchedRoute::setOptions($request);
  }
  
  
  public function sortOrder(Request $request){
  	
  	$data = $request->get('data');
  	
  	foreach ($data as $order => $num ){
  		
  		Order::where('id',$order)->update(['delOrder' => $num]);
  		Dispatched::where('oid',$order)->update(['num' => $num]);
  		
  	}
  	
  	
  }
  
}
