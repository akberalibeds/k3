<?php

namespace App\Http\Controllers\Dispatch;

use Event;
use App\Order;
use App\Driver;
use App\Oreder;
use App\Picker;
use App\Vehicle;
use App\DriverMate;
use App\Dispatched;
use App\Helper\SMS;
use App\Http\Requests;
use App\DispatchedRoute;
use App\Helper\PdfCodOrders;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Helper\Pdfs\PdfLabel;
use App\Helper\PdfDispatchNotes;
use App\Helper\Traits\OutHeaders;
use App\Events\MarkingAsDelivered;
use App\Http\Controllers\Controller;
use App\Helper\Pdfs\PdfSupplierLoadSheets;
use App\Facades\Time;
use App\Oos_Search;

class DispatchedController extends Controller
{
  use OutHeaders;
  /**
   * @author Giomani Designs
   * @param  int $ids dispRoute ids
   * @return \Illuminate\Http\Response
   */
  public function getCsv ($ids) {
    $csvColumnHeadings = ['Accname','address1','address2','address3','Town','County','Postcode','Items','Weight','Reference','Special Instructions'];
    $columns = ['c_name', 'c_number', 'c_street', false, 'c_town', false, 'c_postcode', 'parts_delivered', false, 'oid', false];
    $dispatchedRoutes = DispatchedRoute::whereIn('id', explode('|', $ids))->get();
    $filename = storage_path('Dispatched-Routes.csv');
	 
   
    //$this->outCsvHeaders($filename);
  
    $out = fopen($filename, 'w+');

    fputcsv($out, $csvColumnHeadings);

    foreach ($dispatchedRoutes as $dispRoute) {
      $orders = Order::getDispatchedNotesByRoute($dispRoute->date, $dispRoute->route);
      foreach ($orders as $order) {
        $row = [];
        foreach ($columns as $column) {
          $row[] = $column === false ? '' : $order[$column];
        }
        fputcsv($out, $row);
      }
    }
    fclose($out);
    
    $headers = array(
    		'Content-Type' => 'text/csv',
    );
    
    return response()->download($filename, 'dispatched.csv', $headers);
    
    
  }
  /**
   * @author Giomani Designs
   * @param  int $ids dispRoute ids
   * @return \Illuminate\Http\Response
   */
  public function getRouteCsv ($ids) {
  	$csvColumnHeadings = ['Time','Driver','Route','Helper','Area'];
  	$columns = ['startTime', 'driver', 'route',  'mate', false];
  	$dispatchedRoutes = DispatchedRoute::whereIn('id', explode('|', $ids))->get();
  	$filename = storage_path('Dispatched-Routes.csv');
  
  	 $out = fopen($filename, 'w+');
  	
  	 
  	fputcsv($out, $csvColumnHeadings);
  
  	foreach ($dispatchedRoutes as $dispRoute) {
  		//$orders = Order::getDispatchedByRoute($dispRoute->date, $dispRoute->route);
  		//foreach ($orders as $order) {
  			$row = [];
  			foreach ($columns as $column) {
  				$row[] = $column === false ? '' : $dispRoute[$column];
  			}
  			fputcsv($out, $row);
  		//}
  	}
  	fclose($out);
  	$headers = array(
  			'Content-Type' => 'text/csv',
  	);
  	
  	return response()->download($filename, 'dispatched.csv', $headers);
  }
  /**
   * @author Giomani Designs
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  
  public function index (Request $request) {
    if ($request->ajax()) {
      return DispatchedRoute::getAll($request->all());
    }
    $drivers = Driver::orderBy('name', 'asc')->get();
    $driverMates = DriverMate::orderBy('name', 'asc')->get();
    $pickers = Picker::orderBy('name', 'asc')->get();
    $vehicles = Vehicle::orderBy('reg', 'asc')->get();
    $items = Oos_Search::getItems();
    return view('dispatched.index', ['drivers' => $drivers, 'driverMates' => $driverMates, 'pickers' => $pickers, 'vehicles' => $vehicles, 'items' => $items]);
  }
  /**
   * @author Giomani Designs
   * @param  int $ids dispRoute ids
   * @return \Illuminate\Http\Response
   */
  public function markAsDelivered ($ids) {
    Event::fire(new MarkingAsDelivered($ids));
    $dispatchedRoutes = DispatchedRoute::whereIn('id', explode('|', $ids))->get();
    foreach ($dispatchedRoutes as $dispRoute) {
      $orders = Order::setDispatchedByRouteAsDelivered($dispRoute->date, $dispRoute->route);
    }
    return $orders;
  }
  /**
   * @author Giomani Designs
   * @return \Illuminate\Http\Response
   */
  public function printCodOrders ($date = null) {
    if ($date === null) {
      $date = new \Datetime;
      $date->add(new \DateInterval('P1D'));
      $date = $date->format('d-M-Y');
    }
    $orders = Order::getCod($date);
    $pdf = new PdfCodOrders();
    $pdf->pdfCodOrders($orders, $date);
    return $pdf->outPdf();
  }
  /**
   * @author Giomani Designs
   * @param  int $ids dispRoute ids
   * @return \Illuminate\Http\Response
   */
  public function printDispatchNotes ($ids) {
    $dispatchedRoutes = DispatchedRoute::whereIn('id', explode('|', $ids))->get();
    $pdf = new PdfDispatchNotes();
    foreach ($dispatchedRoutes as $dispRoute) {
      $date = \Datetime::createFromFormat('d-F-Y', $dispRoute->date);
      $pathParts = [$dispRoute->date, $dispRoute->route];
      $orders = Order::getDispatchedNotesByRoute($date, $dispRoute->route);
      $pdf->setHeader($dispRoute);
      $pdf->pdfDispatchNotes($orders, $dispRoute);
    }
    return $pdf->outPdf();
  }
  /**
   * @author Giomani Designs
   * @param  int $ids dispRoute ids
   * @return \Illuminate\Http\Response
   */
  public function printLabels ($ids) {
    $routeLabels = DispatchedRoute::getLabels(explode('|', $ids));
    $pdf = new PdfLabel();
    foreach ($routeLabels as $routeLabel) {
      $pdf->label($routeLabel);
    }
    // foreach ($dispatchedRoutes as $dispRoute) {
    //   $orders = Order::getDispatchedByRoute($dispRoute->date, $dispRoute->route);
    //   $pdf->addLabels($orders, $dispRoute);
    // }
    return $pdf->outPdf();
  }
  /**
   * @author Giomani Designs
   * @param  int $ids dispRoute ids
   * @return \Illuminate\Http\Response
   */
  public function printLoadCounts ($ids) {
    $dispatchedRoutes = DispatchedRoute::whereIn('id', explode('|', $ids))->get();
    $pdf = new PdfDispatchNotes();
    foreach ($dispatchedRoutes as $dispRoute) {
      $date = \Datetime::createFromFormat('d-M-Y', $dispRoute->date);
      $orders = Order::getDispatchedNotesByRoute($date, $dispRoute->route);
      $pdf->setHeader($dispRoute);
      $pdf->pdfPickList($orders, $dispRoute);
    }
    return $pdf->outPdf();
  }
  /**
   * @author Giomani Designs
   * @param  int $ids dispRoute ids
   * @return \Illuminate\Http\Response
   */
  public function printSupplierOrders ($date = null) {
    if ($date === null) {
      $date = new \Datetime;
      $date->add(new \DateInterval('P1D'));
      $date = $date->format('d-M-Y');
    }
    $orders = Order::getForPrintSupplierOnDispatch($date);
    $pdf = new PdfSupplierLoadSheets();
    $pdf->loadSheets($orders, $date);

    return $pdf->outPdf();
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  int  $id
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function show (Request $request, $routeId, $date, $routeName) {
    $detail = Dispatched::getRouteOrders($routeId, $date, $routeName);
    if ($request->ajax()) {
      return $detail->toArray();
    }
    return $detail;
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function textShifts (Request $request, $ids) {
    $sms = new SMS;
    $ids = explode(',', $ids);
    
    $result = [];
    $i=1;
    foreach ($ids as $id) {
      $textingData = DispatchedRoute::getRouteForTexting($id)[0];
     $result[] = $sms->sendSMS($textingData['driver_telnum'], 'mate ' . $textingData['mate_name'] . ' ' . $textingData['mate_telnum'] . "\ntime " . $textingData['disp_route_time'] . "\n reply to 07563011477 to confirm");
      if ($textingData['mate_telnum'] != '') {
     $result[] =  $sms->sendSMS($textingData['mate_telnum'], 'driver ' . $textingData['driver_name'] . ' ' . $textingData['driver_telnum'] .  "\ntime " . $textingData['disp_route_time']  . "\n reply to 07563011477 to confirm");
      }
    }
    return $result;
  }
  /**
   * @author Giomani Designs
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update (Request $request, $id) {
    //
  }
  /**
   * @author Giomani Designs
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function generateBarcodes($ids) {
  	set_time_limit(0);
    $dispatchedRoutes = DispatchedRoute::whereIn('id', explode('|', $ids))->get();
    foreach($dispatchedRoutes as $dispRoute) {
      $orders = Dispatched::getRouteOrders($dispRoute->id, $dispRoute->date, $dispRoute->route);

      foreach($orders as $order) {
        Order::setBarcode($order->oid);
      }
    }
  }
  
  
  public function oos($items){
  	
  	
  	Oos_Search::saveItems($items);
  	$items = json_decode($items);
  	
  	for($i=0;$i<count($items);$i++){
  		$items[$i]="'".$items[$i]."'";
  	}
  	
  	$error = false;
  	$date = Time::date()->days(1)->get('d-M-Y');
  	
  	
  	try{
  		$data = \DB::select(\DB::raw("select d.oid,delRoute,itemCode from disp d join orders o on d.oid=o.id join orderItems i on o.id=i.oid join stock_items s on i.itemid=s.id where date ='$date' and itemCode in (".implode(",",$items).")"));
  	}
  	catch (QueryException $e){
  		$error =  $e->getMessage();
  	}
  	
  	if($error){
  		return $error;
  	}
  	
  	if(!$data){
  		return '<br>No Data';
  	}
  	
  	$heads = array_keys((array)$data[0]);
  	
  	$filename = storage_path()."/file.csv";
  	$handle = fopen($filename, 'w+');
  	fputcsv($handle, $heads);
  	foreach($data as $rows){
  		fputcsv($handle, (array)$rows);
  	}
  	
  	fclose($handle);
  	$headers = array(
  			'Content-Type' => 'text/csv',
  	);
  	
  	return response()->download($filename, 'file '.date("d-m-Y H:i").'.csv', $headers);
  	
  }
  
  
  public function tox(){
  	
  	$date = Time::date()->days(1)->get('d-M-Y');
  	$error = false; 
  	
  	try{
  		$data = \DB::select(\DB::raw("select oid from orderNotes where notes like '%$date%' and staff='zayn'"));
  	}
  	catch (QueryException $e){
  		$error =  $e->getMessage();
  	}
  	 
  	if($error){
  		return $error;
  	}
  	 
  	if(!$data){
  		return 'No Data';
  	}
  	 
  	$heads = array_keys((array)$data[0]);
  	 
  	$filename = storage_path()."/file.csv";
  	$handle = fopen($filename, 'w+');
  	fputcsv($handle, $heads);
  	foreach($data as $rows){
  		fputcsv($handle, (array)$rows);
  	}
  	 
  	fclose($handle);
  	$headers = array(
  			'Content-Type' => 'text/csv',
  	);
  	 
  	return response()->download($filename, 'file '.date("d-m-Y H:i").'.csv', $headers);
  }
  
  
  
  
}
