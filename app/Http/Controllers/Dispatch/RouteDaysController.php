<?php

namespace App\Http\Controllers\Dispatch;

use App\RouteDay;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RouteDaysController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @author Giomani Designs
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function index(Request $request) {
    if ($request->ajax()) {
      $query = $request->all();
      return RouteDay::getAllDays($query);
    }
    return view('route-days.index');
  }
}
