<?php

namespace App\Http\Controllers\Dispatch;

use App\Routes;
use App\Order;
use App\StockItem;
use App\Coordinates;
use App\Settings;
use App\OrderItems;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MapController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }



	public function view_map($orders,$route)
	{
		$orders = json_decode($orders,true);
		$maps_key = Settings::first()->maps_key;
				
		$orders = OrderItems::whereIn('orderItems.oid',$orders)
					->select(\DB::raw('o.id, o.lat, o.lng, if(o.direct = 1,d.postcode,dPostcode) as postcode ,group_concat(s.itemCode) as items'))
					->join('orders as o', 'orderItems.oid','=', 'o.id')
					->join('customers as c', 'o.cid', '=', 'c.id')
					->leftJoin('direct as d', 'o.id', '=', 'd.oid')
					->join('stock_items as s', 'orderItems.itemid', '=', 's.id')
					->groupBy('o.id')
					->get()
					->toArray();


		array_unshift($orders,["id" => 0, "lat" => '52.560458549420300' ,'lng' => '-2.061409374606000', 'postcode' => 'WV14 7HZ', 'items' => '0']);
		
		return view('deliveries.map',['orders' => $orders, 'route' => $route, 'maps_key' => $maps_key]);

	}



	public function api($function,$data)
	{
		if(method_exists($this,$function)){
			return $this->$function($data);
		}

		return 'Error';
	}



	public function geocode($data){

		return Coordinates::forPostcode($data);

	}




	/*
	 * @func pool
	 *
	 */
	public function pool(Request $request)
	  {
		
	  	$date = $request->has('d') ? $request->get('d') : date('d-M-Y',strtotime(date('d-M-Y'). "+ 1 days"));
	  	
		$maps_key = Settings::first()->maps_key;
		$not_allowed = implode(",",["'TNT brown'","'TNT Black'","'TNT'","'Bedtastic'","'Bed Express'","'Durest Beds'","'Cancelled'"]);
		$orders = OrderItems::where(\DB::raw("orderStatus='Allocated' and deliverBy='".$date."' and delRoute not in ($not_allowed) and processed=0  and (lat is not null or lat!='')
					or orderStatus='Allocated' and (delRoute='No Route' or delRoute='NO MSG' or delRoute='Delivery Charge') and processed=0 and (lat is not null or lat!='') "))
					->select(\DB::raw('o.id, o.lat, o.lng, if(o.direct = 1,d.postcode,dPostcode) as postcode ,group_concat(s.itemCode,":",Qty) as items,o.delRoute,o.companyName'))
					->join('orders as o', 'orderItems.oid','=', 'o.id')
					->join('customers as c', 'o.cid', '=', 'c.id')
					->leftJoin('direct as d', 'o.id', '=', 'd.oid')
					->join('stock_items as s', 'orderItems.itemid', '=', 's.id')
					->groupBy('o.id')
					->get()
					;//->toArray();

		$missing = OrderItems::where(\DB::raw("orderStatus='Allocated' and deliverBy='".$date."' and delRoute not in ($not_allowed) and processed=0 and (lat is null or lat='')
					or orderStatus='Allocated' and (delRoute='No Route' or delRoute='NO MSG' or delRoute='Delivery Charge') and processed=0 and (lat is null or lat='')"))
					->select(\DB::raw('o.id, o.lat, o.lng, if(o.direct = 1,d.postcode,dPostcode) as postcode ,group_concat(s.itemCode,":",Qty) as items,o.delRoute,o.companyName'))
					->join('orders as o', 'orderItems.oid','=', 'o.id')
					->join('customers as c', 'o.cid', '=', 'c.id')
					->leftJoin('direct as d', 'o.id', '=', 'd.oid')
					->join('stock_items as s', 'orderItems.itemid', '=', 's.id')
					->groupBy('o.id')
					->get()
					;//->toArray();

		$items =[];
		$quanities=[];
		foreach($orders as $order)
		{
			$a = explode(",",$order->items);
			$order->items = $a;
			foreach($a as $b)
			{
				$c = explode(":",$b);
				$items[$c[0]][]= $order->id;
				$quanities[$c[0]] = isset($quanities[$c[0]]) ? $quanities[$c[0]]+$c[1] : $c[1];
			}
		}

		foreach($missing as $order)
		{
			$a = explode(",",$order->items);
			$order->items = $a;
			foreach($a as $b)
			{
				$c = explode(":",$b);
				$items[$c[0]][]= $order->id;
				$quanities[$c[0]] = isset($quanities[$c[0]]) ? $quanities[$c[0]]+$c[1] : $c[1];
			}
		}

    $stock = StockItem::whereIn('itemCode',array_keys($items))
                        ->select('itemCode','actual')
                        ->get();
    $stockCount = [];
    foreach ($stock->toArray() as $s) {
        $stockCount [$s['itemCode']]=$s['actual'];
    }
									// sold					//actual
		return view('pool.index', ['date'=> $date, 'qty' => $quanities, 'stock' => $stockCount, 'items' => $items,'missing' => $missing, 'orders' => $orders, 'routes' => Routes::getRouteRoutes(),'maps_key' => $maps_key]);

    }


	public function getRow($data)
	{

		$row = OrderItems::where('o.id',$data)
				->select(\DB::raw("group_concat(s.cat) as cat,o.lat,o.lng,o.deliverBy,o.done,o.p, o.delOrder,o.startDate,o.companyName, o.orderType, o.direct, o.id,
		 		o.delRoute,o.dispatchType,if(o.direct=1,d.name,c.dBusinessName) as name, if(o.direct=1,d.postcode,c.dPostcode) as postcode, if(o.direct=1,d.town,c.dTown) as town,
		 		group_concat(s.itemCode,':',orderItems.Qty) as items,group_concat(s.actual) as stockCount"))
				->join('orders as o', 'orderItems.oid','=', 'o.id')
				->join('customers as c', 'o.cid', '=', 'c.id')
		 		->leftJoin('direct as d', 'o.id', '=', 'd.oid')
		 		->join('stock_items as s', 'orderItems.itemid', '=', 's.id')
		 		->groupBy('o.id')
				->first();
				
		return $row;
	}

}
