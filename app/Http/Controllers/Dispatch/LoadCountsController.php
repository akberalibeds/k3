<?php
namespace App\Http\Controllers\Dispatch;

use App\Warehouse;
use App\OrderItems;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class LoadCountsController extends Controller
{
  /**
   * @author Giomani Designs (Development Team)
   * @return \Illuminate\Http\Response
   */
  public function index() {
    $warehouses = Warehouse::all();
    $options = [];
    foreach ($warehouses as $warehouse) {
      $options[$warehouse->id] = $warehouse->name;
    }
    return view('deliveries.load-counts', ['warehouses' => $options]);
  }
  /**
   * @author Giomani Designs (Development Team)
   * @return \Illuminate\Http\Response
   */
  public function loadCountsDay($warehouse = null, $date = null) {
    if ($date === null) {
      $date = date('d-M-Y', strtotime(date('d-M-Y') . ' + 1 days'));
    } else {
      $date = date('d-M-Y', $date + 86400);
    }
    $loadCounts = OrderItems::getLoadCounts($date, $warehouse);
    $title = 'Load counts for ' . $date;
    if ($warehouse !== null) {
      $title .= ', warehouse ' . $warehouse;
    }
    return view('deliveries.load-counts-table', ['loadCounts' => $loadCounts, 'title' => $title]);
  }
  /**
   * @author Giomani Designs (Development Team)
   * @return \Illuminate\Http\Response
   */
  public function loadCountsMorning() {
    $date = date('d-M-Y', strtotime(date('d-M-Y') . ' + 1 days'));
    return view('deliveries.load-counts-table', ['title' => 'Morning Load counts for ' . $date, 'loadCounts' => OrderItems::getLoadCountsM($date)]);
  }
  /**
   * @author Giomani Designs (Development Team)
   * @return \Illuminate\Http\Response
   */
  public function missed() {
    $date = date('d-M-Y', strtotime(date('d-M-Y') . ' + 1 days'));
    return view('deliveries.missed-counts-table', ['title' => 'Missed parts count for ' . $date, 'loadCounts' => OrderItems::getMissedCounts($date)]);
  }
}
