<?php
// Giomani Designs (Development Team)
//
namespace App\Http\Controllers\Payments;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PaymentController extends Controller
{
  /**
   * @author Giomani Designs (Development Team)
   * @return \Illuminate\Http\Response
   */
  public function accepted()
  {
    return view('payments.accepted');
  }
  /**
   * @author Giomani Designs (Development Team)
   * @return \Illuminate\Http\Response
   */
  public function cancelled()
  {
    return view('payments.cancelled');
  }
  /**
   * @author Giomani Designs (Development Team)
   * @return \Illuminate\Http\Response
   */
  public function declined()
  {
    return view('payments.declined');
  }
  /**
   * @author Giomani Designs (Development Team)
   * @return \Illuminate\Http\Response
   */
  public function exception()
  {
    return view('payments.exception');
  }
  /**
   * @author Giomani Designs (Development Team)
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    return view('payments.index');
  }
}
