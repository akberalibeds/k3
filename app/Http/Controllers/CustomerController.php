<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Customer;

class CustomerController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index() {
		/*$orders = Order::join('customers', 'orders.cid', '=', 'customers.id')
  		->join('direct', 'orders.id', '=', 'direct.oid')
  		->select('orders.id', 'orders.iNo', 'customers.userID' , \DB::raw('if(orders.direct=1,direct.name,customers.dBusinessname) as name'),\DB::raw('if(orders.direct=1,direct.postcode,customers.dPostcode) as postcode'))
  		->orderBY('orders.id', 'desc')
  		->paginate(15);
*/
		return view('customer.index');
  }
  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create() {
    //
  }
  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request) {
    //
  }
  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function customer($id) {
    return json_encode(Customer::getCustomer($id));
  }
  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit($id) {
  }
  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function add(Request $request) {
    $data = json_decode($request->get('data'),true);
    return json_encode(Customer::insertIfNotExists($data)->getCustomer());
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id) {
    $data = json_decode($request->get('data'),true);
    return json_encode(Customer::updateCustomer($data,$id)->getCustomer());
	}
  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id) {
    //
  }
  public function searchCustomer($query)
  {
    // return Customer::where('dBusinessName', 'like', '%' . $query . '%')->orWhere('businessName', 'like', '%' . $query . '%')->get();
    if (1 !== preg_match('~[0-9]~', $query)) { //(ctype_alpha($query)) { /* NOT RECOGNISING NAME ALWAYS JUMPS TO ELSE */
      return Customer::nameSearch($query);
    } else {
      return Customer::postcodeSearch($query);
    }
    //return response()->json($customer);
  }
}
