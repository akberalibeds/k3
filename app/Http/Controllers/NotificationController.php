<?php

namespace App\Http\Controllers;

use Auth;
use App\User;
use Carbon\Carbon;
use App\Notification;
use App\NotificationMessage;
use App\Http\Requests;
use Illuminate\Http\Request;
// use Illuminate\Routing\Controller as BaseController;

class NotificationController extends Controller
{

    public $allowedTypes = [
        'App\Notifications\Types\DeliveryDateRequest',
        'App\Notifications\Types\DeliveryDateRequestReminder',
        'App\Notifications\Types\DispatchReady',
        'App\Notifications\Types\DeliveryImminent',
    ];

    //
    public function index($type = null) {
        request('date')? $date = Carbon::parse( request('date')) : $date = Carbon::today();
        // request('sentFlag')? $sent;
        // request('sentFlag');
        // dd($sentFlag);
        if (! is_null($type) ){    

            $type = "App\\Notifications\\Types\\{$type}";
            
            if(in_array($type, $this->allowedTypes)){

                $notificationList = Notification::where('type', $type)
                    ->whereDate('created_at', '=', $date->toDateString())
                    ->paginate(10);
            } else {
                return back();
            }
        } else {
            $notificationList = Notification::whereDate('created_at', '=', $date->toDateString())
                    ->paginate(10);
        }
        return view('notifications.index', [
            'notificationList' => $notificationList,
            'date' => $date,
            // 'hasSent'=> $sentFlag
        ]);
    }

    public function create(){

    }

    public function show($id){
        $notification = Notification::find($id);
        return view('notifications.show')->with('notification', $notification);
    }

    public function update($id){

    }

    public function delete($id){

    }

    public function sentFlag($flag)
    {
        request('date')? $date = Carbon::parse( request('date')) : $date = Carbon::today();
        $notificationList = Notification::whereDate('created_at', '=', $date)
            ->whereDoesntHave('messages', function ($query) use($flag) {
                $query->where('is_sent', ! $flag);
            });
                
        if($flag ){
            $notificationList ->has('messages');
        }
       
        return view('notifications.index', [
            'notificationList' => $notificationList->paginate(10),
            'date' => $date, 
        ]);
    }


    public function sentPartial()
    {
        request('date')? $date = Carbon::parse( request('date')) : $date = Carbon::today();

        $notificationList = Notification::whereDate('created_at', '=', $date->toDateString())
                ->whereHas('messages', function($q) {
                    $q->where('is_sent', true);
                })
                ->whereHas('messages', function($q) {
                    $q->where('is_sent', false);
                })
                ->paginate(10);
        
        return view('notifications.index', [
            'notificationList' => $notificationList,
            'date' => $date, 
        ]);
    }
}
