<?php

namespace App\Http\Controllers\Testing;

use Entrust;
use App\Order;
use App\Seller;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class TestingController extends Controller
{
  /**
   * Display the specified resource.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function index(Request $request) {
    if ($request->ajax()) {
      $orders = Order::getSeller($id, explode('|', $filter));
      return $orders;
    }

    return view('testing.index', ['sellerId' => 1]);
  }
  /**
   * Search for a seller
   *
   * @param  int  $id
   * @param  string  $subject
   * @param  string  $query
   * @return \Illuminate\Http\Response
   */
  public function search($id, $subject, $query, $filter = '') {
    $orders = Order::searchWholesale($id, $subject, $query, explode('|', $filter));

    return response()->json($orders);
  }
  /**
   * Display the specified resource.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show(Request $request, $id, $filter = '') {
    try {
      $seller = Seller::findOrFail($id);
    } catch (ModelNotFoundException $e) {
      return parent::notFound();
    }
    if ($request->ajax()) {
      $orders = Order::getSeller($id, explode('|', $filter));
      return $orders;
    }

    return view('testing.index', ['sellerId' => $id]);
  }
}
