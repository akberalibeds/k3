<?php
// @author Giomani Designs (Development Team)
//
namespace App\Http\Controllers\Message;

use App\User;
use App\Message;
use Illuminate\Http\Request;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;

class MessageController extends BaseController
{
  /**
   * @author Giomani Designs (Development Team)
   * @return \Illuminate\Http\Response
   */
  public function index () {
    $addressBook = json_encode(User::getMessageableUsers()->toArray());
    return view('message.index', ['addressBook' => $addressBook]);
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  integer $id the id of the conversation
   */
  public function read ($id) {
    $messages = Message::getConversationMessages($id);
    Message::markAsRead([$id]);
    return $messages;
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  integer  $id
   * @return \Illuminate\Http\Response
   */
  public function show ($id = null) {
    $messages = Message::getConversations();
    return $messages->toArray();
  }
  /**
   * @author Giomani Designs (Development Team)
   * @return \Illuminate\Http\Response
   */
  public function send (Request $request) {
    $messageBody = $request->input('message');
    $conversationId = $request->input('conversation');
    $subject = $request->input('subject');
    $toId = $request->input('to');
    Message::send($subject, $messageBody, $toId, $conversationId);
    return [];
  }
  
  /**
   * @author Giomani Designs (Development Team)
   * @param  integer  $id
   * @return \Illuminate\Http\Response
   */
  public function getNew () {
  	$messages = Message::getNew();
  	return $messages->toArray();
  }
  
  public function shownNew ($ids) {
  	$messages = Message::shownNew($ids);
  	return [];
  }
}
