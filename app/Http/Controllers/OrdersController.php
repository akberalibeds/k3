<?php

namespace App\Http\Controllers;

use Log;
use Auth;
use Time;
use Entrust;
use App\User;
use App\Links;
use App\Order;
use App\Codes;
use App\Count;
use App\Mailer;
use App\Status;
use App\Routes;
use App\CreditNote;
use App\Customer;
use App\Supplier;
use App\StockItem;
use App\OrderItems;
use App\OrderNotes;
use App\RouteStops;
use App\Coordinates;
use App\Http\Requests;
use App\CancelledOrder;
use App\WarehouseErrors;
use App\Helper\Ebay\Ebay;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Helper\Magento\Magento;
use App\Helper\Magento\Magento2;
use Yajra\Datatables\Datatables;
use App\Helper\Traits\OrderUtils;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\OutOfStock;
use App\Helper\SMS;
use App\DispatchedRoute;
use Symfony\Component\Console\Tests\CustomApplication;
use App\Helper\Barclays;
use App\Helper\BarclaysDirect;
use App\Helper\EbayLaravel\EbayHelper;
use App\Settings;
use App\Helper\CSVParser;

class OrdersController extends Controller
{
  use OrderUtils;
  /**
   * Cancel an Order.
   *
   * @return \Illuminate\Http\Response
   */
  public function cancel($id)
  {

     Order::cancelOrder($id);
  }
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    /* $orders = Order::join('customers', 'orders.cid', '=', 'customers.id')
    ->join('direct', 'orders.id', '=', 'direct.oid')
    ->select('orders.id', 'orders.iNo', 'customers.userID' , \DB::raw('if(orders.direct=1,direct.name,customers.dBusinessname) as name'),\DB::raw('if(orders.direct=1,direct.postcode,customers.dPostcode) as postcode'))
    ->orderBY('orders.id', 'desc')
    ->paginate(15);
    */

	$b = Entrust::hasRole('sys_admin') ? 1 : 0;
    return view('orders.index',['admin' => $b ]);
  }
  
  public function list_orders()
  {
  	$b = Entrust::hasRole('sys_admin') ? 1 : 0;
  	return view('orders.index2',['admin' => $b ]);
  }
  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit($id)
  {
      //
	   $order = Order::edit($id);

	   return view('orders.edit', $order);
  }

    public function save(Request $request, $id)
  {
      //
	   return $order = Order::saveOrder($request, $id);

	   //return view('orders.edit', $order);
  }
  /*
   * CHANGE STATUS
   * Seperate function to change status for extra functions;
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   */
  public function changestatus(Request $request, $id) {
    $order = json_decode($request->get('data'), true);
    Order::changestatus($order, $id);
  }

	/*
	* CHANGE ROUTE
	* Seperate function to change route for extra functions;
	* @param  \Illuminate\Http\Request  $request
    * @param  int  $id
	*/
	public function changeroute(Request $request, $id) {

		$data = json_decode($request->get('data'),true);
        Order::changeroute($data,$id);

	}

	/*
	* add payment
	* Seperate function to change route for extra functions;
	* @param  \Illuminate\Http\Request  $request
    * @param  int  $id
	*/
	public function addpayment(Request $request, $id){

		$data = json_decode($request->get('data'),true);
       	return Order::addpayment($data,$id);

	}

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    //
  }


  /* @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function order($id)
  {
    $data = Order::order($id);
    if ($data) {
      return view('orders.order',[
       'order'=> $data['order'],
       'status'=>$data['status'],
       'routes'=>$data['routes'],
       'suppliers'=>$data['suppliers'],
       'codes'=>$data['codes'],
       'paymentType' => $data['paymentType'],
       'payments' => $data['payments'],
       'visits' => $data['visits'],
     ]);
    } else {
      return 'No order found';
    }
 }
/**
  * @author Giomani Designs
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
 public function order2 ($id) {
   $order = Order::getOrder($id);

   return view('orders.order2', $order);
 }
 
 public function search2(Request $request){
 
 	
 	return Order::search2($request);
 	/*
 		$r = Order::join('customers','orders.cid','=','customers.id')
 					->select(
 							'orders.id',
 							'orders.iNo',
 							'customers.userID',
 							'customers.dBusinessname',
 							'customers.dPostcode',
 							'ouref',
 							'orderType',
 							'orderStatus',
 							'startDate',
 							'total',
 							\DB::raw('total-paid as owed'),
 							'orders.done'
 							)
 					->orderBY('orders.id','desc');
 
 		return Datatables::of($r)->make(true);
 		
 */
 }

		/*					/*
		* Get Orders List	/*
		* 					/*
		/*					*/

	public function search(Request $request){

		/*
		$r = Order::join('customers','orders.cid','=','customers.id')
         ->select(array('orders.id','orders.iNo','customers.userID','customers.dBusinessname','customers.dPostcode'))
		 ->orderBY('orders.id','desc');

		return Datatables::of($r)->make(true);
		*/
		
		$data = Order::search($request);
		return json_encode(array("orders"=>$data['orders'],"pages"=>$data['pages'],"search"=>$data['search']));

	}
  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
    //
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function makePriority(Request $request, $id) {
    if (!Entrust::can('order_priority')) {
      return parent::notAuthorised($request);
    }
    Order::makePriority($id);
  }
   /**
   * @author Giomani Designs (Development Team)
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function printLabel($id) {
    $routeLabels = Order::getLabelsByIds([$id]);
    $pdf = new \App\Helper\Pdfs\PdfLabel();
    foreach ($routeLabels as $routeLabel) {
      $pdf->label($routeLabel);
    }
    
    return $pdf->outPdf();
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function printLabels(Request $request) {
    if (!Entrust::can('post_admin')) {
      return parent::notAuthorised($request);
    }
    $ids = explode('|', $request->input('ids'));
    Order::whereIn('id', $ids)->update(['post' => 0]);
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function removePost(Request $request) {
    if (!Entrust::can('post_admin')) {
      return parent::notAuthorised($request);
    }
    $ids = explode('|', $request->input('ids'));
    Order::whereIn('id', $ids)->update(['post' => 0]);
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function setRoute(Request $request, $id) {
    try {
      $order = Order::findOrFail($id);
      $oldRoute = $order->delRoute . '(' . $order->id_delivery_route . ')';
      $customer = Customer::findOrFail($order->cid);
    } catch (ModelNotFoundException $e) {
      return parent::notFound();
    }
    $postcode = trim($customer->dPostcode);
    $route = RouteStops::getByPostcodeArea($postcode);
    if ($route->count() === 0) {
      return parent::notFound('postcode');
    }
    $route = $route[0];
    $coords = Coordinates::forPostcode($postcode, true);
    $data=[];
    $order->lat = (isset($coords['lat'])) ? $coords['lat'] : '';
    $order->lng = (isset($coords['lng'])) ? $coords['lng'] : '';

    $order->delRoute = $route['name'];
    $order->id_delivery_route = $route['id'];
    $order->save();
    OrderNotes::addNote('Set route by Postcode (' . $postcode . ') from ' . $oldRoute . ' to ' . $route['name'] . '(' . $order->id_delivery_route . ')', $id);
    return ['result' => 'success'];
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function showAfterSales(Request $request) {
    if ($request->ajax()) {
      $query = $request->all();
      return Order::getAfterSales($query);
    }
    $suppliers = Supplier::orderBy('supplier_name')->get();
    return view('orders.after-sales', ['suppliers' => $suppliers]);
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function showMissedParts(Request $request) {
  	if ($request->ajax()) {
  		$query = $request->all();
  		return Order::missed_part_orders($query);
  	}
  	
  	return view('orders.missed-parts');
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function showCancelled(Request $request) {
    if ($request->ajax()) {
      $query = $request->all();
      return CancelledOrder::getCancellled($query);
    }
    return view('orders.cancelled');
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function showCollect(Request $request) {
    if ($request->ajax()) {
      $query = $request->all();
      return Order::getCollect($query);
    }
    return view('orders.collect');
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function showCollected(Request $request) {
    if ($request->ajax()) {
      $query = $request->all();
      return Order::getCollected($query);
    }
    return view('orders.collected');
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function showEscalations(Request $request) {
    if ($request->ajax()) {
      $query = $request->all();
      return Order::getEscalations($query);
    }
    return view('orders.escalations');
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function showFeedback(Request $request) {
    if ($request->ajax()) {
      $query = $request->all();
      return Order::getFeedback($query);
    }
    return view('orders.feedback');
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function showNotLoaded(Request $request) {
    if ($request->ajax()) {
      $query = $request->all();
      return Order::getNotLoaded($query);
    }
    return view('orders.not-loaded');
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function showDelIssue(Request $request) {
  	if ($request->ajax()) {
  		$query = $request->all();
  		return Order::getDelIssue($query);
  	}
  	return view('orders.del-issue');
  }
  /**
  * @author Giomani Designs (Development Team)
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function showOnHold(Request $request) {
    if ($request->ajax()) {
      $query = $request->all();
      return Order::getOnHold($query);
    }
    return view('orders.on-hold');
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function showPayPal(Request $request) {
    if ($request->ajax()) {
      $query = $request->all();
      return Order::getPayPal($query);
    }
    return view('orders.paypal');
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function showPost(Request $request) {
    if ($request->ajax()) {
      $query = $request->all();
      return Order::getPost($query);
    }
    return view('orders.post');
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function showRefunds(Request $request) {
    if ($request->ajax()) {
      $query = $request->all();
      return Order::getRefunds($query);
    }
    return view('orders.refunds');
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function showRetention(Request $request) {
    if ($request->ajax()) {
      $query = $request->all();
      return Order::getRetention($query);
    }
    return view('orders.retention');
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function showTuffnells(Request $request) {
    if ($request->ajax()) {
      $query = $request->all();
      return Order::getTuffnells($query);
    }
    return view('orders.tuffnells');
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function showUrgent(Request $request) {
    if ($request->ajax()) {
      $query = $request->all();
      return Order::getUrgent($query);
    }
    return view('orders.urgent');
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function showWarehouseErrors(Request $request) {
    if ($request->ajax()) {
      $query = $request->all();
      return Order::getWarehouseErrors($query);
    }
    return view('orders.warehouse-errors');
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function warehouseErrorsRemove(Request $request) {
  	 
  	$ids = explode("|",$request->get('ids'));
  	
  	Order::whereIn('id',$ids)->update(['wes' => 0]);
  	$wes = WarehouseErrors::whereIn('oid',$ids)->get();
  	foreach ($wes as $w){
  		$w->delete();
  	}
  	
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function warehouseErrorsCSV(Request $request) {
  	
  	$ids = explode("|",$request->get('ids'));
  	$orders = Order::queryForWarehouseErrors()->whereIn('orders.id',$ids)->get();
  	
  	$filename = "errors-".date('d-M-Y_H:i:s').".csv";
  	$handle = fopen(storage_path($filename), 'w+');
  	fputcsv($handle, array('Order', 'Delivery', 'Route', 'Postcode', 'Driver', 'Loader' , 'Picker', 'Items', 'Note'));
  	
  	foreach($orders as $row) {
  		fputcsv($handle, array($row->oid, $row->deliverBy, $row->name, $row->dPostcode,$row->driver,$row->loader,$row->picker,$row->item,$row->note));
  	}
  	
  	fclose($handle);
  	
  	$headers = array(
  			'Content-Type' => 'text/csv',
  	);
  	
  	return \Illuminate\Support\Facades\Response::download(storage_path($filename), $filename, $headers);
  	
  	
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function showWholesaleErrors(Request $request) {
    if ($request->ajax()) {
      $query = $request->all();
      return Order::getWholesaleErrors($query);
    }
    return view('orders.wholesale-errors');
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  \Illuminate\Http\Request  $request
   * @param  integer                   $id
   * @return \Illuminate\Http\Response
   */
  public function unProcess(Request $request, $id) {
    Order::setUnProcessed($id);
    OrderNotes::addNote('Order Un-Processed', $id);
    Event::fire(new OrderUnProcessing($id));
  }
  /**
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id) {
    $order = json_decode($request->get('data'), true);
    Order::updateOrder($order, $id);
    return [$id, $order];
  }


  public function invoice($id)
  {
	  	$order = Order::order($id);
		$order = $order['order'];
		Order::invoice([$order]);

  }

  public function refund(Request $request, $id){

    $this->validate($request, [
      'requestedBy' => 'required',
      'authorisedBy' => 'required',
      'refundReason' => 'required'
    ]);

    $requestedBy = User::where('email', $request->requestedBy)->where('status', 1)->first()->id;
    $authorisedBy = User::where('email', $request->authorisedBy)->where('status', 1)->first()->id; 

    Order::applyRefund($id, [
      'requested_by' => $requestedBy,
      'authorised_by' => $authorisedBy,
      'action_type' => 'refund',
      'reason' => $request->refundReason
    ]);
  }

   public function discount(Request $request, $id){
     
    $this->validate($request, [
      'requestedBy' => 'required',
      'authorisedBy' => 'required',
      'discountReason' => 'required'
    ]);

    $requestedBy = User::where('email', $request->requestedBy)->where('status', 1)->first()->id;
    $authorisedBy = User::where('email', $request->authorisedBy)->where('status', 1)->first()->id; 

    Order::addDiscount($request, $id, [
      'requested_by' => $requestedBy,
      'authorised_by' => $authorisedBy,
      'action_type' => 'discount',
      'reason' => $request->discountReason
    ]);
  }


  public function createPost(Request $request, $id){

	 return Order::createPost($request, $id);

  }

  public function missedParts(Request $request, $id){

	 return Order::missedParts($request, $id);

  }

  public function moveSupplier(Request $request, $id){
     return Supplier::moveSupplier($request, $id);
  }

   public function resendLink($id){

	 Links::resendLink($id);

  }


   public function genBarcode($id){

	 	Order::setBarcode($id);

  }



	public function exchange(Request $request, $id){

		Order::exchange($request,$id);

	}

	
	public function splitSku($sku){
		
		$parts = explode('-',$sku);
		if(count($parts)==3){
		    return false;
		}
		switch(strtolower($parts[5])){
				
			case 'base':
				//320-Size-3FT-Storage-NOSTO-Base-REDL
				$new_sku = "$parts[0]-$parts[2]-$parts[4]-$parts[6]";
				return $new_sku;
				break;
			case 'headboard':
				$new_sku = "$parts[0]-$parts[2]-$parts[4]";
				$new_sku .= ($parts[6]=="NOHED") ? "/$parts[0]-$parts[2]-MATTRESS" : "/$parts[0]-$parts[2]-MATTRESS/$parts[0]-$parts[2]-HEADBOARD-$parts[6]" ;
				return $new_sku;
				break;
			case 'colour':
				//335-Size-6FT-Storage-NOSTO-Colour-SILVER
				$new_sku = "$parts[0]-$parts[2]-$parts[4]-$parts[6]/$parts[0]-$parts[2]-$parts[6]-MATTRESS/$parts[0]-$parts[2]-HEADBOARD-$parts[6]";
				return $new_sku;
			default:
				return 'error '.$sku;
		}
		
	}
	
	private function shortenColor($str){
	    //$str = str_replace("HEADBOARD", "HDBD", $str);
	    $str = str_replace("CHAMPAGNE", "CPGN", $str);
	   // $str = str_replace("BLACK", "BLK", $str);
	  //  $str = str_replace("WHITE", "WHT", $str);
	  //  $str = str_replace("BROWN", "BRW", $str);
	    $str = str_replace("SILVER", "SLVR", $str);
	  //  $str = str_replace("GREY", "GRY", $str);
	    return $str;
	    
	}
	
	
	private function shortenHB($str){
	    
	    $str = str_replace("HEADBOARD", "HDBD", $str);
	  
	    return $str;
	    
	}
	
	public function magento(Request $request)
	{
        
	   set_time_limit(0);
	   
	   $m = new Magento2();
	   $m->getOrders();
	   echo "<br><br>";
	   foreach($m->orders as $o){
	       $p = (array) $o;
	       echo $p['payment']->lastTransId;
	       echo "<br>";
	   }
	   dd($m->orders);
	   return;
	   /*
	   $i = 0;
	   while($i<130000){
	       $orders = Order::select("id","total","paid",\DB::raw("(select sum(lineTotal) from orderItems where oid=orders.id) as itemTotal"))
	       ->whereRaw("(select sum(lineTotal) from orderItems where oid=orders.id)>0 and total=0 and Date(str_to_date(endDate,'%d-%b-%Y')) between '2017-07-01' and '2017-09-30' and orderstatus='Delivered' and paymentType='cash'")
	       ->orderByRaw('RAND()')
	       ->limit(100)
	       ->get();
	       
	       foreach ($orders as $order){
	          $i+=$order->itemTotal;
	          $o = Order::find($order->id);
	          $o->total=$order->itemTotal;
	          $o->paid=$order->itemTotal;
	          $o->save();
	          if($i>130000){
	              break;
	          }
	       }
	   }
	   	*/
		/*
		//$sku = \DB::select(\DB::raw("select * from stock_items where itemCode like '3%' and itemCode not like '31%' and itemCode not like '3F%'  and itemCode not like '3PK%' and itemCode not like '322%' and itemCode not like '321%' and itemCode not like '370%'  and itemCode not like '30%' or itemCode like '300%'"));
		$sku = \DB::select(\DB::raw("select * from stock_items where itemCode like '315%' or itemCode like '305%' or itemCode like '310%' or itemCode like 'D3%'"));
		$rows =[['old','new']];
		$cat = [0 => 'Adam Divans',1 => 'Adam Mattresses', 2 => 'Adam Headboards'];
		$catId = [0 => 32 ,1 => 33, 2 => 34];
		
		foreach($sku as &$s){
			//echo $s->itemCode;
// 			echo "<br>";
            
		    if(in_array($s->category_id,[32,33,34])) { continue; }
		    $new_sku = $this->splitSku($s->itemCode);
		    $new_sku = $this->shortenHB($new_sku);
			 if(strlen($new_sku)>64){
		        $new_sku = $this->shortenColor($new_sku);
		      }
			
			$new_sku = explode("/",$new_sku);
			
			$i=0;
			foreach ($new_sku as $ns){
			    
			    StockItem::where('itemCode',$ns)->delete();
			    
			    $stockItem = new StockItem();
			   
			    $stockItem->itemCode = $ns;
			    $stockItem->itemQty = 1000;
			    $stockItem->needs_attention = 0;
			    $stockItem->actual = 1000;
			    $stockItem->isMulti = 1;
			    $stockItem->warehouse = 3;
			    $stockItem->label = 1;
			    $stockItem->blocked = 1;
			    $stockItem->seller = 1;
			    
			    $stockItem->cat = $cat[$i];
			    $stockItem->category_id = $catId[$i];
			    
        			$stockItem->retail = ($i==0) ? $s->retail : 0.00;
        			$stockItem->wholesale = ($i==0) ? $s->wholesale : 0.00;
        			$stockItem->pieces = ($i==0) ? 2 : 1;
        			$stockItem->cost = ($i==0) ? $s->cost : 0.00;
        			$stockItem->itemDescription = ($i==0) ?  $s->itemDescription : $ns;
        			$stockItem->save();
                
			    $i++;
			    
			}
			$rows[]=[$s->itemCode,implode("/",$new_sku)];
// 			echo "<br><br>";
		}
		
		
	
		header('Content-Type: text/csv; charset=utf-8');
		header('Content-Disposition: attachment; filename=data.csv');
		
		// create a file pointer connected to the output stream
		$output = fopen('php://output', 'w');
		
		// output the column headings
		foreach($rows as $row){
		  fputcsv($output, $row);
		}
		
		return $output;
		
		
		/*
		ini_set('memory_limit', '1024M');
		
		$ebay = new Ebay;
		
			$orders =  $ebay->get_orders("2017-08-25 09:00:00","2017-08-25 15:25:00")->orders;
			$data = ['missing' => [],'exists' => []];
			foreach($orders as $order){
				
				$exists = Order::where('ebayID',$order->OrderID)->first();
				
				if(!$exists){
					$data['missing'][]=$order->OrderID;
				}
				else if($exists->cid==0){
					
					$address = $order->ShippingAddress;
					$transaction = isset($order->TransactionArray->Transaction->Buyer) ? $order->TransactionArray->Transaction : $order->TransactionArray->Transaction[0];
					
					$customer=[
								'userID' 		=> (string)$order->BuyerUserID,
								'tel'			=> (string)$address->Phone,
								'mob'			=> (string)$address->Phone,
								'email1'		=> (string)$transaction->Buyer->Email,
								'businessName' 	=> (string)$address->Name,
								'number'		=> '',
								'street'		=> (string)$address->Street1 . ' ' . (string)$address->Street2,
								'town'	 		=> (string)$address->CityName,
								'postcode'		=> (string)$address->PostalCode,
								'dBusinessName' => (string)$address->Name,
								'dNumber'		=> '',
								'dStreet'		=> (string)$address->Street1 . ' ' . (string)$address->Street2,
								'dTown'	 		=> (string)$address->CityName,
								'dPostcode'		=> (string)$address->PostalCode,
							  ];
					
					$cid = Customer::insertIfNotExists($customer)->getCustomer()->id;
					$data['exists'][]=[$exists->id,$customer,$cid];
					$exists->cid = $cid;
					$exists->save();
					
				}
			}
	
			
			return $data;
			*/
	}


  /*
   * Add Warehouse Errors
   * @param  \Illuminate\Http\Request  $request
   */
	public function wes(Request $request,$id) {

		$data = $request->get('data');
		$error = new WarehouseErrors;
		$error->oid=$id;
		$error->item = $data['item'];
		$error->note = $data['note'];
		$error->save();

	}

	public function api_change_status($orderID,$oldstatus,$newstatus){

		 return $order = ['new' => $newstatus, 'old' => $oldstatus , 'id' => $orderID];
		 Order::changestatus($order,$orderID);

	}
	
	public static function creditOrder($id){
		return CreditNote::creditOrder($id);
	}
	
	public static function getCredits($id){
		return CreditNote::getCredits($id);
	}
	
	public static function useCredits($order,$id){
		return CreditNote::useCredits($order,$id);
	}
	
	public function missedPartList(){
		return Order::missed_part_orders();
	}

	public function postManifest($ids){
		return Order::postManifest($ids);
	}
	
	public function unconfirmed(Request $request) {
	    if ($request->ajax()) {
	      $query = $request->all();
	      return Order::unconfirmed($query);
	    }
		return view('orders.unconfirmed');
	}
	
	public function getDispatchedRoutes(){
		
		return DispatchedRoute::where('status','Routed')->where('date','!=',date('d-M-Y'))->get();
		
	}
	
	public function addToDispatchedRoute(Request $request,$id){
	
		return DispatchedRoute::addToRoute(json_decode($request->get('data')),$id);
	
	}
	
	public function linked($id){
	    return Order::select('orders.id','total',\DB::raw("group_concat(itemCode) as itemCode"))
	               ->join('orderItems','orders.id','=','orderItems.oid')
	               ->join('stock_items','orderItems.itemid','=','stock_items.id')
	               ->where('orders.code',$id)
	               ->where('orderStatus','Allocated')
	               ->groupBy('orders.id')
	               ->get();
	}
}
