<?php

namespace App\Http\Controllers;

use Entrust;
use App\Queries\DuplicateOrder;

class DuplicateOrdersController extends Controller
{
    protected $duplicate;

    public function __construct(DuplicateOrder $duplicate)
    {
        $this->middleware('auth');
        $this->duplicate = $duplicate;
    }

    /**
     * return duplicate orders
     * 
     */
    public function index()
    {
        return view('orders.duplicates.index', [
            'duplicates' => $this->duplicate->all(),
        ]);
    }

}
