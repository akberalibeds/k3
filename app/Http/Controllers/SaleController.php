<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use Sale;
use Deliveries;
use App\Order;
use App\Customer;
use App\Company;
use App\Status;
use App\Routes;
use App\Codes;
use App\Supplier;
use App\OrderItems;
use App\OrderNotes;
use App\Http\Requests;
use App\PaymentType;
use Yajra\Datatables\Datatables;

class SaleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {

		$sale = Sale::getData()->toArray();
		$companies = Company::find($id);
		$paymentType = PaymentType::getAll();
		
  		return view('sale.index',['SALE' => $sale, 'companies' => $companies, 'paymentType' => $paymentType]);

	}



	public function company(){

		$companies = Company::all();
		return view('sale.company',['companies' => $companies]);

	}



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function create()
    {
        //

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

		$id =  Sale::newSale(json_decode($request->get('order')))
				->manualOrder()
				->addItems()
				->setIno()
				->setDeliveryRoute()
				->setBarcode()
				->check_split()
				->id;

		return $id;
  }
}
