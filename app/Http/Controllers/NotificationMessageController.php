<?php

namespace App\Http\Controllers;

use Auth;
use Carbon\Carbon;
use App\Http\Requests;
use App\NotificationMessage;
use Illuminate\Http\Request;

class NotificationMessageController extends Controller
{
    //
    public function resend(NotificationMessage $message)
    {
        
        $response = (new $message->msgMethod)->send($message);
        
        // New Version of update
        if($response['sent']) {

            $message->update([
                'is_sent' => true,
                'date_sent' => Carbon::now(),
            ]);

            $message->notification->update([
                'hasSent' => 1,
            ]);
            
        } else {
            $message->update([
                'err_message' => $response['errors'],
                'date_sent' => Carbon::now(),
            ]);
        }
        
        return back();
    }
}
