<?php

namespace App\Http\Controllers\ManufacturingError;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\ManufacturingError;
use App\MErrorNotes;

class ManufacturingErrorController extends Controller
{
    //
    
	public function index(Request $request){
		
		if($request->ajax()){
			return ManufacturingError::getAll($request->all());
		}
		return view('manufacturing-error.index',[]);
		
	}
	
	
	public function saveError(Request $request){
		
		return $error = ManufacturingError::addError($request);
		
	}
	
	public function upload(Request $request){
		return ManufacturingError::upload($request);
	}
	
	public function addNote(Request $request){
		MErrorNotes::addNote($request);
	 	$e = ManufacturingError::find($request->get('id'));
		$e->cost+=$request->get('cost');
		$e->save();
	}
	
	
	public function show($id){
		//return ManufacturingError::getError($id);
		return view('manufacturing-error.show',ManufacturingError::getError($id));

	}
	
}
