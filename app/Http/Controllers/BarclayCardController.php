<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Customer;
use App\EpdqOrders;
use App\Helper\BarclaysDirect;

class BarclayCardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        
    	$request->all();
        
    	$payment = new BarclaysDirect();
    	
    	/*
    	 * Auth Data
    	 */
    	$payment->setPSPID("epdq6009357");
    	$payment->setUSERID("APIGD1113");
    	$payment->setPSWD("Giomani786tech,");
    	$payment->setShaIn("zkJNUgqf2anTUxwTm9WLPdqq86");
    	
    	
    	$customer = Customer::find($request->get('cid'));
    	
    	/*
    	 * Customer Details
    	 */
    	$payment->setCustomerName($customer->businessName);
    	$payment->setAddress($customer->number." ".$customer->street);
    	$payment->setTown($customer->town);
    	$payment->setCountry("England");
    	$payment->setPostcode($customer->postcode);
    	$payment->setTel($customer->tel);
    	$payment->setEMAIL($customer->email1);
    	
    	/*
    	 * Order Details
    	 */
    	
    	$epdqOrder = new EpdqOrders();
    	$epdqOrder->save();
    	
    	$payment->setORDERID('Order-'.$epdqOrder->id);
    	$payment->setAMOUNT($request->get('paid'));
    	$payment->setOPERATION("SAL");
    	
    	/*
    	 *Card Data
    	 */
    	//$payment->setBRAND("MasterCard");
    	//$payment->setPM("CreditCard");
    	$payment->setCARDNO($request->get('cardNo'));
    	$payment->setCVC($request->get('cvc'));
    	$payment->setED($request->get('exp'));
    	
    	$result = $payment->send();
    	$epdqOrder->result = json_encode($result);
    	$epdqOrder->save();
    	
    	$result['epdqId'] = $epdqOrder->id;
    	
    	return $result;
    	
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

  
    
    
    
}




