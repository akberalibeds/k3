<?php

namespace App\Http\Controllers;

set_time_limit(720);

use App\Order;
use App\Routes;
use App\Message;
use App\Dispatched;
use App\OrderNotes;
use App\Http\Requests;
use Illuminate\Http\Request;

class ReviewRouteController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'permission:markDeliveries']);
    }
    
    public function index()
    {
        request('date')? $date = (new \DateTime( request('date')))->format('d-M-Y') : $date  = \Carbon\Carbon::today()->format('d-M-Y');
        
        $routes = Routes::whereHas('dispatched', function($query) use ($date) {
            $query->where('name', 'LIKE', 'ROUTE%')
                  ->where('date', '=', $date);
        })->get();
        
        return view('review-routes.index', compact('routes', 'date'));
    }

    public function show(Routes $route, $date = NULL)
    {
        $date? $date = (new \DateTime($date))->format('d-M-Y') : $date  = \Carbon\Carbon::today()->format('d-M-Y');

        $data = Dispatched::getRouteOrders($route->id, $date, $route->name);
        
        return view('review-routes.show', compact('route', 'data', 'date'));
    }

    public function store(Request $request) 
    {
        $data = $request->deliveredStatus;

        $notes = $request->notes;

        if(! isset($data)) return back();

        if( isset($notes)) $this->addNotes($notes);

        $delivered_ids = array_keys(array_filter($data, function($value) {
            return $value == 'Yes';
        }));

        $undelivered_ids = array_keys(array_filter($data, function($value) {
            return $value == 'No';
        }));

        $this->updateStatus($delivered_ids, 'Delivered');
        $this->updateStatus($undelivered_ids, 'Allocated');

        $deliveredList = $this->getStockInfo($delivered_ids);
        $undeliveredList = $this->getStockInfo($undelivered_ids);
        
        $this->generateCsv($deliveredList, ['Item Code', 'Part', 'Total'], 'delivered');
        $this->generateCsv($undeliveredList, ['Item Code', 'Part', 'Total'], 'undelivered');

        return back();
    }

    public function downloadCsv()
    {
        return response()->download(storage_path('csv/reviewOrders/' . request('name'). '.csv' ));
    }

    private function addNotes( array $notes) {

        foreach ($notes as $oid => $note) {
            if ($note) {
                OrderNotes::addNote($note, $oid);
            }
        }
    }

    private function updateStatus($order_ids, $to_status)
    {
        foreach ($order_ids as $id) {
            Order::changestatus([
                'old' => Order::find($id)->orderStatus,
                'new' => $to_status,
            ], $id);
        }
    }

    private function getStockInfo($order_ids)
    {
       return  Order::select('stock_items.itemCode', 'stock_parts.part', \DB::raw('SUM(orderItems.Qty) as total'))
                    ->join('orderItems', 'orders.id', '=', 'orderItems.oid')
                    ->join('stock_items', 'orderItems.itemid', '=', 'stock_items.id')
                    ->leftJoin('stock_parts', 'stock_items.id', '=', 'stock_parts.parent_id')
                    ->whereIn('orders.id', $order_ids)
                    ->groupBy('stock_items.itemCode', 'stock_parts.part')
                    ->orderBy('itemCode', 'part')
                    ->get()
                    ->toArray();
    }

    private function generateCsv($list, $columns, $name)
    {
        if(! $list) return ;

        $file_name = $name. '-' . time();

        $handle = fopen(storage_path("csv/reviewOrders/{$file_name}.csv"), 'w');

        fputcsv($handle, $columns);

        foreach ($list as $item) {
            fputcsv($handle, $item);
        }

        fclose($handle);

        Message::send(
            'The csv is ready for download',
            "<a href='/review/routes/download/csv?name={$file_name}'> Download {$name} CSV </a>",
            auth()->user()->id,
            0
        );
    }

}
