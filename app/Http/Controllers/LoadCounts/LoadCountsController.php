<?php
namespace App\Http\Controllers\LoadCounts;

use Validator;
use App\Warehouse;
use App\OrderItems;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Helper\Traits\OutHeaders;
use App\Helper\Pdfs\PdfTable;
use App\Http\Controllers\Controller;
use App\Exceptions\ValidationException;

class LoadCountsController extends Controller
{
  use OutHeaders;

  private $titles = [
    'Error',
    'Day Load Counts',
    'Morning Load Counts',
    'Missed',
  ];
  /**
   * @var
   */
  private $rules = [
    'type' => 'required|in:1,2,3',
    'date' => 'required|date_format:d-M-Y',
    'warehouse' => 'required|numeric|exists:warehouses,id',
    'stock-part' => 'accepted',
  ];
  /**
   * @author Giomani Designs (Development Team)
   * TODO: needs data validation
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function csvCounts (Request $request, $type, $date, $warehouse) {
    $parts = $request->query('parts');

    $counts = $this->getCounts($type, $date, $warehouse, $parts);
    $counts = $counts->get();

    $rows = [];
    $parts == 'true'? $title = $this->titles[$type] . ($warehouse === 'all' ? ', all warehouses' : ', warehouse ' . $warehouse) . '-Stock_Parts' : $title = $this->titles[$type] . ($warehouse === 'all' ? ', all warehouses' : ', warehouse ' . $warehouse);

    
    $this->outCsvHeaders($title);
    $out = fopen('php://output', 'w');
    if($parts == 'true'){
      if($warehouse == 'per'){
        if($type == '3'){
          fputcsv($out, ['Item Code', 'Stock Part ID', 'Part Boxes', 'Warehouse', 'Quantity Count','Order ID', 'Notes']);
        } else {
          fputcsv($out, ['Item Code', 'Stock Part ID', 'Part Boxes', 'Warehouse', 'Quantity Count']);
        }
      } else {
        if($type == '3'){
          fputcsv($out, ['Item Code', 'Stock Part ID', 'Part Boxes', 'Quantity Count', 'Order ID', 'Notes']);
        } else {
          fputcsv($out, ['Item Code', 'Stock Part ID', 'Part Boxes', 'Quantity Count']);
        }
      }
    } else {
      if($warehouse == 'per')  {
        if($type == '3'){
          fputcsv($out, ['Item Code', 'Warehouse', 'Quantity Count', 'Order ID', 'Notes']);
        } else {
          fputcsv($out, ['Item Code', 'Warehouse', 'Quantity Count']);
        }
      } else {
        if($type == '3'){
          fputcsv($out, ['Item Code', 'Quantity Count', 'Order ID', 'Notes']);
        } else {
          fputcsv($out, ['Item Code', 'Quantity Count']);
        }
      }
    }
    
    foreach ($counts as $count) {
      if($parts == 'true'){
        if($warehouse == 'per'){
          if($type == '3'){
            fputcsv($out, [
              $count['itemCode'] , 
              $count['id'], 
              // ' Part ' . ($i + 1) . ' of ' . $count['pieces'],
              $count['part'],
              $count['warehouse'],
              $count['tot'],
              $count['oid'],
              $count['notes']
            ]);
          } else {
            fputcsv($out, [
              $count['itemCode'] , 
              $count['id'], 
              // ' Part ' . ($i + 1) . ' of ' . $count['pieces'],
              $count['part'],
              $count['warehouse'],
              $count['tot']
            ]);
          }
        } else {
          if($type == '3'){
            fputcsv($out, [
              $count['itemCode'] , 
              $count['id'], 
              // ' Part ' . ($i + 1) . ' of ' . $count['pieces'],
              $count['part'], 
              $count['tot'],
              $count['oid'],
              $count['notes']
            ]);
          } else {
            fputcsv($out, [
              $count['itemCode'] , 
              $count['id'], 
              // ' Part ' . ($i + 1) . ' of ' . $count['pieces'],
              $count['part'], 
              $count['tot']
            ]);
          }
        }
      } else{
        if($warehouse == 'per'){
          if($type == '3'){
            fputcsv($out, [
              $count['itemCode'],
              $count['warehouse'], 
              $count['tot'],
              $count['oid'],
              $count['notes']
            ]);
          } else {
            fputcsv($out, [
              $count['itemCode'],
              $count['warehouse'], 
              $count['tot']
            ]);
          }
        } else {
          if($type == '3'){
            fputcsv($out, [
              $count['itemCode'],
              $count['tot'],
              $count['oid'],
              $count['notes']
            ]);
          } else {
            fputcsv($out, [
              $count['itemCode'],
              $count['tot']
            ]);
          }
        }
      }
      
    }
    fclose($out);
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  integer  $type       1 => 'Day Load Counts'
   *                              2 => 'Morning Load Counts'
   *                              3 => 'Missed'
   * @param  integer  $date       date in format dd-mmm-yyyy
   * @param  integer  $warehouse
   * @return array
   * @throws App\Exceptions\ValidationException
   */
  private function getCounts ($type, $date, $warehouse, $parts) {
    switch ($type) {
      case 1:
        $counts = $this->loadCountsDay($date, $warehouse, $parts);
        break;
      case 2:
        $counts = $this->loadCountsMorning($date, $warehouse, $parts);
        break;
      case 3:
        $counts = $this->missed($date, $warehouse, $parts);
        break;
    }
    return $counts;
  }
  /**
   * @author Giomani Designs (Development Team)
   * @return View Load Count Index with Warehouse Data
   */
  public function index () {
    $warehouses = Warehouse::getWarehouses();
    foreach ($warehouses as $warehouse) {
      $options[$warehouse->id] = $warehouse;
    }
    return view('load-counts.index', ['warehouses' => $options]);
  }
  /**
   * @author Giomani Designs (Development Team)
   * @return \Illuminate\Http\Response
   */
  private function loadCountsDay ($date, $warehouse, $parts){
    return OrderItems::getStocksLoadCount($date, $warehouse, $parts);
  }
  /**
   * @author Giomani Designs (Development Team)
   * @return \Illuminate\Http\Response
   */
  private function loadCountsMorning ($date, $warehouse, $parts){
    return OrderItems::getStocksLoadCountM($date, $warehouse, $parts);
  }
  /**
   * @author Giomani Designs (Development Team)
   * @return \Illuminate\Http\Response
   */
  private function missed ($date, $warehouse, $parts){
    return OrderItems::getStockLoadCountMissed($date, $warehouse, $parts);
  }
  /**
   * @author Giomani Designs (Development Team)
   * TODO: needs data validation
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function printCounts (Request $request, $type, $date, $warehouse, $size) {
     
    $rows = [];
    $parts = $request->query('parts');
    $counts = $this->getCounts($type, $date, $warehouse, $parts);
    $counts = $counts->paginate(20);

    foreach ($counts as $count) {
      if($parts == 'true'){
        if($warehouse !== 'all'){
          $rows[] = [
            $count['itemCode'] , 
            $count['id'], 
            // ' Part ' . ($i + 1) . ' of ' . $count['pieces'],
            $count['part'],
            $count['warehouse'],
            $count['tot']
          ];
        } else {
          $rows[] = [
            $count['itemCode'] , 
            $count['id'], 
            // ' Part ' . ($i + 1) . ' of ' . $count['pieces'],
            $count['part'], 
             
            $count['tot']
          ];
        }
      } else{
        if($warehouse !== 'all'){
          $rows[] = [
            $count['itemCode'],
            $count['warehouse'], 
            $count['tot']
          ];
        } else {
          $rows[] = [
            $count['itemCode'], 
            $count['tot']
          ];
        }
      }
    }
    
    $title = $this->titles[$type] . ($warehouse === 'all' ? ', all warehouses' : ', warehouse ' . $warehouse);
    $pdf = null;
    if($parts === 'true'){
      $pdf = new PdfTable(['Item Code', 'Warehouse', 'Count', 'Stock Part ID', 'No. of Parts'], $rows, $title, null, 'L');
    } else {
      $pdf = new PdfTable(['Item Code', 'Warehouse', 'Count'], $rows, $title, null, 'P');
    }
    if ($size === 'b') {
      $pdf->setColumnWidths([30, 9, 7]);
      $pdf->setSize($size);
    } else {
      $pdf->setColumnWidths([30, 8, 7]);
    }
    $pdf->table();
    return $pdf->out();
  }

  public function htmlCounts(Request $request, $type, $date, $warehouse){
    $parts = $request->query('parts');
    $counts = $this->getCounts($type, $date, $warehouse, $parts);
    $counts = $counts->paginate(20);
    if($parts == 'true'){
      if($warehouse == 'per'){
        if($type == '3'){
          $headers = ['Item Code', 'Stock Part ID', 'Part Boxes', 'Warehouse', 'Quantity Count','Order ID', 'Notes'];
        } else {
          $headers = ['Item Code', 'Stock Part ID', 'Part Boxes', 'Warehouse', 'Quantity Count'];
        }
      } else {
        if($type == '3'){
          $headers = ['Item Code', 'Stock Part ID', 'Part Boxes', 'Quantity Count','Order ID', 'Notes'];
        } else {
          $headers = ['Item Code', 'Stock Part ID', 'Part Boxes', 'Quantity Count'];
        }
      }
    } else {
      if($warehouse == 'per'){
        if($type == '3'){
          $headers = ['Item Code', 'Warehouse', 'Quantity Count', 'Order ID', 'Notes'];
        } else {
          $headers = ['Item Code', 'Warehouse', 'Quantity Count'];
        }
      } else {
        if($type == '3'){
          $headers = ['Item Code', 'Quantity Count', 'Order ID', 'Notes'];
        } else {
          $headers = ['Item Code', 'Quantity Count'];
        }
      }
    }
    if($warehouse != 'all' && $warehouse != 'per'){
      $warehouseName = Warehouse::find($warehouse + 1);
    } else {
      $warehouseName = null;
    }

    return view('load-counts.show', ['counts' => $counts, 'type' => $type, 'date' => $date, 'warehouse' => $warehouse, 'warehouseName' => $warehouseName, 'parts' => $parts, 'tableheaders' => $headers]);
  }

}
