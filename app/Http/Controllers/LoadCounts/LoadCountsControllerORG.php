<?php
namespace App\Http\Controllers\LoadCounts;

use Validator;
use App\Warehouse;
use App\OrderItems;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Helper\Traits\OutHeaders;
use App\Helper\Pdfs\PdfTable;
use App\Http\Controllers\Controller;
use App\Exceptions\ValidationException;

class LoadCountsController extends Controller
{
  use OutHeaders;

  private $titles = [
    'Error',
    'Day Load Counts',
    'Morning Load Counts',
    'Missed',
  ];
  /**
   * @var
   */
  private $rules = [
    'type' => 'required|in:1,2,3',
    'date' => 'required|date_format:d-M-Y',
    'warehouse' => 'required|numeric|exists:warehouses,id',
  ];
  /**
   * @author Giomani Designs (Development Team)
   * TODO: needs data validation
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function csvCounts (Request $request, $type, $date, $warehouse) {
    $counts = $this->getCounts($type, $date, $warehouse);
    $rows = [];
    $title = $this->titles[$type] . ($warehouse === 'all' ? ', all warehouses' : ', warehouse ' . $warehouse);
    $this->outCsvHeaders($title);
    $out = fopen('php://output', 'w');
    fputcsv($out, ['Item Code', 'Warehouse', 'Count']);
    foreach ($counts as $count) {
      for ($i = 0, $j = $count['pieces']; $i < $j; $i++) {
        fputcsv($out, [$count['itemCode'] . ' Part ' . ($i + 1) . ' of ' . $count['pieces'], $count['warehouse'], $count['tot']]);
      }
    }
    fclose($out);
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  integer  $type       1 => 'Day Load Counts'
   *                              2 => 'Morning Load Counts'
   *                              3 => 'Missed'
   * @param  integer  $date       date in format dd-mmm-yyyy
   * @param  integer  $warehouse
   * @return array
   * @throws App\Exceptions\ValidationException
   */
  private function getCounts ($type, $date, $warehouse) {
    switch ($type) {
      case 1:
        $counts = $this->loadCountsDay($date, $warehouse);
        break;
      case 2:
        $counts = $this->loadCountsMorning($date, $warehouse);
        break;
      case 3:
        $counts = $this->missed($date, $warehouse);
        break;
    }
    return $counts;
  }
  /**
   * @author Giomani Designs (Development Team)
   * @return \Illuminate\Http\Response
   */
  public function index () {
    $warehouses = Warehouse::all();
    $options = ['all' => 'all'];
    foreach ($warehouses as $warehouse) {
      // $options[$warehouse->id] = $warehouse->name;
      $options[$warehouse->name] = $warehouse->name;
    }
    return view('load-counts.index', ['warehouses' => $options]);
  }
  /**
   * @author Giomani Designs (Development Team)
   * @return \Illuminate\Http\Response
   */
  private function loadCountsDay ($date, $warehouse) {
    return OrderItems::getLoadCounts($date, $warehouse);
  }
  /**
   * @author Giomani Designs (Development Team)
   * @return \Illuminate\Http\Response
   */
  private function loadCountsMorning ($date, $warehouse) {
    return OrderItems::getLoadCountsM($date, $warehouse);
  }
  /**
   * @author Giomani Designs (Development Team)
   * @return \Illuminate\Http\Response
   */
  private function missed ($date, $warehouse) {
    return OrderItems::getMissedCounts($date, $warehouse);
  }
  /**
   * @author Giomani Designs (Development Team)
   * TODO: needs data validation
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function printCounts (Request $request, $type, $date, $warehouse, $size) {
    $counts = $this->getCounts($type, $date, $warehouse);
    $rows = [];
    foreach ($counts as $count) {
      $rows[] = [$count['itemCode'], $count['warehouse'], $count['tot']];
    }
    $title = $this->titles[$type] . ($warehouse === 'all' ? ', all warehouses' : ', warehouse ' . $warehouse);
    $pdf = new PdfTable(['Item Code', 'Warehouse', 'Count'], $rows, $title, null, 'P');
    if ($size === 'b') {
      $pdf->setColumnWidths([30, 9, 7]);
      $pdf->setSize($size);
    } else {
      $pdf->setColumnWidths([30, 8, 7]);
    }
    $pdf->table();
    return $pdf->out();
  }
}
