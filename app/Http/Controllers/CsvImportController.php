<?php

namespace App\Http\Controllers;

use App\Helper\CsvProcessor;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CsvImportController extends Controller
{
  /**
   * @author Giomani Designs (Development Team)
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function headings($import) {
    return response()->json(CsvProcessor::getColumnHeadings($import));
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function profile($import) {
    return response()->json(CsvProcessor::getProfile($import));
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request) {
    $import = $request->import;
    $csvProcessor = new CsvProcessor;
    $data = $request->input('data');

    $csvProcessor->setColumnSpecs($import)->processCsv($data);

    $acceptedCount = $csvProcessor->getAcceptsCount();
    $accepts = $csvProcessor->getAccepts();
    $rejectedCount = $csvProcessor->getRejectsCount();

    if ($rejectedCount === 0 && strcmp($request->input('mode'), 'upload') === 0) {
      $classname = 'App\\Http\\CsvImport\\' . studly_case($import);
      $object = new $classname;
      $object->setParams($request->input('extra'));
      $object->processCsv($accepts);
      $result = [
        'failures' => $object->getFailures(),
        'successes' => $object->getSuccesses(),
      ];
    } else {
      $result = [
        'acceptedCount' => $acceptedCount,
        'accepts' => $accepts,
        'rejectedCount' => $rejectedCount,
        'rejects' => $csvProcessor->getRejects(),
      ];
    }
    return response()->json($result);
  }
}
