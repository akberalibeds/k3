<?php

namespace App\Http\Controllers\Admin\ImportExport;

use File;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class ProfileController extends Controller
{
  /**
   * @author Giomani Designs
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function index(Request $request) {
    $path = base_path('App/Helper/ImportExport/profiles/');

    $exports = config('importexport.exports');
    $imports = config('importexport.imports');

    return view('import-export.index', ['exports' => $exports, 'imports' => $imports]);
  }
}
