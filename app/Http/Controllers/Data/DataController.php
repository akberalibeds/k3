<?php
// @author Giomani Designs (Development Team)
//
namespace App\Http\Controllers\Data;

use Event;
use Auth;
use Hash;
use Entrust;
use App\Role;
use App\User;
use App\UserBookmark;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Events\UserChangingPassword;
use App\Http\Controllers\Controller;
use App\Http\Requests\UserPostRequest;
use App\Queries;
use Illuminate\Database\QueryException;

class DataController extends Controller
{

	public function index(){
		
		return view('data.index');
	
	}
	
	public function query(Request $request){
	
				
	if($request->get('save')==1){	
		$q = new Queries();
		$q->query=trim($request->get('query'));
		$q->save();
	}
		try{
		$data = \DB::select(\DB::raw($request->get('query')));
		}
		catch (QueryException $e){
			return redirect("/data")->with('error', $e->getMessage());
		}
		$headers = array_keys((array)$data[0]);
		
		return view('data.index',['titles' => $headers ,'data' => $data]);
	
	}
	
	
	public function csv(Request $request){
		
		try{
			$data = \DB::select(\DB::raw($request->get('query')));
		}
		catch (QueryException $e){
			return redirect("/data")->with('error', $e->getMessage());
		}
	
		$heads = array_keys((array)$data[0]);
		
		$filename = storage_path()."/file.csv";
		$handle = fopen($filename, 'w+');
		fputcsv($handle, $heads);
		foreach($data as $rows){
				fputcsv($handle, (array)$rows);
		}
		
		fclose($handle);
		$headers = array(
				'Content-Type' => 'text/csv',
		);
		
		return response()->download($filename, 'file '.date("d-m-Y H:i").'.csv', $headers);
		
		
	}
	
	
	public function saved(){
		
		$queries = Queries::all();
		return view('data.saved',['queries' => $queries]);
		
	}
	
	
	public function remove(Request $request){
	
		$queries = Queries::find($request->get('id'));
		$queries->delete();
		return redirect('/data/saved');
	
	}

}