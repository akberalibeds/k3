<?php

namespace App\Http\Controllers\Seller;

use Entrust;
use App\Order;
use App\Seller;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class SellerOrdersController extends Controller
{
  /**
   * @author Giomani Designs (Development Team)
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function exportCsv(Request $request, $id, $ids) {
    $oids = explode('|', $ids);
    try {
      $seller = Seller::findOrFail($id);
    } catch(ModelNotFoundException $e) {
      return parent::notFound();
    }
    $orders = Order::getWholesaleOrdersByIds($id, $oids)->toArray();
    $datetime = new \DateTime;
    $title = $datetime->format('Ymd-') . str_slug($seller->name);
    header('Content-type: text/csv');
    header('Content-disposition: attachment; filename=' . $title . '.csv');
    header('Content-transfer-encoding: binary\n');
    $out = fopen('php://output', 'w');
    $headings = [
      'Order id',
      'Invoice',
      'Buyer',
      'Customer',
      'Postcode',
      'Ref',
      'Type',
      'Status',
      'Date',
      'total',
      'paid',
      'voucher_code',
      'Order Items'
    ];
    fputcsv($out, $headings);
    foreach($orders as $line) {
      fputcsv($out, $line);
    }
    fclose($out);
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  \App\Http\Requests\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function import(Request $request) {
    $data = $request->get('data');
    $mode = $request->get('mode');
    $csvProcessor = new CsvProcessor;
    $csvProcessor->setColumnSpecs($columnIndexes);
    $csvProcessor->processCsv($data, new Order);
    $accepts = $csvProcessor->getAccepts();
    $rejects = $csvProcessor->getRejects();
    $reply['data'] = array (
        'accepts' => $accepts,
        'rejects' => $rejects,
    );
    if (strcmp($mode, 'upload') == 0 && count($rejects) == 0) {
      foreach ($accepts as $model) {
        $model['row']->save();
      }
    }
    return response()->json($reply);
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function index(Request $request) {
    if ($request->ajax()) {
      return Seller::getOverview();
    }
    return view('seller-orders.index');
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function map(Request $request, $id, $ids) {
    $oids = explode('|', $ids);
    try {
      $seller = Seller::findOrFail($id);
    } catch(ModelNotFoundException $e) {
      return parent::notFound();
    }
    Order::setSellersPaid($id, $oids);
	Order::setDeliveryRouteBulk($oids);
    return [];
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function maup(Request $request, $id, $ids) {
    $oids = explode('|', $ids);
    try {
      $seller = Seller::findOrFail($id);
    } catch(ModelNotFoundException $e) {
      return parent::notFound();
    }
    Order::setSellersUnPaid($id, $oids);
    return [];
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show(Request $request, $id) {
    try {
      $seller = Seller::findOrFail($id);
    } catch (ModelNotFoundException $e) {
      return parent::notFound();
    }
    if ($request->ajax()) {
      $query = $request->all();
      return Order::getSeller($id, $query);
    }
    return view('seller-orders.show', ['seller' => $seller]);
  }
}
