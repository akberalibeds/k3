<?php

namespace App\Http\Controllers\Seller;

use Entrust;
use App\Order;
use App\Seller;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\SellerPutRequest;
use App\Http\Requests\SellerPostRequest;

class SellerController extends Controller
{
  /**
   * @author Giomani Designs (Development Team)
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function index(Request $request) {
    if ($request->ajax()) {
      $query = $request->all();
      return Seller::getAll($query);
    }
    return view('sellers.index');
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id) {
    try {
      $seller = Seller::findOrFail($id);
    } catch (ModelNotFoundException $e) {
      return parent::notFound();
    }

    return view('sellers.show', ['seller' => $seller]);
  }
}
