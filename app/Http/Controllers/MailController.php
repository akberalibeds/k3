<?php

namespace App\Http\Controllers;

use Mail;
use App\Mailer;
use App\Http\Requests;
use Illuminate\Http\Request;

class MailController extends Controller
{
  /**
   * Show the application dashboard.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {

  }
  /**
   *
   * @param  string $template
   */
  public function preview($template)
  {
    $emailTemaplte = 'emails.' . $template;
    if (view()->exists($emailTemaplte)) {
      return view($emailTemaplte);
    }
    return view('emails.not-found');
  }

  /*
   * Test Mail function
   *
   */
  public function test(Request $request)
  {
    //$data = json_decode($request->get('data'),false);

    $link = 'http://premierdeliveries.co.uk?id=code';
    Mail::send('emails.premier.links',[ 'link' => $link ], function ($m)  {

		$m->from('no-reply@giomani-designs.co.uk', 'Premier Deliveries');
	
		$m->to('n.rabbi@live.com', 'nad')
		  ->subject('test')
      ;//->setBody($data->body,'text/html');
    });
  }


  /*
   * Test Mail function
   */
  public function raw(Request $request)
  {
 
    	$data = json_decode($request->get('data'),false);
    	Mailer::sendRaw($data);
 
  }
  
  
  
   /*
   * stock Mail function
   */
  public function sendStockMail(Request $request)
  {
 
    	return Mailer::stockEmail($request);
 
  }
  
  
  
  public function invoice(Request $request)
  {
    	$data = json_decode($request->get('data'),false);
    	Mailer::sendInvoiceMail($data);
  }
  
  
}
