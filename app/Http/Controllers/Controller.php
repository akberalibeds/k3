<?php

namespace App\Http\Controllers;

use Validator;
use Illuminate\Http\Request;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
  use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
  /**
   * @Validator
   */
  protected $request;
  /**
   * @author Giomani Designs (Development Team)
   * @param  \Illuminate\Http\Request  $request
   * @return void
   */
  public function __construct (Request $request) {
    // $this->middleware('auth');
    $this->request = $request;
  }
  /**
   * @author Giomani Designs (Development Team)
   */
  public function notAuthorised ($reason = '') {
    if (request()->ajax()) {
      return response(['error' => 'not authorised'], 401);
    } else {
      return view('auth.not-authorised', ['reason' => $reason]);
    }
  }
  /**
   * @author Giomani Designs (Development Team)
   */
  public function notFound ($transKey = null, $transVars = []) {
    if ($transKey === null) {
      $message = '(no messgae for ' . get_class($this) . ')';
    } else {
      $message = trans('not-found.' . $transKey, $transVars);
    }
    if ($this->request->ajax()) {
      return response($message, 404);
    } else {
      return view('share.not-found', ['message' => $message]);
    }
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  array                    $fields
   * @param  array                    $rules
   * @param  \Illuminate\Http\Request $request
   * @param  string                   $failureRoute     not required for ajax requests
   * @param  array                    $failureRouteVars not required for ajax requests
   * @return boolean|Illuminate\Http\Response
   */
  public function validateForm ($fields, $rules, $request) {
    $validator = Validator::make($fields, $rules);
    if ($validator->fails()) {
      if ($request->ajax()) {
        return response($validator->errors(), 422);
      } else {
        return redirect()->back()->withInput($fields)->withErrors($validator->errors());
      }
    }
    return true;
  }
}
