<?php

namespace App\Http\Controllers\Stock;

use Entrust;
use App\StockItem;
use App\OrderItems;
use App\Order;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class StockOrdersController extends Controller
{
  /**
   * @author Giomani Designs (Development Team)
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id the id of the stock-item
   * @return \Illuminate\Http\Response
   */
  public function index (Request $request, $id) {
    if (!Entrust::can('stock_view'))  {
      return parent::notAuthorised();
    }
    try {
      $stockItem = StockItem::findOrFail($id);
    } catch (ModelNotFoundException $e) {
      return parent::notFound();
    }
    if ($request->ajax()) {
      return OrderItems::getOrdersFor($id, $request->all());
    }
    return view('stock-orders.index', ['stockItem' => $stockItem]);
  }
  
  
  
  public function emailList(Request $request){
  		//return $request;
		if (!Entrust::can('stock_view'))  {
		  return parent::notAuthorised();
		}
		$selected = ($request->get('selected'));
		return Order::select('dBusinessName as name','email1 as email')
					->join('customers','orders.cid','=','customers.id')
					->whereIn('orders.id',$selected)
					->where('email1','like','%@%')
					->whereNotNull('email1')
					->get(); 
  }
  
}
