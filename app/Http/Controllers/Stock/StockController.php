<?php
// @author Giomani Designs (Development Team)
//
namespace App\Http\Controllers\Stock;

use Entrust;
use App\StockItem;
use App\OrderItems;
use App\StockCategory;
use Helper\CsvPeocessor;
use Illuminate\Http\Request;
use App\Helpers\CsvProcessor;
use App\Helper\Traits\StockUtils;
use App\Http\Controllers\Controller;
use Illuminate\Validation\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class StockController extends Controller
{
  use StockUtils;
  /**
   * @author Giomani Designs (Development Team)
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function index (Request $request) {
    if ($request->ajax()) {
      $query = $request->all();
      $stockItems = StockItem::getStock($query);
      $stockItems = $stockItems
      ->where('stock_items.isActive', '=', 1)
      ->orderBY('itemCode')->paginate(config('search.rpp'));
      return $stockItems;

    }
    return view('stock.index', ['categories' => $this->getCategoryOptions()]);
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  string                    $modelNumbers pipe seperated list of model numbers
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function modelNumbers (Request $request, $modelNumbers) {
    return ['stockItems' => StockItem::getForModelNumbers(explode('|', $modelNumbers))];
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  string $query
   * @return \Illuminate\Http\Response
   */
  public function search ($query) {
    return StockItem::searchByItemCode($query);
  }
  /**
   * Display the specified resource.
   *
   * @author Giomani Designs (Development Team)
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show ($id) {
    try {
      $stockItem = StockItem::findOrFail($id);
    } catch(ModelNotFoundException $e) {
      return parent::notFound();
    }
    return view('stock.show', ['stockItem' => $stockItem]);
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  int  $id the id of the stock-item
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function showOrders (Request $request, $id) {
    try {
      $stockItem = Stockitem::findOrFail($id);
    } catch (ModelNotFoundException $e) {
      return parent::notFound();
    }
    if ($request->ajax()) {
      return OrderItems::getOrdersFor($id);
    }
    return view('stock.show-orders', ['stockItem' => $stockItem]);
  }
}
