<?php

namespace App\Http\Controllers\Stock;

use Entrust;
use App\StockItem;
use App\StockCategory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class StockCategoryItemsController extends Controller
{
  /**
   * @author Giomani Designs (Development Team)
   * @param  \Illuminate\Http\Request  $request
   * @param  integer                   $id
   * @return \Illuminate\Http\Response
   */
  public function index(Request $request, $id) {
    try {
      $category = StockCategory::findOrFail($id);
    } catch (ModelNotFoundException $e) {
      return parent::notFound();
    }
    if ($request->ajax()) {
      $query = $request->all();
      $stockItems = StockItem::getForCategory($id, $query);
      return $stockItems;
    }
    return view('stock-category-items.index', ['category' => $category]);
  }
}
