<?php

namespace App\Http\Controllers\Stock;

use Entrust;
use App\StockItem;
use App\StockCategory;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Validation\Validator;
use Illuminate\Support\Facades\Redirect;

class StockCategoryController extends Controller
{
  /**
   * @author Giomani Designs (Development Team)
   * @return \Illuminate\Http\Response
   */
  public function create() {
    return view('stock-categories.create');
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit($id) {
    try {
      $category = StockCategory::findOrFail($id);
    } catch(ModelNotFoundException $e) {
      return parent::notFound();
    }
    return view('stock-categories.edit', ['category' => $category]);
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function index(Request $request) {
    if ($request->ajax()) {
      $query = $request->all();
      return StockCategory::getCategories($query);
    }
    return view('stock-categories.index');
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id) {
    try {
      $category = StockCategory::findOrFail($id);
    } catch(ModelNotFoundException $e) {
      return parent::notFound();
    }
    return view('stock-categories.show', ['category' => $category]);
  }
  /**
   * Display a listing of the resource.
   *
   * @author Giomani Designs (Development Team)
   * @param  \Illuminate\Http\Request  $request
   * @param  int $id
   * @return array
   */
  public function showItems(Request  $request, $id) {
    return StockItem::getForCategory($id);
  }
  /**
   * Store a newly created resource in storage.
   *
   * @author Giomani Designs (Development Team)
   * @param  \App\Http\Requests\StockCategoryPutRequest  $request
   * @return \Illuminate\Http\Response
   */
  public function store(StockCategoryPostRequest $request) {
    $category = $request->except('_method', '_token');
    StockCategory::insert($category);

    return view('stock-categories.show', ['category' => $category]);
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  \App\Http\Requests\StockCategoryPostRequest  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(StockCategoryPutRequest $request, $id) {
    try {
      $category = StockCategory::findOrFail($id);
    } catch(ModelNotFoundException $e) {
      return parent::notFound();
    }
    $category->name = $request->name;
    StockCategory::save($category);

    return view('stock-categories.show', ['category' => $category]);
  }
}
