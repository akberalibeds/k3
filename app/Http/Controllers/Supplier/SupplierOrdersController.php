<?php

namespace App\Http\Controllers\Supplier;

use Excel;
use Event;
use Entrust;
use App\Order;
use App\OrderNotes;
use App\Supplier;
use App\Http\Requests;
use App\Helper\PdfLabel;
use Illuminate\Http\Request;
use App\Events\OrderOrdering;
use App\Events\OrderProcessing;
use App\Events\OrderUnOrdering;
use App\Events\OrderUnProcessing;
use App\Helper\Traits\OutHeaders;
use App\Helper\Traits\OrderUtils;
use App\Http\Controllers\Controller;
use App\Events\OrderSupplierChanging;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class SupplierOrdersController extends Controller
{
  use OutHeaders, OrderUtils;
  /**
   * @author Giomani Designs (Development Team)
   * @param  \Illuminate\Http\Request  $request
   * @param  integer $request
   * @param  string pipe seperated list of order ids
   * @return \Illuminate\Http\Response
   */
  public function exportCsv(Request $request, $id, $full = false) {
    $ids=request('ids');
    return $this->export($id, $ids, $full, 'csv');
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  \Illuminate\Http\Request  $request
   * @param  integer $request
   * @param  string pipe seperated list of order ids
   * @return \Illuminate\Http\Response
   */
  public function exportExcel(Request $request, $id, $full = false) {
    $ids=request('ids');
    return $this->export($id, $ids, $full, 'xlsx');
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  \Illuminate\Http\Request  $request
   * @param  integer $request
   * @param  string pipe seperated list of order ids
   * @return \Illuminate\Http\Response
   */
  private function export($id, $ids, $fullAddress, $type) {
    if (!Entrust::can('supplier_view')) {
      return parent::notAuthorised();
    }
    try {
      $supplier = Supplier::findOrFail($id);
    } catch (ModelNotFoundException $e) {
      return parent::notFound();
    }
    $ids = explode('|', $ids);
    $datetime = new \DateTime;
    $filename = $datetime->format('Ymdhis-') . str_replace(' ', '_', trim($supplier->supplier_name));
    $title = $datetime->format('d M Y') . ' ' . trim($supplier->supplier_name);
    $title = substr($title, 0, 31);
    return Excel::create($title, function($excel) use($ids, $title, $fullAddress) {
      $orders = Order::getSupplierInvoicedCsv($ids);
      $excel->sheet($title, function($sheet) use($orders, $title, $fullAddress) {
        $sheet->setOrientation('landscape');
        $sheet->setPageMargin([0.25, 0.30, 0.25, 0.30]);
        $rowIndex = 4;
        $columns = ['SKU', 'Item', 'Notes', 'Qty', 'Boxes', 'Name'];
        if ($fullAddress) {
          array_push($columns, 'Address');
        }
        $columns = array_merge($columns, ['Postcode', 'Telephone', 'Order', 'Route', 'Del Time','Del #', 'Yes/no']);

        $rows = [[$title], [], array_merge($columns)];
        foreach($orders as $line) {
          $delTime = $line['delTime'];
          $items = explode(';', $line['itemCode']);
          $name = $line['dBusinessName'];
          $oid = $line['oid'];
          $delOrder = $line['delOrder'];

          if ($fullAddress) {
            $address = array_filter($this->getAddress($line, 'a'), function($value) { return $value !== '' && $value !== null; });
            array_pop($address);
            $address = implode(', ', $address);
          }
          $postcode = $line['a_postcode'];
          $route = $line['route_name'];
          $telephone = $line['tel'];
          $mergeRowStart = $rowIndex + 1;
          foreach ($items as $item) {
            $itemDetail = explode(',', $item);
            $row = [$itemDetail[0], $itemDetail[1], $itemDetail[4], $itemDetail[2], $itemDetail[3], $name];
            if ($fullAddress) {
              $row[] = $address;
            }
            $rows[] = array_merge($row, [$postcode, $telephone, $oid, $route, $delTime, $delOrder, '-']);
            $rowIndex++;
          }
        }
        $lastColumn = chr(64 + count($columns));
        $sheet->mergeCells('A1:' . $lastColumn . '1');
        $sheet->fromArray($rows, null, 'A1', false, false);
        $sheet->setBorder('A3:' . $lastColumn . ($rowIndex - 1), 'thin');
        $sheet->cells('E2:I' . $rowIndex, function($cells) {
          $cells->setValignment('top');
        });
        $sheet->cells('A1', function($cells) {
          $cells->setAlignment('center');
        });
        $sheet->cells('A1:' . $lastColumn . '3', function($cells) {
          $cells->setFont(['bold' =>  true]);
        });
        $sheet->cells('A3:' . $lastColumn . '3', function($cells) {
          $cells->setBackground('#a0a0a0');
        });
      });
    })->download($type);
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  int  $id
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function index(Request $request, $id) {
    if (!Entrust::can('supplier_view')) {
      return parent::notAuthorised();
    }
    try {
      $supplier = Supplier::findOrFail($id);
    } catch (ModelNotFoundException $e) {
      return parent::notFound('supplier', ['id' => $id]);
    }
    if ($request->ajax()) {
      $query = $request->all();
      return Order::getSupplier($id, $query);
    }
    $searchConfig = [
      'Order ID' => 'orders.id;oid',
      'Name' => 'dBusinessName;dBusinessName',
      'Email' => 'email1;email1',
    ];
    $suppliers = Supplier::where('id', '<>', $id)->orderby('supplier_name', 'asc')->get();
    return view('supplier-orders.index', ['supplier' => $supplier, 'suppliers' => $suppliers]);
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  string  $subject
   * @param  string  $query
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function invoicedOrders(Request $request, $id, $iid) {
    if (!Entrust::can('supplier_view')) {
      return parent::notAuthorised();
    }
    try {
      $supplier = Supplier::findOrFail($id);
    } catch (ModelNotFoundException $e) {
      return parent::notFound();
    }
    if ($request->ajax()) {
      return Order::getSupplierInvoiced($iid);
    }
    return view('supplier-orders.invoiced', ['supplier' => $supplier, 'invoiceMade' => $iid]);
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  string  $subject
   * @param  string  $query
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function invoicedSearchOrders(Request $request, $subject, $query, $id) {
    if (!Entrust::can('supplier_view')) {
      return parent::notAuthorised();
    }
    try {
      $supplier = Supplier::findOrFail($id);
    } catch (ModelNotFoundException $e) {
      return parent::notFound();
    }
    if ($request->ajax()) {
      return Order::searchSupplierInvoiced($id, $subject, $query);
    }
    return view('share.not-ready-yet');
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  integer $id supplier id
   * @param  string  $ids list of pipe seperated order ids
   * @param  integer $sid supplier id to move orders to
   * @return \Illuminate\Http\Response
   */
  public function moveOrders($id, $ids, $sid) {
    if (!Entrust::can('supplier_view')) {
      return parent::notAuthorised();
    }
    try {
      $supplier = Supplier::findOrFail($id);
    } catch (ModelNotFoundException $e) {
      return parent::notFound();
    }
    $orderIds = explode('|', $ids);
    Order::where('id_supplier', '=', $id)->whereIn('id', $orderIds)->update(['id_supplier' => $sid]);
    Event::fire(new OrderSupplierChanging($ids, $id, $sid));
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  \Illuminate\Http\Request  $request
   * @param  int $id supplier id
   * @return \Illuminate\Http\Response
   */
  public function ordered(Request $request, $id) {
    if (!Entrust::can('supplier_view')) {
      return parent::notAuthorised();
    }
    try {
      $supplier = Supplier::findOrFail($id);
    } catch (ModelNotFoundException $e) {
      return parent::notFound();
    }
    $ids = $request->input('orderIds');
    Order::setOrdered(explode('|', $ids));
    Event::fire(new OrderOrdering($ids));
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  \Illuminate\Http\Request  $request
   * @param  int $id supplier id
   * @return \Illuminate\Http\Response
   */
  public function process(Request $request, $id) {
    if (!Entrust::can('supplier_view')) {
      return parent::notAuthorised();
    }
    try {
      $supplier = Supplier::findOrFail($id);
    } catch (ModelNotFoundException $e) {
      return parent::notFound();
    }
    $ids = $request->input('orderIds');
    Order::setProcessed(explode('|', $ids));
	foreach(explode('|', $ids) as $order){
		OrderNotes::addNote("Order Processed",$order);		
	}
    Event::fire(new OrderProcessing($ids));
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  \Illuminate\Http\Request  $request
   * @param  int $id supplier id
   * @return \Illuminate\Http\Response
   */
  public function unOrdered(Request $request, $id) {
    if (!Entrust::can('supplier_view')) {
      return parent::notAuthorised();
    }
    try {
      $supplier = Supplier::findOrFail($id);
    } catch (ModelNotFoundException $e) {
      return parent::notFound();
    }
    $ids = $request->input('orderIds');
    Order::setUnOrdered(explode('|', $ids));
    Event::fire(new OrderUnOrdering($ids));
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  \Illuminate\Http\Request  $request
   * @param  int $id supplier id
   * @return \Illuminate\Http\Response
   */
  public function unProcess(Request $request, $id) {
    if (!Entrust::can('supplier_view')) {
      return parent::notAuthorised();
    }
    try {
      $supplier = Supplier::findOrFail($id);
    } catch (ModelNotFoundException $e) {
      return parent::notFound();
    }
    $ids = $request->input('orderIds');
    Order::setUnProcessed(explode('|', $ids));
	foreach(explode('|', $ids) as $order){
		OrderNotes::addNote("Order Un-Processed",$order);		
	}
    Event::fire(new OrderUnProcessing($ids));
  }
}
