<?php
namespace App\Http\Controllers\Supplier;

use Entrust;
use App\Order;
use App\Routes;
use App\EInvoice;
use App\Supplier;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use \App\Http\Requests\SupplierPutRequest;
use \App\Http\Requests\SupplierPostRequest;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class SupplierController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function index(Request $request) {
    if (!Entrust::can('supplier_view')) {
      return parent::notAuthorised();
    }
    if ($request->ajax()) {
      $query = $request->all();
      $stockItems = Supplier::getSuppliers($query);
      return $stockItems;
    }
    return view('suppliers.index');
  }
  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id) {
    if (!Entrust::can('supplier_view')) {
      return parent::notAuthorised();
    }
    try {
      $supplier = Supplier::findOrFail($id);
    } catch(ModelNotFoundException $e) {
      return parent::notFound();
    }
    return view('suppliers.show', ['supplier' => $supplier]);
  }
}
