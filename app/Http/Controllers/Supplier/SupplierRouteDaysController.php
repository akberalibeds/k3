<?php

namespace App\Http\Controllers\Supplier;

use Entrust;
use App\Order;
use App\Routes;
use App\Supplier;
use App\Http\Requests;
use App\Helper\PdfLabel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class SupplierRouteDaysController extends Controller
{
  /**
   * Display the specified resource.
   *
   * @author Giomani Designs
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function exportCsv(Request $request, $id, $iid)
  {
    if (!Entrust::can('supplier_view')) {
      return parent::notAuthorised();
    }
    try {
      $supplier = Supplier::findOrFail($id);
    } catch (ModelNotFoundException $e) {
      return parent::notFound();
    }

    $orders = Order::getSupplierInvoicedCsv($iid)->toArray();
    $datetime = new \DateTime;
    $title = $datetime->format('Ymdhis-') . strstr($supplier->supplier_name, ' ', '_');

    header('Content-type: text/csv');
    header('Content-disposition: attachment; filename=' . $title . '.csv');
    // header("Content-transfer-encoding: binary\n");

    $out = fopen('php://output', 'w');

    foreach($orders as $line)
    {
      fputcsv($out, $line);
    }
    fclose($out);
  }
  /**
   * Display a listing of the resource.
   *
   * @author Giomani Designs
   * @param  int  $id supplier id
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function index(Request $request, $id)
  {
    try {
      $supplier = Supplier::findOrFail($id);
    } catch (ModelNotFoundException $e) {
      return parent::notFound();
    }

    $routes = Routes::routeDaysSupplierRoutes($id);

    if ($request->ajax()) {
      return $routes;
    }
    return view('supplier-route-days.index', ['supplier' => $supplier, 'routes' => $routes]);
  }

  /**
   * Search for a supplier
   *
   * @author Giomani Designs
   * @param  int  $id supplier id
   * @param  string  $subject
   * @param  string  $query
   * @return \Illuminate\Http\Response
   */
  public function search($id, $subject, $query)
  {
    if (!Entrust::can('supplier_view')) {
      return parent::notAuthorised();
    }
    $orders = Order::searchSupplier($id, $subject, $query);

    return response()->json($orders);
  }
  /**
   * Display the specified resource.
   *
   * @author Giomani Designs
   * @param  int  $id
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function show(Request $request, $id)
  {
  }
}
