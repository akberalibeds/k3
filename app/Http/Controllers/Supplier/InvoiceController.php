<?php

namespace App\Http\Controllers\Supplier;

use Event;
use App\Supplier;
use App\EInvoice;
use App\Http\Requests;
use App\Http\Response;
use Illuminate\Http\Request;
use App\Events\InvoiceWasPaid;
use App\Http\Controllers\Controller;
use App\Helper\CSVParser;
use App\StockItem;

class InvoiceController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function index(Request $request, $id)
  {
    try {
      $supplier = Supplier::findOrFail($id);
    } catch(ModelNotFoundException $e) {
      return parent::notFound('supplier', ['id' => $id]);
    }
    if ($request->ajax()) {
      return EInvoice::getForSupplier($id);
    }

    return view('suppliers.invoices', ['supplier' => $supplier]);
  }
  /**
   * set an invoice as paid
   *
   * @param  int $sid supplier id
   * @param  int $iid invoice id
   * @return \Illuminate\Http\Response
   */
  public function paidInvoice($sid, $iid)
  {
    try {
      $supplier = Supplier::findOrFail($sid);
    } catch(ModelNotFoundException $e) {
      return parent::notFound('order', ['id' => $sid]);
    }
    try {
      $invoice = EInvoice::findOrFail($iid);
    } catch(ModelNotFoundException $e) {
      return parent::notFound('invoice', ['id' => $iid]);
    }

    EInvoice::setAsPaid($sid, $iid);
    Order::setInvoicePaid($sid, $iid);

    Event::fire(new InvoiceWasPaid($iid, $id));

    return ['timePaid' => date('U')];
  }
  /**
   * Upload Delivery Manifest and return file with prices
   *
   * @param  Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function upload(Request $request, $id)
  {
  		
  		$file = $request->file('file');
  		$parser = new CSVParser($file);
  		$data = $parser->data;
  		
  		foreach($data as &$d){
  			$item = StockItem::where(['itemCode' => $d[0]])
  								->whereIn('cat',['Divan Beds','expressMat','collect express'])
  								->first();
  			
  			if($item){
  				$d[]=$item->cost * $d[1];
  			}
  			else{
  				$d[]='';
  			}
  		}
  		
  		
  		$out = fopen('files/test.csv', 'w');
  		foreach ($data as $fields) {
  			fputcsv($out, $fields);
  		}
  		fclose($out);
  		
  		return response()->download('files/test.csv', 'manifest.csv', ['content-type' => 'text/csv']);
  		
  	}
  
  
  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
    //
  }
}
