<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Container;
use Auth;

class ContainerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        
        if($request->has('field')){
            $containers = Container::where($request->field, 'like' ,'%'.$request->val.'%')->paginate(100);
        }
        else{
            $containers = Container::orderBy('id','desc')->paginate(15);
        }
        
        return view('containers.index',['containers' => $containers]);
        
        
    }

   

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->except('_token','id');
        $data['items'] = json_encode($data['items']);
        $data['staff_id'] = Auth::user()->id;
        if($request->has('id')){
            $c = Container::find($request->get('id'));
            $c->update($data);
        }
        else{
            $c = new Container($data);
        }
        
        $c->save();
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function rxd($id)
    {
        
        $c = Container::find($id);
        $c->rxd=1;
        $c->save();
        
    }
    
    
    public function removeRow($id)
    {
        $c = Container::find($id);
        $c->delete();   
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
