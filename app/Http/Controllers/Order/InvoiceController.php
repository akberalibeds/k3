<?php

namespace App\Http\Controllers\Order;

use Entrust;
use App\Order;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Helper\Pdfs\PdfInvoice;
use App\Helper\PdfDispatchNotes;
use App\Http\Controllers\Controller;

class InvoiceController extends Controller
{
  /**
   * TODO: convert this to use POST request
   */
   public function index($ids, $vat = null) {
     $vat = $vat ? true : false;
     $pdfInvoice = new PdfInvoice();
     $orders = Order::getForInvoice(explode('|', $ids));
     foreach ($orders as $order) {
       $pdfInvoice->invoice($order, $vat);
     }
     return $pdfInvoice->outPdf();
   }
}
