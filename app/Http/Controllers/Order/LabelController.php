<?php

namespace App\Http\Controllers\Order;

use Entrust;
use App\Order;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Helper\Pdfs\PdfPostLabel;
use App\Helper\Pdfs\PdfLabel;

class LabelController extends Controller
{
  /**
   * @author Giomani Designs (Development Team)
   */
   public function index($ids) {
   	 set_time_limit(0);
     $pdf = new PdfLabel();
     $orders = Order::getLabelsByIds(explode('|', $ids));
     
     foreach ($orders as $order) {
     	Order::setBarcode($order->oid);
       	$pdf->label($order);
     }
     return $pdf->outPdf();
   }
   
   /**
    * @author Giomani Designs (Development Team)
    */
   public function post($ids) {
   
   	$pdf = new PdfPostLabel();
   	$orders = Order::getLabelsByIds(explode('|', $ids));
   	 
   	foreach ($orders as $order) {
   		Order::setBarcode($order->oid);
   		$pdf->label($order);
   	}
   	return $pdf->outPdf();
   }
}
