<?php
// Giomani Designs (Development Team)
//
namespace App\Http\Controllers\Order;

use Excel;
use Entrust;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Helper\Import\AjFoamsTracking;
use App\Helper\Import\BedtasticTracking;
use App\Helper\Import\MightyDealsOrders;
use App\Http\Controllers\CsvImportControllerAbstract;

class OrderImportController extends CsvImportControllerAbstract
{
  /**
   * @author Giomani Designs (Development Team)
   * @return \Illuminate\Http\Response
   */
  public function create ($profile) {
    if (!Entrust::can('seller_orders_import')) {
      return parent::notAuthorised();
    }
    return view('order-import.create', ['profile' => $profile, 'profileName' => $this->fetchImportProfile($profile)->getImportProfileName($profile)]);
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  string                     $profile
   * @return \Illuminate\Http\Response
   */
  public function headings ($profile) {
    if (!Entrust::can('seller_orders_import')) {
      return parent::notAuthorised();
    }
    $this->fetchImportProfile($profile);
    return $this->getImportProfileHeadings();
  }
  /**
   * @author Giomani Designs (Development Team)
   * @return \Illuminate\Http\Response
   */
  public function index () {
    if (!Entrust::can('seller_orders_import')) {
      return parent::notAuthorised();
    }
    return view('order-import.index');
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function read (Request $request) {
    if (!Entrust::can('seller_orders_import')) {
      return parent::notAuthorised();
    }
    return $result = $this->readFile($request);
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store (Request $request, $profile) {
    if (!Entrust::can('seller_orders_import')) {
      return parent::notAuthorised();
    }
    switch ($profile) {
      case 'ajfoams-tracking':
        $this->setCsvProcessor(new AjFoamsTracking());
        break;
      case 'bedtastic-tracking':
        $this->setCsvProcessor(new BedtasticTracking());
        break;
      case 'mighty-deals-orders':
        $this->setCsvProcessor(new MightyDealsOrders());
        break;
      default:
        return ['failed'];
    }
    $data = $request->input('data');
    $mode = $request->input('mode');
    $profile = $request->input('profile');
    return $this->fetchImportProfile($profile)->processData($data, $mode);
  }
}
