<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Helper\Magento\Magento;

class MagentoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('magento.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getOrdersByDate(Request $request)
    {
    	
    	 set_time_limit(-1);
        
    	$magento = new Magento();
    	$magento->getOrdersAjax($request->get('datefrom')." ".$request->get('timefrom'),$request->get('dateto')." ".$request->get('timeto'))
    	->removeExistingAjax();
    	
    	return $magento->orders;
    
    }
    
    
    public function getInfo($id){
    	
    	$magento = new Magento();
    	$magento->orders = [['increment_id' => $id]];
    	$magento->getAllInfo()->removeExisting()->formatOrders()->insertOrders();
		    
    }
    
    
    
}




