<?php

namespace App\Http\Controllers\DeliveryDate;

use App\RouteStops;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Helper\Traits\OutHeaders;
use App\Http\Controllers\Controller;

class DeliveryDateController extends Controller
{
  use OutHeaders;
  //
  const DAYS_AHEAD = 14;
  /**
   * @author Giomani Designs (Development Team)
   * @return \Illuminate\Http\Response
   */
  public function index(Request $request) {
   
   $postcode = $request->get('postcode');
   $key = $request->get('key');
   $periodDays = $request->has('days') ? $request->get('days') : -1;
   $items = json_decode($request->get('items'));

    if ($key !== 'VvFYcJT2v4MUz8S2gexBKMVKF9eJeu6w') {
       return abort(400, '[400] KEY ERROR');
    }

    /*
     *	ITEMCODE IS NEEDED TO CONFIRM DATES E.G Collect or for supplier stuff where date not needed.
     *
     */

    $periodDays = $periodDays < 0 ? static::DAYS_AHEAD : $periodDays;
    $routeDays = RouteStops::getByPostcodeArea($postcode);
    $days = $routeDays->pluck('day')->toArray();

    $day0 = new \DateTime;
    $day0->add(new \DateInterval('P2D'));

    $dates = [];
    $interval = new \DateInterval('P1D');
    for ($i = 0; $i < $periodDays; $i++) {
      $day = $day0->format('l');
      if (in_array($day, $days)) {
        $dates[] = $day0->format('l j M Y');
      }
      $day0->add($interval);
    }
    $xml = '<?xml version="1.0" standalone="yes"?><dates>';
    foreach ($dates as $date) {
      $xml .= '<date>' . $date . '</date>';
    }
    $xml .= '</dates>';
    return response($xml)->withHeaders($this->xmlHeaders());
  }
}
