<?php

namespace App\Http\Controllers\Driver;


use App\Driver;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\FilesController;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class DriverController extends Controller
{
	/**
	 * @author Giomani Designs (Development Team)
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function index(Request $request) {
		if ($request->ajax()) {
			return Driver::getAll($request->all());
		}
		return view('drivers.index');
	}
	/**
	 * @author Giomani Designs (Development Team)
	 * @param  intiger  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id) {
		try {
			$driver = Driver::findOrFail($id);
		} catch(ModelNotFoundException $e) {
			return parent::notFound();
		}
		$driverFiles = FilesController::getFileNames('drivers', $id);
		return view('drivers.show', ['driver' => $driver, 'driverFiles' => $driverFiles]);
	}
}
