<?php

namespace App\Http\Controllers\Driver;

use Entrust;
use App\DriverMate;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\FilesController;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class DriverMateController extends Controller
{
  /**
   * @author Giomani Designs (Development Team)
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function index(Request $request) {
    if (!Entrust::can('driver_show')) {
      return parent::notAuthorised();
    }
    if ($request->ajax()) {
      $query = $request->all();
      return DriverMate::getAll($query);
    }
    return view('driver-mates.index');
  }
  /**
   * @author Giomani Designs (Development Team)
   * @author Giomani Designs
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id) {
    if (!Entrust::can('driver_show')) {
      return parent::notAuthorised();
    }
    try {
      $driverMate = DriverMate::findOrFail($id);
    } catch(ModelNotFoundException $e) {
      return parent::notFound();
    }
    $driverMateFiles = FilesController::getFileNames('driver-mate', $id);
    return view('driver-mates.show', ['driverMate' => $driverMate, 'driverMateFiles' => $driverMateFiles]);
  }
}
