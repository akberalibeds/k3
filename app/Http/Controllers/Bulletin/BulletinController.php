<?php
namespace App\Http\Controllers\Bulletin;

use Auth;
use Event;
use Entrust;
use App\Bulletin;
use App\Http\Requests;
use App\UserReadBulletin;
use Illuminate\Http\Request;
use App\Events\BulletinCreating;
use App\Http\Controllers\Controller;

class BulletinController extends Controller
{
  /**
   * @author Giomani Designs
   * @param  string $ids '|' seperated list of bulletin message ids
   * @return json
   */
  public function confirmAsRead($ids) {
    UserReadBulletin::markAsReadByUser(Auth::user()->id, explode('|', $ids));
    return;
  }
  /**
   * @author Giomani Designs
   * @param  string $ids '|' seperated list of bulletin message ids
   * @return json
   */
  public function create() {
    if (!Entrust::can('bulletin_create'))  {
      return parent::notAuthorised();
    }
    return view('bulletins.create');
  }
  /**
   * @author Giomani Designs
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function index(Request $request) {
    if (!Entrust::can('bulletin_create'))  {
      return parent::notAuthorised();
    }
    if ($request->ajax()) {
      return Bulletin::orderBY('id', 'desc')->paginate(config('search.rpp'));
    }
    return view('bulletins.index');
  }
  /**
   * @author Giomani Designs
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function show(Request $request, $id) {
    if (!Entrust::can('bulletin_create'))  {
      return parent::notAuthorised();
    }
    try {
      $bulletin = Bulletin::findOrFail($id);
    } catch (ModelNotFoundException $e) {
      return parent::notFound();
    }
    if ($request->ajax()) {
      return $bulletin;
    }
    return view('bulletins.show', ['bulletin' => $bulletin]);
  }
  /**
   * @author Giomani Designs
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request) {
    if (!Entrust::can('bulletin_create')) {
      return parent::notAuthorised();
    }
    $inputs = $request->except('_method', '_token');
    if (($response = parent::validateForm($inputs, Bulletin::$rules['create'], $request, 'bulletin::index')) !== true) {
      return $response;
    }
    $bulletin = new Bulletin;
    $bulletin->title = $inputs['title'];
    $bulletin->message = $inputs['message'];
    $expires = \DateTime::createFromFormat('Y-m-d', $inputs['expires']);
    if ($expires !== false) {
      $expirey = $expires->format('U');
    } else {
      $date = new \Datetime;
      $date->add(new \DateInterval('P7D'));
      $expirey = $date->format('U');
    }
    $bulletin->expires = $expirey;
    $bulletin->user_id = Auth::user()->id;
    $bulletin->save();
    Event::fire(new BulletinCreating($bulletin->id, $bulletin->title));
    return $bulletin;
  }
}
