<?php

namespace App\Http\Controllers\Tracking;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class TrackingController extends Controller
{
  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create($import)
  {
    $view = 'tracking.' . $import;

    if (!view()->exists($view)) {
      return $this->index();
    }

    return view($view);
  }
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    return view('tracking.index');
  }
}
