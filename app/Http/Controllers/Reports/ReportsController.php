<?php

namespace App\Http\Controllers\Reports;

use Storage;
use App\Company;
use App\Http\Requests;
use App\ReportDataItems;
use App\ReportDataOrders;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ReportsController extends Controller
{
  public static $lastUpdated;
  /**
   * Display a listing of the resource.
   *
   * @author Giomani Designs (Development Team)
   * @return \Illuminate\Http\Response
   */
  public function __construct()
  {
    try {
      self::$lastUpdated = Storage::disk('local')->get('reports.last_dates');
    } catch (\Exception $e) {
      self::$lastUpdated = 'never';
    }
  }
  /**
   * Display a listing of the resource.
   *
   * @author Giomani Designs (Development Team)
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    $companies = Company::all();

    return view('reports.index', ['companies' => $companies, 'lastUpdated' => self::$lastUpdated]);
  }
  /**
   * Return the table data as JSON.
   * @author Giomani Designs (Development Team)
   * @param  Request  $request
   * @param  string  $from
   *
   * @return \Illuminate\Http\Response
   */
  public function itemsData(Request $request, $from, $to, $companies)
  {
    $reportData = ReportDataItems::getReportData ($from, $to, $companies);

    return ['data' => $reportData, 'count' => $reportData->count()];
  }
  /**
   * Display a listing of the resource.
   *
   * @author Giomani Designs (Development Team)
   * @return \Illuminate\Http\Response
   */
  public function items()
  {
    $companies = Company::all();

    return view('reports.items', ['companies' => $companies, 'lastUpdated' => self::$lastUpdated]);
  }
  /**
   * Display a listing of the resource.
   *
   * @author Giomani Designs (Development Team)
   * @return \Illuminate\Http\Response
   */
  public function orders()
  {
    $companies = Company::all();

    return view('reports.orders', ['companies' => $companies, 'lastUpdated' => self::$lastUpdated]);
  }
  /**
   * Return the table data as JSON.
   * @author Giomani Designs (Development Team)
   * @param  Request  $request
   * @param  string  $from
   *
   * @return \Illuminate\Http\Response
   */
  public function ordersData(Request $request, $from, $to, $companies)
  {
    $reportData = ReportDataOrders::getReportData ($from, $to, $companies);

    return ['data' => $reportData, 'count' => $reportData->count()];
  }
}
