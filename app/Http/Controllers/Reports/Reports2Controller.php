<?php
// @author Giomani Designs (Development Team)
//
namespace App\Http\Controllers\Reports;

use Auth;
use Excel;
use Storage;
use App\Company;
use App\Order;
use App\Http\Requests;
use App\ReportDataItems;
use App\ReportDataOrders;
use Illuminate\Http\Request;
use App\Helper\Pdfs\PdfTable;
use App\Http\Controllers\Controller;

class Reports2Controller extends Controller
{
  /**
   * @var
   */
  public static $lastUpdated;
  /**
   * @author Giomani Designs (Development Team)
   * @return \Illuminate\Http\Response
   */
  public function __construct () {
    $this->updateReportData();
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  Request  $request
   * @param  string  $from
   * @return \Illuminate\Http\Response
   */
  public function exportToExcel (Request $request) {
    $headers = explode(',', $request->input('headers'));
    $rows = array_chunk(explode(',', $request->input('rows')), count($headers));
    $title = $request->input('title');

    Excel::create('Filename', function ($excel) use ($title) {
       $excel->setTitle('$title')->setCreator(Auth::user()->id)->setCompany('Giomani Designs Ltd.');
    })->export('xlsx');
  }
  /**
   * @author Giomani Designs (Development Team)
   * @return \Illuminate\Http\Response
   */
  public function index () {
    $companies = Company::all();

    return view('reports-2.index', ['companies' => $companies, 'lastUpdated' => self::$lastUpdated->format('l, j F Y')]);
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  Request  $request
   * @param  string  $from
   * @return \Illuminate\Http\Response
   */
  public function itemsData (Request $request, $from, $to) {
    $reportData = ReportDataItems::getReportData ($from, $to);
    return ['data' => $reportData, 'count' => $reportData->count()];
  }
  /**
   * @author Giomani Designs (Development Team)
   * @return \Illuminate\Http\Response
   */
  public function items () {
    $companies = Company::all();
    return view('reports-2.items', ['companies' => $companies, 'lastUpdated' => self::$lastUpdated->format('l, j F Y')]);
  }
  /**
   * @author Giomani Designs (Development Team)
   * @return \Illuminate\Http\Response
   */
  public function orders () {
    $companies = Company::all();
    return view('reports-2.orders', ['companies' => $companies, 'lastUpdated' => self::$lastUpdated->format('l, j F Y')]);
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  Request  $request
   * @param  string  $from
   * @return \Illuminate\Http\Response
   */
  public function ordersData (Request $request, $from, $to, $companies) {
    $reportData = ReportDataOrders::getReportData ($from, $to, $companies);
    return ['data' => $reportData, 'count' => $reportData->count()];
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  Request  $request
   * @param  string  $from
   * @return \Illuminate\Http\Response
   */
  public function printToPdf (Request $request) {
    $headers = explode(',', $request->input('headers'));
    $rows = array_chunk(explode(',', $request->input('rows')), count($headers));
    $title = $request->input('title');
    $pdf = new PdfTable($headers, $rows, $title);
    $columnWidths = [];
    $columnWidths = array_fill(0, count($headers), 5);
    $columnWidths[0] = 20;
    $columnWidths[] = 7;
    $pdf->setColumnWidths($columnWidths);
    $pdf->table();
    return $pdf->out();
  }
  /**
   * @author Giomani Designs (Development Team)
   * @return void
   */
  public function updateReportData () {
    self::$lastUpdated = ReportDataItems::updateReportItems();
  }
  
  
  public function daily()
  {
		
		$orders = Order::select(\DB::raw("sum(total) as total, sum(paid) as paid, startDate, count(*) as count, sum(total/1.2)-sum(total) as vat "))
						->where(function($query){
								$query->where('startDate','like','%jul-2016')
									->orWhere('startDate','like','%aug-2016')
									->orWhere('startDate','like','%Sep-2016');
						})
						//->whereBetween(\DB::raw("STR_TO_DATE(startDate, '%d-%M-%Y')"), [\DB::raw("STR_TO_DATE('01-Jul-2016', '%d-%M-%Y')"), \DB::raw("STR_TO_DATE('30-Sep-2016', '%d-%M-%Y')")])
						->where(function($query){
								$query->where('orderStatus','Routed')
									->orWhere('orderStatus','Dispatched')
									->orWhere('orderStatus','Refunded')
									->orWhere('orderStatus','Delivered');	
						})
						->where('proforma',0)
						->groupBy(\DB::raw("STR_TO_DATE(startDate, '%d-%M-%Y')"))
						->get();
		
		$totals=[0 => 'Total', 1 => 0 , 2 => 0 , 3 => 0];
		$array=[];
		$i=0;
		foreach($orders as $order)
		{
				$array[$i][0]=$order->startDate;
				$array[$i][1]=$order->count;
				$array[$i][2]=number_format($order->total,2);
				$array[$i++][3]=number_format(abs($order->vat),2);
				
			
			$totals[2]+=$order->total;
			$totals[3]+=abs($order->vat);
			$totals[1]+=$order->count;
			
		}
		
		$totals[2]=number_format($totals[2],2);
		$totals[3]=number_format($totals[3],2);
		$array[]=$totals;
		
		$headers = ['Date','Orders','Total','VAT'];
		$rows = $array;
		$title = "Daily";			
	   	$pdf = new PdfTable($headers, $rows, $title,null,'P');
		$columnWidths = [];
		$columnWidths = array_fill(0, count($headers), 5);
		$columnWidths[0] = 10;
		$columnWidths[1] = 8;
		$columnWidths[2] = 20;
		$columnWidths[3] = 20;
		$pdf->setColumnWidths($columnWidths);
		$pdf->table();
    	return $pdf->out();
		
		//return view('reports-2.daily', ['orders' => $orders , 'totals' => $totals]);
	  
  }
  
}
