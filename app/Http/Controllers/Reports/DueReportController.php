<?php
// Giomani Designs (Development Team)
//
namespace App\Http\Controllers\Reports;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\Helper\Import\DueReport;
use App\Http\Controllers\Controller;
use App\Http\Controllers\CsvImportControllerAbstract;

class DueReportController extends CsvImportControllerAbstract
{
  /**
   * @author Giomani Designs (Development Team)
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function download(Request $request) {
    // if (!Entrust::can('seller_orders_import')) {
    //   return parent::notAuthorised();
    // }
    return ;
  }
  /**
   * @author Giomani Designs (Development Team)
   * @return \Illuminate\Http\Response
   */
  public function getHeadings ($profilegetHeadings) {
    // if (!Entrust::can('seller_orders_import')) {
    //   return parent::notAuthorised();
    // }
    $this->fetchImportProfile('due-report');
    return $this->getImportProfileHeadings();
  }
  /**
   * @author Giomani Designs (Development Team)
   * @return \Illuminate\Http\Response
   */
  public function index() {
    // if (!Entrust::can('seller_orders_import')) {
    //   return parent::notAuthorised();
    // }
    return view('due-report.index');
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function read(Request $request) {
    // if (!Entrust::can('seller_orders_import')) {
    //   return parent::notAuthorised();
    // }
    return $result = $this->readFile($request);
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request) {
    // if (!Entrust::can('seller_orders_import')) {
    //   return parent::notAuthorised();
    // }
    $this->setCsvProcessor(new DueReport());
    $data = json_decode($request->input('data'), true);
    $mode = $request->input('mode');
    $profile = $request->input('profile');
    return $this->fetchImportProfile($profile)->processData($data, $mode);
  }
}
