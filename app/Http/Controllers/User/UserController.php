<?php
// @author Giomani Designs (Development Team)
//
namespace App\Http\Controllers\User;

use Event;
use Auth;
use Hash;
use Entrust;
use App\Role;
use App\User;
use App\UserBookmark;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Events\UserChangingPassword;
use App\Http\Controllers\Controller;
use App\Http\Requests\UserPostRequest;

class UserController extends Controller
{
  /**
   * @author Giomani Designs (Development Team)
   * @param  \Illuminate\Http\Request  $request
   * @return json
   */
  public function addBookmark(Request $request) {
    if (!Entrust::hasRole('staff')) {
      return parent::notAuthorised();
    }
    $bookmark = new UserBookmark;
    $bookmark->description = $request['description'];
    $bookmark->url = $request['url'];
    $bookmark->user_id = Auth::user()->id;
    $bookmark->save();
    return $bookmark;
  }
  /**
   * @author Giomani Designs (Development Team)
   * @return \Illuminate\Http\Response
   */
  public function index() {
    if (!Entrust::hasRole('staff')) {
      return parent::notAuthorised();
    }
    return $this->show(Auth::id());
  }
  /**
   * TODO
   * @param  \Illuminate\Http\Request  $request
   * @return json
   */
  public function removeBookmark(Request $request) {
    if (!Entrust::hasRole('staff')) {
      return parent::notAuthorised();
    }
    return $bookmark;
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id) {
    if (!Entrust::hasRole('staff')) {
      return parent::notAuthorised();
    }
    $user = User::find(Auth::id());
    $userRoles = $user->roles;
    return view('user.show', ['user' => $user, 'userRoles' => $userRoles]);
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function updateName(Request $request) {
    if (!Entrust::hasRole('staff')) {
      return parent::notAuthorised();
    }
    $user = User::find(Auth::user()->id);
    $user->name = $request->input('name');
    $user->save();
    return response(['reply' => $user->name], 200);
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function updatePassword(Request $request) {
    if (!Entrust::hasRole('staff')) {
      return parent::notAuthorised();
    }
    $inputs = $request->only(['old_password', 'new_password', 'new_password_confirmation']);
    if (!Hash::check($inputs['old_password'], Auth::user()->password)) {
      return response(['old_password' => ['Old password is incorrect']], 422);
    }
    if (($response = parent::validateForm($inputs, User::$rules['update-password'], $request, 'admin::stock::create')) !== true) {
      return $response;
    }
    $user = User::find(Auth::user()->id);
    $user->password = Hash::make($request->input('new_password'));
    $user->save();
    Event::fire(new UserChangingPassword());
    return response(['reply' => 'password.changed'], 200);
  }
}
