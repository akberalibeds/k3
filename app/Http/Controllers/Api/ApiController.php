<?php

namespace App\Http\Controllers\Api;


use App\Http\Controllers\Controller;
use App\Helper\Traits\OutHeaders;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\RouteStops;
use App\Order;
use App\AllowedIp;



class ApiController extends Controller
{
	 use OutHeaders;
    /*
	 *	override constructor to remove auth
	 */
	public function __construct()
    {

    }
	
    
    public function ipStatus($ip){
    	AllowedIp::add($ip);
    	return abort('500','Internal Error!!');
    }

	public function api_change_status($orderID,$oldstatus,$newstatus){

		$order = ['new' => $newstatus, 'old' => $oldstatus , 'id' => $orderID];
		return Order::changestatus($order,$orderID);

	}
	
	
	public function barcode($id){
		Order::setBarcode($id);
	}



	/*
	 * MAGENTO request for delivery dates
	 *
	 * @func delivery_dates
	 * @param request 	- Array [postcode,key,days,items]
	 *		postcode  		- Delivery postcode
	 *		key						- a 32 character string 'VvFYcJT2v4MUz8S2gexBKMVKF9eJeu6w'
	 *		days					- (optional - defaults to 14) the number of dates to return
	 *		items					- JSON list of itemcode(s) on order
	 *
	 * @return XML
	 */


	const DAYS_AHEAD = 14;

	public function delivery_dates(Request $request) {



	   $postcode = $request->get('postcode');
	   $key = $request->get('key');
	   $periodDays = $request->has('days') ? $request->get('days') : -1;
	   $items = json_decode($request->get('items'));

		if ($key !== 'VvFYcJT2v4MUz8S2gexBKMVKF9eJeu6w') {
		   return abort(400, '[400] KEY ERROR');
		}


		$periodDays = $periodDays < 0 ? static::DAYS_AHEAD : $periodDays;
		$routeDays = RouteStops::getByPostcodeArea($postcode);
		$days = $routeDays->pluck('day')->toArray();

		$day0 = new \DateTime;
		$day0->add(new \DateInterval('P2D'));

		$dates = [];
		$interval = new \DateInterval('P1D');
		for ($i = 0; $i < $periodDays; $i++) {
		  $day = $day0->format('l');
		  if (in_array($day, $days)) {
			$dates[] = $day0->format('l j M Y');
		  }
		  $day0->add($interval);
		}
		$xml = '<?xml version="1.0" standalone="yes"?><dates>';
		foreach ($dates as $date) {
		  $xml .= '<date>' . $date . '</date>';
		}
		$xml .= '</dates>';
		return response($xml)->withHeaders($this->xmlHeaders());
	  }

}
