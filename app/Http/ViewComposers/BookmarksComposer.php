<?php
// Giomani Designs (Development Team)
//
namespace App\Http\ViewComposers;

use Debugbar;

use Auth;
use App\UserBookmark;
use Illuminate\View\View;

/**
 * @author Giomani Designs (Development Team)
 */
class BookmarksComposer
{
  /**
   * Create a new profile composer.
   *
   * @author Giomani Designs (Development Team)
   * @param  UserRepository  $users
   * @return void
   */
  public function __construct() {
  }
  /**
   * Bind data to the view.
   *
   * @author Giomani Designs (Development Team)
   * @param  View  $view
   * @return void
   */
  public function compose(View $view) {
    if (Auth::user()) {
      $bookmarks = UserBookmark::getForUser(Auth::user()->id);
    } else {
      $bookmarks = [];
    }
    $view->with('bookmarks', $bookmarks);
  }
}
