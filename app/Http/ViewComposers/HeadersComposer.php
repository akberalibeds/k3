<?php
// Giomani Designs (Development Team)
//
namespace App\Http\ViewComposers;

use Auth;
use Illuminate\View\View;

/**
 * @author Giomani Designs (Development Team)
 */
class HeadersComposer
{
  /**
   * @author Giomani Designs (Development Team)
   * @param  UserRepository  $users
   * @return void
   */
  public function __construct() {
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  View  $view
   * @return void
   */
  public function compose(View $view) {
    $headers = [
      'Cache-Control' => 'no-store, no-cache, must-revalidate, max-age=0',
      'Pragma' => 'no-cache',
    ];
    $view->with('headers', $headers);
  }
}
