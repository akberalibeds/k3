<?php
// Giomani Designs (Development Team)
//
namespace App\Http\ViewComposers;

use Auth;
use App\Bulletin;
use Illuminate\View\View;

/**
 * @author Giomani Designs (Development Team)
 */
class BulletinsComposer
{
  /**
   * Create a new profile composer.
   *
   * @author Giomani Designs (Development Team)
   * @param  UserRepository  $users
   * @return void
   */
  public function __construct() {
  }
  /**
   * Bind data to the view.
   *
   * @author Giomani Designs (Development Team)
   * @param  View  $view
   * @return void
   */
  public function compose(View $view) {
    if (Auth::user()) {
      $bulletins = Bulletin::getUnseenForUser(Auth::user()->id);
    } else {
      $bulletins = [];
    }
    $view->with('unreadBulletins', $bulletins);
  }
}
