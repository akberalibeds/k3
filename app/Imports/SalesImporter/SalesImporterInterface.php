<?php

namespace App\Imports\SalesImporter;

interface SalesImporterInterface
{


    /**
     * Each import model should provide a method to retrieve data
     * ....
     * @param $daysAgo
     * @return
     */
    public function pull();


    /**
     * Each import model should provide a method to convert data
     * ....
     * @param $data
     * @return
     */
    public function convert($data);


}