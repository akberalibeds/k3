<?php 

namespace App\Imports\SalesImporter;

use Carbon\Carbon;
use App\Company;

class SalesImporter 
{
    private $vendor;

    public function __construct(SalesImporterInterface $vendor)
    {
        $this->vendor = $vendor;
    }


    /**
     * Import data for each website from online importer(Ebay, Magento, Woocommerce etc.)
     *
     *@return void
     */
    public static function importAll($startDate = null, $endDate = null) 
    {
        $companies = Company::all();

        $startDate = $startDate? $startDate : Carbon::now()->subHours(1);

        $endDate = $endDate? $endDate : Carbon::now();

        foreach($companies as $company)
        {
            if($company->importer != null) {
                echo "importing from $company->company_name \n";
                $vendor_name = $company->importer->class_name;
                $vendor = new $vendor_name($company->company_name, $startDate, $endDate);
                (new static($vendor))->import();
            }
        }
    }

    /**
	 * convert each individual order form online importer and insert them to local database
	 *
     * @return void
     */

    public function import()
    {
        $vendorOrders = $this->vendor->pull();
        $count=count($vendorOrders);
        \Log::info("found $count orders");
        $this->process($vendorOrders);

    }

    /**
     * transform each individual order
     *
     * @param $vendorOrders
     * @return void
     */
    private function process($vendorOrders)
    {
        $transformer = new Transformer($this->vendor->company);

        foreach($vendorOrders as $order)
        {
            $data = $this->vendor->convert($order);
            $transformer->transform($data);
        }
    }


}