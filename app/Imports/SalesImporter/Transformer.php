<?php

namespace App\Imports\SalesImporter;

use App\Company;
use App\Order;
use App\StockItem;
use App\Classes\Sale;

class Transformer
{
    private $company;

    /**
     * Transformer constructor.
     * @param Company $company
     */
    public function __construct(Company $company)
    {
        $this->company = $company;
    }

    /**
     * @param $data
     * @return void
     */
    public function transform($data)
    {
            $data['customer']->save();

            $data['order']['cid'] = $data['customer']->id;
            $data['order']['staffName'] = $this->company->company_name;
            $data['order']['staffid'] = 0;
            $data['order']['ouref'] = $this->company->company_name;

            $data['items'] = $this->convertItems($data['items']);
            $data['order']['items'] = $data['items'];
            $data['order']['companyName'] = $this->company->company_name;

            $duplicateOrder = Order::where([

                ['companyName', $this->company->company_name],
                ['beds', $data['order']['beds'] ]

            ])->exists();
             if ($duplicateOrder) {
               $ref = $data['order']['beds'];
               \Log::info( "Found duplicate: $ref");
echo("Found duplicate: $ref\n");
             }

            if(! $duplicateOrder) {
                $sale = new Sale();
                $sale->newSale((object)$data['order'])
                    ->addItems();

                if($data['order']['deliverBy'] != null) {
                    $sale->addNote("Customer Selected Delivery for ". $data['order']['deliverBy']);
                }

                if($data['order']['customer_note'] != null) {
                    $sale->addNote( $data['order']['customer_note'] );
                }

                $sale->setIno()
                    ->setDeliveryRoute()
                    ->setBarcode()
                    ->securityCheck()
                    ->check_split();
                
                $ref = $data['order']['beds'];
                $id = $sale->id;
                \Log::info( "imported $ref as $id");
echo ("imported $ref as $id\n");
            }

    }

    /**
     * Convert imported line_items and prepare to insert to local database
     *
     * @param $data
     * @return array $items
     */
    private function convertItems($data)
    {
        $orderItems = $data;
        $items = [];
        foreach($orderItems as $item){

            $qty= $item['Qty'];
            $websiteSKU = $item['sku'];
            $parts = explode("/",$websiteSKU);

            // split variation to check which item or sets etc
            if(count($parts)==1){
                $parts = explode("-",$websiteSKU);

                if($parts[0]=="320" || $parts[0]=="D320"){

                    $this->createStandardLineItem($items,$websiteSKU, $item['price'], $qty);

                }
                else if(count($parts)==4){
                    $this->createTwoPartDivan($items,$parts, $item['price'] , $qty);

                }
                else if(count($parts)==6){

                    $this->createTableAndChairs($items, $parts, $item['price'], $qty);
                }
                else{

                    $this->createStandardLineItem($items, $websiteSKU, $item['price'], $qty);
                }
            }
            else{

                $this->splitCompositeItems($items, $parts, $item['price'], $qty);
            }
        }
        return $items;
    }

    private function createStandardLineItem(&$items, $itemCode, $price, $qty)
    {

        $stock = StockItem::itemCodeBeds($this->getSKU($itemCode),$price);
        $var['itemid'] = $stock->id;
        $var['currStock'] = $stock->itemQty;
        $var['itemCode'] = $this->getSKU($itemCode);
        $var['Qty'] = $qty;
        $var['costs'] = $stock->cost;
        $var['price']= $price;
        $var['lineTotal']= $price * $qty;

        $items[]=$var;
    }

    private function createTwoPartDivan(&$items, $parts, $price, $qty)
    {
        $this->createStandardLineItem($items, $itemCode = "$parts[0]-$parts[1]-$parts[2]", $price, $qty);

        $this->createStandardLineItem($items, $itemCode = "$parts[1]-$parts[3]", 0, $qty);

    }

    private function createTableAndChairs(&$items, $parts, $price, $qty)
    {
        $this->createStandardLineItem($items, $itemCode = "$parts[1]-$parts[2]", $price, $qty);

        $this->createStandardLineItem($items, $itemCode = "$parts[3]-$parts[4]", 0, $qty*$parts[5]);

    }

    private function splitCompositeItems(&$items, $parts, $price, $qty)
    {
        $pp=0;
        foreach($parts  as $p){

            $var =[];
            $stock = StockItem::itemCodeBeds($this->getSKU($p),$price);
            $var['itemid'] = $stock->id;
            $var['currStock'] = $stock->itemQty;
            $var['itemCode'] = $this->getSKU($p);
            $var['Qty'] = $qty;
            $var['costs'] = $stock->cost;

            if($pp==0){
                $var['price']= $price;
                $var['lineTotal']= $price * $qty;
            }
            else{
                $var['price']= 0.00;
                $var['lineTotal']= 0.00;
            }
            $items[]=$var;
            $pp++;
        }
    }

    /**
     * Convert the website-SKU version to local sku string
     *
     * @param $itemCode
     * @return string $itemCode
     */
    private function getSKU($itemCode) {

        $itemCode = str_replace("160-","130-",$itemCode);
        $itemCode = str_replace("161-","113-",$itemCode);
        $itemCode = str_replace("921-","901-",$itemCode);
        $itemCode = str_replace("922-","902-",$itemCode);
        $itemCode = str_replace("926-","906-",$itemCode);
        $itemCode = str_replace("927-","907-",$itemCode);
        $itemCode = str_replace("923L-","953L-",$itemCode);
        $itemCode = str_replace("923M-","953M-",$itemCode);
        $itemCode = str_replace("923S-","953S-",$itemCode);

        return $itemCode;
    }
}
