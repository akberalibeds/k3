<?php 

namespace App\Imports\Woocommerce;

use Time;
use App\Order;
use App\Company;
use App\Customer;
use App\OrderItems;
use Automattic\WooCommerce\Client;
use App\Imports\SalesImporter\SalesImporterInterface;
use Carbon\Carbon;

class Woocommerce implements SalesImporterInterface
{

    private $statuses;
    public $company;
    public $woocommerce;
    protected $startDate;
    protected $endDate;

    public function __construct($company_name, Carbon $startDate, Carbon $endDate)
    {
        $this->startDate = $startDate;

        $this->endDate = $endDate;

        $this->statuses = [
            'completed',
            'on-hold'
            ];

        $this->company = Company::where('company_name', $company_name)->first();

        if($this->company == null) {
            throw new \Exception('Company not found.');
        }

        $company = strtolower(str_replace('.', '', $this->company->company_name));

        $this->woocommerce = new Client(
            
            config("services.woocommerce.{$company}.url"), // your store url
            config("services.woocommerce.{$company}.consumer_key"), 
            config("services.woocommerce.{$company}.consumer_secret"),
            [
                'wp_api' => true,
                'version' => 'wc/v2',
                'query_string_auth' => true // Force Basic Authentication as query string true and using under HTTPS
            ]
        );
    }

    /**
     * Return response data from woocommerce
     * ....
     * @param string $status
     * @param int $daysAgo
     * @return array|mixed
     */
    public function pull($daysAgo = 21)
    {
        if( app()->environment() == 'testing') {
            $json_orders = file_get_contents(storage_path('orders.json'));

             return  json_decode($json_orders);
        }
        // $date = Carbon::now()->subHours(16);
        $page_size = 10;
        $statuses = ['completed','processing'];
        
        $result = [];
        foreach ($statuses as $status) {
          $page_number = 0;
          do {
            $page_number +=1;
$params = [
                'status' => $status,
                'page' => $page_number,
                'per_page' => $page_size,
                'after' => $this->startDate->toIso8601String(),
                'before' => $this->endDate->toIso8601String(),

];
            $data = $this->woocommerce->get('orders',$params);
            $result = array_merge($result,$data);

            $request = $this->woocommerce->http->getRequest();
            $response = $this->woocommerce->http->getResponse();
            $headers = $response->getHeaders();
            if (isset($headers['X-WP-TotalPages'])) {
                $total_pages = $headers['X-WP-TotalPages'];
            } else if (isset($headers['x-wp-totalpages'])) {
		$total_pages = $headers['x-wp-totalpages'];
	    } else {
		\Log::info('No totalpages in header');
		\Log::info($response->getCode());
		\Log::info($request->getUrl());
		\Log::info($params);
		\Log::info(json_encode($headers));
                \Log::info(json_encode($response->getBody(),true));
		$total_pages = 1;
            }
          } while ($page_number < $total_pages);
        }
        return $result;


    }

    /**
     * Return an associative array containing
     *
     * "order" => App\Order,
     * "customer" => App\Customer
     * @param $data
     * @return array
     */
    public function convert($data)
    {
        return [
            'order' => $this->makeOrder($data),
            'customer' => $this->makeCustomer($data),
            'items' => $this->makeItems($data)
        ];
    }

    /**
     * Return App\Customer
     * @param $data
     * @return Customer
     */
    public function makeCustomer($data)
    {
      $customer = new Customer();

      $customer->dBusinessName = $data->shipping->first_name ." ". $data->shipping->last_name;

      $customer->dStreet = $data->shipping->address_1 . ' ' . $data->shipping->address_2;
      $customer->dTown= $data->shipping->city;
      $customer->dPostcode = $data->shipping->postcode;

      $customer->businessName = $data->billing->first_name. $data->billing->last_name;
      $customer->street = $data->billing->address_1  . ' ' . $data->billing->address_2;;
      $customer->town = $data->billing->city;
      $customer->postcode = $data->billing->postcode;
      $customer->email1 = $data->billing->email;
      $customer->tel = $data->billing->phone;
      $customer->mob = $this->getMobileNumber($data);
    
      return $customer;
    }

    /**
     * Return App\Order
     * @param $data
     * @return array
     */
    public function makeOrder($data)
    {
        $order = [];

        $order['staffName'] = '';
        $order['staffId'] = 0;
        $order['ouref'] = '';
        $order['companyName'] = '';
        $order['startDate'] = Time::date($data->date_created)->get('d-M-Y');
        $order['startTime'] = Time::date($data->date_created)->get('H:i:s');
        $order['paid'] = $data->date_paid ? $data->total : 0;
        $order['total'] = $data->total;
        $order['beds'] = $data->number;
        $order['startStamp'] = Time::date($data->date_created)->getStamp();
        $order['time_stamp'] = $data->date_created;
        $order['last_trans_id'] = $data->transaction_id;$order['last_trans_id'] = $data->transaction_id;
        $order['deliverBy'] = $this->getDeliveryDate($data);
        $order['customer_note'] = $data->customer_note? $data->customer_note : null;
        $order['paymentType'] = $data->payment_method;
        if ($order['paymentType']=='cod') {
          $order['paymentType']='cash';
        }
        // $order->items = $this->makeItems($data);

        return $order;
    }

    private function getDeliveryDate($data) 
    {
        foreach ($data->meta_data as $meta_data) 
        {
            if(strtolower($meta_data->key) == strtolower('_delivery_date')) {
                 return Time::date($meta_data->value)->get('d-M-Y');
            }
        }

        return null;
    }

    private function getMobileNumber($data) 
    {
        foreach ($data->meta_data as $meta_data) 
        {
            if(strtolower($meta_data->key) == strtolower('_billing_mobile')) {
                 return $meta_data->value;
            }
        }

        return null;
    }

    /**
     * Return array of  App\OrderItems
     * @param $data
     * @return array
     */
    public function makeItems($data)
    {
        $items = [];

        foreach($data->line_items as $item)
        {
            $orderItem = new OrderItems();

            $orderItem->itemCode = html_entity_decode( $item->sku); // you need to unset this value before saving as it is not a field in the database.
            $orderItem->sku = html_entity_decode( $item->sku);
            $orderItem->price = $item->price;
		if ($item->quantity>100) {$item->quantity=100;}
            $orderItem->Qty = $item->quantity;
            $orderItem->lineTotal = $item->price * $item->quantity;
            
            $items[] =  $orderItem->toArray();
        }
      
        return $items;
    }


}
