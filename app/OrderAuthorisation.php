<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderAuthorisation extends Model
{
    protected $fillable = [
        'staff_id',
        'order_id',
        'requested_by',
        'authorised_by',
        'action_type',
        'reason'
    ];

    public function order()
    {
        return $this->belongsTo(Order::class, 'order_id');
    }

    public function staff() 
    {
        return $this->belongsTo(User::class, 'staff_id');
    }

    public function requestedBy() 
    {
        return $this->belongsTo(User::class, 'requested_by');
    }

    public function authorisedBy() 
    {
        return $this->belongsTo(User::class, 'authorised_by');
    }

}
