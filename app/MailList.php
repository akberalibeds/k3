<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MailList extends Model
{
  /**
   * The table associated with the model.
   *
   * @var string
   */
  protected $table = 'mail_lists';
  /**
   * @param  int $uid user id
   * @return Illuminate\Database\Eloquent\Collection
   */
  public function getForUser($uid)
  {
    return MailList::where('user_id', '=', $uid)->get();
  }
}
