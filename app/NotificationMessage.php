<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NotificationMessage extends Model
{
    protected $casts = 
    [
        'date_sent' => 'datetime'
    ];

    protected $fillable = [
        'notificationID',
        'recipient', 
        'recipient_name', 
        'body', 
        'subject', 
        'msgMethod', 
        'is_sent', 
        'err_message', 
        'date_sent'
    ];
 
    public function notification()
    {
        return $this->belongsTo('App\Notification','notificationID');
    }

    public function shortName()
    {
        $method = studly_case((new \ReflectionClass($this->msgMethod))->getShortName());
        
        return $method;
    }
}
