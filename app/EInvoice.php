<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EInvoice extends Model
{
  /**
   * The table associated with the model.
   *
   * @var string
   */
  protected $table = 'eInvoice';
  /**
   * get invoices for a suppier
   *
   * @param  int $id supplier id
   * @return Illuminate\Pagination\LengthAwarePaginator
   */
  public static function getForSupplier($id) {
    return EInvoice::where('id_supplier', '=', $id)
      ->orderBy('id', 'desc')
      ->paginate(config('search.rpp'));
  }
  /**
   *
   * @param  $sid  invoice id
   * @param  $iid supplier id
   * @return
   */
  public static function setAsPaid($sid, $iid) {
    EInvoice::where('id', '=', $id)->
      update(['paid' => '1', 'time' => $time]);
  }
}
