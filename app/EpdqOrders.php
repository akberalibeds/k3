<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EpdqOrders extends Model
{
  /**
   * The table associated with the model.
   *
   * @var string
   */
  protected $table = 'epdq_orders';
  
  public $timestamps = false;
  
 

}
