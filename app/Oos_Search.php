<?php
namespace App;

use Auth;
use Hash;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class Oos_Search extends Model
{
	
	
	protected $table = "oos_search";
	
	
	public static function getItems(){
		
		$items = self::find(1);
		
		return ($items->items);
		
	}
	
	public static function saveItems($items){
	
		$item = self::find(1);
		$item->items = $items;
		$item->save();
		
	}
	
}