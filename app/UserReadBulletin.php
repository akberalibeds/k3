<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserReadBulletin extends Model
{
  /**
   * The table associated with the model.
   *
   * @var string
   */
  protected $table = 'user_read_bulletins';
  /**
   * Get the bulletins that this user has not seen yet.
   *
   * @param int       $uid  user id
   * @param array|int $bIds bulletin ids
   */
  public static function markAsReadByUser($uid, $bIds)
  {
    $data = [];

    foreach ($bIds as $bId) {
      $data[] = [ 'user_id' => $uid, 'bulletin_id' => $bId ];
    }

    static::insert($data);
  }
}
