<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Settings extends Model
{
  /**
   * The table associated with the model.
   *
   * @var string
   */
  protected $table = 'settings';



  protected $fillable = ['id', 'vat', 'showNotes', 'cod', 'wholesale', 'companies', 'payments', 'bedsEmail', 'bedsEmailSend', 'maps_key'];


  public $timestamps = false;

  /**
   * @param  string $key
   * @return string
   */
  public static function get($key) {
    $value = null;
    $settings = self::find(1);
    switch ($key) {
      case 'vat':
           $value = static::getVat();
           break;
      default:
           $value = $settings->{$key};
    }

    return $value;
  }

  public static function getVat(){

		$vat = Settings::select('vat')->get()->first();
		return $vat['vat'];

  }


}
