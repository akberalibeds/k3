<?php

namespace App;

use App\Order;
use App\Helper\Traits\QueryableModel;
use Illuminate\Database\Eloquent\Model;

class Supplier extends Model
{
  use QueryableModel;
  /**
   * @var
   */
  public static $rules = [
    'store' => [
      'supplier_name' => 'required|max:255|unique:supplier',
      'days' => 'required|integer|min:0',
      'print_orders_on_dispatch' => 'required|boolean',
    ],
    'update' => [
      'supplier_name' => 'required|max:255',
      'days' => 'required|integer|min:0',
      'id' => 'required',
      'print_orders_on_dispatch' => 'required|boolean',
    ],
  ];
  /**
   * The table associated with the model.
   *
   * @var string
   */
  protected $table = 'supplier';
  /**
   * @author Giomani Designs (Development Team)
   */
  public static function getPrintOnDispatch() {
    return Supplier::where('print_orders_on_dispatch', '=', 1)->get();
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  array   $queryStrings the query string
   */
  public static function getSuppliers($queryStrings) {
    $query = static::select('*');
    if (count($queryStrings) > 0) {
      $query = parent::modForQueryString($query, $queryStrings);
    }
    return $query->orderBy('supplier_name', 'asc')->paginate(config('search.rpp'));
  }

// /  orderBy('supplier_name', 'asc')->paginate(config('search.rpp'));


  public static function moveSupplier ($request) {
    $suppliers = self::getSupplierList();
    $data = $request->get('data');
    $oid_list = $data['ids'];
    $oids = array();
    foreach ($oid_list as $oid) {
      $oids[] = $oid['id'];
    }
    $these = array('Durest Beds','Bed Express');
    $sup = '';
    if ($data['supplier'] === '0') {
      $updater = [
        'ouref' => 'Beds.co.uk',
        'id_supplier' => '0',
      ];
      foreach ($oid_list as $oid) {
        
		Order::setDeliveryRoute($oid['id']);
     	
	  }
    } else {
      $updater = [
        'ouref' => $suppliers[$data['supplier']]['supplier_name'] ,
        'id_supplier' => $data['supplier'],
      ];
      if (in_array($suppliers[$data['supplier']]['supplier_name'], $these)) {
        $updater['delRoute'] = $suppliers[$data['supplier']]['supplier_name'];
        $updater['id_delivery_route'] = Routes::id_route($suppliers[$data['supplier']]['supplier_name']);
      }
    }
    
	Order::whereIn('id',$oids)->update($updater);
    
	/*
    unset($suppliers[$request->get('supplier')]);

    $array="";
    foreach($suppliers as $s){
      $array[]=$s['supplier_name'];
    }

    $array = implode(",",$array);
    */
  }
	/*
	* return Supplier List
	*/
	public static function getSupplierList(){

		$rows = self::all()->toArray();
		$suppliers= array();

		foreach($rows as $row){

			$suppliers[$row['id']]= $row;

		}


		return $suppliers;
	}
}
