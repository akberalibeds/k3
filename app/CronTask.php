<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Order;

class CronTask extends Model
{
  /**
   * The table associated with the model.
   *
   * @var string
   */
  protected $table = 'cron_tasks'; 

  public $timestamps = false;

  public $fillable = ['name', 'description', 'value'];
  
  
  
  
  
  
  
}