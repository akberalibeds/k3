<?php
namespace App\Filters\Orders;

use App\Order;
use App\Events\Wowcher\OrderDispatched;
use App\Helper\Wowcher\Wowcher;

class WowcherOrderDispatched extends FilterOrder implements OrderActionInterface
{

    public function filter()
    {
        return $this->baseFilter()->where([
            
            ['companyName' , '=', 'wowcher'],
            ['orderStatus', '=', 'routed'] //To be confirmed!

        ]);
    }

    public function get_event_object($order)
    {
        return new OrderDispatched($order);
    }

    public function eventName()
    {
        return 'dispatched';
    }
}