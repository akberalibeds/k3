<?php
namespace App\Filters\Orders;

interface OrderActionInterface 
{
    public function eventName();
    
    public function filter();

    public function get_event_object($order);
}