<?php
namespace App\Filters\Orders;

use App\Order;

abstract class FilterOrder
{

    public function baseFilter()
    {
        return Order::leftJoin('order_events', function($join) {
            $join->on('orders.id', '=', 'order_events.order_id')->where('event_type' , '=', $this->eventName());
        })
                ->where('order_events.order_id',null)
                ->where('time_stamp','>',\Carbon\Carbon::now()->subDays(7)->format('Y-m-d'));
    }
}