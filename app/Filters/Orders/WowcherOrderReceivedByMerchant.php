<?php
namespace App\Filters\Orders;

use App\Order;
use App\Events\Wowcher\OrderReceivedByMerchant;
use App\Helper\Wowcher\Wowcher;

class WowcherOrderReceivedByMerchant extends FilterOrder implements OrderActionInterface
{

    public function filter()
    {
        return  $this->baseFilter()->where('companyName' , '=', 'wowcher');
        
    }
    public function eventName()
    {
        return 'received by merchant';
    }

    public function get_event_object($order)
    {
        return new OrderReceivedByMerchant($order);
    }
}