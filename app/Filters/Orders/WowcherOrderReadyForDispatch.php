<?php
namespace App\Filters\Orders;

use App\Order;
use App\Events\Wowcher\OrderReadyForDispatch;
use App\Helper\Wowcher\Wowcher;

class WowcherOrderReadyForDispatch extends FilterOrder implements OrderActionInterface
{

    public function filter()
    {
        return $this->baseFilter()->where([
            
            ['companyName' , '=', 'wowcher'],
            ['orders.done', '=', 1]
        
        ]);
    }

    public function get_event_object($order)
    {
        return new OrderReadyForDispatch($order);
    }

    public function eventName()
    {
        return 'ready for dispatch';
    }
}