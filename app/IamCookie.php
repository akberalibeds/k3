<?php

namespace App;

use Cookie;

/**
 * @author Giomani Designs (Development Team)
 */
class IamCookie
{
  /**
   * @var
   */
  private static  $iamCookieName = 'iam';
  /**
   * @var
   */
  private static $iamCookieLifetime = 525600;
  /**
   *
   */
  public static function getCookie() {
    return hex2bin(Cookie::get('iam', '5b746f646f5d'));
  }
  /**
   * @param string $iamCookie
   */
  public static function setCookie ($iamCookie) {
    $val = bin2hex($iamCookie);
    Cookie::queue('iam', $val, 525600, null, null, false, false);
  }
}
