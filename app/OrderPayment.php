<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderPayment extends Model
{
  /**
   * The table associated with the model.
   *
   * @var string
   */
  protected $table = 'orderPayments';
  
  public $timestamps = false;
  
  public $fillable = ['stamp', 'paid', 'cardNo', 'exp', 'auth', 'oid', 'type', 'staff', 'id_staff', 'id_payment_type'];
  
  public static function addPayment($payment){
		
		$p = new self($payment);
		$p->save();
		
	}
  
  
}
