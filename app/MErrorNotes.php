<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;

class MErrorNotes extends Model
{
    //
    protected $table = 'm_error_notes';
    
    
    
    public static function getNotes($id){
    	
    	return self::select('m_error_notes.*','users.name')
    				->join('users','m_error_notes.staff_id','=','users.id')
    				->where('oid',$id)
    				->get();
    
    }
    
    
    public static function addNote($request){
    	$n = new self();
    	$n->oid=$request->oid;
    	$n->cost=$request->cost;
    	$n->note =$request->note;
    	$n->staff_id = Auth::user()->id;
    	$n->save();
    }
    
}
