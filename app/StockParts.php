<?php

namespace App;

use App\Warehouse;
use App\WarehouseStock;
use Illuminate\Database\Eloquent\Model;

class StockParts extends Model
{
  /**
   * The table associated with the model.
   *
   * @var string
   */
  protected $table = 'stock_parts';
  /**
   * @param  int $uid user id
   * @return Illuminate\Database\Eloquent\Collection
   */
  public $timestamps = false;

  public $fillable = ['id', 'parent_id', 'part'];

  public function stockItem()
  {
    return $this->belongsTo('App\StockItem', 'parent_id', 'id');
  }

  public function warehouseStocks()
  {
    return $this->hasMany('App\WarehouseStock', 'stock_parts_id');
  }

  public function totalStockQuantity() {
    return $this->warehouseStocks->sum('quantity');
  }
  
  /**
   * Update Stock Item Parts amount
   *  If the items is less than the number of parts,
   *  then update to add the remaining amount
   *  Else don't do anything
   * @param StockItem Stock Item ID
   * @todo Check about Warehouse Issue
   */
  public static function updateStockParts(StockItem $stockItem)
  {
    $stockParts = $stockItem->stockParts;
    $stockPartAmount = $stockParts->count();
    if ($stockItem->pieces > $stockPartAmount) {
      $remaining = $stockItem->pieces - $stockPartAmount;
      $count = $stockPartAmount;
      for ($i = 0; $i < $remaining; $i++) {
        $sp = new StockParts;
        $sp->parent_id = $stockItem->id;
        $sp->part = "Box " . ($count + 1);
        $sp->quantity = 0;
        $stockItem->stockParts->push($sp);
        $count++;
      }
    }
    // StockParts::updateWarehouseStock($stockItem);
  }

  public static function updateWarehouseStock(StockItem $stockItem)
  {
    // $warehouses = [1,2,8,9,10,11,12,13];
    $allwarehouses = Warehouse::getWarehouses();
    foreach ($allwarehouses as $warehouse) {
      $warehouses[] = $warehouse->id;
    }
    
    // Validation
    foreach ($warehouses as $w) {
      foreach ($stockItem->stockParts as $p) {
        // validate if stock part is in warehouse, if not create new stock part for it
        // foreach ($p->warehouseStocks as $d) {
          // dd($p->warehouseStocks->contains('warehouse_id', $w));
          // dd($d);
        if (!$p->warehouseStocks->contains('warehouse_id', $w)) {
          $ws = new WarehouseStock();
          $ws->warehouse_id = $w;
          $ws->stock_parts_id = $p->id;
          $ws->quantity = 0;
          $p->warehouseStocks->push($ws);
          $ws->push();
        }
      }
    }
  }
}
