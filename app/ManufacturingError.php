<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Storage;
use File;
use App\Helper\Traits\QueryableModel;

class ManufacturingError extends Model
{
	
	use QueryableModel;
	/**
	 * The table associated with the model.
	 *
	 * @var string
	 */
	protected $table = 'manufacturing_error';

	/**
	 * Storage path for manufacturing error files
	 */
	private static $storage = "manufacturingErrors/";
	
	
	
	
	public static function getAll($queryStrings){
		$query = self::select(\DB::raw('manufacturing_error.*'),'itemCode')
					->join('stock_items','manufacturing_error.item_id','=','stock_items.id');
					
					if (count($queryStrings) > 0) {
						$query = parent::modForQueryString($query, $queryStrings);
					}
					return $query->orderBY('id', 'desc')->paginate(config('search.rpp'));
	}
	
	
	
	public static function saveFile($id,$file){
		return (string)Storage::disk('public')->put(self::$storage."$id/".$file->getClientOriginalName(),  File::get($file));
		//$destinationPath = '';
		//$file->move($destinationPath, $file;
		//return (String)Storage::disk('public')->put(self::$storage."file.txt");
		
	}
	
	public static function addError($request){
		
		$error = new self();
		$data = $request->except('files','_token');
		foreach($data as $k => $v){
			$error->{$k}=$v;
		}
		
		$error->save();
		
		$files = $request->hasFile("files") ? $request->file("files") : [];
		foreach ($files as $file){
			self::saveFile($error->id, $file);
		}
		
		return $error->id;
	}
	
	public static function getError($id){
		$error = self::select(\DB::raw('manufacturing_error.*'),'itemCode')
					->join('stock_items','manufacturing_error.item_id','=','stock_items.id')
					->where('manufacturing_error.id',$id)
					->first();
	
		$notes  = MErrorNotes::getNotes($error->order_id);
		
		$files = Storage::disk('public')->files(self::$storage.$id."/");
					
		return ['error' => $error , 'errorFiles' => $files , 'notes' => $notes];
	}
	
	public static function upload($request){
		$files = $request->hasFile("files") ? $request->file("files") : [];
		foreach ($files as $file){
			ManufacturingError::saveFile($request->get('id'), $file);
		}
	}
	

}
