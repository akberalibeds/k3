<?php

namespace App\Jobs;

use Log;
use Mail;
use App\Links;
use App\Order;
use App\Direct;
use App\Routes;
use App\Customer;
use App\Jobs\Job;
use App\Helper\SMS;
use App\Helper\Ebay\Ebay;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class NotifyCustomers extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    private $sms;
    private $ebay;
    private $isSmsConnected;
    

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->sms = new SMS();

        $this->ebay = new Ebay();
        
        $this->isSmsConnected = $this->sms->is_connected();
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->resetLinkStatus();
        
        $links = $this->getOrdersToNotify(Links::linksForInitialNotification());

        $this->log("Found " . count($links) ." links for initial notification");

        if( ! empty($links) ) {

            $this->log('About to send notifications');

            $this->sendNotification($links);
        }

        
        $links = $this->getOrdersToNotify(Links::linksForRepeatNotification());

        $this->log("Found " . count($links) ." links for repeat notification");

        if( ! empty($links) ) {

            $this->log('About to send notifications');

            $this->sendNotification($links);
        }

    }
    
    function resetLinkStatus() {
      echo Links::whereHas('order',function($qry) {$qry->where('done',1);})->update(['done'=>1,'sent'=>1]);
      Links::where('done',1)->where('sent',0)->update(['sent'=>1]);
    }

    /**
     * @param Collection $links
     * @return void
     * Loop through orders and send an email and and SMS
     */
    private function sendNotification($links)
    {
        foreach($links as $link) {

            /**
             * @var array $attributes
             */
            $attributes = $this->getAttributes($link);
                    
            $isSmsSent = $this->sendSMS($attributes['tel'],$attributes['mob'], $attributes['code']);
            
            $isEmailSent = $this->sendEmail($attributes['name'], $attributes['email'], $attributes['code']);

            $isEbayMessageSent = $this->sendEbayMessage($link);
            
            

            if($isSmsSent || $isEmailSent || $isEbayMessageSent) {
                $this->updateLink($link);
            }
        }
    }

    /**
     * @param Links $link
     * @return array
     */
    private function getAttributes(Links $link)
    {
        $direct = Direct::forOrder($link->order->id);
        if ($direct) {
          $tel = $direct->tel;
        } else {
          $order=$link->order;
          $customer=$order->customer;
          $tel=$customer->tel;
        }
        $tel=$this->getNum($tel);

        return [
            'tel' => $direct? $this->getNum($direct->tel) : $this->getNum($link->order->customer->tel),
            'mob' =>  $direct? $this->getNum($direct->mob): $this->getNum($link->order->customer->mob),
            'code' => $link->code,
            'email' => $direct? $direct->email : $link->order->customer->email1 ,
            'name' => $direct? $direct->name : $link->order->customer->dBusinessName
        ];
    }
          
    /**
     * @param string $tel
     * @param string $mob
     * @param string $code
     * @return bool was message sent?
     */
    private function sendSMS($tel, $mob, $code)
    {
        if( $this->isSmsConnected && isset($mob) && is_numeric($mob)) {

            try {

                // $this->sms->sendSMS($tel.",".$mob,
                // "Due to COVID 19 please expect delays however our delivery team are doing their upmost best to get all orders out as quick as possible and will be in touch to confirm your delivery date and time. Please ensure correct contact details are provided."
                // );
                
                $this->sms->sendSMS($tel.",".$mob,
                "Please Choose A Delivery Date for your furniture order\nparcel id: 
                $code\nPlease go to:\nhttp://premierdeliveries.co.uk?id=".$code
                );

                $this->log("SMS sent to {$tel} with code: {$code}");

                return true;

            } catch (\Exception $e) {

                $this->log($e->getMessage());

                return false;
            }
        }

        return false;

    }

    /**
     * @param string $name
     * @param string $email
     * @param string $code
     * @return bool was email sent?
     */
    private function sendEmail($name, $email, $code)
    {
        $emailSettings = $this->getEmailSettings();

        if( isset($email) && filter_var($email, FILTER_VALIDATE_EMAIL) ) {
            
          //  try {

                Mail::send('emails.notification', ['code' => $code], function ($m) use ($emailSettings, $email, $code, $name) {
                    $m->from($emailSettings->email, 'Premier Deliveries');
        
                    $m->to($email, $name)->subject("Please Choose a Delivery Date for your Furniture Order {$code}");
                });

                $this->log("Mail sent to {$name} with email: {$email} and code: {$code}");
                
                return true;

            //} catch(\Exception $e) {

//                $this->log($e->getMessage());
//                
//                return false;
            //}
         
        }

        return false;
     
    }

     /**
     * @param Links $link
     * @return bool was ebay message sent?
     */
    private function sendEbayMessage(Links $link)
    {
        if(isset($link->order->ebayID) && $link->order->sender == '0') {

            $emailLink = "http://premierdeliveries.co.uk?id=".$link->code;

            $item=explode("-",$link->order->ebayID);
            $item=$item[0];

            $data = [
                'item' => $item,
                'subject' => "Please Choose a Delivery Date for your Order ".$link->code,
                'body' => "Please copy and paste the below link into your browser to choose a delivery date\n\n$emailLink\n\nmany thanks\n\nTHIS IS AN AUTOMATED MESSAGE PLEASE DO NOT REPLY",
                'buyer' => $link->order->customer->userID ,
            ];
            
            
            // 'subject' => "Please note about your furniture order",
            //     'body' => "Due to COVID 19 please expect delays however our delivery team are doing their upmost best to get all orders out as quick as possible and will be in touch to confirm your delivery date and time. Please ensure correct contact details are provided.",
            //     'buyer' => $link->order->customer->userID ,
//'subject' => "Please Choose a Delivery Date for your Order ".$link->code,
// 'body' => "Please copy and paste the below link into your browser to choose a delivery date\n\n$emailLink\n\nmany thanks\n\nTHIS IS AN AUTOMATED MESSAGE PLEASE DO NOT REPLY",
//                 'buyer' => $link->order->customer->userID ,

            try {

                $this->ebay->send_message($data);

                $this->log("Ebay message sent to customer userID: ".$data['buyer']. " with message: ".$data['body']);

                return true;
                
            } catch(\Exception $e) {

                $this->log($e->getMessage());

                return false;
            }
          
        }

        return false;
        
    }

    /**
     * @return array
     */
    private function getEmailSettings()
    {
        return \DB::table('emailSettings')->select('*')->where('name', '=', 'pd')->first();
    }
    
    /**
     * @param Collection $links
     * @return App\Order that an SMS needs to be sent
     */
    public function getOrdersToNotify($links)
    {
        $data = [];

        foreach($links as $link) {

                    $route = $link->order->route;
                    if (!$route) {
                      $route=$this->getRoute($link->order);
                    }
                    $send = (bool) $route->msg;
                    
                    $shouldSendItemCount = 0;

                    foreach($link->order->items as $item) {
    
                        $go = $this->inStock($item); 
                      
                        if($this->isItemReady($go, $send, $item->blocked) == true) {
                            $shouldSendItemCount ++;
                        }
                    }
                    
                    if($shouldSendItemCount>0 && $shouldSendItemCount == count($link->order->items)); {
                        $data [] = $link;
                    }
             
        }

        return $data;
    }

    /**
     * @param Order $order
     * @return bool
     */
    private function getSendAttribute(Order $order)
    {
        $route = $this->getRoute($order);

        if (! isset($route)) {
            $send = false;

        } else {
            $msg = $route->msg;
        
            $send = ($msg == 1) ? true : false ; 
        }

        return $send;

    }

    /**
     * @param Order $order
     * @return App\Routes
     */
    private function getRoute(Order $order)
    {
        return Routes::where('name', $order->delRoute)->first();
    }
    
    /**
     * @param string $go
     * @param bool $send
     * @param int $blocked
     * @return bool
     */
    private function isItemReady($go, $send, $blocked)
    {
        return ($go == "yes" && $send == true && $blocked != 1);
    }

    /**
     * @param Order $order
     * @return bool
     */
    private function hasBeenProcessed(Order $order)
    {
        return ($order->orderStatus == "Delivered" || $order->orderStatus == "Routed" || $order->orderStatus == "Dispatched");
    }

    /**
     * @param Links $link
     * @return bool
     */
    private function updateLink(Links $link)
    {
       $link->time = date('d-M-Y H:i:s');
       $link->done = 1;
       $link->sent = 1;
        
       $link->save();

       $this->log("Updated link with id: {$link->id_col}");
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection;
     */
    public function getLinks() 
    {
        return Links::linksToNotifyCustomers();
    }

    /**
     * return yes if in stock 
     * or no (if not in stock or if stock is low)
     * @param OrderItem $item
     * @return string
     */
    public function inStock($item)
    {
        $red = 0;
        $yellow =0;
        $green = 0;
        $inStock = [];
        $noStock = [];
   
        $none = FALSE;

        if(count($item->stock)) { 
            if($item->stock->actual > 0) {
                $availableStock = $item->stock->actual - $item->Qty;

                if($availableStock > -1) {
                    $inStock[]=$item->id.':'.$item->Qty;
                    $green++ ;
                } else {
                        $inStock[]=$item->id.':'.$item->currStock;
                        $noStock[]=$item->id.':'.abs($availableStock);	
                }
            } else {
                $noStock[]=$item->id.':'.$item->Qty;
            }
        }
  


        if (count($inStock) >0 ) { 

        }
		// skip this order as nothing is in stock 
        else { 
            $red++; 
        }
        // put no stock items in to new order
        if(count($noStock) > 0 && $none == FALSE) $yellow++;
        
        if ($green > 0 && $red == 0 && $yellow == 0) return "yes";

        if($yellow > 0 && $green > 0) return "no"; 

        if($red > 0 && $yellow > 0) return "no"; 

    }

    /**
     * @param string $str
     * @return string
     */
    public function getNum($str)
    {
		
		$tel=preg_replace('/\s+/', '', $str);
		$tel=str_replace("/",",",$tel);
		
		if (substr($tel, 0, 3) === '447' && strlen($tel)==12){ return $tel; }	
		else if (substr($tel, 0, 2) === '07' && strlen($tel)==11){ return "44".substr($tel, 1);	}
		else if (strlen($tel)<11){ return 0; }
		
		else if (strlen($tel)>12 && strpos($tel,',') !== false){ 
		$as=explode(",",$tel);
		$tel="";
		$i=0;
		foreach($as as $a){
		if (substr($a, 0, 2) === '07' && strlen($a)==11){if($i>0){$tel.=",";}$tel.= "44".substr($a, 1);}
		if (substr($a, 0, 3) === '447' && strlen($a)==12){if($i>0){$tel.=",";}$tel.=$a;}
		$i++;
		}
		
		return ltrim($tel,","); 
		}
		
		else if (!ctype_alpha($tel)) {
		$as= explode("Evetel",$tel);
		$tel="";
		$i=0;
		foreach($as as $a){
		if (substr($a, 0, 2) === '07' && strlen($a)==11){if($i>0){$tel.=",";}$tel.= "44".substr($a, 1);}
		if (substr($a, 0, 3) === '447' && strlen($a)==12){if($i>0){$tel.=",";}$tel.=$a;}
		$i++;
		}
		return ltrim($tel,",");
		}
		
		else {
              return 0; 
        }
	
    }
    
    private function log($data) {
        file_put_contents(storage_path('logs/NotifyCustomersJobLog.log'), date('c')." - " . $data . PHP_EOL , FILE_APPEND);
    }

}
