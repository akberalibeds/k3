<?php
//TODO: evaluate if this is any use at all
namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

abstract class BulkProcess extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    const STATUS_NOT_STARTED=1;
    const STATUS_STARTING=2;
    const STATUS_IN_PROGRESS=3;
    const STATUS_SLEEPING=4;
    const STATUS_FINISHING=5;
    const STATUS_COMPLETE=6;
    
    public function __construct()
    {
 
    }

    /*
     * Override if necessary
     */
    function setup() {
      
    }
    /*
     * Only needed if we're not using $input
     */
    function acquireData($start, $count) {
      return [];
    }
    /*
     * Override if necessary
     */
    function cleanup() {
      
    }

    /*
     * Do the work here!
     */
    abstract function process($item);

    
    /*
     * access this to fold in accumulated results
     */
    protected $accumulator = [];

    /*
     * override if necessary
     */
    protected $batch_size = 10;
    /*
     * where to send user when task complete
     */
    protected $continue_route;

    
    private $input = [];
    private $output = [];
    private $errors = [];
    private $pointer = 0;
    
    private $session_id;
    
    
    private $status=self::STATUS_NOT_STARTED;
    
    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
      switch ($this->status) {
        case self::STATUS_NOT_STARTED:
          $this->status=self::STATUS_STARTING;
          try {
            $this->setup();
          } catch (RuntimeException $ex) {
            
          }
          $this->status=self::STATUS_IN_PROGRESS;
          break;
        case self::STATUS_IN_PROGRESS:
            $this->manage_process();
          break;
        case self::STATUS_FINISHING:
          $this->cleanup();
          $this->status = self::STATUS_COMPLETE;
          break;
      }

      $copy = clone($this);
      $copy->job=null;
      
      if ($this->status != self::STATUS_COMPLETE) {
//        dispatch($copy);
      }
    }
    
    private function manage_process() {
      $start = $this->pointer;
      $count = $this->batch_size;
      
      if (count($this->input) >0) {
        $data = array_slice($this->input, $start, $count);
      } else {
        $data = $this->acquireData($start,$count);
      }
      
      foreach ($data as $item) {
        try {
          $result = $this->process($item);
          if ($result) {
            $this->output []= $result;
          }
        } catch (RuntimeException $ex) {
          $this->errors []= $ex;
        }
        $this->pointer++;
      }
      if (count($data)==0) {
        $this->finish();
      }
    }
    
    public function appendInput($data) {
      $this->input[] = $data;
    }

    public function appendOutput($data) {
      $this->output[] = $data;
    }
    
    public function finish() {
      $this->status = self::STATUS_FINISHING;
    }
    
    
    public function getSessionId() {
      if (!isset($this->session_id)) {
        $this->session_id = 'BULK_' . __CLASS__ . '_' . uniqid();
      }
      return $this->session_id;
    }
    
    public function getStatus() {
      return [
        self::STATUS_NOT_STARTED => 'Not started',
        self::STATUS_STARTING => 'Starting',
        self::STATUS_IN_PROGRESS => 'In progress',
        self::STATUS_SLEEPING => 'Sleeping',
        self::STATUS_FINISHING => 'Finishing',
        self::STATUS_COMPLETE => 'Complete',
        
      ][$this->status];
    }
    
    function getProgress() {
      return $this->pointer;
    }
    function getTotalItems() {
      return count($this->input);
    }
}
