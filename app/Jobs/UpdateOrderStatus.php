<?php

namespace App\Jobs;

use App\Order;
use App\Jobs\Job;
use App\OrderEvent;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class UpdateOrderStatus extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;


    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $actions = [
            'App\Filters\Orders\WowcherOrderReceivedByMerchant',
            'App\Filters\Orders\WowcherOrderReadyForDispatch',
            'App\Filters\Orders\WowcherOrderDispatched',
        ];

        foreach($actions as $action_class)
        {
            $action = new $action_class();


            $filtered_orders = $action->filter()->get();
            foreach($filtered_orders as $filtered_order)
            {
                $this->addOrderToOrderEvents($filtered_order, $action);
                event($action->get_event_object($filtered_order));
                
            }
        }

    }

    public function addOrderToOrderEvents($order, $action)
    {
        
            $orderEvent = new OrderEvent;
            $orderEvent->event_type = $action->eventName();
            $orderEvent->done = true;
            $orderEvent->order_id = $order->id;

            $orderEvent->save();

            
    }

}
