<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class TestBulk extends BulkProcess
{
    public function __construct()
    {
        $x=1;
    }

    public function setup() {
      for ($i=0; $i<100; $i++) {
        $this->appendInput((object)['value'=>$i]);
      }
    }
    
    public function process($item) {
      \Log::info('Processing ' . $item->value);
    }
}
