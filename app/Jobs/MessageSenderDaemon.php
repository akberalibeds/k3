<?php

namespace App\Jobs;

use App\Jobs\Job;
use Carbon\Carbon;
use App\Notification;
use App\NotificationMessage;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class MessageSenderDaemon extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    public $method;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($method = NULL)
    {
        $this->method = $method;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $messageCount = $this->getMessagesByMethod($this->method)->count();

        echo PHP_EOL ."FOUND {$messageCount} MESSAGES THAT NEED TO BE SENT.". PHP_EOL;

        //For each messages in the notification message table
        foreach ($this->getMessagesByMethod($this->method)->limit(1000)->get() as $message) {

            //Get the notification type
            $type = $message->notification->typeInstance();

            if ( !empty($type->allowed_methods() ) && in_array($message->msgMethod, $type->allowed_methods()) ) {
                
                $response = (new $message->msgMethod)->send($message);
                
                if ($response['sent'] == true) {

                    $message->notification->update([
                        'hasSent' => 1,
                    ]);
                    
                    $message->update([
                        'is_sent' => 1,
                        'err_message' => 'Sucessfully sent',
                        'date_sent' => Carbon::now()
                    ]);
                    
                    $output = "SUCCESS! Notification of type {$message->notification->type} was sent to customer: {$message->notification->customerID} with orderID: {$message->notification->orderID} with method: {$message->msgMethod}". PHP_EOL;
                    echo $output;
                    \Log::info($output);
                    
                } else {

                    $message->update([
                        'err_message' => $response['errors']
                    ]);
                    
                    $output = "FAILURE! Notification of type {$message->notification->type} was NOT sent to customer: {$message->notification->customerID} with orderID: {$message->notification->orderID} with method: {$message->msgMethod} Reason {$message->err_message}". PHP_EOL;
                    echo $output;
                    \Log::info($output);
                }
            }
        }   
    }

    public function getMessagesByMethod($method)
    {
        if (! is_null($this->method)) {
            
            return NotificationMessage::where('is_sent', false)->whereNull('err_message')->whereIn('msgMethod', [get_class($method)]);
        }

        return NotificationMessage::where('is_sent', false)->whereNull('err_message');
    }
}
