<?php

namespace App\Jobs;

use App\Jobs\Job;
use App\Notification;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Notifications\NotificationMethod;

class MessageGeneratorDaemon extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $notificationCount = Notification::where('hasSent', false)->count();

        echo "Found {$notificationCount} notifications that a message needs to be generated for.". PHP_EOL;

        foreach (Notification::where('hasSent', false)->where('cantsend',false)->limit(100)->get() as $notification) {
            $hasmessage=false;
            $typeInstance = $notification->typeInstance();
            foreach ($typeInstance->allowed_methods() as $methodToSendNotification) {
                
                $method = new $methodToSendNotification;

                if ( ! $this->isMethodPresent($methodToSendNotification, $notification)) {
                    if( $method->validateRecipient($notification)) {
                      
                        try {
                        $body = $typeInstance->messageText($method->format(), $notification);
                        $subject = $typeInstance->messageSubject($notification);
                        $recipient = $method->getRecipientContact($notification);

                        $notification->messages()->create([
                            'recipient' => $recipient,
                            'recipient_name' => $notification->customer->dBusinessName,
                            'body' => $body,
                            'subject' => $subject,
                            'msgMethod' => $methodToSendNotification
                        ]);

                        $hasmessage = true; 
                        $output = "Message has SUCCESSFULLY been created for {$notification->id} on {$methodToSendNotification}" .PHP_EOL;
                        } catch (\Exception $ex) {
                          $output = "Could not generate message for notification {$notification->id} because {$ex->getMessage()}";
                        }
                        echo $output;
                        \Log::info($output);
                    } else {
                        $output = "Message VALIDATION FAILED for {$notification->id} on {$methodToSendNotification}" .PHP_EOL;
                        echo $output;
                        \Log::info($output);
                    }

                } else {
                    
                    $output = "Message ALREADY EXISTS for {$notification->id} on {$methodToSendNotification}". PHP_EOL;
                    echo $output;
                    \Log::info($output);
                }
            }
            if (! $hasmessage) {
              $notification->cantsend=true;
              $notification->save();
            }
        }
    }

    private function isMethodPresent($method, $notification)
    {
        if ($notification->messages->isEmpty()) return false;

        return $notification->messages()->where('msgMethod', $method)->exists();
    }
}
