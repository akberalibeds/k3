<?php

namespace App\Jobs;

use App\Jobs\Job;
use App\Notification as N;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Notifications\NotificationType;

class SelectionDaemon extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    public $notificationTypes = [];

    /**
     * Create a new job instance.
     *
     * @param $notificationType
     */
    public function __construct($notificationType)
    {
        $this->notificationTypes [] = $notificationType;     
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        foreach ($this->notificationTypes as $notificationType) {

            $typeInstance = NotificationType::instantiate($notificationType);
            
            $this->generateNotifications($typeInstance);

        }
        
        foreach ($this->notificationTypes as $notificationType) {

           $typeInstance = new $notificationType;
           
           if ($typeInstance->repeatAfter()) {
               $typeInstance->repeatNotification = true;
           }
           
           $this->generateNotifications($typeInstance);

        }
        
    }
    
    function generateNotifications($typeInstance) 
    {
        $data = $this->getDataForNotification($typeInstance);

        $ordersCount = count($data);

        echo PHP_EOL ."Generating {$ordersCount}  {$typeInstance->type()} notifications.". PHP_EOL;

        foreach ($data as $item) {

            $this->store($item, $typeInstance);
        }
      
    }
    
    function getDataForNotification($typeInstance) {
      $qry = $typeInstance->query();
      //modify query to exclude data which already has notifications
      $qry->whereDoesntHave('notifications',function($q) use ($typeInstance) {
        $q->whereIn('type',[$typeInstance->type(), $typeInstance->repeatType()]);
        if ($typeInstance->repeatNotification) {
          $q->where('created_at','>=',$typeInstance->repeatAfter());
        }
      });
      echo $qry->toSql();
      return $qry->limit(100)->get();
    }

    protected function store($data, $type)
    {
        N::create([
            'customerID' => $data->cid,
            'orderID' => $data->oid,
            'dispID' => isset($data->id_disp)? $data->id_disp: NULL,
            'type' => $type->repeatNotification ? $type->repeatType() : $type->type(),
            'hasSent' => false
        ]);

       if ($type->repeatNotification) {
         $output = "A REPEAT notification was created of type {$type->type()} for order: {$data->oid} and customer: {$data->cid}". PHP_EOL;
       } else {
         $output = "A NEW notification was created of type {$type->type()} for order: {$data->oid} and customer: {$data->cid}". PHP_EOL;
       } 
       echo $output;
       \Log::info($output);
    }
}
