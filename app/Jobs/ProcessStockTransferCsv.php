<?php
// NOT USED
namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\WarehouseStock;
use App\StockItem;
use App\StockParts;
use App\WarehouseLog;


class ProcessStockTransferCsv extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    
    private $state;
    
    var $csvpath;
    var $fromWarehouse;
    var $toWarehouse;
    var $reason;

    var $stage;

    var $total_items=-1;
    var $items_processed=0;
    var $items_per_pass=10;
    var $results=[];
    var $errors=[];

    var $started = false;
    var $finished = false;
  
  public function __construct()
    {
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
      if (!$this->started) {
        
      }
        switch ($this->stage) {
          case 'check': 
            
            $processed = $this->checkValues(); 
            break;
          case 'execute': 
            $processed = $this->updateValues(); 
            break;
          default: 
            throw new RuntimeException('bad stage ' . $this->stage );
        }
        
        $this->items_processed += $processed;
    }
    
    function checkValues() {
      $values = $this->getValues($this->items_processed,$this->items_per_pass);
      foreach ($values as $value) {
        $item = StockItem::where('itemCode',$value['itemCode'])->first();
        $ok = WarehouseStock::canMoveStockItem(
                $item,
                $this->fromWarehouse, 
                $this->toWarehouse, 
                $this->reason,
                $value['qty']
              );
        
      }
    }
    
    
    function updateValues() {
      $values = $this->getValues($this->items_processed,$this->items_per_pass);
      foreach ($values as $value) {
        $item = StockItem::where('itemCode',$value['itemCode'])->first();
        WarehouseStock::moveStockItem(
                $item,
                $this->fromWarehouse, 
                $this->toWarehouse, 
                $this->reason,
                $value['qty']
                );
      }
    }
    
    function getValues($start,$count) {
      $result = [];
      $csv = fopen($this->csvpath,'r');
      $line = 0;
      while ($record = fgetcsv($csv)) {
        if (count($record)<2) {
          continue;
        }
        $line++;
        if ($line<$start) {
          continue;
        }
        if ($line>=$start+$count) {
          break;
        }
        if ($record[0]) {
          $result[]=['itemCode'=>$record[0], 'qty'=>$record[1]];
        }
      }
      return $result;
    }
}
