<?php namespace App;

use DB;
use Zizaco\Entrust\EntrustRole;

class Role extends EntrustRole
{
  /**
   * @author Giomani Designs (Development Team)
   */
  public static function forAssigning () {
    return Role::where('roles.id', '<>', '1')->get();
  }
  /**
   * @author Giomani Designs (Development Team)
   */
  public static function withPermissions () {
    return Role::select(
        'roles.id as role_id',
        'roles.display_name as role_display_name',
        'roles.name as role_name',
        'permissions.id as permission_id',
        'roles.description as role_description',
        DB::raw('group_concat(permissions.name) as permissions')
      )
      ->leftJoin('permission_role', 'id', '=', 'role_id')
      ->leftJoin('permissions', 'permissions.id', '=', 'permission_id')
      ->where('roles.id', '<>', '1')
      ->groupBy('roles.id')
      ->orderBy('roles.display_name', 'asc')
      ->get();
  }
}
