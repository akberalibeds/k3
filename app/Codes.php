<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Codes extends Model
{
  /**
   * The table associated with the model.
   *
   * @var string
   */
  protected $table = 'codes';
  
  public $timestamps = false;
  
  public $fillable = ['lineid','bar','oid','parcelid','itemcode','warehouse'];
  
  
  
  
}
