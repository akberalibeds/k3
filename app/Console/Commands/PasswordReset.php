<?php

namespace App\Console\Commands;

use File;
use App\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Hash;

class PasswordReset extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'password:reset';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Reset the password of all users and save the output in a text file.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if ($this->confirm('Do you wish to continue?')) {
            
            User::all()->each(function($user) {
                $new_password = str_random(8);

                $user->password = Hash::make($new_password);

                $user->save();

                $this->info($user->email ." ". $new_password ."\n");
            });
        }
        
    }
}
