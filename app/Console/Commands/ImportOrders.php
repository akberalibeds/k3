<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use Illuminate\Console\Command;
use App\Imports\SalesImporter\SalesImporter;

class ImportOrders extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:all {start : hours ago} {end : hours ago}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import orders to the system.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $user = new \App\User();

        \Auth::login($user);
        
        $start =  Carbon::now()->subHours($this->argument('start'));

        $end = Carbon::now()->subHours($this->argument('end'));

        $this->info("Importing all orders between {$start} and {$end}");
        
        SalesImporter::importAll($start, $end);

    }
}
