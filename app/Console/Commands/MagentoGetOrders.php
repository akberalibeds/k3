<?php

namespace App\Console\Commands;

use App\Helper\Magento\Magento;
use Time;
use Illuminate\Console\Command;



class MagentoGetOrders extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'magento:getOrders';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Magento API Call GetOrders';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
		$this->info(Time::date()->get() .' GetOrders Call Started');
		
		$magento = new Magento();
		$magento->getOrders()
				->removeExisting()// remove existing and not valid orders
				->getAllInfo()// get all orderInfo from Magento
				->formatOrders()
				->insertOrders();		
		
		echo count($magento->orders)." New Orders\n";
		
        $this->info(Time::date()->get() .' GetOrders Call Completed');
    }
}
