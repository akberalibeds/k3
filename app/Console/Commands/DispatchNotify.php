<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Helper\DeliveryNotifications;


class DispatchNotify extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'send:dispatched';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send Delivery dispatched';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        
		$this->info(date('d-M-Y H:i:s') .' Send dispatched Started');
		
		$links = new DeliveryNotifications;
		
		$links->customersToNotify()
			  ->sendNotifications()
			  ->removeDone();
		//print_r($links->customers->toArray());
		
		$this->info(date('d-M-Y H:i:s') .' Send dispatched ended');
		
		
			
    }
}
