<?php

namespace App\Console\Commands;

use App\Helper\Magento\Magento2;
use Time;
use Illuminate\Console\Command;



class MagentoGetOrders2 extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'magento2:getOrders';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Magento2 API Call GetOrders';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
		$this->info(Time::date()->get() .' GetOrders2 Call Started');
		
		$magento = new Magento2();
		$magento->getOrders(/*timefrom , timeto*/)
				->removeExisting()// remove existing and not valid orders
				->getAllInfo()// get all orderInfo from Magento
				->formatOrders()
				->insertOrders();		
		
		echo count($magento->orders)." New Orders\n";
        $this->info(Time::date()->get() .' GetOrders2 Call Completed');
    }
}
