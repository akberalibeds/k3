<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Helper\DeliveryDateSelection;

class GetDates extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'send:links';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send delivery date selection email';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
       
		$this->info(date('d-M-Y H:i:s') .' Send Links Started');
		
		$links = new DeliveryDateSelection;
		
		/* $links
		 * 	  ->sendNotifications()
			  ->updateDone();
		*/
		$this->info(date('d-M-Y H:i:s') .' Send Links ended');
		
    }
}
