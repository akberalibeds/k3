<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Jobs\MessageSenderDaemon;

class MessageSender extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'notifications:send {method?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Trigger Daemon to look for any notifications that need to be sent';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $shortClassName = $this->argument('method');

        if ($shortClassName) {

            $cls = "App\\Notifications\\Methods\\{$shortClassName}";

            $method = new $cls;

            (new MessageSenderDaemon($method))->handle();
            
        } else {

            (new MessageSenderDaemon)->handle();

        }
    }
}
