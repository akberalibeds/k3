<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Jobs\SelectionDaemon;

class GenerateNotifications extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'notifications:generate {type}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Trigger daemon to look for any new notifications that need to be created.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {   
        $shortClassName = $this->argument('type');

        $cls = "App\\Notifications\\Types\\{$shortClassName}";

        $type = new $cls;

        (new SelectionDaemon($cls))->handle();
    }
}
