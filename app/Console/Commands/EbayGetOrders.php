<?php

namespace App\Console\Commands;


use App\Helper\Ebay\Ebay;
use App\Helper\Ebay\EbayParser;
use Time;
use Illuminate\Console\Command;


class EbayGetOrders extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ebay:getOrders';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Ebay API Call GetOrders';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
		$this->info(Time::date()->get() .' GetOrders Call Started');
	
		$ebay = new Ebay();
		
		
		$ebay//->allPages()
			 //->test()
			 ->get_orders()
			 ->removeExisting()
			 ->formatOrders();	
	
			var_dump($ebay->orders);	
			
			
		
				
        $this->info(Time::date()->get() .' GetOrders Call Completed');
    }
}
