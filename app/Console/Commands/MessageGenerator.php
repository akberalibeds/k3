<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Jobs\MessageGeneratorDaemon;

class MessageGenerator extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'messages:generate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Trigger deamon to look for any new messages that need to be created.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        (new MessageGeneratorDaemon)->handle();

    }
}
