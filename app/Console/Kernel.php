<?php

namespace App\Console;

use App\Jobs\MessageGeneratorDaemon;
use App\Jobs\MessageSenderDaemon;
use Carbon\Carbon;

use App\CronTask;
use App\StockItem;
use App\Order;
use App\DispatchedRoute;
use App\Links;
use App\OutOfStock;
use App\Helper\SMS;
use App\Customer;
use App\Jobs\SelectionDaemon;
use App\Jobs\UpdateOrderStatus;
use App\Imports\SalesImporter\SalesImporter;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;


class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
		  Commands\EbayGetOrders::class,
		  Commands\MagentoGetOrders::class,
		  Commands\MagentoGetOrders2::class,
		  Commands\DispatchNotify::class,
		  Commands\GetDates::class,
		  Commands\ImportOrders::class,
		  Commands\MessageGenerator::class,
		  Commands\MessageSender::class,
		  Commands\GenerateNotifications::class
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // notification types
        $notificationTypes = \App\Notifications\NotificationType::getTypes();

        foreach ($notificationTypes as $type) {
            $typeInstance=\App\Notifications\NotificationType::instantiate($type);
            $schedule->call(function() use ($type){
                // Daemon to listen for new notifications that need to be generated
                dispatch(new SelectionDaemon($type));
            })->cron($typeInstance->schedule());
        }

        // Daemon to generate messages from notifications table
        $schedule->call(function() {
            dispatch(new MessageGeneratorDaemon());
        })->name('messagegenerator')->everyTenMinutes()->withoutOverlapping(); // to be confirmed

        // Daemon to send messages from messages table
        $schedule->call(function() {
           dispatch(new MessageSenderDaemon());
        })->name('messagesender')->everyTenMinutes(); 

		
		/*
		* Set Routes as Done
		*/
		$schedule->call(function () {
						DispatchedRoute::where('date','!=',date('d-M-Y',strtotime(date('d-M-Y')." +1 days")))
													
	->update(['status' => 'Done']);
		})
		->name('routes')
		->dailyAt('12:00');

		//More frequent tasks

		$schedule->call(function() {
			(new UpdateOrderStatus)->handle();
		})->hourly();

           
        $schedule->call(function () {
            $u=new \App\User();
            \Auth::login($u);
            SalesImporter::importAll(Carbon::now()->subHours(1), Carbon::now());
        })->everyTenMinutes();
		
	//Re-run previous command to catch orders that take to long to process
        $schedule->call(function () {
            $u=new \App\User();
            \Auth::login($u);
            SalesImporter::importAll(Carbon::now()->subDays(2), Carbon::now()->subHours(1));
        })->cron('0 */6 * * *'); // every 6 hours
        
        //replaced by new notification system
//        $schedule->call(function() {
//          (new \App\Jobs\NotifyCustomers())->handle();
//        })->everyFiveMinutes();

    	#add Numerical postcode
    	$schedule->call(function () {
	    	$c =  Customer::where('numerical_pc',0)->whereNotNull('dPostcode')->whereNotIn('dPostcode',['0' ,'00','000', ''])->get();
	    	foreach ($c as $b){
	    			
	    		echo $b->id ." -- ".$b->dPostcode. " -- " . $b->numerical_pc . " -- ";
	    		echo $numerical = Customer::getNumericalValue($b->dPostcode);
	    		$b->numerical_pc = $numerical;
	    		$b->save();
	    		echo "\n";
	    		//echo "update customers set numerical_pc='$numerical' where id=$b->id<br><br>";
	    		//\DB::update(\DB::raw("update customers set numerical_pc='$numerical' where id=$b->id"));
	    			
	    	}
    	})
    	->name('numerical_pc')
    	->everyMinute()
    	->appendOutputTo(storage_path("logs/numerical.log"))
    	;//->withoutOverlapping();
    	
    	
    	
    	/*
    	#remove extra customers
    	$schedule->call(function () {
    		
    		$c = \DB::select(\DB::raw('select customers.id from customers left join orders on  customers.id=orders.cid where orders.id is null'));
    		$customers = [];
    		foreach($c as $b){
    			$customers[]=$b->id;
    		}
    		\DB::delete(\DB::raw('delete from customers where id in ('.implode(",",$customers).')'))
;
    		
    	})
    	->name('extraCustomers')
    	->everyThirtyMinutes()
    	->withoutOverlapping();
    	*/
    	
    	
    	
   
    	
    	/*
    	 * Stock Count Repair Step 1
    	 */
    	$schedule->call(function () {
    		
    		$oos = OutOfStock::select('out_of_stock.id','item_id','actual')
    		->join('stock_items','out_of_stock.item_id','=','stock_items.id')
    		->get();
    		$notIn=[];
    		
    		foreach ($oos as $os){
    			$notIn[]= $os->item_id;
    		
    			if($os->actual>0){
    				$out = OutOfStock::find($os->id);
    				$out->delete();
    			}
    		}
    		
    		$stock = StockItem::select('id','itemCode')
    		->where('actual',0)
    		->whereNotIn('id',$notIn)
    		->get();
    		$SMSSTOCK='';
    		foreach($stock as $s){
    			OutOfStock::add($s->id);
    			$SMSSTOCK=$s->itemCode."\n";
    		}
    		if(count($stock)>0){
    			$sms = new SMS();
    			$sms->sendSMS("447944018268", "Items out of stock\n$SMSSTOCK");
    		}
    	})
    	->name('nostock')
    	->everyMinute()
    	->withoutOverlapping();
			
			/*
			 * Stock Count Repair Step 1
			 */
			$schedule->call(function () {
                            //StockItem::where('id','>',1)->update(['qty_on_so' => \DB::raw("ifnull((select sum(Qty) from orderItems join orders on orderItems.oid=orders.id where itemid=stock_items.id and orderStatus='Allocated'),0)")]);
                            \DB::table('stock_items as si')->leftJoin(
                                \DB::raw("(select sum(qty) as sumqty, itemid from orderItems oi join orders o on oi.oid=o.id where o.orderstatus='Allocated' group by itemid) as b"
                              ), function($join){ $join->on('si.id', '=', 'b.itemid');}
                            )
                            ->update(['qty_on_so' => \DB::raw("ifnull(b.sumqty,0)")]);

			})
			->name('stock1')
			->everyThirtyMinutes()
			->withoutOverlapping();
			
			/*
			 * Stock Count Repair Step 2
			 */
			$schedule->call(function () {
echo "running stock2\n";
				StockItem::where('id','>',1)->update(['itemQty' => \DB::raw('actual - qty_on_so') ]);
				Order::where(['dispatchType' => ''])->update(['dispatchType' => 'Delivery']);
				Order::where('id','>',600000)->update(['id_delivery_route' => \DB::raw("(select id from routeName where name=delRoute)")]);
				Order::where('id','>',600000)->update(['id_status' => \DB::raw("(select id from order_status where order_status=orderStatus)")]);
      \Log::info('Stock2 fixed quantities');

			})
			->name('stock2')
			->everyTenMinutes();
	//		->withoutOverlapping();
			
		   /*
			* Magento Get Orders Call
			*/
			$cron = CronTask::where(['name' => 'Beds.co.uk'])->first();
			if( $cron && $cron->value){
				$schedule->command('magento:getOrders')
							->appendOutputTo($cron->log)
							->everyFiveMinutes()
							->withoutOverlapping();
			}
			
		   /*
			* Magento 2 Get Orders Call
			*/
			$cron = CronTask::where(['name' => 'Beds.co.uk 2'])->first();
			if($cron &&  $cron->value){
				$schedule->command('magento2:getOrders')
							->appendOutputTo($cron->log)
							->everyTenMinutes()
							->withoutOverlapping();
			}
			
		   /*
			* To Ship - replaced by new notifications system
			*/
//			$cron = CronTask::where(['name' => 'ToShip'])->first();
//			if($cron->value){
//				$schedule->command('send:dispatched')
//							->appendOutputTo($cron->log)
//							->dailyAt('08:00');
//			}
			

			
			
			/*
			 * Update links table
			 */
			$schedule->call(function () {
				
				Links::where('links.done',0)
					->where('orders.done',0)
					->whereNotIn('deliverBy',['0',''])
					->join('orders','links.oid','=','orders.id')
					->update(['links.done' => 1, 'orders.done' => 1]);
				
			})
			->name('links')
			->everyTenMinutes()
			->withoutOverlapping();
					
    }
	
	
}
