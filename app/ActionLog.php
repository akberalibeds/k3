<?php
namespace App;

use Auth;
use Hash;
use Cookie;
use App\Helper\Traits\QueryableModel;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class ActionLog extends Model
{
  use SoftDeletes, QueryableModel;

  const VERIFICATION_LEVELS = 6;

  const INVOICE_ACTIONS = '001';
  const STOCK_ACTIONS = '002';
  const ORDER_ACTIONS = '003';
  const USER_ACTIONS = '004';
  const ROUTE_ACTIONS = '005';
  const FREE_SATURDAY_ACTIONS = '006';
  const SELLER_ACTIONS = '007';
  const VEHICL_COST_ACTIONS = '008';
  const PAYMENT_TYPE_ACTIONS = '010';
  const SYSTEM_ACTIONS = '011';
  const BULLETIN_ACTIONS = '012';

  const INVOICE_PAID = '001.001';

  const STOCK_ITEM_WITHDRAWN = '002.001';
  const STOCK_ITEM_BLOCKED = '002.002';
  const STOCK_ITEM_UNBLOCKED = '002.003';
  const STOCK_ITEM_COUNT_ADJUSTMENT = '002.004';
  const STOCK_ITEM_COUNT_ADDITION = '002.005';
  const STOCK_ITEM_COUNT_WITHDRAWN = '002.006';
  const STOCK_ITEM_COUNT_UNWITHDRAWN = '002.007';

  const ORDER_CANCELLED = '003.001';
  const ORDER_MARKED_AS_DELIVERED = '003.002';
  const ORDER_ORDERED = '003.003';
  const ORDER_UNORDERED = '003.004';
  const ORDER_PROCESSED = '003.005';
  const ORDER_UNPROCESSED = '003.006';
  const ORDER_CHANGED_SUPPLIER = '003.007';
  const ORDER_CREDIT_NOTE_ADDED = '003.008';
  const ORDER_AFTER_SALES_REMOVED = '003.009';

  const USER_CHANGED_PASSWORD = '004.001';
  const USER_CHANGED_DETAILS = '004.002';
  const USER_ROLES_PERMISSIONS_CHANGED = '004.003';
  const USER_PASSWORD_CHANGED = '004.004';
  const USER_SUSPENDED = '004.005';

  const ROUTES_MARKED_AS_DELIVERED = '005.001';

  const FREE_SATURDAY_ADDED = '006.001';
  const FREE_SATURDAY_CANCELLED = '006.002';

  const SELLER_ADD_STOCK_ITEM = '007.001';
  const SELLER_REMOVE_STOCK_ITEM = '007.002';

  const VEHICLE_COST_TYPE_ADDED = '008.001';

  const VEHICLE_COST_ADDED = '009.001';

  const PAYMENT_TYPE_ADDED = '010.001';

  const SYSTEM_ROLES_PERMISSIONS_CHANGED = '011.001';

  const BULLETIN_ADDED = '012.001';
  /**
   *
   */
  public static function actionGroups () {
    return [
      ActionLog::INVOICE_ACTIONS => 'INVOICE_ACTIONS',
      ActionLog::STOCK_ACTIONS => 'STOCK_ACTIONS',
      ActionLog::ORDER_ACTIONS => 'ORDER_ACTIONS',
      ActionLog::USER_ACTIONS => 'USER_ACTIONS',
      ActionLog::ROUTE_ACTIONS => 'ROUTE_ACTIONS',
      ActionLog::FREE_SATURDAY_ACTIONS => 'FREE_SATURDAY_ACTIONS',
      ActionLog::SELLER_ACTIONS => 'SELLER_ACTIONS',
      ActionLog::VEHICL_COST_ACTIONS => 'VEHICL_COST_ACTIONS',
      ActionLog::PAYMENT_TYPE_ACTIONS => 'PAYMENT_TYPE_ACTIONS',
      ActionLog::SYSTEM_ACTIONS => 'SYSTEM_ACTIONS',
      ActionLog::BULLETIN_ACTIONS => 'BULLETIN_ACTIONS',
    ];
  }
  /**
   *
   */
  public static function actions() {
    return [
      ActionLog::INVOICE_PAID => 'INVOICE_PAID',
      ActionLog::STOCK_ITEM_WITHDRAWN => 'Stock item was withdrawn',
      ActionLog::STOCK_ITEM_BLOCKED => 'STOCK_ITEM_BLOCKED',
      ActionLog::STOCK_ITEM_UNBLOCKED => 'STOCK_ITEM_UNBLOCKED',
      ActionLog::STOCK_ITEM_COUNT_ADJUSTMENT => 'Set &apos;In Stock&apos; to a number',
      ActionLog::STOCK_ITEM_COUNT_ADDITION => 'Added a number to &apos;In Stock&apos;',
      ActionLog::STOCK_ITEM_COUNT_WITHDRAWN => 'Stock item was Withdrawn',
      ActionLog::STOCK_ITEM_COUNT_UNWITHDRAWN => 'Stock item was UnWithdrawn',
      ActionLog::ORDER_CANCELLED => 'ORDER_CANCELLED',
      ActionLog::ORDER_MARKED_AS_DELIVERED => 'ORDER_MARKED_AS_DELIVERED',
      ActionLog::ORDER_ORDERED => 'Order marked as &apos;ordered&apos;',
      ActionLog::ORDER_UNORDERED => 'Order marked as &apos;un-ordered&apos;',
      ActionLog::ORDER_PROCESSED => 'Order marked as &apos;processed&apos;',
      ActionLog::ORDER_UNPROCESSED => 'Order marked as &apos;un-processed&apos;',
      ActionLog::ORDER_CHANGED_SUPPLIER => 'Changed Supplier for an Order',
      ActionLog::ORDER_CREDIT_NOTE_ADDED => 'A Credit Note was added',
      ActionLog::ORDER_AFTER_SALES_REMOVED => 'Order removed from After Sales',
      ActionLog::USER_CHANGED_PASSWORD => 'User changed own password',
      ActionLog::USER_CHANGED_DETAILS => 'USER_CHANGED_DETAILS',
      ActionLog::USER_SUSPENDED => 'User suspended',
      ActionLog::USER_PASSWORD_CHANGED => 'User password changed by Admin',
      ActionLog::USER_ROLES_PERMISSIONS_CHANGED => 'USER_ROLES_PERMISSIONS_CHANGED',
      ActionLog::ROUTES_MARKED_AS_DELIVERED => 'ROUTES_MARKED_AS_DELIVERED',
      ActionLog::FREE_SATURDAY_ADDED => 'FREE_SATURDAY_ADDED',
      ActionLog::FREE_SATURDAY_CANCELLED => 'FREE_SATURDAY_CANCELLED',
      ActionLog::SELLER_ADD_STOCK_ITEM => 'SELLER_ADD_STOCK_ITEM',
      ActionLog::SELLER_REMOVE_STOCK_ITEM => 'SELLER_REMOVE_STOCK_ITEM',
      ActionLog::VEHICLE_COST_TYPE_ADDED => 'VEHICLE_COST_TYPE_ADDED',
      ActionLog::VEHICLE_COST_ADDED => 'VEHICLE_COST_ADDED',
      ActionLog::PAYMENT_TYPE_ADDED => 'PAYMENT_TYPE_ADDED',
      ActionLog::SYSTEM_ROLES_PERMISSIONS_CHANGED => 'Change to &apos;Roles and Permissions&apos;',
      ActionLog::BULLETIN_ADDED => 'Added a Bulletin',
    ];
  }
  /**
   * @author Giomani Designs (Development Team)
   * @var string
   */
  protected $table = 'action_log';
  /**
   * @author Giomani Designs (Development Team)
   * @param  AcionLog $row
   * @return string
   */
  public static function checkStatus ($row) {
    return Hash::check($row->action . $row->id . $row->more . $row->u . $row->user_id . $row->term, $row->hash) ? 'good' : 'bad';
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  AcionLog $row
   * @return string
   */
  public static function generateHash ($row) {
    return Hash::make($row->action . $row->id . $row->more . $row->u . $row->user_id . $row->term);
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  array   $queryStrings the query string
   * @return string
   */
  public static function getAll ($queryStrings) {
    $query = static::queryForAll();
    if (count($queryStrings) > 0) {
      $query = parent::modForQueryString($query, $queryStrings);
    }
    return $query->orderBy('action_log.id', 'desc')->paginate(config('search.rpp'));
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  array   $queryStrings the query string
   * @return string
   */
  public static function getByIds ($ids) {
    $query = static::queryByIds($ids);
    return $query->get();
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  string  $action
   * @param  array   $queryStrings the query string
   * @return string
   */
  public static function getForAction ($action, $queryStrings) {
    $query = static::queryForAction($action);
    if (count($queryStrings) > 0) {
      $query = parent::modForQueryString($query, $queryStrings);
    }
    return $query->orderBy('action_log.id', 'desc')->paginate(config('search.rpp'));
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  integer   id of the log entry
   * @return string
   */
  public static function getOrFail ($id) {
    $query = static::queryForGetOrFail($id);
    $result = $query->first();
    if ($result->count() === 0) {
      throw new ModelNotFoundException('');
    }
    return $result;
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  string  $action
   * @return array
   */
  public static function queryByIds ($ids) {
    return static::select(
        '*'
      )
      ->whereIn('id', $ids);
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  string  $action
   * @return array
   */
  public static function queryForAll () {
    return static::select(
        'action_log.id',
        'action_log.action',
        'action_log.hash',
        'action_log.more',
        'action_log.term',
        'action_log.u',
        'action_log.user_id',
        'action_log.created_at',
        'action_log.updated_at',
        'action_log.user_id',
        'users.email'
      )
      ->leftJoin('users', 'users.id', '=', 'user_id');
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  string  $action
   * @return array
   */
  public static function queryForAction ($action) {
    return static::select(
        'action_log.id',
        'action_log.action',
        'action_log.hash',
        'action_log.more',
        'action_log.term',
        'action_log.u',
        'action_log.user_id',
        'action_log.created_at',
        'action_log.updated_at',
        'action_log.user_id',
        'users.email'
      )
      ->leftJoin('users', 'users.id', '=', 'user_id')
      ->where('action', 'like', $action . '%');
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  integer  $id
   * @return array
   */
  public static function queryForGetOrFail ($id) {
    return static::select(
        'action_log.id',
        'action_log.action',
        'action_log.hash',
        'action_log.more',
        'action_log.term',
        'action_log.u',
        'action_log.user_id',
        'action_log.created_at',
        'action_log.updated_at',
        'action_log.user_id',
        'users.email'
      )
      ->leftJoin('users', 'users.id', '=', 'user_id')
      ->where('action_log.id', '=', $id);
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  int $level
   * @return array
   */
  public static function verify ($level = 0) {
    $count = static::count();
    $tests = [];

    $r = static::orderBy('id', 'desc')->first();
    $tests['0'] = [];
    $tests['0'][] = $r === null ? 0 : $r->id;
    $tests['0'][] = $count;

    if ($r->id === $count) {
      switch ($level) {
        case 5: // complete hash check
          break;
        case 4: // random sample hash check (greaater of 500 or 5%)
          break;
        case 3: // recent hash check (greaater of 500 or 5%)
          break;
        case 2: // random sample hash check (greaater of 100 or 1%)
          $tests['2'] = static::verifyRandom($count, 0.01, 100);
          break;
        case 1: // recent hash check (lesser of count or 1% or 100)
          $tests['1'] = static::verifyRecent($count, 0.01, 100);
          break;
      }
    }
    return $tests;
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  int   $size
   * @return array
   */
  private static function verifyRandom ($count, $factor, $size) {
    $f = $count * $factor;
    $j = $count < $size ? $count : $f < $size ? $size : $f;
    $ids = [];
    for ($i = 0; $i < $j; $i++) {
      $ids[] = mt_rand(1, $count);
    }
    return static::verifyRows($ids);
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  int   $size
   * @return array
   */
  private static function verifyRecent ($count, $factor, $size) {
    $f = $count * $factor;
    $j = $count < $size ? $count : $f < $size ? $size : $f;
    $ids = [];
    for ($i = 0; $i < $j; $i++) {
      $ids[] = $count - $i;
    }
    return static::verifyRows($ids);
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  int   $size
   * @return array
   */
  private static function verifyRows ($ids) {
    $rows = static::getByIds($ids);
    $results = ['count' => count($ids), 'fails' => [], 'passes' => []];
    foreach ($rows as $row) {
      $result = static::checkStatus($row);
      if ($result) {
        $results['passes'][] = ['id' => $row->id];
      } else {
        $results['fails'][] = ['id' => $row->id, 'row' => $row];
      }
    }
    return $results;
  }
  /**
   * @author Giomani Designs (Development Team)
   * @override
   */
  public function save (array $options = []) {
    if (config('other.act')) {
      $this->u = date('U');
      $this->term = Cookie::get('iam');
      $this->user_id = Auth::user()->id;
      parent::save($options);
      $this->hash = ActionLog::generateHash($this);
      parent::save($options);
    }
  }
}
