<?php
namespace App\Helper\Wowcher;

use Log;

class Wowcher
{
     protected $URL = 'https://public-api02.devwowcher.co.uk';

     protected $client;
     
     protected $token;

     public function __construct($options = [])
     {
         $this->client = new \GuzzleHttp\Client($options);

         $this->setUrl();
         $this->login();
     }

     public function updateOrderStatus($order, $status)
     {
        $this->updateWowcherStatus($order->voucher_code, $status, 'Premier Deliveries', $order->code);
       
     }

    public function updateWowcherStatus($voucherCode, $status, $courierName="", $trackingReference="")
    {

        $response = $this->post('PATCH',"v1/voucher/{$voucherCode}/deliverystatus", 

                [
                    'deliveryStatusUpdate' => [
                    'deliveryStatus' => $status,
                    'courierName' => $courierName,
                    'trackingReference' => $trackingReference,
                    'timestamp' => time() * 1000
                    ]
                ],
                [
                    'wowcher_user' => app()->environment() == 'testing'? 'test-token' :$this->token,
                    'brand' => 'wowcher'
                ]

            );
            
        if($response['status'] == 200) {
            Log::info("Success! Order with voucher code {$voucherCode} was set as {$status}");
        }
        else
        {
            Log::info("The order status was not updated. {$response['result']} ");
        }

            
        
    }

     private function setUrl() 
     {
        if( app()->environment() == 'testing') {
            $this->URL == 'https://public-api02.devwowcher.co.uk';
        } 
        else 
        {
            $this->URL = 'https://public-api.wowcher.co.uk';
        }
     }

     private function post($verb,$path,$data = [],$headers = []) 
     {
         try {
             $newHeaders = array_merge($headers,["Content-Type"=>"application/json","Accept"=>"application/json"]);
            //  echo $path;
            //  print_r($data);
            //  print_r($headers);
             $response = $this->client->$verb("{$this->URL}/{$path}",['headers'=> $newHeaders,'body'=> json_encode($data)]);
             $status = $response->getStatusCode();
             $raw = (string)$response->getBody();
             $result = json_decode($raw);
             if ($result==null) {
                 Log::info('Could not parse results');
                 throw new \Exception('couldnt parse results - '. $raw);
             }
             return ['status'=>$status,'result'=>$result];

         } catch (\GuzzleHttp\Exception\ClientException $e) {
             Log::info($e->getMessage());
             if ($e->hasResponse()) {
                return ['status'=>$e->getResponse()->getStatusCode(),'result'=>$e->getMessage()];
             } else {
               throw $e;
             }
         } 
         
     }

     public function login()
     {
     
        $data = $this->post('POST','v1/login', [
            'loginRequest' => [
                'j_username' => config('services.wowcher.j_username'),
                'j_password' => config('services.wowcher.j_password'),
            ]
            ]);

            switch ($data['status']) {
                    case '200':
                        $this->token = $data['result']->response->data;
                        break;
                    case 401:
                        throw new \Excpeption('Bad credentials');
                    default:
                        throw new \Exception('Other problem');
                }
    

     }

}