<?php
namespace App\Helper;



class FCM {
	
	
	
	public static function send($msg,$device){
		
		$ch = curl_init("https://fcm.googleapis.com/fcm/send");
		$header=array('Content-Type: application/json',
				"Authorization: key=AAAAkR1FbXI:APA91bGDQGu0zKT1LYenEFUda8iJvDDexXTvwtlJkwOP04WKoHSaEA-1YWyAA2vWq58b2GKeiVgrDn8ADo4LTyZypaLPJRneT_SLRQsqkHzEIV34cKF4r76Bgl8Xe9BsWL76pWk0Im99");
		curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
		curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
		
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, "{ \"notification\": {    \"title\": \"Premier Deliveries\",    \"text\": \"$msg\"  },    \"to\" : \"$device\"}");
		
		$result = curl_exec($ch);
		curl_close($ch);
		
		return $result;
		
	}
	
	
}