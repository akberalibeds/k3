<?php

namespace App\Helper;

class INIParser{
	
	
	
	public function __construct(){
		
	}
	
	
	
	 public static function read($file){
		 
			return parse_ini_file($file);
	 
	 }
	
	
	 public static function write($array, $file)
		{
			$res = array();
			foreach($array as $key => $val)
			{
				if(is_array($val))
				{
					$res[] = "[$key]";
					foreach($val as $skey => $sval) $res[] = "$skey = ".(is_numeric($sval) ? $sval : '"'.$sval.'"');
				}
				else $res[] = "$key = ".(is_numeric($val) ? $val : '"'.$val.'"');
			}
			self::safefilerewrite($file, implode("\r\n", $res));
		}
		
		private static function safefilerewrite($fileName, $dataToSave)
		{    if ($fp = fopen($fileName, 'w'))
			{
				$startTime = microtime(TRUE);
				do
				{            $canWrite = flock($fp, LOCK_EX);
				   // If lock not obtained sleep for 0 - 100 milliseconds, to avoid collision and CPU load
				   if(!$canWrite) usleep(round(rand(0, 100)*1000));
				} while ((!$canWrite)and((microtime(TRUE)-$startTime) < 5));
		
				//file was locked so now we can store information
				if ($canWrite)
				{            fwrite($fp, $dataToSave);
					flock($fp, LOCK_UN);
				}
				fclose($fp);
			}
		
		}
	
	
}

?>