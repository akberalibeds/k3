<?php
// @author Giomani Designs (Development Team)
//
namespace App\Helper\Pdfs;

use Auth;
use App\OrderNotes;
use App\Helper\Pdfs\Pdf;
use App\Helper\Traits\OrderUtils;

/**
 * @author Giomani Designs (Development Team)
 */
class PdfInvoice
{
  use OrderUtils;
  /**
   * @var
   */
  private $pdf;
  /**
   *
   */
  public function __construct($pdf = null) {
    if ($pdf === null) {
      $this->pdf = new Pdf('P', 'mm', 'A4');
    } else {
      $this->pdf = $pdf;
    }
  }
  /**
   *
   */
  public function invoice($orderDetail, $showVat = false) {
    $pageWidth = $this->pdf->GetPageWidth() - ($this->pdf->getRightMargin() + $this->pdf->getLeftMargin());

    $this->pdf->AddPage();
    $this->pdf->setShowFooter(false);

    if ($orderDetail['total'] - $orderDetail['paid'] > 0) {
      $text = 'Unpaid';
    } else {
      $text = 'Paid';
    }
    $x = $this->pdf->GetX();
    $y = $this->pdf->GetY();
    $this->pdf->SetFont('Arial', 'B', 16);
    $this->pdf->Cell(0, 1.0, $text, 0, 0);
    $this->pdf->setXY($x, $y);
    $this->pdf->Cell($pageWidth - 4, 1.0, 'INVOICE', 0, 2, 'R');

    if ($orderDetail->priority === 1) {
      $this->pdf->setXY($x, $y);
      $this->pdf->Cell(0, 1.0, 'PRIORITY ORDER', 0, 2, 'C');
    }

    $this->pdf->line($this->pdf->getLeftMargin(), $this->pdf->GetY() + 2.0, $pageWidth, $this->pdf->GetY() + 2.0);

    $this->pdf->ln(4.0);

    $w0 = $this->pdf->longest(['Invoice Number:', 'Order Number:', 'Date:']);
    $y = $this->pdf->GetY();

    $this->pdf->SetFont('Arial', 'B', 10);
    $this->pdf->Cell($w0, 5.0, 'Invoice Number:', 0, 0);
    $this->pdf->SetFont('Arial', '', 10);
    $this->pdf->Cell(0.0, 5.0, $orderDetail['iNo'], 0, 1);
    $this->pdf->SetFont('Arial', 'B', 10);
    $this->pdf->Cell($w0, 5.0, 'Order Number:', 0, 0);
    $this->pdf->SetFont('Arial', '', 10);
    $this->pdf->Cell(0.0, 5.0, $orderDetail['oid'], 0, 1);
    $this->pdf->SetFont('Arial', 'B', 10);
    $this->pdf->Cell($w0, 5.0, 'Date:', 0, 0);
    $this->pdf->SetFont('Arial', '', 10);
    $this->pdf->Cell(0.0, 5.0, $orderDetail['date'], 0, 1);

    $this->pdf->SetFont('Arial', 'B', 10);
    $w0 = $this->pdf->longest(['Company:', 'Our Ref:']);
    $this->pdf->SetFont('Arial', '', 10);
    $w1 = $this->pdf->longest([$orderDetail['companyName'], $orderDetail['ouref']]);

    $this->pdf->setXY($pageWidth - ($w0 + $w1) - 4.0, $y);

    $this->pdf->SetFont('Arial', 'B', 10);
    $this->pdf->Cell($w0 + 3.0, 5.0, 'Company:', 0, 0);
    $this->pdf->SetFont('Arial', '', 10);
    $this->pdf->Cell(0.0, 5.0, $orderDetail['companyName'], 0, 1);

    $this->pdf->setX($pageWidth - ($w0 + $w1) - 4.0);

    $this->pdf->SetFont('Arial', 'B', 10);
    $this->pdf->Cell($w0 + 3.0, 5.0, 'Our Ref:', 0, 0);
    $this->pdf->SetFont('Arial', '', 10);
    $this->pdf->Cell(0.0, 5.0, $orderDetail['ouref'], 0, 1);

    $this->pdf->SetY($this->pdf->GetY() + 5);

    $offsetX = $this->pdf->getLeftMargin();
    $offsetY = $this->pdf->GetY() + 2.0;
    $boxHeight = 50;
    $boxWidth = ($pageWidth / 2) - 4;

    $this->pdf->Rect($offsetX, $offsetY, $boxWidth, $boxHeight);
    $this->pdf->Rect($offsetX + $boxWidth + 2, $offsetY, $boxWidth + 2, $boxHeight);

    $this->pdf->SetXY($offsetX, $offsetY + 2);

    $invoiceAddress = $this->getAddress($orderDetail, 'i');

    if ($orderDetail['direct'] === 1) {
      $deliveryAddress = $this->getAddress($orderDetail, 'd');
    } else {
      $deliveryAddress = $this->getAddress($orderDetail, 'c');
    }

    $w0 = $offsetX + $boxWidth - 2.0;
    $this->pdf->SetFont('Arial', 'B', 10);
    $this->pdf->Cell($w0 - 1, 5.0, 'Invoice To:', 0, 0);
    $this->pdf->Cell(0, 5.0, 'Deliver To:', 0, 1);

    $this->pdf->SetFont('Arial', '', 9.5);
    $y = $this->pdf->GetY();
    $addressLines = explode(',', str_replace('  ', ',', $invoiceAddress['ad0'] . ',' . $invoiceAddress['ad1']));
    $this->pdf->SetFont('Arial', 'B', 9.5);
    $this->pdf->Cell(0.0, 4.5, $invoiceAddress['name'], 0, 1);
    $this->pdf->SetFont('Arial', '', 9.5);
    foreach ($addressLines as $addressLine) {
      $this->pdf->Cell(0.0, 4.5, trim($addressLine), 0, 1);
    }
    $this->pdf->SetFont('Arial', 'B', 9.5);
    $this->pdf->Cell(0.0, 4.5, $invoiceAddress['postcode'], 0, 1);
    $this->pdf->SetFont('Arial', '', 9.5);
    $this->pdf->Cell(0.0, 4.5, $orderDetail['email1'], 0, 1);
    $this->pdf->Cell(0.0, 4.5, $invoiceAddress['telephone'], 0, 1);

    $leftMargin = $this->pdf->GetLeftMargin();

    $this->pdf->SetY($y);
    $this->pdf->SetLeftMargin($w0 + 4.0);
    $addressLines = explode(',', str_replace('  ', ',', $deliveryAddress['ad0'] . ',' . $deliveryAddress['ad1']));
    $this->pdf->SetFont('Arial', 'B', 9.5);
    $this->pdf->Cell(0.0, 4.5, $deliveryAddress['name'], 0, 1);
    $this->pdf->SetFont('Arial', '', 9.5);
    foreach ($addressLines as $addressLine) {
      $this->pdf->Cell(0.0, 4.5, trim($addressLine), 0, 1);
    }
    $this->pdf->SetFont('Arial', 'B', 9.5);
    $this->pdf->Cell(0.0, 4.5, $deliveryAddress['postcode'], 0, 1);
    $this->pdf->SetFont('Arial', '', 9.5);
    $this->pdf->Cell(0.0, 4.5, $orderDetail['email1'], 0, 1);
    $this->pdf->Cell(0.0, 4.5, $deliveryAddress['telephone'], 0, 1);
    $this->pdf->SetLeftMargin($leftMargin);

    $offsetY += $boxHeight + 2;
    $boxHeight = 100.0;
    $boxWidth = $pageWidth - 4;

    $this->pdf->Rect($offsetX, $offsetY, $boxWidth, $boxHeight);
    $this->pdf->Line($offsetX, $offsetY + 6, $boxWidth + $offsetX, $offsetY + 6);
    $this->pdf->SetXY($offsetX, $offsetY);

    $this->pdf->SetFont('Arial', 'B', 10);

    $items = explode(';', $orderDetail['items']);

    $text = count($items) > 1 ? 'Items' : 'Item';
    $this->pdf->Cell(140.0, 5.0, $text, 0, 0);
    $x = $this->pdf->GetX();
    $this->pdf->Line($x, $offsetY, $x, $offsetY + $boxHeight);
    $this->pdf->Cell(10.0, 5.0, 'Qty', 0, 0, 'R');
    $x = $this->pdf->GetX();
    $this->pdf->Line($x, $offsetY, $x, $offsetY + $boxHeight);
    $this->pdf->Cell(20.0, 5.0, 'Price', 0, 0, 'R');
    $x = $this->pdf->GetX();
    $this->pdf->Line($x, $offsetY, $x, $offsetY + $boxHeight);
    $this->pdf->Cell(25, 5.0, 'Total', 0, 1, 'R');
    $line = '';
    $this->pdf->Ln(3.0);
    $this->pdf->SetFont('Arial', '', 10);

    $x = $this->pdf->GetX();
    $nextLine = $this->pdf->GetY();
    foreach ($items as $item) {
      $y = $nextLine;
      $item = explode(',', $item);
      $this->pdf->SetXY($x, $nextLine);
      $this->pdf->MultiCell(140.0, 4.0, $item[1], 0, 'L');
      $this->pdf->SetX($x + 6);
      $this->pdf->MultiCell(134.0, 4.0, $item[2], 0, 'L');
      $this->pdf->Ln(2.0);

      $nextLine = $this->pdf->GetY();

      $this->pdf->SetXY(145.0, $y);
      $this->pdf->Cell(10.0, 6.0, $item[0], 0, 0, 'R');
      $multiplier = 1.0;
      if ($showVat) {
        $multiplier = 100 / ($orderDetail['vatAmount'] + 100);
      }
      $this->pdf->Cell(20.0, 6.0, number_format(0.0 + ($item[4] * $multiplier), 2), 0, 0, 'R');
      $this->pdf->Cell(25.0, 6.0, number_format(0.0 + ($item[5] * $multiplier), 2), 0, 1, 'R');
    }
    $offsetY += $boxHeight + 2;
    $this->pdf->SetY($offsetY);
    $boxHeight = 30.0;
    $this->pdf->Rect($offsetX, $offsetY, $boxWidth, $boxHeight);
    $this->pdf->SetFont('Arial', 'B', 12);
    $this->pdf->Cell(130, 6.0, 'Payment Details', 0, 1);
    $this->pdf->Line($offsetX, $offsetY + 6, $offsetX + 130, $offsetY + 6);
    $this->pdf->Line($offsetX + 130, $offsetY, $offsetX + 130, $offsetY + $boxHeight);
    $this->pdf->SetFont('Arial', '', 11);
    $this->pdf->Cell(130, 6.0, $orderDetail['paymentType'], 0, 1);

    $this->pdf->SetFont('Arial', 'B', 11);
    $w0 = $this->pdf->longest(['Total: ', 'Paid: ', 'Outstanding: ']);
    $this->pdf->SetFont('Arial', '', 11);

    $total = $orderDetail['total'];
    if ($total > 0) {
      $vat = ($total / 120) * $orderDetail['vatAmount'];
      $vatFmt = number_format(0.0 + $vat, 2);
      $subTotal = $total - $vat;
      $subTotalFmt = number_format(0.0 + $subTotal, 2);
    } else {
      $vatFmt = '-';
      $subTotalFmt = '-';
    }
    $paid = $orderDetail['paid'];
    $outstanding = $orderDetail['total'] - $orderDetail['paid'];

    $totalFmt = number_format(0.0 + $orderDetail['total'], 2);
    $paidFmt = number_format(0.0 + $orderDetail['paid'], 2);
    $outstandingFmt = number_format(0.0 + $orderDetail['total'] - $orderDetail['paid'], 2);

    $w1 = $this->pdf->longest([$totalFmt, $paidFmt, $outstandingFmt, $vatFmt]) + 3;

    $this->pdf->SetY($offsetY);
    $x = $pageWidth - ($w0 + $w1);

    if ($showVat) {
      $this->pdf->setX($x);
      $this->pdf->SetFont('Arial', 'B', 11);
      $this->pdf->Cell($w0, 6.0, 'Subtotal: ', 0, 0, 'R');

      $this->pdf->SetFont('Arial', '', 11);
      $this->pdf->Cell($w1, 6.0, $subTotalFmt, 0, 1, 'R');

      $this->pdf->setX($x);
      $this->pdf->SetFont('Arial', 'B', 11);
      $this->pdf->Cell($w0, 6.0, 'VAT (' . $orderDetail['vatAmount'] . '%): ', 0, 0, 'R');
      $this->pdf->SetFont('Arial', '', 11);
      $this->pdf->Cell($w1, 6.0, $vatFmt, 0, 1, 'R');
    }

    $this->pdf->setX($x);
    $this->pdf->SetFont('Arial', 'B', 11);
    $this->pdf->Cell($w0, 6.0, 'Total: ', 0, 0, 'R');

    $this->pdf->SetFont('Arial', '', 11);
    $this->pdf->Cell($w1, 6.0, $totalFmt, 0, 1, 'R');

    $this->pdf->setX($x);
    $this->pdf->SetFont('Arial', 'B', 11);
    $this->pdf->Cell($w0, 6.0, 'Paid: ', 0, 0, 'R');

    $this->pdf->SetFont('Arial', '', 11);
    $this->pdf->Cell($w1, 6.0, $paidFmt, 0, 1, 'R');

    $this->pdf->setX($x);
    $this->pdf->SetFont('Arial', 'B', 11);
    $this->pdf->Cell($w0, 6.0, 'Outstanding: ', 0, 0, 'R');

    $this->pdf->SetFont('Arial', '', 11);
    $this->pdf->Cell($w1, 6.0, $outstandingFmt, 0, 1, 'R');

    $offsetY += $boxHeight + 2;
    $boxHeight = 272.0 - $offsetY;
    $this->pdf->Rect($offsetX, $offsetY, $boxWidth, $boxHeight);
    $this->pdf->Line($offsetX, $offsetY + 6, $boxWidth + $offsetX, $offsetY + 6);

    $this->pdf->SetY($offsetY);
    $this->pdf->SetFont('Arial', 'B', 12);
    $this->pdf->Cell(0, 6.0, 'Notes ', 0, 1);

    $this->pdf->SetFont('Arial', '', 11);
    $notes = explode(';', $orderDetail['order_notes']);
    foreach ($notes as $note) {
      $this->pdf->MultiCell(0, 6.0, $note);
    }

    $this->pdf->SetFont('Arial', '', 8);
    $this->pdf->setXY(0, -25);
    $this->pdf->cell($pageWidth, 6, Auth::user()->id, 0, 0, 'R');
  }
  /**
   * @author Giomani Designs (Development Team)
   */
  public function outPdf($dest = 'I') {
    return $this->pdf->outPdf($dest);
  }
}
