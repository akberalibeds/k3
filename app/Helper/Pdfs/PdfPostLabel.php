<?php
// @author Giomani Designs (Development Team)
//
namespace App\Helper\Pdfs;

use App\Helper\Fpdf\Barcode;
use App\Helper\Traits\OrderUtils;

/**
 * @author Giomani Designs (Development Team)
 */
class PdfPostLabel
{
  use OrderUtils;
  /**
   * @var
   */
  private $pageWidth;
  /**
   *
   */
  public function __construct($pdf = null) {
    if ($pdf === null) {
      $this->pdf = new Pdf('P', 'mm', [100, 148]);
    } else {
      $this->pdf = $pdf;
    }
    $this->pageWidth = $this->pdf->GetPageWidth() - ($this->pdf->getRightMargin() + $this->pdf->getLeftMargin());
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  Datetime $date the date to go on the label
   * @param  array $labels
   */
  public function label($label, $showCustomer = true) {
    $pageNumber = 1;
    $delivery = $this->getDeliveryAddress($label, $label['direct']);
    $items = explode(';', $label['items']);
    sort($items);
    $parcelid = $label->parcelid;
    $count = count($items);
    $i = 1;

    foreach ($items as $item) {
      $this->pdf->AddPage();

      $parts = explode(',', $item);

      // box border
      $this->pdf->Rect(2.0, 4.0, 96.0, 106.0);

      // parcel id
      $this->pdf->SetFont('Arial', 'B', 40);
      $this->pdf->setXY(2.0, 4.0);
      $this->pdf->Cell(96.0, 14.0, $parcelid, 'B', 1, 'C');
      $y = $this->pdf->getY();
      if ($label->priority === 1) {
        $this->pdf->SetFont('Arial', '', 12);
        $this->pdf->setXY(2.0, 4.0);
        $this->pdf->Cell(0, 6.0, 'PRIORITY', 0, 2, 'L');
        $this->pdf->setXY(2.0, 4.0);
        $this->pdf->Cell(0, 6.0, 'ORDER', 0, 2, 'R');
      }
      $this->pdf->setY($y);
      // part
      $part0 = '';
      $part1 = '';
      if ($parts[0] == 'MISSED PARTS Part1 of 1' || $parts[0] == 'Replacement Product Part 1 of 1') {
        $text = $parts[2];
      } else {
        if (preg_match('/part [0-9]+ of [0-9]+/i', $parts[0], $matches, PREG_OFFSET_CAPTURE) === 1) {
          $part0 = substr($parts[0], 0, $matches[0][1]);
          $part1 = substr($parts[0], $matches[0][1]);
        } else {
          $part0 = $parts[0];
        }
        $text = $part0 . " - " . $parts[3];
      }
      // consignment
      $consignment = 'Consignment: ' . $label['code'];
      $w = $this->pdf->GetStringWidth($consignment);
      $this->pdf->SetFont('Arial', '', 10);
      $this->pdf->setX(2.0);
      $this->pdf->Cell($w, 7.0, $consignment, 0, 1);
      $x = $this->pdf->GetX();
      $y = $this->pdf->GetY();
      // order id
      $this->pdf->setXY(2.0, $this->pdf->GetY() - 7.0);
      $this->pdf->Cell(96.0, 7.0, '[' . $label['oid'] . ']', 0, 1, 'R');
      // part
      $this->pdf->SetFont('Arial', 'B', 10);
      $this->pdf->setX(2.0);
      $y = $this->pdf->GetY();
      $this->pdf->MultiCell(96.0, 5.0, $text, 0, 'L');
      $y += 20.0;
      $this->pdf->SetY($y);
      $this->pdf->Line(2.0, $y, 98.0, $y);
      $y = $this->pdf->GetY();
      // address
      if (true) { // $label['showCustomer'] === 1) {
        $this->pdf->SetFont('Arial', '', 11);
        $this->pdf->setX(2.0);
        $this->pdf->Cell(96.0, 5.0, $delivery['name'], 0, 1);
        $this->pdf->setXY(2.0, $y);
        $this->pdf->Cell(96.0, 5.0, '[' . $label['cid'] . ']', 0, 1, 'R');
        //
        $this->pdf->setX(2.0);
        $this->pdf->MultiCell(96.0, 5.0, $delivery['ad0']);
        $this->pdf->setX(2.0);
        $this->pdf->Cell(96.0, 5.0, $delivery['ad1'], 0, 1);
        $this->pdf->setX(2.0);
        $this->pdf->Cell(96.0, 5.0, $delivery['postcode'], 0, 1);
        if ($this->pdf->GetStringWidth($delivery['ad0']) < 96) {
          $this->pdf->Cell(96.0, 5.0, ' ', 0, 1);
        }
        $this->pdf->setX(2.0);
        $y = $this->pdf->GetY();
        $this->pdf->Cell(96.0, 5.0, 'Tel: ' . $delivery['telephone'], 0, 1);
      } else {
        $y += 26.0;
      }
      $this->pdf->setXY(2.0, $y);
      // Part n of m
      $partNom = $i++ . ' of ' . $count;
      $this->pdf->SetFont('Arial', 'B', 10);
      $this->pdf->Cell(96.0, 6.0, $partNom, 0, 1, 'R');
      $y = $this->pdf->GetY();
      $this->pdf->Line(2.0, $y, 98.0, $y);
      $y += 1.0; // extra for barcode
     
      // barcode
      $angle = 0;   // rotation in degrees
      $fontSize = 10;
      $height = 27;   // barcode height in 1D ; module size in 2D
      $marge = 2;   // between barcode and hri in pixel
      $width = 0.5;    // barcode height in 1D ; not use in 2D
      $bx = $this->pdf->GetPageWidth() / 2;  // barcode center
      $by = $y + ($height / 2);  // barcode center
      $type = 'code128';
      $black = '000000'; // color in hex
      $strBarcode = $parts[1];
      //$barcode = Barcode::fpdf($this->pdf, $black, $bx, $by, $angle, $type, $strBarcode, $width, $height);
      $y += 20;//$barcode['height'] - 6.0;
      $this->pdf->SetFont('Arial', 'B', 11);
      $this->pdf->setXY(2.0, $y + 2.0);
      $this->pdf->Cell(96.0, 14.0, '', 0, 1, 'C');
      $y += 14.0;
      
      
      $image = storage_path().'/label.png';
      $this->pdf->Image($image, 5, 80, 91.00);
      
      
      // route
      $this->pdf->SetFont('Arial', 'B', 28);
      $this->pdf->Cell(96.0, 8.0, $label['route_name'], 0, 1, 'L');
      $this->pdf->SetY($y);
      $this->pdf->SetFont('Arial', 'B', 20);
      $this->pdf->Cell(0.0, 12.0, $label['del_order'], 0, 1, 'R');
      $y += 20.0;
      // footer
      $this->pdf->SetFont('Arial', 'B', 20);
      $this->pdf->setY($y);
      $this->pdf->Cell(96.0, 6.0, $label['disp_date'], 0, 0, 'C');
      $this->pdf->SetFont('Arial', 'B', 28);
      $this->pdf->setY($y);
      $this->pdf->Cell(96.0, 6.0, $pageNumber++, 0, 0, 'L');
      $company = trim(substr($label['companyName'], 0, 3));
      $this->pdf->SetFont('Arial', '', 11);
      $this->pdf->setY($y);
      $this->pdf->Cell(94.0, 6.0, $company, 0, 1, 'R');
      $y = $this->pdf->GetY();
      $this->pdf->SetFont('Arial', 'B', 9);
      $this->pdf->Cell(12.0, 6.0, '{nb}', 0, 0, 'L');
      $this->pdf->SetY($y);
      $this->pdf->Cell(94.0, 6.0, 'w' . $label['warehouse'], 0, 0, 'R');
    }
    return $this;
  }
  /**
   * @author Giomani Designs (Development Team)
   */
  public function outPdf($dest = 'I') {
    return $this->pdf->outPdf($dest);
  }
}
