<?php
/** @author Giomani Designs **/
//
namespace App\Helper\Pdfs;

use Auth;
use App\Helper\Fpdf\Fpdf;
use Illuminate\Http\Response;
use App\Helper\Pdfs\PdfHeaderInterface;

/**
 * @author Giomani Designs (Development Team)
 */
class Pdf extends Fpdf
{
  /**
   * @var
   */
  protected $continued = false;
  /**
   * @var
   */
  protected $documentTitle;
  /**
   * @var
   */
  protected $objHeader = null;
  /**
   * @var
   */
  protected $header;
  /**
   * @var
   */
  protected $pageNumber;
  /**
   * @var
   */
  protected $showFooter;
  /**
   * @var
   */
  protected $showHeader;
  /**
   * @author Giomani Designs (Development Team)
   */
  public function __construct($orientation = 'P', $unit = 'mm', $size = 'A4') {
    parent::__construct($orientation, $unit, $size);

    $this->AliasNbPages();
    $this->SetAutoPageBreak(false);
    $this->SetFillColor(0x51, 0x51, 0x51);
    $this->setMargins(5.0, 8.0, 5.0);
    $this->SetTextColor(0x51, 0x51, 0x51);
    $this->SetFont('Arial', '', 11);
  }
  /**
   * @author Giomani Designs (Development Team)
   * @return the widht of the right margin
   */
  public function em($n) {
    return $this->GetStringWidth('m') * $n;
  }
  /**
   * @author Giomani Designs (Development Team)
   * @return the widht of the right margin
   */
  public function ex($n) {
    return $this->GetStringWidth('x') * $n;
  }
  /**
   * @author Giomani Designs (Development Team)
   * @return void
   */
  public function footer() {
    if ($this->objHeader) {
      $this->objHeader->drawFooter();
    }
  }
  /**
   * @author Giomani Designs (Development Team)
   * @return the widht of the right margin
   */
  public function getRightMargin() {
    return $this->rMargin;
  }
  /**
   * @author Giomani Designs (Development Team)
   * @return the widht of the right margin
   */
  public function getLeftMargin() {
    return $this->lMargin;
  }
  /**
   * @author Giomani Designs (Development Team)
   * @return the scaling factor
   */
  public function getScaleFactor() {
    return $this->k;
  }
  /**
   * @author Giomani Designs (Development Team)
   * @return void
   */
  public function header() {
    if ($this->objHeader) {
      $this->objHeader->drawHeader();
    }
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  array $strings
   * @return float
   */
  public function longest($strings) {
    $width = 0;
    foreach ($strings as $string) {
      $w = $this->GetStringWidth($string);
      $width = $w > $width ? $w : $width;
    }
    return $width;
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  string $string
   * @param  float $offsetX
   * @return void
   */
  protected function nlin($string, $offsetX = null) {
    if ($this->GetX() + $this->GetStringWidth($string) > $this->GetPageWidth()) {
      $this->Ln();
      if ($offsetX !== null) {
        $this->SetX($offsetX);
      }
    }
  }
  /**
   * @author Giomani Designs (Development Team)
   * @see
   */
  public function Out($a) {
    return $this->_out($a);
  }
  /**
   * @author Giomani Designs (Development Team)
   */
  public function outPdf($dest = 'I', $title = false) {
    if ($title === false) {
      $date = new \DateTime;
      $title = $this->documentTitle . '_' . $date->format('YmdH') . '_' . Auth::user()->id;
    }
    $this->SetTitle($title, true);

    $name = $title . '.pdf';

    $this->Close();

    $headers = null;

    if (strtoupper($dest) === 'I') {
      $this->_checkoutput();
      $headers = [
        'Content-Type' => 'application/pdf',
        'Content-Disposition' => 'inline; ' . $this->_httpencode('filename', $name, false),
        'Cache-Control' => 'private, max-age=0, must-revalidate',
        'Pragma' => 'public',
      ];
      return response($this->buffer)->withHeaders($headers);
    } else {
      return parent::Output($dest, $name, false);
    }
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  PdfHeaderInterface $object
   * @return void
   */
  public function setHeaderObject(PdfHeaderInterface $object) {
    $this->objHeader = $object;
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  PdfHeaderInterface $object
   * @return void
   */
  public function setShowFooter($show) {
    $this->showFooter = $show;
  }
}
