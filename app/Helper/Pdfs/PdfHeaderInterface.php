<?php
/** @author Giomani Designs (Development Team) **/
//
namespace App\Helper\Pdfs;

/**
 * @author Giomani Designs (Development Team)
 */
interface PdfHeaderInterface
{
  /**
   *
   */
  public function drawFooter();
  /**
   *
   */
  public function drawHeader();
}
