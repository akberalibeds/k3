<?php
// @author Giomani Designs (Development Team)
//
namespace App\Helper\Pdfs;

use App\Helper\Pdfs\Pdf;
use App\Helper\Pdfs\PdfHeaderInterface;

/**
 * @author Giomani Designs (Development Team)
 */
class PdfTable implements PdfHeaderInterface
{
  const DEFAULT_BASE_FONT_SIZE = 9;
  const BASE_LINE_HEIGHT = 5;
  const FOOTER_HEIGHT = 25;
  /**
   * @var
   */
  private $baseFontSize;
  /**
   * @var
   */
  private $columnWidths;
  /**
   * @var
   */
  private $firstColumnIndex;
  /**
   * @var
   */
  private $lineHeight;
  /**
   * @var
   */
  private $margins;
  /**
   * @var
   */
  private $pageHeight;
  /**
   * @var
   */
  private $pageWidth;
  /**
   * @var
   */
  private $rowBreak;
  /**
   * @var
   */
  private $tableHeaders;
  /**
   * @var
   */
  private $tableRows;
  /**
   * @var
   */
  private $title;
  /**
   * @param  array   $headers array of column header titles
   * @param  array   $rows array of table rows
   * @param  string  $title
   */
  public function __construct ($headers, $rows, $title, $pdf = null, $orientation = 'L', $size = 'A4') {
    if ($pdf === null) {
      $this->pdf = new Pdf($orientation, 'mm', $size);
    } else {
      $this->pdf = $pdf;
    }
    $this->pdf->setHeaderObject($this);

    $this->baseFontSize = static::DEFAULT_BASE_FONT_SIZE;

    $this->lineHeight = static::BASE_LINE_HEIGHT;
    $this->pageHeight = $this->pdf->GetPageHeight() - static::FOOTER_HEIGHT; // for footer
    $this->pageWidth = $this->pdf->GetPageWidth();

    $this->tableHeaders = $headers;
    $this->tableRows = $rows;
    $this->title = $title;

    $this->pdf->SetMargins(10, 10, 10);
    $this->footer = static::FOOTER_HEIGHT;

    $this->margins = 10;
  }
  // /**
  //  * @author Giomani Designs (Development Team)
  //  */
  // public function __call($method, $parameters) {
  //
  //   $class = new \ReflectionClass('App\Helper\Pdfs\Pdf');
  //   $methods = $class->getMethods(\ReflectionMethod::IS_PUBLIC);
  //   if (in_array($method, array('retweet', 'favourite')))
  //   {
  //     return call_user_func_array(array($this, $method), $parameters);
  //   }
  //   }
  /**
   * @author Giomani Designs (Development Team)
   * @see App\Helper\Pdfs\PdfHeaderInterface
   */
  public function drawFooter () {
    $this->pdf->SetY(-static::FOOTER_HEIGHT);
    $this->pdf->Cell(0, 10, 'Page ' . $this->pdf->PageNo() . ' of {nb}', 0, 0, 'C');
  }
  /**
   * @author Giomani Designs (Development Team)
   * @see App\Helper\Pdfs\PdfHeaderInterface
   */
  public function drawHeader () { }
  /**
   * @author Giomani Designs (Development Team)
   * @see App\Helper\Pdfs\PdfHeaderInterface
   */
  public function drawHeaderRow () {
    $this->headerFont();
    $x = $this->pdf->getX();
    $y = $this->pdf->getY();
    // if ($this->firstColumnIndex == 1) {
    //   $this->pdf->multiCell($this->columnWidths[0], $this->lineHeight, $this->tableHeaders[0], 1, 'L');
    //   // $x += $this->columnWidths[0];
    //   $this->pdf->setXY($x, $y);
    // }
    for ($i = $this->firstColumnIndex, $j = count($this->tableHeaders); $i < $j; $i++) {
      $x += $this->columnWidths[$i]; // Error Here <-

      if ($x > $this->pageWidth) {
        $this->rowBreak = $i;
        $i = $j;
        continue;
      }
      $this->pdf->multiCell($this->columnWidths[$i], $this->lineHeight, $this->tableHeaders[$i], 1, 'R');
      $this->pdf->setXY($x, $y);
    }
    $this->pdf->ln();
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  array  $row the row to draw
   * @return void
   */
  private function drawRow ($row, $lineHeight) {
  	$this->rowFont();
    $x = $this->pdf->getX();
    $y = $this->pdf->getY();
    if ($this->firstColumnIndex == 1) {
      $this->pdf->multiCell($this->columnWidths[0], $lineHeight, $row[0], 1, 'L');
      $x += $this->columnWidths[0];
      $this->pdf->setXY($x, $y);
    }
    for ($i = $this->firstColumnIndex, $j = count($row); $i < $j; $i++) {
      $x += $this->columnWidths[$i];

      if ($i == $this->rowBreak) {
        $i = $j;
        continue;
      }
      $this->pdf->multiCell($this->columnWidths[$i], $lineHeight, $row[$i], 1, 'R');
      $this->pdf->setXY($x, $y);
    }
    $this->pdf->ln();
  }
  /**
   * @author Giomani Designs (Development Team)
   * @return void
   */
  private function drawRows () {
    foreach ($this->tableRows as $row) {
      $lineHeight = $this->getLineHeight($row);
      if ($this->pdf->getY() + $lineHeight > $this->pageHeight) {
        $this->pdf->addPage();
        $this->drawHeaderRow();
      }
      $this->drawRow($row, $lineHeight);
    }
  }
  /**
   * @author Giomani Designs (Development Team)
   * @return void
   */
  private function drawTable () {
    do {
      $this->drawTitle();
      $this->drawHeaderRow();
      $this->drawRows();
      $this->firstColumnIndex = $this->rowBreak;
      if ($this->rowBreak != 0) {
        $this->rowBreak = 0;
        $this->pdf->addPage();
      }
    } while($this->firstColumnIndex != 0);
  }
  /**
   * @author Giomani Designs (Development Team)
   * @return void
   */
  private function drawTitle () {
    $this->pdf->multiCell(0, $this->lineHeight, $this->title . $this->rowContinuedText);
    if ($this->rowContinued === '') {
      $this->rowContinued = 1;
    } else {
      $this->rowContinued++;
    }
    $this->rowContinuedText = '';
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  array  $row array of row cells
   * @return integer
   */
  private function getLineHeight ($row) {
    if ($this->pdf->getStringWidth($row[0]) > $this->columnWidths[0] - 1) {
      return  $this->lineHeight * 2;
    } else {
      return  $this->lineHeight;
    }
  }
  /**
   * @author Giomani Designs (Development Team)
   * @return void
   */
  private function headerFont () {
    $this->pdf->SetFont('Arial', 'B', $this->baseFontSize + 2);
  }
  /**
   * @author Giomani Designs (Development Team)
   */
  public function out($dest = 'I') {
    return $this->pdf->outPdf($dest);
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  array  $headerRow array of column widths
   * @return void
   */
  private function rowFont () {
    $this->pdf->SetFont('Arial', '', $this->baseFontSize);
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  string  $size
   * @return void
   */
  public function setSize($size) {
    switch ($size) {
      case 'b':
        $fontSize = 11;
        $lineHeight = 6.5;
        break;
      case 'n':
      default:
        $fontSize = 9;
        $lineHeight = 5;
        break;
    }
    $this->baseFontSize = $fontSize;
    $this->lineHeight =  $lineHeight;
    return $this;
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  array   $columnWidths width of columns in em
   * @return void
   */
  public function setColumnWidths($columnWidths) {
    for ($i = 0, $j = count($columnWidths); $i < $j; $i++) {
      $this->columnWidths[] = $this->pdf->em($columnWidths[$i]);
    }
    return $this;
  }
  /**
   * @author Giomani Designs (Development Team)
   * @return void
   */
  public function table () {
    $this->rowContinued = '';
    $this->rowContinuedText = '';
    $this->headerFont();
    $this->firstColumnIndex = 1;
    $this->rowBreak = 0;
    $this->pdf->AddPage();
    $this->drawTable();
  }
}
