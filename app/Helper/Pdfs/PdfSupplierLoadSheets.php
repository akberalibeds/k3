<?php
// @author Giomani Designs (Development Team)
//
namespace App\Helper\Pdfs;

use App\Helper\Fpdf\Barcode;
use App\Helper\Pdfs\PdfHeaderInterface;

/**
 * @author Giomani Designs (Development Team)
 */
class PdfSupplierLoadSheets implements PdfHeaderInterface
{
  /**
   * @var
   */
  private $heading;
  /**
   *
   */
  protected $currentSupplier;
  /**
   * @var
   */
  private $pageWidth;
  /**
   *
   */
  public function __construct($pdf = null) {
    if ($pdf === null) {
      $this->pdf = new Pdf('p', 'mm', 'A4');
    } else {
      $this->pdf = $pdf;
    }
    $this->pageWidth = $this->pdf->GetPageWidth() - ($this->pdf->getRightMargin() + $this->pdf->getLeftMargin());
    $this->pdf->setHeaderObject($this);
  }
  /**
   * @author Giomani Designs (Development Team)
   * @see    App\Helper\Fpdf\FPDF::Header()
   */
  public function drawFooter() {
  }
  /**
   * @author Giomani Designs (Development Team)
   * @see    App\Helper\Fpdf\FPDF::Header()
   */
  public function drawHeader() {
    $font = $this->pdf->SetFont('Arial', 'B', 30);
    $this->pdf->Cell(0, 18, $this->heading, 0, 1);
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  array  $orders
   * @param  string $date
   * @param  array $labels
   */
  public function loadSheets($orders, $date) {
    $this->documentTitle = 'supplier_orders_' . $date . '_';
    $this->setHeading($orders[0]['supplier_name']);
    $this->pdf->AddPage();
    $this->pdf->SetFont('Arial', '', 10);
    $liSpace = $this->pdf->GetStringWidth('99. ');
    $li = 1;
    foreach ($orders as $order) {
      if (stripos($this->heading, $order['supplier_name']) === false) {
        $this->setHeading($order['supplier_name']);
        $this->pdf->AddPage();
        $li = 1;
      }
      $items = explode(';', $order['items']);
      $text = $li++ . '. ';
      $this->pdf->SetFont('Arial', 'B', 10);
      $this->pdf->Write(6.0, $text);
      $seperator = '';
      $text = '';
      foreach ($items as $itemParts) {
        $text .= $seperator;
        $item = explode(',', $itemParts);
        $text .= $item[0] . ' x ';
        if (strpos($item[1], 'MISSED PARTS') === 0 || strpos($item[1], 'SUPPLIER MISSED PARTS') === 0) {
          $text .= $item[2];
        } else {
          $text .= $item[3];
        }
        $seperator = ', ';
      }
      $this->pdf->SetFont('Arial', '', 10);
      $this->pdf->Write(6.0, $text);
      $this->pdf->SetFont('Arial', 'B', 10);
      $this->pdf->Write(6.0, ' - ' . $order['route_name']);
      if ($order['priority'] == 1) {
        $this->pdf->Write(6.0, 'PRIORITY ORDER');
      }
      $this->pdf->Ln(6.0);
    }
    return $this;
  }
  /**
   * @author Giomani Designs (Development Team)
   */
  public function outPdf($dest = 'I', $title = false) {
    return $this->pdf->outPdf($dest);
  }
  /**
   * @author Giomani Designs (Development Team)
   */
  private function setHeading($heading) {
    $this->heading = strToUpper($heading);
  }
}
