<?php

namespace App\Helper;



class BarclaysDirect {


	/*
	 * 
	 * Not part of POST PARAMETERS
	 */
	private $PW;
	private $DigestivePlain="";
	private $strHashedString_plain;
	public $url = 'https://payments.epdq.co.uk/ncol/prod/orderdirect.asp';
	private $required = ['PSPID','ORDERID','USERID','PSWD','AMOUNT','CURRENCY','CARDNO','ED','SHASign','CVC','OPERATION','PW'];
	private $skip = ['PW','DigestivePlain','strHashedString_plain','url','skip','required'];

	
	/**
	 *
	 * @var ACCEPTURL
	 */
	private $ACCEPTURL;
	
	/**
	 *
	 * @var ADDMATCH
	 */
	private $ADDMATCH;
	
	/**
	 *
	 * @var ADDRMATCH
	 */
	private $ADDRMATCH;
	
	/**
	 *
	 * @var AIACTIONNUMBER
	 */
	private $AIACTIONNUMBER;
	
	/**
	 *
	 * @var AIAGIATA
	 */
	private $AIAGIATA;
	
	/**
	 *
	 * @var AIAIRNAME
	 */
	private $AIAIRNAME;
	
	/**
	 *
	 * @var AIAIRTAX
	 */
	private $AIAIRTAX;
	
	
	
	
	
	/**
	 *
	 * @var AICHDET
	 */
	private $AICHDET;
	
	
	/**
	 *
	 * @var AICONJTI
	 */
	private $AICONJTI;
	
	/**
	 *
	 * @var AIDEPTCODE
	 */
	private $AIDEPTCODE;
	
	
	/**
	 *
	 * @var AIEYCD
	 */
	private $AIEYCD;
	
	
	/**
	 *
	 * @var AIGLNUM
	 */
	private $AIGLNUM;
	
	/**
	 *
	 * @var AIINVOICE
	 */
	private $AIINVOICE;
	
	/**
	 *
	 * @var AIIRST
	 */
	private $AIIRST;
	
	/**
	 *
	 * @var AIPASNAME
	 */
	private $AIPASNAME;
	
	/**
	 *
	 * @var AIPROJNUM
	 */
	private $AIPROJNUM;
	
	/**
	 *
	 * @var AITIDATE
	 */
	private $AITIDATE;
	
	/**
	 *
	 * @var AITINUM
	 */
	private $AITINUM;
	
	/**
	 *
	 * @var AITYPCH
	 */
	private $AITYPCH;
	
	/**
	 *
	 * @var AIVATAMNT
	 */
	private $AIVATAMNT;
	
	/**
	 *
	 * @var AIVATAPPL
	 */
	private $AIVATAPPL;
	
	/**
	 *
	 * @var ALIAS
	 */
	private $ALIAS;
	
	/**
	 *
	 * @var ALIASOPERATION
	 */
	private $ALIASOPERATION;
	
	/**
	 *
	 * @var ALIASPERSISTEDAFTERUSE
	 */
	private $ALIASPERSISTEDAFTERUSE;
	
	/**
	 *
	 * @var ALIASUSAGE
	 */
	private $ALIASUSAGE;
	
	/**
	 *
	 * @var AMOUNT
	 */
	private $AMOUNT;
	
	/**
	 *
	 * @var BACKURL
	 */
	private $BACKURL;
	
	/**
	 *
	 * @var BGCOLOR
	 */
	private $BGCOLOR;
	
	/**
	 *
	 * @var BIC
	 */
	private $BIC;
	
	/**
	 *
	 * @var BRAND
	 */
	private $BRAND;
	
	/**
	 *
	 * @var BUTTONBGCOLOR
	 */
	private $BUTTONBGCOLOR;
	
	/**
	 *
	 * @var BUTTONTXTCOLOR
	 */
	private $BUTTONTXTCOLOR;
	
	/**
	 *
	 * @var CANCELURL
	 */
	private $CANCELURL;
	
	/**
	 *
	 * @var CARDNO
	 */
	private $CARDNO;
	
	/**
	 *
	 * @var CATALOGURL
	 */
	private $CATALOGURL;
	
	/**
	 *
	 * @var CIVILITY
	 */
	private $CIVILITY;
	
	/**
	 *
	 * @var CN
	 */
	private $CN;
	
	/**
	 *
	 * @var COM
	 */
	private $COM;
	
	/**
	 *
	 * @var COMPLUS
	 */
	private $COMPLUS;
	
	/**
	 *
	 * @var CREDITCODE
	 */
	private $CREDITCODE;
	
	/**
	 *
	 * @var CUID
	 */
	private $CUID;
	
	/**
	 *
	 * @var CURRENCY
	 */
	private $CURRENCY = 'GBP';
	
	/**
	 *
	 * @var CVC
	 */
	private $CVC;
	
	/**
	 *
	 * @var CVC2
	 */
	private $CVC2;
	
	/**
	 *
	 * @var DATATYPE
	 */
	private $DATATYPE;
	
	/**
	 *
	 * @var DATEIN
	 */
	private $DATEIN;
	
	/**
	 *
	 * @var DATEOUT
	 */
	private $DATEOUT;
	
	/**
	 *
	 * @var DECLINEURL
	 */
	private $DECLINEURL;
	
	/**
	 *
	 * @var DEVICE
	 */
	private $DEVICE;
	
	/**
	 *
	 * @var ECI
	 */
	private $ECI = '1';
	
	/**
	 *
	 * @var ECI_3D
	 */
	private $ECI_3D;
	
	/**
	 *
	 * @var ECOM_BILLTO_POSTAL_CITY
	 */
	private $ECOM_BILLTO_POSTAL_CITY;
	
	/**
	 *
	 * @var ECOM_BILLTO_POSTAL_COUNTRYCODE
	 */
	private $ECOM_BILLTO_POSTAL_COUNTRYCODE;
	
	/**
	 *
	 * @var ECOM_BILLTO_POSTAL_COUNTY
	 */
	private $ECOM_BILLTO_POSTAL_COUNTY;
	
	/**
	 *
	 * @var ECOM_BILLTO_POSTAL_NAME_FIRST
	 */
	private $ECOM_BILLTO_POSTAL_NAME_FIRST;
	
	/**
	 *
	 * @var ECOM_BILLTO_POSTAL_NAME_LAST
	 */
	private $ECOM_BILLTO_POSTAL_NAME_LAST;
	
	/**
	 *
	 * @var ECOM_BILLTO_POSTAL_POSTALCODE
	 */
	private $ECOM_BILLTO_POSTAL_POSTALCODE;
	
	/**
	 *
	 * @var ECOM_BILLTO_POSTAL_STREET_LINE1
	 */
	private $ECOM_BILLTO_POSTAL_STREET_LINE1;
	
	/**
	 *
	 * @var ECOM_BILLTO_POSTAL_STREET_LINE2
	 */
	private $ECOM_BILLTO_POSTAL_STREET_LINE2;
	
	/**
	 *
	 * @var ECOM_BILLTO_POSTAL_STREET_NUMBER
	 */
	private $ECOM_BILLTO_POSTAL_STREET_NUMBER;
	
	/**
	 *
	 * @var ECOM_CONSUMER_GENDER
	 */
	private $ECOM_CONSUMER_GENDER;
	
	/**
	 *
	 * @var ECOM_CONSUMERID
	 */
	private $ECOM_CONSUMERID;
	
	/**
	 *
	 * @var ECOM_PAYMENT_CARD_VERIFICATION
	 */
	private $ECOM_PAYMENT_CARD_VERIFICATION;
	
	/**
	 *
	 * @var ECOM_SHIPMETHODDETAILS
	 */
	private $ECOM_SHIPMETHODDETAILS;
	
	/**
	 *
	 * @var ECOM_SHIPTO_COMPANY
	 */
	private $ECOM_SHIPTO_COMPANY;
	
	/**
	 *
	 * @var ECOM_SHIPTO_DOB
	 */
	private $ECOM_SHIPTO_DOB;
	
	/**
	 *
	 * @var ECOM_SHIPTO_ONLINE_EMAIL
	 */
	private $ECOM_SHIPTO_ONLINE_EMAIL;
	
	/**
	 *
	 * @var ECOM_SHIPTO_POSTAL_CITY
	 */
	private $ECOM_SHIPTO_POSTAL_CITY;
	
	/**
	 *
	 * @var ECOM_SHIPTO_POSTAL_COUNTRYCODE
	 */
	private $ECOM_SHIPTO_POSTAL_COUNTRYCODE;
	
	/**
	 *
	 * @var ECOM_SHIPTO_POSTAL_COUNTY
	 */
	private $ECOM_SHIPTO_POSTAL_COUNTY;
	
	/**
	 *
	 * @var ECOM_SHIPTO_POSTAL_NAME_FIRST
	 */
	private $ECOM_SHIPTO_POSTAL_NAME_FIRST;
	
	/**
	 *
	 * @var ECOM_SHIPTO_POSTAL_NAME_LAST
	 */
	private $ECOM_SHIPTO_POSTAL_NAME_LAST;
	
	/**
	 *
	 * @var ECOM_SHIPTO_POSTAL_NAME_PREFIX
	 */
	private $ECOM_SHIPTO_POSTAL_NAME_PREFIX;
	
	/**
	 *
	 * @var ECOM_SHIPTO_POSTAL_POSTALCODE
	 */
	private $ECOM_SHIPTO_POSTAL_POSTALCODE;
	
	/**
	 *
	 * @var ECOM_SHIPTO_POSTAL_STATE
	 */
	private $ECOM_SHIPTO_POSTAL_STATE;
	
	/**
	 *
	 * @var ECOM_SHIPTO_POSTAL_STREET_LINE1
	 */
	private $ECOM_SHIPTO_POSTAL_STREET_LINE1;
	
	/**
	 *
	 * @var ECOM_SHIPTO_POSTAL_STREET_LINE2
	 */
	private $ECOM_SHIPTO_POSTAL_STREET_LINE2;
	
	/**
	 *
	 * @var ECOM_SHIPTO_POSTAL_STREET_NUMBER
	 */
	private $ECOM_SHIPTO_POSTAL_STREET_NUMBER;
	
	/**
	 *
	 * @var ECOM_SHIPTO_TELECOM_FAX_NUMBER
	 */
	private $ECOM_SHIPTO_TELECOM_FAX_NUMBER;
	
	/**
	 *
	 * @var ECOM_SHIPTO_TELECOM_PHONE_NUMBER
	 */
	private $ECOM_SHIPTO_TELECOM_PHONE_NUMBER;
	
	/**
	 *
	 * @var ED
	 */
	private $ED;
	
	/**
	 *
	 * @var EMAIL
	 */
	private $EMAIL;
	
	/**
	 *
	 * @var EXCEPTIONURL
	 */
	private $EXCEPTIONURL;
	
	/**
	 *
	 * @var EXCLPMLIST
	 */
	private $EXCLPMLIST;
	
	/**
	 *
	 * @var FLAG3D
	 */
	private $FLAG3D;
	
	/**
	 *
	 * @var FONTTYPE
	 */
	private $FONTTYPE;
	
	/**
	 *
	 * @var GLOBORDERID
	 */
	private $GLOBORDERID;
	
	/**
	 *
	 * @var HOMEURL
	 */
	private $HOMEURL;
	
	/**
	 *
	 * @var HTTP_ACCEPT
	 */
	private $HTTP_ACCEPT;
	
	/**
	 *
	 * @var HTTP_USER_AGENT
	 */
	private $HTTP_USER_AGENT;
	
	/**
	 *
	 * @var IP
	 */
	private $IP;
	
	/**
	 *
	 * @var ISSUERID
	 */
	private $ISSUERID;

	
	/**
	 *
	 * @var LANGUAGE
	 */
	private $LANGUAGE = 'en_GB';
	
	
	
	/**
	 *
	 * @var LOGO
	 */
	private $LOGO;
	
	/**
	 *
	 * @var MANDATEID
	 */
	private $MANDATEID;
	
	/**
	 *
	 * @var OPERATION
	 */
	private $OPERATION;
	
	/**
	 *
	 * @var ORDERID
	 */
	private $ORDERID;
	
	/**
	 *
	 * @var ORDERSHIPCOST
	 */
	private $ORDERSHIPCOST;
	
	/**
	 *
	 * @var ORDERSHIPTAXCODE
	 */
	private $ORDERSHIPTAXCODE;
	
	/**
	 *
	 * @var OWNERADDRESS
	 */
	private $OWNERADDRESS;
	
	/**
	 *
	 * @var OWNERCTY
	 */
	private $OWNERCTY;
	
	/**
	 *
	 * @var OWNERTELNO
	 */
	private $OWNERTELNO;
	
	/**
	 *
	 * @var OWNERTOWN
	 */
	private $OWNERTOWN;
	
	/**
	 *
	 * @var OWNERZIP
	 */
	private $OWNERZIP;
	
	
	/**
	 *
	 * @var PARAMPLUS
	 */
	private $PARAMPLUS;
	
	/**
	 *
	 * @var PARAMVAR
	 */
	private $PARAMVAR;
	
	/**
	 *
	 * @var PAYID
	 */
	private $PAYID;
	
	/**
	 *
	 * @var PAYIDSUB
	 */
	private $PAYIDSUB;
	
	/**
	 *
	 * @var PAYMENTOCCURRENCE
	 */
	private $PAYMENTOCCURRENCE;
	
	/**
	 *
	 * @var PM
	 */
	private $PM;
	
	/**
	 *
	 * @var PMLIST
	 */
	private $PMLIST;
	
	/**
	 *
	 * @var PMLISTTYPE
	 */
	private $PMLISTTYPE;
	
	/**
	 *
	 * @var PSPID
	 */
	private $PSPID;
	
	/**
	 *
	 * @var PSWD
	 */
	private $PSWD;
	
	/**
	 *
	 * @var RECIPIENTACCOUNTNUMBER
	 */
	private $RECIPIENTACCOUNTNUMBER;
	
	/**
	 *
	 * @var RECIPIENTDOB
	 */
	private $RECIPIENTDOB;
	
	/**
	 *
	 * @var RECIPIENTLASTNAME
	 */
	private $RECIPIENTLASTNAME;
	
	/**
	 *
	 * @var RECIPIENTZIP
	 */
	private $RECIPIENTZIP;
	
	/**
	 *
	 * @var REF_CUSTOMERID
	 */
	private $REF_CUSTOMERID;
	
	/**
	 *
	 * @var REMOTE_ADDR
	 */
	private $REMOTE_ADDR;
	
	/**
	 *
	 * @var RTIMEOUT
	 */
	private $RTIMEOUT;
	
	/**
	 *
	 * @var SEQUENCETYPE
	 */
	private $SEQUENCETYPE;
	
	/**
	 *
	 * @var SHASIGN
	 */
	private $SHASIGN;
	
	
	
	/**
	 *
	 * @var SIGNDATE
	 */
	private $SIGNDATE;
	
	/**
	 *
	 * @var STATUS_3D
	 */
	private $STATUS_3D;
	
	/**
	 *
	 * @var SUB_AM
	 */
	private $SUB_AM;
	
	/**
	 *
	 * @var SUB_AMOUNT
	 */
	private $SUB_AMOUNT;
	
	/**
	 *
	 * @var SUB_COM
	 */
	private $SUB_COM;
	
	/**
	 *
	 * @var SUB_COMMENT
	 */
	private $SUB_COMMENT;
	
	/**
	 *
	 * @var SUB_CUR
	 */
	private $SUB_CUR;
	
	/**
	 *
	 * @var SUB_ENDDATE
	 */
	private $SUB_ENDDATE;
	
	/**
	 *
	 * @var SUB_ORDERID
	 */
	private $SUB_ORDERID;
	
	/**
	 *
	 * @var SUB_PERIOD_MOMENT
	 */
	private $SUB_PERIOD_MOMENT;
	
	/**
	 *
	 * @var SUB_PERIOD_NUMBER
	 */
	private $SUB_PERIOD_NUMBER;
	
	/**
	 *
	 * @var SUB_PERIOD_UNIT
	 */
	private $SUB_PERIOD_UNIT;
	
	/**
	 *
	 * @var SUB_STARTDATE
	 */
	private $SUB_STARTDATE;
	
	/**
	 *
	 * @var SUB_STATUS
	 */
	private $SUB_STATUS;
	
	/**
	 *
	 * @var SUBSCRIPTION_ID
	 */
	private $SUBSCRIPTION_ID;
	
	/**
	 *
	 * @var TAXINCLUDED
	 */
	private $TAXINCLUDED;
	
	/**
	 *
	 * @var TBLBGCOLOR
	 */
	private $TBLBGCOLOR;
	
	/**
	 *
	 * @var TBLTXTCOLOR
	 */
	private $TBLTXTCOLOR;
	
	/**
	 *
	 * @var TITLE
	 */
	private $TITLE;
	
	/**
	 *
	 * @var TP
	 */
	private $TP;
	
	/**
	 *
	 * @var TXTCOLOR
	 */
	private $TXTCOLOR;
	
	/**
	 *
	 * @var USERID
	 */
	private $USERID;
	
	/**
	 *
	 * @var WIN3DS
	 */
	private $WIN3DS;
	
	


	public function __construct(){


	}


	private function digest(){

		
		$all = get_object_vars($this);
		
		foreach($all as $key => $val){
			
			if(!in_array($key ,$this->skip)){
			
				$this->DigestivePlain .= ($this->{$key}) ? "$key=" . $val . $this->PW : '';
			
			}
		}
		



		$this->DigestivePlain .= "";

		//=SHA encrypt the string=//
		//$this->strHashedString_plain = strtoupper(sha1($this->DigestivePlain));
		$this->strHashedString_plain = strtoupper(openssl_digest($this->DigestivePlain, 'sha512'));


	}

	public function request(){

		$this->digest();
		$params = [];
		$html = '';
		$all = get_object_vars($this);
		
		$symbol = "";
		$i=0;
		foreach($all as $key => $val){
				
			if(!in_array($key ,$this->skip)){
				$html.= ($this->$key) ? "$symbol$key=$val" : '';
					
				if($this->$key){
					$symbol = "&";
					$params[$key]=$val;
				}
				
			}
			
		}
		
		$params['SHASign'] = $this->strHashedString_plain;
		$html.='&SHASign='.$this->strHashedString_plain;
		
		return $params;
		//return $html;
	}
	
	
	private function checkRequired($params){
		
		$data=[];
		foreach($this->required as $k){
			if(in_array($k,$this->skip)){
				if(!$this->$k){
					$data[]=$k;
				}
			}
			else if(!isset($params[$k])){
				$data[]=$k;
			}
		}
		return $data;
	}
	
	
	
	public function send(){
		
		$fields = $this->request();
		
		if($data = $this->checkRequired($fields)){
			
			return ['error' => 'Required Fields Missing!!' , 'required' => $data];
			
		}
		
		$postdata = http_build_query($fields);
		
		$opts = array(
				'http' => array(
						'method'  => 'POST',
						'header'  => 'Content-type: application/x-www-form-urlencoded',
						'content' => $postdata
				)
		);
		$context  = stream_context_create($opts);
		$result = file_get_contents($this->url, false, $context);
	
		$responseDoc = new \DomDocument();
		$responseDoc->loadXML($result);
		$response = simplexml_import_dom($responseDoc);
		$attrArray = [$response->attributes()];
		
		foreach($response->attributes() as $key=>$val){
			$attrArray[(string)$key] = (string)$val;
		}
		
		unset($attrArray[0]);
		return $attrArray;
	}
		

	public function setTestMode($val = false){
		$this->url = ($val) ? "https://mdepayments.epdq.co.uk/ncol/test/testodl.asp"/*"https://mdepayments.epdq.co.uk/ncol/test/orderdirect.asp"*/ : "https://payments.epdq.co.uk/ncol/prod/orderdirect.asp";
	}

	
	public function setShaIn($val){
		$this->PW = $val;
	}
	
	public function setDigestivePlain($val){
		$this->DigestivePlain = $val;
	}
	
	public function setstrHashedString_plain($val){
		$this->strHashedString_plain = $val;
	}
	
	
	public function setACCEPTURL($val){
		$this->ACCEPTURL = $val;
	}
	
	public function setADDMATCH($val){
		$this->ADDMATCH = $val;
	}
	
	public function setADDRMATCH($val){
		$this->ADDRMATCH = $val;
	}
	
	public function setAIACTIONNUMBER($val){
		$this->AIACTIONNUMBER = $val;
	}
	
	public function setAIAGIATA($val){
		$this->AIAGIATA = $val;
	}
	
	public function setAIAIRNAME($val){
		$this->AIAIRNAME = $val;
	}
	
	public function setAIAIRTAX($val){
		$this->AIAIRTAX = $val;
	}
	
	public function setAICHDET($val){
		$this->AICHDET = $val;
	}
	
	public function setAICONJTI($val){
		$this->AICONJTI = $val;
	}
	
	public function setAIDEPTCODE($val){
		$this->AIDEPTCODE = $val;
	}
	
	public function setAIEYCD($val){
		$this->AIEYCD = $val;
	}
	
	public function setAIGLNUM($val){
		$this->AIGLNUM = $val;
	}
	
	public function setAIINVOICE($val){
		$this->AIINVOICE = $val;
	}
	
	public function setAIIRST($val){
		$this->AIIRST = $val;
	}
	
	public function setAIPASNAME($val){
		$this->AIPASNAME = $val;
	}
	
	public function setAIPROJNUM($val){
		$this->AIPROJNUM = $val;
	}
	
	public function setAITIDATE($val){
		$this->AITIDATE = $val;
	}
	
	public function setAITINUM($val){
		$this->AITINUM = $val;
	}
	
	public function setAITYPCH($val){
		$this->AITYPCH = $val;
	}
	
	public function setAIVATAMNT($val){
		$this->AIVATAMNT = $val;
	}
	
	public function setAIVATAPPL($val){
		$this->AIVATAPPL = $val;
	}
	
	public function setALIAS($val){
		$this->ALIAS = $val;
	}
	
	public function setALIASOPERATION($val){
		$this->ALIASOPERATION = $val;
	}
	
	public function setALIASPERSISTEDAFTERUSE($val){
		$this->ALIASPERSISTEDAFTERUSE = $val;
	}
	
	public function setALIASUSAGE($val){
		$this->ALIASUSAGE = $val;
	}
	
	public function setAMOUNT($val){
		 $this->AMOUNT = str_replace(".", "", str_replace(",", "", $val));
	}
	
	public function setBACKURL($val){
		$this->BACKURL = $val;
	}
	
	public function setBGCOLOR($val){
		$this->BGCOLOR = $val;
	}
	
	public function setBIC($val){
		$this->BIC = $val;
	}
	
	public function setBRAND($val){
		$this->BRAND = $val;
	}
	
	public function setBUTTONBGCOLOR($val){
		$this->BUTTONBGCOLOR = $val;
	}
	
	public function setBUTTONTXTCOLOR($val){
		$this->BUTTONTXTCOLOR = $val;
	}
	
	public function setCANCELURL($val){
		$this->CANCELURL = $val;
	}
	
	public function setCARDNO($val){
		$this->CARDNO = $val;
	}
	
	public function setCATALOGURL($val){
		$this->CATALOGURL = $val;
	}
	
	public function setCIVILITY($val){
		$this->CIVILITY = $val;
	}
	
	public function setCustomerName($val){
		$this->CN = $val;
	}
	
	public function setCOM($val){
		$this->COM = $val;
	}
	
	public function setCOMPLUS($val){
		$this->COMPLUS = $val;
	}
	
	public function setCREDITCODE($val){
		$this->CREDITCODE = $val;
	}
	
	public function setCUID($val){
		$this->CUID = $val;
	}
	
	public function setCURRENCY($val){
		$this->CURRENCY = $val;
	}
	
	public function setCVC($val){
		$this->CVC = $val;
	}
	
	public function setCVC2($val){
		$this->CVC2 = $val;
	}
	
	
	public function setDATATYPE($val){
		$this->DATATYPE = $val;
	}
	
	public function setDATEIN($val){
		$this->DATEIN = $val;
	}
	
	public function setDATEOUT($val){
		$this->DATEOUT = $val;
	}
	
	public function setDECLINEURL($val){
		$this->DECLINEURL = $val;
	}
	
	public function setDEVICE($val){
		$this->DEVICE = $val;
	}
	
	public function setECI($val){
		$this->ECI = $val;
	}
	
	public function setECI_3D($val){
		$this->ECI_3D = $val;
	}
	
	public function setECOM_BILLTO_POSTAL_CITY($val){
		$this->ECOM_BILLTO_POSTAL_CITY = $val;
	}
	
	public function setECOM_BILLTO_POSTAL_COUNTRYCODE($val){
		$this->ECOM_BILLTO_POSTAL_COUNTRYCODE = $val;
	}
	
	public function setECOM_BILLTO_POSTAL_COUNTY($val){
		$this->ECOM_BILLTO_POSTAL_COUNTY = $val;
	}
	
	public function setECOM_BILLTO_POSTAL_NAME_FIRST($val){
		$this->ECOM_BILLTO_POSTAL_NAME_FIRST = $val;
	}
	
	public function setECOM_BILLTO_POSTAL_NAME_LAST($val){
		$this->ECOM_BILLTO_POSTAL_NAME_LAST = $val;
	}
	
	public function setECOM_BILLTO_POSTAL_POSTALCODE($val){
		$this->ECOM_BILLTO_POSTAL_POSTALCODE = $val;
	}
	
	public function setECOM_BILLTO_POSTAL_STREET_LINE1($val){
		$this->ECOM_BILLTO_POSTAL_STREET_LINE1 = $val;
	}
	
	public function setECOM_BILLTO_POSTAL_STREET_LINE2($val){
		$this->ECOM_BILLTO_POSTAL_STREET_LINE2 = $val;
	}
	
	public function setECOM_BILLTO_POSTAL_STREET_NUMBER($val){
		$this->ECOM_BILLTO_POSTAL_STREET_NUMBER = $val;
	}
	
	public function setECOM_CONSUMER_GENDER($val){
		$this->ECOM_CONSUMER_GENDER = $val;
	}
	
	public function setECOM_CONSUMERID($val){
		$this->ECOM_CONSUMERID = $val;
	}
	
	public function setECOM_PAYMENT_CARD_VERIFICATION($val){
		$this->ECOM_PAYMENT_CARD_VERIFICATION = $val;
	}
	
	public function setECOM_SHIPMETHODDETAILS($val){
		$this->ECOM_SHIPMETHODDETAILS = $val;
	}
	
	public function setECOM_SHIPTO_COMPANY($val){
		$this->ECOM_SHIPTO_COMPANY = $val;
	}
	
	public function setECOM_SHIPTO_DOB($val){
		$this->ECOM_SHIPTO_DOB = $val;
	}
	
	public function setECOM_SHIPTO_ONLINE_EMAIL($val){
		$this->ECOM_SHIPTO_ONLINE_EMAIL = $val;
	}
	
	public function setECOM_SHIPTO_POSTAL_CITY($val){
		$this->ECOM_SHIPTO_POSTAL_CITY = $val;
	}
	
	public function setECOM_SHIPTO_POSTAL_COUNTRYCODE($val){
		$this->ECOM_SHIPTO_POSTAL_COUNTRYCODE = $val;
	}
	
	public function setECOM_SHIPTO_POSTAL_COUNTY($val){
		$this->ECOM_SHIPTO_POSTAL_COUNTY = $val;
	}
	
	public function setECOM_SHIPTO_POSTAL_NAME_FIRST($val){
		$this->ECOM_SHIPTO_POSTAL_NAME_FIRST = $val;
	}
	
	public function setECOM_SHIPTO_POSTAL_NAME_LAST($val){
		$this->ECOM_SHIPTO_POSTAL_NAME_LAST = $val;
	}
	
	public function setECOM_SHIPTO_POSTAL_NAME_PREFIX($val){
		$this->ECOM_SHIPTO_POSTAL_NAME_PREFIX = $val;
	}
	
	public function setECOM_SHIPTO_POSTAL_POSTALCODE($val){
		$this->ECOM_SHIPTO_POSTAL_POSTALCODE = $val;
	}
	
	public function setECOM_SHIPTO_POSTAL_STATE($val){
		$this->ECOM_SHIPTO_POSTAL_STATE = $val;
	}
	
	public function setECOM_SHIPTO_POSTAL_STREET_LINE1($val){
		$this->ECOM_SHIPTO_POSTAL_STREET_LINE1 = $val;
	}
	
	public function setECOM_SHIPTO_POSTAL_STREET_LINE2($val){
		$this->ECOM_SHIPTO_POSTAL_STREET_LINE2 = $val;
	}
	
	public function setECOM_SHIPTO_POSTAL_STREET_NUMBER($val){
		$this->ECOM_SHIPTO_POSTAL_STREET_NUMBER = $val;
	}
	
	public function setECOM_SHIPTO_TELECOM_FAX_NUMBER($val){
		$this->ECOM_SHIPTO_TELECOM_FAX_NUMBER = $val;
	}
	
	public function setECOM_SHIPTO_TELECOM_PHONE_NUMBER($val){
		$this->ECOM_SHIPTO_TELECOM_PHONE_NUMBER = $val;
	}
	
	public function setED($val){
		$this->ED = $val;
	}
	
	public function setEMAIL($val){
		$this->EMAIL = $val;
	}
	
	public function setEXCEPTIONURL($val){
		$this->EXCEPTIONURL = $val;
	}
	
	public function setEXCLPMLIST($val){
		$this->EXCLPMLIST = $val;
	}
	
	public function setFLAG3D($val){
		$this->FLAG3D = $val;
	}
	
	public function setFONTTYPE($val){
		$this->FONTTYPE = $val;
	}
	
	public function setGLOBORDERID($val){
		$this->GLOBORDERID = $val;
	}
	
	public function setHOMEURL($val){
		$this->HOMEURL = $val;
	}
	
	public function setHTTP_ACCEPT($val){
		$this->HTTP_ACCEPT = $val;
	}
	
	public function setHTTP_USER_AGENT($val){
		$this->HTTP_USER_AGENT = $val;
	}
	
	public function setIP($val){
		$this->IP = $val;
	}
	
	public function setISSUERID($val){
		$this->ISSUERID = $val;
	}
	
	public function setLANGUAGE($val){
		$this->LANGUAGE = $val;
	}
	
	public function setLOGO($val){
		$this->LOGO = $val;
	}
	
	public function setMANDATEID($val){
		$this->MANDATEID = $val;
	}
	
	public function setOPERATION($val){
		$this->OPERATION = $val;
	}
	
	public function setORDERID($val){
		$this->ORDERID = $val;
	}
	
	public function setORDERSHIPCOST($val){
		$this->ORDERSHIPCOST = $val;
	}
	
	public function setORDERSHIPTAXCODE($val){
		$this->ORDERSHIPTAXCODE = $val;
	}
	
	public function setAddress($val){
		$this->OWNERADDRESS = $val;
	}
	
	public function setCountry($val){
		$this->OWNERCTY = $val;
	}
	
	public function setTel($val){
		$this->OWNERTELNO = $val;
	}
	
	public function setTown($val){
		$this->OWNERTOWN = $val;
	}
	
	public function setPostcode($val){
		$this->OWNERZIP = $val;
	}
	
	public function setPARAMPLUS($val){
		$this->PARAMPLUS = $val;
	}
	
	public function setPARAMVAR($val){
		$this->PARAMVAR = $val;
	}
	
	public function setPAYID($val){
		$this->PAYID = $val;
	}
	
	public function setPAYIDSUB($val){
		$this->PAYIDSUB = $val;
	}
	
	public function setPAYMENTOCCURRENCE($val){
		$this->PAYMENTOCCURRENCE = $val;
	}
	
	public function setPM($val){
		$this->PM = $val;
	}
	
	public function setPMLIST($val){
		$this->PMLIST = $val;
	}
	
	public function setPMLISTTYPE($val){
		$this->PMLISTTYPE = $val;
	}
	
	public function setPSPID($val){
		$this->PSPID = $val;
	}
	
	
	public function setRECIPIENTACCOUNTNUMBER($val){
		$this->RECIPIENTACCOUNTNUMBER = $val;
	}
	
	public function setRECIPIENTDOB($val){
		$this->RECIPIENTDOB = $val;
	}
	
	public function setRECIPIENTLASTNAME($val){
		$this->RECIPIENTLASTNAME = $val;
	}
	
	public function setRECIPIENTZIP($val){
		$this->RECIPIENTZIP = $val;
	}
	
	public function setREF_CUSTOMERID($val){
		$this->REF_CUSTOMERID = $val;
	}
	
	public function setREMOTE_ADDR($val){
		$this->REMOTE_ADDR = $val;
	}
	
	public function setRTIMEOUT($val){
		$this->RTIMEOUT = $val;
	}
	
	public function setSEQUENCETYPE($val){
		$this->SEQUENCETYPE = $val;
	}
	
	public function setSHASIGN($val){
		$this->SHASIGN = $val;
	}
	
	public function setSIGNDATE($val){
		$this->SIGNDATE = $val;
	}
	
	public function setSTATUS_3D($val){
		$this->STATUS_3D = $val;
	}
	
	public function setSUB_AM($val){
		$this->SUB_AM = $val;
	}
	
	public function setSUB_AMOUNT($val){
		$this->SUB_AMOUNT = $val;
	}
	
	public function setSUB_COM($val){
		$this->SUB_COM = $val;
	}
	
	public function setSUB_COMMENT($val){
		$this->SUB_COMMENT = $val;
	}
	
	public function setSUB_CUR($val){
		$this->SUB_CUR = $val;
	}
	
	public function setSUB_ENDDATE($val){
		$this->SUB_ENDDATE = $val;
	}
	
	public function setSUB_ORDERID($val){
		$this->SUB_ORDERID = $val;
	}
	
	public function setSUB_PERIOD_MOMENT($val){
		$this->SUB_PERIOD_MOMENT = $val;
	}
	
	public function setSUB_PERIOD_NUMBER($val){
		$this->SUB_PERIOD_NUMBER = $val;
	}
	
	public function setSUB_PERIOD_UNIT($val){
		$this->SUB_PERIOD_UNIT = $val;
	}
	
	public function setSUB_STARTDATE($val){
		$this->SUB_STARTDATE = $val;
	}
	
	public function setSUB_STATUS($val){
		$this->SUB_STATUS = $val;
	}
	
	public function setSUBSCRIPTION_ID($val){
		$this->SUBSCRIPTION_ID = $val;
	}
	
	public function setTAXINCLUDED($val){
		$this->TAXINCLUDED = $val;
	}
	
	public function setTBLBGCOLOR($val){
		$this->TBLBGCOLOR = $val;
	}
	
	public function setTBLTXTCOLOR($val){
		$this->TBLTXTCOLOR = $val;
	}
	
	public function setTITLE($val){
		$this->TITLE = $val;
	}
	
	public function setTP($val){
		$this->TP = $val;
	}
	
	public function setTXTCOLOR($val){
		$this->TXTCOLOR = $val;
	}
	
	public function setUSERID($val){
		$this->USERID = $val;
	}
	
	public function setWIN3DS($val){
		$this->WIN3DS = $val;
	}
	
	public function setPSWD($val){
		$this->PSWD = $val;
	}
	
	

}
