<?php

namespace App\Helper;

use Time;
use App\Order;
use App\ToShip;
use App\Texts;
use App\Mailer;
use App\Helper\SMS;
use App\Helper\Ebay\Ebay;



class DeliveryNotifications {
	
	public $date;
	
	public $dater;
	
	public $ebayDate;
	
	public $customers;
	
	public $sent = [];
	
	public function __construct()
	{
		
		$this->date = Time::date()->days(1)->get('d-M-Y');
		$this->dater = Time::date()->get('d-M-Y');
		$this->ebayDate = Time::date()->days(1)->get('d M Y');

		$this->customers = ToShip::customersToNotify($this->dater);
		
		ToShip::removeOld($this->dater);
		
	}
	
	
	// public function customersToNotify()
	// {
		
	// 	$this->customers = ToShip::customersToNotify($this->dater);
	// 	return $this;				
	
	// }
	
	
	public function handle()
	{
	
		foreach($this->customers as $customer)
		{
			
			if($customer->isMail)
			{
				echo "Send Mail\n";	
				
				$this->sendEmail($customer);
				
				if($customer->ebayID!='' && $customer->userID!='')
				{
					echo "Send eBay Message\n";
					$this->sendEbayMessage($customer);	
				}
			}
			
			
			if($customer->isTel)
			{
				echo "Send SMS\n";
				$this->sendSMS($customer);	
			}
				
		}
		
		$this->removeDone();
	}



	
	
	/*
	 * Send Email
 	 */
	private function sendEmail($customer)
	{
		$customer->slot = $this->timeSlot($customer->delTime);
		Mailer::sendDispatchedMail($customer);
	
	}
	
	
	/*
	 * Send eBay Message
	 */
	private function sendEbayMessage($customer)
	{
		
		$ebay = new Ebay;
		// item date slot buyer code

		$date = $this->ebayDate;
		$slot = $this->timeSlot($customer->delTime);
		$item = explode("-",$customer->ebayID);
		
		$ebay->send_message([
			'item' => $item[0] ,
			'subject' => 'Your items have been dispatched',
			'body' => "Your items have been dispatched, and will be delivered to you on $date between the hours of $slot.\n\nThank you for your business</Body>",
			'buyer' => $customer->userID , 
			'code' => $customer->code 
	    ]);
	
	}
	
	
	private function sendSMS($customer)
	{
		
		$call="0121 568 8878";
		
		if($customer->done==0){
			$msg="Your Furniture Order will be delivered tomorrow. To decline delivery reply NO by 16:00 Today. For further information please call $call.";
		}
		else{
			$msg="Your order has been dispatched to be delivered tomorrow between ".$this->timeSlot($customer->delTime).".";// To Track and Trace your order go to:\nhttp://premierdeliveries.co.uk/?$tel[6]";	
		}
		
		$sms = new SMS;
		if($sms->is_connected()){
                        
                  $number = $sms->formatNumber($customer->tel).",".$sms->formatNumber($customer->mob);
			$response = $sms->sendSMS($sms->formatNumber($customer->tel).",".$sms->formatNumber($customer->mob),$msg); 
		
			if($response['status']=="success"){
				Texts::add($customer);
// 				Order::where('id',$customer->oid)->update(['txt' => 1]);
				$this->sent[]=$customer->oid;
				echo( "Txt Sent to $number\n");
			}
			else { 
                          $status=$response['status'];
                          echo( "Failed Sms $status $number\n"); }	
		}
	}
	
	
	
	
	
	private function timeSlot($time)
	{
	
		$time.=":00";
		$startTime = Time::date($time)->hours(-1)->get('H:00');
		$finishTime = Time::date($time)->hours(5)->get('H:00');
		
		if (strtotime($startTime)<strtotime("7:00:00")){
				$startTime="07:00";
		}
	
		if (strtotime($finishTime)>strtotime("21:00:00")){
				$finishTime="21:00";
		}
			
		for($u=0;$u<7;$u++){
			if ($finishTime=="0$u:00"){
					$finishTime="21:00";
			}
		}
				
		return "$startTime-$finishTime";
	}
	
	
	
	
	public function removeDone()
	{
		
	    ToShip::whereIn('oid',$this->sent)->delete();
	    Order::whereIn('id',$this->sent)->update(['txt' => 1]);
		//foreach($this->customers as $customer)
		//{
			//ToShip::where('oid',$customer->oid)->delete();	
		//}
		
	}
	


}




		
/*




	
	if($isMail==1){ 
	
	$array=array();
	$array['code']=$code;
	$array['name']=$name;
	$array['email']=$email;
	$array['slot']=$slot;
	$emails[]=$array;
	
	if($db_field['userID']!=""){$array=array(); $array[]=$db_field['userID'];$array[]=$db_field['ebayID'];$array[]=$date; $array[]=$db_field['ebay']; $array[]=$code; $array[]=$slot; $array[]=$shipped; $ebayMail[]= $array;} 
	if($db_field['ebayID']!=""){ $array=array(); $array['ebayID']=$db_field['ebayID']; $array['folder']=$db_field['ebay']; $array['code']=$db_field['code']; $array['shipped']=$shipped; $ebayIDS[]=$array; $isEbay=1;}  
	}
	
	if($isTel==1){
	$tel=getNum($db_field['tel']); 
	if($tel!=""){$array=array();  $array[]=$tel; $array[]=$isEbay; $array[]=$isTel; $array[]=$oid; $array[]=$db_field['done']; $array[]=$slot; $array[]=$code;  $tels[]=$array;}
	}
	
	}// while db loop end
	
	
	}// for loop end
	
	if(!class_exists('text')){ include '../text.php'; }


	$texter = new text;
	
	// send text to each mobile number
	foreach($tels as $tel){
	$call="0121 568 8878";
	
	
	if($tel[4]==0){
	$msg="Your Furniture Order will be delivered tomorrow. To decline delivery reply NO by 16:00 Today. For further information please call $call.";
	}
	else{
	$msg="Your order has been dispatched to be delivered tomorrow between $tel[5].";// To Track and Trace your order go to:\nhttp://premierdeliveries.co.uk/?$tel[6]";	
	}
	
	
	//echo "sent $tel[0],$msg\n\n";
	if(is_connected()){ $response = $texter->sendSMS($tel[0],$msg); 
	
	if($response['status']=="success"){
	$date =date('d-M-Y');
	$string=mysqli_real_escape_string($link,$tel[0]); 
	mysqli_query($link,"insert into txts (num,oid,date) values ('".$string."',".$tel[3].",'$date')");  
	mysqli_query($link,"update orders set txt=1 where id=".$tel[3]); 
	echo( "Txt Sent to $tel[0]\n");
	}
	else { echo( "Failed to $tel[0]\n"); }
	
	}
	}
	
	
	
	
	// send ebay msg to notify delivery
	foreach($ebayMail as $mail){
		
	$user 		= $mail[0];
	$item 		= explode("-",$mail[1]);
	$item 		= $item[0];
	$date 		= $mail[2];
	$folder 	= $mail[3];
	$coder 		= $mail[4];
	$sloter 	= $mail[5];
	$shipped 	= $mail[6];
	echo( "Ebay Msg Sent to $user\n\n");	
	$a = get("http://localhost/giomani/$folder/message.php?buyer=$user&item=$item&date=".urlencode($ebayDate)."&code=".urlencode($coder)."&slot=".urlencode($sloter));
	//if (strpos($a,"Success") !== false) { echo 'Done'; } else { echo $a; }
	}
	
	
	foreach($emails as $m){
		//code , email , slot , name  
		get("http://localhost/giomani/ebay/dispEmail.php?code=$m[code]&email=$m[email]&slot=$m[slot]&name=$m[name]");
	}
	
	
	
	foreach($orders as $order){
	$oid=$order[0];
	if(!mysqli_query($link,"delete from toShip where oid=$oid")){echo mysqli_error($link);}
	}
	
	mysqli_close($link);
	//if($this->i==0){
	//$this->i++;	
	//sleep(300);
	//$this->start();
	//}
}


}// class getStuff Finish
?>
*/