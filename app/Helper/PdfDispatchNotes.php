<?php
// @author Giomani Designs **/
//
namespace App\Helper;

use App\OrderNotes;
use App\Helper\Pdfs\Pdf;
use App\Helper\Fpdf\Barcode;
use App\Helper\Pdfs\PdfInvoice;
use App\Helper\Traits\OrderUtils;

/**
 * @author Giomani Designs (Development Team)
 */
class PdfDispatchNotes extends Pdf
{
  use OrderUtils;
  /**
   *
   */
  protected $routeNames;
  /**
   * @author Giomani Designs (Development Team)
   */
  public function __construct() {
    parent::__construct('P', 'mm', [210, 297]);
    $this->routeNames = [];
    $this->showFooter = true;
    $this->showHeader = true;
  }
  /**
   * @author Giomani Designs (Development Team)
   * @see    App\Helper\Fpdf\FPDF::Footer()
   */
  function Footer() {
    if ($this->showFooter) {
      $this->SetY(-12);
      $this->SetFont('Arial', 'B', 10);
      $this->Cell(0, 6.0, $this->header['route'] . ', ' . $this->header['date'] . ', page ' . $this->pageNumber++, 0, 1, 'R');
    }
  }
  /**
   * @author Giomani Designs (Development Team)
   * @see    App\Helper\Fpdf\FPDF::Header()
   */
  function Header() {
    if ($this->showHeader) {
      $this->SetFont('Arial', 'B', 30);
      $this->Cell(0.0, 10.0, $this->header['route'] . ', ' . $this->header['date'], 0, 1, 'C');
      $this->SetFont('Arial', 'B', 14);
      $this->Cell(203.0, 10.0,  'Vehicle: ' . $this->header['vehicle'] , 0, 1);
      $y = $this->GetY() - 2.0;
      $this->Line(0.0, $y, $this->GetPageWidth(), $y);
      $this->continued = true;
    }
  }
  /**
   * @author Giomani Designs (Development Team)
   * @return void
   */
  public function getOrderNotes() {
    $this->documentTitle = 'OrderNotes_' . implode('_', $this->routeNames);
    return parent::outPdf();
  }
  /**
   *
   */
  public function newRoute() {
    $this->AddPage();
  }
  /**
   *
   */
  public function outPdf($dest = 'I', $title = false) {
    $this->documentTitle = 'DispatchNotes_' . implode('_', $this->routeNames);
    return parent::outPdf($dest);
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param Datetime $date the date to go on the label
   * @param array $dispatchNotes
   * @return App\Helper\Pdf;
   */
  public function pdfDispatchNotes($dispatchNotes, $dispRoute) {
    $notes = [];

    $this->pdfPickList($dispatchNotes);

    $this->showFooter = true;
    $this->showHeader = true;

    $this->SetAutoPageBreak(false);
    $this->setPageNumber(1);
    $this->AddPage();

    $pageHeight = $this->GetPageHeight() - 20;
    $pageWidth = $this->GetPageWidth();

    $height = 0;
    $li = 0;

    foreach ($dispatchNotes as $dispatchNote) {
      if ($this->getY() + $height > $pageHeight) {
        $this->AddPage();
      }

      $delivery = $this->getDeliveryAddress($dispatchNote, $dispatchNote['direct']);

      $y0 = $this->getY();

      $this->SetFont('Arial', 'B', 11);
      $this->Cell(10.0, 6.0, (++$li) . ' . ', 0, 0, 'R');
      $this->SetFont('Arial', '', 11);
      $offset = $this->getX();
      $bits = [$delivery['name'], $delivery['ad0'], $delivery['ad1'], $delivery['telephone']];

      $width = 0;
      $seperator = '';
      $this->SetFont('Arial', 'B', 11);
      foreach ($bits as $bit) {
        $text = $seperator . $bit;
        $seperator = ', ';

        $w = $this->GetStringWidth($text);
        if ($width + $w > $pageWidth) {
          $this->Ln();
        }
        $this->Cell($w, 6.0, $text, 0, 0);
        $this->SetFont('Arial', '', 11);
      }
      $this->Ln();
      $this->setX($offset);
      $this->SetFont('Arial', 'B', 11);
      $this->Cell(20.0, 6.0, strtoupper($delivery['postcode']), 0, 0);
      $this->SetFont('Arial', '', 11);

      $collect = 0;
      if ($dispatchNote['id_seller'] == 0) {
        $collect = $dispatchNote['total'] - $dispatchNote['paid'];
      }
      $this->SetFont('Arial', '', 18);
      $this->Cell(32.0, 6.0, number_format($collect, 2), 0, 0, 'R');

      $this->SetFont('Arial', '', 11);

      $y = $this->GetY();

      if ($dispatchNote['g_id'] > 0) {
        $co = 'Order: ' . $dispatchNote['g_num'] . ', Item: ' . $dispatchNote['g_id'];
      } else {
        $co = $dispatchNote['companyName'];
      }

      $co .= ' (' . $dispatchNote['oid'] . ')';
      $this->Cell(100.0, 6.0, $co, 0, 0);

      // barcode
      $angle = 0;   // rotation in degrees
      $fontSize = 10;
      $height = 5;   // barcode height in 1D ; module size in 2D
      $marge = 2;   // between barcode and hri in pixel
      $width = 0.5;    // barcode height in 1D ; not use in 2D
      $bx = $this->GetPageWidth() - 34.0;  // barcode center
      $by = $y + ($height / 2);  // barcode center
      $type = 'code128';
      $black = '000000'; // color in hex

      // $strBarcode = str_pad($dispatchNote['oid'], 7, '0', STR_PAD_LEFT);
      $strBarcode = $dispatchNote['oid'];

      $barcode = Barcode::fpdf($this, $black, $bx, $by, $angle, $type, $strBarcode, $width, $height);

      if ($dispatchNote['priority'] == 1) {
        $this->SetXY(0.0, $y);
        $this->Cell(0.0, 6.0, 'PRIORITY ORDER', 0, 1, 'R');
      }

      $this->Ln();
      $this->setX($offset);

      $seperator = ', ';
      $orderItems = explode(';', $dispatchNote['items']);

      $textCollections = '';
      $textItems = '';
      $partsRecieved = 0;
      for ($i = 0, $j = count($orderItems); $i < $j; $i++) {
        if ($i + 1 == $j) {
          $seperator = null;
        }

        $item = explode(',', $orderItems[$i]);
        if (strpos($item[1], 'MISSED PART') !== false || strpos($item[1], 'Deliver Item') !== false || strpos($item[1], 'Replacement Product') !== false || strpos($item[1], 'SUPPLIER MISSED PARTS') !== false) {
          // $partsRecieved++;
          $text = 'MISSED PART: ' . trim($item[3]) . '  ';
          $this->nlin($text, $offset);
          $this->SetTextColor(0xFF);
          $this->Cell($this->GetStringWidth($text), 6.0, $text, 1, 0, 'L', true);
          $this->SetTextColor(0x51);
        } else if (strpos($item[1], 'Collect') !== false) {
          $this->SetX($this->GetX() + $this->GetStringWidth('  '));
          $text = trim('COLLECT: ' . $item[3]) . '  ';
          $this->nlin($text, $offset);
          $this->SetTextColor(0xFF);
          $this->Cell($this->GetStringWidth($text), 6.0, $text, 1, 0, 'L', true);
          $this->SetTextColor(0x51);
        } else {
          $text = trim($item[0] . ' x ' . $item[1]);
          $this->nlin($text, $offset);
          $this->Cell($this->GetStringWidth($text), 6.0, $text, 0, 0);
        }
        if ($seperator != null) {
          $this->Cell($this->GetStringWidth($seperator), 6.0, $seperator, 0, 0);
        }
        $partsRecieved += $item[7];
      }
      $this->Ln();

      $orderNotes = explode(';', $dispatchNote['order_notes']);
      foreach ($orderNotes as $orderNote) {
        $this->SetX($offset);
        $this->MultiCell(0, 6.0, $orderNote, 0, 'L', false);
      }

      $this->setXY($offset, $this->getY() + 4);
      $text = $partsRecieved . ' Parts received by : ';
      $this->Cell($this->GetStringWidth($text), 6.0, $text, 'B', 0);
      $this->SetFont('Arial', 'B', 11);
      $text = ' (Print name)' . str_repeat(' ', 52);
      $this->Cell($this->GetStringWidth($text), 6.0, $text, 'B', 0);
      $text = '(Sign)';
      $this->Cell(0.0, 6.0, $text, 'B', 1);
      $this->SetFont('Arial', '', 11);
      $this->setY($this->getY() + 4);

      $height = $this->getY() - $y0;
    }
    $y = $this->GetY();
    $this->Line(4.0, $y, $this->GetPageWidth() - 5.0, $y);
    $y+=0.5;
    $this->Line(4.0, $y, $this->GetPageWidth() - 5.0, $y);
    $y+=0.5;
    $this->Line(4.0, $y, $this->GetPageWidth() - 5.0, $y);

    $this->AddPage();

    $this->Cell(30.0, 6.0, 'Postcode', 1, 0);
    $this->Cell(120.0, 6.0, 'Reason', 1, 0);
    $this->Cell(52.0, 6.0, 'Staff', 1, 1);

    $x = $this->getX();
    $y = $this->getY();

    $boxHeight = 22.0;

    for ($i = 0; $i < 5; $i++) {
      $this->Rect($x, $y, 30.0, $boxHeight);
      $this->Rect($x + 30, $y, 120.0, $boxHeight);
      $this->Rect($x + 150, $y, 52.0, $boxHeight);
      $y += $boxHeight;
    }

    $this->SetY($y);
    $this->Ln();

    $this->SetFont('Arial', 'B', 18);
    $this->Cell(0.0, 6.0, 'Driver\'s Daily Operations Check');
    $this->Ln();
    $this->Ln();

    $this->SetFont('Arial', '', 14);

    $y = $this->GetY() + 7.0;
    //
    $text = 'Departure Time: ';
    $this->Line($this->GetStringWidth($text) + 4.0, $y, 96.0, $y);
    $this->Cell(96.0, 9.0, $text);
    //
    $text = 'Return Time: ';
    $this->Cell(64.0, 9.0, $text);
    $this->Line($this->GetStringWidth($text) + 100.0, $y, 196.0, $y);

    $this->Ln();

    $y = $this->GetY() + 7.0;
    //
    $text = 'Mileage on Departure: ';
    $this->Cell(96.0, 9.0, $text);
    $this->Line($this->GetStringWidth($text) + 4.0, $y, 96.0, $y);
    //
    $text = 'Mileage on Return: ';
    $this->Cell(64.0, 9.0, $text);
    $this->Line($this->GetStringWidth($text) + 100.0, $y, 196.0, $y);
    $this->Ln();

    $y = $this->GetY() + 7.0;
    //
    $text = 'Fuel Cost: ';
    $this->Cell(96.0, 9.0, $text);
    $this->Line($this->GetStringWidth($text) + 4.0, $y, 96.0, $y);
    //
    $text = 'Total Mileage: ';
    $this->Cell(64.0, 9.0, $text);
    $this->Line($this->GetStringWidth($text) + 100.0, $y, 196.0, $y);
    $this->Ln();

    $this->SetFont('Arial', '', 14);

    $y = $this->GetY();

    $this->Cell(96.0, 9.0, 'Tyre Pressure');
    $x = $this->GetX();
    $this->Rect($x - 32.0, $y + 4.0, 3.0, 3.0);

    $this->Cell(96.0, 9.0, 'Coolant Level');
    $x = $this->GetX();
    $this->Rect($x - 32.0, $y + 4.0, 3.0, 3.0);
    $this->Ln();
    $y = $this->GetY();

    $this->Cell(96.0, 9.0, 'Engine Oil');
    $x = $this->GetX();
    $this->Rect($x - 32.0, $y + 4.0, 3.0, 3.0);

    $this->Cell(96.0, 9.0, 'Brake Fluid');
    $x = $this->GetX();
    $this->Rect($x - 32.0, $y + 4.0, 3.0, 3.0);
    $this->Ln();
    $y = $this->GetY();

    $this->Cell(96.0, 9.0, 'Steering Wheel Fluid');
    $x = $this->GetX();
    $this->Rect($x - 32.0, $y + 4.0, 3.0, 3.0);
    $this->Ln();
    $this->Ln();

    $this->SetFont('Arial', 'B', 10);

    $y = $this->GetY();
    $this->MultiCell(0.0, 6.0, 'ADDITIONAL INFORMATION');

    $x = $this->GetX();
    $this->Rect($x, $y, 202.0, 50.0);
    $y += 50;

    $this->SetY($y);

    $this->MultiCell(0.0, 5.0, 'DRIVERS NOTE: ALL ABOVE CHECKS NEED TO BE CARRIED OUT DAILY.  USE THE BOX PROVIDED FOR ADDITIONAL INFORMATION - E.G. ENGINE SOUNDS, DAMAGE TO WINDSCREEN, BRAKES, WIPERS, LIGHTS, TYRES, AND ANYTHING ELSE YOU MAY THINK RELEVANT.');

    $this->showHeader = false;

    $this->pdfInvoice($dispatchNotes);

    $this->continued = false;

    return $this;
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  array $dispatchNotes
   * @return void
   */
  public function pdfInvoice($dispatchNotes) {
    $pdfInvoice = new PdfInvoice($this);

    foreach ($dispatchNotes as $dispatchNote) {
      if (strtolower($dispatchNote['order_type']) == 'retail' && $dispatchNote['total'] - $dispatchNote['paid'] > 0) {
        $pdfInvoice->invoice($dispatchNote);
      }
    }
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  array $dispatchNotes
   * @return void
   */
  public function pdfPickList($dispatchNotes) {
    $this->SetAutoPageBreak(true);
    $this->AddPage();

    $i = 0;

    foreach ($dispatchNotes as $dispatchNote) {
      $seperator = '';
      $items = explode(';', $dispatchNote['items']);

      $line = '';

      $this->SetFont('Arial', '', 10);
      $this->Cell(10.0, 6.0, (++$i) . ' . ', 0, 0, 'R');

      foreach ($items as $itemParts) {
        $item = explode(',', $itemParts);
        $p = strtolower($item[1]);
        if (strpos($p, 'uk_othercourier24') === false && strpos($p, 'delivery charge') === false && strpos($p, 'direct delivery') === false && strpos($p, 'collection item') === false)
        {
          if (strpos($p, 'missed parts') !== false || strpos($p, 'deliver item') !== false || strpos($p, 'replacement product') !== false)
          {
            $line = $seperator . trim($item[3]) . ' - ' . $item[1];
          } else {
            $line = $seperator . trim($item[0] . ' x ' . $item[1]);
          }
          $this->Cell($this->GetStringWidth($line), 6.0, $line, 0, 0);
          $seperator = ', ';
        }
      }
      // // barcode
      // $angle = 0;   // rotation in degrees
      // $fontSize = 10;
      // $height = 3;   // barcode height in 1D ; module size in 2D
      // $marge = 2;   // between barcode and hri in pixel
      // $width = 0.5;    // barcode height in 1D ; not use in 2D
      // $bx = $this->GetPageWidth() - 34.0;  // barcode center
      // $by = $this->getY() + ($height / 2);  // barcode center
      // $type = 'code128';
      // $black = '000000'; // color in hex
      //
      // $strBarcode = str_pad($dispatchNote['oid'], 7, '0', STR_PAD_LEFT);
      //
      // $barcode = Barcode::fpdf($this, $black, $bx, $by, $angle, $type, $strBarcode, $width, $height);
      $this->Ln();
    }
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  array $dispatchNotes
   * @return void
   */
  public function setHeader($headData) {
    $this->header = [];
    $this->header['date'] = $headData->date;
    $this->header['driver'] = $headData['driver'];
    $this->header['mate'] = $headData['mate'];
    $this->header['route'] = $headData['route'];
    $this->header['vehicle'] = $headData['vehicle'];
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  array $dispatchNotes
   * @return void
   */
  public function setPageNumber($number) {
    $this->pageNumber = $number;
  }
}
