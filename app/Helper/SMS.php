<?php

namespace App\Helper;

class SMS
{
  private $username = "u6282", $password = "vrdx7", $originator = "+447860020123";


  
// Simple SMS send function
public function sendSMS($number, $message)
{
	  if (app()->environment() !== 'testing') {
		  //$URL = 'https://www.textmarketer.biz/gateway/'."?username=$username&password=$password&option=xml";
		  $URL = "https://api.textmarketer.co.uk/gateway/?username=$this->username&password=$this->password&option=xml";
		  $URL .= "&number=$number&message=" . urlencode($message) . '&orig=' . urlencode($this->originator);
		  //$fp = fopen($URL, 'r');
		  $response         = simplexml_load_file($URL);
		  $array            = array();
		  $array['id']      = $response['id'];
		  $array['credits'] = $response->credits;
		  $array['status']  = $response['status'];
		  $array['used']    = $response->credits_used;

		  return $array;
	  }

	  \Log::info("Send SMS to {$number} with message {$message}");

	  return;
}

  public function credits()
  {
    $URL      = "https://api.textmarketer.co.uk/services/rest/credits?username=$this->username&password=$this->password";
    $response = simplexml_load_file($URL);
    return $response->credits;
  }

	
/*
 * Check if Connection to 
 * SMS Gateway is available
 * @return boolean		 
 */
  public function is_connected()
	{
		$connected = @fsockopen("www.textmarketer.co.uk", 80); 
											//website, port  (try 80 or 443)
		if ($connected){
			$is_conn = true; //action when connected
			fclose($connected);
		}else{
			$is_conn = false; //action in connection failure
		}
		
		return $is_conn;
	
	}
	
	
	
  /*
   * Format Number for SMS
   * removes extra characters from Tel ,Fax or Mob Fields
   * @param str - Numbers to Format
   * @return formatted numbers
   */
  
  public function formatNumber($str){
			
		
			$number = preg_replace('/\s+/', '', $str);
			$number = str_replace("/",",",$number);
			$number = preg_replace('/[\s\+]/', '',$number);
			
				if (substr($number, 0, 2) === '07' && strlen($number)==11){ $number = "44".substr($number, 1);	}
				if (strlen($number)==10 && substr($number, 0, 1) === '7') { $number = "44".$number; }
			
		return $number;
	
		/*
		$tel=preg_replace('/\s+/', '', $str);
		$tel=str_replace("/",",",$tel);
		
		if (substr($tel, 0, 3) === '447' && strlen($tel)==12){ return $tel; }	
		else if (substr($tel, 0, 2) === '07' && strlen($tel)==11){ return "44".substr($tel, 1);	}
		else if (strlen($tel)<11){ return 0; }
		
		else if (strlen($tel)>12 && strpos($tel,',') !== false){ 
		$as=explode(",",$tel);
		$tel="";
		$i=0;
		foreach($as as $a){
		if (substr($a, 0, 2) === '07' && strlen($a)==11){if($i>0){$tel.=",";}$tel.= "44".substr($a, 1);}
		if (substr($a, 0, 3) === '447' && strlen($a)==12){if($i>0){$tel.=",";}$tel.=$a;}
		$i++;
		}
		
		return ltrim($tel,","); 
		}
		
		else if (!ctype_alpha($tel)) {
		$as= explode("Evetel",$tel);
		$tel="";
		$i=0;
		foreach($as as $a){
		if (substr($a, 0, 2) === '07' && strlen($a)==11){if($i>0){$tel.=",";}$tel.= "44".substr($a, 1);}
		if (substr($a, 0, 3) === '447' && strlen($a)==12){if($i>0){$tel.=",";}$tel.=$a;}
		$i++;
		}
		return ltrim($tel,",");
		}
		
		else{  return 0; }
	 */
	}	
	
}
