<?php
// @author Jason Fleet (jason.fleet@googlemail.com)
//
namespace App\Helper\Traits;

trait OrderUtils {
  /**
   * @author Giomani Designs (Development Team)
   * @param  array  $dispatchNote
   * @param  int $direct
   * @return array
   */
  public function cleanLine($line) {
    $t = explode(PHP_EOL, $line);
    if (count($t) > 1) {
      $m = [];
      for ($i = 0, $j = count($t); $i < $j; $i++) {
        $l = array_shift($t);
        if (!in_array($l, $t)) {
          $m[] = $l;
        }
      }
      $line = implode(', ', $m);
    }
    return $line;
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  array  $dispatchNote
   * @param  int $direct
   * @return array
   */
  public function getDeliveryAddress($source, $direct) {
    $prefix = $direct === 1 ? 'd' : 'c';
    $antiPrefix = $direct === 1 ? 'c' : 'd';
    $ad0 = ucwords(trim($source[$prefix . '_number']));
    $ad1 = ucwords(trim($source[$prefix . '_street']));
    if (is_numeric($ad0)) {
      if ($ad0 == 0) {
        $ad0 = '';
      } else {
        if (substr_compare($ad0, $ad1, 0, strlen($ad0)) === 0) {
          $ad0 = '';
        }
      }
    }
    $ad0 = $this->cleanLine($ad0);
    $ad1 = $this->cleanLine($ad1);
    $tel = trim($source[$prefix . '_tel']);
    $tel1 = trim($source[$antiPrefix . '_tel']);
    if (strlen($tel1) > 0) {
      $tel .=  ' or ' . $tel1;
    }
    return [
      'id' => $source['c_id'],
      'name' => ucwords(trim($source[$prefix . '_name'])),
      'ad0' => ucwords(strtolower(trim($ad0 . ' ' . $ad1))),
      'ad1' => ucwords(trim($source[$prefix . '_town'])),
      'postcode' => ucwords(trim($source[$prefix . '_postcode'])),
      'telephone' => trim($tel),
    ];
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  array  $dispatchNote
   * @param  char   $type
   * @return array
   */
  public function getAddress($source, $type) {
    $prefix = $type;

    if ($source[$prefix . '_number'] === null) {
      $ad0 = '';
    } else {
      $ad0 = trim($source[$prefix . '_number']);
    }
    $ad1 = trim($source[$prefix . '_street']);

    if (is_numeric($ad0)) {
      if ($ad0 == 0) {
        $ad0 = '';
      } else {
        if (substr_compare($ad0, $ad1, 0, strlen($ad0)) === 0) {
          $ad0 = '';
        }
      }
    }

    $ad0 = $this->cleanLine($ad0);
    $ad1 = $this->cleanLine($ad1);

    $tels = [];
    foreach (['i', 'c', 'd'] as $c) {
      if (isset($source[$c . '_tel'])) {
        $tels[] = trim(ucwords($source[$c . '_tel']));
      }
      if (isset($source[$c . '_mob'])) {
        $tels[] = trim(ucwords($source[$c . '_mob']));
      }
    }

    $tel = implode('/', $tels);

    return [
      'id' => $source['c_id'],
      'name' => ucwords(strtolower(trim($source[$prefix . '_name']))),
      'ad0' => ucwords(strtolower(trim($ad0 . ' ' . $ad1))),
      'ad1' => ucwords(strtolower(trim($source[$prefix . '_town']))),
      'postcode' => trim(strtoupper($source[$prefix . '_postcode'])),
      'telephone' => $tel,
    ];
  }

}
