<?php
// @author Jason Fleet (jason.fleet@googlemail.com)
//
namespace App\Helper\Traits;

trait OutHeaders {
  /**
   * @author Giomani Designs (Development Team)
   * @param  string $title
   * @return void
   * @deprecated use csvHeaders instead
   */
  public function outCsvHeaders($title) {
    header('Pragma: public');
    header('Expires: 0');
    header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
    header('Cache-Control: private', false);
    header('Content-Type: application/octet-stream');
    header('Content-Disposition: attachment; filename="' . $title . '.csv";' );
    header('Content-Transfer-Encoding: binary');
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  string $title
   * @return array
   */
  public function csvHeaders($title) {
    return [
      'Pragma' => 'public',
      'Expires' => '0',
      'Cache-Control' => 'must-revalidate, post-check=0, pre-check=0',
      'Cache-Control' => 'private', false,
      'Content-Type' => 'application/octet-stream',
      'Content-Disposition' => 'attachment; filename="' . $title . '.csv";',
      'Content-Transfer-Encoding' => 'binary'
    ];
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  string $title
   * @return array
   */
  public function xmlHeaders() {
    return ['Content-type' => 'text/xml'];
  }
}
