<?php
// @author Giomani Designs (Development Team)
//
namespace App\Helper\Traits;

trait QueryableModel {
  /**
   * @author Giomani Designs (Development Team)
   * @param         $object  the query object
   * @param array   $columns the columns to search
   * @param string  $query the term to search for
   * @return Illuminate\Database\Query\Builder
   */
  protected static function modForQuery($object, $columns, $query) {
    $object->where(function ($qz) use ($columns, $query) {
      foreach ($columns as $column) {
        $qz->orWhere($column, 'like', '%' . trim($query) . '%');
      }
    });
    return $object;
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param         $query       the query object
   * @param string  $queryString the query to apply to the query
   * @return Illuminate\Database\Query\Builder
   */
  protected static function modForQueryString($query, $queryStrings) {
    if (isset($queryStrings['query'])) {
      $ab = '%';
      if (isset($queryStrings['ab'])) {
        switch ($queryStrings['ab']) {
          case 'end':
            $qa = '%';
            $qb = '';
            break;
          case 'begin':
            $qa = '';
            $qb = '%';
            break;
          case 'any':
            default:
            $qa = '%';
            $qb = '%';
            break;
        }
      }
      $ps = explode('|', $queryStrings['query']);
      $fs = explode('-', $ps[0]);
      $query->where(function ($q) use ($fs, $ps, $qa, $qb) {
        foreach ($fs as $f) {
          $q->orWhere($f, 'like', $qa . $ps[1] . $qb);
        }
      });
    }
    if (isset($queryStrings['filter'])) {
      $filters = explode(';', $queryStrings['filter']);
      $query->where(function ($q) use ($filters) {
        foreach ($filters as $filter) {
          $ps = explode('|', $filter);
          $q->where($ps[0], '=', $ps[1]);
        }
      });
    }
    if (isset($queryStrings['order'])) {
      $ps = explode('|', $queryStrings['order']);
      $query->orderBy($ps[0], $ps[1]);
    }
    return $query;
  }
}
