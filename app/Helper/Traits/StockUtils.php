<?php
// @author Giomani Designs (Development Team)
//
namespace App\Helper\Traits;

use App\StockCategory;

trait StockUtils {
  /**
   * @author Giomani Designs (Development Team)
   * @return Illuminate\Database\Eloquent\Collection
   */
  protected function getCategoryOptions () {
    $categories = StockCategory::orderBy('name', 'asc')->get();
    foreach ($categories as $category) {
      $categoryOptions[$category->id] = $category->name;
    }
    return $categoryOptions;
  }
}
