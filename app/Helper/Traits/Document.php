<?php
// @author Giomani Designs (Development Team)
//
namespace App\Helper\Traits;

use App\Helper\Files;
use App\Info;

trait Document {
  /**
   * @author Giomani Designs (Development Team)
   * @param  int $id
   * @return \Illuminate\Http\Response
   */
  public function showDocument($id)
  {
    try {
      $info = Info::findOrFail($id);
    } catch(ModelNotFoundException $e) {
      return parent::notFound();
    }

    $files = new Files;

    return $files->file($info->datum, $info->name);
  }
}
