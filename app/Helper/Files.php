<?php

namespace App\Helper;

use Illuminate\Http\Request;

use App\Http\Requests;

class Files
{
  const PART_ZERO_PADDING = 4;
  const FILES_PATH = '/files/';
  const UPLOAD_PATH = '/upload/';
  /**
   * @author Giomani Designs (Development Team)
   * @param  string  $src the image filename
   */
  public function file($name, $src) {
    return $this->getFile($name, $src);
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  string  $src the image filename
   */
  protected function getFile($src, $name) {
    $file = resource_path() . Files::FILES_PATH . $src;
    return response()->download($file, $name);
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  string  $dir the directory for this image
   * @param  string  $id the id for the image (likely a row id)
   */
  public static function getFileNames($dir, $id) {
    $fileNames = [];
    $filesDir = resource_path() . Files::FILES_PATH . $dir;

    if (is_dir($filesDir)) {
      $d = dir($filesDir);

      while (false !== ($entry = $d->read())) {
        $parts = explode('.', $entry);
        if (strcmp(array_pop($parts), 'png') == 0 && strcmp(substr($parts[0], 0, Files::PART_ZERO_PADDING), str_pad($id, Files::PART_ZERO_PADDING, '0', STR_PAD_LEFT)) == 0) {
          $fileNames[] = $entry;
        }
      }
      $d->close();
    }

    return $fileNames;
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param $chunk           string  Base64 chunk of data
   * @param $type            string  the type of file (decides the directory)
   * @param $fileId          string  an unique id for the file (used in the destination file name)
   * @param $chunkIndex      integer
   * @param $totalChunks     integer
   * @param $fileExtension   string  the file extension
   */
  protected function processChunk($chunk, $chunkIndex, $fileId, $totalChunks, $fileExtension, $fileName) {
    $fid = $fileId;
    $chunkDir = storage_path() .  Files::UPLOAD_PATH . $fid;

    if (!is_dir($chunkDir)) {
      @mkdir($chunkDir);
    }

    $path = $chunkDir . '/' . $fid . '-' . $totalChunks . '-' . str_pad($chunkIndex, Files::PART_ZERO_PADDING, '0', STR_PAD_LEFT) . '.part';
    file_put_contents($path, $chunk);

    $d = dir($chunkDir);
    $chunkCount = 0;
    while (false !== ($entry = $d->read())) {
      $parts = explode('.', $entry);
      if (strcmp(array_pop($parts), 'part') == 0) {
        $chunkCount++;
      }
    }
    $d->close();

    if ($chunkCount == $totalChunks) {
      $dir = resource_path() .  Files::FILES_PATH;
      $filePathname = $dir . $fileName . '-' . $fid . '.' . $fileExtension;

      $fp = fopen($filePathname, 'a');

      for ($i = 0; $i < $totalChunks; $i++) {
        $chunkName = $chunkDir . '/' . $fid . '-' . $totalChunks . '-' . str_pad($i, Files::PART_ZERO_PADDING, '0', STR_PAD_LEFT) . '.part';
        fwrite($fp, base64_decode(file_get_contents($chunkName)));
        unlink($chunkName);
      }
      fclose($fp);
      rmdir($chunkDir);

      return $filePathname;
    }
    return false;
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  \Illuminate\Http\Request  $request
   */
  public function recieve($chunk, $chunkIndex, $fileId, $fileExtension, $fileName, $totalChunks) {
    return $this->processChunk($chunk, $chunkIndex, $fileId, $totalChunks, $fileExtension, $fileName);
  }
}
