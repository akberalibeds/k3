<?php
// @author Giomani Designs
//
// ---------========== DEPRECATED ==========---------
//
namespace App\Helper;

use App\Helper\Pdfs\Pdf;
use App\Helper\Fpdf\Barcode;
use App\Helper\Traits\OrderUtils;

/**
 * @author Giomani Designs (Development Team)
 */
class PdfLabel extends Pdf
{
  use OrderUtils;
  /**
   *
   */
  protected $routeNames;
  /**
   * @author Giomani Designs (Development Team)
   */
  public function __construct() {
    parent::__construct('P', 'mm', [100, 148]);
    $this->routeNames = [];
  }
  /**
   * @author Giomani Designs (Development Team)
   * @return void
   */
  public function outPdf($dest = 'I') {
    $this->documentTitle = 'labels_' . implode('_', $this->routeNames);

    return parent::outPdf($dest);
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param Datetime $date the date to go on the label
   * @param array $labels
   */
  public function addLabels($labels) {
    // $this->routeNames[] = $dispRoute->route;
    // $strDate = $dispRoute->date;
    $pageNumber = 1;

    foreach ($labels as $label) {

      $delivery = $this->getDeliveryAddress($label, $label['direct']);

      $items = explode(';', $label['items']);
      sort($items);

      $parcelid = $label->parcelid;

      $count = count($items);
      $i = 1;

      foreach ($items as $item) {
        $this->AddPage();

        $parts = explode(',', $item);

        // box border
        $this->Rect(2.0, 4.0, 96.0, 102.0);

        // parcel id
        $this->SetFont('Arial', 'B', 44);
        $this->setXY(2.0, 4.0);
        $this->Cell(96.0, 16.0, $parcelid, 'B', 1, 'C');

        // consignment
        $this->SetFont('Arial', '', 11);
        $this->setX(2.0);
        $this->Cell(96.0, 7.0, 'Consignment: ' . $label['barcode'], 0, 1);

        // order id
        $this->setXY(2.0, $this->GetY() - 7.0);
        $this->Cell(96.0, 7.0, '[' . $label['oid'] . ']', 0, 1, 'R');

        // part
        $this->SetFont('Arial', 'B', 11);
        $this->setX(2.0);
        if ($parts[0] == 'MISSED PARTS Part 1 of 1' || $parts[0] == 'Replacement Product Part 1 of 1') {
          $text = $parts[2];
        } else {
          $text = $parts[0];
        }
        $y = $this->GetY();
        $this->MultiCell(96.0, 6.0, $text, 0, 'L');
        $y += 12.0;
        $this->SetY($y);

        $this->Line(2.0, $y, 98.0, $y);

        // address
        $y = $this->GetY();
        $this->SetFont('Arial', '', 11);
        $this->setX(2.0);
        $this->Cell(96.0, 5.0, $delivery['name'], 0, 1);
        $this->setXY(2.0, $y);
        $this->Cell(96.0, 5.0, '[' . $delivery['id'] . ']', 0, 1, 'R');

        //
        $this->setX(2.0);
        $this->MultiCell(96.0, 5.0, $delivery['ad0']);
        $this->setX(2.0);
        $this->Cell(96.0, 5.0, $delivery['ad1'], 0, 1);
        $this->setX(2.0);
        $this->Cell(96.0, 5.0, $delivery['postcode'], 0, 1);
        if ($this->GetStringWidth($delivery['ad0']) < 96) {
          $this->Cell(96.0, 5.0, ' ', 0, 1);
        }
        $this->setX(2.0);
        $y = $this->GetY();
        $this->Cell(96.0, 5.0, 'Tel: ' . $delivery['telephone'], 'B', 1);

        // Part n of m
        $partNom = $i++ . ' of ' . $count;
        $this->SetFont('Arial', 'B', 10);
        $this->setXY(2.0, $y);
        $this->Cell(96.0, 6.0, $partNom, 0, 1, 'R');
        $y += 8.0; // extra for barcode

        // barcode
        $angle = 0;   // rotation in degrees
        $fontSize = 10;
        $height = 27;   // barcode height in 1D ; module size in 2D
        $marge = 2;   // between barcode and hri in pixel
        $width = 0.5;    // barcode height in 1D ; not use in 2D
        $bx = $this->GetPageWidth() / 2;  // barcode center
        $by = $y + ($height / 2);  // barcode center
        $type = 'code128';
        $black = '000000'; // color in hex

        $strBarcode = $parts[1];

        $barcode = Barcode::fpdf($this, $black, $bx, $by, $angle, $type, $strBarcode, $width, $height);
        $y += $barcode['height'] - 6.0;

        $this->SetFont('Arial', 'B', 11);
        $this->setXY(2.0, $y + 2.0);
        $this->Cell(96.0, 14.0, $barcode['hri'], 0, 1, 'C');
        $y += 14.0;

        // route
        $this->SetFont('Arial', 'B', 30);
        $this->Cell(96.0, 8.0, $label['route_name'], 0, 1, 'C');
        $this->Cell(96.0, 12.0, $label['del_order'], 0, 1, 'C');

        $y += 26.0;
        // footer
        $this->SetFont('Arial', 'B', 20);
        $this->setY($y);
        $this->Cell(96.0, 6.0, $label['disp_date'], 0, 0, 'C');

        $this->SetFont('Arial', 'B', 28);
        $this->setY($y);
        $this->Cell(96.0, 6.0, $pageNumber++, 0, 0, 'L');

        $company = trim(substr($label['companyName'], 0, 3));
        $this->SetFont('Arial', '', 11);
        $this->setY($y);
        $this->Cell(94.0, 6.0, $company, 0, 1, 'R');

        $y = $this->GetY();

        $this->SetFont('Arial', 'B', 9);
        $this->Cell(12.0, 6.0, '{nb}', 0, 0, 'L');

        $this->SetY($y);
        $this->Cell(94.0, 6.0, 'w' . $label['warehouse'], 0, 0, 'R');
      }
    }
    return $this;
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param Datetime $date
   * @param array $labels
   */
  public function postLabels($labels, $date) {
  }
}
