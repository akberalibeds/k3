<?php
 
namespace App\Helper\EbayLaravel;


use App\Helper\EbayLaravel\getcommon\eBaySession;
use App\Helper\INIParser;
use Time;
use App\EbayAuth;








class Ebay {

	private $certid,$userToken, $devID, $appID, $certID, $serverUrl, $compatabilityLevel, $siteID, $verb,
			$gotIno=0,$pageNo=1,$resultArray=array(),$XMLBody,$session,$to=false,$from=false,$type=false,
			$existing_orders,$prepared=false,$allPages=false,$test=false;
	
	public  $orders, $response;

	
	public function __construct($compat_level=null){
		
		
		
	    //$ebayAuth = json_decode(json_encode(Config::get('ebay')));
		
	    
	    
	    $ebayAuth = EbayAuth::find(1);
	    
	    $this->userToken=$ebayAuth->token;
	    $this->devID=$ebayAuth->devID;
	    $this->appID=$ebayAuth->appID;
	    $this->certID=$ebayAuth->certID;
	    $this->serverUrl='https://api.ebay.com/ws/api.dll';
	    $this->compatabilityLevel=$ebayAuth->compat_level;
		$this->siteID = 3;
		
		//$this->existing_orders = $this->getExistingOrders();
		
	}
	
	
	
	/*
	* @func refresh
	* reset variables ready for next api call
	*/
	private function refresh(){
		
		$this->resultArray		=	array();
		     $this->pageNo		=	0;
			   $this->from		=	false;
				 $this->to		=	false;	
	
		
	}
	
	
	
	
	/*
	* 	get Results returned after API Call completes
	*	@return $this->resultArray;
	*/
	private function prepResult(){
	    
		switch($this->verb){
			
			case 'GetOrders':
			 
				$result = array();
				foreach($this->resultArray as $r){
						$orders = $r->OrderArray->Order;
						foreach($orders as $o){
							$result[]=$o;	
						}
				}
				$this->orders = $result;
				$this->response = $this->resultArray;
				break;
				
			case 'GetMyeBaySelling':
			   
				$result = array();
				foreach($this->resultArray as $r){
					$list = $r->ActiveList->ItemArray->Item;
					foreach($list as $o){
						$result[]=$o;
					}
				}
				$this->response = $result;
				break;
				
			default:
			    
			$this->response = $this->resultArray;
			break;
		}
		
		
		//$result = $this->resultArray;
		
		$this->refresh();
			
		//return $result;	
		
		
	}
	
	
	/*
	*	get all pages 
	*	default last 2 pages only;
	*	 
	*/
	public function allPages()
	{
		$this->allPages=true;
		return $this;
	}
	
	
	/*
	*	test mode 
	*	returns first page only;
	*	 
	*/
	public function test()
	{
		$this->test=true;
		return $this;
	}
	
	
	/*
	*	Start Call to ebay api
	*	@param $paging use pagination if results expected > 100
	*	@param $function used with pagination to recall original method 
	*/
	private function start(Options $options){
			
		
		//Create a new eBay session with all details pulled in from included keys.php
		$this->session = new eBaySession($this->userToken, $this->devID, $this->appID, $this->certID, $this->serverUrl, $this->compatabilityLevel, $this->siteID, $this->verb);
		
		//send the request and get response
			$responseXml = $this->session->sendHttpRequest($this->XMLBody);
			if (stristr($responseXml, 'HTTP 404') || $responseXml == '')
				{ 
					echo('<P>Error sending request'); 
					//$this->start(new Options(__FUNCTION__));
					//$this->start($options);
					
					//var_dump($responseXml);
					return;
				}
			
			//Xml string is parsed and creates a DOM Document object
			$responseDoc = new \DomDocument();
			$responseDoc->loadXML($responseXml);
			
			//get any error nodes
			$errors = $responseDoc->getElementsByTagName('Errors');
			$response = simplexml_import_dom($responseDoc);
			
			
			$pages=1;
			$entries=1;
			
			
			
			if($options->listings){
				$entries = $response->ActiveList->PaginationResult->TotalNumberOfEntries;
				$pages = $response->ActiveList->PaginationResult->TotalNumberOfPages;	
			}

			if($options->orders){
				$entries = $response->PaginationResult->TotalNumberOfEntries;
				$pages = $response->PaginationResult->TotalNumberOfPages;	
			}
			
			//if there are error nodes
			if ($errors->length > 0) {
				//echo '<P><B>eBay returned the following error(s):</B>';
				//display each error
				//Get error code, ShortMesaage and LongMessage
				$code = $errors->item(0)->getElementsByTagName('ErrorCode');
				$shortMsg = $errors->item(0)->getElementsByTagName('ShortMessage');
				$longMsg = $errors->item(0)->getElementsByTagName('LongMessage');
				
				//Display code and shortmessage
				//echo '<P>', $code->item(0)->nodeValue, ' : ', str_replace(">", "&gt;", str_replace("<", "&lt;", $shortMsg->item(0)->nodeValue));
				
				//if there is a long message (ie ErrorLevel=1), display it
			//	if (count($longMsg) > 0)
		//			echo '<BR>', str_replace(">", "&gt;", str_replace("<", "&lt;", $longMsg->item(0)->nodeValue));
					
					
						
					
			}//else { //If there are no errors, continue
				if(isset($_REQUEST['debug']))
				{  
				   header("Content-type: text/xml");
				   print_r($responseXml);
				}
				else
				 {  
					  
						$this->resultArray[]= $response;
						//var_dump($response);
						//echo Time::date()->get() . ' Fetched page ' . ($this->pageNo) . ' of ' . $pages . "\n" ;
						
						if($this->pageNo<$pages && !$this->test){ 
								
									if($options->orders && $this->pageNo==1 && !$this->allPages){
										$this->pageNo = $pages-1;
									}
									else{
										$this->pageNo++;	
									}
								
							
								$func = $options->function;
								$this->$func();
							
								return;
						}
						else{
								$this->prepResult();
								return $this;	
						}
			
				}
				
			  //} 
			
	}
	
	
	/*
	*
		itemID, date , slot, buyer
	*/
	public function send_message($data){
		
		$this->verb = 'AddMemberMessageAAQToPartner';
		$this->XMLBody ='<?xml version="1.0" encoding="utf-8" ?>';
		$this->XMLBody.='<AddMemberMessageAAQToPartnerRequest xmlns="urn:ebay:apis:eBLBaseComponents">';
		$this->XMLBody.="<ItemID>$data[item]</ItemID>";
		$this->XMLBody.="<MemberMessage>";
		$this->XMLBody.="<Subject>Your items have been dispatched</Subject>";
		$this->XMLBody.="<Body>Your items have been dispatched, and will be delivered to you on $data[date] between the hours of $data[slot].\n\nThank you for your business</Body>";
		$this->XMLBody.="<QuestionType>CustomizedSubject</QuestionType>";
		$this->XMLBody.="<RecipientID>$data[buyer]</RecipientID>";
		$this->XMLBody.="</MemberMessage>";
		$this->XMLBody.="<RequesterCredentials><eBayAuthToken>$this->userToken</eBayAuthToken></RequesterCredentials>";
		$this->XMLBody.='</AddMemberMessageAAQToPartnerRequest>';
		
		$this->start(new Options(__FUNCTION__));
	}
	
	
	/*
	 * 
	 * gets all listings on ebay
	 * 
	 */
	public function get_Listings(){
		
		$this->verb = 'GetMyeBaySelling';
		
		///Build the request Xml string
		$this->XMLBody = '<?xml version="1.0" encoding="utf-8" ?>';
		$this->XMLBody  .= '<GetMyeBaySellingRequest xmlns="urn:ebay:apis:eBLBaseComponents">';
		//$requestXmlBody .= "<StartTimeFrom>$timeFrom</StartTimeFrom><StartTimeTo>$timeTo</StartTimeTo><DetailLevel>ReturnAll</DetailLevel><IncludeVariations>True</IncludeVariations>";
		$this->XMLBody  .= "<RequesterCredentials><eBayAuthToken>$this->userToken</eBayAuthToken></RequesterCredentials>";
		$this->XMLBody  .= "<ActiveList><Include>True</Include><IncludeNotes>False</IncludeNotes>";
		$this->XMLBody  .= "<Pagination><EntriesPerPage>100</EntriesPerPage><PageNumber>$this->pageNo</PageNumber></Pagination><Sort>Title</Sort>";
		$this->XMLBody  .= '</ActiveList></GetMyeBaySellingRequest>';
		$this->start(new Options(__FUNCTION__,'listings'));
	
		return $this;
	
	}
	
	
	
   /*
	* 	Update ebay listing quantity available
	* 	@param $item_id
	*	@param $sku 
	*	@param $qty 
	*/
	
	public function updateItem(EbayUpdateItemArray $data){
		
		$this->verb = 'ReviseFixedPriceItem';
		
		$this->XMLBody = '<?xml version="1.0" encoding="utf-8" ?>';
		$this->XMLBody.= '<ReviseFixedPriceItemRequest xmlns="urn:ebay:apis:eBLBaseComponents">';
		
		//foreach ($data as $itemId => $v) {
		    $this->XMLBody.= '<Item>';
        		$this->XMLBody.= "<ItemID>".$data[0]->itemId."</ItemID>";	
        		$this->XMLBody.= "<Variations>";
        		
        		foreach($data as $d){
        	       	$this->XMLBody.= "<Variation><SKU>".htmlspecialchars($d->sku)."</SKU><Quantity>$d->qty</Quantity></Variation>";
        		}
        	       	
        		$this->XMLBody.= "</Variations>";
        		$this->XMLBody.= '</Item>';
		//}
		
		$this->XMLBody.= "<RequesterCredentials><eBayAuthToken>$this->userToken</eBayAuthToken></RequesterCredentials>";
		$this->XMLBody.= '</ReviseFixedPriceItemRequest>';	
		$this->start(new Options(__FUNCTION__));
		
		return $this;
	}
	
	
	/*
	 * 	Update ebay listing Out of stock control
	 * 	@param $item_id
	 *	@param $sku
	 *	@param $qty
	 */
	
	public function setStockControl($value = true){
	  
	    $this->verb    = 'SetUserPreferences';
	    $this->XMLBody = '<?xml version="1.0" encoding="utf-8" ?>';
	    $this->XMLBody.= '<SetUserPreferencesRequest xmlns="urn:ebay:apis:eBLBaseComponents">';
	    $this->XMLBody.= '<OutOfStockControlPreference>'.$value.'</OutOfStockControlPreference>';
	    $this->XMLBody.= "<RequesterCredentials><eBayAuthToken>$this->userToken</eBayAuthToken></RequesterCredentials>";
	    $this->XMLBody.= '</SetUserPreferencesRequest>';
	    
		$this->start(new Options(__FUNCTION__));
		
		return $this;
	}
	
	
	
	public function getStockControl(){
	    
	    $this->verb    = 'GetUserPreferences';
	    $this->XMLBody = '<?xml version="1.0" encoding="utf-8" ?>';
	    $this->XMLBody.= '<GetUserPreferencesRequest xmlns="urn:ebay:apis:eBLBaseComponents">';
	    $this->XMLBody.= '<ShowOutOfStockControlPreference>true</ShowOutOfStockControlPreference>';
	    $this->XMLBody.= "<RequesterCredentials><eBayAuthToken>$this->userToken</eBayAuthToken></RequesterCredentials>";
	    $this->XMLBody.= '</GetUserPreferencesRequest>';
	    
	    $this->start(new Options(__FUNCTION__));
	    
	    return $this;
        	
	}
	
	
	/*
	* 	Mark Order as Shipped 
	* 	@param $ebay_order_id
	*/
	public function mark_shipped($ebay_order_id,$code){
	
		$this->verb = 'CompleteSale';
		
		$this->XMLBody = 	'<?xml version="1.0" encoding="utf-8" ?>';
		$this->XMLBody.=	'<CompleteSaleRequest xmlns="urn:ebay:apis:eBLBaseComponents">';
		//$this->XMLBody.=	"<FeedbackInfo>";
		//$this->XMLBody.=	"<CommentText>Wonderful buyer, very fast payment.</CommentText><CommentType>Positive</CommentType>";
		//$this->XMLBody.=	"<TargetUser>$_REQUEST[userID]</TargetUser>";
		//$this->XMLBody.=	"</FeedbackInfo>";
		$this->XMLBody.=	"<Shipment>";
		$this->XMLBody.=	"<ShipmentTrackingDetails>";
		$this->XMLBody.=	"<ShipmentTrackingNumber>$code</ShipmentTrackingNumber>";
		$this->XMLBody.=	"<ShippingCarrierUsed>Premier Deliveries</ShippingCarrierUsed>";
		$this->XMLBody.=	"</ShipmentTrackingDetails>";
		$this->XMLBody.=	"</Shipment>";
		$this->XMLBody.=	"<OrderID>$ebay_order_id</OrderID><Shipped>True</Shipped>";
		$this->XMLBody .= 	"<RequesterCredentials><eBayAuthToken>$this->userToken</eBayAuthToken></RequesterCredentials>";
		$this->XMLBody .= 	'</CompleteSaleRequest>';
		$this->start(new Options(__FUNCTION__));
	}
	
	
	
	
	/**
	 *	get_orders between given dates : defaults to previous 48 hours
	 *
	 *	@param $from date String ('2015-07-09T00:00:00')
	 * 	@param $to date String ('2015-07-09T00:00:00')
	 */
	public function get_orders($from=false,$to=false){
			
	        
			$this->to = ($this->to) ? $this->to : (($to) ? $to : Time::date()->get("Y-m-d\TH:i:s")); 
			$this->from = ($this->from) ? $this->from : (($from) ? $from : Time::date()->days(-1)->get('Y-m-d\T00:00:00')); 
		    //  echo "Getting Orders from $this->from - $this->to \n"; 		
			$this->verb = 'GetOrders';
			 
			//Build the request Xml string
			$this->XMLBody = '<?xml version="1.0" encoding="utf-8" ?>';
			$this->XMLBody .= '<GetOrdersRequest xmlns="urn:ebay:apis:eBLBaseComponents">';
			$this->XMLBody .= '<DetailLevel>ReturnAll</DetailLevel>';
			$this->XMLBody .= "<CreateTimeFrom>$this->from</CreateTimeFrom><CreateTimeTo>$this->to</CreateTimeTo>";
			$this->XMLBody .= '<OrderRole>Seller</OrderRole>';//<OrderStatus>Active</OrderStatus>';
			$this->XMLBody .= "<RequesterCredentials><eBayAuthToken>$this->userToken</eBayAuthToken></RequesterCredentials>";
			$this->XMLBody .= "<Pagination><EntriesPerPage>100</EntriesPerPage><PageNumber>$this->pageNo</PageNumber></Pagination>";
			$this->XMLBody .= '</GetOrdersRequest>';
			
			$this->start(new Options(__FUNCTION__,'orders'));
			
			return $this;
	}
	


	private function getExistingOrders(){
			$file = __DIR__."/ebay.ini";
			return INIParser::read($file);
	}
	
	
	/*
     *   Remove Existing and non valid orders from Orders Array
  	 */
	 
	 public function removeExisting(){
		
			echo "removing existing orders \n";
			
			$this->removed = [];
			for($i=0;$i<count($this->orders);$i++)
			{
				if(in_array($this->orders[$i]->OrderID,$this->existing_orders))
				{
					$this->removed[]=$i;
				}
				else 
				{
						
				}
			}
			
			foreach($this->removed as $r)
			{
				unset($this->orders[$r]);	
			}
			
			
			$this->orders = array_values($this->orders);
			
			return $this;
	 }

	
	
	
	
	
}



/*
* Ebay API Call Options
*/
class Options{
	
	/**
	* 	@var function 
	*/
	public $function;
	
	/**
	* 	@var orders 
	*/
	public $orders = false;
	
	/**
	 * 
	 * @var listings
	 */
	public $listings = false;
	
	
	/*
	* 	
	*/
	public function __construct($function,$type=false){
		$this->function=$function;
		
		if($type){	
			$type = strtolower($type);
			switch($type){
				case 'orders':
					$this->orders=true;
					break;
				case 'listings':
					$this->listings=true;
					break;
			}
		}
	}
	
	
}





?>