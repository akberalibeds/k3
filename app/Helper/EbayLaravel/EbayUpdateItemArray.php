<?php

namespace App\Helper\EbayLaravel;



class EbayUpdateItemArray extends \ArrayObject {
    
    /**
     * 
     * @param EbayUpdateItem $item
     */
    public function __construct(EbayUpdateItem $item = null){
        parent::__construct();
        
        if($item)
            $this [$item->itemId][]= $item; 
        
    }
    
    
    /**
     * 
     * @param EbayUpdateItem $item
     */
    public function add(EbayUpdateItem $item){
        
        if(!isset($this[$item->itemId])){
            $this[$item->itemId] = new self();
            $this[$item->itemId][]=$item;
        }
        else{
            $this[$item->itemId][]=$item;
        }
        
        return $this;
    }
    
    
    public function startUpdate(){
        
        set_time_limit(0);
        
        $i=0;
        $response = [];
        
        foreach ($this as $d){
            $response[$i]=$d;
            $ebay = new Ebay();
            $response[$i++]['response'] = $ebay->updateItem($d)->response;
        }
        
        return $response;
    }
    
    
    
}