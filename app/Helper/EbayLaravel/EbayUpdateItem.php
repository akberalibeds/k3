<?php

namespace App\Helper\EbayLaravel;


class EbayUpdateItem {

    
    public $itemId;
    
    public $sku;
    
    public $qty;
    
 
    public function __construct($itemId,$sku,$qty){
        
        $this->itemId    = $itemId;
           $this->qty    = $qty;
           $this->sku    = $sku;
    
    }
    
    
    
}