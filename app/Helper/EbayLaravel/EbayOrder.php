<?php

namespace App\Helper\EbayLaravel;





use App\PaymentMethod;
use App\Settings;
use App\Customer;
use App\Stock;
use App\Order;

class EbayOrder{
    
    private $order;
    
    
    public function __construct($order){
        
       $this->order=$order;
       $this->getCustomer();
       $this->getDetails();
       $this->getItems();
       unset($this->order);
    }
    
    /**
     * @func getCustomer
     * 
     * Parse Customer details from ebay
     */
    private function getCustomer(){
        
        $customer = $this->order->ShippingAddress;
        
        $this->customer                         = (object)[];
        $this->customer->buyer_id               = $this->order->BuyerUserID;
        $this->customer->{"invoice-name"}       = $customer->Name;
        $this->customer->{"invoice-address"}    = $customer->Street1 . "\n" . $customer->Street2;
        $this->customer->{"invoice-town"}       = $customer->CityName;
        $this->customer->{"invoice-postcode"}   = $customer->PostalCode;
        $this->customer->{"invoice-tel"}        = $customer->Phone;
        $this->customer->{"invoice-email"}      = $this->order->TransactionArray->Transaction[0]->Buyer->Email;
        $this->customer->{"delivery-name"}      = $customer->Name;
        $this->customer->{"delivery-address"}   = $customer->Street1 . "\n" . $customer->Street2;
        $this->customer->{"delivery-town"}      = $customer->CityName;
        $this->customer->{"delivery-postcode"}  = $customer->PostalCode;
        $this->customer->{"delivery-tel"}       = $customer->Phone;
        $this->customer->{"delivery-email"}     = $this->order->TransactionArray->Transaction[0]->Buyer->Email;
        
        $customer = Customer::insertIfNotExists((array)$this->customer);
        unset($this->customer);
        $this->customer_id=$customer->id;
        
        
    }
    
    
    
    
    /**
     * @func getItems
     *
     * Parse item details from ebay
     */
    private function getItems(){
        
        $this->items =[];
        
        foreach ($this->order->TransactionArray->Transaction as $t){
            
            $oItem = (object)[];
            $oItem->title      = $t->Item->Title;
            $oItem->quantity   = intval($t->QuantityPurchased);
            $oItem->lineTotal  = floatval($t->TransactionPrice) * $t->QuantityPurchased;
            $oItem->sku        = $t->Variation->SKU;
            $oItem->price      = floatval($t->TransactionPrice);
            
            $split_item = explode("/", $oItem->sku);
            $i=0;
            foreach($split_item as $sku){
                if($i==0){
                    $array = ['sku' => rtrim($sku,"-"), 'price' => $oItem->price, 'description' => $oItem->title];
                    $price = $oItem->price;
                    $lineTotal = $oItem->lineTotal;
                }
                else{
                    $array = ['sku' => rtrim($sku,"-"), 'price' => 0.00, 'description' =>  rtrim($sku,"-")];
                    $price=0.00;
                    $lineTotal=0.00;
                }
                
                $new_item = Stock::getEbayItem($array);
                
                $item=[
                    'stock_id' 	=> $new_item->id,
                    'quantity' 	=> $oItem->quantity,
                    'price'	 	=> $price,
                    'lineTotal'	=> $lineTotal,
                    'staff_id'	=> 1,
                ];
                
                $this->items[] = $item;
                
                $i++;
            }
                
        }
   
    }
    
 
    
    /**
     * @func getDetails
     *
     * Parse order details from ebay
     */
    
    private function getDetails(){
        
    		  
    	
              $this->ref                = 'eBay'; 
              $this->status_id          = ($this->order->CheckoutStatus->Status == "Complete" && $this->order->OrderStatus=="Completed") ? 1 : 0;
              $this->platform_id        = 2;//ebay
              $this->platform_order_id  = (string)$this->order->OrderID;
              $this->payment_id         = PaymentMethod::getIdForMethod($this->order->CheckoutStatus->PaymentMethod); 
              $this->vat                = Settings::getVat();
              $this->total              = floatval($this->order->Total);
              $this->paid               = floatval($this->order->AmountPaid);
              $this->delivery_date      = date('Y-m-d 00:00:00',strtotime($this->order->TransactionArray->Transaction[0]->ShippingServiceSelected->ShippingPackageInfo[0]->EstimatedDeliveryTimeMax));
              $this->staff_id           = 1;//System
              $this->created_at         = date('Y-m-d H:i:s',strtotime($this->order->CreatedTime));
              
  //            $time = new Time($this->created_at);
//              $delivery_date = $time->days(10)->get('Y-m-d H:i:s');
              
              
    }
    
    
    
    
}