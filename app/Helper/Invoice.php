<?php
// 
//
namespace App\Helper;

use App\Helper\Fpdf\Fpdf;
use App\Helper\Fpdf\Barcode;

class Invoice extends Fpdf
{
  /**
   *
   */
  public function __construct()
  {
    parent::__construct('P', 'mm', 'A4');
  }


  public function getInvoicePDF($orders)
  {



	  foreach($orders as $order)
	  {

	  	//$this->SetLeftMargin();
	 	$this->AddPage();


		$y=0;

	    $this->SetFont('Arial', 'B', 24);
        $this->setXY(10.0, 5.0);
        $this->Cell(0, 18.0, 'Invoice', 'B', 1, 'R');


		//invoice No
		$this->SetFont('Arial', 'B', 10);
        $this->setXY(120.0, 30.0);
        $this->Cell(50, 8.0, "Invoice No:", 0, 1, 'R');

		$this->setXY(120.0, 35.0);
        $this->Cell(50, 8.0, "Date:", 0, 1, 'R');

		$this->setXY(120.0, 40.0);
        $this->Cell(50, 8.0, "Company:", 0, 1, 'R');

	    $this->setXY(120.0, 45.0);
        $this->Cell(50, 8.0, "Ref:", 0, 1, 'R');


	    //invoice No
		$this->SetFont('Arial', '', 10);
        $this->setXY(170.0, 30.0);
        $this->Cell(50, 8.0, $order['iNo'], 0, 1, 'L');

		$this->setXY(170.0, 35.0);
        $this->Cell(50, 8.0, $order['startDate'], 0, 1, 'L');

		$this->setXY(170.0, 40.0);
        $this->Cell(50, 8.0, $order['companyName'], 0, 1, 'L');

	    $this->setXY(170.0, 45.0);
        $this->Cell(50, 8.0, $order['ouref'], 0, 1, 'L');



		//PAID | UNPAID
		$this->SetFont('Arial', 'B', 40);
        $this->setXY(20.0, 35.0);

		$label = ($order->paid<$order->total) ? 'UNPAID' : 'PAID';
        $this->Cell(50, 8.0, $label, 0, 1, 'L');


		$this->RoundedRect(10,55,($this->GetPageWidth()/2.3),35,3);
		$this->RoundedRect(3+($this->GetPageWidth()/2.0),55,($this->GetPageWidth()/2.3),35,3);

		$this->SetFont('Arial', '', 10);
        $this->setXY(10.0, 55.0);

		$invoice_address = "  ".$order['customer']['businessName'] . "\n  " . $order['customer']['number'] . "\n  " . $order['customer']['street'] . "\n  " . $order['customer']['town'] . "\n  " . $order['customer']['postcode'] . "\n  " . $order['customer']['tel'] . " / " . $order['customer']['mob'];

        $this->MultiCell(($this->GetPageWidth()/2.3), 5, " Invoice To:\n$invoice_address", 0, 'L');

		$this->setXY(4+($this->GetPageWidth()/2.0), 55.0);
        $del_address = "  ".$order['customer']['dBusinessName'] . "\n  " . $order['customer']['dNumber'] . "\n  " . $order['customer']['dStreet'] . "\n  " . $order['customer']['dTown'] . "\n  " . $order['customer']['dPostcode'] . "\n  " . $order['customer']['tel'] . " / " . $order['customer']['mob'];

		$this->MultiCell(($this->GetPageWidth()/2.3), 5, " Deliver To:\n$del_address", 0, 'L');


		$this->SetFont('Arial', 'B', 12);
		$this->setXY(10, 100.0);
		$this->Cell(($this->GetPageWidth()-20)/100*80, 8.0, "Description", 1, 0, 'C');
		$this->Cell(($this->GetPageWidth()-20)/100*20, 8.0, "Price", 1, 0,'C');
		$this->Rect(10,108,($this->GetPageWidth()-20)/100*80,60);
		$this->Rect(10+($this->GetPageWidth()-20)/100*80,108,($this->GetPageWidth()-20)/100*20,60);

		$this->SetFont('Arial', '', 9);
		$this->setXY(10, 110.0);

		foreach($order['orderItems'] as $item){
			$info = $item->Qty . " x " . $item->itemCode . " ---- " . $item->itemDescription;

			$this->Cell(($this->GetPageWidth()-20)/100*80, 8, $info, 0,0);
			$this->Cell(($this->GetPageWidth()-20)/100*20, 8.0, number_format($item->lineTotal,2), 0, 1,'C');
		}



		$this->SetFont('Arial', '', 12);
		$this->setXY(10+($this->GetPageWidth()-20)/100*60, 168.0);
		$this->Cell(($this->GetPageWidth()-20)/100*20, 8.0, "Total", 1, 0, 'C');
		$this->Cell(($this->GetPageWidth()-20)/100*20, 8.0, number_format($order['total'],2), 1, 0,'C');

		$this->setXY(10+($this->GetPageWidth()-20)/100*60, 176.0);
		$this->Cell(($this->GetPageWidth()-20)/100*20, 8.0, "Paid", 1, 0, 'C');
		$this->Cell(($this->GetPageWidth()-20)/100*20, 8.0, number_format($order['paid'],2), 1, 0,'C');

		$this->setXY(10+($this->GetPageWidth()-20)/100*60, 184.0);
		$this->Cell(($this->GetPageWidth()-20)/100*20, 8.0, "Outstanding", 1, 0, 'C');
		$this->Cell(($this->GetPageWidth()-20)/100*20, 8.0, number_format($order['total']-$order['paid'],2), 1, 0,'C');

		$this->setXY(10+($this->GetPageWidth()-20)/100*60, 192.0);
		$this->Cell(($this->GetPageWidth()-20)/100*20, 8.0, "Payment Detail", 1, 0, 'C');
		$this->Cell(($this->GetPageWidth()-20)/100*20, 8.0, $order['paymentType'], 1, 0,'C');

		$this->SetFont('Arial', 'B', 12);
		$this->setXY(10, 210.0);
		$this->Cell(($this->GetPageWidth()-20), 8.0, "Notes", 1, 0, 'C');
		$this->Rect(10,108,($this->GetPageWidth()-20),60);
		$this->Rect(10,218,($this->GetPageWidth()-20),60);

		$this->SetFont('Arial', '', 12);
		$this->setXY(10, 220.0);

		$notes="";

		foreach($order['orderNotes'] as $note){
			if($note->driver==1){
				$notes.=$note->notes."\n";
			}
		}

		//$this->MultiCell(($this->GetPageWidth()-20),5,$notes,0)

	  }//@endforeach

    	return $this;
  }



}
