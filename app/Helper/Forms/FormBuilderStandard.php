<?php
namespace App\Helper\Forms;

use Collective\Html\FormBuilder;

/**
 * This class is overrides and extras to get normalised
 * bootstrap styled forms.
 *
 * @author Giomani Designs (Development Team)
 */
class FormBuilderStandard extends FormBuilder
{
  // /**
  //  * @param  string $value
  //  * @param  array  $options
  //  * @return \Illuminate\Support\HtmlString
  //  */
  // public function button($value = null, $options = [])
  // {
  //   if (!array_key_exists('type', $options)) {
  //     $options['type'] = 'button';
  //   }
  //
  //   return $this->toHtmlString('<button' . $this->html->attributes($options) . '>' . $value . '</button>');
  // }
  /**
   *
   */
  public function buttons() {
    $html = '<div class="col-md-6 text-right"><div class="text-right">';
    $html .= '<a class="btn btn-default" href="' . url()->previous() . '" rel="prev">back</a>';
    $html .= '&nbsp;';
    $html .= '<button class="btn btn-default" role="reset" type="reset">reset</button>';
    $html .= '&nbsp;';
    $html .= '<button class="btn btn-primary" role="save" type="submit">save</button>';
    $html .= '</div>';
    return $html;
  }
  /**
   * @see Collective\Html\FormBuilder
   * @return string
   */
  public function calendar($name, $value = null, $options = [], $colWidths = [4, 4]) {
    $options = array_merge_recursive($options, ['autocomplete' => 'off', 'class' => 'form-control']);
    $html = '<div class="col-md-' . $colWidths[0] . ' col-sm-' . $colWidths[1] . '">';
    $html .= '<div class="input-group">';
    $html .= '<div class="input-group-addon"><i class="fa fa-calendar" aria-hidden="true"></i></div>';
    $html .= parent::date($name, $value, $options);
    $html .= '</div>';
    $html .= '</div>';
    return $html;
  }
  /**
   * @see Collective\Html\FormBuilder
   * @return string
   */
  public function checkbox($name, $value = 1, $checked = null, $options = []) {
    $options = array_merge($options, ['autocomplete' => 'off', 'class' => 'form-control', 'data-off-text' => 'No', 'data-on-text' => 'Yes', 'data-size' => 'mini']);
    $html = '<div class="col-md-4 col-sm-4">';
    $html .= '<input type="hidden" name="' . $name . '" value="0">';
    $html .= parent::checkable('checkbox', $name, 1, $checked, $options);
    $html .= '</div>';
    return $html;
  }
  /**
   * @see Collective\Html\FormBuilder
   * @return string
   */
  public function email($name, $value = null, $options = []) {
    $options = array_merge($options, ['autocomplete' => 'off', 'class' => 'form-control']);
    $html = '<div class="col-md-4 col-sm-4">';
    $html .= '<input type="hidden" name="' . $name . '" value="0">';
    $html .= parent::email($name, $value, $options);
    $html .= '</div>';
    return $html;
  }
  /**
   * @see Collective\Html\FormBuilder
   * @return string
   */
  public function file($name, $options = []) {
    $options = array_merge($options, ['autocomplete' => 'off']);
    $html = '<div class="col-md-4 col-sm-4">';
    $html .= parent::file($name, $options);
    $html .= '</div>';
    return $html;
  }
  /**
   *
   */
  public function error($name) {
    $html = '<div class="col-md-4 col-sm-4">';
    $html .= '<span class="text-warning" id="error-' . $name . '">';
    $errors = $this->view->shared('errors');
    if (count($errors) > 0) {
      $html .= $errors->first($name);
    }
    $html .= '</span>';
    $html .= '</div>';
    return $html;
  }
  /**
   * @see Collective\Html\FormBuilder
   * @return string
   */
  public function label($name, $value = null, $options = [], $tip = '') {
    $options = array_merge($options, ['autocomplete' => 'off', 'class' => 'control-label']);
    $html = '<div class="col-md-2 col-sm-2">';
    $html .= parent::label($name, $value, $options);
    if ($tip != '') {
      $html .= '<sup><span data-placement="right" data-toggle="tooltip" data-original-title="' . $tip . '"><i class="fa fa-question-circle" aria-hidden="true"></i></span></sup>';
    }
    $html .= '</div>';
    return $html;
  }
  /**
   * @see Collective\Html\FormBuilder
   * @return string
   */
  public function metres($name, $value = null, $options = []) {
    $options = array_merge($options, ['autocomplete' => 'off', 'class' => 'form-control']);
    $html = '<div class="col-md-4 col-sm-4">';
    $html .= '<div class="input-group">';
    $html .= parent::text($name, $value, $options);
    $html .= '<div class="input-group-addon">m</div>';
    $html .= '</div>';
    $html .= '</div>';
    return $html;
  }
  /**
   * @see Collective\Html\FormBuilder
   * @return string
   */
  public function money($name, $value = null, $options = [], $colWidths = [4, 4]) {
    $options = array_merge($options, ['autocomplete' => 'off', 'class' => 'form-control']);
    $html = '<div class="col-md-' . $colWidths[0] . ' col-sm-' . $colWidths[1] . '">';
    $html .= '<div class="input-group">';
    $html .= '<div class="input-group-addon">£</div>';
    $html .= parent::text($name, $value, $options);
    $html .= '</div>';
    $html .= '</div>';
    return $html;
  }
  /**
   * @see Collective\Html\FormBuilder
   * @return string
   */
  public function number($name, $value = null, $options = [], $colWidths = [4, 4]) {
    $options = array_merge($options, ['autocomplete' => 'off', 'class' => 'form-control']);
    $html = '<div class="col-' . $colWidths[0] . ' col-sm-' . $colWidths[1] . '">';
    $html .= parent::number($name, $value, $options);
    $html .= '</div>';
    return $html;
  }
  /**
   * @see Collective\Html\FormBuilder
   * @return string
   */
  public function open(array $options = []) {
    $options = array_merge($options, ['class' => 'form-horizontal']);
    return parent::open($options);
  }
  /**
   * @see Collective\Html\FormBuilder
   * @return string
   */
  public function password($name, $options = []) {
      $options = array_merge($options, ['autocomplete' => 'off', 'class' => 'form-control']);
      $html = '<div class="col-md-4 col-sm-4">';
      $html .= parent::password($name, $options);
      $html .= '</div>';
      return $html;
  }
  /**
   * @see Collective\Html\FormBuilder
   * @return string
   */
  public function percent($name, $value = null, $options = []) {
    $options = array_merge($options, ['autocomplete' => 'off', 'class' => 'form-control']);
    $html = '<div class="col-md-4 col-sm-4">';
    $html .= '<div class="input-group">';
    $html .= parent::text($name, $value, $options);
    $html .= '<div class="input-group-addon">%</div>';
    $html .= '</div>';
    $html .= '</div>';
    return $html;
  }
  /**
   * @see Collective\Html\FormBuilder
   * @return string
   */
  public function select($name, $list = [], $selected = null, $options = [], $colWidths = [4, 4]) {
    $options = array_merge($options, ['autocomplete' => 'off', 'class' => 'form-control selectpicker']);
    $html = '<div class="col-md-' . $colWidths[0] . ' col-sm-' . $colWidths[1] . '">';
    $html .= parent::select($name, $list, $selected, $options);
    $html .= '</div>';
    return $html;
  }
  /**
   * @param  string $value
   * @param  array  $options
   * @return \Illuminate\Support\HtmlString
   */
  public function submit($value = null, $options = []) {
    $html = '<div class="col-md-4 col-sm-4">';
    $html .= parent::input('submit', null, $value, $options);
    $html .= '</div>';
    return $html;
  }
  /**
   * @see Collective\Html\FormBuilder
   * @return string
   */
  public function tel($name, $value = null, $options = []) {
    $options = array_merge($options, ['autocomplete' => 'off', 'class' => 'form-control']);
    $html = '<div class="col-md-4 col-sm-4">';
    $html .= parent::tel($name, $value, $options);
    $html .= '</div>';
    return $html;
  }
  /**
   * @see Collective\Html\FormBuilder
   * @return string
   */
  public function text($name, $value = null, $options = [], $tip = '') {
    $options = array_merge($options, ['autocomplete' => 'off', 'class' => 'form-control']);
    $html = '<div class="col-md-4 col-sm-4">';
    $html .= parent::text($name, $value, $options);
    $html .= '</div>';
    return $html;
  }
  /**
   * @see Collective\Html\FormBuilder
   * @return string
   */
  public function textarea($name, $value = null, $options = [], $colWidths = [4, 4]) {
    $options = array_merge($options, ['autocomplete' => 'off', 'class' => 'form-control']);
    $html = '<div class="col-md-' . $colWidths[0] . ' col-sm-' . $colWidths[1] . '">';
    $html .= parent::textarea($name, $value, $options);
    $html .= '</div>';
    return $html;
  }
  /**
   * @see Collective\Html\FormBuilder
   * @return string
   */
  public function weight($name, $value = null, $options = []) {
    $options = array_merge($options, ['autocomplete' => 'off', 'class' => 'form-control']);
    $html = '<div class="col-md-4 col-sm-4">';
    $html .= '<div class="input-group">';
    $html .= parent::text($name, $value, $options);
    $html .= '<div class="input-group-addon">Kg</div>';
    $html .= '</div>';
    $html .= '</div>';
    return $html;
  }
}
