<?php
// @author Jason Fleet (jason.fleet@googlemail.com)
//
namespace App\Helper;

use App\Exceptions\UidException;

class Uid
{
  //
  // request sources
  const REQUEST_SOURCE_BEDS   = 'BD';
  const REQUEST_SOURCE_EBAY   = 'EB';
  const REQUEST_SOURCE_OTHER  = 'TH';
  const REQUEST_SOURCE_STAFF  = 'FF';
  const REQUEST_SOURCE_IMPORT = 'MP';
  //
  // types
  const TYPE_ADDRESS        = 'AD';
  const TYPE_ADDRESS_CHANGE = 'AC';
  const TYPE_ORDER          = 'RD';
  const TYPE_CUSTOMER       = 'CU';
  const TYPE_EXCHANGE       = 'XC';
  const TYPE_SPLIT_ORDER    = 'PL';
  const TYPE_CREDIT_NOTE    = 'TE';
  /**
   *
   */
  protected function __construct() {}
  /**
   * @param  string $requestSource {  }
   * @param  array|string  $belongingToNumber the number as an array or with hyphen seperated parts.
   *                       The number could have many sources - account, customer, seller, delivery...
   * @return array of order parts
   */
  public static function addressChange($requestSource, $addressNumber) {
    $addressChangeSources = [ Uid::TYPE_ADDRESS, Uid::TYPE_ADDRESS_CHANGE ];
    $numberParts = static::verifyNumber($orderNumber);
    static::verifySource($requestSource);

    // verify that the number is an address
    if (!in_array($numberParts[0], $addressChangeSources)) {
      throw new UidException('Bad $addressNumber: [' . $addressNumber . '|' . $addressNumber . ']');
    }

    return [
      Uid::TYPE_ADDRESS_CHANGE,
      $requestSource,
      $numberParts[2],
      $numberParts[3],
      str_pad($numberParts[4] + 1, 3, '0', STR_PAD_LEFT),
    ];
  }
  /**
   * @param  string $requestSource {  }
   * @param  array|string  $belongingToNumber the number as an array or with hyphen seperated parts.
   *                       The number could have many sources - account, customer, seller, delivery...
   * @return array of order parts
   */
  public static function addressNumber($requestSource, $belongingToNumber) {
    $numberParts = static::verifyNumber($orderNumber);
    static::verifySource($requestSource);

    return [
      Uid::TYPE_ADDRESS,
      $requestSource,
      $numberParts[2],
      $numberParts[3],
      str_pad($numberParts[4] + 1, 3, '0', STR_PAD_LEFT),
    ];
  }
  /**
   * @param  string $requestSource {  }
   * @param  array|string  $orderNumber the order number as an array or with hyphen seperated parts
   * @return array of order parts
   */
  public static function creditNoteNumber($requestSource, $orderNumber) {
    $numberParts = static::verifyNumber($orderNumber);
    static::verifySource($requestSource);

    return [
      Uid::TYPE_CREDIT_NOTE,
      $requestSource,
      $numberParts[2],
      $numberParts[3],
      str_pad($numberParts[4] + 1, 3, '0', STR_PAD_LEFT),
    ];
  }
  /**
   * @param  string $requestSource {  }
   * @param  array|string  $orderNumber the order number as an array or with hyphen seperated parts
   * @return array of order parts
   */
  public static function customerNumber($requestSource) {
    static::verifySource($requestSource);

    return [
      Uid::TYPE_CUSTOMER,
      $requestSource,
      uniqid(),
      static::getRandom(),
      '000'
    ];
  }
  /**
   *
   */
  private static function getRandom() {
    return str_pad('' . dechex(mt_rand(0, 255)), 2, '0', STR_PAD_LEFT);
  }
  /**
   * @param  string $requestSource {  }
   * @return array of order parts
   */
  public static function nextOrderNumber($requestSource) {
    static::verifySource($requestSource);

    return [
      Uid::TYPE_ORDER,
      $requestSource,
      uniqid(),
      static::getRandom(),
      '000'
    ];
  }
  /**
   *
   */
  private static function verifyNumber($number) {
    if (is_array($number)) {
        $numberParts = $number;
    } else {
      $numberParts = explode('-', $number);
    }
    if (count($numberParts) !== 5) {
      throw new UidException('Bad $orderNumberParts: [' . $numberParts . '|' . $number . ']');
    }

    return $numberParts;
  }
  /**
   *
   */
  private static function verifySource($source) {
    if (!in_array($source, [Uid::REQUEST_SOURCE_BEDS, Uid::REQUEST_SOURCE_EBAY, Uid::REQUEST_SOURCE_IMPORT, Uid::REQUEST_SOURCE_OTHER, Uid::REQUEST_SOURCE_STAFF])) {
      throw new UidException('Bad $requestSource: [' . $source . ']');
    }
  }
}
