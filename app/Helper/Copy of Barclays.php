<?php

namespace App\Helper;



class Barclays {
	
	
	private $PW;
	private $PSPID;
	
	private $UserTitle;
	private $UserFirstname;
	private $UserSurname;
	private $BillHouseNumber;
	private $Ad1;
	private $Ad2;
	private $BillTown;
	private $BillCountry;
	private $Pcde;
	private $ContactTel;
	private $ShopperEmail;
	private $ShopperLocale = "en_GB";
	private $CurrencyCode="GBP";
	
	private $PaymentAmount = "0.00";
	private $OrderDataRaw = ""; 	// order description
	private $OrderID = "";
	
	//- payment design options - //
	private $TXTCOLOR;
	private $TBLTXTCOLOR;
	private $FONTTYPE;
	private $BUTTONTXTCOLOR;
	private $BGCOLOR;
	private $TBLBGCOLOR;
	private $BUTTONBGCOLOR;
	private $TITLE;
	private $LOGO;
	private $PMLISTTYPE;
	private $Addressline1n2;
	private $CustomerName;
	private $DigestivePlain="";
	private $strHashedString_plain;
	
	private $mode = 'Prod'; // deprecated
	
	private $url = 'https://payments.epdq.co.uk/ncol/Prod/orderstandard_utf8.asp';
	private $ECI = 1;
	
	//CARD DETAILS
	private $CardBrand;
	private $CardPaymentMethod;
	private $CardNo;
	private $cvc;
	
	
	
	
	
	
	
	public function __construct(){
		
		
	}
	
	
	private function digest(){
		
		//$this->Addressline1n2 = $this->BillHouseNumber . " " .$this->Ad1 . ", ". $this->Ad2;
		//$this->CustomerName = $this->UserTitle . " " . $this->UserFirstname . " " . $this->UserSurname;
		
		$this->DigestivePlain .= ($this->PaymentAmount) ? "AMOUNT=" . $this->PaymentAmount . $this->PW : '';
		$this->DigestivePlain .= ($this->BGCOLOR) ? "BGCOLOR=" . $this->BGCOLOR . $this->PW : '';
		$this->DigestivePlain .= ($this->CardBrand) ? "BRAND=" . $this->CardBrand  . $this->PW : '';
		$this->DigestivePlain .= ($this->BUTTONBGCOLOR) ? "BUTTONBGCOLOR=" . $this->BUTTONBGCOLOR . $this->PW : '';
		$this->DigestivePlain .= ($this->BUTTONTXTCOLOR) ? "BUTTONTXTCOLOR=" . $this->BUTTONTXTCOLOR . $this->PW : '';
		$this->DigestivePlain .= ($this->CardNo) ? "CARDNO=" . $this->CardNo  . $this->PW : '';
		$this->DigestivePlain .= ($this->CustomerName) ? "CN=" . $this->CustomerName  . $this->PW : '';
		$this->DigestivePlain .= ($this->OrderDataRaw) ? "COM=" . $this->OrderDataRaw  . $this->PW : '';
		$this->DigestivePlain .= ($this->CurrencyCode) ? "CURRENCY=" . $this->CurrencyCode . $this->PW : '';
		$this->DigestivePlain .= ($this->cvc) ? "CVC=" . $this->cvc . $this->PW : '';
		$this->DigestivePlain .= ($this->ECI) ? "ECI=" . $this->ECI . $this->PW : '';
		$this->DigestivePlain .= ($this->ShopperEmail) ? "EMAIL=" . $this->ShopperEmail . $this->PW : '';
		$this->DigestivePlain .= ($this->FONTTYPE) ? "FONTTYPE=" . $this->FONTTYPE . $this->PW : '';
		$this->DigestivePlain .= ($this->ShopperLocale) ? "LANGUAGE=" . $this->ShopperLocale . $this->PW : '';
		$this->DigestivePlain .= ($this->LOGO) ? "LOGO=" .$this->LOGO . $this->PW : '';
		$this->DigestivePlain .= ($this->OrderID) ? "ORDERID=" . $this->OrderID . $this->PW : '';
		$this->DigestivePlain .= ($this->Addressline1n2) ? "OWNERADDRESS=" . $this->Addressline1n2 . $this->PW : '';
		$this->DigestivePlain .= ($this->BillCountry) ? "OWNERCTY=" . $this->BillCountry . $this->PW : '';
		$this->DigestivePlain .= ($this->ContactTel) ? "OWNERTELNO=" . $this->ContactTel . $this->PW : '';
		$this->DigestivePlain .= ($this->BillTown) ? "OWNERTOWN=" . $this->BillTown . $this->PW : '';
		$this->DigestivePlain .= ($this->Pcde) ? "OWNERZIP=" . $this->Pcde . $this->PW : '';
		$this->DigestivePlain .= ($this->CardPaymentMethod) ? "PM=" . $this->CardPaymentMethod  . $this->PW : '';
		$this->DigestivePlain .= ($this->PMLISTTYPE) ? "PMLISTTYPE=". $this->PMLISTTYPE . $this->PW : '';
		$this->DigestivePlain .= ($this->PSPID) ? "PSPID=" . $this->PSPID . $this->PW : '';
		$this->DigestivePlain .= ($this->TBLBGCOLOR) ? "TBLBGCOLOR=" . $this->TBLBGCOLOR . $this->PW : '';
		$this->DigestivePlain .= ($this->TBLTXTCOLOR) ? "TBLTXTCOLOR=" . $this->TBLTXTCOLOR . $this->PW : '';
		$this->DigestivePlain .= ($this->TITLE) ? "TITLE=" . $this->TITLE . $this->PW : '';
		$this->DigestivePlain .= ($this->TXTCOLOR) ? "TXTCOLOR=" . $this->TXTCOLOR . $this->PW : '';
		
		
		
		
		
		
		
		$this->DigestivePlain .= "";
		
		//=SHA encrypt the string=//
		//$this->strHashedString_plain = strtoupper(sha1($this->DigestivePlain));
		$this->strHashedString_plain = strtoupper(openssl_digest($this->DigestivePlain, 'sha512'));
		
	
	}
	
	public function request(){
		
		$this->digest();
		
		$html = '';
		$html.='<html>';
		$html.='<body>';
		$html.='<p>Press submit to continue to payment.</p>';
		
		$html.='<form name="OrderForm" id="OrderForm" action="'.$this->url.'" method="POST">'."\n";
		
		$html.= ($this->PaymentAmount) ? '<input type="hidden" name="AMOUNT" id="AMOUNT" value="'.$this->PaymentAmount.'"/>'."\n" : '';
		
		$html.= ($this->CustomerName) ? '<input type="hidden" name="CN" value="'.$this->CustomerName.'">'."\n" : '';
		$html.= ($this->OrderDataRaw) ? '<input type="hidden" name="COM" value="'.$this->OrderDataRaw.'">'."\n" : '';
		$html.= ($this->CardNo) ? '<input type="hidden" name="CARDNO" value="'.$this->CardNo.'">'."\n" : '';
		$html.= ($this->CurrencyCode) ? '<input type="hidden" name="CURRENCY" id="CURRENCY" value="'.$this->CurrencyCode.'"/>'."\n" : '';
		$html.= ($this->cvc) ? '<input type="hidden" name="CVC" value="'.$this->cvc.'">'."\n" : '';
		$html.= ($this->ShopperEmail) ? '<input type="hidden" name="EMAIL" id="EMAIL" value="'.$this->ShopperEmail.'">'."\n" : '';
		$html.= ($this->FONTTYPE) ? '<input type="hidden" name="FONTTYPE" id="FONTTYPE" value="'.$this->FONTTYPE.'">'."\n" : '';
		$html.= ($this->ShopperLocale) ? '<input type="hidden" name="LANGUAGE" id="LANGUAGE" value="'.$this->ShopperLocale.'">'."\n" : '';
		$html.= ($this->LOGO) ? '<input type="hidden" name="LOGO" value="'.$this->LOGO.'">'."\n" : '';
		$html.= ($this->OrderID) ? '<input type="hidden" name="ORDERID" id="ORDERID" value="'.$this->OrderID.'"/>'."\n" : '';
		$html.= ($this->Address) ? '<input type="hidden" name="OWNERADDRESS" id="OWNERADDRESS" value="'.$this->Address.'">'."\n" : '';
		$html.= ($this->BillCountry) ? '<input type="hidden" name="OWNERCTY" id="OWNERCTY" value="'.$this->BillCountry.'">'."\n" : '';
		$html.= ($this->ContactTel) ? '<input type="hidden" name="OWNERTELNO" value="'.$this->ContactTel.'">'."\n" : '';
		$html.= ($this->BillTown) ? '<input type="hidden" name="OWNERTOWN" id="OWNERTOWN" value="'.$this->BillTown.'">'."\n" : '';
		$html.= ($this->Pcde) ? '<input type="hidden" name="OWNERZIP" id="OWNERZIP" value="'.$this->Pcde.'">'."\n" : '';
		$html.= ($this->PMLISTTYPE) ? '<input type="hidden" name="PMLISTTYPE" id="PMLISTTYPE" value="'.$this->PMLISTTYPE.'"/>'."\n" : '';
		$html.= ($this->PSPID) ? '<input type="hidden" name="PSPID" id="PSPID" value="'.$this->PSPID.'"/>'."\n" : '';
		$html.= ($this->BGCOLOR) ? '<input type="hidden" name="BGCOLOR" id="BGCOLOR" value="'.$this->BGCOLOR.'"/>'."\n" : '';
		$html.= ($this->BUTTONBGCOLOR) ? '<input type="hidden" name="BUTTONBGCOLOR" id="BUTTONBGCOLOR" value="'.$this->BUTTONBGCOLOR.'"/>'."\n" : '';
		$html.= ($this->BUTTONTXTCOLOR) ? '<input type="hidden" name="BUTTONTXTCOLOR" id="BUTTONTXTCOLOR" value="'.$this->BUTTONTXTCOLOR.'"/>'."\n" : '';
		$html.= ($this->TBLBGCOLOR) ? '<input type="hidden" name="TBLBGCOLOR" id="TBLBGCOLOR" value="'.$this->TBLBGCOLOR.'"/>'."\n" : '';
		$html.= ($this->TBLTXTCOLOR) ? '<input type="hidden" name="TBLTXTCOLOR" id="TBLTXTCOLOR" value="'.$this->TBLTXTCOLOR.'">'."\n" : '';
		$html.= ($this->TITLE) ? '<input type="hidden" name="TITLE" id="TITLE" value="'.$this->TITLE.'"/>'."\n" : '';
		$html.= ($this->TXTCOLOR) ? '<input type="hidden" name="TXTCOLOR" id="TXTCOLOR" value="'.$this->TXTCOLOR.'">'."\n" : '';
		
		$html.='<input type="hidden" name="SHASign" value="'.$this->strHashedString_plain.'">'."\n";
		
		$html.='<input onclick="document.OrderForm.submit(); return false;" type="button" id="tstbtn" value="Submit">'."\n";
		
		$html.='</form>';
		$html.='</body>';
		$html.='</html>';
		
		
		return $html;
	}
			
	
	public function setTestMode($val = false){
		$this->url = ($val) ? "https://mdepayments.epdq.co.uk/ncol/test/orderstandard_utf8.asp" : "https://payments.epdq.co.uk/ncol/Prod/orderstandard.asp";
	}
	
	
	public function setPassphrase($val){
		$this->PW = $val;
	}
	
	public function setPSPID($val){
		$this->PSPID = $val;
	}
	
	public function setUserTitle($val){
		$this->UserTitle = $val;
	}
	
	public function setUserFirstname($val){
		$this->UserFirstname = $val;
	}
	
	public function setUserSurname($val){
		$this->UserSurname = $val;
	}
	
	public function setBillHouseNumber($val){
		$this->BillHouseNumber = $val;
	}
	
	public function setAddressLine1($val){
		$this->Ad1 = $val;
	}
	
	public function setAddressLine2($val){
		$this->Ad2 = $val;
	}
	
	public function setAddress($val){
		$this->Address = $val;
	}
	
	public function setCustomerName($val){
		$this->CustomerName = $val;
	}
	
	public function setBillTown($val){
		$this->BillTown = $val;
	}
	
	public function setBillCountry($val){
		$this->BillCountry = $val;
	}
	
	public function setPostcode($val){
		$this->Pcde = $val;
	}
	
	public function setContactTel($val){
		$this->ContactTel = $val;
	}
	
	public function setShopperEmail($val){
		$this->ShopperEmail = $val;
	}
	
	public function setShopperLocale($val){
		$this->ShopperLocale = $val;
	}
	
	public function setCurrencyCode($val){
		$this->CurrencyCode = $val;
	}
	
	public function setPaymentAmount($val){
		$val = str_replace(".", "",$val);
		$this->PaymentAmount = str_replace(",", "",$val);
	}
	
	public function setOrderDataRaw($val){
		$this->OrderDataRaw = $val;
	}
	
	public function setOrderID($val){
		$this->OrderID = $val;
	}
	
	public function setTXTCOLOR($val){
		$this->TXTCOLOR = $val;
	}
	
	public function setTBLTXTCOLOR($val){
		$this->TBLTXTCOLOR = $val;
	}
	
	public function setFONTTYPE($val){
		$this->FONTTYPE = $val;
	}
	
	public function setBUTTONTXTCOLOR($val){
		$this->BUTTONTXTCOLOR = $val;
	}
	
	public function setBGCOLOR($val){
		$this->BGCOLOR = $val;
	}
	
	public function setTBLBGCOLOR($val){
		$this->TBLBGCOLOR = $val;
	}
	
	public function setBUTTONBGCOLOR($val){
		$this->BUTTONBGCOLOR = $val;
	}
	
	public function setTITLE($val){
		$this->TITLE = $val;
	}
	
	public function setLOGO($val){
		$this->LOGO = $val;
	}
	
	public function setPMLISTTYPE($val){
		$this->PMLISTTYPE = $val;
	}
	
	public function setFullAddressline($val){
		$this->Addressline1n2 = $val;
	}
	
	public function setCustomerName($val){
		$this->CustomerName = $val;
	}
	
	public function setCardBrand($val){
		$this->CardBrand = $val;
	}
	
	public function setCardPaymentMethod($val){
		$this->CardPaymentMethod = $val;
	}
	
	public function setCardNo($val){
		$this->CardNo = $val;
	}
	
	public function setCVC($val){
		$this->cvc = $val;
	}
	
	
	
	
}