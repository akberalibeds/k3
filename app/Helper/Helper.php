<?php

namespace App\Helper;


class Helper
{
	
	/*
	*	return json encoded array
	*/
	
	public static function JSON($array){
		$json=array();

		$array = json_decode(json_encode($array),true);

		foreach($array as $key => $val){
			$json[$key]= str_replace(array("\r","\n","\r\n"), "", $val);
		}

		return addslashes(json_encode($json));
	}
	
	
	
	
	public static function timestampFromDate($date) {
			$unixtime	= 0;
			$hr			= date('H',strtotime($date));
			$min		= date('i',strtotime($date));
			$sec		= date('s',strtotime($date));
			$mon		= date('m',strtotime($date));
			$day 		= date('d',strtotime($date));
			$yr			= date('Y',strtotime($date));
  			$dt 		= localtime($unixtime, true);

			$unixnewtime = mktime(
      		$dt['tm_hour']+$hr-2, $dt['tm_min']+$min, $dt['tm_sec']+$sec,
      		$dt['tm_mon']+$mon, $dt['tm_mday']+$day-1, $dt['tm_year']+$yr-70);

  			return $unixnewtime;
	}
	
	
	
}
