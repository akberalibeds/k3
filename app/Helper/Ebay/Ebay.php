<?php
 
namespace App\Helper\Ebay;


use App\Helper\Ebay\getcommon\eBaySession;
use App\Helper\Ebay\EbayParser;
use Time;
use App\Order;
use App\Customer;
use App\EbayStock;
use App\Helper\INIParser;
use App\EbayAuth; 
use App\Settings;
use App\OrderStatus;
use App\OrderType;
use App\PaymentType;
use App\Company;
use App\StockItem;





//include_once __DIR__."/get-common/eBaySession.php";

class Ebay {
			
	/**
	 * @var String
	 * 
	 * @property $certid
	 */
	private $certid = '';
	/**
	 * 
	 * @var $userToken  string
	 */
	private $userToken = '';
	/**
	 * 
	 * @var string $devID
	 */
	private $devID = '';
	/**
	 * 
	 * @var string $appID
	 */
	private $appID='';
	/**
	 * 
	 * @var string $certID
	 */
	private $certID = '';
	/**
	 * 
	 * @var string $serverUrl
	 */
	private $serverUrl = '';
	/**
	 * 
	 * @var integer $compatabilityLevel
	 */
	private $compatabilityLevel = 0;
	/**
	 * 
	 * @var integer $siteID
	 */
	private $siteID = 0;
	/**
	 * 
	 * @var string $verb
	 */
	private $verb = '';
	/**
	 * 
	 * @var integer $gotIno
	 */
	private $gotIno = 0;
	/**
	 * 
	 * @var integer $pageNo
	 */
	private $pageNo=1;
	/**
	 * 
	 * @var array $resultArray
	 */
	private $resultArray=array();
	/**
	 * 
	 * @var string $XMLBody
	 */
	private $XMLBody='';
	/**
	 * 
	 * @var eBaySession $session
	 */
	private $session;
	/**
	 * 
	 * @var string $to
	 * Date to get data until
	 */
	private $to=false;
	/**
	 * @var string $from
	 * 
	 * Date to got orders from
	 */
	private $from=false;
	/**
	 * 
	 * @var boolean $type
	 */
	private $type=false;
	/**
	 * 
	 * @var array() $existing_orders
	 */
	private $existing_orders;
	/**
	 * 
	 * @var boolean $prepared
	 */
	private $prepared=false;
	/**
	 * 
	 * @var boolean $allPages
	 */
	private $allPages=false;
	/**
	 * 
	 * @var boolean $test
	 */
	private $test=false;
	/**
	 * 
	 * @var boolean $errors
	 * 
	 * @desc When 'true' any errors from ebay
	 * are displayed.
	 */
	private $errors = false;
	
	/**
	 * 
	 * @var array $orders
	 * 
	 * @desc List of returned eBay Orders
	 */
	public  $orders = array();
	/**
	 * 
	 * @var array $response
	 * 
	 * @desc Response from eBay
	 */
	public  $response = array();
	
	
	public function __construct(){
		
		//include __DIR__."/getcommon/keys.php";  //include keys file for auth token and other credentials
		
		$ebayAuth = EbayAuth::find(1);
		
		$this->userToken=$ebayAuth->token; 
		$this->devID=$ebayAuth->devID;
		$this->appID=$ebayAuth->appID;
		$this->certID=$ebayAuth->certID;
		$this->serverUrl='https://api.ebay.com/ws/api.dll'; 
		$this->compatabilityLevel=$ebayAuth->compat_level;
		
		//SiteID must also be set in the Request's XML
		//SiteID = 0  (US) - UK = 3, Canada = 2, Australia = 15, ....
		//SiteID Indicates the eBay site to associate the call with
		$this->siteID = 3;
		$this->existing_orders = $this->getExistingOrders();
	
	}
	
	
	
	/**
	 * @method refresh()
	 * @desc reset variables ready for next api call
	 */
	private function refresh(){
		
		$this->resultArray		=	array();
		     $this->pageNo		=	0;
			   $this->from		=	false;
				 $this->to		=	false;	
	
		
	}
	
	
	
	
	/*
	 * 	get Results returned after API Call completes
	 *	@return $this->resultArray;
	 */
	private function prepResult(){
		
		switch($this->verb){
			
			case 'GetOrders':
				$result = array();
				foreach($this->resultArray as $r){
						$orders = $r->OrderArray->Order;
						foreach($orders as $o){
							$result[]=$o;	
						}
				}
				$this->orders = $result;
				break;
				
			case 'GetMyeBaySelling':
				$result = array();
				foreach($this->resultArray as $r){
					$list = $r->ActiveList->ItemArray->Item;
					foreach($list as $o){
						$result[]=$o;
					}
				}
				$this->response = $result;
				break;
				
			default:
			$this->response = $this->resultArray;
			break;
		}
		
		
		//$result = $this->resultArray;
		
		$this->refresh();
			
		//return $result;	
		
		
	}
	
	
	/**
	 *	@method allPages($bool = true)
	 *
	 *  @param boolean $bool - true : false
	 *	@desc by default only last 2 pages of results
	 * are returned. If set to 'true' all result pages
	 * are returned.
	 *	 
	 */
	public function allPages($bool = true)
	{
		$this->allPages=$bool;
		return $this;
	}
	
	
	/**
	 *	@method test($bool = true)
	 *
	 *  @param boolean $bool - true : false
	 *  
	 *	@desc Toggle test mode. dumps response 
	 * data when 'true' 
	 *
	 *	 
	 */
	public function test($bool = true)
	{
		$this->test=$bool;
		return $this;
	}
	
	
	/*
	 *	error mode
	 *	returns errors;
	 *
	 */
	public function error($status = true)
	{
		$this->errors=$status;
		return $this;
	}
	
	
	/*
	*	Start Call to ebay api
	*	@param $paging use pagination if results expected > 100
	*	@param $function used with pagination to recall original method 
	*/
	private function start(Options $options){
			
		
		//Create a new eBay session with all details pulled in from included keys.php
		$this->session = new eBaySession($this->userToken, $this->devID, $this->appID, $this->certID, $this->serverUrl, $this->compatabilityLevel, $this->siteID, $this->verb);
		
		//send the request and get response
			$responseXml = $this->session->sendHttpRequest($this->XMLBody);
			if (stristr($responseXml, 'HTTP 404') || $responseXml == '')
				{ 
					echo('<P>Error sending request'); 
					//$this->start(new Options(__FUNCTION__));
					//$this->start($options);
					
					//var_dump($responseXml);
					return;
				}
			
			//Xml string is parsed and creates a DOM Document object
			$responseDoc = new \DomDocument();
			$responseDoc->loadXML($responseXml);
			
			//get any error nodes
			$errors = $responseDoc->getElementsByTagName('Errors');
			$response = simplexml_import_dom($responseDoc);
			
			
			$pages=1;
			$entries=1;
			
			if($options->listings){
				$entries = $response->ActiveList->PaginationResult->TotalNumberOfEntries;
				$pages = $response->ActiveList->PaginationResult->TotalNumberOfPages;	
			}

			if($options->orders){
				$entries = $response->PaginationResult->TotalNumberOfEntries;
				$pages = $response->PaginationResult->TotalNumberOfPages;	
			}
			
			//if there are error nodes
			if ($errors->length > 0 && $this->errors) {
				echo '<P><B>eBay returned the following error(s):</B>';
				//display each error
				//Get error code, ShortMesaage and LongMessage
				$code = $errors->item(0)->getElementsByTagName('ErrorCode');
				$shortMsg = $errors->item(0)->getElementsByTagName('ShortMessage');
				$longMsg = $errors->item(0)->getElementsByTagName('LongMessage');
				
				//Display code and shortmessage
				//echo '<P>', $code->item(0)->nodeValue, ' : ', str_replace(">", "&gt;", str_replace("<", "&lt;", $shortMsg->item(0)->nodeValue));
				
				//if there is a long message (ie ErrorLevel=1), display it
				if (count($longMsg) > 0)
					echo '<BR>', str_replace(">", "&gt;", str_replace("<", "&lt;", $longMsg->item(0)->nodeValue));
					
					
						
					
			}else { //If there are no errors, continue
				if(isset($_REQUEST['debug']))
				{  
				   header("Content-type: text/xml");
				   print_r($responseXml);
				}
				else
				 {  
					
						$this->resultArray[]= $response;
						//echo Time::date()->get() . ' Fetched page ' . ($this->pageNo) . ' of ' . $pages . "\n" ;
						
						if($this->pageNo<$pages && !$this->test){ 
								
									if($options->orders && $this->pageNo==1 && !$this->allPages){
										$this->pageNo = $pages-1;
									}
									else{
										$this->pageNo++;	
									}
								
							
								$func = $options->function;
								$this->$func();
							
								return;
						}
						else{
								$this->prepResult();
								return $this;	
						}
			
				}
				
			  } 
			
	}
	

	/**
	 *	@method send_message(array $data) 
	 *	@param array( itemID, date , slot, buyer )
	 *  @return Object - use respone to get eBay response
	 */
	public function send_message($data){

		if(app()->environment() != 'testing') { 
	
			$this->verb = 'AddMemberMessageAAQToPartner';
			$this->XMLBody ='<?xml version="1.0" encoding="utf-8" ?>';
			$this->XMLBody.='<AddMemberMessageAAQToPartnerRequest xmlns="urn:ebay:apis:eBLBaseComponents">';
			$this->XMLBody.="<ItemID>$data[item]</ItemID>";
			$this->XMLBody.="<MemberMessage>";
			$this->XMLBody.="<Subject>$data[subject]</Subject>";
			$this->XMLBody.="<Body>$data[body]</Body>";
			$this->XMLBody.="<QuestionType>CustomizedSubject</QuestionType>";
			$this->XMLBody.="<RecipientID>$data[buyer]</RecipientID>";
			$this->XMLBody.="</MemberMessage>";
			$this->XMLBody.="<RequesterCredentials><eBayAuthToken>$this->userToken</eBayAuthToken></RequesterCredentials>";
			$this->XMLBody.='</AddMemberMessageAAQToPartnerRequest>';
			
			$this->start(new Options(__FUNCTION__));
		}
	}
	
	
	/**
	 * 
	 * gets all listings on ebay
	 * 
	 */
	public function get_Listings(){
		
		$this->verb = 'GetMyeBaySelling';
		
		///Build the request Xml string
		$this->XMLBody = '<?xml version="1.0" encoding="utf-8" ?>';
		$this->XMLBody  .= '<GetMyeBaySellingRequest xmlns="urn:ebay:apis:eBLBaseComponents">';
		//$requestXmlBody .= "<StartTimeFrom>$timeFrom</StartTimeFrom><StartTimeTo>$timeTo</StartTimeTo><DetailLevel>ReturnAll</DetailLevel><IncludeVariations>True</IncludeVariations>";
		$this->XMLBody  .= "<RequesterCredentials><eBayAuthToken>$this->userToken</eBayAuthToken></RequesterCredentials>";
		$this->XMLBody  .= "<ActiveList><Include>True</Include><IncludeNotes>False</IncludeNotes>";
		$this->XMLBody  .= "<Pagination><EntriesPerPage>100</EntriesPerPage><PageNumber>$this->pageNo</PageNumber></Pagination><Sort>Title</Sort>";
		$this->XMLBody  .= '</ActiveList></GetMyeBaySellingRequest>';
		$this->start(new Options(__FUNCTION__,'listings'));
	
		return $this;
	}
	
	
	
   /**
    * 	@method updateItem(String $item_id,String $sku,Integer $qty);
	* 	@desc Update ebay listing quantity available
	* 	@param String - $item_id - eBay Item Number
	*	@param String - $sku - eBay Item Variation sku
	*	@param Integer - $qty - Quantity to update
	*
	*/
	
	public function updateItem($item_id,$sku,$qty){
		
		$this->verb = 'ReviseFixedPriceItem';
		
		$this->XMLBody = '<?xml version="1.0" encoding="utf-8" ?>';
		$this->XMLBody.= '<ReviseFixedPriceItemRequest xmlns="urn:ebay:apis:eBLBaseComponents">';
		$this->XMLBody.= '<Item>';
		$this->XMLBody.= "<ItemID>$item_id</ItemID>"; //'<SKU>133-BED</SKU>';
		$this->XMLBody.= "<Variations><Variation><SKU>".htmlspecialchars($sku)."</SKU><Quantity>$qty</Quantity>";
		$this->XMLBody.= "</Variation>";
		$this->XMLBody.= "</Variations>";
		$this->XMLBody.= '</Item>';
		$this->XMLBody.= "<RequesterCredentials><eBayAuthToken>$this->userToken</eBayAuthToken></RequesterCredentials>";
		$this->XMLBody.= '</ReviseFixedPriceItemRequest>';	
		$this->start(new Options(__FUNCTION__));
	}
	
	
	/**
	 * 	Update ebay listing Out of stock control
	 * 	@param String - $item_id - Ebay Item Number 
	 */
	
	public function stockControl($item_id){
	
		$this->verb = 'ReviseFixedPriceItem';
	
		$this->XMLBody = '<?xml version="1.0" encoding="utf-8" ?>';
		$this->XMLBody.= '<ReviseFixedPriceItemRequest xmlns="urn:ebay:apis:eBLBaseComponents">';
		$this->XMLBody.= '<Item>';
		$this->XMLBody.= "<ItemID>$item_id</ItemID>";
		$this->XMLBody.= "<OutOfStockControl>True</OutOfStockControl>";
		$this->XMLBody.= '</Item>';
		$this->XMLBody.= "<RequesterCredentials><eBayAuthToken>$this->userToken</eBayAuthToken></RequesterCredentials>";
		$this->XMLBody.= '</ReviseFixedPriceItemRequest>';
		
		$this->start(new Options(__FUNCTION__));
		
		return $this;
	}
	
	
	
	/**
	 * 	Mark Order as Shipped 
	 * 	@param String  -  $ebay_order_id
	 *  @param String  -  $code  - consigment code 
	 */
	public function mark_shipped($ebay_order_id,$code){
	
		$this->verb = 'CompleteSale';
		
		$this->XMLBody = 	'<?xml version="1.0" encoding="utf-8" ?>';
		$this->XMLBody.=	'<CompleteSaleRequest xmlns="urn:ebay:apis:eBLBaseComponents">';
		//$this->XMLBody.=	"<FeedbackInfo>";
		//$this->XMLBody.=	"<CommentText>Wonderful buyer, very fast payment.</CommentText><CommentType>Positive</CommentType>";
		//$this->XMLBody.=	"<TargetUser>$_REQUEST[userID]</TargetUser>";
		//$this->XMLBody.=	"</FeedbackInfo>";
		$this->XMLBody.=	"<Shipment>";
		$this->XMLBody.=	"<ShipmentTrackingDetails>";
		$this->XMLBody.=	"<ShipmentTrackingNumber>$code</ShipmentTrackingNumber>";
		$this->XMLBody.=	"<ShippingCarrierUsed>Premier Deliveries</ShippingCarrierUsed>";
		$this->XMLBody.=	"</ShipmentTrackingDetails>";
		$this->XMLBody.=	"</Shipment>";
		$this->XMLBody.=	"<OrderID>$ebay_order_id</OrderID><Shipped>True</Shipped>";
		$this->XMLBody .= 	"<RequesterCredentials><eBayAuthToken>$this->userToken</eBayAuthToken></RequesterCredentials>";
		$this->XMLBody .= 	'</CompleteSaleRequest>';
		$this->start(new Options(__FUNCTION__));
	
	}
	
	
	
	
	/**
	 *	@desc get_orders between given dates : defaults to previous 48 hours
	 *	@param $from date String ('2015-07-09T00:00:00')
	 * 	@param $to date String ('2015-07-09T00:00:00')
	 */
	public function get_orders($from=false,$to=false){
			
			$this->to = ($this->to) ? $this->to : (($to) ? $to : Time::date()->get("Y-m-d\TH:i:s")); 
			$this->from = ($this->from) ? $this->from : (($from) ? $from : Time::date()->days(-1)->get('Y-m-d\T00:00:00')); 
					
			$this->verb = 'GetOrders';
			 
			//Build the request Xml string
			$this->XMLBody = '<?xml version="1.0" encoding="utf-8" ?>';
			$this->XMLBody .= '<GetOrdersRequest xmlns="urn:ebay:apis:eBLBaseComponents">';
			$this->XMLBody .= '<DetailLevel>ReturnAll</DetailLevel>';
			$this->XMLBody .= "<CreateTimeFrom>$this->from</CreateTimeFrom><CreateTimeTo>$this->to</CreateTimeTo>";
			$this->XMLBody .= '<OrderRole>Seller</OrderRole>';//<OrderStatus>Active</OrderStatus>';
			$this->XMLBody .= "<RequesterCredentials><eBayAuthToken>$this->userToken</eBayAuthToken></RequesterCredentials>";
			$this->XMLBody .= "<Pagination><EntriesPerPage>100</EntriesPerPage><PageNumber>$this->pageNo</PageNumber></Pagination>";
			$this->XMLBody .= '</GetOrdersRequest>';
			
			$this->start(new Options(__FUNCTION__,'orders'));
			
			return $this;
	}
	


	private function getExistingOrders(){
			$file = __DIR__."/ebay.ini";
			return INIParser::read($file);
	}
	
	
	/*
     *   Remove Existing and non valid orders from Orders Array
  	 */
	 
	 public function removeExisting(){
		
			echo "removing existing orders \n";
			
			$this->removed = [];
			for($i=0;$i<count($this->orders);$i++)
			{
				if(in_array($this->orders[$i]->OrderID,$this->existing_orders))
				{
					$this->removed[]=$i;
				}
				else 
				{
						
				}
			}
			
			foreach($this->removed as $r)
			{
				unset($this->orders[$r]);	
			}
			
			
			$this->orders = array_values($this->orders);
			
			return $this;
	 }

	

	public function formatOrders(){
			echo "formatting orders \n";
			$array =[];
			foreach($this->orders as $order){
			
				
				$parsedOrder = new EbayParser($order);
				
				if(!$parsedOrder->isValid){ echo "Invalid Order \n"; continue; }	
					
				$array[]=$parsedOrder;
			
		}//endforeach
			
		
		$this->orders = $array;
			
	}

}



/*
* Ebay API Call Options
*/
class Options{
	
	/*
	* 	@param function 
	*/
	public $function;
	
	/*
	* 	@param orders 
	*/
	public $orders = false;
	
	/*
	* 	@param listings 
	*/
	public $listings = false;
	
	
	/*
	* 	
	*/
	public function __construct($function,$type=false){
		$this->function=$function;
		
		if($type){	
			$type = strtolower($type);
			switch($type){
				case 'orders':
					$this->orders=true;
					break;
				case 'listings':
					$this->listings=true;
					break;
			}
		}
	}
	
	
}


?>