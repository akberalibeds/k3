<?php

namespace App\Helper\Ebay;

use App\Settings;
use App\Helper\Helper;
use App\OrderStatus;
use App\OrderType;
use App\PaymentType;
use App\Order;
use App\Company;
use App\StockItem;
use App\EbayStock;
use Time;

class EbayParser
{
  /*
   *    parsed Ebay Order
   */
  public $order = [];

  /*
   *    parsed Customer
   */
  public $customer = [];

  /*
   *    Indicates if order is Multiple Orders Combined
   */
  public $isMulti = false;

  /*
   *    Array of ORDERIDs if isMulti
   */
  public $orderLineItemIDs = array();

  /*
   *
   */
  public $isValid = false;

  /*
   *
   */
  public $status;

  /*
   *
   */
  public $ebayOrder;
  /*
   *
   */
  public $paymentStatus;
  /**
   * 
   * @var paymentMethod
   */
  public $paymentMethod;

  public $checkoutStatus;

  public $buyerMsg;

  public $time;
  /*
   *
   */
  public function __construct($order){
    $this->isValid = ($order->OrderStatus == 'Completed' || $order->OrderStatus == 'Active') ? true : false;
    $this->ebayOrder = $order;

    if ($this->isValid) {
      $this->get_info();
    }
  }
  /*
   *
   */
  public function get_info () {
    $order = $this->ebayOrder;
    $this->status = $order->OrderStatus;
    $this->isMulti = (count(explode('-',$order->OrderID)) == 1) ? true : false;
    $this->checkoutStatus = $order->CheckoutStatus->Status;
    $this->buyerMsg = ($order->BuyerCheckoutMessage) ? $order->BuyerCheckoutMessage : '';
    $this->time = ($order->ExternalTransaction) ? $order->ExternalTransaction->ExternalTransactionTime : '';
    $this->formatOrder();
  }
  /*
   * @func checkIsMultiOrder
   * check if new order is multipled orders combined
   * if so delete extra orders.
   */
  public function checkIsMultiOrder () {
    if ($this->isMulti) {
      $transactions = $this->ebayOrder->TransactionArray;
      if ($transactions) {
        foreach ($transactions->Transaction as $transaction) {
          $order = Order::select('id')->where('ebayID', $transaction->OrderLineItemID)->get()->first();
          if (count($order) > 0) {
            Order::cancelOrder($order->id);
          }
        }
      }
    }
  }

  public function parseItems () {
    $transactions = $this->ebayOrder->TransactionArray;
    $fullOrderTotal = 0;
    $items = [];
    if ($transactions) {
      // iterate through each transaction for the order
      foreach ($transactions->Transaction as $transaction) {
        $qty = (string)$transaction->QuantityPurchased;
        $VariationSKU = (string)$transaction->Variation->SKU;
        $price = (string)$transaction->TransactionPrice;
        $parts = explode('/', $VariationSKU);
        $stockCount = array();

        // split variation to check which item or sets etc
        if (count($parts) == 1) {
          $parts = explode('-', $VariationSKU);

          $var = [];

          if(count($parts) == 4 && $parts[0] != 'CH'){
            $stock = StockItem::itemCodeEbay($parts[0] . '-' . $parts[1] . '-' . $parts[2], $price);
            $var['itemid'] = $stock->id;
            $var['currStock'] = $stock->itemQty;
            $var['Qty'] = $qty;
            $var['itemCode'] = $parts[0] . '-' . $parts[1] . '-' . $parts[2];
            $var['price'] = $price;
            $var['lineTotal'] = $price * $qty;
            $var['costs'] = $stock->cost;
            $items[] = $var;

            $stock = StockItem::itemCodeEbay($parts[1] . '-' . $parts[3], $price);
            $var['itemid'] = $stock->id;
            $var['currStock'] = $stock->itemQty;
            $var['itemCode'] = ($parts[1] . '-' . $parts[3]);
            $var['Qty'] = $qty;
            $var['price'] = 0.00;
            $var['lineTotal']= 0.00;
            $var['costs'] = $stock->cost;
            $items[]=$var;
          } else if (count($parts) == 4 && $parts[0] == 'CH') {
            $stock = StockItem::itemCodeEbay(($parts[1] . '-' . $parts[2]), $price);
            $var['itemid'] = $stock->id;
            $var['currStock'] = $stock->itemQty;
            $var['itemCode'] = $parts[1] . '-' . $parts[2];
            $var['Qty'] = $parts[3] * $qty;
            $var['price'] = $price;
            $var['lineTotal'] = $price * $qty;
            $var['costs'] = $stock->cost;
            $items[] = $var;
          } else if (count($parts) == 6) {
            $stock = StockItem::itemCodeEbay($parts[1] . '-' . $parts[2], $price);
            $var['itemid'] = $stock->id;
            $var['currStock'] = $stock->itemQty;
            $var['itemCode'] = ($parts[1] . '-' . $parts[2]);
            $var['Qty'] = $qty;
            $var['price']= $price;
            $var['lineTotal']= $price * $qty;
            $var['costs'] = $stock->cost;
            $items[]=$var;

            $stock = StockItem::itemCodeEbay($parts[3] . '-' . $parts[4], $price);
            $var['itemid'] = $stock->id;
            $var['currStock'] = $stock->itemQty;
            $var['itemCode'] = ($parts[3] . '-' . $parts[4]);
            $var['Qty'] = $parts[5] * $qty;
            $var['price'] = 0.00;
            $var['lineTotal'] = 0.00;
            $var['costs'] = $stock->cost;
            $items[] = $var;
          } else {
            $stock = StockItem::itemCodeEbay(($VariationSKU), $price);
            $var['itemid'] = $stock->id;
            $var['currStock'] = $stock->itemQty;
            $var['itemCode'] = ($VariationSKU);
            $var['Qty'] = $qty;
            $var['price'] = $price;
            $var['lineTotal'] =  $price * $qty;
            $var['costs'] = $stock->cost;
            $items[] = $var;
          }
        } else {
          $pp = 0;
          foreach($parts as $p) {
            $var = [];
            $stock = StockItem::itemCodeEbay(($p), $price);
            $var['itemid'] = $stock->id;
            $var['currStock'] = $stock->itemQty;
            $var['itemCode'] = ($p);
            $var['Qty'] = $qty;
            $var['costs'] = $stock->cost;

            if ($pp == 0) {
              $var['price'] = $price;
              $var['lineTotal'] = $price * $qty;
            } else{
              $var['price'] = 0.00;
              $var['lineTotal'] = 0.00;
            }
            $items[] = $var;
            $pp++;
          }
        }
      }
    }
    return $items;
  }
  /*
   * Format order for DB Entry
   */
  public function formatOrder () {
    $this->order['customer'] = $this->parseCustomer();
    $this->order['order'] = $this->parseOrder();
    $this->order['order']['items'] = $this->parseItems();
    $this->prepared = true;

    return $this;
  }
  /*
   * Get Customer
   * @param  Array - Ebay Customer
   * @return Array - Parsed Customer
   *
   */
   public function parseCustomer () {
    $customer = array();
    $order = $this->ebayOrder;

    $shippingAddress = $order->ShippingAddress;
    $transaction = $order->TransactionArray->Transaction[0];

    // If status is active only the following information is available
    if ($order->OrderStatus == 'Active') {
      $customer['businessName'] = ($transaction->Buyer->Email != '') ? (string)$transaction->Buyer->Email : '';
      $customer['dBusinessName'] = ($transaction->Buyer->Email != '') ? (string)$transaction->Buyer->Email : '';
      $customer['email1'] = ($transaction->Buyer->Email != '') ? (string)$transaction->Buyer->Email : '';
      $customer['userID'] = (string)$this->ebayOrder->BuyerUserID;

      return $customer;
    }

    $customer['businessName'] = ($shippingAddress->Name) ? (string)$shippingAddress->Name : '';

    if ($shippingAddress->Street1 != '') {
      $customer['street'] = '';
      $parts =  explode(' ', (string)$shippingAddress->Street1);
      $customer['number'] = $parts[0];
      for ($i = 1; $i < count($parts); $i++) {
         $customer['street'] .= $parts[$i] . ' ';
       }
      $customer['street'] = rtrim($customer['street'] , ' ');
    }

    if ($shippingAddress->Street2 != '') {
      if($customer['street'] == '') {
        $customer['street'] = (string)$shippingAddress->Street2;
      } else if($customer['street'] != '' && $shippingAddress->Street2 != '') {
        $customer['number'] .= ' ' . $customer['street'];
        $customer['street'] = (string)$shippingAddress->Street2;}
    }

    $customer['town'] = ($shippingAddress->CityName != '') ? (string)$shippingAddress->CityName : '';
    $customer['postcode'] = ($shippingAddress->PostalCode != '') ? (string)$shippingAddress->PostalCode : '';
    $customer['tel'] = ($shippingAddress->Phone != '') ? (string)$shippingAddress->Phone : '';
    $customer['email1'] = ($transaction->Buyer->Email !=' ') ? (string)$transaction->Buyer->Email : '';
    $customer['dBusinessName'] = $customer['businessName'];
    $customer['dNumber'] = $customer['number'];
    $customer['dStreet'] = $customer['street'];
    $customer['dTown'] = $customer['town'];
    $customer['dPostcode'] = $customer['postcode'];
    $customer['userID'] = (string)$order->BuyerUserID;
    return $customer;
  }
  /*
   * Parse Order
   * @param  Array - Ebay Order
   * @return Array - Parsed Order
   *
   */
  public function parseOrder () {
    $ebayOrder = $this->ebayOrder;
    $paid = (string)$ebayOrder->AmountPaid;
    $paid = ((string)$ebayOrder->CheckoutStatus->PaymentMethod!="CreditCard") ? 0 : $paid;
    $paid = ($ebayOrder->CheckoutStatus->Status!="Complete" || $ebayOrder->CheckoutStatus->eBayPaymentStatus!="NoPaymentFailure") ? 0 : $paid;
    $time = ($ebayOrder->ExternalTransaction) ? $ebayOrder->ExternalTransaction->ExternalTransactionTime : 'now';
    $order = [
      'staffName' =>  'eBay',
      'staffid' =>  0,
      'ouref' =>  'eBay',
      'companyName' =>  'IJ Interiors',
      'startDate' =>  Time::date($time)->get('d-M-Y'),
      'startTime' =>  Time::date($time)->get('H:i:s'),
      'startStamp' =>  Time::date($time)->getStamp(),
      'ebayID' =>  (string)$ebayOrder->OrderID,
      'paid' =>  $paid,
      'paymentType' =>  'eBay Payments', //(string)$ebayOrder->CheckoutStatus->PaymentMethod,
      'orderStatus' => 'Allocated',
      'total' =>  (string)$ebayOrder->Total,
      'ebayStatus' =>  (string)$ebayOrder->OrderStatus,
      'ebay' =>   'ebay',
      'id_status' => OrderStatus::id_status('Allocated'),
      'id_order_type' => OrderType::id_type('retail'),
      'id_company' => Company::id_company('IJ Interiors'),
      'last_trans_id' => (string)$ebayOrder->ExternalTransaction->ExternalTransactionID,
      'time_stamp' => Time::date($time)->get('Y-m-d H:i:s'),
      'id_payment_type' => PaymentType::id_type('eBay Payments'), //(string)$ebayOrder->CheckoutStatus->PaymentMethod
      'sales_record' => (string)$ebayOrder->ShippingDetails->SellingManagerSalesRecordNumber,
    ];
    return $order;
  }
}

/*


SimpleXMLElement Object
(
    [OrderID] => 111791840971-1563884971001
    [OrderStatus] => Completed
    [AdjustmentAmount] => 0.0
    [AmountPaid] => 204.0
    [AmountSaved] => 0.0
    [CheckoutStatus] => SimpleXMLElement Object
        (
            [eBayPaymentStatus] => NoPaymentFailure
            [LastModifiedTime] => 2016-09-14T12:23:54.000Z
            [PaymentMethod] => PayPal
            [Status] => Complete
            [IntegratedMerchantCreditCardEnabled] => false
        )

    [ShippingDetails] => SimpleXMLElement Object
        (
            [SalesTax] => SimpleXMLElement Object
                (
                    [SalesTaxPercent] => 0.0
                    [SalesTaxState] => SimpleXMLElement Object
                        (
                        )

                    [ShippingIncludedInTax] => false
                    [SalesTaxAmount] => 0.0
                )

            [ShippingServiceOptions] => Array
                (
                    [0] => SimpleXMLElement Object
                        (
                            [ShippingService] => UK_OtherCourier48
                            [ShippingServiceCost] => 0.0
                            [ShippingServicePriority] => 1
                            [ExpeditedService] => false
                            [ShippingTimeMin] => 1
                            [ShippingTimeMax] => 2
                        )

                    [1] => SimpleXMLElement Object
                        (
                            [ShippingService] => UK_OtherCourier24
                            [ShippingServiceCost] => 99.95
                            [ShippingServicePriority] => 2
                            [ExpeditedService] => true
                            [ShippingTimeMin] => 1
                            [ShippingTimeMax] => 1
                        )

                )

            [SellingManagerSalesRecordNumber] => 295419
            [GetItFast] => false
        )

    [CreatedTime] => 2016-09-14T12:23:02.000Z
    [PaymentMethods] => PayPal
    [SellerEmail] => furnitureitaliastore@yahoo.com
    [ShippingAddress] => SimpleXMLElement Object
        (
            [Name] => Ozgun Siuar
            [Street1] => 8 Plough Way
            [Street2] => SimpleXMLElement Object
                (
                )

            [CityName] => London
            [StateOrProvince] => Rotherhithe
            [Country] => GB
            [CountryName] => United Kingdom
            [Phone] => 07590 036484
            [PostalCode] => se16 2eu
            [AddressID] => 4224696263011
            [AddressOwner] => eBay
            [ExternalAddressID] => SimpleXMLElement Object
                (
                )

        )

    [ShippingServiceSelected] => SimpleXMLElement Object
        (
            [ShippingService] => UK_OtherCourier48
            [ShippingServiceCost] => 0.0
        )

    [Subtotal] => 204.0
    [Total] => 204.0
    [ExternalTransaction] => SimpleXMLElement Object
        (
            [ExternalTransactionID] => 36T214607M855544B
            [ExternalTransactionTime] => 2016-09-14T12:23:01.000Z
            [FeeOrCreditAmount] => 3.06
            [PaymentOrRefundAmount] => 204.0
        )

    [TransactionArray] => SimpleXMLElement Object
        (
            [Transaction] => SimpleXMLElement Object
                (
                    [Buyer] => SimpleXMLElement Object
                        (
                            [Email] => siuarozgun@hotmail.com
                            [UserFirstName] => SimpleXMLElement Object
                                (
                                )

                            [UserLastName] => SimpleXMLElement Object
                                (
                                )

                        )

                    [ShippingDetails] => SimpleXMLElement Object
                        (
                            [SellingManagerSalesRecordNumber] => 295419
                        )

                    [CreatedDate] => 2016-09-14T12:23:02.000Z
                    [Item] => SimpleXMLElement Object
                        (
                            [ItemID] => 111791840971
                            [Site] => UK
                            [Title] => LIBRETTO 4FT6 DOUBLE & 5FT KING SIZE MODERN LEATHER BED + MEMORY FOAM MATTRESS[BLACK,5FT Kingsize,Memory Foam Mattress]
                            [ConditionID] => 1000
                            [ConditionDisplayName] => New
                        )

                    [QuantityPurchased] => 1
                    [Status] => SimpleXMLElement Object
                        (
                            [PaymentHoldStatus] => None
                        )

                    [TransactionID] => 1563884971001
                    [TransactionPrice] => 204.0
                    [TransactionSiteID] => UK
                    [Platform] => eBay
                    [Variation] => SimpleXMLElement Object
                        (
                            [SKU] => 115-5FT-BLACK-FOAM
                            [VariationSpecifics] => SimpleXMLElement Object
                                (
                                    [NameValueList] => Array
                                        (
                                            [0] => SimpleXMLElement Object
                                                (
                                                    [Name] => Main Colour
                                                    [Value] => BLACK
                                                )

                                            [1] => SimpleXMLElement Object
                                                (
                                                    [Name] => Size
                                                    [Value] => 5FT Kingsize
                                                )

                                            [2] => SimpleXMLElement Object
                                                (
                                                    [Name] => Mattress
                                                    [Value] => Memory Foam Mattress
                                                )

                                        )

                                )

                            [VariationTitle] => LIBRETTO 4FT6 DOUBLE & 5FT KING SIZE MODERN LEATHER BED + MEMORY FOAM MATTRESS[BLACK,5FT Kingsize,Memory Foam Mattress]
                            [VariationViewItemURL] => http://cgi.ebay.co.uk/ws/eBayISAPI.dll?ViewItem&item=111791840971&vti=Main+Colour%09BLACK%0ASize%095FT+Kingsize%0AMattress%09Memory+Foam+Mattress
                        )

                    [Taxes] => SimpleXMLElement Object
                        (
                            [TotalTaxAmount] => 0.0
                            [TaxDetails] => Array
                                (
                                    [0] => SimpleXMLElement Object
                                        (
                                            [Imposition] => SalesTax
                                            [TaxDescription] => SalesTax
                                            [TaxAmount] => 0.0
                                            [TaxOnSubtotalAmount] => 0.0
                                            [TaxOnShippingAmount] => 0.0
                                            [TaxOnHandlingAmount] => 0.0
                                        )

                                    [1] => SimpleXMLElement Object
                                        (
                                            [Imposition] => WasteRecyclingFee
                                            [TaxDescription] => ElectronicWasteRecyclingFee
                                            [TaxAmount] => 0.0
                                        )

                                )

                        )

                    [OrderLineItemID] => 111791840971-1563884971001
                )

        )

    [BuyerUserID] => chopycob
    [PaidTime] => 2016-09-14T12:23:02.000Z
    [IntegratedMerchantCreditCardEnabled] => false
    [EIASToken] => nY+sHZ2PrBmdj6wVnY+sEZ2PrA2dj6wNmYGkAJSBog6dj6x9nY+seQ==
    [MonetaryDetails] => SimpleXMLElement Object
        (
            [Payments] => SimpleXMLElement Object
                (
                    [Payment] => SimpleXMLElement Object
                        (
                            [PaymentStatus] => Succeeded
                            [Payer] => chopycob
                            [Payee] => ijinteriors
                            [PaymentTime] => 2016-09-14T12:23:01.000Z
                            [PaymentAmount] => 204.0
                            [ReferenceID] => 36T214607M855544B
                            [FeeOrCreditAmount] => 3.06
                        )

                )

        )

)

*/
