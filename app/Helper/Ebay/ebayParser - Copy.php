<?php

namespace App\Helper\Ebay;

use App\Settings;
use App\Helper\Helper;
use App\OrderStatus;
use App\OrderType;
use App\PaymentType;
use App\Order;
use App\Company;
use App\StockItem;
use App\EbayStock;

class EbayParser
{

	
	
	/*
	*	Indicates if order is Multiple Orders Combined
	*/
	public $isMulti=false;
	
	/*
	*	Array of ORDERIDs if isMulti 
	*/
	public $orderLineItemIDs = array();
	
	/*
	*	
	*/
	public $isValid =false;
	
	/*
	*
	*/
	public $status;
	
	/*
	*	
	*/
	public $ebayOrder;
	/*
	 *
	 */
	
	
	
	public $paymentStatus;
	
	public $paymentMethod;
	
	public $checkoutStatus;
	
	public $buyerMsg;
	
	public $time;
	
	

	/*
	*	
	*/
	public function __construct($order){
		
				
				
				
				$this->isValid		=	($order->OrderStatus == 'Completed' || $order->OrderStatus == 'Active') ? true : false;
				$this->ebayOrder 	= 	$order;		
					
				if($this->isValid){
					
						$this->get_info();	
					
				}
			
	}
	
	
	
	
	public function get_info(){
		
		$order = $this->ebayOrder;
		
		$this->status			=	$order->OrderStatus;
		$this->isMulti 			=	(count(explode("-",$order->OrderID))==1) ? true : false; 
		$this->checkoutStatus	=	$order->CheckoutStatus->Status;
		$this->buyerMsg 			=	($order->BuyerCheckoutMessage) ? $order->BuyerCheckoutMessage : '';
		$this->time				=	($order->ExternalTransaction) ? $order->ExternalTransaction->ExternalTransactionTime : '';	
		
		
	}
	
	
	
	
	/*
	 * @func parseOrder
	 * return array;
	 */	
	public function parseOrder(){
		
		$ebayOrder = $this->ebayOrder;
		
		$order = array();
		
		$order['ebayID'] 			= 	$ebayOrder->OrderID;
		$order['paid']  		 		= 	$ebayOrder->AmountPaid;
		$order['paymentType'] 		= 	$ebayOrder->CheckoutStatus->PaymentMethod;
		$order['paid'] 				= 	($order['paymentType']!="PayPal") ? 0 : $order['paid'];
		$order['paid'] 				= 	($this->checkoutStatus!="Complete" || $ebayOrder->CheckoutStatus->eBayPaymentStatus!="NoPaymentFailure") ? 0 : $order['paid'];	
		$order['staffName']			=	'eBay';
		$order['staffid']			=	0;
		$order['startDate']			=	date('d-M-Y',strtotime($this->time));
		$order['startTime']			=	date('H:i:s',strtotime($this->time));
		$order['orderStatus']		=	'Allocated';
		$order['lastMaintainID']		=	0;
		$order['orderType']			=	'retail';
		$order['direct']				=	0;
		$order['cid']				=	0;
		$order['delOrder']			=	0;
		$order['vatAmount']			=	Settings::getVat();
		$order['total']				=	$ebayOrder->Total;
		$order['dispatchType']		=	'Delivery';
		$order['carrier']			=	'own';
		$order['companyName']		=	'IJ Interiors';
		$order['ouref']				=	'eBay';
		$order['time_stamp']			=	$order['startDate']." ".$order['startTime'];
		$order['ebayStatus']			=	$ebayOrder->OrderStatus;
		$order['deliverBy']			=	0;
		$order['ebay']				=	'ebay';
		$order['startStamp']			=	Helper::timestampFromDate($order['time_stamp']);
		$order['id_status']			=	OrderStatus::id_status('Allocated');
		$order['id_order_type']		= 	OrderType::id_type('retail');
		$order['id_payment_type']	= 	PaymentType::id_type($order['paymentType']);
		$order['id_company']			= 	Company::id_company('IJ Interiors') ;

		return $order;
		
		
		
		
	}
	
	
	
	/*
	 * @func parseCustomer
	 * return array;
	 */	
	public function parseCustomer(){
		
		$customer = array();
		
		$shippingAddress = $this->ebayOrder->ShippingAddress;
		$transaction = $this->ebayOrder->TransactionArray->Transaction[0];
		
		
		// If status is active only the following information is available
		if($this->ebayOrder->OrderStatus == 'Active'){
			
			$customer['businessName'] 	= 	($transaction->Buyer->Email!="") ? $transaction->Buyer->Email : '';
			$customer['dBusinessName']  	= 	($transaction->Buyer->Email!="") ? $transaction->Buyer->Email : '';
			$customer['email1']			=	($transaction->Buyer->Email!="") ? $transaction->Buyer->Email : '';
			$customer['userID']			=	$this->ebayOrder->BuyerUserID;
				
			return $customer;	
		}
		
		
		
		
		
		$customer['businessName'] 	= ($shippingAddress->Name) ? $shippingAddress->Name : '';
		
		if ($shippingAddress->Street1 != "") {
                    $customer['street']="";
					$parts =  explode(" ",$shippingAddress->Street1);
					$customer['number']= $parts[0];
					for($i=1;$i<count($parts);$i++){ $customer['street'].=$parts[$i]." ";}
					$customer['street'] = rtrim($customer['street'] , " ");
        }
		
		if ($shippingAddress->Street2 != "") {
				if($customer['street']==""){ $customer['street']=$shippingAddress->Street2; }
				else if($customer['street']!="" && $shippingAddress->Street2!=""){ $customer['number'] .=" ".$customer['street']; $customer['street']=$shippingAddress->Street2;}
		}
		
		$customer['town'] 			= ($shippingAddress->CityName != "") ? $shippingAddress->CityName : '';
        $customer['postcode']   		= ($shippingAddress->PostalCode != "") ? $shippingAddress->PostalCode : '';
        $customer['tel']				= ($shippingAddress->Phone != "") ? $shippingAddress->Phone : '';  
		$customer['email1'] 			= ($transaction->Buyer->Email!="") ? $transaction->Buyer->Email : ''; 	
		$customer['dBusinessName'] 	= $customer['businessName'];
		$customer['dNumber'] 		= $customer['number'];
		$customer['dStreet'] 		= $customer['street'];
		$customer['dTown'] 			= $customer['town'];
		$customer['dPostcode'] 		= $customer['postcode'];
		$customer['userID'] 			= $this->ebayOrder->BuyerUserID;
		
		
		return $customer;
		
	}
	
	
	
	
	/*
	 * @func checkIsMultiOrder
	 * check if new order is multipled orders combined
	 * if so delete extra orders.
	 */
	public function checkIsMultiOrder(){
		if($this->isMulti){
			
			$transactions = $this->ebayOrder->TransactionArray;
                if ($transactions) {
					foreach ($transactions->Transaction as $transaction) {
					
							$order = Order::select('id')->where('ebayID',$transaction->OrderLineItemID)->get()->first();
							
							if(count($order)>0){
								Order::cancelOrder($order->id);		
							}
					}
				}
		
		}
			
	}
	
	
	
	public function parseItems(){
		
		
		$transactions = $this->ebayOrder->TransactionArray;
				$fullOrderTotal=0;
				
				$itemCode = [];
                if ($transactions) {
                    
                    // iterate through each transaction for the order
                    foreach ($transactions->Transaction as $transaction) {
						
						
						$qty= $transaction->QuantityPurchased;
                        $VariationSKU = $transaction->Variation->SKU;
                       
					    $chairQty = 0;
						$DS = 0;
						$CH= 0;
						
						
						$parts = explode("/",$VariationSKU);
						$itemCodes 	= array();
						$stockCount = array();									
						
						// split variation to check which item or sets etc
						if(count($parts)==1){
							$parts = explode("-",$VariationSKU);
						
							if(count($parts)==4 && $parts[0]!="CH"){$itemCodes [] = "$parts[0]-$parts[1]-$parts[2]"; $itemCodes [] = "$parts[1]-$parts[3]";}
							else if(count($parts)==4 && $parts[0]=="CH"){$itemCodes [] = "$parts[1]-$parts[2]"; $chairQty=$parts[3]*$qty; $CH=1;}
							else if(count($parts)==6){ $itemCodes [] = "$parts[1]-$parts[2]"; $itemCodes [] = "$parts[3]-$parts[4]"; $chairQty=$parts[5]*$qty; $DS=1; }
							else{ $itemCodes [] = $VariationSKU; }
						
						}
						
						else{
							foreach($parts  as $p){
								$itemCodes [] = $p;
							}
						}
						
						
						
					} //@endforeach
					
				}
				
		
		return $itemCodes; 
			
	}
	
	
	
	

	
}




	


?>


SimpleXMLElement Object
(
    [OrderID] => 111791840971-1563884971001
    [OrderStatus] => Completed
    [AdjustmentAmount] => 0.0
    [AmountPaid] => 204.0
    [AmountSaved] => 0.0
    [CheckoutStatus] => SimpleXMLElement Object
        (
            [eBayPaymentStatus] => NoPaymentFailure
            [LastModifiedTime] => 2016-09-14T12:23:54.000Z
            [PaymentMethod] => PayPal
            [Status] => Complete
            [IntegratedMerchantCreditCardEnabled] => false
        )

    [ShippingDetails] => SimpleXMLElement Object
        (
            [SalesTax] => SimpleXMLElement Object
                (
                    [SalesTaxPercent] => 0.0
                    [SalesTaxState] => SimpleXMLElement Object
                        (
                        )

                    [ShippingIncludedInTax] => false
                    [SalesTaxAmount] => 0.0
                )

            [ShippingServiceOptions] => Array
                (
                    [0] => SimpleXMLElement Object
                        (
                            [ShippingService] => UK_OtherCourier48
                            [ShippingServiceCost] => 0.0
                            [ShippingServicePriority] => 1
                            [ExpeditedService] => false
                            [ShippingTimeMin] => 1
                            [ShippingTimeMax] => 2
                        )

                    [1] => SimpleXMLElement Object
                        (
                            [ShippingService] => UK_OtherCourier24
                            [ShippingServiceCost] => 99.95
                            [ShippingServicePriority] => 2
                            [ExpeditedService] => true
                            [ShippingTimeMin] => 1
                            [ShippingTimeMax] => 1
                        )

                )

            [SellingManagerSalesRecordNumber] => 295419
            [GetItFast] => false
        )

    [CreatedTime] => 2016-09-14T12:23:02.000Z
    [PaymentMethods] => PayPal
    [SellerEmail] => furnitureitaliastore@yahoo.com
    [ShippingAddress] => SimpleXMLElement Object
        (
            [Name] => Ozgun Siuar
            [Street1] => 8 Plough Way
            [Street2] => SimpleXMLElement Object
                (
                )

            [CityName] => London
            [StateOrProvince] => Rotherhithe
            [Country] => GB
            [CountryName] => United Kingdom
            [Phone] => 07590 036484
            [PostalCode] => se16 2eu
            [AddressID] => 4224696263011
            [AddressOwner] => eBay
            [ExternalAddressID] => SimpleXMLElement Object
                (
                )

        )

    [ShippingServiceSelected] => SimpleXMLElement Object
        (
            [ShippingService] => UK_OtherCourier48
            [ShippingServiceCost] => 0.0
        )

    [Subtotal] => 204.0
    [Total] => 204.0
    [ExternalTransaction] => SimpleXMLElement Object
        (
            [ExternalTransactionID] => 36T214607M855544B
            [ExternalTransactionTime] => 2016-09-14T12:23:01.000Z
            [FeeOrCreditAmount] => 3.06
            [PaymentOrRefundAmount] => 204.0
        )

    [TransactionArray] => SimpleXMLElement Object
        (
            [Transaction] => SimpleXMLElement Object
                (
                    [Buyer] => SimpleXMLElement Object
                        (
                            [Email] => siuarozgun@hotmail.com
                            [UserFirstName] => SimpleXMLElement Object
                                (
                                )

                            [UserLastName] => SimpleXMLElement Object
                                (
                                )

                        )

                    [ShippingDetails] => SimpleXMLElement Object
                        (
                            [SellingManagerSalesRecordNumber] => 295419
                        )

                    [CreatedDate] => 2016-09-14T12:23:02.000Z
                    [Item] => SimpleXMLElement Object
                        (
                            [ItemID] => 111791840971
                            [Site] => UK
                            [Title] => LIBRETTO 4FT6 DOUBLE & 5FT KING SIZE MODERN LEATHER BED + MEMORY FOAM MATTRESS[BLACK,5FT Kingsize,Memory Foam Mattress]
                            [ConditionID] => 1000
                            [ConditionDisplayName] => New
                        )

                    [QuantityPurchased] => 1
                    [Status] => SimpleXMLElement Object
                        (
                            [PaymentHoldStatus] => None
                        )

                    [TransactionID] => 1563884971001
                    [TransactionPrice] => 204.0
                    [TransactionSiteID] => UK
                    [Platform] => eBay
                    [Variation] => SimpleXMLElement Object
                        (
                            [SKU] => 115-5FT-BLACK-FOAM
                            [VariationSpecifics] => SimpleXMLElement Object
                                (
                                    [NameValueList] => Array
                                        (
                                            [0] => SimpleXMLElement Object
                                                (
                                                    [Name] => Main Colour
                                                    [Value] => BLACK
                                                )

                                            [1] => SimpleXMLElement Object
                                                (
                                                    [Name] => Size
                                                    [Value] => 5FT Kingsize
                                                )

                                            [2] => SimpleXMLElement Object
                                                (
                                                    [Name] => Mattress
                                                    [Value] => Memory Foam Mattress
                                                )

                                        )

                                )

                            [VariationTitle] => LIBRETTO 4FT6 DOUBLE & 5FT KING SIZE MODERN LEATHER BED + MEMORY FOAM MATTRESS[BLACK,5FT Kingsize,Memory Foam Mattress]
                            [VariationViewItemURL] => http://cgi.ebay.co.uk/ws/eBayISAPI.dll?ViewItem&item=111791840971&vti=Main+Colour%09BLACK%0ASize%095FT+Kingsize%0AMattress%09Memory+Foam+Mattress
                        )

                    [Taxes] => SimpleXMLElement Object
                        (
                            [TotalTaxAmount] => 0.0
                            [TaxDetails] => Array
                                (
                                    [0] => SimpleXMLElement Object
                                        (
                                            [Imposition] => SalesTax
                                            [TaxDescription] => SalesTax
                                            [TaxAmount] => 0.0
                                            [TaxOnSubtotalAmount] => 0.0
                                            [TaxOnShippingAmount] => 0.0
                                            [TaxOnHandlingAmount] => 0.0
                                        )

                                    [1] => SimpleXMLElement Object
                                        (
                                            [Imposition] => WasteRecyclingFee
                                            [TaxDescription] => ElectronicWasteRecyclingFee
                                            [TaxAmount] => 0.0
                                        )

                                )

                        )

                    [OrderLineItemID] => 111791840971-1563884971001
                )

        )

    [BuyerUserID] => chopycob
    [PaidTime] => 2016-09-14T12:23:02.000Z
    [IntegratedMerchantCreditCardEnabled] => false
    [EIASToken] => nY+sHZ2PrBmdj6wVnY+sEZ2PrA2dj6wNmYGkAJSBog6dj6x9nY+seQ==
    [MonetaryDetails] => SimpleXMLElement Object
        (
            [Payments] => SimpleXMLElement Object
                (
                    [Payment] => SimpleXMLElement Object
                        (
                            [PaymentStatus] => Succeeded
                            [Payer] => chopycob
                            [Payee] => ijinteriors
                            [PaymentTime] => 2016-09-14T12:23:01.000Z
                            [PaymentAmount] => 204.0
                            [ReferenceID] => 36T214607M855544B
                            [FeeOrCreditAmount] => 3.06
                        )

                )

        )

)
