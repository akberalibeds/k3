<?php
/** @author Giomani Designs **/
//
namespace App\Helper;

use Illuminate\Support\ServiceProvider;

/**
 * @author Giomani Designs (Development Team)
 */
class Barcode
{
  /**
   * @var
   */
  private static $characters = '!#$%&()*+-0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
  /**
   *
   * @param int $id order id
   * @param Datetime $date
   */
  public static function makeBarcode($id, $date, $last2Chars)
  {
    $paddedId = str_pad('' + $id, 7, '0', STR_PAD_LEFT);

    for ($i = 0; $i < 7; $i++) {
      $barcode .= $characters[$paddedId[$i]];
    }

    $barcode .= $characters[$date->format('y') - 10];
    $barcode .= $characters[$date->format('n')];
    $barcode .= $characters[$date->format('j')];

    return $barcode;
  }
}
