<?php
namespace App\Helper;


class Json {
	
	
	
	public function __construct($data){	
		
		if(is_array($data) || is_object($data)){
			$this->encode($data);
		}
		
		else{
			$this->decode($data);
		}
	}
	
	
	public function encode($data = null){
		return $this->toJson($data);
	}
	
	public function decode($data = null){
		
		$data = $data ? $data : get_object_vars($this); 
		
		$a = json_decode($data);
		foreach($a as $k => $v){
			$this->{$k} = $v;
		}
	
		return $this;
	}
	
	
	
	public function addArray(array $data){
		
		foreach($data as $k => $v){
			$this->{$k} = $v;
		}
		return $this;
	}
	
	
	public function addKeyValuePair(string $key,string $value){
	
			$this->{$key} = $value;	
		
		return $this;
	}
	
	
	private function toJson($data = null){
		
		if($data){
			return json_encode($data);
		}
		else{
			return json_encode(get_object_vars($this));
		}
		
	}
	
	public function toArray(){
	
		return get_object_vars($this);
	
	}
	
	
	
}