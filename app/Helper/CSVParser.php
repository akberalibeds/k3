<?php

//
namespace App\Helper;


class CSVParser
{
	public $data;
	
	
	public function __construct($file = false)
	{
		$this->data=[];
		if($file){
			$file = fopen($file,"r");
			$i=0;
			while(! feof($file))
			{
				$row = fgetcsv($file);	
				$this->data[$i]=$row; 
				$i++;
			}
		fclose($file);
		}
		
	}
	
	
	
}