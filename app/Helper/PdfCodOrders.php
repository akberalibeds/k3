<?php
/** @author Giomani Designs **/
//
namespace App\Helper;

use App\Helper\Pdfs\Pdf;
use App\Helper\Fpdf\Barcode;

/**
 * @author Giomani Designs (Development Team)
 */
class PdfCodOrders extends Pdf
{
  /**
   * @var
   */
  private $columnAligns = ['R', 'R', 'R', 'R', 'R', 'L', 'R', 'C'];
  /**
   * @var
   */
  private $columnHeadings = ['Order', 'Time', 'Total', 'Paid', 'Owed', 'Name', 'Telephone', 'Priority'];
  /**
   * @var
   */
  private $columnWidths = [15, 11, 16, 10, 16, 88, 28, 15];
  /**
   * @var
   */
  private $pageHeading;
  /**
   * @author Giomani Designs (Development Team)
   */
  public function __construct() {
    parent::__construct();
    $this->SetAutoPageBreak(true);
  }
  /**
   * @author Giomani Designs (Development Team)
   * @see    App\Helper\Fpdf\FPDF::Header()
   */
  function Header() {
    $this->SetFont('Arial', 'B', 10);
    $this->Cell(0, 6.0, $this->pageHeading, 0, 1);
    for ($i = 0, $j = count($this->columnHeadings); $i < $j; $i++) {
      $this->Cell($this->columnWidths[$i], 6.0, $this->columnHeadings[$i], 1, 0, 'L');
    }
    $this->Ln();
  }
  /**
   * @author Giomani Designs (Development Team)
   * @see    App\Helper\Fpdf\FPDF::Header()
   */
  function Footer() {
    $this->SetY(-15);
    $this->SetFont('Arial', 'B', 10);
    $this->Cell(0, 6.0, 'page ' . $this->PageNo() . ' of {nb}', 0, 1, 'C');
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param Collection $orders
   * @param striing $date date int he format dd-mmm-yyyy
   */
  public function pdfCodOrders($orders, $date) {
    $this->pageHeading = 'COD Orders (' . $orders->count() . ') ' . $date ;
    $this->AddPage();
    $this->SetFont('Arial', '', 10);
    foreach ($orders as $order) {
      $row = [$order['oid'], $order['delTime'], number_format($order['total'], 2), number_format($order['paid'], 2), number_format($order['owed'], 2)];
      if ($order['direct'] == '1') {
        $row[] = $order['d_name'];
        $row[] = $order['d_tel'];
      } else {
        $row[] = $order['c_name'];
        $row[] = $order['c_tel'];
      }
      $row[] = $order['priority'] == 1 ? 'Yes' : '';
      for ($i = 0, $j = count($row); $i < $j; $i++) {
        $this->Cell($this->columnWidths[$i], 6.0, $row[$i], 1, 0, $this->columnAligns[$i]);
      }
      $this->Ln();
      if ($this->GetY() > 275) {
        $this->AddPage();
      }
    }
    return $this;
  }
}
