<?php

//
namespace App\Helper;


class Time extends \DateTime
{
	
	
	
	public function __construct()
	{
		 
			
	}
	
	
	public function date($stamp='now'){
	
		if(is_int($stamp)){
			parent::__construct();
			$this->setTimezone(new \DateTimeZone("Europe/London"));    
			$this->setTimestamp($stamp);
		}
		else{
			parent::__construct($stamp, new \DateTimeZone("Europe/London"));
		}
	
		return $this;
	
	}
	
	
	public function timezone($tz)
	{
		$this->setTimezone(new \DateTimeZone($tz));	
	}
	
	
	public function get($format='d-M-Y H:i:s')
	{
		return $this->format($format);		
	}
	
	
	
	/*
	 * Adjust Years
	 */
	
	public function years($val)
	{
		$this->modify($val.' years');
		return $this;
	}
	
	/*
	 * Adjust months
	 */
	public function months($val)
	{
		$this->modify($val.' months');
		return $this;
	}
	
	
	
	/*
	 * Adjust days
	 */
	public function days($val)
	{
		$this->modify($val.' days');
		return $this;
	}
	
	
	/*
	 * Adjust Hours
	 */
	public function hours($val)
	{
		$this->modify($val.' hours');
		return $this;
	}
	
	
	/*
	 * Adjust minutes
	 */
	public function minutes($val)
	{
		$this->modify($val.' minutes');
		return $this;
	}
	
	
	
	/*
	 * Adjust Seconds
	 */
	public function seconds($val)
	{
		$this->modify($val.' seconds');
		return $this;
	}
	
	
	public function getStamp()
	{
		return $this->getTimestamp();
	}
	
	
	public static function compare($from,$to)
	{
		
		$datetime1 = new \DateTime($from);
		$datetime2 = new \DateTime($to);
		
		$interval = $datetime1->diff($datetime2);
		return $interval->format('%R%a days');	
	}
	
}
