<?php

namespace App\Helper\Magento;

error_reporting(E_ERROR);
//ini_set('display_errors','on');

use App;
use App\Order;
use App\Helper\INIParser;
use Time;
use Sale;
use App\Customer;
use App\StockItem;
use App\Mailer;
use App\MagentoSettings;


class Magento {

	private $url='https://beds.co.uk/api/soap/'; 
	public $client;
	public $session;
	private $mailer; 
	private $expressMailer;
	private $splitMailer;
	private $existing_orders = NULL;
	public  $orders;
	private $prepared= false;
 	
 
 public function __construct () {
	  // $this->mailer = new Mail; 	
	   $this->client = new \SoapClient($this->url."?wsdl",
		    array(
		        "trace" => 1,
		        "location" => $this->url,
		        'exceptions' => 1,
		        "stream_context" => stream_context_create(
		            array(
		                'ssl' => array(
		                    'verify_peer'       => false,
		                    'verify_peer_name'  => false,
		                )
		            )
		        )
		    )  
		);  
	   $this->session = $this->client->login('system', 'Alphabravo147@');
	//    $this->existing_orders = $this->getExistingOrders();
 }


/*
 * Get Orders Call
 */
 public function getOrders($from=NULL,$to=NULL){
	
	 $to = ($to) ? $to : $this->getToTime();
	 $from = ($from) ? $from : $this->getFromTime();
	 echo "getting Orders start from $from - $to\n";
	 try{
		 if(app()->environment() == 'testing') {
			$orders = file_get_contents(storage_path('magentoOrders.json'));
			$result = json_decode($orders, true);
		 }
		 else {
			$result = $this->client->call($this->session, 'sales_order.list',
	        array(
	            array(
	                'created_at'=>
	                    array(
	                        'from'=> $from,
	                        'to'=> $to
	                    )
	                )
	            )
	        );
		 }	
		 
		 $this->orders = $this->filter($result);
		 
		 
	 } catch (\SoapFault $e) {
	 	echo $e->getMessage();
	 }
	 echo "getting Orders end\n"; 	
	 return $this; 
 }

	private function filter($orders)
	{
		foreach($orders as $key => $order) 
		{
			if($order['state'] != 'processing' && $order['status'] != 'pending')
			{
				unset($orders[$key]);
			}

		}

		return $orders;
	}


    /**
     * remove duplicate orders that we get from Magento
     */
    public function removeExisting()
	{
		$increment_ids = [];

		foreach ($this->orders as $order) {
			$increment_ids[] = $order['increment_id'];
		}

		$toBeRemoved = Order::whereIn('companyName', ['Beds.co.uk'])
								->whereIn('beds', $increment_ids)
								->pluck('beds');

		foreach ($this->orders as $key => $order) {

			if (in_array($order['increment_id'], $toBeRemoved->toArray())) {

				unset($this->orders[$key]);
			}
		}

		$this->orders = array_values($this->orders);
		return $this;
	}


 /*
  *	 Get All order Info from Magento	
  */
  public function getAllInfo()
  {		
  		 echo "getting order info\n";
		for($i=0;$i<count($this->orders);$i++)
		{
			$this->orders[$i] = $this->getOrderInfo($this->orders[$i]['increment_id']); 
		}
	
	return $this;  
  }
  

  /*
   * Format orders for DB Entry
   */
  public function formatOrders()
  {
		echo "formatting orders \n";
		$orders = [];
		
		foreach ($this->orders as $order)
		{
			$current = [];

                $current['customer'] = $this->getCustomerDetails($order);
                $current['order'] = $this->parseOrder($order);

			$orders[]=$current;
		}
			
		$this->orders = $orders;
		$this->prepared= true;
		
	return $this;  
  }
  
  
  /*
   * Insert prepared Orders in to table
   */
  public function insertOrders()
  {
	  echo "inserting orders \n";
	  if(!$this->prepared)
	  {
		return App::abort(404, 'Orders Not Prepared');  
	  }
	  
	 
	  
	  
	foreach($this->orders as &$order)  
	  {	  
	  	 
	  	  $customer = Customer::insertIfNotExists($order['customer'])->getCustomer();
		  if(!isset($customer->id))
		  {
			  return App::abort(404, 'Customer error');
		  }
		  $order['order']['cid'] =  $customer->id;
		  $sale = Sale::newSale((object)$order['order'])
				->addItems()
				->setIno()
				->setDeliveryRoute()
				->setBarcode()
		  		->securityCheck()
		  		->check_split();


		  if($sale->expressMail){
		  		$this->sendBedsMail($order['customer']['email1'],$order['customer']['businessName'],'express');
		  		echo "Sent Express Mail\n";
		  }
		  
		  if($sale->splitMail){
		  		$this->sendBedsMail($order['customer']['email1'],$order['customer']['businessName'],'split');	
				echo "Sending Split Mail\n";	
		  }
		//   $this->existing_orders[] = $order['order']['beds'];
		
		  echo "inserted : ".$order['order']['beds']."\n";
	  }
	  
	//   $this->writeExistingOrders();

	  return $this;
  }
  
  
  
 	private function getFromTime(){
		return Time::date()->hours(-4)->get('Y-m-d H:i:s');
 		//return Time::date()->get('Y-m-d');
	}
	
	private function getToTime(){
		return Time::date()->get('Y-m-d H:i:s');		
		//return Time::date($this->getFromTime())->minutes(60)->get('Y-m-d H:i:s');
	}
	
	public function getOrderInfo($id){

		if(app()->environment() == 'testing') {
			$info = file_get_contents(storage_path('magentoOrderInfo.json'));
			$result = json_decode($info, true);
			
		} else {
			$attempt = 1; 
			
			while($attempt<10){
				$result = $this->client->call($this->session, 'sales_order.info',$id);
				if (!is_soap_fault($result)) {
					break;
				}
				$attempt++;
			}
		}
	
		return $result;
	}
	
	
	
	
	public function getStockList(){
			//$result = $this->client->call($this->session, 'cataloginventory_stock_item.list', '1');	
			$result = $this->client->call($this->session, 'catalog_product.list');	
			return $result;
	}
	
	
	
	
	private function getCustomerDetails($details){
		$array=array();
		$array['dBusinessName'] = $details['shipping_address']['firstname']." ".$details['shipping_address']['lastname'];
		$array['dStreet']= $details['shipping_address']['street'];
		$array['dTown']= $details['shipping_address']['city'];
		$array['dPostcode']= $details['shipping_address']['postcode'];
		
		$array['businessName'] = $details['billing_address']['firstname']." ".$details['billing_address']['lastname'];
		$array['street']= $details['billing_address']['street'];
		$array['town']= $details['billing_address']['city'];
		$array['postcode']= $details['billing_address']['postcode'];
		$array['email1'] = $details['billing_address']['email'];
		$array['tel'] = $details['billing_address']['telephone'];
		return $array;	
	}
	
	

	/*
	 * Parse Order
	 * @param  Array - Magento Order
	 * @return Array - Parsed Order
	 *
	 */
	private function parseOrder($details)
	{
	
		$order = [
				
				'staffName' 		=> 'Beds.co.uk',
				'staffid'		=> 0,
				'ouref'			=> 'Beds.co.uk',
				'companyName'	=> 'Beds.co.uk',
				'startDate'		=> Time::date($details['created_at'])->get('d-M-Y'),
				'startTime'		=> Time::date($details['created_at'])->get('H:i:s'),
				'total'			=> str_replace(",","",$details['grand_total']),
				'paid'  			=> ($details['total_paid']=='') ? 0.00 : str_replace(",","",$details['total_paid']),
				'beds'			=> $details['increment_id'],
				'startStamp'		=> Time::date($details['created_at'])->getStamp(),
				'time_stamp'		=> $details['created_at'],
				'last_trans_id'	=> $details['payment']['last_trans_id'],
				'items'			=> $this->getItems($details),	
				
				];
		
			
		if($details['status']=='pending' && $details['state']!="processing") { $order['paymentType'] = 'Cash'; }
		else {	$order['paymentType'] = "Paypal";	}
		
		return $order;
	}
	

	private function getItems($details){
		
		$orderItems = $details['items'];	
		$i=0;
		$items = array();
		foreach($orderItems as $item){
		
					 $qty= $item['qty_ordered'];
					 $VariationSKU = $item['sku'];
					 $parts = explode("/",$VariationSKU);
					 $stockCount = array();									
									
					// split variation to check which item or sets etc
					if(count($parts)==1){
						$parts = explode("-",$VariationSKU);
						
							$var = [];
							
							if($parts[0]=="320" || $parts[0]=="D320"){
							        
							        $stock = StockItem::itemCodeBeds($this->getSKU($VariationSKU),$item['price']);
							        $var['itemid'] = $stock->id;
							        $var['currStock'] = $stock->itemQty;
							        $var['itemCode'] = $this->getSKU($VariationSKU);
							        $var['Qty'] = $qty;
							        $var['costs'] = $stock->cost;
							        $var['price']= $item['price'];
							        $var['lineTotal']= $item['price'] * $qty;
							       
							        $items[]=$var;
							        $pp++;
							        $i++;
							    
							}
							else if(count($parts)==4 && $parts[0]!="CH"){ 
									$stock = StockItem::itemCodeBeds($this->getSKU("$parts[0]-$parts[1]-$parts[2]"),$item['price']);
									$var['itemid'] = $stock->id; 
									$var['currStock'] = $stock->itemQty;
									$var['Qty'] = $qty; 
									$var['itemCode'] = $this->getSKU("$parts[0]-$parts[1]-$parts[2]"); 
									$var['price']= $item['price'];
									$var['lineTotal']= $item['price'] * $qty;
									$var['costs'] = $stock->cost;
									$items[]=$var;
									 
									$stock = StockItem::itemCodeBeds($this->getSKU("$parts[1]-$parts[3]"),$item['price']);
									$var['itemid'] = $stock->id; 
									$var['currStock'] = $stock->itemQty;
									$var['itemCode'] = $this->getSKU("$parts[1]-$parts[3]"); 
									$var['Qty'] = $qty;
									$var['price'] = 0.00;
									$var['lineTotal']= 0.00;
									$var['costs'] = $stock->cost;
									$items[]=$var;
							}
							else if(count($parts)==4 && $parts[0]=="CH"){
									$stock = StockItem::itemCodeBeds($this->getSKU("$parts[1]-$parts[2]"),$item['price']);
									$var['itemid'] = $stock->id; 
									$var['currStock'] = $stock->itemQty;
									$var['itemCode'] = $this->getSKU("$parts[1]-$parts[2]"); 
									$var['Qty']=$parts[3]*$qty;
									$var['price']= $item['price'];
									$var['lineTotal']= $item['price'] * $qty;
									$var['costs'] = $stock->cost;
									$items[]=$var;
									
							}
							else if(count($parts)==6){ 
									$stock = StockItem::itemCodeBeds($this->getSKU("$parts[1]-$parts[2]"),$item['price']);
									$var['itemid'] = $stock->id; 
									$var['currStock'] = $stock->itemQty;
									$var['itemCode'] = $this->getSKU("$parts[1]-$parts[2]"); 
									$var['Qty'] = $qty;
									$var['price']= $item['price'];
									$var['lineTotal']= $item['price'] * $qty;
									$var['costs'] = $stock->cost;
									$items[]=$var;
									
									$stock = StockItem::itemCodeBeds($this->getSKU("$parts[3]-$parts[4]"),$item['price']);
									$var['itemid'] = $stock->id; 
									$var['currStock'] = $stock->itemQty;
									$var['itemCode'] = $this->getSKU("$parts[3]-$parts[4]"); 
									$var['Qty']=$parts[5]*$qty; 
									$var['price']= 0.00;
									$var['lineTotal']= 0.00;
									$var['costs'] = $stock->cost;
									$items[]=$var;
									
							}
							else{ 
									$stock = StockItem::itemCodeBeds($this->getSKU($VariationSKU),$item['price']);
									$var['itemid'] = $stock->id; 
									$var['currStock'] = $stock->itemQty;
									$var['itemCode'] = $this->getSKU($VariationSKU); 
									$var['Qty'] = $qty;
									$var['price']= $item['price'];
									$var['lineTotal']= $item['price'] * $qty;
									$var['costs'] = $stock->cost;
									$items[]=$var;
							}
							
					}
									
					else{
						
						$pp=0;
						foreach($parts  as $p){
							$var =[];
							$stock = StockItem::itemCodeBeds($this->getSKU($p),$item['price']);
							$var['itemid'] = $stock->id; 
							$var['currStock'] = $stock->itemQty;
							$var['itemCode'] = $this->getSKU($p);
							$var['Qty'] = $qty;
							$var['costs'] = $stock->cost;
							
							if($pp==0){
								$var['price']= $item['price'];
								$var['lineTotal']= $item['price'] * $qty;
							}
							else{
								$var['price']= 0.00;
								$var['lineTotal']= 0.00;	
							}
							$items[]=$var;
							$pp++;
							$i++;
						}
					}
			$i++;		
			
			}
			
			
		return $items;
	  
	}
	
	
	private function getDate($details){
		return date('d-M-Y', strtotime($details['created_at']));	
	}
	
	private function getTime($details){
		return date('H:i:s', strtotime($details['created_at']));	
	}
	
	public function sendBedsMail($to,$name,$template){
		Mailer::sendBedsMail((object)['toName' => $name, 'toEmail' =>  $to, 'template' => $template]);
	}
	
//	public function getExistingOrders(){
//	//	echo "loading ini\n";
//		$parser = new INIParser;
//		$file = __DIR__."/beds.ini";
//	//	echo "loaded ini\n";
//		return $parser->read($file);
//	}
//
//	public function writeExistingOrders(){
//
//		echo "writing ini\n";
//		$parser = new INIParser;
//		echo $file = __DIR__."/beds.ini";
//		echo "written ini\n";
//		echo "Saving new time\n";
//		MagentoSettings::saveTime();
//		echo "Time Saved\n";
//		return $parser->write($this->existing_orders,$file);
//	}

	
	private function getSKU($itemCode){
	
		$itemCode = str_replace("160-","130-",$itemCode);
		$itemCode = str_replace("161-","113-",$itemCode);
		$itemCode = str_replace("921-","901-",$itemCode);
		$itemCode = str_replace("922-","902-",$itemCode);
		$itemCode = str_replace("926-","906-",$itemCode);
		$itemCode = str_replace("927-","907-",$itemCode);
		$itemCode = str_replace("923L-","953L-",$itemCode);
		$itemCode = str_replace("923M-","953M-",$itemCode);
		$itemCode = str_replace("923S-","953S-",$itemCode);	
		
		return $itemCode;	
	}

	
	/*
	 * 
	 * AJAX MAGENTO
	 * 
	 */
	/*
	 * Get Orders Call
	 */
	public function getOrdersAjax($from=NULL,$to=NULL){
	
		$to = ($to) ? $to : $this->getToTime();
		$from = ($from) ? $from : $this->getFromTime();
		try{
			$result = $this->client->call($this->session, 'sales_order.list',
					array(
							array(
									'created_at'=>
									array(
											'from'=> $from,
											'to'=> $to
									)
							)
					)
					);
	
			   $this->orders = $this->filter($result);
			// $this->orders = $result;
		} catch (\SoapFault $e) {
			echo $e->getMessage();
		}
		return $this;
	}
	
	
	
}