<?php

namespace App\Helper\Magento;

error_reporting(E_ERROR);

use App;
use App\Helper\INIParser;
use Time;
use Sale;
use App\Customer;
use App\StockItem;
use App\Mailer;

class Magento2 {
	
	//http://beds.uk/soap/default?wsdl_list=1
	
	private $url= 'http://beds.uk/soap/default?wsdl&services='; //'http://magento.local/soap/default?wsdl&services=';
	private $token = '0ihsdje5jwwe0o404wyhht0mcqildwvd';//'0px57chpn7xbktb89c4ihqseb4nreluh';
	private $client;
	private $session;
	private $mailer; 
	private $expressMailer;
	private $splitMailer;
	private $existing_orders = NULL;
	private $prepared= false;
	private $soapOptions;
	
	/*
	 * Array of returned Orders
	 */
	public  $orders;
 
 
 public function __construct () {
	  
	   	$this->existing_orders = $this->getExistingOrders();
 	   	
		$opts = [
				'http' => ['header' => "Authorization: Bearer ".$this->token ],
				'ssl'  => ['verify_peer' => false, 'verify_peer_name'  => false ]
				];
				
	 	$context = stream_context_create($opts);
	 	$this->soapOptions['stream_context'] = $context;
		
 }



/*
 *  Call to retrieve orders from magento
 *  @func getOrders
 *  @param from date(Y-m-d h:i:s) default -1 Day
 *  @param to date(Y-m-d h:i:s) default now
 */ 
 public function getOrders($from=NULL,$to=NULL){
	 
	 
	 $this->client = new \SoapClient($this->url."salesOrderRepositoryV1",$this->soapOptions); 
	
	 //$from = "2016-09-21 00:00:00";
	 //$to   = "2016-09-22 00:00:00";
	
	 $to = ($to) ? $to : $this->getToTime();
	 $from = ($from) ? $from : $this->getFromTime();
	 
	 $data = ['searchCriteria' => [
				  'filterGroups' => [
					0 => [
					  'filters' => [
						 0 => [
						   'field' => 'created_at',
						   'value' => $from,
						   'conditionType' => 'gteq'
						 ]
					  ]
					],
					1 => [
					  'filters' => [
						 0 => [
						   'field' => 'created_at',
						   'value' => $to,
						   'conditionType' => 'lteq'
						 ]
					  ]
					]
				  ]
				]
			  ]; 
	 
	 $result = $this->client->salesOrderRepositoryV1GetList($data);
	 $this->orders = (array)$result->result->items->item;
	 echo "getting Orders end\n"; 	
	 return $this; 
 }
 
 
 
 /*
 *  Call to retrieve detailed order info
 *  @func getOrderById
 *  @param id - magento order entityId
 */
 public function getOrderById($id){
	 
	 $this->client = new \SoapClient($this->url."salesOrderRepositoryV1",$this->soapOptions); 
	 $to = ($to) ? $to : $this->getToTime();
	 $from = ($from) ? $from : $this->getFromTime();
	 $data = ['id' => $id]; 	  
	 $result = $this->client->salesOrderRepositoryV1Get($data);
	 return (array)$result->result;
	  
 }
 
 
  
 /*
  * Remove Existing and non valid orders from Orders Array
  */
 public function removeExisting(){
 	
	echo "removing existing orders \n";
	
	$this->removed = [];
	for($i=0;$i<count($this->orders);$i++)
	{
		if(in_array($this->orders[$i]->incrementId,$this->existing_orders))
		{
			$this->removed[]=$i;
		}
		else 
		{
			if($this->orders[$i]->state=='processing' || $this->orders[$i]->status=='pending'){ }
			else{ $this->removed[]=$i; }
		}
 	}
	
	foreach($this->removed as $r)
	{
		unset($this->orders[$r]);	
	}
	
	$this->orders = array_values($this->orders);
	
	return $this;
 }
 
 
 
 /*
  *	 Get All order Info from Magento	
  */
  public function getAllInfo()
  {		
  		
		for($i=0;$i<count($this->orders);$i++)
		{
			$this->orders[$i] = $this->getOrderById( (string)$this->orders[$i]->entityId );
		}
	
	return $this;  
  }
  
  
  
  /*
   * Format orders for DB Entry
   */
  public function formatOrders()
  {
		echo "formatting orders \n";
		$orders = [];
		
		foreach ($this->orders as $o)
		{
			$order = (array) $o;
			$current = [];
			$current['customer'] = $this->getCustomerDetails($order);
			$current['order'] = $this->parseOrder($order); 	
			$orders[]=$current;
		}
			
		$this->orders = $orders;
		$this->prepared= true;
		
	return $this;  
  }
  
  
  /*
   * Insert prepared Orders in to table
   */
  public function insertOrders()
  {
	  echo "inserting orders \n";
	  if(!$this->prepared)
	  {
		return App::abort(404, 'Orders Not Prepared');  
	  }
	  
	 
	  
	  
	foreach($this->orders as &$order)  
	  {	  
	  	 
	  	  $customer = Customer::insertIfNotExists($order['customer'])->getCustomer();
		  if(!isset($customer->id))
		  {
			  return App::abort(404, 'Customer error');
		  }
		  $order['order']['cid'] =  $customer->id;
		  $sale = Sale::newSale((object)$order['order'])
				->addItems()
				->setIno()
				->setDeliveryRoute()
				->setBarcode()
				->securityCheck()
				->check_split();


		  if($sale->expressMail){
		  		$this->sendBedsMail($order['customer']['email1'],$order['customer']['businessName'],'express');
		  		echo "Sent Express Mail\n";
		  }
		  
		  if($sale->splitMail){
		  		$this->sendBedsMail($order['customer']['email1'],$order['customer']['businessName'],'split');	
				echo "Sending Split Mail\n";	
		  }
		  $this->existing_orders[] = $order['order']['beds'];
		
		  echo "inserted : ".$order['order']['beds']."\n";
	  }
	  
	  $this->writeExistingOrders();

	  return $this;
  }
  
  
  
 	private function getFromTime(){
		return Time::date()->days(-1)->get('Y-m-d 10:00:00');
	}
	
	private function getToTime(){
		return Time::date()->get('Y-m-d H:i:s');		
	}
	
	public function getOrderInfo($id){
		$attempt = 1; 
		while($attempt<10){
			$result = $this->client->call($this->session, 'sales_order.info',$id);
			if (!is_soap_fault($result)) {
				break;
			}
			$attempt++;
		}
		return $result;
	}
	
	
	
	
	private function getCustomerDetails($details){
		$array=array();
		
		$shipping = (array) $details['extensionAttributes']->shippingAssignments->item->shipping->address;
		$billing = (array) $details['billingAddress'];
		
		$array['dBusinessName'] = $shipping['firstname']." ".$shipping['lastname'];
		$array['dStreet']= implode(" ",(array) $shipping['street']);
		$array['dTown']= $shipping['city'];
		$array['dPostcode']= $shipping['postcode'];
		
		$array['businessName'] = $billing['firstname']." ".$billing['lastname'];
		$array['street']= implode(" ",(array) $billing['street']);
		$array['town']= $billing['city'];
		$array['postcode']= $billing['postcode'];
		$array['email1'] = $details['customerEmail'];
		$array['tel'] = $billing['telephone'];
		return $array;	
	}
	
	
	
	
	/*
	 * Parse Order
	 * @param  Array - Magento Order
	 * @return Array - Parsed Order
	 *
	 */
	private function parseOrder($details)
	{
	
		$order = [
				
				'staffName' 		=> 'Beds.uk',
				'staffid'		=> 0,
				'ouref'			=> 'Beds.uk',
				'companyName'	=> 'Beds.co.uk',
				'startDate'		=> Time::date($details['createdAt'])->get('d-M-Y'),
				'startTime'		=> Time::date($details['createdAt'])->get('H:i:s'),
				'total'			=> str_replace(",","",$details['grandTotal']),
		        'paid'  			=> str_replace(",","",$details['grandTotal']) - str_replace(",","",$details['totalDue']) ,
				'beds'			=> $details['incrementId'],
				'startStamp'		=> Time::date($details['createdAt'])->getStamp(),
				'time_stamp'		=> $details['createdAt'],
		        'last_trans_id'	=> $details['payment']->lastTransId,
				'items'			=> $this->getItems($details),
				
				];
		
		if($details['status']=='pending' && $details['state']!="processing") { $order['paymentType'] = 'Cash'; }
		else {	$order['paymentType'] = "Paypal";	}
		
		return $order;
	}
	
	
	
	private function getItems($details){
		
		$orderItems = $details['items'];	
		$i=0;
		$items = array();
		foreach($orderItems as $itemer){
		
		 			 $item = (array) $itemer;
					 $item = (array) $item[0];
					 $qty= $item['qtyOrdered'];
					 $VariationSKU = $item['sku'];
					 $parts = explode("/",$VariationSKU);
					 $stockCount = array();									
									
					// split variation to check which item or sets etc
					if(count($parts)==1){
						$parts = explode("-",$VariationSKU);
						
							$var = [];
							if(count($parts)==4 && $parts[0]!="CH"){ 
									$stock = StockItem::itemCodeBeds($this->getSKU("$parts[0]-$parts[1]-$parts[2]"),$item['price']);
									$var['itemid'] = $stock->id; 
									$var['currStock'] = $stock->itemQty;
									$var['Qty'] = $qty; 
									$var['itemCode'] = $this->getSKU("$parts[0]-$parts[1]-$parts[2]"); 
									$var['price']= $item['price'];
									$var['lineTotal']= $item['price'] * $qty;
									$var['costs'] = $stock->cost;
									$items[]=$var;
									 
									$stock = StockItem::itemCodeBeds($this->getSKU("$parts[1]-$parts[3]"),$item['price']);
									$var['itemid'] = $stock->id; 
									$var['currStock'] = $stock->itemQty;
									$var['itemCode'] = $this->getSKU("$parts[1]-$parts[3]"); 
									$var['Qty'] = $qty;
									$var['price'] = 0.00;
									$var['lineTotal']= 0.00;
									$var['costs'] = $stock->cost;
									$items[]=$var;
							}
							else if(count($parts)==4 && $parts[0]=="CH"){
									$stock = StockItem::itemCodeBeds($this->getSKU("$parts[1]-$parts[2]"),$item['price']);
									$var['itemid'] = $stock->id; 
									$var['currStock'] = $stock->itemQty;
									$var['itemCode'] = $this->getSKU("$parts[1]-$parts[2]"); 
									$var['Qty']=$parts[3]*$qty;
									$var['price']= $item['price'];
									$var['lineTotal']= $item['price'] * $qty;
									$var['costs'] = $stock->cost;
									$items[]=$var;
									
							}
							else if(count($parts)==6){ 
									$stock = StockItem::itemCodeBeds($this->getSKU("$parts[1]-$parts[2]"),$item['price']);
									$var['itemid'] = $stock->id; 
									$var['currStock'] = $stock->itemQty;
									$var['itemCode'] = $this->getSKU("$parts[1]-$parts[2]"); 
									$var['Qty'] = $qty;
									$var['price']= $item['price'];
									$var['lineTotal']= $item['price'] * $qty;
									$var['costs'] = $stock->cost;
									$items[]=$var;
									
									$stock = StockItem::itemCodeBeds($this->getSKU("$parts[3]-$parts[4]"),$item['price']);
									$var['itemid'] = $stock->id; 
									$var['currStock'] = $stock->itemQty;
									$var['itemCode'] = $this->getSKU("$parts[3]-$parts[4]"); 
									$var['Qty']=$parts[5]*$qty; 
									$var['price']= 0.00;
									$var['lineTotal']= 0.00;
									$var['costs'] = $stock->cost;
									$items[]=$var;
									
							}
							else{ 
									$stock = StockItem::itemCodeBeds($this->getSKU($VariationSKU),$item['price']);
									$var['itemid'] = $stock->id; 
									$var['currStock'] = $stock->itemQty;
									$var['itemCode'] = $this->getSKU($VariationSKU); 
									$var['Qty'] = $qty;
									$var['price']= $item['price'];
									$var['lineTotal']= $item['price'] * $qty;
									$var['costs'] = $stock->cost;
									$items[]=$var;
							}
							
					}
									
					else{
						
						$pp=0;
						foreach($parts  as $p){
							$var =[];
							$stock = StockItem::itemCodeBeds($this->getSKU($p),$item['price']);
							$var['itemid'] = $stock->id; 
							$var['currStock'] = $stock->itemQty;
							$var['itemCode'] = $this->getSKU($p);
							$var['Qty'] = $qty;
							$var['costs'] = $stock->cost;
							
							if($pp==0){
								$var['price']= $item['price'];
								$var['lineTotal']= $item['price'] * $qty;
							}
							else{
								$var['price']= 0.00;
								$var['lineTotal']= 0.00;	
							}
							$items[]=$var;
							$pp++;
							$i++;
						}
					}
			$i++;		
			
			}
			
			
		return $items;
	  
	}
	
	
	private function getDate($details){
		return date('d-M-Y', strtotime($details['created_at']));	
	}
	
	private function getTime($details){
		return date('H:i:s', strtotime($details['created_at']));	
	}
	
	public function sendBedsMail($to,$name,$template){
		Mailer::sendBedsMail((object)['toName' => $name, 'toEmail' =>  $to, 'template' => $template]);
	}
	
	public function getExistingOrders(){
		echo "loading ini\n";
		$parser = new INIParser;
		$file = __DIR__."/beds2.ini";
		echo "loaded ini\n";
		return $parser->read($file);	
	}
	
	public function writeExistingOrders(){
		echo "writing ini\n";
		$parser = new INIParser;
		$file = __DIR__."/beds2.ini";
		echo "written ini\n";
		return $parser->write($this->existing_orders,$file);	
	}
	
	
	
	private function getSKU($itemCode){
	
		$itemCode = str_replace("160-","130-",$itemCode);
		$itemCode = str_replace("161-","113-",$itemCode);
		$itemCode = str_replace("921-","901-",$itemCode);
		$itemCode = str_replace("922-","902-",$itemCode);
		$itemCode = str_replace("926-","906-",$itemCode);
		$itemCode = str_replace("927-","907-",$itemCode);
		$itemCode = str_replace("923L-","953L-",$itemCode);
		$itemCode = str_replace("923M-","953M-",$itemCode);
		$itemCode = str_replace("923S-","953S-",$itemCode);	
		
		return $itemCode;	
	}
	
}



/*

[0] => stdClass Object
        (
            [baseCurrencyCode] => GBP
            [baseDiscountAmount] => 0
            [baseGrandTotal] => 139
            [baseDiscountTaxCompensationAmount] => 0
            [baseShippingAmount] => 0
            [baseShippingDiscountAmount] => 0
            [baseShippingInclTax] => 0
            [baseShippingTaxAmount] => 0
            [baseSubtotal] => 139
            [baseSubtotalInclTax] => 139
            [baseTaxAmount] => 0
            [baseTotalDue] => 139
            [baseToGlobalRate] => 1
            [baseToOrderRate] => 1
            [billingAddressId] => 250038
            [createdAt] => 2016-10-19 11:54:09
            [customerEmail] => a.nideem@live.com
            [customerGroupId] => 0
            [customerIsGuest] => 1
            [customerNoteNotify] => 1
            [discountAmount] => 0
            [emailSent] => 1
            [entityId] => 125019
            [globalCurrencyCode] => GBP
            [grandTotal] => 139
            [discountTaxCompensationAmount] => 0
            [incrementId] => 000000033
            [isVirtual] => 0
            [orderCurrencyCode] => GBP
            [protectCode] => c70141
            [quoteId] => 280003
            [remoteIp] => 46.102.218.58
            [shippingAmount] => 0
            [shippingDescription] => Free Shipping - Free
            [shippingDiscountAmount] => 0
            [shippingDiscountTaxCompensationAmount] => 0
            [shippingInclTax] => 0
            [shippingTaxAmount] => 0
            [state] => new
            [status] => pending
            [storeCurrencyCode] => GBP
            [storeId] => 1
            [storeName] => Beds.co.uk
Main Website Store
De
            [storeToBaseRate] => 0
            [storeToOrderRate] => 0
            [subtotal] => 139
            [subtotalInclTax] => 139
            [taxAmount] => 0
            [totalDue] => 139
            [totalItemCount] => 1
            [totalQtyOrdered] => 1
            [updatedAt] => 2016-10-19 11:54:10
            [weight] => 1
            [items] => stdClass Object
                (
                    [item] => Array
                        (
                            [0] => stdClass Object
                                (
                                    [amountRefunded] => 0
                                    [baseAmountRefunded] => 0
                                    [baseDiscountAmount] => 0
                                    [baseDiscountInvoiced] => 0
                                    [baseDiscountTaxCompensationAmount] => 0
                                    [baseOriginalPrice] => 555
                                    [basePrice] => 139
                                    [basePriceInclTax] => 139
                                    [baseRowInvoiced] => 0
                                    [baseRowTotal] => 139
                                    [baseRowTotalInclTax] => 139
                                    [baseTaxAmount] => 0
                                    [baseTaxInvoiced] => 0
                                    [createdAt] => 2016-10-19 11:54:09
                                    [discountAmount] => 0
                                    [discountInvoiced] => 0
                                    [discountPercent] => 0
                                    [freeShipping] => 0
                                    [discountTaxCompensationAmount] => 0
                                    [isQtyDecimal] => 0
                                    [isVirtual] => 0
                                    [itemId] => 134189
                                    [name] => Roma Italian Modern Designer Leather Bed
                                    [noDiscount] => 0
                                    [orderId] => 125019
                                    [originalPrice] => 555
                                    [price] => 139
                                    [priceInclTax] => 139
                                    [productId] => 322
                                    [productType] => configurable
                                    [qtyCanceled] => 0
                                    [qtyInvoiced] => 0
                                    [qtyOrdered] => 1
                                    [qtyRefunded] => 0
                                    [qtyShipped] => 0
                                    [quoteItemId] => 466843
                                    [rowInvoiced] => 0
                                    [rowTotal] => 139
                                    [rowTotalInclTax] => 139
                                    [rowWeight] => 1
                                    [sku] => 116-4FT6-BLACK
                                    [storeId] => 1
                                    [taxAmount] => 0
                                    [taxInvoiced] => 0
                                    [taxPercent] => 0
                                    [updatedAt] => 2016-10-19 11:54:09
                                    [weight] => 1
                                )

                            [1] => stdClass Object
                                (
                                    [amountRefunded] => 0
                                    [baseAmountRefunded] => 0
                                    [baseDiscountAmount] => 0
                                    [baseDiscountInvoiced] => 0
                                    [basePrice] => 0
                                    [baseRowInvoiced] => 0
                                    [baseRowTotal] => 0
                                    [baseTaxAmount] => 0
                                    [baseTaxInvoiced] => 0
                                    [createdAt] => 2016-10-19 11:54:09
                                    [discountAmount] => 0
                                    [discountInvoiced] => 0
                                    [discountPercent] => 0
                                    [freeShipping] => 0
                                    [isQtyDecimal] => 0
                                    [isVirtual] => 0
                                    [itemId] => 134190
                                    [name] => Enzo Italian Modern Designer Leather Bed - Colour Black - Size 4FT6 Double - Mattress No Mattress - Frame Only
                                    [noDiscount] => 0
                                    [orderId] => 125019
                                    [originalPrice] => 0
                                    [parentItemId] => 134189
                                    [price] => 0
                                    [productId] => 323
                                    [productType] => simple
                                    [qtyCanceled] => 0
                                    [qtyInvoiced] => 0
                                    [qtyOrdered] => 1
                                    [qtyRefunded] => 0
                                    [qtyShipped] => 0
                                    [quoteItemId] => 466844
                                    [rowInvoiced] => 0
                                    [rowTotal] => 0
                                    [rowWeight] => 0
                                    [sku] => 116-4FT6-BLACK
                                    [storeId] => 1
                                    [taxAmount] => 0
                                    [taxInvoiced] => 0
                                    [taxPercent] => 0
                                    [updatedAt] => 2016-10-19 11:54:09
                                    [weight] => 1
                                    [parentItem] => stdClass Object
                                        (
                                            [amountRefunded] => 0
                                            [baseAmountRefunded] => 0
                                            [baseDiscountAmount] => 0
                                            [baseDiscountInvoiced] => 0
                                            [baseDiscountTaxCompensationAmount] => 0
                                            [baseOriginalPrice] => 555
                                            [basePrice] => 139
                                            [basePriceInclTax] => 139
                                            [baseRowInvoiced] => 0
                                            [baseRowTotal] => 139
                                            [baseRowTotalInclTax] => 139
                                            [baseTaxAmount] => 0
                                            [baseTaxInvoiced] => 0
                                            [createdAt] => 2016-10-19 11:54:09
                                            [discountAmount] => 0
                                            [discountInvoiced] => 0
                                            [discountPercent] => 0
                                            [freeShipping] => 0
                                            [discountTaxCompensationAmount] => 0
                                            [isQtyDecimal] => 0
                                            [isVirtual] => 0
                                            [itemId] => 134189
                                            [name] => Roma Italian Modern Designer Leather Bed
                                            [noDiscount] => 0
                                            [orderId] => 125019
                                            [originalPrice] => 555
                                            [price] => 139
                                            [priceInclTax] => 139
                                            [productId] => 322
                                            [productType] => configurable
                                            [qtyCanceled] => 0
                                            [qtyInvoiced] => 0
                                            [qtyOrdered] => 1
                                            [qtyRefunded] => 0
                                            [qtyShipped] => 0
                                            [quoteItemId] => 466843
                                            [rowInvoiced] => 0
                                            [rowTotal] => 139
                                            [rowTotalInclTax] => 139
                                            [rowWeight] => 1
                                            [sku] => 116-4FT6-BLACK
                                            [storeId] => 1
                                            [taxAmount] => 0
                                            [taxInvoiced] => 0
                                            [taxPercent] => 0
                                            [updatedAt] => 2016-10-19 11:54:09
                                            [weight] => 1
                                        )

                                )

                        )

                )

            [billingAddress] => stdClass Object
                (
                    [addressType] => billing
                    [city] => walsall
                    [countryId] => GB
                    [email] => a.nideem@live.com
                    [entityId] => 250038
                    [firstname] => Mohammad
                    [lastname] => Rabbi
                    [parentId] => 125019
                    [postcode] => ws1 4la
                    [street] => stdClass Object
                        (
                            [item] => 83 milton street
                        )

                    [telephone] => 07843164819
                )

            [payment] => stdClass Object
                (
                    [accountStatus] => 
                    [additionalInformation] => stdClass Object
                        (
                            [item] => Array
                                (
                                    [0] => Cash On Delivery
                                    [1] => 
                                )

                        )

                    [amountOrdered] => 139
                    [baseAmountOrdered] => 139
                    [baseShippingAmount] => 0
                    [ccExpYear] => 0
                    [ccLast4] => 
                    [ccSsStartMonth] => 0
                    [ccSsStartYear] => 0
                    [entityId] => 125019
                    [method] => cashondelivery
                    [parentId] => 125019
                    [shippingAmount] => 0
                )

            [statusHistories] => stdClass Object
                (
                )

            [extensionAttributes] => stdClass Object
                (
                    [shippingAssignments] => stdClass Object
                        (
                            [item] => stdClass Object
                                (
                                    [shipping] => stdClass Object
                                        (
                                            [address] => stdClass Object
                                                (
                                                    [addressType] => shipping
                                                    [city] => Walsall
                                                    [countryId] => GB
                                                    [email] => h.rabbi@live.com
                                                    [entityId] => 250039
                                                    [firstname] => joe
                                                    [lastname] => beds
                                                    [parentId] => 125020
                                                    [postcode] => WS1 3NP
                                                    [street] => stdClass Object
                                                        (
                                                            [item] => 40 Caldmore Road
                                                        )

                                                    [telephone] => 07843164819
                                                )

                                            [method] => freeshipping_freeshipping
                                            [total] => stdClass Object
                                                (
                                                    [baseShippingAmount] => 0
                                                    [baseShippingDiscountAmount] => 0
                                                    [baseShippingInclTax] => 0
                                                    [baseShippingTaxAmount] => 0
                                                    [shippingAmount] => 0
                                                    [shippingDiscountAmount] => 0
                                                    [shippingDiscountTaxCompensationAmount] => 0
                                                    [shippingInclTax] => 0
                                                    [shippingTaxAmount] => 0
                                                )

                                        )

                                    [items] => stdClass Object
                                        (
                                            [item] => Array
                                                (
                                                    [0] => stdClass Object
                                                        (
                                                            [amountRefunded] => 0
                                                            [baseAmountRefunded] => 0
                                                            [baseDiscountAmount] => 0
                                                            [baseDiscountInvoiced] => 0
                                                            [baseDiscountTaxCompensationAmount] => 0
                                                            [baseOriginalPrice] => 990
                                                            [basePrice] => 99
                                                            [basePriceInclTax] => 99
                                                            [baseRowInvoiced] => 0
                                                            [baseRowTotal] => 99
                                                            [baseRowTotalInclTax] => 99
                                                            [baseTaxAmount] => 0
                                                            [baseTaxInvoiced] => 0
                                                            [createdAt] => 2016-10-19 12:03:36
                                                            [discountAmount] => 0
                                                            [discountInvoiced] => 0
                                                            [discountPercent] => 0
                                                            [freeShipping] => 0
                                                            [discountTaxCompensationAmount] => 0
                                                            [isQtyDecimal] => 0
                                                            [isVirtual] => 0
                                                            [itemId] => 134191
                                                            [name] => Jarah Metal Bunk Bed Frame
                                                            [noDiscount] => 0
                                                            [orderId] => 125020
                                                            [originalPrice] => 990
                                                            [price] => 99
                                                            [priceInclTax] => 99
                                                            [productId] => 9947
                                                            [productType] => configurable
                                                            [qtyCanceled] => 0
                                                            [qtyInvoiced] => 0
                                                            [qtyOrdered] => 1
                                                            [qtyRefunded] => 0
                                                            [qtyShipped] => 0
                                                            [quoteItemId] => 466845
                                                            [rowInvoiced] => 0
                                                            [rowTotal] => 99
                                                            [rowTotalInclTax] => 99
                                                            [rowWeight] => 1
                                                            [sku] => B580-3FT-SLVR
                                                            [storeId] => 1
                                                            [taxAmount] => 0
                                                            [taxInvoiced] => 0
                                                            [taxPercent] => 0
                                                            [updatedAt] => 2016-10-19 12:03:36
                                                            [weight] => 1
                                                        )

                                                    [1] => stdClass Object
                                                        (
                                                            [amountRefunded] => 0
                                                            [baseAmountRefunded] => 0
                                                            [baseDiscountAmount] => 0
                                                            [baseDiscountInvoiced] => 0
                                                            [basePrice] => 0
                                                            [baseRowInvoiced] => 0
                                                            [baseRowTotal] => 0
                                                            [baseTaxAmount] => 0
                                                            [baseTaxInvoiced] => 0
                                                            [createdAt] => 2016-10-19 12:03:36
                                                            [discountAmount] => 0
                                                            [discountInvoiced] => 0
                                                            [discountPercent] => 0
                                                            [freeShipping] => 0
                                                            [isQtyDecimal] => 0
                                                            [isVirtual] => 0
                                                            [itemId] => 134192
                                                            [name] => Jarah Metal Bunk Bed Frame - Size 3FT - Colour SLVR - Mattress NOMAT
                                                            [noDiscount] => 0
                                                            [orderId] => 125020
                                                            [originalPrice] => 0
                                                            [parentItemId] => 134191
                                                            [price] => 0
                                                            [productId] => 9948
                                                            [productType] => simple
                                                            [qtyCanceled] => 0
                                                            [qtyInvoiced] => 0
                                                            [qtyOrdered] => 1
                                                            [qtyRefunded] => 0
                                                            [qtyShipped] => 0
                                                            [quoteItemId] => 466846
                                                            [rowInvoiced] => 0
                                                            [rowTotal] => 0
                                                            [rowWeight] => 0
                                                            [sku] => B580-3FT-SLVR
                                                            [storeId] => 1
                                                            [taxAmount] => 0
                                                            [taxInvoiced] => 0
                                                            [taxPercent] => 0
                                                            [updatedAt] => 2016-10-19 12:03:36
                                                            [weight] => 1
                                                            [parentItem] => stdClass Object
                                                                (
                                                                    [amountRefunded] => 0
                                                                    [baseAmountRefunded] => 0
                                                                    [baseDiscountAmount] => 0
                                                                    [baseDiscountInvoiced] => 0
                                                                    [baseDiscountTaxCompensationAmount] => 0
                                                                    [baseOriginalPrice] => 990
                                                                    [basePrice] => 99
                                                                    [basePriceInclTax] => 99
                                                                    [baseRowInvoiced] => 0
                                                                    [baseRowTotal] => 99
                                                                    [baseRowTotalInclTax] => 99
                                                                    [baseTaxAmount] => 0
                                                                    [baseTaxInvoiced] => 0
                                                                    [createdAt] => 2016-10-19 12:03:36
                                                                    [discountAmount] => 0
                                                                    [discountInvoiced] => 0
                                                                    [discountPercent] => 0
                                                                    [freeShipping] => 0
                                                                    [discountTaxCompensationAmount] => 0
                                                                    [isQtyDecimal] => 0
                                                                    [isVirtual] => 0
                                                                    [itemId] => 134191
                                                                    [name] => Jarah Metal Bunk Bed Frame
                                                                    [noDiscount] => 0
                                                                    [orderId] => 125020
                                                                    [originalPrice] => 990
                                                                    [price] => 99
                                                                    [priceInclTax] => 99
                                                                    [productId] => 9947
                                                                    [productType] => configurable
                                                                    [qtyCanceled] => 0
                                                                    [qtyInvoiced] => 0
                                                                    [qtyOrdered] => 1
                                                                    [qtyRefunded] => 0
                                                                    [qtyShipped] => 0
                                                                    [quoteItemId] => 466845
                                                                    [rowInvoiced] => 0
                                                                    [rowTotal] => 99
                                                                    [rowTotalInclTax] => 99
                                                                    [rowWeight] => 1
                                                                    [sku] => B580-3FT-SLVR
                                                                    [storeId] => 1
                                                                    [taxAmount] => 0
                                                                    [taxInvoiced] => 0
                                                                    [taxPercent] => 0
                                                                    [updatedAt] => 2016-10-19 12:03:36
                                                                    [weight] => 1
                                                                )

                                                        )

                                                )

                                        )

                                )

                        )

                )

        )
		
		*/
		
		