<?php
// @author Giomani Designs (Development Team)
//
namespace App\Helper\Import;

use Mail;
use Auth;
use App\Order;
use App\OrderNotes;
use App\Helper\Import\CsvProcessorInterface;

/**
 * @author Giomani Designs (Development Team)
 */
class BedtasticTracking implements CsvProcessorInterface
{
  /**
   * @var
   */
  public $failures;
  /**
   * @var
   */
  public $successes;
  /**
   * @author Giomani Designs (Development Team)
   */
  public function processCsv($data, $singles) {
    $ids = [];
    $rows = [];
    foreach ($data as $row) {
      $ids[] = $row['row']['order_id'];
      $rows[$row['row']['order_id']] = $row['row'];
    }
    $orders = Order::getForTracking($ids);
    $this->failures = [];
    $this->successes = [];
    foreach ($orders as $order) {
      $orderId = $order['id'];
      $row = $rows[$orderId];
      $order['bedtastic_tracking_number'] = $row['tracking_number'];
      $note = 'Added Bedtastic Tracking Number [' . $order['bedtastic_tracking_number'] . ']';
      OrderNotes::addNote($note, $orderId);
      $rows[$orderId]['result'] = $this->sendEmail($order, $order['email'], $row['name'], $singles);
    }
    foreach ($rows as $key => $row) {
      if ($row['result']) {
        $this->successes[] = ['row' => $row, 'error_messages' => 'Order ' . $key . 'SUCCESS - could not find order.'];
      } else {
        $this->failures[] = ['row' => $row, 'error_messages' => 'Order ' . $key . 'FAILED - could not find order.'];
      }
    }
    return $this;
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param         $order
   * @param string  $email
   */
  protected function sendEmail($order, $email, $name, $singles) {
    $emailParams = array_merge([
      'recipient' => $name . '(' . $email . ')',
      'trackingNumber' => $order['ajfoams_tracking_number'],
    ], $singles);
    Mail::send('emails.bedtastic.tracking', $emailParams, function ($m) use ($email, $name) {
      $m->from('no-reply@premierdeliveries.com', 'Premier Deliveries');
      $m->to($email, $name)->subject('Your furniture order has been dispatched');
    });
    return !(count(Mail::failures()) > 0);
  }
}
