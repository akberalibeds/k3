<?php
// @author Jason Fleet (jason.fleet@googlemail.com)
//
namespace App\Helper\Import;

use Auth;
use App\Order;
use App\Seller;
use App\Settings;
use App\OrderType;
use App\OrderItems;
use App\OrderStatus;
use App\Helper\Import\CsvImportAbstract;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class MightyDealsOrders extends CsvImportAbstract
{
  /**
   * @var
   */
  private $allocatedOrderStatusId;
  /**
   * @var
   */
  private $sellerName = 'mighty deals';
  /**
   * @var
   */
  private $wholesaleOrderTypeId;
  /**
   * @author Giomani Designs (Development Team)
   */
  public function __construct() {
    $allocatedOrderStatusId = OrderStatus::select('id')->where('order_status', '=', 'Allocated');
    $wholesaleOrderTypeId = OrderType::select('id')->where('order_type', '=', 'Wholesale');
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  array $row
   * @return int   the customer id
   */
  private function addCustomer($row) {
    $email = $row['email'];
    try {
      $customer = Customer::where('email1', '=', $email)->findOrFail();
    } catch (ModelNotFoundException $e) {
      $customer = new Customer;
      $addr1 = $row['house_number'] . $row['address_1'];;
      $name = $row['name_first'] . ' ' . $row['name_last'];
      $customer->businessName = $name;
      $customer->email1 = $email;
      $customer->number = $addr1;
      $customer->street = $row['address_2'];
      $customer->tel = $row['phone'];
      $customer->town = $row['city'];
      $customer->postcode = $row['postcode'];
      $customer->dBusinessName = $name;
      $customer->dNumber = $addr1;
      $customer->dStreet = $row['address_2'];
      $customer->dTown = $row['address_3'];
      $customer->dPostcode = $row['postcode'];
      // $customer->save();
    }
    return $customer->id;
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  array $item
   * @param  int $customerId
   * @return boolean false on failure
   */
  private function addItemToOrder($item, $orderId) {
    $orderItem = new OrderItem;
    $orderItem->discount = 0;
    $orderItem->costs = 0;
    $orderItem->id_staff = $session->user('id');
    $orderItem->itemid = $item->id;
    $orderItem->lineTotal = $lineTotal;
    $orderItem->oid = $orderId;
    $orderItem->price = 0;
    $orderItem->Qty = 1;
    $orderItem->staffName = Auth::user()->id;
    // $orderItem->save();

    OrderItem::addToAnOrder($orderItem->id);
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  array $row
   * @param  int $customerId
   * @return int the order id
   */
  private function addOrder($row, $customerId, $wowcherId) {
    $voucheCode = $row['wowcher_code'];
    $orderDate = \DateTime::createFromFormat('Y-m-d H:i:s e', $row['date']);
    $order = new OrderModel();
    $order->carrier = 'own';
    $order->cid = $customerId;
    $order->companyName = 'Wowcher';
    $order->deliverBy = '';
    $order->delOrder = 0;
    $order->direct = 0;
    $order->dispatchType = 'Delivery';
    $order->id_company = $wowcherId;
    $order->id_order_type = $wholesaleOrderTypeId;
    $order->id_status = $allocatedOrderStatusId;
    $order->id_seller = $wowcherId;
    $order->lastMaintainID = 0;
    $order->orderStatus = 'Allocated';
    $order->orderType = 'Wholesale';
    $order->ouref = 'Wowcher';
    $order->paid = 0;
    $order->paymentType = '';
    $order->startStamp = date('U');
    $order->staffid = Auth::user()->id;
    $order->staffName = Auth::user()->name;
    $order->startDate = $orderDate->format('d-M-Y');
    $order->startTime = $orderDate->format('H:i:s');
    $order->total = 0;
    $order->vatAmount = Settings::get('vat');
    $order->voucher_code = $row['voucher_code'];
    // $order->save();

    return $order;
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  $skuRef string the key for the sku/item code
   */
  private function getOrderItem($row) {
    $skuRef = $row['deal_id'];

    if (!isset($this->skuRefs[$skuRef])) {
      $this->failures[] = [
        'reason' => 'Could not find skuref: ' . $skuRef,
        'row' => $row,
      ];
      return false;
    }
    $skuCode = $this->skuRefs[$skuRef];

    try {
      $item = OrderItems::where('itemCode', '=', $skuCode)->firstOrFail();
    } catch(ModelNotFoundException $e) {
      $this->failures[] = [
        'reason' => 'Could not find item ' . $skuRef . ' / ' . $skuCode,
        'row' => $row,
      ];
      return false;
    }
    return $item;
  }
  /**
   * @author Giomani Designs (Development Team)
   * @return false on failure
   *         array on success
   */
  private function getSellerId() {
    try {
      $seller = Seller::where('name', '=', $this->sellerName)->firstOrFail();
    } catch(ModelNotFoundException $e) {
      $this->failures = ['reason' => 'FATAL: Could not find Seller: ' . $sellerName];
      return false;
    }
    return $seller->id;
  }
  /**
   * @see App\Http\CsvImport\CsvImportAbstract
   */
  public function processCsv($data) {
    $result = [];
    $customers = [];
    $orderIds = [];

    foreach ($data as $obj) {
      $row = $obj['row'];
      $customerId = $this->addCustomer($row);
      $orderItem = $this->getOrderItem($row);
      if ($orderItem === false) {
        continue;
      }
      $orderId = $this->addOrder($row, $customerId, $wowcherId);
      $orderIds[] = $order->id;
      // $this->addItemToOrder($orderItem, $orderId);
    }
    $result['failures'] = $this->failures;
    $result['orders'] = $orderIds;
    return $result;
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  integer $mode
   * @return $this
   */
  public function setMode($mode) {
    $this->mode = $mode;
    return $this;
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  $skurefs
   * @return vaoid
   */
  public function setSkuRefs($skuRefs) {
    $this->skuRefs = $skuRefs;
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  $data array mixed
   * @param  $prototype model|array model object(s) to be cloned
   * @return void
   */
  public function verifyCsv($data) {
    $profile = include(config('import.paths.profiles') . '/mighty-deals-orders.php');
    $this->setMode(CsvImportAbstract::MODE_IMPORT)->setColumnSpecs($profile['columns']);
    parent::verifyCsv($data);
  }
}
