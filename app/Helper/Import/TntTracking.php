<?php
// @author Giomani Designs (Development Team)
//
namespace App\Helper\Import;

use Auth;
use Mail;
use App\Order;
use App\Customer;
use App\OrderNote;
use App\Helper\Import\CsvImportAbstract;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class TntTracking extends CsvImportAbstract
{
  /**
   * @author Giomani Designs (Development Team)
   * @param string $consignmentNumber
   */
  private function addNote($consignmentNumber) {
    $datetime = new \DateTime;

    $note = new OrderNote;
    $note->date = $datetime->format('Y-m-d');
    $note->oid = $order->id;
    $note->id_staff = Auth::user()->id;
    $note->notes = 'TEST NOTE - IGNORE THIS (tnt)' . ' - ' . $consignmentNumber;
    $note->staff = Auth::user()->name;
    $note->time = $datetime->format('HH:MM');

    $note->save();
  }
  /**
   * @see App\Http\CsvImport\CsvImportAbstract
   */
  public function processCsv($data) {
    $sendEmail = $this->getParam('send_email');

    foreach ($data as $obj) {
      $row = $obj['row'];

      $orderId = (int)$row['order_id'];

      if ($orderId === 0) {
        $this->addFailure('invalid order id [oid: ' . $orderId . ']');
        continue;
      }

      try {
        $order = Order::findOrFail($orderId);
      } catch(ModelNotFoundException $e) {
        $this->addFailure('could not find order [oid: ' . $orderId . ']');
        continue;
      }

      $order->tnt_consignment = $row['tracking_number'];
      $order->save();

      if ($sendEmail) {
        $result = $this->sendEmail($order->tnt_consignment, $order->cid, $order->id);
      }
    }

    return $this;
  }

  /**
   * send the email
   *
   * @author Giomani Designs (Development Team)
   * @param string  $tntConsignmentNumber
   * @param int  $customerId
   * @param int  $orderId
   */
  protected function sendEmail($tntConsignmentNumber, $customerId, $orderId) {
    $datetime = new \DateTime;
    $dateFrom = $datetime->format('l d F Y');
    $datetime->add(new \DateInterval('P3D'));
    $dateTo = $datetime->format('l d F Y');

    try {
      $customer = Customer::findOrFail($customerId);

      $name = $customer->businessName;

      $emailParams = [
        'dateFrom' => $dateFrom,
        'recipient' => $name,
        'dateTo' => $dateTo,
        'trackingNumber' => $tntConsignmentNumber,
      ];

      $emailTo = $customer->email1;
      $emailTo = 'jfgiomaniwk@gmail.com';

      Mail::send('emails.tnt.consignment', $emailParams, function ($m) use ($emailTo, $name) {
        $m->from('no-reply@premierdeliveries.com', 'Premier Deliveries');
        $m->to($emailTo, $name)->subject('Your furniture order has been dispatched');
      });
      if (count(Mail::falures()) > 0) {
        $this->addFailure('could not send email [cid: ' . $customerId . '] [oid: ' . $orderId . ']');
      } else {
        $this->addSuccess('sent email [email: ' . $emailTo . '] [oid: ' . $orderId . ']');
      }
    } catch (ModelNotFoundException $e) {
      $this->addFailure('could not find customer [cid: ' . $customerId . '] [oid: ' . $orderId . ']');
    }
  }
}
