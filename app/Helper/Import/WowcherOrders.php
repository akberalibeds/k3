<?php
// @author Giomani Designs (Development Team)
//
namespace App\Helper\Import;

use Auth;
use App\Order;
use App\Settings;
use App\OrderItems;
use App\Helper\Import\CsvImportAbstract;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class WowcherOrders extends CsvImportAbstract
{
  /**
   * @var
   */
  private $failures = [];
  /**
   * @var
   */
  private $sellerName = 'wowcher';
  /**
   * @author Giomani Designs (Development Team)
   * @param  array $row
   * @return int   the customer id
   */
  private function addCustomer($row) {
    $customer = new Customer;
    //
    $customer->businessName = $row['name'];
    $customer->email1 = $row['email'];
    $customer->number = $row['house_number'] . $row['address_1'];
    $customer->street = $row['address_2'];
    $customer->tel = $row['phone'];
    $customer->town = $row['city'];
    $customer->postcode = $row['postcode'];
    $customer->dBusinessName = $row['name'];
    $customer->dNumber = $row['house_number'] . $row['address_1'];
    $customer->dStreet = $row['address_2'];
    $customer->dTown = $row['city'];
    $customer->dPostcode = $row['postcode'];
    //
    // $customer->save();

    return $customer->id;
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  array $item
   * @param  int $customerId
   * @return boolean false on failure
   */
  private function addItemToOrder($item, $orderId) {
    $orderItem = new OrderItem;

    $orderItem->discount = 0;
    $orderItem->costs = 0;
    $orderItem->id_staff = $session->user('id');
    $orderItem->itemid = $item->id;
    $orderItem->lineTotal = $lineTotal;
    $orderItem->oid = $orderId;
    $orderItem->price = 0;
    $orderItem->Qty = 1;
    $orderItem->staffName = Auth::user()->id;
    //
    $orderItem->save();

    OrderItem::addToAnOrder($orderItem->id);
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  array $row
   * @param  int $customerId
   * @return int the order id
   */
  private function addOrder($row, $customerId, $wowcherId) {
    $voucheCode = $row['wowcher_code'];

    $orderDate = \DateTime::createFromFormat('Y-m-d H:i:s e', $row['date']);

    $order = new OrderModel();

    $order->carrier = 'own';
    $order->cid = $customerId;
    $order->companyName = 'Wowcher';
    $order->deliverBy = '';
    $order->delOrder = 0;
    $order->direct = 0;
    $order->dispatchType = 'Delivery';
    $order->id_company = $wowcherId;
    $order->id_order_type = $lookups['order_type']['Wholesale']; // TODO get the order type
    $order->id_status = $lookups['order_statuses']['Allocated']; // TODO get the order status
    $order->id_seller = $wowcherId;
    $order->lastMaintainID = 0;
    $order->orderStatus = 'Allocated';
    $order->orderType = 'Wholesale';
    $order->ouref = 'Wowcher';
    $order->paid = 0;
    $order->paymentType = 'Wowcher';
    $order->startStamp = date('U');
    $order->staffid = Auth::user()->id;
    $order->staffName = Auth::user()->name;
    $order->startDate = $orderDate->format('d-M-Y');
    $order->startTime = $orderDate->format('H:i:s');
    $order->total = 0;
    $order->vatAmount = Settings::get('vat');
    $order->voucher_code = $row['woucher-code'];

    // $order->save();

    return $order->id;
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param $skuRef string the key for the sku/item code
   */
  private function getOrderItem($row) {
    $skuRef = $row['deal_id'];

    if (!isset($this->extras[$skuRef])) {
      $this->failures[] = [
        'reason' => 'Could not find skuref: ' . $skuRef,
        'row' => $row,
      ];
      return false;
    }
    $skuCode = $this->extras[$skuRef];

    try {
      $item = OrderItems::where('itemCode', '=', $skuCode)->firstOrFail();
    } catch(ModelNotFoundException $e) {
      $this->failures[] = [
        'reason' => 'Could not find item ' . $skuRef . ' / ' . $skuCode,
        'row' => $row,
      ];
      return false;
    }
    return $item;
  }
  /**
   * get the id for the seller
   *
   * @author Giomani Designs (Development Team)
   * @return false on failure
   *         array on success
   */
  private function getSellerId() {
    try {
      $seller = Seller::where('name', '=', $this->sellerName)->firstOrFail();
    } catch(ModelNotFoundException $e) {
      $this->failures = ['reason' => 'FATAL: Could not find Seller: ' . $sellerName];
      return false;
    }
    return $seller->id;
  }
  /**
   * @see App\Http\CsvImport\CsvImportAbstract
   */
  public function processCsv($data) {
    $result = [];
    $customers = [];
    $orderIds = [];

    $wowcherId = $this->getSellerId();

    if ($wowcherId === false) {
      return;
    }

    foreach ($data as $obj) {
      $row = $obj['row'];

      $customerId = $this->addCustomer($row);
      $orderItem = $this->getOrderItem($row);

      if ($orderItem === false) {
        continue;
      }

      $orderId = $this->addOrder($row, $customerId, $wowcherId);
      $orderIds[] = $order->id;

      $this->addItemToOrder($orderItem, $orderId);
    }
    $result['failures'] = $this->failures;
    $result['orders'] = $orderIds;

    return $result;
  }
}
