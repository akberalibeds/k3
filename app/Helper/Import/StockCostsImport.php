<?php
// @author Giomani Designs (Development Team)
//
namespace App\Helper\Import;

use Auth;
use App\Order;
use App\Settings;
use App\OrderItems;
use App\Helper\Import\CsvProcessorInterface;

class StockCostsImport implements CsvProcessorInterface
{
  /**
   * @see App\Http\CsvImport\CsvImportAbstract
   */
  public function processCsv($data) {
    $result = [];

    foreach ($data as $obj) {
      $row = $obj['row'];
      $stockItem = StockItem::where('itemCode', '=', $row['sku']);
      $stockItem->itemCode = $row['cost'];
      //
      //$stockItem->save();
    }
  }
}
