<?php
// @author Giomani Designs (Development Team)
//
namespace App\Helper\Import;

use Auth;
use App\Order;
use App\Settings;
use App\OrderItems;
use App\Helper\Import\CsvProcessorInterface;

class StockImport implements CsvProcessorInterface
{
  /**
   * @see App\Http\CsvImport\CsvImportAbstract
   */
  public function processCsv($data) {
    $result = [];

    foreach ($data as $obj) {
      $row = $obj['row'];

      $stockItem = new StockItem;
      $stockItem->itemCode = $row['sku'];
      $stockItem->itemDescription = $row['description'];
      $stockItem->itemQty = $row['quantity'];
      $stockItem->retail = $row['retail'];
      $stockItem->wholesale = $row['wholesale'];
      //
      $stockItem->save();
    }
  }
}
