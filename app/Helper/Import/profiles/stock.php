<?php

return array (
  'description' => array (
    'column_heading' => 'Item Description',
    'required' => true,
    'validation' => array (
      'not_empty' => array (
        'message' => 'description must not be empty',
      ),
    ),
  ),
  'quantity' =>  array (
    'column_heading' => 'Quantity',
    'required' => true,
    'validation' => array (
      'number' => array (
        'message' => 'quantity must be a valid whole number',
      ),
    ),
  ),
  'retail' =>  array (
    'column_heading' => 'Retail',
    'required' => true,
    'validation' => array (
      'number' => array (
        'message' => 'retail must be a valid number',
      ),
    ),
  ),
  'sku' =>  array (
    'column_heading' => 'Item Code',
    'required' =>  true,
    'validation' => array (
      'regex' => array (
        'message' => 'the sku can only be letters, &apos;-&apos; and contain no spaces',
        'expression' => '/^\w(\w|-)+$/',
      ),
    ),
  ),
  'wholesale' =>  array (
    'column_heading' => 'Wholesale',
    'required' => true,
    'validation' => array (
      'number' => array (
        'message' => 'wholesale must be a valid number',
      ),
    ),
  ),
);
