<?php

return [
  'columns' => [
    'color' => [
      'column_heading' => 'COLOR',
      'nis' => false,
      'required' => true,
      'validation' => [
        'not_empty' => [
          'message' => 'no color',
        ],
      ],
    ],
    'gross_weight' => [
      'column_heading' => 'U/ G.W',
      'nis' => false,
      'required' => true,
      'validation' => [
        'regex' => [
          'expression' => '/^[0-9]{1,}(\.[0-9]{1,})?KG$/i',
          'message' => 'no gross weight',
        ],
      ],
    ],
    'itemDescription' => [
      'column_heading' => 'DESCRIPTION',
      'nis' => false,
      'required' => true,
      'validation' => [
        'not_empty' => [
          'message' => 'no description',
        ],
      ],
    ],
    'itemCode' => [
      'column_heading' => 'ITEM CODE',
      'nis' => true,
      'required' => true,
      'validation' => [
        'not_empty' => [
          'message' => 'no item code',
        ],
      ],
    ],
    'model_no' => [
      'column_heading' => 'MODEL NO.',
      'nis' => false,
      'required' => true,
      'validation' => [
        'not_empty' => [
          'message' => 'no model number',
        ],
      ],
    ],
    'net_weight' => [
      'column_heading' => 'U/ N.W',
      'nis' => false,
      'required' => true,
      'validation' => [
        'regex' => [
          'expression' => '/^[0-9]{1,}(\.[0-9]{1,})?KG$/i',
          'message' => 'no net weight',
        ],
      ],
    ],
    'product_size' => [
      'column_heading' => 'PRODUCT SIZE',
      'nis' => false,
      'required' => true,
      'validation' => [
        'not_empty' => [
          'message' => 'no product size',
        ],
      ],
    ],
    'quantity' => [
      'column_heading' => 'QTY. (PCS)',
      'nis' => false,
      'required' => true,
      'validation' => [
        'number' => [
          'message' => 'must be a number',
        ],
      ],
    ],
  ],
  'inputs' => [
    'itemCode' => [
      'column_heading' => 'Item Code',
      'name' => 'itemCode',
    ],
  ],
];
