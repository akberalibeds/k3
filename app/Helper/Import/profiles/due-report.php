<?php

return [
  'columns' => [
    'date' => [
      'column_heading' => 'Date',
      'nis' => false,
      'required' => true,
      'validation' => [
        'string' => [
          'message' => 'a',
        ],
      ],
    ],
    'invoice-number' => [
      'column_heading' => 'Invoice Number',
      'nis' => false,
      'required' => true,
      'validation' => [
        'number' => [
          'message' => 'must be a number',
        ],
      ],
    ],
    'include-0' => [
      'column_heading' => 'Include',
      'nis' => false,
      'required' => false,
    ],
    'include-1' => [
      'column_heading' => 'Include',
      'nis' => false,
      'required' => false,
    ],
    'include-2' => [
      'column_heading' => 'Include',
      'nis' => false,
      'required' => false,
    ],
    'include-3' => [
      'column_heading' => 'Include',
      'nis' => false,
      'required' => false,
    ],
    'include-4' => [
      'column_heading' => 'Include',
      'nis' => false,
      'required' => false,
    ],
    'total' => [
      'column_heading' => 'Total',
      'nis' => false,
      'required' => true,
      'validation' => [
        'number' => [
          'message' => 'must be a number',
        ],
      ],
    ],
  ],
  'inputs' => [
  ],
];
