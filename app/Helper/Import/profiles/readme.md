CSV Importing
=============

validation
----------

- column_heading  ->  string
- required ->  boolean
- validation  
  - 'date'
    - format ->  *date format* string
    - message ->  string
  - 'email'
    - message -> string
  - 'number'
    - message ->  string
  - 'postcode'
    - message ->  string
  - 'regex'
    - 'expression' -> string
    - message ->  string
