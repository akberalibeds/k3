<?php

return [
  'columns' => [
    'cost' =>  [
      'column_heading' => 'Cost',
      'nis' => false,
      'required' => true,
      'validation' => [
        'number' => [
          'message' => 'Cost must be a valid number',
        ],
      ],
    ],
    'sku' =>  [
      'column_heading' => 'Item Code',
      'nis' => false,
      'required' =>  true,
      'validation' => [
        'regex' => [
          'message' => 'the sku can only be letters, &apos;-&apos; and contain no spaces',
          'expression' => '/^\w(\w|-)+$/',
        ],
      ],
    ],
  ],
];
