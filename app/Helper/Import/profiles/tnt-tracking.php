<?php

return array (
  'tracking_number' => array (
    'column_heading' => 'TNT Consigment Number',
    'required' => true,
    'validation' => array (
      'number' => array (
        'message' => 'must be a number',
      ),
    ),
  ),
  'date' =>  array (
    'column_heading' => 'Date',
    'required' => true,
    'validation' => array (
      'date' => array (
        'format' => 'd/m/Y',
        'message' => 'should be in the format dd/mm/yyyy',
      ),
    ),
  ),
  'name' =>  array (
    'column_heading' => 'Name',
    'required' => true,
  ),
  'postocde' =>  array (
    'column_heading' => 'Postcode',
    'required' =>  true,
    'validation' => array (
      'postcode' => array (
        'message' => 'must be a valid postcode',
      ),
    ),
  ),
  'order_id' =>  array (
    'column_heading' => 'Order Id',
    'required' => true,
    'validation' => array (
      'number' => array (
        'message' => 'must be a number',
      ),
    ),
  ),
);
