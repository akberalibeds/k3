<?php

return [
  'columns' => [
    'order_id' => [
      'column_heading' => 'Order Id',
      'nis' => false,
      'required' => true,
      'validation' => [
        'number' => [
          'message' => 'must be a valid number',
        ],
      ],
    ],
    'name' => [
      'column_heading' => 'Name',
      'nis' => false,
      'required' => true,
      'validation' => [
        'not_empty' => [
          'message' => 'ther must be a name',
        ],
      ],
    ],
    'tracking_number' => [
      'column_heading' => 'Tracking Number',
      'nis' => false,
      'required' => true,
      'validation' => [
        'not_empty' => [
          'message' => 'there must be a tracking number',
        ],
      ],
    ],
  ],
  'inputs' => [],
  'name' => 'AJ Foams Tracking',
  'singles' => [
    'delivery_date' => [
      'label' => 'Therefore we can confirm the delivery...',
      'name' => 'delivery_date',
      'type' => 'text',
    ]
  ],
];
