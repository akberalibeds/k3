<?php

return [
  'name' => 'Mighty Deals',
  'columns' => [
    'address_1' =>  [
      'column_heading' => 'Address 1',
      'nis' => false,
      'required' => true,
    ],
    'address_2' =>  [
      'column_heading' => 'Address 2',
      'nis' => false,
      'required' => true,
    ],
    'address_3' =>  [
      'column_heading' => 'Address 3',
      'nis' => false,
      'required' => false,
    ],
    'deal_sku' =>  [
      'column_heading' => 'Deal SKU',
      'nis' => false,
      'required' =>  true,
      'validation' => [
        '=' => [
          'message' => 'must be all numeric',
        ],
      ],
    ],
    'email' => [
      'column_heading' => 'Email',
      'nis' => false,
      'required' => true,
      'validation' => [
        'email' => [
          'message' => 'must be a valid email address',
        ],
      ],
    ],
    'name_first' =>  [
      'column_heading' => 'First Name',
      'nis' => false,
      'required' => true,
    ],
    'name_last' =>  [
      'column_heading' => 'Last Name',
      'nis' => false,
      'required' => true,
    ],
    'postcode' =>  [
      'column_heading' => 'Postcode',
      'nis' => false,
      'required' =>  true,
      'validation' => [
        'postcode' => [
          'message' => 'must be a valid postcode',
        ],
      ],
    ],
    'telephone' =>  [
      'column_heading' => 'Telephone',
      'nis' => false,
      'required' => true,
    ],
    'voucher_code' =>  [
      'column_heading' => 'Voucher Code',
      'nis' => false,
      'required' => true,
      'validation' => [
        'length_equal' => [
          'size' => 9,
          'message' => 'must be 9 characters long',
        ],
      ],
    ],
  ],
];
