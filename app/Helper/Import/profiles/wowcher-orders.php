<?php

return array (
  'address_1' =>  array (
    'column_heading' => 'Address 1',
    'required' => true,
  ),
  'address_2' =>  array (
    'column_heading' => 'Address 2',
    'required' => true,
  ),
  'address_3' =>  array (
    'column_heading' => 'Address 3',
    'required' => false,
  ),
  'address_4' =>  array (
    'column_heading' => 'Address 4',
    'required' => false,
  ),
  'deal_id' =>  array (
    'column_heading' => 'Deal ID',
    'required' =>  true,
    'validation' => array (
      'number' => array (
        'message' => 'must be all numeric',
      ),
    ),
  ),
  'email' => array (
    'column_heading' => 'Email',
    'required' => true,
    'validation' => array (
      'email' => array (
        'message' => 'email must be a valid email address',
      ),
    ),
  ),
  'house_number' =>  array (
    'column_heading' => 'House Number',
    'required' => false,
  ),
  'name' =>  array (
    'column_heading' => 'Name',
    'required' => true,
  ),
  'postcode' =>  array (
    'column_heading' => 'Postcode',
    'required' =>  true,
    'validation' => array (
      'postcode' => array (
        'message' => 'email must be a valid email address',
      ),
    ),
  ),
  'date' =>  array (
    'column_heading' => 'Date',
    'required' =>  true,
    'validation' => array (
      'date' => array (
        'format' => 'Y-m-d H:i:s e',
        'message' => 'must be all numeric',
      ),
    ),
  ),
  'telephone' =>  array (
    'column_heading' => 'Telephone',
    'required' => true,
  ),
  'woucher_code' =>  array (
    'column_heading' => 'Wowcher Code',
    'required' => true,
  ),
);
