<?php

return [
  'columns' => [
    'order_id' => [
      'column_heading' => 'Order Id',
      'nis' => false,
      'required' => true,
      'validation' => [
        'number' => [
          'message' => 'must be a valid number',
        ],
      ],
    ],
    /*
    'email' => [
      'column_heading' => 'Email Address',
      'nis' => false,
      'required' => true,
      'validation' => [
        'email' => [
          'message' => 'must be a valid email address',
        ],
      ],
    ],
    */
    'name' => [
      'column_heading' => 'Name',
      'nis' => false,
      'required' => true,
      'validation' => [
        'not_empty' => [
          'message' => 'there must be a name',
        ],
      ],
    ],
    'tracking_number' => [
      'column_heading' => 'Tracking Number',
      'nis' => false,
      'required' => true,
      'validation' => [
        'not_empty' => [
          'message' => 'there must be a tracking number',
        ],
      ],
    ],
  ],
  'inputs' => [],
  'name' => 'Bedtastic Tracking',
  'singles' => [
    'delivery_date' => [
      'help' => 'goes on the email to tell the reciever when the items should arrive',
      'label' => 'Therefore we can confirm the delivery...',
      'name' => 'delivery_date',
      'required' => true,
      'validation' => [
        'not_empty' => [
          'message' => 'The delivery date text is required',
        ],
      ],
      'type' => 'text',
    ]
  ],
];
