<?php
// @author Giomani Designs (Development Team)
//
namespace App\Helper\Import;

use Auth;
use Excel;
use App\Order;
use App\Settings;
use App\OrderItems;
use App\Helper\Import\CsvProcessorInterface;

class DueReport implements CsvProcessorInterface
{
  /**
   * @see App\Http\CsvImport\CsvImportAbstract
   */
  public function processCsv($data) {
    $invoiceNumbers = [];
    $rows = [];

    foreach ($data as $obj) {
      $row = $obj['row'];
      $rows[$row['invoice-number']] = $row;
      $invoiceNumbers[] = $row['invoice-number'];
    }
    $result = Order::getByInvoiceNumbers($invoiceNumbers)->toArray();

    foreach ($result as $row) {
      $rows[$row['iNo']]['orders'][] = $row;
    }

    $datetime = new \DateTime;

    $filename = 'Due Report - ' .  $datetime->format('d M Y');
    $title = $filename;

    Excel::create($title, function($excel) use($rows, $title) {
      $excel->sheet($title, function($sheet) use($rows) {
        $sheet->setOrientation('landscape');
        $sheet->setPageMargin([0.25, 0.30, 0.25, 0.30]);

        $sheetRows = [];

        foreach($rows as $key => $row) {
          $sheetRow = [];
          foreach($row as $k => $r) {
            $sheetRow[] = $r;
          }
          $orders = isset($row['orders']) ? $row['orders'] : [];
          foreach($orders as $order) {
            $sheetRows[] = array_merge($sheetRow, ['-'], $order); // [$date, $invoiceNumber, $a0, $a1, $a2, $a3, $a4, $total, $order];
          }
        }
        // dd($sheetRows);
        $sheet->fromArray($sheetRows, null, 'A1', false, false);
      });
    })->store('xlsx', storage_path('excel/exports'));

    return $title;
  }
}
