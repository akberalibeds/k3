<?php
// @author Giomani Designs (Development Team)
//
namespace App\Helper\Import;


interface CsvProcessorInterface
{
  /**
   * @author Giomani Designs (Development Team)
   * @param  array $data
   * @param  array $singles key value pairs 
   */
  public function processCsv($data, $singles);
}
