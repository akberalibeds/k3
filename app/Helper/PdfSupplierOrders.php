<?php
// @author Giomani Designs
//
// ---------========== DEPRECATED ==========---------
// @see Helper\Pdfs\PdfSupplierOrders
//
namespace App\Helper;

use App\Helper\Pdfs\Pdf;
use App\Helper\Fpdf\Barcode;

/**
 * @author Giomani Designs (Development Team)
 */
class PdfSupplierOrders extends Pdf
{
  /**
   * @var
   */
  private $heading;
  /**
   *
   */
  protected $currentSupplier;
  /**
   * @author Giomani Designs (Development Team)
   */
  public function __construct()
  {
    parent::__construct();
  }
  /**
   * @author Giomani Designs (Development Team)
   * @see    App\Helper\Fpdf\FPDF::Header()
   */
  public function Header()
  {
    $font = $this->SetFont('Arial', 'B', 30);
    $this->Cell(0, 18, $this->heading, 0, 1);
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param Datetime $date the date to go on the label
   * @param array $labels
   */
  public function pdfLoadSheets($orders, $date) {
    $this->documentTitle = 'supplier_orders_' . $date . '_';
    $this->setHeading($orders[0]['supplier_name']);
    $this->AddPage();

    $this->SetFont('Arial', '', 10);
    $liSpace = $this->GetStringWidth('99. ');

    $li = 1;
    foreach ($orders as $order) {
      if (stripos($this->heading, $order['supplier_name']) === false) {
        $this->setHeading($order['supplier_name']);
        $this->AddPage();
        $li = 1;
      }
      $items = explode(';', $order['items']);

      $this->SetFont('Arial', '', 10);

      $text = $li++ . '. ';
      $this->Cell($liSpace, 6.0, $text, 0, 0, 'R');

      $seperator = '';
      $text = '';
      foreach ($items as $itemParts) {
        $text .= $seperator;

        $item = explode(',', $itemParts);
        $text .= $item[0] . ' x ';

        if (strpos($item[1], 'MISSED PARTS') === 0 || strpos($item[1], 'SUPPLIER MISSED PARTS') === 0) {
          $text .= $item[2];
        } else {
          $text .= $item[3];
        }
        $seperator = ', ';
      }
      $this->Cell($this->GetStringWidth($text), 6.0, $text, 0, 0);

      $this->SetFont('Arial', 'B', 10);
      $this->Cell(0, 6.0, ' - ' . $order['route_name'], 0, 1);
    }

    return $this;
  }
  /**
   *
   */
  private function setHeading($heading) {
    $this->heading = strToUpper($heading);
  }
}
