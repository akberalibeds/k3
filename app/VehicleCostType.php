<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VehicleCostType extends Model
{
  /**
   * @var
   */
  public static $messages = [
    'description.required' => 'Required.',

    'type.required' => 'Required.',
    'type.max' => 'Must be less than 48 charatcers',
    'type.min' => 'Must be 4 characters or more',
    'type.unique' => 'There is already a type with that name.',
  ];
  /**
   * The table associated with the model.
   *
   * @var string
   */
  protected $table = 'vehicle_cost_types';
  /**
   * Get the validation rules that apply to the request.
   *
   * @return array
   */
  public static $rules = [
    'description' => 'required',
    'type' => 'required|min:3|max:48|unique:vehicle_cost_types,type',
  ];
  /**
   * Get height attributece .
   *
   * @param  string  $value
   * @return string
   */
  public static function getIdTypes() {
    return static::select('id', 'type')->orderBy('type', 'desc')->get();
  }
}
