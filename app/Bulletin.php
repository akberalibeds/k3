<?php

namespace App;

use DB;
use Auth;
use Debugbar;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Bulletin extends Model
{
  use SoftDeletes;
  /**
   * @var
   */
  public static $rules = [
    'create' => [
        'expirey' => 'date_format:format',
        'message' => 'required',
        'title' => 'required|max:255',
    ],
  ];
  /**
   * @return App/bulletin
   */
  public static function getAll()
  {
    return Bulletin::all();
  }
  /**
   * Relationship
   */
  public function user()
  {
    return $this->belongsTo('App\User', 'user_id', 'id');
  }
  /**
   * Get the bulletins that this user.
   *
   * @param int $id user id
   */
  public static function getForUser($id)
  {
    return static::all();
  }
  /**
   * Get the bulletins that this user has not seen yet.
   *
   * @param int $id user id
   */
  public static function getUnseenForUser($id)
  {
    return static::select(
      'id',
      'message',
      'title'
    )
    ->whereNotIn('id', function ($query) {
      $query->select(DB::raw('bulletin_id from user_read_bulletins where user_id = ' . Auth::user()->id));
    })
    ->get();
  }
}
