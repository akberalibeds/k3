<?php

namespace App;

use App\OrderAuthorisation;
use App\OrderNotes;
use App\Coordinates;
use App\Supplier;
use App\Routes;
use App\OrderStatus;
use App\OrderItems;
use App\Codes;
use App\Links;
use App\Customer;
use App\StockItem;
use App\Dispatched;
use App\Mailer;
use DB;
use App\Helper\Traits\QueryableModel;
use Illuminate\Database\Eloquent\Model;
use Auth;
use App\Direct;
use App\Helper\Invoice;
use Time;
use Illuminate\Http\Request;
use App\Helper\SMS;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Response;
use phpDocumentor\Reflection\Types\Float_;

class Order extends Model
{
  use QueryableModel;

  public function route()
  {
    return $this->belongsTo(Routes::class, 'id_delivery_route');
  }

  public function customer()
  {
    return $this->belongsTo(Customer::class, 'cid');
  }

  public function authorisations()
  {
    return $this->hasMany(OrderAuthorisation::class, 'order_id');
  }

  public function company()
  {
    return $this->belongsTo(Company::class, 'id_company');
  }

  public function parentOrder()
  {
    return $this->belongsTo(Order::class, 'parent_order_id');
  }

  public function childOrders()
  {
    return $this->hasMany(Order::class, 'parent_order_id');
  }

  public function notifications() {
    return $this->hasMany(\App\Notification::class, 'orderId');
  }

  /**
   * The table associated with the model.
   *
   * @var string
   */
  protected $table = 'orders';

  public $fillable = ['id_company', 'id_status', 'cid', 'delRoute', 'endDate', 'endTime', 'lastMaintainer', 'lastMaintainID', 'orderStatus', 'paid', 'startDate', 'startTime', 'staffName', 'staffid', 'total', 'dispatchType', 'collector', 'colDelDate', 'carrier', 'orderType', 'direct', 'paymentType', 'delOrder', 'authNo', 'cardNo', 'exp', 'companyName', 'ouref', 'time_stamp', 'proforma', 'showVat', 'showNotes', 'serType', 'icType', 'iNo', 'ebayID', 'vatAmount', 'zeroVat', 'ebayStatus', 'deliverBy', 'tx', 'delTime', 'delStat', 'agent', 'agentPaid', 'agentOwed', 'rate', 'txt', 'code', 'parcelid', 'neighbour', 'aftersales', 'safePlace', 'done', 'ebay', 'notLoaded', 'p', 'wes', 'cStatus', 'colOrder', 'colTime', 'colRoute', 'extra', 'beds', 'delDate', 'colID', 'delID', 'lat', 'lng', 'amazonID', 'amzShipped', 'refund', 'postLoc', 'post', 'sender', 'stamp', 'startStamp', 'wholeError', 'mfError', 'feedbackScore', 'delissue', 'defects', 'g_id', 'g_num', 'g_desc', 'g_ship', 'oos', 'mgmt', 'processed', 'paidBeds', 'pTime', 'refundBox', 'id_delivery_route', 'id_order_type', 'id_payment_type', 'hold', 'missed', 'urgent', 'retention', 'paypal', 'reopen', 'id_supplier', 'paid_supplier', 'invoice_made', 'id_seller', 'seller_paid', 'discount_amount', 'discount_rate', 'vat_paid', 'paypal_payment', 'last_trans_id', 'to_collect', 'is_split', 'voucher_code', 'is_shipped', 'paid_courier', 'tnt_consignment', 'bedtatstic_tracking_number', 'is_collected', 'escalate', 'ordered', 'merch_ref', 'tuffnells','manual_order'];

  public $timestamps = false;

  public function links()
  {
    return $this->hasMany(Links::class, 'oid');
  }

  public function items()
  {
    return $this->hasMany(OrderItems::class, 'oid');
  }

  public static function pickle () {
    return ['a', 'b'];
  }
  /*
   *
   */
  public static function addpayment($data,$id)
  {
    $order = Order::find($id);
    $order->paid += $data['paid'];
    $order->save();

    $payment = new OrderPayment;
    $payment->stamp = Time::date()->getStamp();
    $payment->paid = $data['paid'];
    $payment->cardNo = substr($data['cardNo'],-4);
    $payment->exp = $data['exp'];
    $payment->auth = $data['auth'];
    $payment->oid = $id;
    $payment->type = $data['pt'];
    $payment->staff = Auth::user()->email;
    $payment->save();

    $payment->stamp = Time::date($payment->stamp)->get('d-M-Y');
    return json_encode($payment);
  }


  /*
   * $order = array [new=> status string, old => status string]
   * $id = order id
   */
  public static function changestatus($order,$id) {
    $new = $order['new'];
    $old =  $order['old'];

    Order::where(['id' => $id])->update(['orderStatus' => $new , 'id_status' => OrderStatus::id_status($new)]);
    OrderItems::where(['oid' => $id])->update(['itemStatus' => $new]);

    Links::updateLink($id, '', $new, '', $old);

    if (($new == "Allocated" || $new == "Routed") && ($old == "Dispatched" || $old == "Delivered" || $old == "Not Delivered")) {
      //put items back in stock
      Links::updateLink($id, '', $new, '', $old);

      if (isset($_REQUEST['stock'])) {
        if ($_REQUEST['stock'] == 1) {
          \DB::query("update stock_items join orderItems on stock_items.id=orderItems.itemid set actual=actual+Qty, qty_on_so=qty_on_so+Qty where oid=$id");
        }
      }
      Dispatched::where(['oid' => $id])->delete();
    }

    if ($new == "Allocated" && $old == "Routed") {
      Links::updateLink($id, '', 'Routed For Delivery', '', $old);
      Dispatched::where(['oid' => $id])->delete();
    }

    if ($new == "Refunded") {
      Links::updateLink($id, '', 'Cancelled', '', $old);
      // Dispatched::where(['oid' => $id])->delete();
    }

    if ($new == "Not Delivered") {
      Links::updateLink($id, '', 'Not Delivered', '', $old);
    }

    if ($new == "Allocated") {
      Links::updateLink($id, '', 'Awaiting Dispatch', '', $old);
    }

    if ($new == "Routed" && $old == "Allocated") {
      //routed ready for dispatch

      Links::updateLink($id, '', 'Awaiting Dispatch', '', $old);
    }

    if (($new == "Dispatched" || $new == "Delivered") && ($old == "Routed" || $old == "Allocated")) {
      Links::updateLink($id, '', $new, '', $old);
      Order::where(['id' => $id])->update(['stamp' => Time::date()->getStamp(), 'endDate' => Time::date()->get('d-M-Y')]);
	  \DB::query("update stock_items join orderItems on stock_items.id=orderItems.itemid set actual=actual-Qty , qty_on_so=qty_on_so-Qty where oid=$id");
    }

    if ($new == "Delivered" && $old == "Dispatched") {
      Links::updateLink($id, '', $new, '', $old);
      Order::where(['id' => $id])->update(['stamp' => Time::date()->getStamp(), 'endDate' => Time::date()->get('d-M-Y')]);
    }

    if ($new == 'Allocated' &&  in_array($old, ['Routed', 'Delivered', 'Dispatched'])) {
      static::setDeliveryRoute($id);
    }

    OrderNotes::addNote("Order status changed to {$new}",$id);
  }

  /*
   * $orders - array/collection of orders
   * $status - string
   */
  public static function changestatus_bulk($orders, $status) {
    foreach ($orders as $order) {
      if ($order instanceof Order) {
        $statechange = ['new' => $status, 'old' => $order->orderStatus];
        static::changestatus($statechange, $order->id);
      }
    }
  }

  /**
   *
   */
  public static function changeroute($data,$id)
  {
    Order::where(['id' => $id])->update($data);
  }

  /**
   *
   */
  public static function changeroute_mass($data)
  {
    $route = Routes::find($data['route']);
    Order::whereIn('id', $data['orders'])->update(['id_delivery_route' => $data['route'], 'delRoute' => $route->name]);
  }
  /**
   *
   */
  public static function order ($id) {
    $order = Order::where(['id' => $id])->first();

    if (!$order) {
      return false;
    }

    $order['customer'] = Customer::find($order->cid);
    $email = (strtolower($order->orderType) == "wholesale") ? "Giomani Designs" : $order->companyName;
    $mailer = Mailer::getEmail($email);

    $order['mailer_account'] = isset($mailer) ? $mailer->email : '';
    $statuses = OrderStatus::where('show_status', '=', 1)->get();
    $routes = Routes::all();
    $suppliers = Supplier::getSupplierList();
    $paymentTypes = PaymentType::getAll();

    $codes = Codes::where('oid', '=', $order->id)->get();
    $payments = OrderPayment::where('oid', '=', $order->id)->get();

    $order['VAT'] = round($order->total - ((100 / (100 + $order->vatAmount) * $order->total)),2);
    $order['subTotal'] = round($order->total - $order['VAT'], 2);
    $order['total']= round($order['total'], 2);
    $order['paid']= round($order['paid'], 2);
    $order['orderItems'] = OrderItems::onOrder($order->id);
    //\DB::select(\DB::raw("select i.id,s.itemCode,s.itemDescription,i.Qty,i.price,i.lineTotal,i.notes from orderItems i join stock_items s on i.itemid=s.id where i.oid=".$order->id));
    $order['orderNotes'] = OrderNotes::where('oid','=',$order->id)->where('notes','not like','%linked%')->orderBy('id', 'desc')->get();
    $order['linkNotes'] = OrderNotes::where('oid','=',$order->id)->where('notes','like',"%linked%")->orderBy('id', 'desc')->get();
    $order['stockCount'] = \DB::select(\DB::raw("select group_concat(itemQty) as stockCount from orderItems i join stock_items s on i.itemid=s.id where i.oid=".$order->id));
    $order['stockCount'] = Order::getStock($order['stockCount'][0]->stockCount);

    $visits = PageVisits::add($order['id'])->getVisits($order['id']);

    return array('order'=> $order,'status'=>$statuses,'routes'=>$routes,'suppliers'=>$suppliers,'codes'=>$codes,'paymentType'=>$paymentTypes,'payments' => $payments, 'visits' => $visits );
  }
  /**
   * @param  int  $id
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public static function updateOrder($data, $id) {
    Order::where(['id' => $id])->update($data);
  }


  /*
   * Search
   */
  public static function search2($request){
  	$search = false;
  	$orders = array();
  	$rows = array();
  	$direct = false;
  	$data = array();

  	$ORDER = self::where('id_status','!=',1);

  	if($request->has('search')){
  		$data= json_decode($request->get('search'),true);
  		foreach($data as $field=>$val){

  			if($field=='direct'){

  				$ORDER->where('direct.name' , 'like', "%$val%")
  						->orWhere('direct.postcode' , 'like', "%$val%");
  				$direct = true;
  				break;
  			}
  			if($field=="c.dPostcode" || $field=="c.postcode"){

  				$ORDER->where(\DB::raw("replace($field, ' ', '')") , 'like', \DB::raw("concat('%',replace('$val', ' ', ''),'%')"));

  			}
  			else{
  				$ORDER->where($field,'like',"%$val%");

  			}
  		}

  		$search = true;

  	}


  	$ORDER->select(
  			\DB::raw('group_concat(actual) as stockCount'),
  			'orders.id',
  			'orders.direct',
  			'iNo',
  			'c.userID',
  			'ouref',
  			'orderType',
  			'orderStatus',
  			'startDate',
  			'total',
  			\DB::raw('total-paid as owed'),
  			\DB::raw("REPLACE(REPLACE(if(orders.direct=1,d.name,dBusinessName),'\n',''), '\n', '') as name"),
  			\DB::raw("if(orders.direct=1,d.postcode,dPostcode) as postcode")
  			)
  			->join('customers as c','orders.cid','=','c.id')
  			->join('orderItems as i' ,'orders.id','=','i.oid')
  			->leftJoin('stock_items as s','i.itemid','=','s.id')
  			->leftJoin('direct as d','orders.id','=','d.oid')
  			->groupBy('orders.id')
  			->orderBy('orders.id','desc');

  			$count = $ORDER->get()->count();

  	$page = $request->has('page_num') ? $request->get('page_num') : 1;
  	$per_page =  $request->has('per_page') ? $request->get('per_page') : 15;
  	$start_limit = ($page == 1) ? $page : $page*$per_page-$per_page+1;

  	$rows = $ORDER->limit($start_limit-1,$per_page)->get()->toArray();

  	$searcher = '';//($search) ? '&search='.json_encode($_REQUEST['search']) : '';

  	$pagination  = '<div class="pull-right"><ul class="pagination">';
  	$pagination.= ($page-1>0) ? "<li><a class='pageClick' ref='".($page-1).$searcher."'>&laquo; Previous</a></li>" : '';
  	$pagination.="<li><a class='pageClick' ref='".($page+1).$searcher."'>Next &raquo;</a></li>";
  	$pagination.='</ul></div>'." Showing ".($start_limit)."-".(($page*$per_page>$count) ? $count : $page*$per_page)." of $count";

  	return response(["orders"=>$rows,"pages"=>$pagination,"search"=>$data]);

  }
  /*
	 * Search
	 */
	public static function search(){
			$search = false;
			$orders = array();
			$rows = array();
			$direct = false;
			$data = array();

			//$ORDER = self::where('id_status','!=',1);

			$where=" where id_status!='1' and";
			if(isset($_REQUEST['search'])){
				$data= json_decode($_REQUEST['search'],true);
				foreach($data as $field=>$val){

					if($field=='direct'){

						$where = " where d.name like '%$val%' or d.postcode like '%$val%' ";
						$direct = true;
						break;
					}
					if($field=="c.dPostcode"){
						//$where.=" replace($field, ' ', '') like concat('%',replace('$val', ' ', ''),'%') and";
						$where.=" numerical_pc = '".Customer::getNumericalValue($val)."' and";
					}else if ($field=='c.userID') {
                                          $where .= " c.userID like '$val%' and" ;
                                        }else if ($field=='o.id') {
                                          $val = preg_replace('/[^0-9]/','',$val);
                                          $where .= " o.id = '$val' and";
                                        }
					else{
						$where.=" $field like '%$val%' and"	;
					}
				}

			$search = true;

			}

			$where=rtrim($where,"and");

			$SQL = ($direct) ? "select * from direct d join orders o on d.oid=o.id $where" : "select count(*) as counts from orders o join customers c on o.cid=c.id $where";

			$counts = \DB::select(\DB::raw($SQL));
			$count = ($counts[0]->counts);

			$page = (isset($_REQUEST['page_num'])) ? $_REQUEST['page_num'] : 1;
			$per_page =  (isset($_REQUEST['per_page'])) ? $_REQUEST['per_page'] : 15;
			$start_limit = ($page-1)*$per_page;

			$SQL = ($direct) ? "select oid as id from direct d $where order by oid desc limit $start_limit,$per_page" : "select o.id from orders o join customers c on o.cid=c.id $where order by id desc limit $start_limit,$per_page";

			$result = \DB::select(\DB::raw($SQL));
			foreach($result as $r){
				$orders[]=$r->id;
			}

			$orders = implode(",",$orders);

			$SQL = "select group_concat(actual) as stockCount ,o.id as id,o.direct,iNo,c.userID,o.ouref,o.orderType,o.orderStatus,o.startDate,o.total,o.total-o.paid as owed,o.done, REPLACE(REPLACE(if(o.direct=1,d.name,dBusinessName),'\n',''), '\n', '') as name, if(o.direct=1,d.postcode,dPostcode) as postcode, priority from orders o join orderItems i on o.id=i.oid join customers c on o.cid=c.id left join direct d on o.id=d.oid left join stock_items s on i.itemid=s.id where o.id in ($orders) group by o.id order by o.id desc";


			$rows = ($count>0) ? \DB::select(\DB::raw($SQL)) : '';

			$searcher = '';//($search) ? '&search='.json_encode($_REQUEST['search']) : '';

			$pagination  = '<div class="pull-right"><ul class="pagination">';
			$pagination.= ($page-1>0) ? "<li><a class='pageClick' ref='".($page-1).$searcher."'>&laquo; Previous</a></li>" : '';
			$pagination.="<li><a class='pageClick' ref='".($page+1).$searcher."'>Next &raquo;</a></li>";
			$pagination.='</ul></div>'." Showing ".($start_limit+1)."-".(($page*$per_page>$count) ? $count : $page*$per_page)." of $count";

			return array("orders"=>$rows,"pages"=>$pagination,"search"=>$data);

	}



	public static function cancelOrder($id=false){



		$items = OrderItems::where(['oid' => $id])
				->join('stock_items','orderItems.itemid','=','stock_items.id')
				->select('itemid','Qty','itemCode','orderItems.id')
				->get();

		foreach($items as $item){


			StockItem::removeFromOrder($item->itemid,$item->Qty);

			$cancelled = StockItem::itemCode("Cancelled",true);
			$newItem = new OrderItems;
			$newItem->itemid = $cancelled->id;
			$newItem->oid = $id;
			$newItem->notes=$item->itemCode . " x".$item->Qty;
			$newItem->currStock=0;
			$newItem->itemStatus="Cancelled";
			$newItem->lineTotal = 0;
			$newItem->price = 0;
			$newItem->Qty=1;
			$newItem->staffName= Auth::user()->email;
			$newItem->save();

			OrderItems::where(['id' => $item->id])->delete();
		}




		$order = Order::find($id);
		$order->orderStatus = "Cancelled";
		$order->id_status = OrderStatus::id_status("Cancelled");
		$order->total=0;
		$order->paid=0;
		$order->save();

		//CancelledOrder::insert($row);

		//Order::where(['id' => $id])->delete();
		OrderNotes::addNote("Cancelled Order",$id);
		Codes::where(['oid' => $id])->delete();
		Links::where(['oid' => $id])->delete();

	}

	public static function getStock($row) {



		//$res = mysqli_query($link,"select group_concat(actual) as i from orderItems join stock_items on orderItems.itemid=stock_items.id where oid=$id)");
		//$row = mysqli_fetch_assoc($res);
		 $str = explode(",",$row);
		 $r=0;
		 $g=0;
		for($zz=0;$zz<count($str);$zz++){
				if($str[$zz]>0){ $g++; }
				if($str[$zz]<1){ $r++;	}
		}

		if($r>0 && $g>0)		{ return 'warning'; }
		else if($r>0)		{ return 'danger';  }
		else if($g>0)		{ return 'success'; }
		else				{ return 'danger';  }

}
  /**
   * @author Giomani Designs
   * @param  array   $queryStrings the query string
   * @return Illuminate\Pagination\LengthAwarePaginator
   */
  public static function getAfterSales ($queryStrings) {
    $query = static::queryForAnOrder();
    if (count($queryStrings) > 0) {
      $query = parent::modForQueryString($query, $queryStrings);
    }
    $query->where('orders.aftersales', '=', '1');
    return $query->orderBy('orders.id', 'desc')->paginate(config('search.rpp'));
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  array  $ids collection of invoice numbers
   * @return Illuminate\Pagination\LengthAwarePaginator
   */
  public static function getByInvoiceNumbers($ids) {
    return static::queryForByInvoiceNumber()
      ->orderBy('orders.id', 'desc')
      ->whereIn('iNo', $ids)
      ->get();
  }
  /**
   * @author Giomani Designs (Development Team)
   * @return Illuminate\Pagination\LengthAwarePaginator
   */
  public static function getCod($date) {
    return static::queryForCod()
      ->orderBy('orders.id', 'desc')
      ->where('disp.date', '=', $date)
      ->get();
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  array   $queryStrings the query string
   * @return Illuminate\Pagination\LengthAwarePaginator
   */
  public static function getCollect($queryStrings) {
    $query = static::queryForCollect();
    if (count($queryStrings) > 0) {
      $query = parent::modForQueryString($query, $queryStrings);
    }
    return $query->orderBy('orders.id', 'desc')->paginate(config('search.rpp'));
  }
  /**
   * get collected orders
   * @author Giomani Designs (Development Team)
   * @param  array   $queryStrings the query string
   * @return Illuminate\Pagination\LengthAwarePaginator
   */
  public static function getCollected($queryStrings) {
    $query = static::queryForCollected();
    if (count($queryStrings) > 0) {
      $query = parent::modForQueryString($query, $queryStrings);
    }
    return $query->orderBy('orders.id', 'desc')->paginate(config('search.rpp'));
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  string $date
   * @param  string $routeName
   * @return Illuminate\Pagination\LengthAwarePaginator
   */
  public static function getDispatchedByRoute($date, $routeName) {
    return static::queryForDispatched()
      ->where('disp.date', '=', $date)
      ->where('disp.route', '=', $routeName)
      ->get();
  }
  /**
   * get orders by date and route
   * used to get the orders for a route, for the labels
   *
   * @author Giomani Designs (Development Team)
   * @param  \Datetime $date
   * @param  string $routeName
   * @return Illuminate\Pagination\LengthAwarePaginator
   */
  public static function getDispatchedNotesByRoute($date, $routeName) {
    return static::queryForDispatchedNotes()
      ->where('disp.date', '=', (is_string($date)) ? $date : $date->format('d-M-Y'))
      ->where('disp.route', '=', $routeName)
      ->orderBy('delOrder', 'asc')
      ->get();
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  array   $queryStrings the query string
   * @return Illuminate\Pagination\LengthAwarePaginator
   */
  public static function getEod($queryStrings) {
    $query = static::queryForEod();
    if (count($queryStrings) > 0) {
      $query = parent::modForQueryString($query, $queryStrings);
    }
    return $query->paginate(config('search.rpp'));
  }
  /**
   * get collected orders
   * @author Giomani Designs (Development Team)
   * @param  array   $queryStrings the query string
   * @return Illuminate\Pagination\LengthAwarePaginator
   */
  public static function getEscalations($queryStrings) {
    $query = static::queryForEscalations();
    if (count($queryStrings) > 0) {
      $query = parent::modForQueryString($query, $queryStrings);
    }
    return $query->orderBy('orders.id', 'desc')->paginate(config('search.rpp'));
  }
  /**
   * get feedback orders
   * @author Giomani Designs (Development Team)
   * @param  array   $queryStrings the query string
   * @return Illuminate\Pagination\LengthAwarePaginator
   */
  public static function getFeedback($queryStrings) {
    $query = static::queryForFeedback();
    if (count($queryStrings) > 0) {
      $query = parent::modForQueryString($query, $queryStrings);
    }
    return $query->orderBy('orders.id', 'desc')->paginate(config('search.rpp'));
  }
  /**
   * get feedback orders
   * @author Giomani Designs (Development Team)
   * @param  array $ids the ids of the orders the invoices are for
   * @return Illuminate\Database\Eloquent\Collection
   */
  public static function getForInvoice($ids) {
    return static::queryForDispatchedNotes()
      ->whereIn('orders.id', $ids)
      ->get();
  }
  /**
   * get feedback orders
   * @author Giomani Designs (Development Team)
   * @param  string $date date in the format dd-mmm-yyyy
   * @return Illuminate\Database\Eloquent\Collection
   */
  public static function getForPrintSupplierOnDispatch($date) {
    return static::queryForPrintSupplierOnDispatch()
      ->where('disp.date', '=', $date)
      ->get();
  }
  /**
   * @author Giomani Designs
   * @param  array   $queryStrings the query string
   */
  public static function getForTracking ($ids) {
    return static::queryForTracking($ids)
      ->whereIn('orders.id', $ids)
      ->orderBy('orders.id', 'desc')
      ->get();
  }
  /**
   * get order labels by ids
   * @author Giomani Designs (Development Team)
   * @param  array|int $ids TODO: make it work for'int'
   * @return Illuminate\Database\Eloquent\Collection
   */
  public static function getLabelsByIds($ids) {
    DB::statement('SET SESSION group_concat_max_len = 4096');
    return static::queryForLabels()
      ->whereIn('orders.id', $ids)
      ->get();
  }
  /**
   * get not-loaded orders
   * @author Giomani Designs (Development Team)
   * @param  array   $queryStrings the query string
   * @return Illuminate\Pagination\LengthAwarePaginator
   */
  public static function getNotLoaded($queryStrings) {
    $query = static::queryForNotLoaded();
    if (count($queryStrings) > 0) {
      $query = parent::modForQueryString($query, $queryStrings);
    }
    return $query->orderBy('orders.id', 'desc')->paginate(config('search.rpp'));
  }
  /**
   * get del-issue orders
   * @author Giomani Designs (Development Team)
   * @param  array   $queryStrings the query string
   * @return Illuminate\Pagination\LengthAwarePaginator
   */
  public static function getDelIssue($queryStrings) {
  	$query = static::queryForDelIssue();
  	if (count($queryStrings) > 0) {
  		$query = parent::modForQueryString($query, $queryStrings);
  	}
  	return $query->orderBy('orders.id', 'desc')->paginate(config('search.rpp'));
  }
  /**
   * @param  integer     $id the order id
   */
  public static function getOrder ($id) {
    $query = static::queryForOrder();
    try {
      $order = static::findOrFail($id);
    } catch (ModelNotFoundException $e) {
      return false;
    }

    $order['customer'] = Customer::find($order->cid);
    $email = (strtolower($order->orderType) == "wholesale") ? "Giomani Designs" : $order->companyName;
    $mailer = Mailer::getEmail($email);

    $order['mailer_account'] = isset($mailer) ? $mailer->email : '';
    $statuses = OrderStatus::where('order_status', '!=', 'Open')->get();
    $routes = Routes::all();
    $suppliers = Supplier::getSupplierList();
    $paymentTypes = PaymentType::getAll();

    $codes = Codes::where('oid', '=', $order->id)->get();
    $payments = OrderPayment::where('oid', '=', $order->id)->get();

    $order['VAT'] = round($order->total - ((100 / (100 + $order->vatAmount) * $order->total)),2);
    $order['subTotal'] = round($order->total - $order['VAT'], 2);
    $order['total']= round($order['total'], 2);
    $order['paid']= round($order['paid'], 2);
    $order['orderItems'] = OrderItems::onOrder($order->id);
    //\DB::select(\DB::raw("select i.id,s.itemCode,s.itemDescription,i.Qty,i.price,i.lineTotal,i.notes from orderItems i join stock_items s on i.itemid=s.id where i.oid=".$order->id));
    $order['orderNotes'] = OrderNotes::where('oid','=',$order->id)->orderBy('id', 'desc')->get();
    //$order['links'] =
    $order['stockCount'] = \DB::select(\DB::raw("select group_concat(itemQty) as stockCount from orderItems i join stock_items s on i.itemid=s.id where i.oid=".$order->id));
    $order['stockCount'] = Order::getStock($order['stockCount'][0]->stockCount);

    $visits = PageVisits::add($order['id'])->getVisits($order['id']);

    return array('order'=> $order,'status'=>$statuses,'routes'=>$routes,'suppliers'=>$suppliers,'codes'=>$codes,'paymentType'=>$paymentTypes,'payments' => $payments, 'visits' => $visits );
  }
  /**
   * get on-hold orders
   * @author Giomani Designs (Development Team)
   * @param  array   $queryStrings the query string
   * @return Illuminate\Pagination\LengthAwarePaginator
   */
  public static function getOnHold($queryStrings) {
    $query = static::queryForOnHold();
    if (count($queryStrings) > 0) {
      $query = parent::modForQueryString($query, $queryStrings);
    }
    return $query->orderBy('orders.id', 'desc')->paginate(config('search.rpp'));
  }
  /**
   * get paypal orders
   * @author Giomani Designs (Development Team)
   * @param  array   $queryStrings the query string
   * @return Illuminate\Pagination\LengthAwarePaginator
   */
  public static function getPayPal($queryStrings) {
    $query = static::queryForPayPal();
    if (count($queryStrings) > 0) {
      $query = parent::modForQueryString($query, $queryStrings);
    }
    return $query->orderBy('orders.id', 'desc')->paginate(config('search.rpp'));
  }
  /**
   * get after-sales orders
   * @author Giomani Designs (Development Team)
   * @param  array   $queryStrings the query string
   * @return Illuminate\Pagination\LengthAwarePaginator
   */
  public static function getPost($queryStrings) {
    $query = static::queryForPost($queryStrings);
    if (count($queryStrings) > 0) {
      $query = parent::modForQueryString($query, $queryStrings);
    }
    return $query->orderBy('orders.id', 'desc')->paginate(config('search.rpp'));
  }
  /**
   * get refunds orders
   * @author Giomani Designs (Development Team)
   * @param  array   $queryStrings the query string
   * @return Illuminate\Pagination\LengthAwarePaginator
   */
  public static function getRefunds($queryStrings) {
    $query = static::queryForRefunds($queryStrings);
    if (count($queryStrings) > 0) {
      $query = parent::modForQueryString($query, $queryStrings);
    }
    return $query->orderBy('orders.id', 'desc')->paginate(config('search.rpp'));
  }
  /**
   * get retention orders
   * @author Giomani Designs (Development Team)
   * @param  array   $queryStrings the query string
   * @return Illuminate\Pagination\LengthAwarePaginator
   */
  public static function getRetention($queryStrings) {
    $query = static::queryForRetention($queryStrings);
    if (count($queryStrings) > 0) {
      $query = parent::modForQueryString($query, $queryStrings);
    }
    return $query->orderBy('orders.id', 'desc')->paginate(config('search.rpp'));
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  string $id   route id
   * @return Illuminate\Pagination\LengthAwarePaginator
   */
  public static function getRoute($id) {
    return static::queryForRoute($id)
      ->orderBy('id', 'desc')
      ->paginate(config('search.rpp'));
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  integer $id           the id of the seller
   * @param  array   $queryStrings the query string
   * @return Illuminate\Pagination\LengthAwarePaginator
   */
  public static function getSeller($id, $queryStrings) {
    $query = static::queryForSeller($id);
    if (count($queryStrings) > 0) {
      $query = parent::modForQueryString($query, $queryStrings);
    }
    return $query->paginate(config('search.rpp'));
  }
  /**
   * @author Giomani Designs (Development Team)
   * @return Illuminate\Database\Eloquent\Collection
   */
  public static function getSellersPaid() {
    return static::queryForSellers()
      ->where('orders.seller_paid', '=', '1')
      ->get();
  }
  /**
   * @author Giomani Designs (Development Team)
   * @return Illuminate\Database\Eloquent\Collection
   */
  public static function getSellersUnpaid() {
    return static::queryForSellers()
      ->where('orders.seller_paid', '=', '0')
      ->get();
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  integer $id           the wholesaler id
   * @param  array   $queryStrings the query string
   * @return Illuminate\Pagination\LengthAwarePaginator
   */
  public static function getSupplier($id, $queryStrings) {
    $query = static::queryForSupplier($id);
    if (count($queryStrings) > 0) {
      $query = parent::modForQueryString($query, $queryStrings);
    }
    return $query->orderBy('orders.id', 'desc')->paginate($queryStrings['limit']);
  }
  /**
   * @author Giomani Designs (Development Team)
   * @return Illuminate\Pagination\LengthAwarePaginator
   */
  public static function getSupplierInvoiced($id) {
    return static::queryForSupplierInvoiced($id)
      ->orderBy('orders.id', 'desc')
      ->paginate(config('search.rpp'));
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  array  $ids  the order ids
   * @return Illuminate\Database\Eloquent\Collection
   */
  public static function getSupplierInvoicedCsv($ids) {
    return static::queryForSupplierInvoiced($ids)
      ->orderBy('orders.id', 'desc')
	  //->whereNotIn('orderStatus',['routed','dispatched','delivered'])
      ->get();
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  int $id
   * @param  array $oids
   * @return Illuminate\Database\Eloquent\Collection
   */
  public static function getSupplierOrdersByIds($id, $oids) {
    return static::queryForSupplier($id)
      ->whereIn('orders.id', $oids)
      ->orderBy('orders.id', 'desc')
      ->get();
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param int $id supplier id
   * @param Date $date
   * @return Illuminate\Database\Eloquent\Collection
   */
  public static function getSupplierLabels($id, $date) {
    return static::queryForLabels()
      ->whereIn('id_status', [2, 3])
      ->where('orders.id_supplier', '=', $id)
      ->where('orders.deliverBy', '=', $date)
      ->get();
  }
  /**
   * @author Giomani Designs
   * @param  array   $queryStrings the query string
   * @return Illuminate\Pagination\LengthAwarePaginator
   */
  public static function getTuffnells ($queryStrings) {
    $query = static::queryForAnOrder();
    if (count($queryStrings) > 0) {
      $query = parent::modForQueryString($query, $queryStrings);
    }
    $query->where('orders.tuffnells', '=', '1');
    return $query->orderBy('orders.id', 'desc')->paginate(config('search.rpp'));
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  array   $queryStrings the query string
   * @return Illuminate\Pagination\LengthAwarePaginator
   */
  public static function getUrgent($queryStrings) {
    $query = static::queryForUrgent($queryStrings);
    if (count($queryStrings) > 0) {
      $query = parent::modForQueryString($query, $queryStrings);
    }
    return $query->orderBy('orders.id', 'desc')->paginate(config('search.rpp'));
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  array   $queryStrings the query string
   * @return Illuminate\Pagination\LengthAwarePaginator
   */
  public static function getWarehouseErrors ($queryStrings) {
    $query = static::queryForWarehouseErrors($queryStrings);
    if (count($queryStrings) > 0) {
      $query = parent::modForQueryString($query, $queryStrings);
    }
    return $query->orderBy('orders.id', 'desc')->paginate(config('search.rpp'));
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  integer   integer the order id
   * @return Illuminate\Pagination\LengthAwarePaginator
   */
  public static function getWithCustomer ($id) {
    $order = static::queryForAnOrder()->where('orders.id', '=', $id)->get();
    if (count($order) === 0) {
      throw(new ModelNotFoundException());
    }
    return $order->first();
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  $id the wholesaler id
   * @return Illuminate\Pagination\LengthAwarePaginator
   */
  public static function getWholesale ($id) {
    return static::queryForSeller($id)
      ->orderBy('orders.id', 'desc')
      ->paginate(config('search.rpp'));
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  array   $queryStrings the query string
   * @return Illuminate\Pagination\LengthAwarePaginator
   */
  public static function getWholesaleErrors ($queryStrings) {
    $query = static::queryForSellerErrors($queryStrings);
    if (count($queryStrings) > 0) {
      $query = parent::modForQueryString($query, $queryStrings);
    }
    return $query->orderBy('orders.id', 'desc')->paginate(config('search.rpp'));
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param int $idSeller
   * @param array $oids
   * @return Illuminate\Database\Eloquent\Collection
   */
  public static function getWholesaleOrdersByIds ($idSeller, $oids) {
    return static::queryForSeller($idSeller)
      ->whereIn('orders.id', $oids)
      ->orderBy('orders.id', 'desc')
      ->get();
  }
  /**
  * @author Giomani Designs (Development Team)
   * @return Illuminate\Database\Eloquent\Collection
   */
  public static function getWholesalerUnpaid ($id) {
    return static::queryForSeller($id)
    ->where('seller_paid', '=', '0')
      ->paginate(config('search.rpp'));
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  integer       $id
   * @return Illuminate\Database\Query\Builder
   */
  public static function makePriority($id) {
    $this->setPriority($id, 1);
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param        $object
   * @param array  $columns the columns to search
   * @param string $query the term to search for
   * @return Illuminate\Database\Query\Builder
   */
  protected static function modForQuery ($object, $columns, $query) {
    $object->where(function ($qz) use ($columns, $query) {
      foreach ($columns as $column) {
        $qz->orWhere($column, 'like', '%' . $query . '%');
      }
    });
    return $object;
  }
  /**
   * @author Giomani Designs (Development Team)
   * @return Illuminate\Database\Query\Builder
   */
  protected static function queryForAnOrder() {
    return Order::select(
        'orders.id as oid',
        'iNo',
        'dBusinessName',
        'dPostcode',
        'postcode',
        'staffName as ref',
        'orderType',
        'order_status',
        'startDate',
        'total',
        'paid',
        'done',
        'userID',
        'supplier_name',
        'priority'
      )
      ->join('customers', 'orders.cid', '=', 'customers.id')
      ->join('order_status', 'orders.id_status', '=', 'order_status.id')
      ->leftJoin('supplier', 'orders.id_supplier', '=', 'supplier.id')
      ->leftJoin('users', 'orders.staffid', '=', 'users.id');
  }
  /**
   * @author Giomani Designs (Development Team)
   * @return Illuminate\Database\Query\Builder
   */
  protected static function queryForMissedParts() {
  	return OrderItems::select(
  			'orders.id as oid',
  			'iNo',
  			'dBusinessName as name',
  			'dPostcode',
  			'postcode',
  			'orders.staffName as ref',
  			'orderType',
  			'order_status',
  			'startDate',
  			'total',
  			'paid',
  			'done',
  			'userID',
  			'deliverBy',
  			'supplier_name',
  			\DB::raw('group_concat(orderItems.notes) as items')
  			)
  			->join('orders','orderItems.oid','=','orders.id')
  			->join('customers', 'orders.cid', '=', 'customers.id')
  			->join('order_status', 'orders.id_status', '=', 'order_status.id')
  			->leftJoin('supplier', 'orders.id_supplier', '=', 'supplier.id')
  			->leftJoin('users', 'orders.staffid', '=', 'users.id')
  			->groupBy('orders.id');
  }
  /**
   * @author Giomani Designs (Development Team)
   * @return Illuminate\Database\Query\Builder
   */
  protected static function queryForEod() {
    return Order::select(
        'orders.id',
        'orders.staffId',
        'orders.staffName',
        'orders.startDate',
        'orders.total',
        'orders.orderStatus',
        DB::raw('COUNT(staffName) as count')
      )
      ->groupBy('staffName')
      ->leftJoin('users', 'users.id', '=', 'orders.staffId')
      ->orderBy('staffName', 'asc');
  }

  /**
   * @author Giomani Designs (Development Team)
   * @return Illuminate\Database\Query\Builder
   */
  protected static function queryForByInvoiceNumber() {
    return Order::select(
        'orders.iNo',
        'orders.id',
        'orders.total',
        'stock_items.itemCode',
        'orderItems.Qty',
        'orders.orderStatus',
        'orderItems.price'
      )
      ->leftJoin('orderItems', 'orderItems.oid', '=', 'orders.id')
      ->leftJoin('stock_items', 'stock_items.id', '=', 'orderItems.itemid');
  }
  /**
   * @author Giomani Designs (Development Team)
   * @return Illuminate\Database\Query\Builder
   */
  protected static function queryForCod() {
    return Order::select(
        'customers.dBusinessName as c_name',
        // 'customers.dNumber as c_number',
        // 'customers.dStreet as c_street',
        // 'customers.dTown as c_town',
        // 'customers.dPostcode as c_postcode',
        'customers.tel as c_tel',
        'customers.mob as c_mob',

        'direct.name as d_name',
        // 'direct.number as d_number',
        // 'direct.street as d_street',
        // 'direct.town as d_town',
        // 'direct.postcode as d_postcode',
        'direct.tel as d_tel',
        'direct.mob as d_mob',

        'orders.id as oid',
        'orders.delTime',
        'orders.direct',
        'orders.done',
        'orders.orderType',
        'orders.orderStatus',
        'orders.paid',
        'orders.startDate',
        'orders.total',
        'orders.priority',
        DB::raw('orders.total - orders.paid as owed')
      )
      ->rightJoin('disp', 'disp.oid', '=', 'orders.id')
      ->leftJoin('direct', 'direct.oid', '=', 'orders.id')
      ->leftJoin('customers', 'customers.id', '=', 'orders.cid')
      ->where('orders.paymentType', '=', 'cash')
      ->whereRaw(DB::raw('orders.total - orders.paid > 0'));
  }
  /**
   * @author Giomani Designs (Development Team)
   * @return Illuminate\Database\Query\Builder
   */
  protected static function queryForCollect() {
    return Order::select(
        'orders.id as oid',
        'iNo',
        'dBusinessName',
        'postcode',
        'staffName as ref',
        'orderType',
        'order_status',
        'startDate',
        'total',
        'paid',
        'done',
        'orders.priority',
        'userID'
      )
      ->join('customers', 'orders.cid', '=', 'customers.id')
      ->join('order_status', 'orders.id_status', '=', 'order_status.id')
      ->leftJoin('users', 'orders.staffid', '=', 'users.id')
      ->where('orders.to_collect', '=', '1');
  }
  /**
   * @author Giomani Designs (Development Team)
   * @return Illuminate\Database\Query\Builder
   */
  protected static function queryForCollected() {
    return Order::select('orders.id as oid',
        'iNo',
        'dBusinessName',
        'postcode',
        'staffName as ref',
        'orderType',
        'order_status',
        'startDate',
        'total',
        'paid',
        'done',
        'orders.priority',
        'userID'
      )
      ->join('customers', 'orders.cid', '=', 'customers.id')
      ->join('order_status', 'orders.id_status', '=', 'order_status.id')
      ->leftJoin('users', 'orders.staffid', '=', 'users.id')
      ->where('orders.is_collected', '=', '1');
  }
  /**
   * @author Giomani Designs (Development Team)
   * @return Illuminate\Database\Query\Builder
   */
  protected static function queryForEscalations() {
    return Order::select('orders.id as oid',
        'iNo',
        'dBusinessName',
        'postcode',
        'staffName as ref',
        'orderType',
        'order_status',
        'startDate',
        'total',
        'paid',
        'done',
        'orders.priority',
        'userID'
      )
      ->join('customers', 'orders.cid', '=', 'customers.id')
      ->join('order_status', 'orders.id_status', '=', 'order_status.id')
      ->leftJoin('users', 'orders.staffid', '=', 'users.id')
      ->where('orders.escalate', '=', '1');
  }
  /**
   * @author Giomani Designs (Development Team)
   * @return Illuminate\Database\Query\Builder
   */
  protected static function queryForFeedback() {
    return Order::select('orders.id as oid',
        'iNo',
        'dBusinessName',
        'postcode',
        'staffName as ref',
        'orderType',
        'order_status',
        'startDate',
        'total',
        'paid',
        'done',
        'orders.priority',
        'userID'
      )
      ->join('customers', 'orders.cid', '=', 'customers.id')
      ->join('order_status', 'orders.id_status', '=', 'order_status.id')
      ->leftJoin('users', 'orders.staffid', '=', 'users.id')
      ->where('orders.feedbackScore', '=', '1');
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  int $id the supplier id
   * @param  string $date the date the labels are for in the format dd-mmm-yyyy
   * @return Illuminate\Database\Query\Builder
   */
  protected static function queryForDispatched() {
    return Order::select(
        'orders.id as oid',
        'orders.delOrder as del_order',
        'orders.direct',
        'orders.code as barcode',
        'orders.parcelid',
        'orderItems.itemid',
         //'orderItems.priority',
        'customers.dBusinessName as c_name',
        'customers.dNumber as c_number',
        'customers.dStreet as c_street',
        'customers.dTown as c_town',
        'customers.dPostcode as c_postcode',
        'customers.tel as c_tel',
        'customers.mob as c_mob',
        'direct.name as d_name',
        'direct.number as d_number',
        'direct.street as d_street',
        'direct.town as d_town',
        'direct.postcode as d_postcode',
        'direct.tel as d_tel',
        'direct.mob as d_mob',
        'disp.route as route_name',
        'disp.date as date',
        'stock_items.itemCode as item_code',
        'stock_items.isMulti',
        'stock_items.pieces',
        'categories.name as category_name',
        'customers.userID'
      )
      ->rightjoin('disp', 'orders.id', '=', 'disp.oid')
      ->leftjoin ('customers', 'customers.id', '=', 'orders.cid')
      ->leftjoin ('direct', 'direct.oid', '=', 'orders.id')
      ->leftjoin ('orderItems', 'orderItems.oid', '=', 'orders.id')
      ->leftjoin ('stock_items', 'stock_items.id', '=', 'orderItems.itemid')
      ->leftjoin ('categories', 'categories.id', '=', 'stock_items.category_id')
      ->orderBy('delOrder', 'asc');
  }
  /**
   * @author Giomani Designs (Development Team)
   * @return Illuminate\Database\Query\Builder
   */
  protected static function queryForDispatchedNotes() {
    return Order::select(
        'categories.name as category_name',
        'orders.companyName',
        'orders.code as barcode',
        'orders.direct',
        'orders.delOrder as del_order',
        'orders.g_id',
        'orders.g_num',
        'orders.id as oid',
        'orders.id_seller',
        'orders.iNo',
        'orders.orderType as order_type',
        'orders.paymentType',
        'orders.parcelid',
        'orders.paid',
        'orders.priority',
        'orders.ouref',
        'orders.total',
        'orders.vatAmount',
        DB::raw('GROUP_CONCAT(DISTINCT concat(orderItems.Qty, ",", COALESCE(stock_items.itemCode, ""), ",", COALESCE(stock_items.itemDescription, ""), ",", COALESCE(orderItems.notes, ""), ",", COALESCE(orderItems.price, ""), ",", COALESCE(orderItems.lineTotal, ""), ",", COALESCE(stock_items.isMulti, ""), ",", COALESCE(stock_items.pieces, "")) separator ";") as items'),
        DB::raw('GROUP_CONCAT(DISTINCT COALESCE(orderNotes.notes, "") separator ";") as order_notes'),
        DB::raw('GROUP_CONCAT(DISTINCT codes.itemCode) as parts_delivered'),
        'orderItems.itemid',
        'customers.businessName as i_name',
        'customers.number as i_number',
        'customers.street as i_street',
        'customers.town as i_town',
        'customers.postcode as i_postcode',
        'customers.dBusinessName as c_name',
        'customers.dNumber as c_number',
        'customers.dStreet as c_street',
        'customers.dTown as c_town',
        'customers.dPostcode as c_postcode',
        'customers.email1',
        'customers.tel as c_tel',
        'customers.mob as c_mob',
        'direct.name as d_name',
        'direct.number as d_number',
        'direct.street as d_street',
        'direct.town as d_town',
        'direct.postcode as d_postcode',
        'direct.tel as d_tel',
        'direct.mob as d_mob',
        'disp.route as route_name',
        'disp.date as date',
        'customers.userID'
      )
      ->leftjoin('disp', 'orders.id', '=', 'disp.oid')
      ->leftjoin('customers', 'customers.id', '=', 'orders.cid') // I think...
      ->leftjoin('direct', 'direct.oid', '=', 'orders.id')
      ->leftjoin('orderItems', 'orderItems.oid', '=', 'orders.id')
      ->leftjoin('stock_items', 'stock_items.id', '=', 'orderItems.itemid')
      ->leftjoin('categories', 'categories.id', '=', 'stock_items.category_id')
      // ->leftJoin('orderNotes', 'orderNotes.oid', '=', 'disp.oid')
      ->leftJoin('orderNotes', function ($join) {
            $join->on('orderNotes.oid', '=', 'disp.oid')
                 ->where('orderNotes.driver', '=', 1);
        })
      ->leftJoin('codes', 'codes.oid', '=', 'disp.oid')
      ->groupBy(['orders.id', 'orderNotes.oid']);
  }
  /**
   * @author Giomani Designs (Development Team)
   * @return Illuminate\Database\Eloquent\Collection
   */
  public static function queryForLabels() {
    return static::select(
        // 'categories.name as category_name',

        'orders.id as oid',
        'orders.cid',
        'orders.code',
        'orders.delOrder as del_order',
        'orders.parcelid',
        'orders.priority',

        'codes.parcelid',
        'codes.warehouse',

        'customers.businessName as i_name',
        'customers.number as i_number',
        'customers.street as i_street',
        'customers.town as i_town',
        'customers.postcode as i_postcode',
        'customers.dBusinessName as c_name',
        'customers.dNumber as c_number',
        'customers.dStreet as c_street',
        'customers.dTown as c_town',
        'customers.dPostcode as c_postcode',
        'customers.email1',
        'customers.tel as c_tel',
        'customers.mob as c_mob',

        'direct.name as d_name',
        'direct.number as d_number',
        'direct.street as d_street',
        'direct.town as d_town',
        'direct.postcode as d_postcode',
        'direct.tel as d_tel',
        'direct.mob as d_mob',

        'disp.date as disp_date',
        'routeName.name as route_name',
        // 'dispRoute.date as disp_date',
        // 'dispRoute.route as route_name',

         DB::raw('GROUP_CONCAT(DISTINCT concat(COALESCE(codes.itemCode, ""), ",", COALESCE(bar, ""), ",", COALESCE(orderItems.notes, ""), ",", COALESCE(stock_items.itemDescription, ""))  separator ";") as items'),
         DB::raw('IF (id_supplier = 11, 0, 1) AS showCustomer')
      )
      ->leftJoin('codes', 'codes.oid', '=', 'orders.id')
      ->leftJoin('customers', 'customers.id', '=', 'orders.cid')
      ->leftJoin('orderItems', 'orderItems.id', '=', 'codes.lineid')
      ->leftJoin('stock_items', 'stock_items.id', '=', 'orderItems.itemid')
      ->leftJoin ('direct', 'direct.oid', '=', 'orders.id')
      ->leftJoin('disp', 'disp.oid', '=', 'orders.id')
      ->leftJoin('routeName', 'routeName.id', '=', 'orders.id_delivery_route')
      ->groupBy('oid');
      // ->orderBy('codes.warehouse')
      // ->orderBy('codes.parcelid');
   }
  /**
   * @author Giomani Designs (Development Team)
   * @return Illuminate\Database\Query\Builder
   */
  protected static function queryForNotLoaded() {
    return Order::select(
        'orders.id as oid',
        'orders.priority',
        'name',
        'postcode',
        'disp.date',
        'driver'
      )
      ->join('customers', 'orders.cid', '=', 'customers.id')
      ->leftJoin('routeName', 'orders.id_delivery_route', '=', 'routeName.id')
      ->leftJoin('disp', 'disp.oid', '=', 'orders.id')
      ->leftJoin('dispRoute', function($join) {
        $join->on('orders.id_delivery_route', '=', 'dispRoute.id_route');
        $join->on('dispRoute.date', '=', 'disp.date');
      })
      ->where('orders.notLoaded', '=', '1');
  }
  /**
   * @author Giomani Designs (Development Team)
   * @return Illuminate\Database\Query\Builder
   */
  protected static function queryForDelIssue() {
  	return Order::select(
  			'orders.id as oid',
  			'orders.priority',
  			'name',
  			'postcode',
  			'disp.date',
  			'driver'
  			)
  			->join('customers', 'orders.cid', '=', 'customers.id')
  			->leftJoin('routeName', 'orders.id_delivery_route', '=', 'routeName.id')
  			->leftJoin('disp', 'disp.oid', '=', 'orders.id')
  			->leftJoin('dispRoute', function($join) {
  				$join->on('orders.id_delivery_route', '=', 'dispRoute.id_route');
  				$join->on('dispRoute.date', '=', 'disp.date');
  			})
  			->where('orders.delivery_issue', '=', '1');
  }
  /**
   * @author Giomani Designs (Development Team)
   * @return Illuminate\Database\Query\Builder
   */
  protected static function queryForOnHold () {
    return Order::select(
        'orders.id as oid',
        'iNo',
        'dBusinessName',
        'postcode',
        'staffName as ref',
        'orderType',
        'order_status',
        'startDate',
        'total',
        'paid',
        'done',
        'priority',
        'userID'
      )
      ->join('customers', 'orders.cid', '=', 'customers.id')
      ->leftJoin('order_status', 'orders.id_status', '=', 'order_status.id')
      ->leftJoin('users', 'orders.staffid', '=', 'users.id')
      ->where('orders.hold', '=', '1');
  }
  /**
   * @author Giomani Designs (Development Team)
   * @return Illuminate\Database\Query\Builder
   */
  protected static function queryForOrder () {
    return Order::select(
        'orders.id',
        'iNo',
        'code',
        'parcelid',

        'neightbour',
        'aftersales',
        'wes',
        'wholeError',

        'id_delivery_route',

        'businessName',
        'dBusinessName',
        'street',
        'dStreet',
        'town',
        'dTown',
        'postcode',
        'dPostcode',

        'staffName as ref',
        'orderType',
        'order_status',
        'startDate',
        'total',
        'paid',
        'done',
        'priority',
        'userID',

        DB::raw('GROUP_CONCAT (CONCAT (COALESCE "", orderItems.itemCode))')
      )
      ->join('customers', 'orders.cid', '=', 'customers.id')
      ->leftJoin('order_status', 'orders.id_status', '=', 'order_status.id')
      ->leftJoin('users', 'orders.staffid', '=', 'users.id')
      ->leftJoin('routeName', 'routeName.id', '=', 'orders.id_delivery_route');
  }
  /**
   * @author Giomani Designs (Development Team)
   * @return Illuminate\Database\Query\Builder
   */
  protected static function queryForPayPal () {
    return Order::select(
        'orders.id as oid',
        'orders.priority',
        'dBusinessname as name',
        'dPostcode as postcode',
        'stock_items.itemCode'
      )
      ->join('customers', 'orders.cid', '=', 'customers.id')
      ->leftJoin('orderItems', 'orders.id', '=', 'orderItems.oid')
      ->leftJoin('stock_items', 'orderItems.itemid', '=', 'stock_items.id')
      ->where('paypal', '=', '1');
  }
  /**
   * @author Giomani Designs (Development Team)
   * @return Illuminate\Database\Query\Builder
   */
  protected static function queryForPost () {
    return Order::select(
        'orders.id as oid',
        'orders.priority',
        'routeName.name',
        'postcode',
        'postLoc',
        'startDate'
      )
      ->join('customers', 'orders.cid', '=', 'customers.id')
      ->leftJoin('routeName', 'orders.id_delivery_route', '=', 'routeName.id')
      ->where('post', '=', '1');
  }
  /**
   * @author Giomani Designs (Development Team)
   * @return Illuminate\Database\Query\Builder
   */
  protected static function queryForPrintSupplierOnDispatch () {
    return Order::select(
        'orders.id as oid',
        'orders.priority',
        DB::raw('GROUP_CONCAT(DISTINCT concat(COALESCE(orderItems.qty, ""), ",", COALESCE(stock_items.itemCode, ""), ",", COALESCE(orderItems.notes, ""), ",", COALESCE(stock_items.itemDescription, "")) separator ";") as items'),
        'stock_items.itemCode',
        'supplier_name',
        'orders.id_delivery_route',
        'routeName.name as route_name'
      )
      ->rightJoin('disp', 'disp.oid', '=', 'orders.id')
      ->rightJoin('supplier', function ($join) {
        $join->on('supplier.id', '=', 'orders.id_supplier')
          ->where('supplier.print_orders_on_dispatch', '=', '1');
      })
      ->leftJoin('orderItems', 'orderItems.oid','=', 'orders.id')
      ->leftJoin('stock_items', 'stock_items.id', '=', 'orderItems.itemid')
      ->leftJoin('routeName', 'routeName.id', '=', 'orders.id_delivery_route')
      ->groupBy('orders.id')
      ->orderBy('id_supplier', 'asc');

// select
// orders.id as oid,
// GROUP_CONCAT(DISTINCT concat(COALESCE(orderItems.qty, ''), ',', COALESCE(stock_items.itemCode, ''), ',', COALESCE(orderItems.notes, ''), ',', COALESCE(stock_items.itemDescription, '')) separator ';') as items,
//   stock_items.itemCode,
//   id_supplier,
//   orders.id_delivery_route,
//   `routeName`.`name`
// from orders
//
// right join disp on disp.oid = orders.id
// left join orderItems on orderItems.oid = orders.id
// left join stock_items on stock_items.id = orderItems.itemid
// left join routeName on routeName.id = orders.id_delivery_route
//
// where id_supplier in (6, 5, 3)
// and disp.date = '19-jul-2016'
//
// group by orders.id
// order by id_supplier asc;
  }
  /**
   * @author Giomani Designs (Development Team)
   * @return Illuminate\Database\Query\Builder
   */
  protected static function queryForRefunds () {
    return Order::select(
        'orders.id as oid',
        'name',
        'dPostcode as postcode',
        'orders.companyName',
        'orders.paymentType',
        'orders.priority',
        'orders.total'
      )
      ->join('customers', 'orders.cid', '=', 'customers.id')
      ->leftJoin('routeName', 'orders.id_delivery_route', '=', 'routeName.id')
      ->where('refundBox', '=', '1');
  }
  /**
   * @author Giomani Designs (Development Team)
   * @return Illuminate\Database\Query\Builder
   */
  protected static function queryForRetention () {
    return Order::select(
        'orders.id as oid',
        'orders.priority',
        'dBusinessname as name',
        'dPostcode as postcode',
        'stock_items.itemCode'
      )
      ->join('customers', 'orders.cid', '=', 'customers.id')
      ->leftJoin('orderItems', 'orders.id', '=', 'orderItems.oid')
      ->leftJoin('stock_items', 'orderItems.itemid', '=', 'stock_items.id')
      ->where('retention', '=', '1');
  }
  /**
   * @author Giomani Designs (Development Team)
   * @return Illuminate\Database\Query\Builder
   */
  protected static function queryForRoute ($routeId) {
    return static::select(
      'orders.id',
      'orders.cid',
      'orders.iNo',
      'orders.orderStatus',
      'orders.orderType',
      'orders.paid',
      'orders.staffName',
      'orders.startDate',
      'orders.total',
      'orders.priority',
      'order_status',
      'customers.userID',
      'customers.businessName',
      'customers.dBusinessName',
      'customers.postcode',
      'customers.dPostcode'
    )
    ->leftJoin('customers', 'customers.id', '=', 'orders.cid')
    ->join('order_status', 'orders.id_status', '=', 'order_status.id')
    ->where('id_delivery_route', '=', $routeId);
  }
  /**
   * @author Giomani Designs (Development Team)
   * @return Illuminate\Database\Query\Builder
   */
  protected static function queryForSellers () {
    return Order::select(
        DB::raw('count(*) as count'),
        DB::raw('sum(total) as total'),
        'id_seller',
        'sellers.iName',
        'sellers.name'
      )
      ->rightJoin('sellers', 'sellers.id', '=', 'orders.id_seller')
      ->groupBy('sellers.id');
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  $id the supplier id
   * @return Illuminate\Database\Query\Builder
   */
  protected static function queryForSupplier ($id) {
    return Order::select(
        'orders.id as oid',
        'ordered',
        'processed',
        'paid_supplier',
        'startDate',
        'deliverBy',
        'iNo',
        'dBusinessName',
        'routeName.name as route_name',
        'dPostcode',
        'orders.paymentType',
        'total',
        'paid',
        'done',
        'priority',
        DB::raw('group_concat(stock_items.itemCode separator ";") as itemCode')
      )
      ->join('customers', 'orders.cid', '=', 'customers.id')
      ->leftJoin('orderItems', 'orders.id', '=', 'orderItems.oid')
      ->leftJoin('stock_items', 'orderItems.itemId', '=', 'stock_items.id')
      ->join('routeName', 'orders.id_delivery_route', '=', 'routeName.id')
      ->where('orders.id_supplier', '=', $id)
      ->groupBy('orders.id');
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  array  $ids the order ids
   * @return Illuminate\Database\Query\Builder
   */
  protected static function queryForSupplierInvoiced ($ids) {
    return Order::select(
      'delTime',
      'orders.id as oid',
      'iNo',
      'startDate',
      'deliverBy',
      'paid_supplier',
      'dBusinessName',
      'routeName.name as route_name',
      'dNumber as a_number',
      'dStreet as a_street',
      'dTown as a_town',
      'dPostcode as a_postcode',
      'orders.paymentType',
      'total',
      'paid',
      'priority',
      'customers.tel',
      'orders.delOrder',
      DB::raw('orders.paid - orders.total as balance'),
      //DB::raw('GROUP_CONCAT( CONCAT (COALESCE(stock_items.itemCode, ""), ",", COALESCE(stock_items.itemDescription, ""), ",", COALESCE(orderItems.notes, ""), ",", COALESCE(orderItems.Qty, ""), ",", COALESCE(stock_items.pieces, "")) SEPARATOR ";") as itemCode'),
      DB::raw('GROUP_CONCAT( CONCAT (COALESCE(stock_items.itemCode, ""), ",", COALESCE(stock_items.itemDescription, ""), ",", COALESCE(orderItems.Qty, ""), ",", COALESCE(stock_items.pieces, ""), ",", COALESCE(orderItems.notes, "")) SEPARATOR ";") as itemCode'),
      'processed',
      'ordered'
    )
    ->join('customers', 'orders.cid', '=', 'customers.id')
    ->leftJoin('orderItems', 'orders.id', '=', 'orderItems.oid')
    ->leftJoin('stock_items', 'orderItems.itemId', '=', 'stock_items.id')
    ->join('routeName', 'orders.id_delivery_route', '=', 'routeName.id')
    ->groupBy('orders.id')
    ->whereIn('orders.id', $ids);
  }
  /**
   * @author Giomani Designs (Development Team)
   * @return Illuminate\Database\Eloquent\Collection
   */
  public static function queryForTracking () {
    return static::select(
      'orders.id',
      'customers.businessName',
      'customers.email1 as email'
    )
    ->leftJoin('customers', 'customers.id', '=', 'orders.cid');
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  $iid the invoice id
   * @return Illuminate\Database\Query\Builder
   */
  protected static function queryForUpdateDispatchedByRouteAsDelivered () {
    return Order::update()
      ->rightJoin('disp', 'disp.oid', '=', 'orders.id');
  }
  /**
   * @author Giomani Designs (Development Team)
   * @return Illuminate\Database\Query\Builder
   */
  protected static function queryForUrgent() {
    return Order::select(
        'orders.id as oid',
        'iNo',
        'dBusinessName',
        'postcode',
        'staffName as ref',
        'orderType',
        'order_status',
        'startDate',
        'total',
        'paid',
        'done',
        'priority',
        'userID'
      )
      ->join('customers', 'orders.cid', '=', 'customers.id')
      ->leftJoin('order_status', 'orders.id_status', '=', 'order_status.id')
      ->where('urgent', '=', '1');
  }
  /**
   * @author Giomani Designs
   * @return Illuminate\Database\Query\Builder
   */
  public static function queryForWarehouseErrors () {
    return Order::select(
        'orders.id as oid',
        'routeName.name',
        'dPostcode',
        'note',
        'item',
        'deliverBy',
    	\DB::raw('(select picker from dispRoute where date=disp.date and route=disp.route) as picker'),
    	\DB::raw('(select loader from dispRoute where date=disp.date and route=disp.route) as loader'),
    	\DB::raw('(select driver from dispRoute where date=disp.date and route=disp.route) as driver')
      )
      ->join('customers', 'orders.cid', '=', 'customers.id')
      ->join('routeName', 'orders.id_delivery_route', '=', 'routeName.id')
      ->join('warehouse_errors', 'orders.id', '=', 'warehouse_errors.oid')
      ->leftJoin('disp' , 'orders.id','=','disp.oid')
      ->where('wes', '=', '1');
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  $id the wholesaler/seller id
   * @return Illuminate\Database\Query\Builder
   */
  protected static function queryForSeller ($id) {
    return Order::select(
        'orders.id as oid',
        'iNo',
        'userID',
        'dBusinessName',
        'postcode',
        'orders.staffName',
        'orderType',
        'order_status',
        'startDate',
        'total',
        'paid',
        'seller_paid',
        'voucher_code',
        'orders.staffName as ref',
        'orders.done',
        'orders.priority',
        DB::raw('GROUP_CONCAT(COALESCE(stock_items.itemCode, ""), ",", COALESCE(orderItems.Qty, "")) as items')
      )
      ->leftJoin('customers', 'orders.cid', '=', 'customers.id')
      ->leftJoin('order_status', 'orders.id_status', '=', 'order_status.id')
      ->leftJoin('orderItems', 'orderItems.oid', '=', 'orders.id')
      ->leftJoin('stock_items', 'stock_items.id', '=', 'orderItems.itemid')
      ->where('orders.id_seller', '=', $id)
      ->groupBy('orders.id')
      ->orderBy('orders.id', 'desc');
  }
  /**
   * @author Giomani Designs (Development Team)
   * @return Illuminate\Database\Query\Builder
   */
  public static function queryForSellerErrors () {
    return  Order::select(
        'orders.id as oid',
        'iNo',
        'dBusinessName',
        'postcode',
        'staffName as ref',
        'orderType',
        'order_status',
        'startDate',
        'total',
        'paid',
        'done',
        'priority',
        'userID'
      )
      ->join('customers', 'orders.cid', '=', 'customers.id')
      ->leftJoin('order_status', 'orders.id_status', '=', 'order_status.id')
      ->where('wholeError', '=', '1');
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  int $sid supplier id
   * @param  int $iid invoice id
   */
  public static function setDispatchedByRouteAsDelivered ($date, $routeName) {
    return static::queryForUpdateDispatchedByRouteAsDelivered()
      ->update(['status' => 'Done'])
      ->where('disp.date', '=', $date)
      ->where('disp.route', '=', $routeName);
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  int $sid supplier id
   * @param  int $iid invoice id
   */
  public static function setInvoicePaid ($sid, $iid)
  {
    return Order::where('id_supplier', '=', $sid)
      ->where('invoice_made', '=', $iid)
      ->update(['paid_supplier' => 1]);
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  int $id order id
   */
  public static function setOrdered ($id) {
    if (is_array($id)) {
      $query = Order::whereIn('id', $id);
    } else {
      $query = Order::where('id', '=', $id);
    }
    return $query->update(['ordered' => 1]);
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  integer  $id
   * @param  integer  $priority
   * @return
   */
  public static function setPriority ($id, $priority) {
    return Order::where('id', '=', $id)->update(['priority' => $priority]);
  }

  /**
   * @author Giomani Designs (Development Team)
   * @param  array   $ids order id or array od order ids
   */
  public static function setProcessed ($id) {
    if (is_array($id)) {
      $query = Order::whereIn('id', $id);
    } else {
      $query = Order::where('id', '=', $id);
    }
    return $query->update(['processed' => 1]);
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  int $id order id
   */
  public static function setSellerUnProcessed ($id) {
    return Order::where('id', '=', $id)
      ->update(['processed' => 0]);
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  int   $id  seller id
   * @param  array $ids order ids
   */
  public static function setSellersPaid($id, $ids) {

    return Order::where('id_seller', '=', $id)
      ->whereIn('id', $ids)
      ->update(['seller_paid' => 1, 'paid' => \DB::raw('total')]);
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  int   $id  seller id
   * @param  array $ids order ids
   */
  public static function setSellersUnPaid($id, $ids) {
    return Order::where('id_seller', '=', $id)
      ->whereIn('id', $ids)
      ->update(['seller_paid' => 0]);
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  int $id order id
   * @param  int $sid supplier id
   */
  public static function setSupplier($id, $sid) {
    return Order::where('id', '=', $id)
      ->update(['id_supplier' => 0]);
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  integer|array $id order id
   */
  public static function setUnOrdered($id) {
    if (is_array($id)) {
      $query = Order::whereIn('id', $id);
    } else {
      $query = Order::where('id', '=', $id);
    }
    return $query->update(['ordered' => 0]);
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  integer|array   $id order id or array od order ids
   */
  public static function setUnProcessed($id) {
    if (is_array($id)) {
      $query = Order::whereIn('id', $id);
    } else {
      $query = Order::where('id', '=', $id);
    }
    return $query->update(['processed' => 0]);
  }


	/*
	 *
	 */
	public static function setDeliveryRouteBulk($ids) {
		foreach($ids as $id){
			self::setDeliveryRoute($id);
		}
	}
  /*
   *
   */
  public static function setDeliveryRoute($id) {


	$supplier = [];
    $order = Order::where(['orders.id' => $id])
      ->join('customers', 'orders.cid', '=', 'customers.id')
      ->select(DB::Raw('IF( orders.direct = 1 , (select postcode from direct where oid='.$id.'), customers.dPostcode  ) as postcode'))
      ->get()->first();

    $postcode = $order['postcode'];
    $string = preg_replace('/\s+/', '', $postcode);
    $string = strtoupper($string);

    $coords = Coordinates::forPostcode($string,true);
    $data=[];

    $data['lat'] =  (isset($coords['lat'])) ? $coords['lat'] : "" ;
    $data['lng'] =  (isset($coords['lng'])) ? $coords['lng'] : "" ;

    $text = preg_replace('/[^\\/\-a-z\s]/i', '', $postcode);
    $text = explode(" ", $text);
    $post = $text[0];

    if (count($text) == 1) {
      $post = substr($post, 0, -2);
    }

    $route = RouteStops::where('postCode',$post)
      ->join('routeName','routeStops.routeID', '=', 'routeName.id')
      ->select('name','routeID as id')
      ->get()
      ->first();

    $data['delRoute'] = isset($route->name) ? $route->name : '';
    $data['id_delivery_route'] = isset($route->id) ? $route->id : 0;

    if (isset($route->name)) {
      Order::where('id',$id)
	  	//	->where('delRoute','not like', 'Route%')
			->update($data);
    } else {
       Order::where('id',$id)->update(['delRoute' => 'No Route' , 'id_delivery_route' => 84]);
    }
    return isset($route->name) ? $route->name : 'No Route';
  }

  public static function setBarcodes(array $orders){
  		foreach($orders as $order){
  			setBarcode($order);
  		}
  }

  public static function setBarcode($oid) {
    $error = 0;
    $errorsStr = array();
    $parcelid = '';
    $code = '';

    $order = Order::findOrFail($oid);

    // $parcelid = preg_replace('/\s+/', '', $order->parcelid);
    $parcelid = $order->parcelid;
    // $code = preg_replace('/\s+/', '', $order->code);
    $code = $order->code;

    $id_order = $order->id;
    $time_stamp = $order->time_stamp;

    if ($parcelid == '') {
      $parcelid = Order::parcelid();
    }

    if ($code == '') {
      $code = Order::barcode($id_order, $time_stamp);
    }

    $order->code = $code;
    $order->parcelid = $parcelid;
    $debug['codes'][] = $code;
    $order->save();

    Links::updateLink($oid, $code, '', '', '');
    Codes::where('oid',$oid)->delete();

    $items  = array();

    $db_fields =  OrderItems::select('orderItems.id','pieces','Qty','itemCode','isMulti','warehouse','label')
      ->leftJoin('stock_items', 'orderItems.itemid', '=' , 'stock_items.id')
      ->where('orderItems.oid', '=', $oid)->get();

    foreach ($db_fields as $db_field) {
      $array = array();
      $array[] = $db_field['id'];
      $array[] = $db_field['pieces'];
      $array[] = $db_field['Qty'];
      $array[] = $db_field['itemCode'];
      $array[] = $db_field['isMulti'];
      $array[] = $db_field['warehouse'];
      $array[] = $db_field['label'];
      $items[] = $array;
    }

    $i = 1;

    foreach ($items as $item) {
      $id = $item[0];
      $parts = $item[1];
      $qty = $item[2];
      $itemCode = $item[3];
      $isMulti = $item[4];
      $warehouse = $item[5];
      $label = $item[6];

      if ($label == 1) {
        if (strlen($isMulti) > 1 || $isMulti > 1) {
          $boxCount = Order::boxes($isMulti, $qty, $parts);
          $thisPart = 1;
          for ($s = 0; $s < $boxCount; $s++) {
            $thisCode = $code . sprintf("%'.03d", $i);
            $itemer = $itemCode . ' Part' . $thisPart . ' of ' . $boxCount;
            $codeModel = new Codes();
            $codeModel->lineid = $id;
            $codeModel->bar = $thisCode;
            $codeModel->oid = $oid;
            $codeModel->parcelid = $parcelid;
            $codeModel->itemcode = $itemer;
            $codeModel->warehouse = $warehouse;
            $codeModel->save();
            $i++;
            $thisPart++;
          }
        } else {
          for ($q = 0; $q < $qty; $q++) {
            $thisPart = 1;

            // add code for each box per item
            for ($s = 0; $s < $parts; $s++) {
              $thisCode = $code . '00' . $i;
              $itemer = $itemCode . ' Part' . $thisPart .' of ' . $parts;
              $codeModel = new Codes();
              $codeModel->lineid = $id;
              $codeModel->bar = $thisCode;
              $codeModel->oid = $oid;
              $codeModel->parcelid = $parcelid;
              $codeModel->itemcode = $itemer;
              $codeModel->warehouse = $warehouse;
              $codeModel->save();
              $i++;
              $thisPart++;
            }
          }
        }
      }
    }
    Order::sortBarcodes($oid);
    return $error;
  }


  public static function boxes ($isMulti, $qty, $parts) {
    $box = 0;
    //$isMulti="1,".$isMulti;

    $multi = explode(",", $isMulti);

    for ($cc = count($multi) - 1; $cc > -1; $cc--) {
      //echo "Qty=$multi[$cc]<br>";

      if ($qty >= $multi[$cc]) {
        $box += floor($qty / $multi[$cc]);
        //echo floor($qty/$multi[$cc])." X $multi[$cc] pack  <br>";
        $qty = $qty % $multi[$cc];
      }

      if (($cc <= 0 && $qty < $multi[$cc] && $qty != 0)) {
        $box++;
      }
    }

    return $box * $parts;
  }


  /**
   *
   */
	public static function parcelid () {

    $row = Count::select('parcel_id_seed')->get()->first();
    $seed = (int)$row['parcel_id_seed'];
    while (($parcelid = (7597 * $seed + 290189) % 10007) > 9999) {
      $seed = $parcelid;
    }
    Count::where('parcel_id_seed', $row['parcel_id_seed'])->update(['parcel_id_seed' => $parcelid]);
    return $parcelid;
  }
  /**
   *
   */
  public static function barcode ($id_order, $time_stamp, $length = 10) {
    $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $randomString = '';

    for($n = 1; $n > 0;){
    	$code = str_random($length);
    	$n = Order::where('code',$code)->count();
    }

    return $code;
    /*
    for ($n = 1; $n > 0; ) {
      for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, strlen($characters) - 1)];
      }

      $n = Order::	where('code',$randomString)->get()->count();

      if ($n > 0) {
        continue;
      }
      $n = Links::	where('code',$randomString)->get()->count();
    }
    return $randomString;
    */
  }


  /**
   * @func sortBarcodes
   * @param id (Order ID)
   * Checks if order has items from different supplier
   * and splits accordingly.
   */
  public static function sortBarcodes($id) {
    $row1 = Order::select('code', 'is_split')->where('id',$id)->first();

    if($row1->is_split==0) {
      return;
    }

    $codes = Codes::select()->where('bar','like', $row1->code.'%')->get();
    $i=1;

    foreach ($codes as $code) {
      Codes::where('pk',$code->pk)->update(['bar' => $row1['code'] . "00" . ($i++)]);
    }
  }



	/*
	 * @func check_split
	 * @param id (Order ID)
	 * Checks if order has items from different supplier
	 * and splits accordingly.
	 */
	public static function check_split ($orderid,$edit = false) {

		$categories = array();
		$cats = OrderItems::select('cat')
		  ->join('stock_items','itemid','=','stock_items.id')
		  ->where('oid',$orderid)
		  ->get();

		foreach($cats as $cat) {
			$categories[]=$cat->cat;
		}

		$splitter = false;
		$expressMail = false;

		if((in_array("AJ Foams Direct",$categories) || in_array("Divan beds",$categories) || in_array("Durest Beds",$categories) || in_array("ExpressMat",$categories) || in_array("Birlea",$categories) || in_array("Bedtastic",$categories) || in_array("Seconique",$categories) || in_array("Collect Express",$categories) || in_array("Collect Durest",$categories) || in_array("Collect BedCompany",$categories) || in_array("Collect DreamBeds",$categories) || in_array("Adam Divans",$categories) || in_array("Adam Mattresses",$categories) || in_array("Adam Headboards",$categories))){

			$split_items=array();

			$rows2 = OrderItems::select('orderItems.*','b.cat','c.id_supplier')
						->join('stock_items as b','orderItems.itemid','=','b.id')
						->join('categories as c', 'b.cat','=','c.name')
						->where('orderItems.oid',$orderid)
						->get();

			foreach($rows2 as $row2){
				$split_items[$row2['id_supplier']][]=$row2;
			}

			ksort($split_items);

			//echo ("order $orderid has been split into ".count($split_items));

			$original = array_shift($split_items);
			$id_supplier = $original[0]['id_supplier'];
			$supplier = Supplier::find($id_supplier);
			$array=[];

			if(isset($supplier->del_route_id)) {
				if($supplier->del_route_id!=0) {
					$array['delRoute'] = Routes::route($supplier->del_route_id);
					$array['id_delivery_route'] = $supplier->del_route_id;
				}
			}

			$supplier = isset($supplier->supplier_name) ? $supplier->supplier_name : '';

			$array['id_supplier'] = $id_supplier;
			$array['ouref'] = $supplier;

			if (count($categories) > 1) {
				$array['is_split'] = 1 ;
				$splitter = true;
			}
			else if( in_array("Divan beds",$categories) || in_array("ExpressMat",$categories) || in_array("Durest Beds",$categories) ){
				$expressMail = true;
			}

			Order::where('id',$orderid)->update($array);

			$orignial_order = Order::find($orderid);
			$orignial_order = $orignial_order->toArray();
			unset($orignial_order['id']);
			unset($orignial_order['ouref']);
			//unset($orignial_order['delRoute']);
			unset($orignial_order['id_supplier']);
			//unset($orignial_order['code']);
			if(count($categories)>1){
				$orignial_order['is_split']=1;
			}
			$split  = false;
			foreach($split_items as $split_Order){

					$split=true;

					$new_order = new Order($orignial_order);
					$new_order->save();
					$split_id =$new_order->id;
					$item_total=0;

					foreach($split_Order as &$split_item){

						$split_item['oid']=$split_id;
						OrderItems::where('id',$split_item['id'])->delete();
						unset($split_item['id']);

						$new_item = new OrderItems($split_item->toArray());
						$new_item->save();

						$item_total+=$split_item['lineTotal'];
						$id_supplier=$split_item['id_supplier'];

						//echo ("inserted items for split order ".$split_id);
					}


					$supplier = Supplier::find($id_supplier);
					$arr=[];

					if(isset($supplier->del_route_id))
					{
						if($supplier->del_route_id!=0)
						{
							$arr['delRoute'] = Routes::route($supplier->del_route_id);
							$arr['id_delivery_route'] = $supplier->del_route_id;

						}
						else{
							 self::setDeliveryRoute($split_id);
						}
					}
					else{
						 self::setDeliveryRoute($split_id);
					}



					//if($orignial_order['paid']==$orignial_order['total'] && $orignial_order['total']>0){
					    \DB::update("update orders set total=total-$item_total,paid=paid-$item_total where id=$orderid");
					//}

					\DB::update("update orders set id_supplier=$id_supplier,ouref=ifnull((select supplier_name from supplier where id=$id_supplier),''), total=$item_total, paid=$item_total where id=$split_id");

					//\DB::update("update orders set paid=$item_total where id=$split_id");

					OrderNotes::addNote("Split Order linked to order <a href='orderDetail.php?id=$orderid'>$orderid</a>",$split_id);
					OrderNotes::addNote("Split Order linked to order <a href='orderDetail.php?id=$split_id'>$split_id</a>",$orderid);

					//Sales::setIno($split_id);

					//set Barcode
					Order::setBarcode($split_id);



			}


		}


		//set Barcode
		Order::setBarcode($orderid);



		return ['splitMail' => $splitter, 'expressMail' => $expressMail];
	}





	public static function invoice($data){

		$pdf = new Invoice();
	  	$pdf->getInvoicePDF($data)->output('I');

		return Response::make(file_get_contents($path), 200, [
			'Content-Type' => 'application/pdf',
			//'Content-Disposition' => 'inline; filename="'.$filename.'"'
		]);

	}



	/*
	* REFUND Order
	*/
	public static function applyRefund($id, $data = [])
    {


		$refund_item = StockItem::itemCode('Refund');
		$itemid = $refund_item->id;

		$original_record = Order::find($id);
		$duplicate_record = $original_record;

		$original_record->orderStatus='Delivered';
		$original_record->id_status = 5;
		$original_record->save();

		$duplicate_record->iNo 			= Count::getRefundNo();
		$duplicate_record->staffName 	= Auth::user()->email;
		$duplicate_record->staffid 		= Auth::user()->id;
		$duplicate_record->total 		= $duplicate_record->total * -1;
		$duplicate_record->paid  		= $duplicate_record->total;
        $duplicate_record->startDate 	= Time::date()->get('d-M-Y');
        $duplicate_record->startTime 	= Time::date()->get('H:i:s');
        $duplicate_record->time_stamp 	= Time::date()->get("Y-m-d H:i:s");
        $duplicate_record->startStamp 	= Time::date()->getStamp();
		$duplicate_record->refund 		= 1;
		$duplicate_record->invoice_made 	= 0;
		$duplicate_record->id_status 	     = OrderStatus::id_status('Refunded');
		$duplicate_record->orderStatus    = 'Refunded';
		$duplicate_record->orderType 	= 'Refund';
		$duplicate_record->id_order_type = OrderType::id_type('Refund');
		$duplicate_record 				= $duplicate_record->toArray();
		unset($duplicate_record['id']);

    $refund = new Order($duplicate_record);
    $refund->parent_order_id = $id ;
		$refund->save();

		//OrderItems::where('oid',$original_record->id)
		//			->join('stock_items','orderItems.itemid','=','stock_items.id')
		//			->update(['actual' => \DB::raw('actual+Qty') , 'itemStatus' => 'Refunded']);

		$refundCost = OrderItems::where('oid',$original_record->id)
					->join('stock_items','orderItems.itemid','=','stock_items.id')
					->sum('costs');

	   	$new_item = new OrderItems();
	   	$new_item->itemid = $itemid;
	   	$new_item->oid = $refund->id;
	   	$new_item->Qty = 1;
	   	$new_item->lineTotal = $refund->total;
	   	$new_item->itemStatus = "Refunded";
	   	$new_item->staffName = Auth::user()->email;
	   	$new_item->currStock = 10000;
	   	$new_item->price = $refund->total;
	   	$new_item->costs = $refundCost;
	   	$new_item->save();

	   	OrderNotes::addNote("Order Refunded <a href='/orders/$refund->id'>$refund->id</a>",$original_record->id);

	   	OrderNotes::addNote("Refund linked to order <a href='/orders/$id'>$id</a>",$refund->id);

		OrderStatusHistory::add([
							'from_status' => 'Allocated',
							'order_id' => $id,
							'text' => 'Delivery cancelled by supplier',
							'to_status' => 'Refunded',
              ]);

    OrderAuthorisation::create([
      'staff_id' => auth()->user()->id,
      'order_id' => $refund->id,
      'requested_by' => $data['requested_by'],
      'authorised_by' => $data['authorised_by'],
      'action_type' =>  $data['action_type'],
      'reason' =>  $data['reason'],
    ]);

		Links::updateLink($id,'', 'Cancelled', '', 'Allocated');

        // return;
    }


	public static function addDiscount($request, $id, $data = [])
    {

		$discount_item = StockItem::itemCode('Discount');
		$itemid = $discount_item->id;

		$itemRetail = $request->get('discount') * -1;

        $original_record = Order::find($id);

		$duplicate_order = $original_record;
		$duplicate_order->iNo 			 = Count::getRefundNo();
		$duplicate_order->startDate      = Time::date()->get('d-M-Y');
        $duplicate_order->startTime    	 = Time::date()->get('H:i:s');
        $duplicate_order->time_stamp     = Time::date()->get("Y-m-d H:i:s");
        $duplicate_order->startStamp     = Time::date()->getStamp();
		$duplicate_order->total  		 = $itemRetail;
		$duplicate_order->paid  		 	 = $itemRetail;
		$duplicate_order->id_status 	     = OrderStatus::id_status('Refunded');
		$duplicate_order->orderStatus    = 'Refunded';
		$duplicate_order->orderType 	 	 = 'Refund';
		$duplicate_order->id_order_type  = OrderType::id_type('Refund');

		$duplicate_order = $duplicate_order->toArray();
		unset($duplicate_order['id']);

    $discount = new Order($duplicate_order);
    $discount->parent_order_id = $id;
		$discount->save();

		$new_item = new OrderItems();
	   	$new_item->itemid = $itemid;
	   	$new_item->oid = $discount->id;
	   	$new_item->Qty = 1;
	   	$new_item->lineTotal = $discount->total;
	   	$new_item->itemStatus = "Refunded";
	   	$new_item->staffName = Auth::user()->email;
	   	$new_item->currStock = 10000;
	   	$new_item->price = $discount->total;
	   	$new_item->costs = 0;
	   	$new_item->save();

		OrderNotes::addNote("Added discount of " . $request->get('discount') . " to order <a href='/orders/$discount->id'>$discount->id</a>",$id);
        OrderNotes::addNote("Discount linked to order <a href='/orders/$id'>$id</a>",$discount->id);

        OrderAuthorisation::create([
          'staff_id' => auth()->user()->id,
          'order_id' => $discount->id,
          'requested_by' => $data['requested_by'],
          'authorised_by' => $data['authorised_by'],
          'action_type' =>  $data['action_type'],
          'reason' =>  $data['reason'],
        ]);


        return 1;
    }


	/**
     *
     */
    public static function createPost($request, $id)
    {


        $order = Order::find($id)->toArray();

        unset($order['id']);
        $order['total'] = 0;
        $order['post']  = 1;
        $order['paid']  = 0;
		$order['iNo']   = Count::getPostNo();
		$order['orderType'] = "Post";
		$order['id_order_type'] = OrderType::id_type('Post');
        $order['orderStatus'] = 'Delivered';
        $order['startDate'] = Time::date()->get('d-M-Y');
        $order['startTime'] = Time::date()->get('H:i:s');
		$order['endDate'] = Time::date()->get('d-M-Y');
        $order['endTime'] = Time::date()->get('H:i:s');
        $order['time_stamp'] = Time::date()->get("Y-m-d H:i:s");
        $order['postLoc'] = $request->get('loc');
        $order['startStamp'] = Time::date()->getStamp();
        $order['stamp'] = Time::date()->getStamp();
        $order = new Order($order);
		$order->save();

		$item = StockItem::itemCode('Missed Parts');
		$itemid = $item['id'];

		$item = new orderItems;
		$item->itemid = $itemid;
		$item->oid = $order->id;
		$item->Qty = 1;
		$item->lineTotal = 0;
		$item->itemStatus='Delivered';
		$item->staffName = Auth::user()->email;
		$item->currStock = 10000;
		$item->price = 0;
		$item->notes = $request->get('item');
		$item->save();


        OrderNotes::addNote("Post Created <a href='/orders/$order->id'>$order->id</a>",$id);

        OrderNotes::addNote("Post linked to order <a href='/orders/$id'>$id</a>",$order->id);

        if ($order->direct == 1) {

            $direct = Direct::forOrder($id)->toArray();
			if($direct){
				$direct['oid']=$order->id;
				$direct = new Direct($direct);
				$direct->save();
			}
		}


        return $order->id;
    }




	public static function edit($id)
	{

		$order = self::find($id);
		$order->orderItems = OrderItems::onOrder($id);
		$order->customer = Customer::find($order->cid);


		$order->VAT = number_format($order->total - ((100 / (100 + $order->vatAmount) * $order->total)),2);
        $order->subTotal = number_format($order->total - $order->VAT,2);
		$order->total= number_format($order->total,2);
		$order->paid= number_format($order->paid,2);

		$suppliers = Supplier::getSupplierList();
		$paymentTypes = PaymentType::all();
		$companies = Company::where(['company_name' => $order->companyName])->get();


		return ['order' => $order, 'suppliers' => $suppliers, 'paymentType' => $paymentTypes, 'companies' => $companies];


	}

  /**
   * 2016.08.22 - Giomani Designs - added calls to stock control (StockItem) functions
   */
  public static function saveOrder ($request) {
    $order =  $request->get('order');

    foreach ($order['items'] as $item) {
      if (isset($item['id'])) {
        //update
        $update = OrderItems::find($item['id']);
        $stock = StockItem::find($update->itemid);

        if ($item['price'] != $update->price) {
          OrderNotes::addNote('Updated ' . $stock->itemCode . ' price ' . $item['price'] ,$order['id']);
        }

        if ($item['Qty'] != $update->Qty) {
          OrderNotes::addNote('Updated '.$stock->itemCode.' Qty '.$item['Qty'] ,$order['id']);

          if ($update->Qty < $item['Qty']) {
            StockItem::addToOrder($update->itemid, $item['Qty'] - $update->Qty);
          } elseif ($update->Qty > $item['Qty']) {
            StockItem::removeFromOrder($update->itemid, $update->Qty - $item['Qty']);
          }
        }

        if($item['discount']!=$update->discount) {
          OrderNotes::addNote('Updated '.$stock->itemCode.' Discount '.$item['discount'] ,$order['id']);
        }
        $update->update($item);
      } else {
        //add
        $orderitem = new OrderItems($item);
        $orderitem->oid = $order['id'];
        $orderitem->staffName = Auth::user()->email;
        $orderitem->id_staff = Auth::user()->id;
        $orderitem->itemStatus = 'Allocated';
        $orderitem->id_item_status = OrderStatus::id_status('Allocated');
        $orderitem->save();
        $item = StockItem::find($orderitem->itemid);
        StockItem::addToOrder($item->id);
        OrderNotes::addNote('Added ' . $item->itemCode . ' Price ' . $orderitem->price, $order['id']);
      }
    }

    if (isset($order['remove'])) {
      foreach ($order['remove'] as $item) {
        $orderitem = OrderItems::find($item);
        $stockitem = StockItem::find($orderitem->itemid);
        OrderNotes::addNote('Removed ' . $stockitem->itemCode, $orderitem->oid);
        StockItem::removeFromOrder($orderitem->itemid, $orderitem->Qty);
        $orderitem->delete();
      }
    }

    unset($order['items']);
    unset($order['remove']);
    Order::check_split($order['id']);
    Order::find($order['id'])->update($order);
  }

  public static function exchange($request,$id) {
    $order = self::find($id);
    unset($order->id);
    unset($order->code);
    unset($order->iNo);

    // TO DO.
    // copy direct customer details

    $exchange = new Order($order->toArray());

    $exchange->iNo = Count::getExchangeNo();
    $exchange->startTime = Time::date()->get('H:i:s');
    $exchange->startDate = Time::date()->get('d-M-Y');
    $exchange->time_stamp = Time::date()->get();
    $exchange->startStamp = Time::date()->getStamp();
    $exchange->id_status = OrderStatus::id_status('Allocated');
    $exchange->orderStatus = 'Allocated';
    $exchange->staffName = Auth::user()->email;
    $exchange->staffid = Auth::user()->id;
    $exchange->paid = 0;
    $exchange->total = 0;
    $exchange->endDate = '';
    $exchange->endTime = '';
    $exchange->id_payment_type = PaymentType::id_type('Exchange');
    $exchange->paymentType = 'Exchange';
    $exchange->authNo = '';
    $exchange->cardNo = '';
    $exchange->exp = '';
    $exchange->ouref = 'Exchange';
    $exchange->orderType = 'Exchange';
    $exchange->id_order_type= OrderType::id_type('Exchange');
    $exchange->deliverBy = 0;
    $exchange->delTime = '';
    $exchange->delStat = '';
    $exchange->parcelid = 0;
    $exchange->neighbour = 0;
    $exchange->aftersales = 0;
    $exchange->safePlace = 0;
    $exchange->done = 0;
    $exchange->notLoaded = 0;
    $exchange->p = 0;
    $exchange->wes = 0;
    $exchange->delDate = '';
    $exchange->colID = 0;
    $exchange->delID = 0;
    $exchange->lat = '';
    $exchange->lng = '';
    $exchange->amazonID = '';
    $exchange->amzShipped = 0;
    $exchange->refund = 0;
    $exchange->postLoc = 0;
    $exchange->post = 0;
    $exchange->sender = 0;
    $exchange->stamp = '';
    $exchange->wholeError = 0;
    $exchange->mfError = 0;
    $exchange->feedbackScore= 0;
    $exchange->delissue = 0;
    $exchange->defects = 0;
    $exchange->g_id = 0;
    $exchange->g_num = 0;
    $exchange->g_desc = 0;
    $exchange->g_ship = 0;
    $exchange->oos = 0;
    $exchange->mgmt = 0;
    $exchange->processed = 0;
    $exchange->paidBeds = 0;
    $exchange->pTime = 0;
    $exchange->refundBox = 0;
    $exchange->hold = 0;
    $exchange->missed = 0;
    $exchange->urgent = 0;
    $exchange->retention = 0;
    $exchange->paypal = 0;
    $exchange->reopen = 0;
    $exchange->paid_supplier= 0;
    $exchange->invoice_made = 0;
    $exchange->seller_paid = 0;
    $exchange->discount_amount = 0.00;
    $exchange->discount_rate= 0.00;
    $exchange->vat_paid = 0.00;
    $exchange->paypal_payment = '';
    $exchange->last_trans_id= '0';
    $exchange->to_collect = 0;
    $exchange->is_split = 0;
    $exchange->voucher_code = '';
    $exchange->is_shipped = 0;
    $exchange->paid_courier = 0;
    $exchange->tnt_consignment = '';
    $exchange->bedtatstic_tracking_number = '';
    $exchange->is_collected = 0;
    $exchange->escalate = 0;
    $exchange->ordered = 0;
    $exchange->merch_ref = '';
    $exchange->save();

    $items = $request->get('items');
    $collect = $items['collect'];
    $send = isset($items['send']) ? $items['send'] : [];

    foreach($collect as $c) {
      $stock = StockItem::itemCode($c['itemCode']);
      $orderItem = new OrderItems;
      $orderItem->notes = $c['note'];
      $orderItem->itemid = $stock->id;
      $orderItem->oid = $exchange->id;
      $orderItem->id_staff = Auth::user()->id;
      $orderItem->staffName = Auth::user()->email;
      $orderItem->itemStatus = 'Allocated';
      $orderItem->currStock = 10000;
      $orderItem->blocked = $stock->blocked;
      $orderItem->discount = 0;
      $orderItem->lineTotal = 0.00;
      $orderItem->price = 0.00;
      $orderItem->Qty = 1;
      $orderItem->costs = 0.00;
      $orderItem->save();
      StockItem::addToOrder($stock->id);
    }

    foreach($send as $s) {
      $stock = StockItem::itemCode(html_entity_decode($s['itemCode']));
      $orderItem = new OrderItems;
      $orderItem->notes = $s['note'];
      $orderItem->itemid = $stock->id;
      $orderItem->oid = $exchange->id;
      $orderItem->id_staff = Auth::user()->id;
      $orderItem->staffName = Auth::user()->email;
      $orderItem->itemStatus = 'Allocated';
      $orderItem->currStock = $stock->itemQty;
      $orderItem->blocked = $stock->blocked;
      $orderItem->discount = 0;
      $orderItem->lineTotal = 0.00;
      $orderItem->price = 0.00;
      $orderItem->Qty = $s['qty'];
      $orderItem->costs = 0.00;
      $orderItem->save();
      StockItem::addToOrder($stock->id);
    }

    self::setDeliveryRoute($exchange->id);
    self::setBarcode($exchange->id);

    $type = (count($send)==0) ? 'Collection' : 'Exchange' ;
    OrderNotes::addNote("Created $type <a href='/orders/$exchange->id'>$exchange->id</a>", $id);
    OrderNotes::addNote("$type linked to order <a href='/orders/$id'>$id</a>", $exchange->id);
  }


	/**
     *
     */
    public static function missedParts($request, $id)
    {


        $order = Order::find($id)->toArray();

        unset($order['id']);
		unset($order['code']);
		unset($order['parcelid']);
        $order['total'] = 0;
        $order['paid']  = 0;
		$order['orderType'] = "Retail";
		$order['id_order_type'] = OrderType::id_type('Retail');
        $order['orderStatus'] = 'Allocated';
		$order['id_status'] = OrderStatus::id_status("Allocated");
        $order['startDate'] = Time::date()->get('d-M-Y');
        $order['startTime'] = Time::date()->get('H:i:s');
		$order['endDate'] = '';
        $order['endTime'] = '';
        $order['time_stamp'] = Time::date()->get("Y-m-d H:i:s");
        $order['startStamp'] = Time::date()->getStamp();
        $order['stamp'] = '';

        $suppliers = [0];
		if(in_array($order['id_supplier'], $suppliers)){
			$order['id_supplier']=17;
		}


		$order = new Order($order);
		$order->save();
		Count::setIno($order->id);


		$item = StockItem::itemCode('Missed Parts');
		$itemid = $item['id'];


		foreach($request->get('data') as $part)
		{
			$item = new orderItems;
			$item->itemid = $itemid;
			$item->oid = $order->id;
			$item->Qty = 1;
			$item->lineTotal = 0;
			$item->itemStatus='Allocated';
			$item->staffName = Auth::user()->email;
			$item->currStock = 10000;
			$item->price = 0;
			$item->notes =$part;
			$item->save();
		}

        OrderNotes::addNote("Missed Parts Created <a href='/orders/$order->id'>$order->id</a>",$id);

        OrderNotes::addNote("Missed Parts linked to order <a href='/orders/$id'>$id</a>",$order->id);

        if ($order->direct == 1) {

            $direct = Direct::forOrder($id)->toArray();
			if($direct){
				$direct['oid']=$order->id;
				$direct = new Direct($direct);
				$direct->save();
			}
		}

		self::setDeliveryRoute($order->id);
		self::setBarcode($order->id);

        return $order->id;
    }


	public static function missed_part_orders($queryStrings){
		$query = static::queryForMissedParts();
		if (count($queryStrings) > 0) {
			$query = parent::modForQueryString($query, $queryStrings);
		}
		$query->where(['itemid' => 615 , 'orderStatus' => 'Allocated']);
		return $query->orderBy('orders.id', 'desc')->paginate($queryStrings['limit']);

	}

	public static function postManifest($ids){
		$post =  explode("|",$ids);

		 $post = self::queryForPost()
					->whereIn('orders.id',$post)
					->addSelect(\DB::raw('group_concat(orderItems.notes) as items'))
					->join('orderItems','orders.id','=','orderItems.oid')
					->groupBy('orders.id')
					->get()
					->toArray();

					$filename=date('Y-m-d').".csv";
					$file_path=storage_path().'/'.$filename;
					$file = fopen($file_path,"w+");
					foreach ($post as $exp_data){
						fputcsv($file,$exp_data);
					}

					fclose($file);
					$headers = ['Content-Type' => 'application/csv'];
					return response()->download($file_path,$filename,$headers );
	}

	public static function queryForUnconfirmed(){
		$datefrom = '2017-03-01';
		return self::select(
					'orders.id',
					'orders.startDate',
					'customers.dBusinessName as name',
					'customers.dPostcode as postcode'
				)
			   ->join('customers','orders.cid','=','customers.id')
			   ->where(\DB::raw("date(str_to_date(startDate,'%d-%b-%Y'))"),'>',$datefrom)
			   ->where('orders.done',0)
			   ->where('id_status',2);
	}

	public static function unconfirmed($queryStrings){
		$query = static::queryForUnconfirmed();
		if (count($queryStrings) > 0) {
			$query = parent::modForQueryString($query, $queryStrings);
		}
		return $query->paginate($queryStrings['limit']);
	}



	public static function hasRole($role){
		return \Entrust::hasRole($role);
	}


}
