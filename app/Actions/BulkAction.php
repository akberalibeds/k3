<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Actions;

/**
 * Description of BulkAction
 *
 * @author chris
 */
class BulkAction {
  
  function __construct(array $options=[]) {
    if (isset($options['params'])) {
      $this->commonParams+=$options['params'];
    }
    if (isset($options['values'])) {
      $this->commonParams += array_keys($options['values']);
      $this->setParams($options['values']);
    }
  }
  
  protected $commonParams = [];
  protected $itemParams = [];
  
  private $sessionid=null;
  private $testMode=false;
  private $paramValues = [];
  private $inputItems = [];
  
  private $results = [];
  
  private $errors = [];
   
  function process($item) {
    return null;
  }
  
  function setup() {
    
  }
  
  function cleanup() {

  }
  
  public function testRun(Callable $function=null) {
    \DB::beginTransaction();
      $this->run($function);
    \DB::rollBack();
    $this->testMode=true;
    
  }
  public function liveRun(Callable $function=null) {
    $this->run();
    $this->testMode=false;
  }
  
  protected function run(Callable $function=null) {
    
    if ($function) {
      $function = $function->bindTo($this);
    }
    $this->setup();
    
    $count = count($this->inputItems);
    
    for ($i=0;$i<$count;$i++) {
      $item = $this->inputItems[$i];
      try {
        if ($function) {
          $result = $function($item);
        } else {
          $result = $this->process($item);
        }
        $this->results[$i] = $result;
      } catch (\Exception $ex) {
        $results[$i] = null;
        $this->errors[$i] = $ex->getMessage();
      }

    }
    $this->cleanup();
  }
  
  function __get($name) {
    return $this->paramValues[$name];
  }
  function __set($name, $value) {
    $this->setParam($name,$value);
  }
  function setParam($key, $value) {
    if (in_array($key,$this->commonParams)) {
      $this->paramValues[$key]=$value;
    }
  }
  function setParams(array $params) {
    foreach ($params as $key=>$value) {
      $this->setParam($key,$value);
    }
  }
  
  function addItem($item) {
    $this->inputItems[]=$item;
  }
  
  function success() {
    return count($this->getFailures())==0;
  }
  
  function getSuccess() {
    $count = count($this->inputItems);
    $result=[];
    for ($i=0;$i<$count;$i++) {
      if (!isset($this->errors[$i])) {
        $result[] = (object)['item'=>$this->inputItems[$i], 'result'=>$this->results[$i]];
      }
    }
    return $result;
  }
  
  function getFailures() {
    $count = count($this->inputItems);
    $result=[];
    for ($i=0;$i<$count;$i++) {
      if (isset($this->errors[$i])) {
        $result[] = (object)['item'=>$this->inputItems[$i], 'message'=>$this->errors[$i]];
      }
    }
    return $result;
    
  }
  
  function testMode() {
    return $this->testMode;
  }
  
  function save() {
    if (!$this->sessionid) {
      $this->sessionid = uniqid(__CLASS__);
    }
    session([$this->sessionid=>$this]);
    return $this->sessionid;
  }
  
  function sessionid() {
    return $this->sessionid;
  }
  
}
