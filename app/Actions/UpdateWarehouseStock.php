<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Actions;

use App\WarehouseStock;

/**
 * Description of UpdateWarehouseStock
 *
 * @author chris
 */
class UpdateWarehouseStock extends BulkAction {
  
  protected $commonParams = ['fromWarehouse','toWarehouse','reason','thisWarehouse','twoway','sign'];
  
  function process($data) {
    $item = \App\StockItem::where('itemCode',$data['itemCode'])->first();
    $qty = $data['qty'];
    if ($item && is_numeric($qty)) {
      if ($this->twoway) {
        return WarehouseStock::transferStock($item->stockParts, $this->fromWarehouse, $this->toWarehouse, $qty);
      } else {
        return WarehouseStock::alterStock($item->stockParts, $this->thisWarehouse, $qty * $this->sign, $this->reason);
      }
    }    
  }
  
}
