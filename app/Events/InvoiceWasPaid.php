<?php
// @author Giomani Designs (Development Team)
//
namespace App\Events;

use App\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class InvoiceWasPaid extends Event
{
  use SerializesModels;
  /**
   * @var
   */
  public $invoiceId;
  /**
   * @var
   */
  public $orderId;
  /**
   * @author Giomani Designs (Development Team)
   * @param  int $invoiceId
   * @param  int $orderId
   * @return void
   */
  public function __construct($invoiceId, $orderId)
  {
    $this->invoiceId = $invoiceId;
    $this->orderId = $orderId;
  }
  /**
   * @author Giomani Designs (Development Team)
   * @return array
   */
  public function broadcastOn()
  {
    return [];
  }
}
