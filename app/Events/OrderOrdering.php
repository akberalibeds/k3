<?php
// @author Giomani Designs (Development Team)
//
namespace App\Events;

use App\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class OrderOrdering extends Event
{
  use SerializesModels;
  /**
   * @var
   */
  public $orderId;
  /**
   * @author Giomani Designs (Development Team)
   * @return void
   */
  public function __construct($orderId) {
    $this->orderId = $orderId;
  }
  /**
   * @author Giomani Designs (Development Team)
   * @return array
   */
  public function broadcastOn() {
    return [];
  }
}
