<?php
// @author Giomani Designs (Development Team)
//
namespace App\Events;

use App\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class CreditNoteAdding extends Event
{
  use SerializesModels;
  /**
   * @var
   */
  public $customer;
  /**
   * @var
   */
  public $id;
  /**
   * @var
   */
  public $orderId;
  /**
   * @var
   */
  public $value;
  /**
   * @author Giomani Designs (Development Team)
   * @return void
   */
  public function __construct($id, $value, $customer, $orderId) {
    $this->id = $id;
    $this->value = $value;
    $this->customer = $customer;
    $this->orderId = $orderId;
  }
  /**
   * @author Giomani Designs (Development Team)
   * @return array
   */
  public function broadcastOn() {
    return [];
  }
}
