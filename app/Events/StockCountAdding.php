<?php
// @author Giomani Designs (Development Team)
//
namespace App\Events;

use App\ActionLog;
use App\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class StockCountAdding extends Event
{
  use SerializesModels;
  /**
   * @var
   */
  public $id;
  /**
   * @var
   */
  public $is;
  /**
   * @var
   */
  public $itemCode;
  /**
   * @var
   */
  public $was;
  /**
   * @author Giomani Designs (Development Team)
   * @return void
   */
  public function __construct($id, $is, $itemCode, $was) {
    $this->id = $id;
    $this->is = $is;
    $this->itemCode = $itemCode;
    $this->was = $was;
  }
  /**
   * @author Giomani Designs (Development Team)
   * @return array
   */
  public function broadcastOn() {
    return [];
  }
}
