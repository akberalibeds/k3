<?php
// @author Giomani Designs (Development Team)
//
namespace App\Events;

use App\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class UserDetailsChange extends Event
{
  use SerializesModels;
  /**
   *
   */
  public $details;
  /**
   *
   */
  public $userId;
  /**
   * @author Giomani Designs (Development Team)
   * @param string $details
   * @param int $userId - the user id of the details being changed
   * @return void
   */
  public function __construct($details, $userId) {
    $thiks->details = $details;
    $thiks->userId = $userId;
  }

  /**
   * @author Giomani Designs (Development Team)
   * @return array
   */
  public function broadcastOn() {
    return [];
  }
}
