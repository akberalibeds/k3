<?php
// @author Giomani Designs (Development Team)
//
namespace App\Events;

use App\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class UserRolesPermissionsChange extends Event
{
  use SerializesModels;
  /**
   *
   */
  public $added;
  /**
   *
   */
  public $removed;
  /**
   *
   */
  public $userId;
  /**
   * @author Giomani Designs (Development Team)
   * @param  array|string $added - the roles added to the user
   * @param  array|string $removed - the roles removed from the user
   * @param  array|string $userId - the user id of the user being changed
   * @return void
   */
  public function __construct($added, $removed, $userId) {
    $this->added = implode('|', $added);
    $this->removed = implode('|', $removed);
    $this->userId = $userId;
  }
  /**
   * @author Giomani Designs (Development Team)
   * @return array
   */
  public function broadcastOn()
  {
    return [];
  }
}
