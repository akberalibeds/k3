<?php
// @author Giomani Designs (Development Team)
//
namespace App\Events;

use App\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class SellerStockAdding extends Event
{
  use SerializesModels;
  /**
   * @var
   */
  private $itemCode;
  /**
   * @var
   */
  private $price;
  /**
   * @var
   */
  private $seller;
  /**
   * @author Giomani Designs (Development Team)
   * @return void
   */
  public function __construct($itemCode, $price, $seller) {
    $this->itemCode =$itemCode ;
    $this->price = $price;
    $this->seller = $seller;
  }

  /**
   * @author Giomani Designs (Development Team)
   * @return array
   */
  public function broadcastOn() {
    return [];
  }
}
