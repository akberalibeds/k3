<?php
// @author Giomani Designs (Development Team)
//
namespace App\Events;

use App\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class FreeSaturdayCancelling extends Event
{
  use SerializesModels;
  /**
   * @var
   */
  public $id;
  /**
   * @var
   */
  public $saturday;
  /**
   * @author Giomani Designs (Development Team)
   * @return void
   */
  public function __construct($id, $saturday) {
    $this->id = $id;
    $this->saturday = $saturday;
  }
  /**
   * @author Giomani Designs (Development Team)
   * @return array
   */
  public function broadcastOn() {
    return [];
  }
}
