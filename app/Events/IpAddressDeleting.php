<?php
// @author Giomani Designs (Development Team)
//
namespace App\Events;

use App\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class IpAddressDeleting extends Event
{
  use SerializesModels;
  /**
   * @var
   */
  private $ipAddress;
  /**
   * @author Giomani Designs (Development Team)
   * @return void
   */
  public function __construct($ipAddress) {
    $this->ipAddress = $ipAddress;
  }
  /**
   * @author Giomani Designs (Development Team)
   * @return array
   */
  public function broadcastOn() {
    return [];
  }
}
