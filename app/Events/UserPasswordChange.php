<?php
// @author Giomani Designs (Development Team)
//
namespace App\Events;

use Auth;
use App\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class UserPasswordChange extends Event
{
  use SerializesModels;
  /**
   *
   */
  public $userId;
  /**
   * Create a new event instance.
   *
   * @return void
   */
  public function __construct($id) {
    $this->userId =  $id;
  }
  /**
   * @author Giomani Designs (Development Team)
   * @return array
   */
  public function broadcastOn() {
    return [];
  }
}
