<?php
// @author Giomani Designs (Development Team)
//
namespace App\Events;

use App\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class FreeSaturdayAdding extends Event
{
  use SerializesModels;
  /**
   * @var
   */
  public $saturday;
  /**
   * @author Giomani Designs (Development Team)
   * @return void
   */
  public function __construct($saturday) {
    $this->saturday = $saturday;
  }
  /**
   * @author Giomani Designs (Development Team)
   * @return array
   */
  public function broadcastOn() {
    return [];
  }
}
