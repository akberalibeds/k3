<?php
// @author Giomani Designs (Development Team)
//
namespace App\Events;

use App\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class StockCountAdjustment extends Event
{
  use SerializesModels;
  /**
   * @var
   */
  public $id;
  /**
   * @var
   */
  public $is;
  /**
   * @var
   */
  public $itemCode;
  /**
   * @var
   */
  public $was;
  /**
   * @author Giomani Designs (Development Team)
   * @param  int $is - the current stock level (after alteration)
   * @param  int $stockItemId
   * @param  int $was - the stock level before the change
   * @return void
   */
  public function __construct($id, $is, $itemCode, $was) {
    $this->id = $id;
    $this->is = $is;
    $this->itemCode = $itemCode;
    $this->was = $was;
  }
  /**
   * @author Giomani Designs (Development Team)
   * @return array
   */
  public function broadcastOn()
  {
    return [];
  }
}
