<?php
// @author Giomani Designs (Development Team)
//
namespace App\Events;

use App\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class BaseEvent extends Event
{
  use SerializesModels;
  /**
   * @var
   */
  public $kvs;
  /**
   * @author Giomani Designs (Development Team)
   * @param  array  $kvs
   * @return void
   */
  public function __construct($kvs) {
    $this->kvs = $kvs;
  }
  /**
   * @author Giomani Designs (Development Team)
   * @return array
   */
  public function broadcastOn() {
    return [];
  }
}
