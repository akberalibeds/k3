<?php

namespace App\Events;

use App\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class UserSuspending extends Event
{
    use SerializesModels;

    /**
     * @var
     */
    public $userId;
    /**
     * @author Giomani Designs (Development Team)
     * @param  integer  $userId
     * @return void
     */
    public function __construct($userId) {
      $this->userId = $userId;
    }
    /**
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
