<?php
// @author Giomani Designs (Development Team)
//
namespace App\Events;

use App\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class MarkingAsDelivered extends Event
{
  use SerializesModels;
  /**
   * @var
   */
  public $ids;
  /**
   * @author Giomani Designs (Development Team)
   * @return void
   */
  public function __construct($ids) {
    $this->ids = $ids;
  }
  /**
   * @author Giomani Designs (Development Team)
   * @return array
   */
  public function broadcastOn() {
    return [];
  }
}
