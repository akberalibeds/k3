<?php
// @author Giomani Designs (Development Team)
//
namespace App\Events;

use App\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class SellerStockRemoving extends Event
{
  use SerializesModels;
  /**
   * @var
   */
  private $itemCode;
  /**
   * @var
   */
  private $seller;
  /**
   * @author Giomani Designs (Development Team)
   * @return void
   */
  public function __construct($itemCode, $seller) {
    $this->itemCode = $itemCode;
    $this->seller = $seller;
  }
  /**
   * @author Giomani Designs (Development Team)
   * @return array
   */
  public function broadcastOn() {
    return [];
  }
}
