<?php

namespace App\Events;

use App\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class OrderSupplierChanging extends Event
{
    use SerializesModels;
    /**
     * @var
     */
    public $orderId;
    /**
     * @var
     */
    public $supplierFrom;
    /**
     * @var
     */
    public $supplierTo;
    /**
     * @author Giomani Designs (Development Team)
     * @return void
     */
    public function __construct($orderId, $supplierFrom, $supplierTo) {
      $this->orderId = $orderId;
      $this->supplierFrom = $supplierFrom;
      $this->supplierTo = $supplierTo;
    }
    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn() {
        return [];
    }
}
