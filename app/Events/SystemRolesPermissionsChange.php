<?php
// @author Giomani Designs (Development Team)
//
namespace App\Events;

use App\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class SystemRolesPermissionsChange extends Event
{
  use SerializesModels;
  /**
   *
   */
  public $adds;
  /**
   *
   */
  public $removes;
  /**
   * @author Giomani Designs (Development Team)
   * @return void
   */
  public function __construct($adds, $removes) {
    $this->adds = ($adds === null) ? 'null' : implode(';', $adds);
    $this->removes = ($removes === null) ? 'null' : implode(';', $removes);
  }
  /**
   * @author Giomani Designs (Development Team)
   * @return array
   */
  public function broadcastOn() {
    return [];
  }
}
