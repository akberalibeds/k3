<?php

namespace App\Events;

use App\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class BulletinCreating extends Event
{
  use SerializesModels;
  /**
   * @var
   */
  public $bulletinId;
  /**
   * @var
   */
  public $title;
  /**
   * Create a new event instance.
   *
   * @return void
   */
  public function __construct($bulletinId, $title) {
    $this->bulletinId = $bulletinId;
    $this->title = $title;
  }
  /**
   * Get the channels the event should be broadcast on.
   *
   * @return array
   */
  public function broadcastOn() {
    return [];
  }
}
