<?php
// @author Giomani Designs (Development Team)
//
namespace App\Events;

use App\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class VehicleCostTypeAdding extends Event
{
  use SerializesModels;
  /**
   * @var
   */
  public $description;
  /**
   * @var
   */
  public $type;
  /**
   * @var
   */
  public $id;
  /**
   * @author Giomani Designs (Development Team)
   * @param string  $description
   * @param integer $id
   * @param string  $type
   *
   * @return void
   */
  public function __construct($description, $id, $type) {
    $this->description = $description;
    $this->type = $type;
    $this->id = $id;
  }
  /**
   * @author Giomani Designs (Development Team)
   * @return array
   */
  public function broadcastOn() {
    return [];
  }
}
