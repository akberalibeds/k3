<?php

namespace App;

use App\Helper\Traits\QueryableModel;
use Illuminate\Database\Eloquent\Model;
use Hash;

class Driver extends Model
{
  use QueryableModel;
  /**
   * @var
   */
  public static $rules = [
    'store' => [
      'active' => 'required|in:1,0',
      'name' => 'required|max:255|min:4',
      'num' => 'string|max:24',
      'pwd' => 'required|confirmed|max:64|min:4',
      'rate' => 'numeric|min:0|max:9999.99',
      'score' => 'numeric',
      'uName' => 'required|alpha_dash|max:64|min:4|unique:drivers',
    ],
    'update' => [
      'active' => 'required|in:1,0',
      'name' => 'required|max:255|min:4',
      'num' => 'max:24',
      'rate' => 'numeric|min:0|max:9999.99',
      'score' => 'numeric',
    ],
  ];
  /**
   * @author Giomani Designs (Development Team)
   * @param  array   $queryStrings the query string
   * @return Illuminate\Pagination\LengthAwarePaginator
   */
  public static function getAll($queryStrings) {
    $query = static::select('*');
    if (count($queryStrings) > 0) {
      $query = parent::modForQueryString($query, $queryStrings);
    }
    return $query->orderBY('name', 'asc')->paginate(config('search.rpp'));
  }
  
  /**
   * @author Giomani Designs (Development Team)
   * @param  array   $queryStrings the query string
   * @return Illuminate\Pagination\LengthAwarePaginator
   */
  public static function getActive() {
  	$query = static::select('name','num');
  	return $query->orderBY('name', 'asc')->get();
  }
  
  
  
  public static function login($request){
	

  	$driver = Driver::where(['uName'=> $request->get('username')])->first();
  	if(!$driver){
  		return ["error" => 'User/Password Error'];
  	}
  	 
  	if (!Hash::check($request->get('password'), $driver->pwd)) {
  		return ["error" => 'User/Password Error'];
  	}
  	
  	
  	unset($driver->pwd);
  	$data = $request->except('username','password');
  	$data['driver']=$driver->id;
  	Device::logDevice($data);
  	return $driver;
  }
  
  
}
