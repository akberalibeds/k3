<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Notifications\NotificationType;

class Notification extends Model
{
    protected $fillable = ['customerID', 'orderID', 'dispID', 'type', 'hasSent'];

    public function messages()
    {
        return $this->hasMany('App\NotificationMessage', 'notificationID');
    }

    public function customer()
    {
        return $this->belongsTo('App\Customer','customerID');
    }

    public function order()
    {
        return $this->belongsTo('App\Order', 'orderID');
    }
    
    public function typeInstance() {
      return NotificationType::instantiate($this->type);
    }

    public function shortName()
    {
        $type = studly_case((new \ReflectionClass($this->typeInstance()))->getShortName());
        
        return $type;
    }

    public function getCompletion()
    {
        $totalMsg = $this->messages->count();

        if(empty($totalMsg)) return 'none';
        if ($totalMsg == $this->messages()->where('is_sent', true)->count()) return 'completed';
        if ($totalMsg == $this->messages()->where('is_sent', false)->count()) return 'none';
        
        return 'partial';
    }
}
