<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Direct extends Model
{
  /**
   * The table associated with the model.
   *
   * @var string
   */
  protected $table = 'direct';
  

  public $timestamps = false;


  
  public static function forOrder($id)
  {
	return self::where(['oid' => $id])->first();  
  }
 
  
}
