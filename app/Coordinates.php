<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Settings;

class Coordinates extends Model
{
  
  /**
   * The table associated with the model.
   *
   * @var string
   */
  protected $table = 'coordinates';
  
  public $timestamps = false;
  
  public $fillable = ['pc','lat','lng','street','town'];
  
  
  public static function forPostcode($PC,$return_array=false)
  {
	  
		$PC = preg_replace('/\s*/', '', $PC);
		$PC = strtoupper($PC);
		
		$row = self::where('pc',$PC)->first();
		
		if(!$row)
		{
			$row = self::geocode($PC);
			$row['from']= "google";		
		}
		
		if($return_array)
		
			return $row;
		else	
			return json_encode($row);
		  
  }
  
  
 
  
  
  
  // function to geocode address, it will return false if unable to geocode address
  public static function geocode($address){
 
    // google map geocode api url
    $url = "http://maps.google.com/maps/api/geocode/json?address=".urlencode($address)."&key".Settings::first()->maps_key;
 
    // get the json response
    $resp_json = file_get_contents($url);
     
    // decode the json
    $resp = json_decode($resp_json, true);
 
    // response status will be 'OK', if able to geocode given address 
    if($resp['status']=='OK'){

        // get the important data
        $lat = $resp['results'][0]['geometry']['location']['lat'];
        $long = $resp['results'][0]['geometry']['location']['lng'];
        $street = $resp['results'][0]['address_components'][1]['long_name'];
		$town = $resp['results'][0]['address_components'][2]['long_name'];
         
        // verify if data is complete
        if($lat && $long && $street){
         
            // put the data in the array
                $data_arr =	[
							'lat' => $lat, 
							'lng' => $long, 
							'pc'  => $address,
							'street' => $street,
							'town' => $town,
                			];
            
				$row = self::where(['pc' => $address])->first();
				
				if(!$row){
            		$row = new self($data_arr);
					$row->save();
				}
				else{
					$row->town=$town;
					$row->street=$street;
					$row->save();	
				}
			 
			
			return $row;
           
        }else{
            return false;
        }
         
    }else{
        return false;
    }
}
  
  
}
