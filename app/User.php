<?php

namespace App;

use Auth;
use App\Helper\Traits\QueryableModel;
use Zizaco\Entrust\Traits\EntrustUserTrait;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
  use EntrustUserTrait, QueryableModel;
  /**
   * user statuses
   */
  const USER_STATUS_ACTIVE = 1;
  const USER_STATUS_SUSPENDED = 0;
  /**
   * @var array
   */
  public static $rules = [
    'update-password' => [
      'old_password' => 'required',
      'new_password' => 'required|confirmed',
    ]
  ];
   /**
    * @var array
    */
  public static $statuses = array (
    User::USER_STATUS_ACTIVE => 'active',
    User::USER_STATUS_SUSPENDED => 'suspended',
  );
   /**
    * @var array
    */
  public static $validStatuses = [User::USER_STATUS_ACTIVE, User::USER_STATUS_SUSPENDED];
  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
    'name', 'email', 'password',
  ];

  /**
   * The attributes that should be hidden for arrays.
   *
   * @var array
   */
  protected $hidden = [
    'password', 'remember_token',
  ];
  /**
   * @author Giomani Designs (Development Team)
   * @param  array   $queryStrings the query string
   * @return Illuminate\Pagination\LengthAwarePaginator
   */
  public static function getAll($queryStrings) {
    $query = static::queryForAll();
    if (count($queryStrings) > 0) {
      $query = parent::modForQueryString($query, $queryStrings);
    }
    return $query->orderBY('users.email')->paginate(config('search.rpp'));
  }
  /**
   * Return the user id, name and username of the users messages can be sent to
   *
   */
  public static function getMessageableUsers() {
    return User::select(
        'id',
        'name',
        'email'
      )
      ->where('id', '<>', Auth::user()->id)
      ->get();
  }
  protected static function queryForAll() {
    return static::select(
        'id',
        'name',
        'email',
        'created_at',
        'updated_at',
        'status'
      )
      ->with('roles')
      ->where('id', '<>', 54);
  }
  /**
   * Return the text for the status type
   * @param int $status
   */
  public static function statusText($status) {
    return static::$statuses[$status];
  }
}
