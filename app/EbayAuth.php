<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EbayAuth extends Model
{
   
   
     protected $table = 'ebayAccounts';
     public $timestamps = false;
   	 protected $fillable = ['name', 'appID', 'certID', 'devID', 'token', 'folder', 'compat_level', 'last_edit'];
   
   
   
}
