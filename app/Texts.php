<?php

namespace App;

use DB;
use Time;
use Illuminate\Database\Eloquent\Model;

class Texts extends Model
{
	
	
  /*
   * The table associated with the model.
   *
   * @var string
   *
   */
   
  protected $table = 'txts';
  
  public $fillable = ['num','oid','date'];
    
  public $timestamps  = false;
  
  
  
  public static function add($customer)
  {
		$text = new self;
		$text->num = $customer->tel;
		$text->oid = $customer->oid;
		$text->date = Time::date()->get('d-M-Y');
		$text->save();  
  }
  
  
}
