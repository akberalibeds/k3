<?php

namespace App;

use DB;
use App\Helper\Traits\QueryableModel;
use Illuminate\Database\Eloquent\Model;

class OrderItems extends Model
{
    use QueryableModel;
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'orderItems';

    public $timestamps = false;

    public $fillable = ['id', 'id_staff', 'id_item_status', 'itemid', 'oid', 'bedsName', 'blocked', 'currStock', 'discount', 'itemStatus', 'lineTotal', 'notes', 'price', 'Qty', 'staffName', 'costs'];

    public function stock()
    {
        return $this->belongsTo(StockItem::class, 'itemid');
    }

    public function order()
    {
        return $this->belongsTo(Order::class, 'oid');
    }

    /**
     * Get Initial Stock Load Count Query Data
     * @param Boolean Stock Parts to add
     * @return Query Portion of the Load Count Data 
     */
    private static function getStockLoadCountData($parts)
    {
        if ($parts === 'true') {
            return OrderItems::select(
                'itemid',
                'itemCode',
                'pieces',
                'warehouse',
                'stock_parts.parent_id',
                'stock_parts.id',
                'stock_parts.part',
                DB::raw('sum(Qty) tot')
            )
                ->join('stock_items', 'orderItems.itemid', '=', 'stock_items.id')
                ->join('stock_parts', 'stock_items.id', '=', 'stock_parts.parent_id');
        } else {
            return OrderItems::select('itemid', 'itemCode', 'pieces', 'warehouse', DB::raw('sum(Qty) tot'))
                ->join('stock_items', 'orderItems.itemid', '=', 'stock_items.id');
        }
    }

    /**
     * Exclude specific Items from the query
     * @param Query Current Requested Query
     * @return Query Updated Query
     */
    private static function excludeItemsCondition($query)
    {
        $query->where('label', 1)
            ->where('cat', 'not like', '%collect%')
            ->where('itemCode', 'not like', '%collect%')
            ->whereNotIn('cat', ['birlea', 'seconique', 'durest beds']);

        return $query;
    }

    public static function getStockLoadCountMissed($date, $warehouse, $parts)
    {
        $fromDate = \Carbon\Carbon::parse($date);
        $toDate = $fromDate->subDays(7);

        $query = self::getStockLoadCountData($parts);
        $query->addSelect('orderItems.oid', 'orderItems.notes') //@todo Check about OrderItems.notes
            ->join('disp', 'disp.oid', '=', 'orderItems.oid');


        $query
            ->whereBetween('disp.date', [$fromDate->format('d-M-Y'), $toDate->format('d-M-Y')])
            ->where('itemCode', '=', 'missed parts');

      // Where clauses
        if ($warehouse !== 'all' || $warehouse !== 'per' ) {
            $query->where('warehouse', '=', $warehouse);
        }
        return $query;
    }

    /**
     * Get Today's Load Count
     * 
     * @param String User Date Selected
     * @param String User Warehouse Selection (all is a known value)
     * @param Boolean Add Stock Parts Condition to query
     * 
     * @return Query Load count for the Week/Day
     */
    public static function getStocksLoadCount($date, $warehouse, $parts)
    {
        $fromDate = \Carbon\Carbon::parse($date);
        $toDate = $fromDate->subDays(7);

        $query = self::getStockLoadCountData($parts);

        $query
            ->join('disp', 'disp.oid', '=', 'orderItems.oid');

      // Where clauses
        if ($warehouse != 'all' && $warehouse !== 'per' ) {
            $query->where('warehouse', '=', $warehouse);
        }

        $query->whereBetween('disp.date', [$fromDate->format('d-M-Y'), $toDate->format('d-M-Y')]);
        $query->whereNotIn('disp.route', function ($query) {
            $query->select('name')
                ->from(with(new Routes)->getTable())
                ->where('in_load_count', '=', 0);
        });
        $query = self::excludeItemsCondition($query);

      // Groups And Sorting
        if ($parts === 'true') {
            if ($warehouse !== 'all') {
                $query
                    ->groupBy('itemid', 'stock_parts.id', 'part')
                    ->havingRaw('sum(Qty) > 0')
                    ->orderBy('itemCode', 'asc')
                    ->orderBy('warehouse', 'asc')
                    ->orderBy('stock_parts.id', 'asc');
            } else {
                $query
                    ->groupBy('itemid', 'stock_parts.id', 'part')
                    ->havingRaw('sum(Qty) > 0')
                    ->orderBy('stock_parts.id', 'asc')
                    ->orderBy('itemCode', 'asc');
            }
        } else {
            if ($warehouse !== 'all') {
                $query
                    ->groupBy('itemid')
                    ->havingRaw('sum(Qty) > 0')
                    ->orderBy('itemCode', 'asc')
                    ->orderBy('warehouse', 'asc');
            } else {
                $query
                    ->groupBy('itemid')
                    ->havingRaw('sum(Qty) > 0')
                    ->orderBy('itemCode', 'asc');
            }
        }

        return $query;
    }


    /**
     * Get All Load Counts for the Morning
     * 
     */
    public static function getStocksLoadCountM($date, $warehouse, $parts)
    {
        $fromDate = \Carbon\Carbon::parse($date);
        $toDate = $fromDate->subDays(7);

        $query = null;
        // Selection and Joins
        if ($parts === 'true') {
            $query = OrderItems::select('itemid', 'itemCode', 'pieces', 'warehouse', 'stock_parts.parent_id', 'stock_parts.id', 'stock_parts.part', DB::raw('sum(Qty) tot'))
                ->join('stock_items', 'orderItems.itemid', '=', 'stock_items.id')
                ->join('stock_parts', 'stock_items.id', '=', 'stock_parts.parent_id');
        } else {
            $query = OrderItems::select('itemid', 'itemCode', 'pieces', 'warehouse', DB::raw('sum(Qty) tot'))
                ->join('stock_items', 'orderItems.itemid', '=', 'stock_items.id');
        }

        // Where clauses
        if ($warehouse !== 'all' || $warehouse !== 'per') {
            $query->where('warehouse', '=', $warehouse);
        }
            // Check with if needed within 7 days or need within a specific day
        $query
            ->join('orders', 'orderItems.oid', '=', 'orders.id')
            ->whereBetween('orders.deliverBy', [$fromDate->format('d-M-Y'), $toDate->format('d-M-Y')]);

        $query->whereNotIn('orders.delRoute', function ($query) {
            $query->select('name')
                ->from(with(new Routes )->getTable())
                ->where('in_load_count', '=', 0);
            });
        $query = self::excludeItemsCondition($query);

        if ($parts === 'true') {
            if ($warehouse !== 'all') {
                $query
                    ->groupBy('itemid', 'id', 'part')
                    ->havingRaw('sum(Qty) > 0')
                    ->orderBy('itemCode', 'asc')
                    ->orderBy('warehouse', 'asc')
                    ->orderBy('id', 'asc');
            } else {
                $query
                    ->groupBy('itemid', 'id', 'part')
                    ->havingRaw('sum(Qty) > 0')
                    ->orderBy('id', 'asc')
                    ->orderBy('itemCode', 'asc');
            }
        } else {
            if ($warehouse !== 'all') {
                $query
                    ->groupBy('itemid')
                    ->havingRaw('sum(Qty) > 0')
                    ->orderBy('itemCode', 'asc')
                    ->orderBy('warehouse', 'asc');
            } else {
                $query
                    ->groupBy('itemid')
                    ->havingRaw('sum(Qty) > 0')
                    ->orderBy('itemCode', 'asc');
            }
        }
        return $query;
    }

    /**
     * @author Giomani Designs (Development Team)
     * @param  integer    $id
     * @param  string     $queryStrings the query string
     * @param  integer    $statusId the status of the order (defaults to 2 - Allocated)
     * @return Illuminate\Pagination\LengthAwarePaginator
     */
    public static function getOrdersFor($id, $queryStrings, $statusId = 2)
    {
        $query = static::queryForOrdersFor($id, $statusId);
        if (count($queryStrings) > 0) {
            $query = parent::modForQueryString($query, $queryStrings);
        }
        return $query->orderBy('orders.id', 'desc')->paginate(config('search.rpp'));
    }
    /**
     * @author Giomani Designs (Development Team)
     * @param  integer    $id
     * @param  integer    $statusId the status of the order (defaults to 2 - Allocated)
     * @return Illuminate\Pagination\LengthAwarePaginator
     */
    public static function queryForOrdersFor($id, $statusId = 2)
    {
        return static::select(
            'orders.id',
            'orders.cid',
            'orders.iNo',
            'orders.orderStatus',
            'orders.orderType',
            'orders.paid',
            'orders.staffName',
            'orders.startDate',
            'orders.total',
            'customers.userID',
            'customers.businessName',
            'customers.dBusinessName',
            'customers.postcode',
            'customers.dPostcode'
        )
            ->leftJoin('orders', 'orders.id', '=', 'orderItems.oid')
            ->leftJoin('customers', 'customers.id', '=', 'orders.cid')
            ->where('orderItems.itemid', '=', $id)
            ->where('id_status', '=', $statusId);
    }

    public static function onOrder($id)
    {
        return self::where('oid', $id)
            ->join('stock_items', 'orderItems.itemid', '=', 'stock_items.id')
            ->select('orderItems.*', 'stock_items.itemCode', 'stock_items.itemDescription')
            ->get();
    }

  //--------- Old Variation of Conditions ------------//

    /**
     * @return Illuminate\Database\Eloquent\Collection
     */
    public static function getLoadCounts($date, $warehouse)
    {
        $query = OrderItems::select(
            'label',
            'itemid',
            'itemCode',
            'pieces',
            'warehouse',
            DB::raw('sum(Qty) tot')
        )
            ->join('stock_items', 'orderItems.itemid', '=', 'stock_items.id')
            ->join('disp', 'disp.oid', '=', 'orderItems.oid')
            ->where('disp.date', '=', $date)
            ->whereNotIn('disp.route', function ($query) {
                $query->select('name')
                    ->from(with(new Routes)->getTable())
                    ->where('in_load_count', '=', 0);
            })
            ->where('label', 1)
            ->where('cat', 'not like', '%collect%')
            ->where('itemCode', 'not like', '%collect%')
            ->whereNotIn('cat', ['birlea', 'seconique', 'durest beds'])
            ->groupBy('itemid')

            ->orderBy('warehouse', 'asc');
        if ($warehouse !== 'all') {
            $query->where('warehouse', '=', $warehouse);
        }
        return $query->get();
    }

    /**
     * @author Giomani Designs (Development Team)
     * @return Illuminate\Database\Eloquent\Collection
     */
    public static function getLoadCountsM($date)
    {
        return static::select(
            'label',
            'itemid',
            'itemCode',
            'pieces',
            DB::raw('sum(Qty) tot')
        )
            ->join('stock_items', 'orderItems.itemid', '=', 'stock_items.id')
            ->join('orders', 'orderItems.oid', '=', 'orders.id')
            ->where('orders.deliverBy', '=', $date)
            ->whereNotIn('orders.delRoute', function ($query) {
                $query->select('name')
                    ->from(with(new Routes)->getTable())
                    ->where('in_load_count', '=', 0);
            })
            ->where('label', 1)
            ->whereNotIn('cat', ['birlea', 'seconique', 'durest beds'])
            ->where('itemCode', 'not like', '%collect%')
            ->where('cat', 'not like', '%collect%')
            ->groupBy('itemid')
            ->having('tot', '>', '0')
            ->orderBy('warehouse', 'asc')
            ->get();
    }

    /*
     * get the missed parts counts
     *
     * @return Illuminate\Pagination\LengthAwarePaginator
     */
    public static function getMissedCounts($date)
    {
        return OrderItems::select('orderItems.oid', 'orderItems.notes')
            ->join('stock_items', 'orderItems.itemid', '=', 'stock_items.id')
            ->join('disp', 'disp.oid', '=', 'orderItems.oid')
            ->where('disp.date', '=', $date)
            ->where('itemCode', '=', 'missed parts')
            ->get();
    }

}
