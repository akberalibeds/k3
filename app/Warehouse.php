<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Warehouse extends Model
{
	
	protected $table = "warehouses";
	
	
	public $timestamps = false;
	
	
	
	
	public static function getWarehouses(){
	
		return self::whereNotIn('id',[3,4,5,6,7])->get();
		
	}
	
}
