<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class PDA extends Model
{
	
	
	protected $table ="pda_data";
	
	
	
	public static function getPdaData(){
		
		return self::find(1)->toArray();
		
	}
	
	
}