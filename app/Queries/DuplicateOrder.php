<?php

namespace App\Queries;

use App\Order;
use Illuminate\Support\Facades\DB;

class DuplicateOrder
{
    public function all()
    {   
        //Days back
        $date = \Carbon\Carbon::now()->subDays(2);

        $groupFields = ['beds','ouref','companyName'];

        $duplicates =  DB::table('orders')
                ->select('*')
                ->join(DB::raw("(SELECT "
                    .implode(',',$groupFields)."  FROM orders
                    WHERE time_stamp > '$date' and beds<>'' AND beds <>'0'
                    GROUP BY " .implode(',',$groupFields)."
                    HAVING count(*) > 1
                    ) as x"),function($join) use($groupFields){
                    
                    foreach($groupFields as $field) {
                        $join->on("orders.".$field, "=", "x.".$field);
                    }
                });

                foreach($groupFields as $field) {
                    $duplicates->orderBy('x.'.$field);
                 }
                 
                $duplicates->where('time_stamp', '>', $date);

                $results = $duplicates->paginate(50);

                $groupnumber = 0;
                $current_vals = array_flip($groupFields);

                foreach($results as &$duplicate) {
                    $match = true;
                    foreach ($groupFields as $field) {
                        if ($duplicate->$field != $current_vals[$field]) {
                            $match=false;
                        }
                        $current_vals[$field]=$duplicate->$field;
                    }
                    if (!$match) {
                        $groupnumber+=1;
                    }
                    $duplicate->groupnumber=$groupnumber;
                }

                return $results;
    }

}