<?php

namespace App\Classes;


use DB;
use Auth;
use App\Order;
use App\Count;
use App\Company;
use App\StockItem;
use App\Settings;
use App\OrderType;
use App\OrderItems;
use App\OrderNotes;
use App\OrderStatus;
use App\PaymentType;
use App\OrderPayment;
use App\Helper\Uid;
use Time;
use App\Customer;
use Symfony\Component\Console\Tests\CustomApplication;
use App\Routes;
use App\EpdqOrders;

class Sale
{
	private $id;
	private $cid;
	private $items;
	public $expressMail = false;
	public $splitMail = false;

  public function __construct(){


  }

  public function newSale($order){

		$data = new Order;

		$data["staffName"]=(isset($order->staffName)) ? $order->staffName : Auth::user()->email;
		$data["staffid"]= (isset($order->staffid)) ? $order->staffid : Auth::user()->id;
		$data["orderStatus"] = (isset($order->orderStatus)) ? $order->orderStatus : "Allocated";
		$data["lastMaintainID"] = (isset($order->lastMaintainID)) ? $order->lastMaintainID : 0 ;
		$data["orderType"] = (isset($order->orderType)) ? $order->orderType : "retail";
		$data["delOrder"] = (isset($order->delOrder)) ? $order->delOrder : 0;
		$data["deliverBy"] = (isset($order->deliverBy)) ? $order->deliverBy : '0';
		$data["startDate"]= (isset($order->startDate)) ? $order->startDate : Time::date()->get("d-M-Y");
		$data["startTime"]= (isset($order->startTime)) ? $order->startTime : Time::date()->get("H:i:s");
		$data["startStamp"]= (isset($order->startStamp)) ? $order->startStamp : Time::date()->getStamp();
		$data["iNo"]= (isset($order->iNo)) ? $order->iNo : 0;
		$data["cid"]= (isset($order->cid)) ? $order->cid : $order->cid;
		$data["direct"]= (isset($order->direct)) ? $order->direct : 0;
		$data["carrier"]= (isset($order->carrier)) ? $order->carrier : "own";
		$data["paymentType"]=(isset($order->paymentType)) ? $order->paymentType : '';
		$data["cardNo"]=(isset($order->cardNo)) ? $order->cardNo : '';
		$data["authNo"]=(isset($order->authNo)) ? $order->authNo : '';
		$data["exp"]=(isset($order->exp)) ? $order->exp : '';
		$data["dispatchType"]=(isset($order->dispatchType)) ? $order->dispatchType : 'Delivery';
		$data["companyName"]= (isset($order->companyName)) ? $order->companyName : '' ;
		$data["id_company"] = Company::id_company($order->companyName);
		$data["ouref"]= (isset($order->ouref)) ? $order->ouref : '';
		$data["proforma"]=0;
		$data["total"]=(isset($order->total)) ? $order->total : 0 ;
		$data["vatAmount"]=(isset($order->vatAmount)) ? $order->vatAmount : Settings::getVat();
		$data['beds'] = (isset($order->beds)) ? $order->beds : '0';
		$data['id_status'] = (isset($order->id_status)) ? $order->id_status : 2;
		
		
		//$orderNumber = Uid::nextOrderNumber(Uid::REQUEST_SOURCE_STAFF);
		//$data['order_number_part_0'] = $orderNumber[0];
		//$data['order_number_part_1'] = $orderNumber[1];
		//$data['order_number_part_2'] = $orderNumber[2];
		//$data['order_number_part_3'] = $orderNumber[3];
		//$data['order_number_part_4'] = $orderNumber[4];

		$data["paid"] = (isset($order->paid)) ? $order->paid : 0;
		$data['paid'] = ($data['paymentType']=="Cash") ? 0 : $data['paid'];

		$data["id_order_type"]=OrderType::id_type($data['orderType']);
		$data["last_trans_id"]=(isset($order->last_trans_id)) ? $order->last_trans_id : '0' ;
		$data["beds"]= (isset($order->beds)) ? $order->beds : '0' ;
		$data["merch_ref"]= (isset($order->merch_ref)) ? $order->merch_ref : '' ;

		$data->save();
		
		$this->id = $data->id;
		$this->cid= $data->cid;
		$this->items = json_decode(json_encode($order->items),true);

		if($data['paid']>0){

			$payment =	[];
			$payment['stamp'] = time();
			$payment['paid'] = $data['paid'];
			$payment['cardNo'] = substr($data['cardNo'],-4);
			$payment['exp'] = $data['exp'];
			$payment['auth'] = $data['authNo'];
			$payment['oid'] = $data->id;
			$payment['type'] = $data['paymentType'];
			$payment['id_staff'] = (isset($order->staffid)) ? $order->staffid : Auth::user()->id;
			$payment['staff'] = (isset($order->staffName)) ? $order->staffName : Auth::user()->email;
			$payment['id_payment_type'] = PaymentType::id_type($data['paymentType']);
			Sale::addPayment($payment);

		}

		
		if(isset($order->epdqId)){
			
			$a = EpdqOrders::find($order->epdqId);
			$a->oid = $data->id;
			$a->save();
			
		}
		
		return $this;
  }

  public function addItems () {
    foreach($this->items as $item) {
      $orderitem = new OrderItems((array)$item);
      $orderitem->oid = $this->id;
      $orderitem->staffName = Auth::user()->email;
      $orderitem->id_staff = Auth::user()->id;
      $orderitem->itemStatus = 'Allocated';
      $orderitem->id_item_status = OrderStatus::id_status('Allocated');
      $orderitem->save();

      StockItem::addToOrder($orderitem->itemid);
    }

    return $this;
  }


public function manualOrder () {
    $order = Order::find($this->id);
	$order->manual_order=1;
	$order->save();
	
	return $this;
  }	

  public function addPayment ($payment) {
    OrderPayment::addPayment($payment);
  }

  public function addNote ($note,$id = false) {
    $oid = ($id) ? $id : $this->id;
    OrderNotes::addNote($note,$oid);

    return $this;
  }

  public function setIno($id=false) {
    $oid = ($id) ? $id : $this->id;
    Count::setIno($oid);

    return $this;
  }

  public function setDeliveryRoute() {
    Order::setDeliveryRoute($this->id);
    return $this;
  }


  public function setBarcode() {
    Order::setBarcode($this->id);
	return $this;
  }
  
  
  
  public function securityCheck(){
  		
  		if($this->valueCheck()){
  			$order = Order::find($this->id);
  			$order->id_delivery_route = Routes::id_route('beds blocked');
  			$order->delRoute = 'Beds Blocked';
  			$order->save();
  		}
  		
  		if($this->is_matching()){
  			$order = Order::find($this->id);
  			$order->id_delivery_route = Routes::id_route('Double Orders');
  			$order->delRoute = 'Double Orders';
  			$order->save();
  		}
  		
  		return $this;
  }
  
  public function valueCheck() {
  	$order = Order::find($this->id);
  	if($order->total>399 && $order->paymentType!='Cash'){
  		return true;
  	}
  		return false;
  }
  

  
  public function is_matching(){
  
  	$customer = Customer::find($this->cid);
  	
  	$match = Customer::where(['dNumber' => $customer->dNumber , 'dPostcode' => $customer->dPostcode])
  					->where('id','!=',$this->cid)
  					->orWhere(['number'=>$customer->number, 'postcode' => $customer->postcode])
  					->where('id','!=',$this->cid)
  					->first();
  	
  	if($match){
  		return true;
  	}
  
  	return false;
  
  }
  
  public function check_split() {
    $a = Order::check_split($this->id);
	$this->splitMail = $a['splitMail'];
	$this->expressMail = $a['expressMail'];
    return $this;
  }

  
  public function getData() {
    $dbname = env('DB_DATABASE');
    $fields = \DB::select(\DB::raw("SELECT `COLUMN_NAME` as field FROM `INFORMATION_SCHEMA`.`COLUMNS` WHERE `TABLE_SCHEMA`='$dbname' AND `TABLE_NAME`='orders'"));

    foreach($fields as $row) {
      $this->{$row->field}=null;
    }

    $this->vatAmount=Settings::getVat();

    return $this;
  }

  public function toArray() {
    return json_decode(json_encode($this),true);
  }

  public function __set($prop,$val) {
    $this->{$prop}=$val;
    return $this;
  }

  public function __get($prop) {
   return $this->{$prop};
  }
}
