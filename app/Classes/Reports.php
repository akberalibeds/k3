<?php

namespace App\Classes;

use App\Order;
use App\OrderItems;

class Reports extends ClassModel
{
  
  private $vatAmount;
  
  
	public function __construct(){
	
		parent::__construct();
		
			
	}
  	
	
		
	public static function getReportDashboard(){
		
	  $monthly = Order::select(\DB::raw("concat(MONTHNAME(str_to_date(startDate,'%d-%b-%Y')),' ',year(str_to_date(startDate,'%d-%b-%Y'))) as date"),\DB::raw('count(*) as count'),\DB::raw('sum(total) as total'),\DB::raw('sum(total)/1.2 as subtotal'),'companyName as company',\DB::raw('sum(total) - (sum(total)/1.2) as VAT'))
	 				->where(\DB::Raw("date(str_to_date(startDate,'%d-%b-%Y'))"),'>',date('Y-m-01 00:00:00',strtotime(date('Y-m-d')." -11 months")))
					->whereIn('id_status',[3,4,5,7])
					->groupBy(\DB::Raw("concat(MONTHNAME(str_to_date(startDate,'%d-%b-%Y')))"))
					->orderBy(\DB::Raw("str_to_date(startDate,'%d-%b-%Y')"))
					->get();		
	
	  $today = Order::select('startDate as date',\DB::raw('count(*) as count'),\DB::raw('sum(total) as total'),\DB::raw('sum(total)/1.2 as subtotal'),'companyName as company',\DB::raw('sum(total) - (sum(total)/1.2) as VAT'))
	 				->where('startDate','=',date('d-M-Y'))
					->get();	
					
					
	  $retail = Order::select('manual_order','startDate as date',\DB::raw('count(*) as count'),\DB::raw('sum(total) as total'),\DB::raw('sum(total)/1.2 as subtotal'),'companyName as company',\DB::raw('sum(total) - (sum(total)/1.2) as VAT'))
	 				->where('startDate','=',date('d-M-Y'))
					->groupBy('companyName','manual_order')
					->get();	
					
	  $refunds = Order::select('startDate as date',\DB::raw('count(*) as count'),\DB::raw('sum(total) as total'),\DB::raw('sum(total)/1.2 as subtotal'),'companyName as company',\DB::raw('sum(total) - (sum(total)/1.2) as VAT'))
	 				->where('startDate','=',date('d-M-Y'))
					->groupBy('companyName')
					->get();	
		
		return ['reports' => ['monthly' => $monthly, 'today' => $today, 'retail' => $retail, 'refunds' => $refunds]];
	}
	
	
  
  	
  
 	public static function search($request)
	{
		
		$func = $request->get('type');
		return self::$func($request);
		
	}
	
	
	
	public static function daily($request)
	{
		
		
		 $daily = Order::select(\DB::raw("startDate as date"),\DB::raw('count(*) as count'),\DB::raw('sum(total) as total'),\DB::raw('sum(total)/1.2 as subtotal'),\DB::raw('sum(total) - (sum(total)/1.2) as VAT'))
	 				->whereBetween(\DB::Raw("date(str_to_date(startDate,'%d-%b-%Y'))"),[date('Y-m-d 00:00:00',strtotime($request->get('from'))),date('Y-m-d 23:59:59',strtotime($request->get('to')))])
					//->whereIn('id_status',[3,4,5,7])
					->groupBy(\DB::Raw("date(str_to_date(startDate,'%d-%b-%Y'))"))
					->orderBy(\DB::Raw("str_to_date(startDate,'%d-%b-%Y')"))
					->get();	
					
					
		return view('admin.reports.daily', ['reports' => $daily]);		
		
	}
	
	
	public static function monthly($request)
	{
	
		
		return 'monthly';
		
		
	}
	
	
	
	public static function items($request)
	{
	
		return "items";	
		
	}
  
  
  
  
  
  
	
  
  
}
