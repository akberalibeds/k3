<?php

namespace App;

use App\Helper\Traits\QueryableModel;
use Illuminate\Database\Eloquent\Model;

class SellerItem extends Model
{
  use QueryableModel;
  /**
   * @var
   */
  public static $rules = [
    'store' => [
      'cost' => 'required|numeric|min:0.01',
      'id_seller' => 'required|numeric|exists:sellers,id',
      'id_stock' => 'required|numeric|exists:stock_items,id',
    ],
  ];
  /**
   * The table associated with the model.
   *
   * @var string
   */
  protected $table = 'wholesale';
  /**
   * @param  int  $id seller id
   * @return \Illuminate\Http\Response
   */
  public static function getAll ($queryStrings = []) {
    $query = static::queryForItem();
    if (count($queryStrings) > 0) {
      $query = parent::modForQueryString($query, $queryStrings);
    }
    return $query->orderBY('itemCode')->paginate(config('search.rpp'));
  }
  /**
   * @param  int  $id seller id
   * @return \Illuminate\Http\Response
   */
  public static function getItem ($id) {
    $query = static::queryForItem();
    return $query->where('id', '=', $id);
  }
  /**
   * @param  int  $id seller id
   * @return \Illuminate\Http\Response
   */
  public static function queryForItem () {
    return static::select(
        'sellers.name',
        'stock_items.actual',
        'stock_items.beds_price',
        'stock_items.cost as cost',
        'stock_items.ebay_price',
        'stock_items.itemCode',
        'stock_items.retail',
        'stock_items.wholesale',
        'wholesale.id',
        'wholesale.id_stock',
        'wholesale.id_seller',
        'wholesale.cost as w_cost'
      )
      ->leftJoin('sellers', 'sellers.id', '=', 'wholesale.id_seller')
      ->leftJoin('stock_items', 'stock_items.id', '=', 'wholesale.id_stock');
  }
}
