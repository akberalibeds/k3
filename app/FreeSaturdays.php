<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FreeSaturdays extends Model
{
  /**
   * The table associated with the model.
   *
   * @var string
   */
  protected $table = 'freeSat';
  /**
   *
   */
  public static function getAfterToday () {
    return static::select('saturday')
      ->where('date', '>', 'current_date()')
      ->get();
  }
  /**
   *
   */
  public static function getWithStaff () {
    return static::select(
        'freeSat.id as freesat_id',
        'freeSat.created_at',
        'freeSat.date',
        'users.email',
        'freeSat.saturday',
        'users.id as user_id'
      )
      ->leftJoin('users', 'users.id', '=', 'freeSat.user_id')
      ->get();
  }
}
