<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Mail;
use App\Helper\Pdfs\PdfInvoice;

class Mailer extends Model
{
  /**
   * The table associated with the model.
   *
   * @var string
   */
  protected $table = 'emailSettings';

  public $timestamps = false;


  public static function sendRaw($data){
    //echo "<pre>";
    //print_r($data);
    //echo "</pre>";
    try {
      $i =  Mail::send([],[], function ($m) use ($data) {

      $m->from($data->account);
      $m->to($data->toEmail, $data->toName)
      ->subject($data->subject)
      ->setBody($data->body,'text/html');
      });
      echo $i ? 'Sent' : $i ;
    }
    catch(\Swift_RfcComplianceException $e){
      echo $e->getMessage();
    }
  }


  public static function getEmail($company)
  {

	  return self::where(['company.company_name' => $company])
				->select('email')
				->join('company','emailSettings.id','=','company.l_id_email')
				->first();
  }






  public static function sendBedsMail($data)
  {

	  try {
		echo Mail::send('emails.beds.'.$data->template,[],function ($m) use ($data) {

			  $m->from('no-reply@beds.co.uk', 'Beds.co.uk')
				->to($data->toEmail, $data->toName)
			  	->subject('Your Beds.co.uk order');
		 });
	 }
	 catch(\Swift_RfcComplianceException $e){
			 echo $e->getMessage();
	 }


  }


  public static function sendDispatchedMail($data)
  {

	  try {
			echo Mail::send('emails.premier.dispatched',$data->toArray(),function ($m) use ($data) {

			  		$m->from('no-reply@premierdeliveries.co.uk', 'Premier Deliveries')
					->to($data->email, $data->dBusinessName)
			  		->subject('Your delivery has been dispatched');
			});
		}
		catch(\Swift_RfcComplianceException $e){
			 echo $e->getMessage();
		}
  }



    public static function sendInvoiceMail($data)
  {

	//$order = Order::order();
	//$pdf = new Invoice();
	//$data->invoice = $pdf->getInvoicePDF([$order['order']])->output("","S");

	$pdf = new PdfInvoice();
    $order = Order::getForInvoice([$data->id]);
    $pdf->invoice($order[0]);
    $data->invoice = $pdf->outPdf("S");

	try {
	$i= Mail::send('emails.invoice',$order->toArray()[0],function ($m) use ($data) {

			  $m->from($data->account)
			  	->to($data->toEmail, $data->toName)
				->subject('Your Order : '.$data->id)
				->attachData($data->invoice,'invoice.pdf');
		 });

	echo $i ? 'Sent' : $i;
    }
    catch(\Swift_RfcComplianceException $e){
			 echo $e->getMessage();
	}
  }




	public static function stockEmail($request){
		

		return Mail::send([], [], function($message) use ($request)
				{    
					$message->to($request->get('email'),$request->get('name'))
							->from('sales@beds.co.uk')
							->subject($request->get('subject'))   
							->setBody($request->get('body'),'text/html');
				});	
	}


}
