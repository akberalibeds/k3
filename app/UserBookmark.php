<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserBookmark extends Model
{
  /**
   * get on-hold orders
   * @author Giomani Designs
   * @param  int $id user id
   * @return Illuminate\Database\Eloquent\Collection
   */
  public static function getForUser($id)
  {
    return UserBookmark::select('url', 'description')->where('user_id', '=', $id)->get();
  }
}
