<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EbayStock extends Model
{
  /**
   * The table associated with the model.
   *
   * @var string
   */
  protected $table = 'ebayStock';
  
  public $timestamps = false;
  
  
 public static function add(array $data){
 	
 	$a = new self;
 	foreach ($data as $key => $val){
 		$a->{$key} = $val;
 	}
 	$a->save();
 	
 }
 
 
}
