<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderStatusHistory extends Model
{
  /**
   * The table associated with the model.
   *
   * @var string
   */
  protected $table = 'order_status_history';


  public $timestamps = false;

  public $fillable = ['from_status','order_id','text','to_status'];



  public static function add($data){

	$history = new OrderStatusHistory($data);
	$history->save();

  }
}
