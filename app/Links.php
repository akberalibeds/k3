<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Links extends Model
{
    protected $guarded = [];

    protected $primaryKey = 'id_col';

    public function order()
    {
        return $this->belongsTo(Order::class, 'oid');
    }

  /**
   * The table associated with the model.
   *
   * @var string
   */
  protected $table = 'links';
  
  public $timestamps = false;
  
  
  public static function updateLink($oid, $code, $newStatus, $done, $oldStatus){
	
		$row = Links::select()->where(['oid' => $oid])->get()->first();
        $num    = (count($row)>0) ? count($row) : 0;

		

        if ($num == 0) {
			$data=array(
					'oid'=>$oid,
					'code'=>$code,
					'status'=>'Pending delivery date',
					);
            Links::insert($data);
        } else {

           

            //mysqli_query($link, "insert into history (status, oid, timestamp) values ('" . $row['status'] . "'," . $row['oid'] . "," . $row['timestamp'] . ")");
			
			$data = [
            		'code'   =>  $done == '' ? $row['code']   : $code,
					'done' 	 =>  $done == '' ? $row['done']   : $done,
            		'status' => 	 $newStatus == '' ? $row['status'] : $newStatus,
					];

//            $sql = "update links set code='$codeUpdate', status = '$statusUpdate', done = $doneUpdate where oid=$oid";
//            mysqli_query($link, $sql);
			
			Links::where(['oid' => $oid])->update($data);
        }

        // if ($newStatus == $lookups['order_statuses']['Allocated'] || $oldStatus == $lookups['order_statuses']['Allocated']) {
        //     $cache = new Cache();
        //
        //     try {
        //         $cache->updateCache($oid, $newStatus, $oldStatus);
        //     } catch(Exception $e) {
        //         error_log('***** CACHE-ERROR :' . $e);
        //     }
        // }
    }
	
	
	
	
	public static function resendLink($id){
		
		Links::where('oid',$id)->update(['sent' => 0 , 'done' => 0]);
		Order::find($id)->update(['done' => 0]);
			
	}
	
	
	
	public static function customersToNotify(){
		
		
		return self::where('sent','=',0)
				->select('links.oid','orders.code','orders.ebayID','customers.tel',
				'customers.userID','orders.ebay','customers.dBusinessName','customers.email1 as email')
				->join('orders','links.oid','=','orders.id')
				->join('customers','orders.cid','=','customers.id')
				->get();	
		
		
		
    }
    
    public static function linksSuitableForNotification() {
      return static::whereIn('orderStatus',[ "Allocated"])
          ->join('orders',function($join) {
              $join->on('links.oid','=','orders.id');
          })
          ->join('routeName',function($join) {
              $join->on('orders.delRoute','=','routeName.name');
          })
          ->join('customers',function($join) {
              $join->on('orders.cid','=','customers.id');
          })
        ->where('routeName.msg',1)
        ->where('orders.time_stamp', '>=' , \Carbon\Carbon::now()->subDays(3))                  
        ->with(['order.customer','order.route']);
    }
    
    public static function linksForInitialNotification() {
      return static::linksSuitableForNotification()
              ->where('sent','=',0)
              ->get();
    }
    
    public static function linksForRepeatNotification() {
      $date = date('Y-m-d H:i:s', strtotime(date('d-M-Y H:i:s'). " - 24 hours" ));
      return static::linksSuitableForNotification()
              ->where('sent','=',1)
              ->where('links.done','=',0)
              ->where('time','<=',$date)
              ->get();
    }
    
//    public static function linksToNotifyCustomers()
//    {
//        $date = date('Y-m-d H:i:s', strtotime(date('d-M-Y H:i:s'). " - 24 hours" ));
//        
//        return static::where(function ($q) use($date) { 
//          $q->where('sent', '=', 0)
//              ->orWhere('sent','=', 1)
//              ->where('orders.done', '=', 0)
//              ->where('links.timestamp', '<=', $date);
//          })
//          ->whereNotIn('orderStatus',[ "Delivered" ,"Routed" , "Dispatched"])
//          ->join('orders',function($join) {
//              $join->on('links.oid','=','orders.id');
//          })
//          ->join('routeName',function($join) {
//              $join->on('orders.delRoute','=','routeName.name');
//          })
//          ->join('customers',function($join) {
//              $join->on('orders.cid','=','customers.id');
//          })
//        ->where('routeName.msg',1)
//                  
//        ->with(['order.customer','order.route'])
//        ->get();
//    }
//	  
}
  
  

