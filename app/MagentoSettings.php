<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Time;

class MagentoSettings extends Model
{
  /**
   * The table associated with the model.
   *
   * @var string
   */
  protected $table = 'magento';
  
  /**
   * Use Timestamps in model.
   *
   * @var boolean : default true
   */
  public $timestamps = false;
  
  /**
   * Get the Last Call Time.
   *
   */
  public static function getLastCallTime()
  {
    
	$time =  static::find(1)->time;
	return $time;
  
  }
  
  
  /**
   * update the Last Call Time.
   *
   */
  public static function saveTime()
  {
    
	$time =  static::find(1);
	$time->time = Time::date()->get('Y-m-d H:i:s');
	$time->save();
  
  }
  
}
