<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderEvent extends Model
{

    public $fillable = ['event_type','order_id','done'];
    
}
