<?php
/** @author Giomani Designs **/
namespace App;

use Illuminate\Database\Eloquent\Model;

class Info extends Model
{
  /**
   * types of info
   */
  const INFO_TYPE_DOCUMENT = 1;
  const INFO_TYPE_EMAIL = 2;
  const INFO_TYPE_NUMBER = 3;
  const INFO_TYPE_LINK = 4;
  /**
   * The table associated with the model.
   *
   * @var string
   */
  protected $table = 'info';
  /**
   * @var array
   */
  public static $types = array (
    self::INFO_TYPE_DOCUMENT => 'document',
    self::INFO_TYPE_EMAIL => 'email',
    self::INFO_TYPE_NUMBER => 'number',
    self::INFO_TYPE_LINK => 'link',
  );
  /**
   * get the documents for the dashboard
   *
   * @param  array $documents ['description' => string, 'filename' => string]
   * @return array
   */
  public static function addDocument($description, $filename) {
    return Info::addInfo($description, $filename, Info::INFO_TYPE_DOCUMENT);
  }
  /**
   * get the documents for the dashboard
   *
   * @param  array $documents ['description' => string, 'email' => string]
   * @return array
   */
  public static function addEmails($emails) {
    $accepts = [];
    $rejects = [];
    foreach ($emails as $email) {
      if (filter_var($email['email'], FILTER_VALIDATE_EMAIL)) {
        $accepts[] = Info::addInfo($email['description'], $email['email'], Info::INFO_TYPE_EMAIL);
      } else {
        $rejects[] = $email;
      }
    }
    return ['accepts' => $accepts, 'rejects' => $rejects];
  }
  /**
   * add a row to the Info resource
   *
   * @param  string $description
   * @param  string $datum
   * @param  int    $type (see Info::types)
   * @return array
   */
  public static function addInfo($description, $datum, $type) {
    $info = new Info;
    $info->datum = $datum;
    $info->description = $description;
    $info->type = $type;
    $info->save();

    return $info;
  }
  /**
   * get the documents for the dashboard
   *
   * @param  array $documents ['description' => string, 'email' => string]
   * @return array
   */
  public static function addLinks($links) {
    $accepts = [];
    $rejects = [];
    foreach ($links as $link) {
      $accepts[] = Info::addInfo($link['description'], $link['link'], Info::INFO_TYPE_LINK);
    }
    return ['accepts' => $accepts, 'rejects' => $rejects];
  }
  /**
   * get the documents for the dashboard
   *
   * @param  array $numbers ['description' => string, 'number' => string]
   * @return array
   */
  public static function addTelephoneNumbers($numbers) {
    $accepts = [];
    $rejects = [];
    foreach ($numbers as $number) {
      $accepts[] = Info::addInfo($number['description'], $number['number'], Info::INFO_TYPE_NUMBER);
    }
    return ['accepts' => $accepts, 'rejects' => $rejects];
  }
  /**
   * get the documents for the dashboard
   *
   * @return array
   */
  public static function getDocuments() {
    return Info::where('type', '=', Info::INFO_TYPE_DOCUMENT)
      ->get();
  }
  /**
   * get the emails for the dashboard
   *
   * @return array
   */
  public static function getEmails() {
    return Info::where('type', '=', Info::INFO_TYPE_EMAIL)
      ->get();
  }
  /**
   * get the telephone numbers for the dashboard
   *
   * @return array
   */
  public static function getTelephoneNumbers() {
    return Info::where('type', '=', Info::INFO_TYPE_NUMBER)
      ->get();
  }
  /**
   * get the links for the dashboard
   *
   * @return array
   */
  public static function getLinks() {
    return Info::where('type', '=', Info::INFO_TYPE_LINK)
      ->get();
  }
}
