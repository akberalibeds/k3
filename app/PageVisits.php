<?php

namespace App;

use DB;
use Illuminate\Database\Eloquent\Model;
use Auth;

class PageVisits extends Model
{
  /**
   * The table associated with the model.
   *
   * @var string
   */
  protected $table = 'page_visits';
  
  public $timestamps  = false;
  
  
  
  public static function add($id){
	
		$pv = new PageVisits;
		$pv->oid=$id;
		$pv->ip=$_SERVER['REMOTE_ADDR'];
		$pv->user = Auth::user()->id;
		$pv->save(); 
		
		return new PageVisits;	  
  }
  
  
  public static function getVisits($id)
  {
		return self::where(['oid' => $id])
					->select('page_visits.*','users.name')
					->join('users','page_visits.user','=','users.id')
					->orderBy('id','desc')
					->get();  
  }
  
  
}
