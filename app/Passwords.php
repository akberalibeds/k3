<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Passwords extends Model
{
   
    
	/* The table associated with the model.
     *
     * @var string
     */
	 protected $table = 'passwords';
     
	 /* Use Timestamps.
     *
     * @var bool
     */
	 public $timestamps = false;
   	 
	 /* The fillable fields in model.
     *
     * @var array
     */
	 protected $fillable = ['type', 'pwd'];
   
   
   	
	
	/*
	 *
	 */
   
   
   
}
