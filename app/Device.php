<?php

namespace App;


use Illuminate\Database\Eloquent\Model;
use phpDocumentor\Reflection\DocBlock\Tags\See;


class Device extends Model
{

	protected $table = 'devices';
	
	
	
	public static function logDevice($data){
		
		$d = new self();
		foreach($data as $key => $val){
			$d->{$key}=$val;
		}
		$d->save();
		
	}
	
	
	public static function getDriver($device){
		$driver = self::select('name')
						->where('imei',$device)
						->join('drivers','devices.driver','=','drivers.id')
						->first();
		return $driver->name;
	}
	
	
	public static function getDriverID($device){
		$driver = self::select('drivers.id')
						->where('imei',$device)
						->join('drivers','devices.driver','=','drivers.id')
						->orderBy('devices.id','desc')
						->first();
		return $driver->id;
	}
	
	
	public static function updateToken($request){
		self::where('imei',$request->get('device'))->update(['token' => $request->get('token')]);
	}
	
	
	public static function getTokenForDriver($id){
		$a = self::where('driver',$id)->select('token')->first();
		return $a->token;
	}
	
}