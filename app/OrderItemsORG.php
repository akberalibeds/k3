<?php

namespace App;

use DB;
use App\Helper\Traits\QueryableModel;
use Illuminate\Database\Eloquent\Model;

class OrderItems extends Model
{
  use QueryableModel;
  /**
   * The table associated with the model.
   *
   * @var string
   */
  protected $table = 'orderItems';

  public $timestamps = false;

  public $fillable = ['id', 'id_staff', 'id_item_status', 'itemid', 'oid', 'bedsName', 'blocked', 'currStock', 'discount', 'itemStatus', 'lineTotal', 'notes', 'price', 'Qty', 'staffName', 'costs'];

  public function stock()
  {
    return $this->belongsTo(StockItem::class, 'itemid');
  }

  public function order()
  {
    return $this->belongsTo(Order::class, 'oid');
  }


  /*
   * get the missed parts counts
   *
   * @return Illuminate\Pagination\LengthAwarePaginator
   */


  public static function getMissedCounts($date) {
    return OrderItems::select('orderItems.oid', 'orderItems.notes')
      ->join('stock_items', 'orderItems.itemid', '=', 'stock_items.id')
      ->join('disp', 'disp.oid', '=', 'orderItems.oid')
      ->where('disp.date', '=', $date)
      ->where('itemCode','=','missed parts')
      ->get();

	/*
   	select orderItems.oid,notes
	from orderItems
	join stock_items on orderItems.itemid=stock_items.id
	join disp on disp.oid = orderItems.oid
	where disp.date='16-Aug-2016' and disp.route not in ('TNT','TNT Black','TNT Brown')  and itemCode = 'missed parts';
   */

  }
  /**
   * @return Illuminate\Database\Eloquent\Collection
   */
  public static function getLoadCounts($date, $warehouse) {
    $query = OrderItems::select(
        'label',
        'itemid',
        'itemCode',
        'pieces',
        'warehouse',
        DB::raw('sum(Qty) tot')
      )
      ->join('stock_items', 'orderItems.itemid', '=', 'stock_items.id')
      ->join('disp', 'disp.oid', '=', 'orderItems.oid')
      ->where('disp.date', '=', $date)
      ->whereNotIn('disp.route', function ($query) {
        $query->select('name')
          ->from(with(new Routes )->getTable())
          ->where('in_load_count', '=', 0);
      })
      ->where('label',1)
      ->where('cat','not like','%collect%')
      ->where('itemCode','not like','%collect%')
      ->whereNotIn('cat',['birlea','seconique','durest beds'])
      ->groupBy('itemid')
      ->having('tot', '>', '0')
      ->orderBy('warehouse', 'asc');
    if ($warehouse !== 'all') {
      $query->where('warehouse', '=', $warehouse);
    }
    return $query->get();
  }
  /**
   * @author Giomani Designs (Development Team)
   * @return Illuminate\Database\Eloquent\Collection
   */
  public static function getLoadCountsM($date) {
    return static::select(
        'label',
        'itemid',
        'itemCode',
        'pieces',
    	'warehouse',
        DB::raw('sum(Qty) tot')
      )
      ->join('stock_items', 'orderItems.itemid', '=', 'stock_items.id')
      ->join('orders', 'orderItems.oid', '=', 'orders.id')
      ->where('orders.deliverBy', '=', $date)
      ->whereNotIn('orders.delRoute', function ($query) {
        $query->select('name')
          ->from(with(new Routes )->getTable())
          ->where('in_load_count', '=', 0);
      })
      ->where('label',1)
      ->whereNotIn('cat',['birlea','seconique','durest beds'])
      ->where('itemCode','not like','%collect%')
      ->where('cat','not like','%collect%')
      ->groupBy('itemid')
      ->having('tot', '>', '0')
      ->orderBy('warehouse', 'asc')
      ->get();
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  integer    $id
   * @param  string     $queryStrings the query string
   * @param  integer    $statusId the status of the order (defaults to 2 - Allocated)
   * @return Illuminate\Pagination\LengthAwarePaginator
   */
  public static function getOrdersFor($id, $queryStrings, $statusId = 2) {
    $query = static::queryForOrdersFor($id, $statusId);
    if (count($queryStrings) > 0) {
      $query = parent::modForQueryString($query, $queryStrings);
    }
    return $query->orderBy('orders.id', 'desc')->paginate(config('search.rpp'));
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  integer    $id
   * @param  integer    $statusId the status of the order (defaults to 2 - Allocated)
   * @return Illuminate\Pagination\LengthAwarePaginator
   */
  public static function queryForOrdersFor($id, $statusId = 2) {
    return static::select(
      'orders.id',
      'orders.cid',
      'orders.iNo',
      'orders.orderStatus',
      'orders.orderType',
      'orders.paid',
      'orders.staffName',
      'orders.startDate',
      'orders.total',
      'customers.userID',
      'customers.businessName',
      'customers.dBusinessName',
      'customers.postcode',
      'customers.dPostcode'
    )
    ->leftJoin('orders', 'orders.id', '=', 'orderItems.oid')
    ->leftJoin('customers', 'customers.id', '=', 'orders.cid')
    ->where('orderItems.itemid', '=', $id)
    ->where('id_status', '=', $statusId);
  }


  public static function onOrder($id) {
    return self::where('oid',$id)
      ->join('stock_items','orderItems.itemid','=','stock_items.id')
      ->select('orderItems.*','stock_items.itemCode','stock_items.itemDescription')
      ->get();
  }
}
