<?php

namespace App;

use DB;
use App\Helper\Traits\QueryableModel;
use Illuminate\Database\Eloquent\Model;

class Vehicle extends Model
{
  use QueryableModel;
  /**
   * The table associated with the model.
   *
   * @var string
   */
  protected $table = 'vehicles';
  /**
   * @author Giomani Designs (Development Team)
   * @param  array   $queryStrings the query string
   * @return Illuminate\Pagination\LengthAwarePaginator
   */
  public static function getAll ($queryStrings) {
    $query = static::queryForGetAll();
    if (count($queryStrings) > 0) {
      $query = parent::modForQueryString($query, $queryStrings);
    }
    return $query->orderBy('reg', 'desc')->paginate(config('search.rpp'));
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  int  $id vehicle id
   * @return Illuminate\Pagination\LengthAwarePaginator
   */
  public static function get($id) {
      $query = static::queryForGet($id);
      return $query->get();
  }
  /**
   * Get height attributece .
   *
   * @param  string  $value
   * @return string
   */
  public static function getIdRegistrations () {
    return static::select('id', 'reg')->orderBy('reg', 'desc')->get();
  }
  /**
   * @param  int  $id vehicle id
   * @return lluminate\Database\Query\Builder
   */
  protected static function queryForGet ($id) {
    return static::select(
      'vehicles.depth',
      'vehicles.description',
      'vehicles.id',
      'vehicles.height',
      'vehicles.reg',
      'vehicles.width',
      'vehicle_costs.date_incurred',
      'vehicle_costs.id as vcid',
      'vehicle_costs.cost',
      'vehicle_costs.notes',
      'vehicle_costs.mileage',
      'vehicle_cost_types.type'
    )
    ->leftJoin('vehicle_costs', 'vehicle_costs.vehicle_id', '=', 'vehicles.id')
    ->leftJoin('vehicle_cost_types', 'vehicle_cost_types.id', '=', 'vehicle_costs.vehicle_cost_type_id')
    ->orderBy('vehicle_costs.date_incurred', 'desc')
    ->where('vehicles.id', '=', $id);
  }
  /**
   * @return lluminate\Database\Query\Builder
   */
  protected static function queryForGetAll () {
    return static::select(
        'vehicles.id',
        'vehicles.depth',
        'vehicles.height',
        'vehicles.reg',
        'vehicles.width',
        'vehicles.description',
        DB::raw('max(vehicle_costs.mileage) as mileage')
      )
      ->leftJoin('vehicle_costs', 'vehicle_costs.vehicle_id', '=', 'vehicles.id')
      ->groupBy('vehicles.id')
      ->orderBy('vehicles.reg');
  }
}
