<?php

namespace App;

use DB;
use App\Helper\Traits\QueryableModel;
use Illuminate\Database\Eloquent\Model;
use App\Helper\SMS;

class DispatchedRoute extends Model
{
  use QueryableModel;
  /**
   * @var string
   */
  protected $table = 'dispRoute';
  /**
   * @author Giomani Designs (Development Team)
   * @param  string  $queryStrings
   * @return Illuminate\Database\Eloquent\Collection
   */
  public static function getAll($queryStrings) {
  	$date = new \Datetime();
    $dates = [];
    $interval = new \DateInterval('P1D');
    $date->add($interval);
    $count = config('other.dispatched-routes-count');
    for ($i = 0; $i < $count; $i++) {
      $dates[] = $date->format('d-M-Y');
      $date->sub($interval);
    }
    
    $query = static::queryForAll($dates);
    if (count($queryStrings) > 0) {
      $query = parent::modForQueryString($query, $queryStrings);
    }
    return $query->orderBY('dispRoute.id', 'desc')->paginate($queryStrings['limit']/*config('search.rpp')*/);
  }
  /**
   * @param  array $dispatchIds
   * @return Illuminate\Database\Eloquent\Collection
   */
  public static function getLabels($dispatchIds) {
    return
      static::select(
      // 'categories.name as category_name',

      'dispRoute.date as disp_date',
      'dispRoute.route as route_name',

      'orders.companyName',
      'orders.code as code',
      'orders.direct',
      'orders.delOrder as del_order',
      'orders.g_id',
      'orders.g_num',
      'orders.id as oid',
      'orders.id_seller',
      'orders.priority',

      'codes.parcelid',
      'codes.warehouse',

      DB::raw('GROUP_CONCAT(DISTINCT concat(COALESCE(codes.itemCode, ""), ",", COALESCE(bar, ""), ",", COALESCE(orderItems.notes, ""), ",", COALESCE(stock_items.itemDescription, ""))  separator ";") as items'),

      'customers.id as cid',
      'customers.dBusinessName as c_name',
      'customers.dNumber as c_number',
      'customers.dStreet as c_street',
      'customers.dTown as c_town',
      'customers.dPostcode as c_postcode',
      'customers.email1',
      'customers.tel as c_tel',
      'customers.mob as c_mob',
      //
      'direct.name as d_name',
      'direct.number as d_number',
      'direct.street as d_street',
      'direct.town as d_town',
      'direct.postcode as d_postcode',
      'direct.tel as d_tel',
      'direct.mob as d_mob'
    )
    ->leftJoin('disp', function ($leftJoin) {
      $leftJoin
        ->on('disp.date', '=', 'dispRoute.date')
        ->on('disp.route', '=', 'dispRoute.route');
      })
    ->join('orders', 'disp.oid', '=', 'orders.id')
    ->join('customers', 'orders.cid', '=', 'customers.id')
    ->leftjoin ('direct', 'direct.oid', '=', 'orders.id')
    ->join('codes', 'codes.oid', '=', 'disp.oid')
    ->leftjoin('orderItems', 'orderItems.id', '=', 'codes.lineid')
    ->leftJoin('stock_items', 'stock_items.id', '=', 'orderItems.itemid')
    ->whereIn('dispRoute.id',  $dispatchIds)
    ->groupBy('oid')
    ->orderBy('codes.warehouse')
    ->orderBy('codes.parcelid')
    ->get()
    ;
    // dd($s->get());
  }
  /**
   * @param  String $id the dispRoute id
   * @return Illuminate\Database\Eloquent\Collection
   */
  public static function getRouteForTexting($id) {
    return DispatchedRoute::select(
        'customers.dPostcode as c_postcode',
        'direct.postcode as d_postcode',
        'disp.oid',
        'dispRoute.id',
        'dispRoute.startTime as disp_route_time',
        'drivers.name as driver_name',
        'drivers.num as driver_telnum',
        'mates.num as mate_telnum',
        'mates.name as mate_name',
        'orders.direct'
      )
      ->join('disp', function ($join) {
            $join->on('disp.date', '=', 'dispRoute.date')
                 ->on('disp.route', '=', 'dispRoute.route')
                 ->where('disp.num', '=', 1);
        })
      ->leftJoin ('orders', 'disp.oid', '=', 'orders.id')
      ->leftJoin('customers', 'customers.id', '=', 'orders.cid')
      ->leftJoin('drivers', 'drivers.name', '=', 'dispRoute.driver')
      ->leftJoin('mates', 'mates.name', '=', 'dispRoute.mate')
      ->leftJoin('direct', 'direct.oid', '=', 'orders.id')
      ->where('dispRoute.id', '=', $id)
      ->get();
  }
  /**
   * There is a limit imposed to speed up the queryForAll
   * @param  String $id the dispRoute id
   * @return Illuminate\Database\Eloquent\Collection
   */
  public static function queryForAll($dates = null) {
    $query = DispatchedRoute::select(
        //'disp.oid',

        'dispRoute.id',
        'dispRoute.date',
        'dispRoute.driver',
        'dispRoute.mate',
        'dispRoute.payments',
    	'dispRoute.loader',
        'dispRoute.picker',
        'dispRoute.route',
        'dispRoute.staff',
        'dispRoute.vehicle',
        'dispRoute.startTime'

        //'orders.direct',
        //'orders.priority'
      );
      /*->leftJoin('disp', function ($join) {
            $join->on('disp.date', '=', 'dispRoute.date')
                 ->on('disp.route', '=', 'dispRoute.route');
        })
      ->leftJoin ('orders', 'orders.id', '=', 'disp.oid')
      ->where('disp.num', '=', 1);
	*/
    if ($dates !== null) {
      $query->whereIn('dispRoute.date', $dates);
    }
    // dd($query->get());
    return $query;
  }
  

  public static function setOptions($request){
		
  		$action = explode("-",$request->get('action'));
  		$action = $action[1];
  		$text = (is_array($request->get('text'))) ? implode(",",$request->get('text')) : $request->get('text');
  		$route = self::find($request->get('route'));
  		$route->{$action} = $text;
		return $route->save() ? 1 : 0;  		
		

  }
  
  public static function textShifts($request)
  {
  	
  
  	$data = json_decode($request->get('data'));
  	
  	return $data;
  	
  	$text = new SMS();
  	foreach ($data as $d) {
  
  		$mate   = $d->mate;
  		$route  = $d->route;
  		$driver = $d->driver;
  		$date   = $d->date;
  		$time   = $d->time;
  
  		$result = mysqli_query($link, "select num from drivers where name='$driver'");
  		while ($row = mysqli_fetch_assoc($result)) {
  			$driverNum = $row['num'];
  		}
  
  		$result = mysqli_query($link, "select num from mates where name='$mate'");
  		while ($row = mysqli_fetch_assoc($result)) {
  			$mateNum = $row['num'];
  		}
  
  		$result = mysqli_query($link, "select oid from disp where route='$route' and date='$date' and num=1");
  		while ($row = mysqli_fetch_assoc($result)) {
  			$oid = $row['oid'];
  		}
  
  		$result = mysqli_query($link, "select dPostcode,direct from orders join customers on orders.cid=customers.id where orders.id=$oid");
  		while ($row = mysqli_fetch_assoc($result)) {
  			$postcode = $row['dPostcode'];
  			$direct   = $row['cid'];
  		}
  
  		if ($direct == 1) {
  			$result = mysqli_query($link, "select postcode direct where oid=$oid");
  			while ($row = mysqli_fetch_assoc($result)) {
  				$postcode = $row['postcode'];
  			}
  		}
  
  		$pc     = strtoupper($postcode[0] . $postcode[1]);
  		$result = mysqli_query($link, "select name from routeStops join routeName on routeStops.routeID=routeName.id where postCode='$pc'");
  		while ($row = mysqli_fetch_assoc($result)) {
  			$route = $row['name'];
  		}
  
  
  		$text->sendSMS("$driverNum", "mate $mate $mateNum \ntime $time\n reply to 07756899173 to confirm");
  		if ($mateNum != "") {
  			$text->sendSMS("$mateNum", "driver $driver $driverNum \ntime $time \n reply to 07756899173 to confirm");
  		}
  	}
  }
  
  
  
  
  public static function addToRoute($data,$orderId){
  	
  	
  	$route = self::find($data->route);
 	//$count = Dispatched::where('route',$route->route)->where('date',$route->date)->count();
 	//$count++;
  	
 	$order = Order::find($orderId);
 	$order->extra='A';
 	$order->delRoute=$route->route;
 	$order->delOrder = 0;
 	$order->id_delivery_route = Routes::id_route($route->route);
 	$order->save();
 	
 	$disp = new Dispatched();
 	$disp->oid = $orderId; 
 	$disp->route = $route->route;
 	$disp->date = $route->date;
 	$disp->num  = 0;
 	$disp->id_route = $route->id;
 	$disp->save();
 	
 	$ship = new ToShip();
 	$ship->oid = $orderId;
 	$ship->tel = $data->txt ? 1 : 0;
 	$ship->mail = $data->email ? 1 : 0;
 	$ship->date = date('d-M-Y');
 	$ship->save();
 	
 	OrderNotes::addNote("Addition to $route->route", $orderId);
 	Order::changestatus(['new' => 'Routed', 'old' => 'Allocated'], $orderId);

  	
  }

  
  
}
