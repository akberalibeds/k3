<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class ComposerServiceProvider extends ServiceProvider
{
  /**
   * Bootstrap the application services.
   *
   * @return void
   */
  public function boot()
  {
    view()->composer(['share.bookmarks', 'home'], 'App\Http\ViewComposers\BookmarksComposer');
    view()->composer('share.bulletins', 'App\Http\ViewComposers\BulletinsComposer');
    view()->composer('*', 'App\Http\ViewComposers\HeadersComposer');
  }

  /**
   * Register the application services.
   *
   * @return void
   */
  public function register()
  {
  }
}
