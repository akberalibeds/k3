<?php

namespace App\Providers;

use Auth;
use Hash;
use Blade;
use App\User;
use Validator;
use App\UserBookmark;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
  /**
   * @author Giomani Designs (Development Team)
   * @return void
   */
  public function boot()
  {

    // $this->app['request']->server->set('HTTPS', $this->app->environment() != 'local');

    Blade::directive('cross', function($expression) {
      return '<i class="color-danger fa fa-times" aria-hidden="true"></i>';
    });
    Blade::directive('hasday', function($expression) {
      $parts = explode(',', trim($expression, "()"));
      return "<?php echo (stripos($parts[0], $parts[1]) !== false) ? '<i class=\"color-success fa fa-check\" aria-hidden=\"true\"></i>' : '<i class=\"color-danger fa fa-times\" aria-hidden=\"true\"></i>'; ?>";
    });
    Blade::directive('pullFlashMessages', function($expression) {
      $out = 'false';

      if ($fms = view()->shared('flash_messages')) {
        foreach (view()->shared('flash_messages') as $msg) {
          $out .= $msg;
        }
      }
      return $out;
    });
    Blade::directive('pushFlashMessage', function($expression) {
      if (!($fms = view()->shared('flash_messages'))) {
        $fms = array ();
      }
      $fms[] = trim($expression, "()\'");
      view()->share('flash_messages', $fms);
    });
    Blade::directive('resultCounts', function($expression) {
      return "<?php echo 'Showing ' . with($expression)->firstItem() . ' to ' . with($expression)->lastItem() . ' of ' . with($expression)->total(); ?>";
    });
    Blade::directive('str2Datetime', function($expression) {
      return "<?php echo with($expression) ? DateTime::createFromFormat('Y-m-d H:i:s', $expression)->format('Y-M-d H:i') : '-' ?>";
    });
    Blade::directive('tick', function($expression) {
      return '<i class="color-success fa fa-check" aria-hidden="true"></i>';
    });
    Blade::directive('tickOrCross', function($expression) {
      return "<?php echo $expression ? '<i class=\"color-success fa fa-check\" aria-hidden=\"true\"></i>' : '<i class=\"color-danger fa fa-times\" aria-hidden=\"true\"></i>'; ?>";
    });
    Blade::directive('tilebutton', function($expression) {
      return "<?php echo '<div class=\"col-sm-4 col-md-2\"><button class=\"btn btn-tile\" onclick=\"' . with{$expression}[0] . '\">' . with{$expression}[1] . '</button></div>'; ?>";
    });
    Blade::directive('tilelink', function($expression) {
      // $link = route(with($expression)[0]);
      return "<?php echo '<a class=\"btn btn-tile\" href=\"' . with{$expression}[0] . '\"' . (isset(with{$expression}[2]) ? ' target=\"' . with{$expression}[2] . '\"' : '') . '>' . with{$expression}[1] . '</a>'; ?>";
    });
    Blade::directive('userstatus', function($expression) {
      return "<?php echo App\User::statusText($expression); ?>";
    });
    Blade::directive('yesOrNo', function($expression) {
      return "<?php echo $expression ? 'yes' : 'no'; ?>";
    });
  }
  /**
   * Register any application services.
   *
   * @return void
   */
  public function register()
  {
    //
  }
}
