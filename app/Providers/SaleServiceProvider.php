<?php

namespace App\Providers;
use App;
use Illuminate\Support\ServiceProvider;



class SaleServiceProvider extends ServiceProvider {
   public function boot() {
      //
   }
   public function register() {
      App::bind('sale', function()
        {
            return new \App\Classes\Sale;
        });
   }
}