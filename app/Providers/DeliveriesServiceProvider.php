<?php

namespace App\Providers;
use App;
use Illuminate\Support\ServiceProvider;



class DeliveriesServiceProvider extends ServiceProvider {
   public function boot() {
      //
   }
   public function register() {
      App::bind('deliveries', function()
        {
            return new \App\Classes\Deliveries;
        });
   }
}