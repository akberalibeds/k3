<?php

namespace App\Providers;

use Illuminate\Contracts\Events\Dispatcher as DispatcherContract;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
  /**
   * The event listener mappings for the application.
   *
   * @var array
   */
  protected $listen = [
    //
    // after sales removing
    'App\Events\AfterSalesRemoving' => [
        'App\Listeners\AfterSalesRemoved',
    ],
    //
    // credit notes
    'App\Events\BulletinCreating' => [
        'App\Listeners\BulletinCreated',
    ],
    //
    // credit notes
    'App\Events\CreditNoteAdding' => [
        'App\Listeners\CreditNoteAdded',
    ],
    //
    // free saturdays
    'App\Events\FreeSaturdayAdding' => [
        'App\Listeners\FreeSaturdayAdded',
    ],
    'App\Events\FreeSaturdayCancelling' => [
        'App\Listeners\FreeSaturdayCancelled',
    ],
    //
    // invoices
    'App\Events\InvoiceWasPaid' => [
        'App\Listeners\InvoicePaid',
    ],
    //
    // ip address
    'App\Events\IpAddressAdding' => [
        'App\Listeners\IpAddressAdded',
    ],
    'App\Events\IpAddressDeleting' => [
        'App\Listeners\IpAddressDeleted',
    ],
    //
    // mark as delivered
    'App\Events\MarkingAsDelivered' => [
        'App\Listeners\MarkedAsDelivered',
    ],
    //
    // change order supplier
    'App\Events\OrderSupplierChanging' => [
        'App\Listeners\OrderSupplierChanged',
    ],
    //
    // order cancelled
    'App\Events\OrderCancellation' => [
        'App\Listeners\OrderCancelled',
    ],
    //
    // order ordered
    'App\Events\OrderOrdering' => [
        'App\Listeners\OrderOrdered',
    ],
    //
    // order un-ordered
    'App\Events\OrderUnOrdering' => [
        'App\Listeners\OrderUnOrdered',
    ],
    //
    // order processing
    'App\Events\OrderProcessing' => [
        'App\Listeners\OrderProcessed',
    ],
    //
    // order un-processing
    'App\Events\OrderUnProcessing' => [
        'App\Listeners\OrderUnProcessed',
    ],
    //
    // payment type
    'App\Events\PaymentTypeAdding' => [
        'App\Listeners\PaymentTypeAdded',
    ],
    //
    // seller stock
    'App\Events\SellerStockAdding' => [
        'App\Listeners\SellerStockAdded',
    ],
    'App\Events\SellerStockRemoving' => [
        'App\Listeners\SellerStockRemoved',
    ],
    //
    // stock count
    'App\Events\StockCountAdjustment' => [
        'App\Listeners\StockCountAdjusted',
    ],
    'App\Events\StockCountAdding' => [
        'App\Listeners\StockCountAdded',
    ],
    //
    // system role/permissions
    'App\Events\SystemRolesPermissionsChange' => [
        'App\Listeners\SystemRolesPermissionsChanged',
    ],
    //
    // user password - changed by user
    'App\Events\UserChangingPassword' => [
        'App\Listeners\UserChangedPassword',
    ],
    //
    // user details
    'App\Events\UserDetailsChange' => [
        'App\Listeners\UserDetailsChanged',
    ],
    //
    // user password - changed by admin
    'App\Events\UserPasswordChange' => [
        'App\Listeners\UserPasswordChanged',
    ],
    //
    // user role/permissions
    'App\Events\UserRolesPermissionsChange' => [
        'App\Listeners\UserRolesPermissionsChanged',
    ],
    //
    // user suspended
    'App\Events\UserSuspending' => [
        'App\Listeners\UserSuspended',
    ],
    //
    // user un-suspended
    'App\Events\UserUnSuspending' => [
        'App\Listeners\UserUnSuspended',
    ],
    //
    // vehicle cost
    'App\Events\VehicleCostAdding' => [
        'App\Listeners\VehicleCostAdded',
    ],
    //
    // vehicle cost type
    'App\Events\VehicleCostTypeAdding' => [
        'App\Listeners\VehicleCostTypeAdded',
    ],

    'App\Events\Wowcher\OrderReceivedByMerchant' => [
        'App\Listeners\Wowcher\NotifyWowcherOrderCreated'
    ]
    ,
    'App\Events\Wowcher\OrderDispatched' => [
        'App\Listeners\Wowcher\NotifyWowcherOrderDispatched'
    ]
    ,
    'App\Events\Wowcher\OrderReadyForDispatch' => [
        'App\Listeners\Wowcher\NotifyWowcherOrderReadyForDispatch'
    ],    
  ];

  /**
   * Register any other events for your application.
   *
   * @param  \Illuminate\Contracts\Events\Dispatcher  $events
   * @return void
   */
  public function boot(DispatcherContract $events)
  {
    parent::boot($events);
    //
  }
}
