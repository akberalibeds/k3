<?php

namespace App;

use App\Helper\Traits\QueryableModel;
use Illuminate\Database\Eloquent\Model;

class DriverMate extends Model
{
  use QueryableModel;
  /**
   * @var
   */
  public static $rules = [
    'store' => [
      'name' => 'required|max:255|min:4',
      'num' => 'string|max:24',
      'rate' => 'numeric|min:0|max:9999.99',
    ],
    'update' => [
      'id' => 'requied',
      'name' => 'required|max:255|min:4',
      'num' => 'string|max:24',
      'rate' => 'numeric|min:0|max:9999.99',
    ],
  ];
  /**
   * The table associated with the model.
   *
   * @var string
   */
  protected $table = 'mates';
  /**
   * @author Giomani Designs (Development Team)
   * @param  array   $queryStrings the query string
   * @return Illuminate\Pagination\LengthAwarePaginator
   */
  public static function getAll($queryStrings) {
    $query = static::select('*');
    if (count($queryStrings) > 0) {
      $query = parent::modForQueryString($query, $queryStrings);
    }
    return $query->orderBY('name', 'asc')->paginate(config('search.rpp'));
  }
  
  /**
   * @author Giomani Designs (Development Team)
   * @param  array   $queryStrings the query string
   * @return Illuminate\Pagination\LengthAwarePaginator
   */
  public static function getActive() {
  	$query = static::select('name','num');
  	return $query->orderBY('name', 'asc')->get();
  }
}
