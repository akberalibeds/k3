<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;

class OrderNotes extends Model
{
  /**
   * The table associated with the model.
   *
   * @var string
   */
  protected $table = 'orderNotes';


   /**
   * Use created_at update_at fields.
   *
   * @var boolean
   */
  public $timestamps=false;




  public static function updateNote($data,$id){

	  \DB::table('orderNotes')
			->where('id', $id)
            ->update($data);

  }

  
  public static function addNote($notes, $id, $driver=null) {

		$note = new OrderNotes;
		$note['date'] = date("d-M-Y");
		$note['time'] = date("H:i");
		$note['staff']=	($driver) ? $driver : Auth::user()->email;
		$note['id_staff'] = ($driver) ? 0 : Auth::user()->id;
		$note['oid'] = $id;
		$note['notes'] = $notes;
		$note->save();

		return $note;

	}

}
