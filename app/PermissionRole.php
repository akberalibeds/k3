<?php

namespace App;

use DB;
use Illuminate\Database\Eloquent\Model;

class PermissionRole extends Model
{
  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = ['permission_id', 'role_id'];
  /**
   * The table associated with the model.
   *
   * @var string
   */
  protected $table = 'permission_role';
  /**
   * The table associated with the model.
   *
   * @var string
   */
  public $timestamps = false;
  /**
   * @author Giomani Designs
   * @param  array|int $roleId      the role-id
   * @param  array|int $permissions array of permission ids to add to the role
   */
  public static function addPermissions($roleId, $permissions)
  {
    $inserts = [];

    foreach ($permissions as $permission) {
      $inserts[] = ['permission_id' => $permission, 'role_id' => $roleId];
    }
    DB::table('permission_role')->insert($inserts);
  }
  /**
   * @author Giomani Designs
   * @param  array|int $roleId      the role-id
   * @param  array|int $permissions array of permission ids to add to the role
   */
  public static function removePermissions($roleId, $permissions)
  {
    PermissionRole::where('role_id', '=', $roleId)
      ->whereIn('permission_id', $permissions)
      ->delete();
  }
}
