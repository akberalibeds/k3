<?php

namespace App;

use DB;
use Illuminate\Database\Eloquent\Model;

class Dispatched extends Model
{
  /**
   * @var string
   */
  protected $table = 'disp';
  /**
   * @var string
   */
  public $timestamps  = false;

  public function routes() 
  {
    return $this->belongsTo(Routes::class, 'name', 'route');
  }
  
  public function notifications() {
    return $this->hasMany(\App\Notification::class, 'dispId','id_disp');
  }

  /**
   * Get the details
   *
   * @param  String $routeId
   * @param  String $date a text verion of the date in the format dd-mmm-yyyy
   * @param  String $routeName - needed becasue the route-id (route_id) column is not being filled
   * @param  $date
   * @return Illuminate\Database\Query\Builder
   */
  public static function getRouteOrders($routeId, $date, $routeName)
  {
    return Dispatched::select(
        'orders.code',
        'oid',
        'delOrder',
        'orderStatus',
        'customers.dBusinessName',
        'dPostcode',
        'txt',
        'tel',
    	'extra'
      )
      ->join('orders', 'disp.oid', '=', 'orders.id')
      ->join('customers', 'orders.cid', '=', 'customers.id')
      ->where('date', '=', $date)
      ->where(function ($query) use ($routeId, $routeName) {
        $query
          ->where('id_route', '=', $routeId)
          ->orWhere('route', '=', $routeName);
        }
      )
      ->orderBy('delOrder', 'asc')
      ->get();
  }
}
