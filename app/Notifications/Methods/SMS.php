<?php

namespace App\Notifications\Methods;

use Log;
use App\Notifications\NotificationMethod;

class SMS extends NotificationMethod
{
    private $username = "u6282", $password = "vrdx7", $originator = "+447860020123";

    public function validateRecipient($notification)
    {
      return null !== $this->_recipient($notification);
    }

    function _recipient($notification)
    {
        if (isset($notification->customer)) {
            if (isset($notification->customer->mob) || isset($notification->customer->tel)) {
                $formatMob = $this->formatNumber($notification->customer->mob);
                $formatTel =  $this->formatNumber($notification->customer->tel);
                if($this->verifyNumber($formatMob)){
                    return $formatMob;
                } else if ($this->verifyNumber($formatTel)){
                    return $formatTel;
                }
            }
        }
    }
    
    /**
     * Format a Customer Numbers
     *  This allows to format a customer's number to a DB compatible format
     * @param   Integer     Customer's Intended Number to use
     * @return  Integer     Formatted version of the number
     */
    private function formatNumber($number){
        return str_replace(' ', '', $number);
    }

    /**
     * Verify the intended Customer's Number
     *  This allows to verify if the provided number is a proper number
     * @param   Integer     Customer's Formatted Number
     * @return  Boolean     Valid Number to use or not
     */
    private function verifyNumber($number)
    {
        if (! is_null($number) && ! empty($number)) {
            if (is_numeric($number)) {
                if (strlen($number) >= 11 && strlen($number) <= 13) {
                    return true;
                }
            }
        }
        return false;
    }

    public function send($message)
    {
        if (app()->environment() != 'testing') {
            
            if (! $message || is_null($message->recipient)) return false;

            try {
                $URL = "https://api.textmarketer.co.uk/gateway/?username=$this->username&password=$this->password&option=xml";
                $URL .= "&number=$message->recipient&message=" . urlencode($message->body) . '&orig=' . urlencode($this->originator);
                Log::info("Seb logging" . $URL);
                
                    $response         = simplexml_load_file($URL);
                    if(! $response->reason == ""){
                        
                        $reason = (string)$response->reason;
                        Log::info($reason);

                        return [
                            'sent' => false,
                            'errors' => $reason
                        ];
                    } else {
                        $array            = array();
                        $array['id']      = $response['id'];
                        $array['credits'] = $response->credits;
                        $array['status']  = $response['status'];
                        $array['used']    = $response->credits_used;

                        Log::info("Send SMS to {$message->recipient} with message {$message->body}");
                
                        return [
                            'sent' => true,
                            'errors' => $array['status']
                        ];
                    }               

            } catch (\Exception $e) {

                Log::info($e->getMessage());

                return [
                    'sent' => false,
                    'errors' => $e->getMessage()
                ];
            }
        } else {
            return [
                'sent' => false,
                'errors' => 'In Testing Mode'
            ];
        }     
    }

    public function getRecipientContact($notification)
    {
        if (! $this->validateRecipient($notification)) return Null;

        return $this->_recipient($notification);
    }

    public function format()
    {
        return (object) ['length' => 'short', 'representation' => 'plain_text'];
    }

}