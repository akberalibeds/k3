<?php

namespace App\Notifications\Methods;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use App\Notifications\NotificationMethod;

class Email extends NotificationMethod
{

    public function validateRecipient($notification)
    {
        if (! $notification || ! $notification->customer) return false;

        if (! $notification->customer->email1 || ! filter_var($notification->customer->email1, FILTER_VALIDATE_EMAIL)) return false;

        return true;
    }

    public function send($message)
    {

            if (! $message || is_null($message->recipient)) return false;

            $view = (new \ReflectionClass($message->notification->typeInstance()))->getShortName(); // i.e DispatchReady

            $settings = DB::table('emailSettings')->select('*')->where('name', '=', 'pd')->first();

            try {

                Mail::send("emails.{$view}", ['notification_message' => $message], function ($mail) use ($settings, $message) {

                    $mail->from($settings->email, 'Premier Deliveries');
        
                    $mail->to($message->recipient, $message->recipient_name)->subject($message->subject);
                });

                return [
                    'sent' => true,
                    'errors' => ''
                ];

            } catch (\Exception $e) {

                \Log::info($e->getMessage());

                return [
                    'sent' => false,
                    'errors' => $e->getMessage()
                ];
            }
    }

    public function getRecipientContact($notification)
    {
        if (! $this->validateRecipient($notification)) return Null;

        return $notification->customer->email1;
    }

    public function format()
    {
       return (object) ['length' => 'long', 'representation' => 'html'];
    }
}