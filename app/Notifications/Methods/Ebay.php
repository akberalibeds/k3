<?php

namespace App\Notifications\Methods;

use App\Helper\Ebay\Ebay as CoreEbay;
use App\Notifications\NotificationMethod;

class Ebay extends NotificationMethod
{
    protected $ebay;
    
    public function __construct()
    {
        $this->ebay = new CoreEbay();
    }


    public function validateRecipient($notification)
    {
        if (! $notification || ! $notification->customer) return false;

        if (! $notification->customer->userID) return false;

        if (! $notification->order->ebayID ) return false;

        return true;
    }

    // Change Testing Scenario as necessary
    public function send($message)
    {
        if(! app()->environment() == 'testing') {

            if (! $message || is_null($message->recipient)) return false;

            try {

                $result = $this->ebay->send_message([
                    'item' => explode("-", $message->notification->order->ebayID)[0],
                    'subject' => $message->subject,
                    'body' => $message->body,
                    'buyer' => $message->recipient,
                ]);
                
                if ($result) {
                  return [
                      'sent' => true,
                      'errors' => ''
                  ];
                } else {
                  return [
                      'sent' => false,
                      'errors' => 'Unknown failure'
                  ];
                }

            } catch (\Exception $e) {

                \Log::info($e->getMessage());

                return [
                    'sent' => false,
                    'errors' => $e->getMessage()
                ];
            }
        } else {

            return [
                'sent' => true,
                'errors' => ''
            ];
        }
    }

    public function getRecipientContact($notification)
    {
        if (! $this->validateRecipient($notification)) return Null;

        return $notification->customer->userID;
    }

    public function format()
    {
        return (object) ['length' => 'short', 'representation' => 'plain_text'];
    }
}