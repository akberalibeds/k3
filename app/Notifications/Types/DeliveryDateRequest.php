<?php

namespace App\Notifications\Types;

use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use App\Notifications\NotificationType;

class DeliveryDateRequest extends NotificationType
{
    public function __construct()
    {
        
    }

    public function query()
    {
        return  \App\Order::query()
                    ->select('orders.id as oid', 'cid')
                    ->where('done', '=', 0)
                    ->where('orderStatus', 'Allocated')
                    ->where('time_stamp', '>=', Carbon::now()->subDays(7));
    }
    
    public function repeatAfter() 
    {
      return Carbon::now()->subDay();
    }

    public function messageText($message_format, $notification) // given that message_format is an object
    {
        $code = $notification->order->code;
        $url = "http://premierdeliveries.co.uk?id={$notification->order->code}";
      
// $messages = [
//             'short' => [
//                 'html' => "<p>Due to Covid 19 please expect delays however our delivery team are doing their upmost best to get all orders out as quick as possible and will be in touch to confirm your delivery date and time</p>",

//                 'plain_text' => "Due to COVID-19 Service to track or choose a delivery date is currently Unavailable.
// We will contact you to book in your recent furniture order.
// Thanks"
//                  //'plain_text' => "Please Choose A Delivery Date for your furniture order.\nParcel ID2: $code\nPlease go to: $url"
//             ],
//             'long' => [

//                 'html' =>  "<p>Dear Customer, </p>
//                 <p>Due to Covid 19 please expect delays however our delivery team are doing their upmost best to get all orders out as quick as possible and will be in touch to confirm your delivery date and time. Please ensure correct contact details are provided.<br /><br /> Please don't respond this email thank you so much.</p>
//                 <p>Many Thanks,<br> Premier Deliveries Team</p><br /><br /><p>This email is automatically generated please do not reply to it. If you wish to speak to someone regarding your order please contact your vendor directly.</p>",

//                 'plain_text' => "Due to Covid 19 please expect delays however our delivery team are doing their upmost best to get all orders out as quick as possible and will be in touch to confirm your delivery date and time. Please ensure correct contact details are provided. \n\nPlease don't respond this email thank you.  \n\n\n\n This email is automatically generated please do not reply to it. If you wish to speak to someone regarding your order please contact your vendor directly."

//             ]
            
            $messages = [
            'short' => [
                'html' => "<p>Please Choose A Delivery Date for your furniture order.<br>Parcel ID: $code<br>Continue to this link: 
                            <a href='$url'>$url</a></p>",

                'plain_text' => "Please Choose A Delivery Date for your furniture order.\nParcel ID: $code\nPlease go to: $url"
            ],
            'long' => [

                'html' =>  "<p>Dear Customer, </p>
                <p>Please click the link below to be redirected to our webpage where you will be required to input the following information in order to choose a delivery date suitable for you. <br>
                    <br>
                    <b style='color:red;'>Please note: Your order will not be Dispatched <span style=\"text-decoration: underline\">until you have confirmed a delivery date.</span></b>
                </p> 
                <ul>
                    <li>Your Delivery Postcode to login</li>
                    <li>Choose <b>ONE</b> of the following options: 
                    <br>
                    <ol>
                        <li>Choose a Delivery date at your address</li>
                        <li>Deliver to a neighbour</li>
                        <li>Deliver to a Safe Place</li>
                        </li>
                    </ol>
                </ul>
                <a href='$url'>$url</a>
                <br><br>
                <p>Many Thanks,<br> Premier Deliveries Team</p>",

                'plain_text' => "Please choose a Delivery Date for your furniture order. \nParcel ID: $code\n\nPlease go to:\n$url , with your Delivery Postcode to login.\nPlease Note your order will not be Dispatched until you have confirmed a delivery date."

            ]
        ];

        return $messages[$message_format->length][$message_format->representation];
    }

    public function messageSubject($notification)
    {
        return "Delivery Update of Furniture Order {$notification->order->code}";
        //return "Delivery Date for your Furniture Order {$notification->order->code}";
    }

    public function schedule()
    {
          return '*/5 * * * *'; // 5-minutes
    }


}