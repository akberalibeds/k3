<?php

namespace App\Notifications\Types;

use Time;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use App\Notifications\NotificationType;

class DispatchReady extends NotificationType
{

    public function __construct()
    {

    }

    /**
     * Asks for a specific condition from the database to use
     * @return MySQL Query of the Database
     */
    public function query()
    {
        return  \App\Dispatched::query()
            ->join('orders', 'orders.id', '=', 'disp.oid')
            ->where('disp.date', '=', Carbon::now()->addDays(1)->format('d-M-Y') )
             ->whereNotNull('deltime')
             ->where('deltime','<>','');
        
    }

    /**
     * Type of Message Text that is sent to the customer
     *      Format Variation of the Message
     *      Text Format of the message
     * @param message_format Format of the message
     * @param notification Notification list
     */
    public function messageText($message_format, $notification)
    {
        $slot = $this->timeRange($notification->order->delTime);
        $messages = [
            'short' => [
                'html' => "<p>Your item(s) have been dispatched, and will be delivered to you tomorrow between the hours of $slot.<br><br>Thank you for your business.</p>",

                'plain_text' => "Your item(s) have been dispatched, and will be delivered to you tomorrow between the hours of $slot.\n\nThank you for your business."
            ],
            'long' => [

                'html' =>  "<p>Dear Customer, </p>
                <p>Your item(s) have been dispatched, and will be delivered to you tomorrow between the hours of $slot.</p>
                <p>Thank you for your business.</p>
                <p>Many Thanks,<br>Premier Deliveries Team</p>",

                'plain_text' => "Your item(s) has been dispatched, and will be delivered to you tomorrow between the hours of $slot.\n\nThank you for your business."

            ]
        ];

        return $messages[$message_format->length][$message_format->representation];
    }

    /**
     * Message Subject of the Notification
     * 
     * @param notification Notification List
     * @return String Item is dispatched
     */
    public function messageSubject($notification)
    {
        return 'Your items have been dispatched';
    }

    /**
     * Scheduler that works with CronJobs
     * 
     * @return String Hourly CronJob Schedule Format
     */
    public function schedule()
    {
        return '0/5 8-22 * * *';  // hourly
    }




    private function timeRange($time)
	{
	
		$time.=":00";
		$startTime = Time::date($time)->hours(-1)->get('H:00');
		$finishTime = Time::date($time)->hours(5)->get('H:00');
		
		if (strtotime($startTime)<strtotime("7:00:00")){
				$startTime="07:00";
		}
	
		if (strtotime($finishTime)>strtotime("21:00:00")){
				$finishTime="21:00";
		}
			
		for($u=0;$u<7;$u++){
			if ($finishTime=="0$u:00"){
					$finishTime="21:00";
			}
		}
				
		return "$startTime-$finishTime";
	}
}
