<?php

namespace App\Notifications\Types;

use Time;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use App\Notifications\NotificationType;

class DeliveryImminent extends NotificationType
{
    public function __construct()
    {

    }

    public function query()
    {
        return  \App\Dispatched::query()
            ->join('orders', 'orders.id', '=', 'disp.oid')
            ->where('orderStatus', '=', 'Routed')
            ->where('disp.date', '=', Carbon::now()->format('d-M-Y'))
            ->where('orders.delTime', '<', Carbon::now()->addHours(2)->format('H:m'));
    }

    public function messageText($message_format, $notification)
    {
        $us = "0121 568 8878";
        $slot = $this->timeRange($notification->order->delTime);
        
        $messages = [
            'short' => [
                'html' => "<p>Your Furniture Order will be delivered shortly. For further information, please call <a href='tel:$us'>$us</a></p>",
                
                'plain_text' => "Your Furniture Order will be delivered shortly. For further information, please call <a href='tel:$us'>$us</a>."
            ],
            'long' => [
                // Cross Check if there is a support email or delivery support email
                'html' => "<p>Dear Customer, </p>
                <p>Your Furniture Order will be delivered shortly.<br></p>
                <p>For any further information, please call <a href='tel:$us'>$us</a>.</p>
                <p>Many Thanks,<br>Premier Deliveries Team</p>",

                'plain_text' => "Your Furniture Order will be delivered shortly. For any further information, please call $us."
            ]
        ];

        return $messages[$message_format->length][$message_format->representation];
    }
    
    private function timeRange($time)
    {

        $time.=":00";
        $startTime = Time::date($time)->hours(-1)->get('H:00');
        $finishTime = Time::date($time)->hours(5)->get('H:00');

        if (strtotime($startTime)<strtotime("7:00:00")){
                        $startTime="07:00";
        }

        if (strtotime($finishTime)>strtotime("21:00:00")){
                        $finishTime="21:00";
        }

        for($u=0;$u<7;$u++){
                if ($finishTime=="0$u:00"){
                                $finishTime="21:00";
                }
        }

        return "$startTime-$finishTime";
    }
    public function schedule()
    {
        return '*/20 * * * *'; // 20-mins
    }

    public function messageSubject($notification)
    {
        return 'Your order will be delivered soon';
    }

    
    public function denied_methods()
           
    {
      return ['App\Notifications\Methods\Ebay'];
    }
}