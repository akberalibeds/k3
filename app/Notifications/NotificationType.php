<?php

namespace App\Notifications;

abstract class NotificationType
{
  
    const REPEATSTRING = ' (repeat)';
    
    public static function instantiate($typename) 
    {
      if (strpos($typename, static::REPEATSTRING)>0) {
        $typename = str_replace(static::REPEATSTRING, '', $typename);
        $instance = new $typename;
        $instance->repeatNotification = true;
      } else {
        $instance = new $typename;
      }
      if (! $instance instanceof NotificationType) {
        throw new RuntimeException("Bad notification type: $typename");
      }
      return $instance;
    }
    
    public static function getTypes() {
      return config('notifications.types');
    }
    
    /**
     * Query the object (orders, dispatches) for which notifications may be generated
     * 
     * @return Query object
     */
    abstract public function query();

    abstract public function messageText($message_format,$notification);

    abstract public function schedule();

    public function allowed_methods()  {
      return array_diff(NotificationMethod::getMethods(), $this->denied_methods());
    }
    
    
    public function denied_methods() {
      return [];
    }

    /**
     * Type of Notification
     *      Grabs the information of the class name of
     *      Email, Ebay and/or SMS
     * 
     * @return String Class name
     */

    public function type()
    {
       return static::class;
    }
    public function repeatType() 
    {
        return static::class . static::REPEATSTRING;
    }
    public function repeatAfter()
    {
      return false; // most recent carbon-time after which we can repeat, or false for never.
    }

    
    var $repeatNotification = false; // is this instance for repeat notifications?
}