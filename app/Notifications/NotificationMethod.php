<?php

namespace App\Notifications;

abstract class NotificationMethod
{
    abstract public function validateRecipient($notification);

    abstract public function send($message);

    abstract public function getRecipientContact($notification);

    abstract public function format();
    
    public static function getMethods() {
      return config('notifications.methods');
    }
}