<?php

namespace App;

use DB;
use Illuminate\Database\Eloquent\Model;

class ToShip extends Model
{
  /**
   * The table associated with the model.
   *
   * @var string
   */
  protected $table = 'toShip';
  
  public $timestamps  = false;
  
  
  
  public static function customersToNotify($date)
  {
	  
	  return self::where('date','=',"$date")
				->select('toShip.oid','toShip.tel as isTel','toShip.mail as isMail','orders.code','orders.ebayID','customers.tel',
				'orders.delTime','customers.userID','orders.ebay','orders.done','customers.dBusinessName','customers.email1 as email')
				->join('orders','toShip.oid','=','orders.id')
				->join('customers','orders.cid','=','customers.id')
				->get();	
		  
  }
  
  
  public static function removeOld($date)
  {
  		self::where('date','!=',"$date")->delete();
  }
  
  
}
