<?php

namespace App\Facades;
use Illuminate\Support\Facades\Facade;


class Sale extends Facade{
	
   protected static function getFacadeAccessor() { return 'sale'; }

}