<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Order;

class Count extends Model
{
  /**
   * The table associated with the model.
   *
   * @var string
   */
  protected $table = 'count'; 
  public $timestamps = false;
  
  
  
  	/*
	 *
	 */
	public static function setIno($id){
		
		$order = Order::where(['id' => $id])->select('iNo')->first();
		
		if($order->iNo==0){
			$count = Count::select('num')->get()->first();	
			$count = $count->num;
			Count::where('num',$count)->update(['num' => (++$count)]);
			Order::where('id',$id)->update(['iNo' => $count , 'proforma' => 0 , 'time_stamp' => date('Y-m-d H:i:s')]);
		}
		
	}
	
	public static function getRefundNo(){
		
		$count = Count::select('refund')->get()->first();
		Count::where('refund',$count->refund)->update(['refund' => $count->refund+1]);
		return $count->refund;
		
	}
	
	public static function getPostNo(){
		
		$count = Count::select('post')->get()->first();
		Count::where('post',$count->post)->update(['post' => $count->post+1]);
		
		return $count->post;
		
	}
	
	
	public static function getExchangeNo(){
		
		$count = Count::select('exchange')->get()->first();
		Count::where('exchange',$count->exchange)->update(['exchange' => $count->exchange+1]);
		
		return $count->exchange;
		
	}
  
}
