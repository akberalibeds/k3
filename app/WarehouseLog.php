<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WarehouseLog extends Model
{
   
   
     protected $table = 'warehouse_log';
     //public $timestamps = true;
   	 
     protected $guarded = [];
     
     public function user() {
       return $this->belongsTo('App\User','staff_id','id');
     }
     
   
   
   
}
