<?php

namespace App;

use DB;
use App\Helper\Traits\QueryableModel;
use Illuminate\Database\Eloquent\Model;

class Seller extends Model
{
  use QueryableModel;
  /**
   * @var
   */
  public static $rules = [
    'store' => [
      'account_limit' => 'required|numeric|min:0|max:10000',
      'discount' => 'required|numeric|min:0|max:50',
      'email' => 'required|email',
      'iName' => 'required|max:64',
      'name' => 'required|max:255',
      'num' => 'required',
      'pc' => 'required',
      'tel' => 'required',
    ],
    'update' => [
      'account_limit' => 'required|numeric|min:0|max:10000',
      'discount' => 'required|numeric|min:0|max:50',
      'email' => 'required|email',
      'iName' => 'required|max:64',
      'name' => 'required|max:255',
      'num' => 'required',
      'pc' => 'required',
      'tel' => 'required',
    ],
  ];
  /**
   * @author Giomani Designs (Development Team)
   * @param  mixed $value
   * @return string
   */
  public function getAccountLimitAttribute ($value) {
    return is_numeric($value) ? number_format($value, 2) : '0.00';
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  array   $queryStrings the query string
   * @return Illuminate\Pagination\LengthAwarePaginator
   */
  public static function getAll($queryStrings = []) {
    $query = static::select('*');
    if (count($queryStrings) > 0) {
      $query = parent::modForQueryString($query, $queryStrings);
    }
    return $query->orderBY('name')->paginate(config('search.rpp'));
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  mixed $value
   * @return string
   */
  public function getDiscountAttribute($value) {
    return is_numeric($value) ? number_format($value, 2) : '0.00';
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  array   $queryStrings the query string
   * @return Illuminate\Pagination\LengthAwarePaginator
   */
  public static function getForSelect() {
    return static::select('*')->orderBY('name')->get();
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  mixed $value
   * @return string
   */
  public static function getOverview () {
    $query = static::select(
        'sellers.id',
        'name',
        'iName',
        DB::raw('SUM(paid) as orders_paid'),
        DB::raw('SUM(IF(total <= paid, 1, 0)) as orders_paid_count'),
        DB::raw('SUM(total) as orders_total'),
        DB::raw('COUNT(*) as orders_count')
      )
      ->leftJoin('orders', 'orders.id_seller', '=', 'sellers.id')
      ->groupBy('sellers.id')
      ->orderBy('sellers.name');

    return $query->paginate(config('search.rpp'));
  }
  
  /**
   * @author Giomani Designs
   * @param  id   
   * @return Illuminate\Pagination\LengthAwarePaginator
   */
  public static function getSeller($id) {
    return static::find($id);
  }
}
