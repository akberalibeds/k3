<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

//------------
// DEPRICATED
//
// Use App\Helper\Traits\QueryableModel instead
//------------
class BaseModel extends Model
{
  /**
   * @author Giomani Designs
   * @param         $object  the query object
   * @param array   $columns the columns to search
   * @param string  $query the term to search for
   * @return Illuminate\Database\Query\Builder
   */
  protected static function modForQuery($object, $columns, $query) {
    $object->where(function ($qz) use ($columns, $query) {
      foreach ($columns as $column) {
        $qz->orWhere($column, 'like', '%' . $query . '%');
      }
    });
    return $object;
  }
  /**
   * @author Giomani Designs
   * @param         $query       the query object
   * @param string  $queryString the query to apply to the query
   * @return Illuminate\Database\Query\Builder
   */
  protected static function modForQueryString($query, $queryStrings) {
    if (isset($queryStrings['query'])) {
      $ps = explode('|', $queryStrings['query']);
      $query->where($ps[0], 'like', '%' . $ps[1] . '%');
    }
    if (isset($queryStrings['filter'])) {
      $ps = explode('|', $queryStrings['filter']);
      $query->where($ps[0], '=', $ps[1]);
    }
    if (isset($queryStrings['order'])) {
      $ps = explode('|', $queryStrings['order']);
      $query->orderBy($ps[0], $ps[1]);
    }
    return $query;
  }
}
