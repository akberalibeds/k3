<?php

namespace App;

use DB;
use App\Helper\Traits\QueryableModel;
use Illuminate\Database\Eloquent\Model;

class RouteDay extends Model
{
  use QueryableModel;
  /**
   * The table associated with the model.
   *
   * @var string
   */
  protected $table = 'routeDays';
  /**
   * @author Giomani Designs (Development Team)
   * @param  array   $queryStrings the query string
   * @return Illuminate\Pagination\LengthAwarePaginator
   */
  public static function getDays($id, $queryStrings = []) {
    $query = static::queryForRouteDays();
    if (count($queryStrings) > 0) {
      $query = parent::modForQueryString($query, $queryStrings);
    }
    return $query->where('routeName.id', '=', $id)->first();
  }
  /**
   * @author Giomani Designs (Development Team)
   * @param  array   $queryStrings the query string
   * @return Illuminate\Pagination\LengthAwarePaginator
   */
  public static function getAllDays($queryStrings = []) {
    $query = static::queryForRouteDays();
    if (count($queryStrings) > 0) {
      $query = parent::modForQueryString($query, $queryStrings);
    }
    return $query->orderBY('route', 'asc')->paginate(config('search.rpp'));
  }
  /**
   * @author Giomani Designs (Development Team)
   * @return Illuminate\Pagination\LengthAwarePaginator
   */
  public static function queryForRouteDays() {
    return RouteDay::select(
      DB::raw('group_concat(DISTINCT postCode separator ", ") as postcodes'),
      DB::raw('group_concat(DISTINCT day separator ";") as days'),

      'route',
      'route_id as id',
      'routeName.max as max',
      'van'
    )
    ->leftJoin('routeName', 'routeName.id', '=', 'routeDays.route_id')
    ->leftJoin('routeStops', 'routeStops.routeID', '=', 'routeDays.route_id')
    ->groupBy('route_id');
    ;

  //   return Routes::select(
  //       DB::raw('group_concat(DISTINCT postCode separator ", ") as postcodes'),
  //       DB::raw('group_concat(DISTINCT day separator ";") as days'),
  //       'in_load_count',
  //       'is_route_route',
  //       'is_wholesale',
  //       'msg',
  //       'name',
  //       'routeName.id',
  //       'routeName.max as max',
  //       'van'
  //     )
  //     ->leftJoin('routeDays', 'routeDays.route_id', '=', 'routeName.id')
  //     ->leftJoin('routeStops', 'routeStops.routeID', '=', 'routeName.id')
  //     ->groupBy('routeName.id');
  }
}
